package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import com.svcon.jdbf.*;
import javax.sql.RowSet;
import java.sql.*;
import java.util.zip.*;
import java.io.*;
import java.net.*;
import neo.qltn.ftp;
import neo.qltn.FTPupload;
import sun.misc.*;

public class qltn_common extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("QLTN Gach no");
		//funcschema=getSysConst("FuncSchema");
    	//dataschema=getUserVar("sys_dataschema");
    	//userid=getUserVar("userID");		
	}
	public String doc_tuyenthu(
		String psDonviQL
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_lay_ds_nv"
  			+ "&" + CesarCode.encode("donviql_id") + "=" + psDonviQL;
	}
	public String doc_ds_khachhang(
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_ds_KHACHHANGS_s"
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String doc_ds_thuebao(
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_ds_thuebao_s"
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String doc_tim_khachhang(
		String psSomay,
		String psTen_TB,
		String psDiachi_CT,
		String psMa_KH,
		String psCoquan,
		String psDiachi_KH,
		String psMa_TT,
		String psTen_TT,
		String psDiachi_TT,
		String psDonvi,
		String psMa_nv,		
		String psChukyno,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_tim_KHACHHANGS_s"
			+ "&" + CesarCode.encode("SOMAY") + "=" + psSomay
			+ "&" + CesarCode.encode("TEN_TB") + "=" + psTen_TB
			+ "&" + CesarCode.encode("DIACHI_CT") + "=" + psDiachi_CT
			+ "&" + CesarCode.encode("MA_KH") + "=" + psMa_KH
			+ "&" + CesarCode.encode("COQUAN") + "=" + psCoquan
			+ "&" + CesarCode.encode("DIACHI_KH") + "=" + psDiachi_KH
			+ "&" + CesarCode.encode("MA_TT") + "=" + psMa_TT
			+ "&" + CesarCode.encode("TEN_TT") + "=" + psTen_TT
			+ "&" + CesarCode.encode("DIACHI_TT") + "=" + psDiachi_TT
			+ "&" + CesarCode.encode("DONVI") + "=" + psDonvi
			+ "&" + CesarCode.encode("ma_nv") + "=" + psMa_nv
			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno	
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String doc_tim_thuebao(
		String psSomay,
		String psTen_TB,
		String psDiachi_CT,
		String psMa_KH,
		String psCoquan,
		String psDiachi_KH,
		String psMa_TT,
		String psTen_TT,
		String psDiachi_TT,
		String psDonvi,
		String psMa_nv,	
		String psChukyno,	
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_tim_thuebao_s"
			+ "&" + CesarCode.encode("SOMAY") + "=" + psSomay
			+ "&" + CesarCode.encode("TEN_TB") + "=" + psTen_TB
			+ "&" + CesarCode.encode("DIACHI_CT") + "=" + psDiachi_CT
			+ "&" + CesarCode.encode("MA_KH") + "=" + psMa_KH
			+ "&" + CesarCode.encode("COQUAN") + "=" + psCoquan
			+ "&" + CesarCode.encode("DIACHI_KH") + "=" + psDiachi_KH
			+ "&" + CesarCode.encode("MA_TT") + "=" + psMa_TT
			+ "&" + CesarCode.encode("TEN_TT") + "=" + psTen_TT
			+ "&" + CesarCode.encode("DIACHI_TT") + "=" + psDiachi_TT
			+ "&" + CesarCode.encode("DONVI") + "=" + psDonvi
			+ "&" + CesarCode.encode("ma_nv") + "=" + psMa_nv
			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String value_capnhat_seri
	       (String seri
           ,String quyen
           ,String soseri)
	{		
	 	return  "begin ?:="+getSysConst("FuncSchema")+"qltn.capnhat_seri('"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"','"+seri+"','"+quyen+"','"+soseri+"'); end;";	 
	}
	public String laytt_seri()
	{
		return "select seri,quyen,soseri from "+getUserVar("sys_dataschema")+"nguoigachnos where nguoigach='"+getUserVar("userID")+"'";
	}
	public String doc_ds_chukyno(
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_gachno_chungtu/ajax_ds_chukyno"
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String rec_tt_khachhang_doituyen(
		String psUserInput,
		String psDonviQL
	)
	{
		return "begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.laytt_kh_doituyen('"+psUserInput+"',"+psDonviQL+",'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
	}
	public String value_capnhat_congno(
		String psMaKH,
        String psTenTT,
        String psDiachiTT,
        String psMSThue,
        String psSodaidien,
        String psMaBC,
        String psMaNV_Cu,
        String psChukyno,
        String psMaNV_Moi,
        String psThang,
        String psChuyenTTKH,
        String psCMTND
	)
	{
		return "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.chuyentuyen_congno_kh('"+psMaKH
			+"','"+psTenTT+"','"+psDiachiTT+"','"+psMSThue+"','"+psSodaidien+"','"+psMaBC+"','"
			+psMaNV_Cu+"' ,'"+psChukyno+"','"+psMaNV_Moi+"','"+psThang+"','"
			+psChuyenTTKH+"','"+psCMTND+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"') ; end;";
	}
	public String value_check_trangthai(String psTrangthai)
	{
		String s= "begin ? := "
			+getSysConst("FuncSchema")+"qltn_donno.check_trang_thai('"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"','"
			+psTrangthai
			+"'); end; ";
		System.out.println(s);
		return s;
	}
	public String value_taobang_kyhoadon(String psKyhoadon)
	{
		String s= "begin ? := "
			+getSysConst("FuncSchema")+"qltn_donno.tao_bang_all('"
			+psKyhoadon+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")
			+"'); end; ";
		System.out.println(s);
		return s;
	}
	public String value_chotno_cuoiky(String psKyhoadon)
	{
		String s= "begin ? := "
			+getSysConst("FuncSchema")+"qltn_donno.chotno_cuoiky('"
			+psKyhoadon+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")
			+"'); end; ";
		System.out.println(s);
		return s;
	}
	public String value_agent_id(String psSomay)
	{
		return "begin ? := "+getSysConst("FuncSchema")+
			"qltn_tracuu.lay_agent_parameter('"+psSomay+"','agent_id'); end;";
	}
	public String doc_tracuu_phieuhuy(
		String psForm,
		String psTungay,
		String psDenngay,
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psSeri,
		String psPageNum,
		String psPageRec
	)
	{			
		int psForm_=1;
		if (psForm=="ajax_vat") psForm_=1;
		if (psForm=="ajax_cuocnong") psForm_=2;
		if (psForm=="ajax_chuyenkhoan") psForm_=3;
		if (psForm=="ajax_kyquy") psForm_=4;
		if (psForm=="ajax_thuho") psForm_=5;
		if (psForm=="ajax_chicuocnong") psForm_=6;
		if (psForm=="ajax_chikyquy") psForm_=7;
		
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tienmat/phieuhuys/"+psForm
			+ "&" + CesarCode.encode("form") + "=" + psForm_
			+ "&" + CesarCode.encode("tungay") + "=" + psTungay
			+ "&" + CesarCode.encode("denngay") + "=" + psDenngay
			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
			+ "&" + CesarCode.encode("ma_kh") + "=" + psMaKH
			+ "&" + CesarCode.encode("ma_tb") + "=" + psMaTB
			+ "&" + CesarCode.encode("soseri") + "=" + psSeri
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String value_capnhat_seri_phieutra(
		String psKyhoadon,
		String psDsPhieuID,
		String psDsMaKH,
		String psDsSoeriCu,
		String psDsSoeriMoi,
		String psDsSeri,
		String psDsGomHD
    )
    {
    	String a= "begin ? :="+getSysConst("FuncSchema")+"qltn.capnhat_seri_phieutra("
			+"'"+psKyhoadon+"',"
			+"'"+psDsPhieuID+"',"
			+"'"+psDsMaKH+"',"
			+"'"+psDsSoeriCu+"',"
			+"'"+psDsSoeriMoi+"',"	
			+"'"+psDsSeri+"',"
			+"'"+psDsGomHD+"',"
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
    }
	public String value_capnhat_seri_phieutra(
		String psKyhoadon,
		String psDsPhieuID,
		String psDsMaKH,
		String psDsSoeriCu,
		String psDsSoeriMoi,
		String psDsSeri
    )
    {
    	String a= "begin ? :="+getSysConst("FuncSchema")+"qltn.capnhat_seri_phieutra("
			+"'"+psKyhoadon+"',"
			+"'"+psDsPhieuID+"',"
			+"'"+psDsMaKH+"',"
			+"'"+psDsSoeriCu+"',"
			+"'"+psDsSoeriMoi+"',"	
			+"'"+psDsSeri+"',"
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
    }
    public String value_capnhat_seri_phieuthu(
		String psKyhoadon,
		String psbang,
		String psloai,
		String psdsphieu,
		String psdssoseri
    )
    {
    	String a= "begin ? :="+getSysConst("FuncSchema")+"qltn.capnhat_seri_phieuthu("
			+"'"+psKyhoadon+"',"
			+"'"+psbang+"',"
			+"'"+psloai+"',"
			+"'"+psdsphieu+"',"
			+"'"+psdssoseri+"',"		
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
    }    
	public char GetType(String types)
    {
        char return_value='C';
        if (types=="VARCHAR2")
        	return_value='C';
        else if(types=="NUMBER")
        	return_value='N';
        else if (types=="DATE")
        	return_value='D';
        else
           return_value='C';
        return return_value;
    }
    public void doZip(String filename,String zipfilename) throws IOException {
    	
        try {
            FileInputStream fis = new FileInputStream(filename);
            File file=new File(filename);
            byte[] buf = new byte[fis.available()];
            fis.read(buf,0,buf.length);
            
            CRC32 crc = new CRC32();
            ZipOutputStream s = new ZipOutputStream(
                    (OutputStream)new FileOutputStream(zipfilename));
            
            s.setLevel(6);
            
            ZipEntry entry = new ZipEntry(file.getName());
            entry.setSize((long)buf.length);
            crc.reset();
            crc.update(buf);
            entry.setCrc( crc.getValue());
            s.putNextEntry(entry);
            s.write(buf, 0, buf.length);
            s.finish();
            s.close();
            fis.close();
            //file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String export(String psPath,String psKyhoadon)
    {   
        try{
            String sSql="begin ? := admin_v2.QLT_DONNO.laydulieu_cuoiky('"+psKyhoadon+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
            System.out.println(sSql);
            RowSet rows = reqRSet(sSql);
            ResultSetMetaData metas=rows.getMetaData();
            com.svcon.jdbf.JDBField[] fields = new com.svcon.jdbf.JDBField[metas.getColumnCount()];
            for (int i=1;i<=metas.getColumnCount();i++){
                String cname=metas.getColumnName(i);
                char ctype=GetType(metas.getColumnTypeName(i));
                if (cname.length()>9) cname=cname.substring(0,9);
                int clength=metas.getColumnDisplaySize(i);
                if (ctype=='N')
                    clength=20;
                else if (ctype=='D')
                    clength=8;
                else if (clength>=254)
                    clength=253;
                fields[i-1]=new com.svcon.jdbf.JDBField(cname,ctype,clength,0);
            }
            com.svcon.jdbf.DBFWriter writer = new com.svcon.jdbf.DBFWriter(psPath,fields);
            Object[] record=new Object[metas.getColumnCount()];
            while (rows.next()){
                for (int j=1;j<=metas.getColumnCount();j++){
                    if (GetType(metas.getColumnTypeName(j))=='N'){
                        record[j-1]=new Integer(rows.getString(metas.getColumnName(j)));
                    }else{
                        record[j-1]=(rows.getString(metas.getColumnName(j)));
                    }
                }
                writer.addRecord(record);
            }
            writer.close();
            return "Complete";
        }catch (Exception ex){
            return "Complete";

        }
    }
  	public void xCopy(String Host,int Port,String sFile) throws UnknownHostException{
  		try {
  			InetAddress host = InetAddress.getByName(Host);
		  	Socket socket = new Socket(host, Port);
			InputStream in = (InputStream)(new FileInputStream(sFile));
			OutputStream out = new BufferedOutputStream(socket.getOutputStream());
			byte[] buffer = new byte[in.available()];
			while (true) {
			int nBytes = in.read(buffer, 0, in.available());
			if (nBytes < 0)
			break;
				out.write(buffer, 0, nBytes);
			}
			out.flush();
			out.close();
			in.close();
		}catch (IOException ex){
			System.out.println(ex.getMessage());
		}
	}
	public String layds_tuyenthus(
		String psManv
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_thutukh/ajax_tuyenthus"
  			+ "&" + CesarCode.encode("manv") + "=" + psManv;
	}
	public String layds_nhanvientc(
		String psMabc
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_thutukh/ajax_nhanvientcs"
  			+ "&" + CesarCode.encode("mabc") + "=" + psMabc;
	}
	public String layds_khachhangs(
		String psMabc,
		String psManv,
		String psMat
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_thutukh/ajax_layds_khachhangs"
  			+ "&" + CesarCode.encode("mabc") + "=" + psMabc
			+ "&" + CesarCode.encode("manv") + "=" + psManv
			+ "&" + CesarCode.encode("mat") + "=" + psMat;
	}
	public String capnhat_thutu_kh(
		String psChukyno,
		String psDsMA_KH,
		String psDsSTT)
	{
		String s= "begin ? := "
			+getSysConst("FuncSchema")+"qltn_thtoan.capnhat_thutu_kh('"
			+psChukyno+"','"
			+psDsMA_KH+"','"
			+psDsSTT+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")
			+"'); end; ";
		return s;
		
	}
	public String capnhat_hinhthuctt(
    	String psDsHTTT,
    	String psDsUser,
    	String psXoaphieului,
    	String psGachnolui){
    		String users=psDsUser.replace(",","','");
    		String out_ = "declare out_ varchar2(1000); begin begin "
    			+ "	update "+getUserVar("sys_dataschema")+"nguoigachnos set "
    			+ "	HINHTHUCTT_IDS='"+psDsHTTT+"' where nguoigach in ('"
    			+ users +"'); "
    			+ " update "+getUserVar("sys_dataschema")+"users set "
    			+ " gachno_pre="+psGachnolui+",xoaphieu_pre="+psXoaphieului+" where name in ('"
    			+ users + "'); out_ := qltn.log_quyengn('"+psDsHTTT+"','"+psDsUser+"','"+psXoaphieului+"','"+psGachnolui+"','"+						getUserVar("sys_dataschema")+"'); "
    			+ " exception when others then "
    			+ " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "
    			+ " end; ?:=out_; end;";
    		return out_;    		
    }
    public String value_chuyentuyen(
		String psKyhoadon,
        String psdsmakh,
        String psdsmanv_cu,
        String psdsmanv_moi,
        String psdsmat_cu,
        String psdsmat_moi,
        String pschuyentuyen
	)
	{
		return "begin ? :="+getSysConst("FuncSchema")+"qltn.chuyentuyen("+
			"'"+psKyhoadon+"',"+
			"'"+psdsmakh+"',"+
			"'"+psdsmanv_cu+"',"+
			//"'"+psdsmanv_cu+"',"+
			"'"+psdsmat_cu+"',"+
			"'"+psdsmanv_moi+"',"+
			"'"+psdsmat_moi+"',"+
			"'"+pschuyentuyen+"',"+
			"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"') ; end;";
	}
	public String layDauky(String psKyhoadon){
		try{			
			String path=getSysConst("FileUploadDir")+"\\NODK\\";
			String chukysau="";			
			String ccs=getUserVar("sys_dataschema").toLowerCase();
			String ccs1=getUserVar("sys_agentcode");
			chukysau=psKyhoadon;
			//Process p = Runtime.getRuntime().exec("cmd /C mkdir "+path+ccs.replace(".","")+"\\"+chukysau);
			String file=path+ccs.replace(".","")+"\\NODK_"+chukysau+".DBF";
			String zipfile=path+ccs.replace(".","")+"\\NODK_"+chukysau+".ZIP";
			file = file.replace("\\","/").toLowerCase();
			zipfile= zipfile.replace("\\","/").toLowerCase();
			String out_="Tao du lieu dau ky: "+export(file,chukysau);
			doZip(file,zipfile);
			//new FTPupload("190.10.10.87", "cskh", "cskh2010", zipfile , "nodk_"+chukysau+".zip");
			new FTPupload("190.10.10.86:8181", "luattd", "123456", zipfile , ccs1+"/"+chukysau+"/nodk_"+chukysau+".zip");
			//new FTPupload("190.10.10.86:8181", "ductt", "qwer1234", zipfile , ccs.replace(".","")+"\\"+chukysau+"\\NODK_"+chukysau+".ZIP");
			//new FTPupload("190.10.10.86:8181", "ductt", "qwer1234", zipfile , ccs.replace(".","")+"\\"+chukysau+"\\NODK_"+chukysau+".ZIP");
			return "begin ?:='"+out_+"'; end;";
		}catch (Exception ex){
			return ex.getMessage();
	  	}
	}
	public String exportTra(String psPath,String psKyhoadon)
  	{	
  		try{
	  		String sSql="begin ? := admin_v2.QLT_DONNO.laydulieu_tra('"+psKyhoadon+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
	  		System.out.println(sSql);
	  		RowSet rows = reqRSet(sSql);
	  		ResultSetMetaData metas=rows.getMetaData();
	  		com.svcon.jdbf.JDBField[] fields = new com.svcon.jdbf.JDBField[metas.getColumnCount()];
	  		for (int i=1;i<=metas.getColumnCount();i++){
	  			String cname=metas.getColumnName(i);
	  			char ctype=GetType(metas.getColumnTypeName(i));
	  			if (cname.length()>9) cname=cname.substring(0,9);
	  			int clength=metas.getColumnDisplaySize(i);
	  			if (ctype=='N')
	  				clength=20;
	  			else if (ctype=='D')
	  				clength=8;
	  			else if (clength>=254)
	  				clength=253;
	  			fields[i-1]=new com.svcon.jdbf.JDBField(cname,ctype,clength,0);
	  		}
	  		com.svcon.jdbf.DBFWriter writer = new com.svcon.jdbf.DBFWriter(psPath,fields);
	  		Object[] record=new Object[metas.getColumnCount()];
  			while (rows.next()){
  				for (int j=1;j<=metas.getColumnCount();j++){
  					if (GetType(metas.getColumnTypeName(j))=='N'){
  						record[j-1]=new Integer(rows.getString(metas.getColumnName(j)));
  					}else{
  						record[j-1]=(rows.getString(metas.getColumnName(j)));
  					}
  				}
  				writer.addRecord(record);
  			}
	  		writer.close();
	  		return "Complete";
	  	}catch (Exception ex){
			return "Complete";

	  	}
  	}
	public String layDulieuTra(String psKyhoadon){
		try {		
			String path=getSysConst("FileUploadDir")+"\\NODK\\";
			String chukysau="";
			String ccs=getUserVar("sys_dataschema").toLowerCase();
			String ccs1=getUserVar("sys_agentcode");
			
			chukysau=psKyhoadon;
			//Process p = Runtime.getRuntime().exec("cmd /C mkdir "+path+ccs.replace(".","")+"\\"+chukysau);			
			String file=path+ccs.replace(".","")+"\\"+"TRA_"+chukysau+".DBF";
			String zipfile=path+ccs.replace(".","")+"\\"+"TRA_"+chukysau+".ZIP";
			file = file.replace("\\","/").toLowerCase();
			zipfile = zipfile.replace("\\","/").toLowerCase();
			String out_="Tao tra thang "+psKyhoadon+": "+exportTra(file,chukysau);
			doZip(file,zipfile);			
			//new FTPupload("190.10.10.86:8181", "ductt", "qwer1234", zipfile , ccs.replace(".","")+"\\"+chukysau+"\\TRA_"+chukysau+".ZIP");											
			new FTPupload("190.10.10.86:8181", "luattd", "123456", zipfile , ccs1+"/"+chukysau+"/tra_"+chukysau+".zip");											
			String pskytruoc=reqValue("declare a varchar2(100); begin select "+getSysConst("FuncSchema")+"qltn.chukyno_kytruoc('"+psKyhoadon+"',null,null) into a from dual; ? := a; end;");
			file=path+ccs.replace(".","")+"\\"+"TRA_"+pskytruoc+".DBF";			
			zipfile=path+ccs.replace(".","")+"\\"+"TRA_"+pskytruoc+".ZIP";
			file = file.replace("\\","/").toLowerCase();
			zipfile = zipfile.replace("\\","/").toLowerCase();
			out_+=" - Tao tra thang "+pskytruoc+": "+exportTra(file,pskytruoc);			
			doZip(file,zipfile);			
			//String destfile=ccs.replace(".","")+"\\"+pskytruoc+"\\TRA_"+pskytruoc+".ZIP";						
			String destfile="\\"+pskytruoc+"\\TRA_"+pskytruoc+".ZIP";
			destfile = ccs1+destfile.replace("\\","/").toLowerCase();
			new FTPupload("190.10.10.86:8181", "luattd", "123456", zipfile , destfile);
			return "begin ?:='"+out_+"'; end;";
			
		}catch (Exception ex){
	  		return ex.getMessage();
	  	}
	}
	public String value_capnhat_serithuchi
	       (String serithu
           ,String serichi)
	{		
	 	return  "begin ?:="+getSysConst("FuncSchema")+"qltn.capnhat_seri_thuchi('"+serithu+"','"+serichi+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";	 
	}
	public String suppend_timeout(){
		return "begin ?:=1; end;";
	}
	public String value_upd_trangthai_ckn(
		String chukynos,
		String dis_ckns
	)
	{		
		return "begin ?:="+getSysConst("FuncSchema")+"qltn.capnhat_trangthai_chukyno('"+chukynos+"','"+dis_ckns+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
	}
	public String ftpCopy(String script, String ftp, String file, String source){
		try{
			int a;
			Process p = Runtime.getRuntime().exec("cmd /C del "+script);			
			System.out.println(p.exitValue());
			p = Runtime.getRuntime().exec("cmd /C echo.>"+script);
			System.out.println(p.exitValue());
            p = Runtime.getRuntime().exec("cmd /C echo open "+ftp+">>"+script);
			System.out.println(p.exitValue());
            p = Runtime.getRuntime().exec("cmd /C echo ductt>>"+script);
			System.out.println(p.exitValue());
            p = Runtime.getRuntime().exec("cmd /C echo qwer1234>>"+script);
			System.out.println(p.exitValue());
            p = Runtime.getRuntime().exec("cmd /C echo delete "+source+">>"+script);
			System.out.println(p.exitValue());
            p = Runtime.getRuntime().exec("cmd /C echo put "+file+" "+source+">>"+script);
			System.out.println(p.exitValue());
            p = Runtime.getRuntime().exec("cmd /C ftp -s:"+script);
			System.out.println(p.exitValue());
		}catch (Exception ex1){
    		ex1.printStackTrace();
    	}
        return "0";
    }
	/*Hientt cap nhat ngay 07/09/2012*/
	public String exportVNP(
    	String psPath,
    	String psFileName,
    	String psSql,
    	String psScript,
    	String psFtp,
		String psSqlID,
    	String psFtpPath
    ){
		String a = exportDBFext_temp(psSqlID,psPath.replace("$UploadDir$",getSysConst("FileUploadDir")),psFileName, psSql, psScript, psFtp, psFtpPath);	
		return a;							
	}
/*DucTT cap nhat ngay 04/02/2009 
     Muc dich export du lieu cua cac tinh
     Day ra dang dbf vao ftp zip file
 */
    public String exportDBF(
    	String psPath,
    	String psFileName,
    	String psSql,
    	String psScript,
    	String psFtp,
    	String psFtpPath
    ){	
  		try{
	  		RowSet rows = reqRSet(psSql);
	  		ResultSetMetaData metas=rows.getMetaData();
	  		
	  		com.svcon.jdbf.JDBField[] fields = new com.svcon.jdbf.JDBField[metas.getColumnCount()];
	  		for (int i=1;i<=metas.getColumnCount();i++){
	  			String cname=metas.getColumnName(i);
	  			char ctype=GetType(metas.getColumnTypeName(i));
	  			if (cname.length()>10) cname=cname.substring(0,10);
	  			int clength=metas.getColumnDisplaySize(i);
	  			if (ctype=='N')
	  				clength=20;
	  			else if (ctype=='D')
	  				clength=8;
	  			else if (clength>=254)
	  				clength=253;
	  			fields[i-1]=new com.svcon.jdbf.JDBField(cname,ctype,clength,0);
	  		}
			System.out.println(psPath+psFileName+".DBF");
	  		com.svcon.jdbf.DBFWriter writer = new com.svcon.jdbf.DBFWriter( (psPath+psFileName+".DBF").replace("\\","/").toLowerCase(),fields);
	  		Object[] record=new Object[metas.getColumnCount()];
  			while (rows.next()){
  				for (int j=1;j<=metas.getColumnCount();j++){
  					if (GetType(metas.getColumnTypeName(j))=='N'){
  						try{
  							record[j-1]=new Integer(rows.getString(metas.getColumnName(j)));
  						}catch(Exception iex){
  							//record[j-1]=0;
  						}
  					}else{
						try{
							record[j-1]=(rows.getString(metas.getColumnName(j)));
						}catch(Exception cex){
						}
  					}
  				}
  				writer.addRecord(record);
  			}
	  		writer.close();
	  		//Lay lai thong tin ftp
	  		String ftp_="190.10.10.86:8181", user_="ductt", pass_="qwer1234";
	  		doZip( (psPath+psFileName+".DBF").replace("\\","/").toLowerCase() ,(psPath+psFileName+".ZIP").replace("\\","/").toLowerCase() );
			new FTPupload(ftp_, user_, pass_, (psPath+psFileName+".ZIP").replace("\\","/").toLowerCase() , (psFtpPath+"\\"+psFileName+".ZIP").replace("\\","/").toLowerCase() );
	  		return "Complete";
	  	}catch (Exception ex){
	  		return ex.getMessage();
	  	}
  	}
/* 
	Luattd cap nhat ngay 25/02/2009 
	Muc dich export du lieu cua cac tinh
	theo cau truc nhat dinh	 
	- Dang test
 */
	private static final	int ICOLUMN_NUM_THTT = 23;  
	String[] sColumnNameTHTT = {"NGAY_HD","NGAY","HOPDONG_ID","SO_MAY","MA_YC","MA_LOAI_YC","TEN_KH","DIA_CHI","CHUA_VAT","THUE_VAT","CHIET_KHAU","SO_DU_TK","TIEN_KM","PHIEU_ID","NGUOI_GT","MA_USER","MA_BC","DK_DV","H_DV","LOAI_TB","DONVIQL_ID","DICHVU","THUTU"};
	char[] sColumnTypeTHTT = {'D','D','C','C','C','C','C','C','N','N','N','N','N','N','C','C','C','N','N','C','C','C','N'};
	int[] iColumnLengthTHTT = {8,8,20,15,10,10,200,200,10,10,10,10,10,10,40,40,4,10,10,10,10,80,10};	
		
	public String exportDBF3(
    	String psPath,
    	String psFileName,
    	String psSql,
    	String psScript,
    	String psFtp,
    	String psFtpPath
    ){	
  		try{
	  		RowSet rows = reqRSet(psSql);
			com.svcon.jdbf.JDBField[] fields = new com.svcon.jdbf.JDBField[ICOLUMN_NUM_THTT];
			
			for (int i = 0; i<ICOLUMN_NUM_THTT; i++)
			{
	    		fields[i] = new JDBField(sColumnNameTHTT[i] ,sColumnTypeTHTT[i], iColumnLengthTHTT[i], 0 );	    		
	    	}
			
			System.out.println(psPath+psFileName+".DBF");
	  		com.svcon.jdbf.DBFWriter writer = new com.svcon.jdbf.DBFWriter(psPath+psFileName+".DBF",fields);	  		
			Object[] record=new Object[ICOLUMN_NUM_THTT];
  						
			while (rows.next()){	        	
	  				for (int j=0;j<ICOLUMN_NUM_THTT;j++){	  					
	  					if (sColumnTypeTHTT[j]=='C')
	  						record[j]=rows.getString(sColumnNameTHTT[j]);
						if (sColumnTypeTHTT[j]=='D')
	  						record[j]=rows.getDate(sColumnNameTHTT[j]);	
	  					if (sColumnTypeTHTT[j]=='N')
	  						record[j]=new Integer(rows.getInt(sColumnNameTHTT[j]));	
	  								
	  				}
	  				writer.addRecord(record);
	  			}
	  			
	  		writer.close();			
	  		doZip(psPath+psFileName+".DBF",psPath+psFileName+".ZIP");
	  		ftpCopy(psPath+psScript,psFtp,psPath+psFileName+".ZIP",psFtpPath+"\\"+psFileName+".ZIP");
	  		return "Complete !";
	  	}
		catch (Exception ex){
	  		return ex.getMessage();
	  	}
  	}
  	public String exportDBF1(
    	String psPath,
    	String psFileName,
    	String psSqlID,
    	String psScript,
    	String psFtp,
    	String psFtpPath
    ){
    	try{
    		String sql_=reqValue(
    			"declare"+
    			"	re_ varchar2(30000);"+
    			"begin"+
    			"	select "+getSysConst("FuncSchema")+"qltn_baocao.chuanhoa_data_export(sql_text,'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"') into re_ from ccs_common.data_exports where sql_id="+psSqlID+";"+
    			"	?:=re_;"+
    			"end;"
    		);
    		System.out.println("Data Export SQL:"+sql_);
    		String path_=psPath.replace("$UploadDir$",getSysConst("FileUploadDir"));
    		String return_=exportDBFext(
				psSqlID,
    			path_,
    			psFileName.toLowerCase(),sql_, psScript, psFtp, 
    			psFtpPath);
    		return "begin ? :='"+return_.replace("'","''")+"'; end;";
    	}catch (Exception ex){
	  		return ex.getMessage();
	  	}
    }
	public String exportDBF2(
    	String psPath,
    	String psFileName,
    	String psSql,
    	String psScript,
    	String psFtp,		
    	String psFtpPath
    ){
		String a ;
			 a=exportDBFext("21",psPath.replace("$UploadDir$",getSysConst("FileUploadDir")),psFileName, psSql, psScript, psFtp, psFtpPath);	
		return "begin ? :='"+a.replace("'","''")+"'; end;";					
		
	}
	public String exportDBF1_NEW(
    	String psPath,
    	String psFileName,
    	String psSqlID,
    	String psScript,
    	String psFtp,
    	String psFtpPath
    ){
    	try{
    		String sql_=reqValue(
    			"declare"+
    			"	re_ varchar2(30000);"+
    			"begin"+
    			"	select admin_v2.pk_data_exports.chuanhoa_data_export(sql_text,'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"') into re_ from ccs_common.data_exports where sql_id="+psSqlID+";"+
    			"	?:=re_;"+
    			"end;"
    		);
    		System.out.println("Data Export SQL:"+sql_);
    		String path_=psPath.replace("$UploadDir$",getSysConst("FileUploadDir"));
    		String return_=exportDBFext(
				psSqlID,
    			path_,
    			psFileName.toLowerCase(),sql_, psScript, psFtp, 
    			psFtpPath);
    		return "begin ? :='"+return_.replace("'","''")+"'; end;";
    	}catch (Exception ex){
	  		return ex.getMessage();
	  	}
    }
	public String exportDBF_86(
    	String psPath,
    	String psFileName,
    	String psSql,
    	String psScript,
    	String psFtp,
		String psSqlID,
    	String psFtpPath
    ){
		String a = exportDBFext_temp(psSqlID,psPath.replace("$UploadDir$",getSysConst("FileUploadDir")),psFileName, psSql, psScript, psFtp, psFtpPath);	
		return "begin ? :='"+a.replace("'","''")+"'; end;";							
	}
	
	public String exportDBFext(
		String psSqlID,
    	String psPath,
    	String psFileName,
    	String psSql,
    	String psScript,
    	String psFtp,
    	String psFtpPath
    ){	
  		try{
	  		RowSet rows = reqRSet(psSql);
	  		ResultSetMetaData metas=rows.getMetaData();
	  		
	  		com.svcon.jdbf.JDBField[] fields = new com.svcon.jdbf.JDBField[metas.getColumnCount()];
	  		for (int i=1;i<=metas.getColumnCount();i++){
	  			String cname=metas.getColumnName(i);
	  			char ctype=GetType(metas.getColumnTypeName(i));
	  			if (cname.length()>10) cname=cname.substring(0,10);
	  			int clength=metas.getColumnDisplaySize(i);
	  			if (ctype=='N')
	  				clength=20;
	  			else if (ctype=='D')
	  				clength=8;
	  			else if (clength>=254)
	  				clength=253;
	  			fields[i-1]=new com.svcon.jdbf.JDBField(cname,ctype,clength,0);
	  		}
			String f = (psPath+psFileName).replace("\\","/").toLowerCase();
			System.out.println("AAAAAAAAAAAAA:"+f);
	  		com.svcon.jdbf.DBFWriter writer = new com.svcon.jdbf.DBFWriter( f+".dbf" ,fields);			
	  		Object[] record=new Object[metas.getColumnCount()];
  			while (rows.next()){
  				for (int j=1;j<=metas.getColumnCount();j++){
  					if (GetType(metas.getColumnTypeName(j))=='N'){
  						try{
  							record[j-1]=new Integer(rows.getString(metas.getColumnName(j)));
  						}catch(Exception iex){
  							//record[j-1]=0;
  						}
  					}else{
						try{
							record[j-1]=(rows.getString(metas.getColumnName(j)));
						}catch(Exception cex){
						}
  					}
  				}
  				writer.addRecord(record);
  			}
	  		writer.close();
			//System.out.println("AAAAAAAAAAAAAbbbbbbbbbbbbbb:"+f);
	  		//Lay lai thong tin ftp
	  		RowSet rs_ = this.reqRSet("SELECT ftp,username,pass FROM ccs_common.data_exports where sql_id="+psSqlID);
	  		String ftp_="190.10.10.86:8181", user_="ductt", pass_="qwer1234";			
	  		while (rs_.next()){
	  			ftp_ = rs_.getString("ftp");
	  			user_ = rs_.getString("username");
	  			pass_ = rs_.getString("pass");	  			
	  		}
			BASE64Decoder decoder = new BASE64Decoder(); 
			byte[] decodedBytes = decoder.decodeBuffer(pass_);
			String pass_d = new String(decodedBytes);
	  		doZip( f+".dbf" , f+".zip" );	  	
			
			//System.out.println("luattd : " + ftp_ + user_ + pass_d + psPath +psFileName+".ZIP" + psFtpPath+"\\"+psFileName+".ZIP");
	  		new FTPupload(ftp_, user_, pass_d, f+".zip" , (psFtpPath+"/"+psFileName+".zip").replace("\\","/").toLowerCase() );
	  		return "Complete";
	  	}catch (Exception ex){
	  		return ex.getMessage();			
	  	}
  	}
	
	public String exportDBFext_temp(
		String psSqlID,
    	String psPath,
    	String psFileName,
    	String psSql,
    	String psScript,
    	String psFtp,
    	String psFtpPath
    ){	
  		try{
	  		RowSet rows = reqRSet(psSql);
	  		ResultSetMetaData metas=rows.getMetaData();
	  		
	  		com.svcon.jdbf.JDBField[] fields = new com.svcon.jdbf.JDBField[metas.getColumnCount()];
	  		for (int i=1;i<=metas.getColumnCount();i++){
	  			String cname=metas.getColumnName(i);
	  			char ctype=GetType(metas.getColumnTypeName(i));
	  			if (cname.length()>10) cname=cname.substring(0,10);
	  			int clength=metas.getColumnDisplaySize(i);
	  			if (ctype=='N')
	  				clength=20;
	  			else if (ctype=='D')
	  				clength=8;
	  			else if (clength>=254)
	  				clength=253;
	  			fields[i-1]=new com.svcon.jdbf.JDBField(cname,ctype,clength,0);
	  		}
			System.out.println(psPath+psFileName+".DBF");
	  		com.svcon.jdbf.DBFWriter writer = new com.svcon.jdbf.DBFWriter( (psPath+psFileName+".DBF").replace("\\","/").toLowerCase() ,fields);
	  		Object[] record=new Object[metas.getColumnCount()];
  			while (rows.next()){
  				for (int j=1;j<=metas.getColumnCount();j++){
  					if (GetType(metas.getColumnTypeName(j))=='N'){
  						try{
  							record[j-1]=new Integer(rows.getString(metas.getColumnName(j)));
  						}catch(Exception iex){  		
  						}
  					}else{
						try{
							record[j-1]=(rows.getString(metas.getColumnName(j)));
						}catch(Exception cex){
						}
  					}
  				}
  				writer.addRecord(record);
  			}
	  		writer.close();
	  		//Lay lai thong tin ftp
	  		RowSet rs_ = this.reqRSet("SELECT ftp,username,pass FROM ccs_common.data_exports where sql_id="+psSqlID);			
	  		String ftp_="190.10.10.86:8181", user_="luattd", pass_="123456";
	  		while (rs_.next()){
	  			ftp_ = rs_.getString("ftp");
	  			user_ = rs_.getString("username");
	  			pass_ = rs_.getString("pass");	  			
	  		}
			BASE64Decoder decoder = new BASE64Decoder(); 
			byte[] decodedBytes = decoder.decodeBuffer(pass_);
			String pass_d = new String(decodedBytes);
	  		doZip( (psPath+psFileName+".DBF").replace("\\","/").toLowerCase() , (psPath+psFileName+".ZIP").replace("\\","/").toLowerCase() );	  	
			
			//System.out.println("luattd1 : " + ftp_ + user_ + pass_d + psPath +psFileName+".ZIP" + getUserVar("sys_agentcode") +"\\"+psFileName+".ZIP");
	  		//new FTPupload("190.10.10.86:8181", "luattd", "123456", psPath+psFileName+".ZIP" ,getUserVar("sys_agentcode") +"\\"+psFileName+".ZIP");	
			new FTPupload(ftp_, user_, pass_d, (psPath+psFileName+".ZIP").replace("\\","/").toLowerCase() , (getUserVar("sys_agentcode") +"\\"+psFileName+".ZIP").replace("\\","/").toLowerCase() );			
	  		return "Complete";
	  	}catch (Exception ex){
	  		return ex.getMessage();
	  	}
  	}
	public String doc_tracuu_phieu_dc_huy(
		String psForm,
		String psTungay,
		String psDenngay,
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psSeri,
		String psPageNum,
		String psPageRec
	)
	{			
		int psForm_=1;
		if (psForm=="ajax_vat") psForm_=1;
		if (psForm=="ajax_cuocnong") psForm_=2;
		if (psForm=="ajax_chuyenkhoan") psForm_=3;
		if (psForm=="ajax_kyquy") psForm_=4;
		if (psForm=="ajax_thuho") psForm_=5;
		if (psForm=="ajax_chicuocnong") psForm_=6;
		if (psForm=="ajax_chikyquy") psForm_=7;
		
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_dieuchinh/phieuhuys/"+psForm
			+ "&" + CesarCode.encode("form") + "=" + psForm_
			+ "&" + CesarCode.encode("tungay") + "=" + psTungay
			+ "&" + CesarCode.encode("denngay") + "=" + psDenngay
			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
			+ "&" + CesarCode.encode("ma_kh") + "=" + psMaKH
			+ "&" + CesarCode.encode("ma_tb") + "=" + psMaTB
			+ "&" + CesarCode.encode("soseri") + "=" + psSeri
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
}