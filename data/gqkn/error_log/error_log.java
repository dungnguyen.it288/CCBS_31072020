package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class error_log extends NEOProcessEx {
	public String new_error_log(
		String pschema,
		String puserid,
		String puserip,
		String pstart_date,
		String pend_date,
		String perror_type,
		String powner,
		String pcontent,
		String pnote) throws Exception {
			String s ="begin ? :="+pschema+"pkg_helpdesk.new_error_log_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"
				+"'"+pstart_date+"',"
				+"'"+pend_date+"',"
				+"'"+perror_type+"',"
				+"'"+powner+"',"
				+"'"+ConvertFont.UtoD(pcontent)+"',"
				+"'"+ConvertFont.UtoD(pnote)+"'"		
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_error_log(
		String pschema,		
		String puserid,
		String puserip,
		String pid) throws Exception {
			String s ="begin ? :="+pschema+"pkg_helpdesk.del_error_log_func("
				+"'"+pschema+"',"				
				+"'"+puserid+"',"
				+"'"+puserip+"',"
				+"'"+pid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_error_log(
		String pschema,
		String puserid,
		String puserip,
		String pid,
		String pstart_date,
		String pend_date,
		String perror_type,
		String powner,
		String pcontent,
		String pnote) throws Exception {
			String s ="begin ? :="+pschema+"pkg_helpdesk.edit_error_log_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"
				+"'"+pid+"',"
				+"'"+pstart_date+"',"
				+"'"+pend_date+"',"
				+"'"+perror_type+"',"
				+"'"+powner+"',"
				+"'"+ConvertFont.UtoD(pcontent)+"',"
				+"'"+ConvertFont.UtoD(pnote)+"'"				
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
}