package news;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import neo.smartui.process.*;
public class pps extends NEOProcessEx{
	public String layds_file(
			  String ps_agent
			, String ps_date
	  	) throws Exception {
		DateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
		DateFormat formatter2 = new SimpleDateFormat("yyyymmdd");
		Date date = (Date)formatter.parse(ps_date); 
		String format_file=ps_agent+"_"+formatter2.format(date);
		String format_file2=ps_agent+"_20121110_"+formatter2.format(date);
	    String s="<table border=0 cellpadding=0 cellspacing=0 class=secContent width=\"100%;\" >"+
      	"<tr class=\"tableHeadBg\" style=\"font-weight:bold;width:100%\">"+
              "<td align=center>STT</td>"+
              "<td align=center>Filename</td>"+
              "<td align=center>Download</td>"+
         "</tr>";
    	s+= "<tr style='width:100%'>" +
    			"<td align=center>1 </td>" +
    			"<td align=center> "+format_file+"_chua_kh.csv</td>" +
    			"<td align=center> <img src=\"zoho/images/move.gif\" title=\"Download file "+format_file+"_chua_kh.csv\" onClick=\"download('"+format_file+"_chua_kh.csv')\" width=\"16\" height=\"16\"></td>" +
    		"<tr>";
    	s+= "<tr style='width:100%'>" +
    			"<td align=center>2</td>" +
    			"<td align=center> "+format_file+"_da_kh.csv</td>" +
    			"<td align=center> <img src=\"zoho/images/move.gif\" title=\"Download file "+format_file+"_da_kh.csv\" onClick=\"download('"+format_file+"_da_kh.csv')\" width=\"16\" height=\"16\"></td>" +
    		"<tr>";
    	s+= "<tr style='width:100%'>" +
    			"<td align=center>3</td>" +
    			"<td align=center> "+format_file+"_da_psc.csv</td>" +
    			"<td align=center> <img src=\"zoho/images/move.gif\" title=\"Download file "+format_file+"_da_psc.csv\" onClick=\"download('"+format_file+"_da_psc.csv')\" width=\"16\" height=\"16\"></td>" +
    		"<tr>";
    	s+= "<tr style='width:100%'>" +
    			"<td align=center>3</td>" +
    			"<td align=center> "+format_file2+"_da_kh.csv</td>" +
    			"<td align=center> <img src=\"zoho/images/move.gif\" title=\"Download file "+format_file2+"_da_kh.csv\" onClick=\"download('"+format_file2+"_da_kh.csv')\" width=\"16\" height=\"16\"></td>" +
    		"<tr>";
    	s+= "<tr style='width:100%'>" +
    			"<td align=center>3</td>" +
    			"<td align=center> "+format_file2+"_da_psc.csv</td>" +
    			"<td align=center> <img src=\"zoho/images/move.gif\" title=\"Download file "+format_file2+"_da_psc.csv\" onClick=\"download('"+format_file2+"_da_psc.csv')\" width=\"16\" height=\"16\"></td>" +
    		"<tr>";
	    s+="</table>";
	    return s;
	}
	public String layds_file_tt(
			  String ps_agent
			, String ps_date
	  	) throws Exception {
		DateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
		DateFormat formatter2 = new SimpleDateFormat("yyyymm");
		Date date = (Date)formatter.parse(ps_date); 
		String format_file=ps_agent+"_"+formatter2.format(date);
	    String s="<table border=0 cellpadding=0 cellspacing=0 class=secContent width=\"100%;\" >"+
      	"<tr class=\"tableHeadBg\" style=\"font-weight:bold;width:100%\">"+
              "<td align=center>STT</td>"+
              "<td align=center>Filename</td>"+
              "<td align=center>Download</td>"+
         "</tr>";
    	s+= "<tr style='width:100%'>" +
    			"<td align=center>1 </td>" +
    			"<td align=center> "+format_file+"_tt6_tt12.zip</td>" +
    			"<td align=center> <img src=\"zoho/images/move.gif\" title=\"Download file "+format_file+"_tt6_tt12.zip\" onClick=\"download('"+format_file+"_tt6_tt12.zip')\" width=\"16\" height=\"16\"></td>" +
    		"<tr>";
	    s+="</table>";
	    return s;
	}
	/*
	public String layds_file_ftp(
			  String ps_agent
			, String ps_date
	  	) throws ParseException {
		DateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
		DateFormat formatter2 = new SimpleDateFormat("yyyymmdd");
		Date date = (Date)formatter.parse(ps_date); 
		String format_file=ps_agent+"_"+formatter2.format(date);
		FTPClient client = new FTPClient();
		String s="";
		int stt=0;
		try
		{
		    client.connect("190.10.10.86",8181);
		    client.login("pps", "pps");
		    String[] names = client.listNames();
		    for (String name : names) {
		      System.out.println("Name = " + name);
	    }

	    FTPFile[] ftpFiles = client.listFiles();
	    s="<table border=0 cellpadding=0 cellspacing=0 class=secContent width=\"100%;\" >"+
        	"<tr class=\"tableHeadBg\" style=\"font-weight:bold;width:100%\">"+
                "<td align=center>STT</td>"+
                "<td align=center>Filename</td>"+
                "<td align=center>Download</td>"+
           "</tr>";
	    for (FTPFile ftpFile : ftpFiles) {
	    	if (ftpFile.getName().length()>12)
		    	if(ftpFile.getName().substring(0, 12).equals(format_file)){
			    	stt=stt+1;
			    	s+= "<tr style='width:100%'>" +
			    			"<td align=center> "+stt+"</td>" +
			    			"<td align=center> "+ftpFile.getName()+"</td>" +
			    			"<td align=center> <a href=\"download_file.jsp?file_name="+ftpFile.getName()+"\">"+ftpFile.getName()+"</a></td>" +
			    		"<tr>";
		    	}
	    }
	    s+="</table>";
	    client.logout();
	    client.disconnect();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return e.getMessage();
		}
	    return s;
	}*/
	public String getuserid(String s){
		System.out.println(getUserVar("sys_userid"));
		System.out.println(getUserVar("sys_userip"));
		return s;
	}
}
