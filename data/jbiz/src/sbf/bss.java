package sbf;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class bss extends NEOProcessEx {
	public String new_imei(
		String pschema,
		String pso_imei,
		String pstatus,
		String pten_may,
		String pnote,	
		String pagent,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=dms_admin.pkg_bss.new_imei("
				+"'"+pschema+"',"
				+"'"+pso_imei+"',"
				+"'"+pstatus+"',"
				+"'"+ConvertFont.UtoD(pten_may)+"',"
				+"'"+ConvertFont.UtoD(pnote)+"',"			
				+"'"+pagent+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("PPS_DBMS", s);
	}
	public String del_imei(
		String pschema,
		String pso_imei,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=dms_admin.pkg_bss.del_imei("
				+"'"+pschema+"',"
				+"'"+pso_imei+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("PPS_DBMS", s);
	}
	public String edit_imei(
		String pschema,
		String pso_imei,
		String pstatus,
		String pten_may,
		String pnote,	
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=dms_admin.pkg_bss.edit_imei("
				+"'"+pschema+"',"
				+"'"+pso_imei+"',"	
				+"'"+pstatus+"',"
				+"'"+ConvertFont.UtoD(pten_may)+"',"
				+"'"+ConvertFont.UtoD(pnote)+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("PPS_DBMS", s);
	}
	public String capnhat_phanquyen(
			  String ps_schema
			, String ps_idnhom
			, String ps_daisos
			, String ps_userid
			, String ps_note
			, String ps_agentcode
			, String ps_user_cn
			, String ps_ip_cn
	  	) throws Exception {
		 
	     String s=    "begin ? := "+ps_schema+"pkg_bss.capnhat_phanquyen("	
			+"'"+ps_schema+"'"
			+",'"+ps_idnhom+"'"
			+",'"+ps_daisos+"'"                           
			+",'"+ps_userid+"'"         
			+",'"+ConvertFont.UtoD(ps_note)+"'"  
			+",'"+ps_agentcode+"'"
			+",'"+ps_user_cn+"'"		
			+",'"+ps_ip_cn+"'"				
			+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("PPS_DBMS", s);
		}
	public String xoa_phanquyen(
			  String ps_schema
			, String ps_idnhom
			, String ps_user_cn
			, String ps_ip_cn
	  	) throws Exception {
		 
	     String s=    "begin ? := "+ps_schema+"pkg_bss.xoa_phanquyen("	
			+"'"+ps_schema+"'"
			+",'"+ps_idnhom+"'"
			+",'"+ps_user_cn+"'"		
			+",'"+ps_ip_cn+"'"				
			+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("PPS_DBMS", s);
		}
}