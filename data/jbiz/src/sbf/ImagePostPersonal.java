package sbf;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import cosaldap.Base64.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;

public class ImagePostPersonal extends NEOProcessEx 
{	
	public NEOCommonData value_reUpload_tt(String psUploadID, String pssotbs, String pstentbs, String pssogts, String psngaysinhs, String psngaycaps,String psnoicaps, String psdiachis,
		String psghichus
	)
	{	
		String xs_ = "";	
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PKG_UPLOAD.reupload_dv_tt_file('"+psUploadID+"','"+pssotbs+"','"+pstentbs+"','"+pssogts+"','"+psngaysinhs+"','"+psngaycaps+"','"+psnoicaps+"','"+psdiachis+"','"+psghichus+"');";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);	
	}
	public NEOCommonData layTTThueBaoPPS(String so_tb)
	{		
		/*EnvInfo ei_ = getDataParameter().getEnvInfo();
		HttpServletRequest httpRequest_ = ei_.getServletRequest();		
		String clientIP_ = httpRequest_.getRemoteHost();
		System.out.println("############################## clientIP_: " + clientIP_);*/

		String xs_ = "";
		xs_ = xs_ + " begin ";
		//xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.get_info_subscribers(?); ";
		//xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION_v2.get_info_subscribers(?); ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_API.get_info_subscribers(?); ";
		xs_ = xs_ + " end; ";	
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		ei_.bindParameter(2,so_tb);
		
		RowSet rs_ = null;
		try{
			rs_ = reqRSet(ei_);			
			
		} catch (Exception ex) {
			ex.printStackTrace();
			//System.out.println("#################dinhductoan###########dinhductoan"+rs_.getMaxRows());
		}
		
		return new NEOCommonData(rs_);
	} 	
	 //Upload nhieu file anh
	public String layds_file_multi(String psid){
	return "/main?"+CesarCode.encode("configFile")+"=prepaid/upload_cmt_hc/ajax_ds_file"
			+"&"+CesarCode.encode("id_")+"="+psid
			;
  }
  public NEOCommonData value_insert_image(String psuploadid)
	{	
		String xs_ = "";	
		try{	
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := PREPAID.PKG_UPLOAD.insert_file_images('"+psuploadid+"'); ";	
		xs_ = xs_ + " end; "; 
		}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);			
	}	
  public NEOCommonData value_del_uploads(String psid)
	{	
		String xs_ = "";	
		try{	
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := PREPAID.PKG_UPLOAD.xoa_file_images('"+psid+"'); ";	
		xs_ = xs_ + " end; "; 
		}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);			
	}
	
	public NEOCommonData saveDataTB(String _msisdn,String _tenkhachhang,String _ngaysinh,String _sochungminh,String _hochieu,String _diachi,String _congty,String _nghenghiep,String _gioitinh,String _nuoc,String _email, String _noicap, String _loai_kh, String _loai_gt, String _ghichu, String _ngaycap,String _diachicty,String _maxacthuc,String _visa,String _visahh)	
	{

		String xs_ = "";	
		String _userid = getUserVar("userID");
		String sys_agentcode_ = getUserVar("sys_agentcode");
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PREPAID_FUNCTION.add_subscibers('"+_msisdn+"','"+ _tenkhachhang+"', '"+_ngaysinh+"','"+ _sochungminh+"','"+ _hochieu+"','"+_diachi+"','"+ _congty+"','"+ _nghenghiep+"','"+_gioitinh+"','"+_nuoc+"','"+_email+"','"+_loai_gt+"','"+_noicap+"','"+_loai_kh+"','"+_ghichu+"','"+_ngaycap+"','"+_diachicty+"','"+_visa+"','"+_visahh+"','"+_maxacthuc+"','"+_userid+"','"+sys_agentcode_+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	
	//03062017: ND49
	public NEOCommonData updateDataTB(String _msisdn,String _tenkhachhang,String _ngaysinh,String _sochungminh,String _hochieu,String _diachi,String _congty,String _nghenghiep,String _gioitinh,String _nuoc,String _email, String _noicap, String _loai_kh, String _loai_gt, String _ghichu, String _ngaycap,String _diachicty,String _maxacthuc,String _visa,String _visahh,String psdoituongsd, String psdiachipgd, String psdienthoaipgd, String psghichuanh)	
	{

		String xs_ = "";	
		String _userid = getUserVar("userID");
		String sys_agentcode_ = getUserVar("sys_agentcode");
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PREPAID_API.add_subscibers('"+_msisdn+"','"+ _tenkhachhang+"', '"+_ngaysinh+"','"+ _sochungminh+"','"+ _hochieu+"','"+_diachi+"','"+ _congty+"','"+ _nghenghiep+"','"+_gioitinh+"','"+_nuoc+"','"+_email+"','"+_loai_gt+"','"+_noicap+"','"+_loai_kh+"','"+_ghichu+"','"+_ngaycap+"','"+_diachicty+"','"+_visa+"','"+_visahh+"','"+_maxacthuc+"','"+_userid+"','"+sys_agentcode_+"','"+psdoituongsd+"','"+psdiachipgd+"','"+psdienthoaipgd+"','"+psghichuanh+"'); ";	
			xs_ = xs_ + " end; "; 		
			
			System.out.println(xs_);
			}
			catch (Exception ex) {
			ex.printStackTrace();
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	
	//15062017: ND49
	public NEOCommonData updateDataTB_dn_canhan(String _msisdn,String _tenkhachhang,String _ngaysinh,String _sochungminh,String _hochieu,String _diachi,String _congty,String _nghenghiep,String _gioitinh,String _nuoc,String _email, String _noicap, String _loai_kh, String _loai_gt, String _ghichu, String _ngaycap,String _diachicty,String _maxacthuc,String _visa,String _visahh,String psdoituongsd, String psdiachipgd, String psdienthoaipgd, String psghichuanh, String psmakh, String psmanguoidaidien)	
	{

		String xs_ = "";	
		String _userid = getUserVar("userID");
		String sys_agentcode_ = getUserVar("sys_agentcode");
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PREPAID_API_KHDN.update_subscibers_dn_canhan('"+_msisdn+"','"+ _tenkhachhang+"', '"+_ngaysinh+"','"+ _sochungminh+"','"+ _hochieu+"','"+_diachi+"','"+ _congty+"','"+ _nghenghiep+"','"+_gioitinh+"','"+_nuoc+"','"+_email+"','"+_loai_gt+"','"+_noicap+"','"+_loai_kh+"','"+_ghichu+"','"+_ngaycap+"','"+_diachicty+"','"+_visa+"','"+_visahh+"','"+_maxacthuc+"','"+_userid+"','"+sys_agentcode_+"','"+psdoituongsd+"','"+psdiachipgd+"','"+psdienthoaipgd+"','"+psghichuanh+"','"+psmakh+"','"+psmanguoidaidien+"'); ";	
			xs_ = xs_ + " end; "; 		
			
			System.out.println(xs_);
			}
			catch (Exception ex) {
			ex.printStackTrace();
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	
	/* //OLD
	public NEOCommonData saveDataTB_new(String _msisdn,String _tenkhachhang,String _ngaysinh,String _sochungminh,String _hochieu,String _diachi,String _congty,String _nghenghiep,String _gioitinh,String _nuoc,String _email, String _noicap, String _loai_kh, String _loai_gt, String _ghichu, String _ngaycap,String _diachicty,String _maxacthuc,String _visa,String _visahh,String _loaikt)	
	{

		String xs_ = "";	
		String _userid = getUserVar("userID");
		String sys_agentcode_ = getUserVar("sys_agentcode");
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.pkg_prepaid_new.add_subscibers('"+_msisdn+"','"+ _tenkhachhang+"', '"+_ngaysinh+"','"+ _sochungminh+"','"+ _hochieu+"','"+_diachi+"','"+ _congty+"','"+ _nghenghiep+"','"+_gioitinh+"','"+_nuoc+"','"+_email+"','"+_loai_gt+"','"+_noicap+"','"+_loai_kh+"','"+_ghichu+"','"+_ngaycap+"','"+_diachicty+"','"+_visa+"','"+_visahh+"','"+_maxacthuc+"','"+_userid+"','"+sys_agentcode_+"','"+_loaikt+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	*/
	///NEW_SPS
	public NEOCommonData saveDataTB_new(String _msisdn,String _tenkhachhang,String _ngaysinh,String _sochungminh,String _hochieu,String _diachi,String _congty,String _nghenghiep,String _gioitinh,String _nuoc,String _email, String _noicap, String _loai_kh, String _loai_gt, String _ghichu, String _ngaycap,String _diachicty,String _maxacthuc,String _visa,String _visahh,String _loaikt)	
	{

		String xs_ = "";	
		String _userid = getUserVar("userID");
		String sys_agentcode_ = getUserVar("sys_agentcode");
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := prepaid.prepaid_dktt.dangky_tt_ccbs('"+_msisdn+"','"+ _tenkhachhang+"', '"+_ngaysinh+"','"+ _sochungminh+"','"+ _hochieu+"','"+_diachi+"','"+ _congty+"','"+ _nghenghiep+"','"+_gioitinh+"','"+_nuoc+"','"+_email+"','"+_loai_gt+"','"+_noicap+"','"+_loai_kh+"','"+_ghichu+"','"+_ngaycap+"','"+_diachicty+"','"+_visa+"','"+_visahh+"','"+_maxacthuc+"','"+_userid+"','"+sys_agentcode_+"','"+_loaikt+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	
	///NEW_SPS 03062017: ND49
	public NEOCommonData saveDataTB_Personal(String _msisdn,String _tenkhachhang,String _ngaysinh,String _sochungminh,String _hochieu,String _diachi,String _congty,String _nghenghiep,String _gioitinh,String _nuoc,String _email, String _noicap, String _loai_kh, String _loai_gt, String _ghichu, String _ngaycap,String _diachicty,String _maxacthuc,String _visa,String _visahh,String _loaikt,String psdoituongsd, String psdiachipgd, String psdienthoaipgd, String psghichuanh)	
	{

		String xs_ = "";	
		String _userid = getUserVar("userID");
		String sys_agentcode_ = getUserVar("sys_agentcode");
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := prepaid.prepaid_api.dangky_tt_ccbs('"+_msisdn+"','"+ _tenkhachhang+"', '"+_ngaysinh+"','"+ _sochungminh+"','"+ _hochieu+"','"+_diachi+"','"+ _congty+"','"+ _nghenghiep+"','"+_gioitinh+"','"+_nuoc+"','"+_email+"','"+_loai_gt+"','"+_noicap+"','"+_loai_kh+"','"+_ghichu+"','"+_ngaycap+"','"+_diachicty+"','"+_visa+"','"+_visahh+"','"+_maxacthuc+"','"+_userid+"','"+sys_agentcode_+"','"+_loaikt+"','"+psdoituongsd+"','"+psdiachipgd+"','"+psdienthoaipgd+"','"+psghichuanh+"'); ";	
			xs_ = xs_ + " end; "; 		
			
			System.out.println(xs_);
			
			}
			catch (Exception ex) {
			ex.printStackTrace();
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	
	///NEW_SPS 15062017: ND49: ham cho KH ca nhan cua doanh nghiep
	public NEOCommonData saveDataTB_DN_Personal(String _msisdn,String _tenkhachhang,String _ngaysinh,String _sochungminh,String _hochieu,String _diachi,String _congty,String _nghenghiep,String _gioitinh,String _nuoc,String _email, String _noicap, String _loai_kh, String _loai_gt, String _ghichu, String _ngaycap,String _diachicty,String _maxacthuc,String _visa,String _visahh,String _loaikt,String psdoituongsd, String psdiachipgd, String psdienthoaipgd, String psghichuanh, String ma_kh, String ma_nguoidaidien)	
	{

		String xs_ = "";	
		String _userid = getUserVar("userID");
		String sys_agentcode_ = getUserVar("sys_agentcode");
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := prepaid.prepaid_api_khdn.dangky_tt_ccbs_dn_canhan('"+_msisdn+"','"+ _tenkhachhang+"', '"+_ngaysinh+"','"+ _sochungminh+"','"+ _hochieu+"','"+_diachi+"','"+ _congty+"','"+ _nghenghiep+"','"+_gioitinh+"','"+_nuoc+"','"+_email+"','"+_loai_gt+"','"+_noicap+"','"+_loai_kh+"','"+_ghichu+"','"+_ngaycap+"','"+_diachicty+"','"+_visa+"','"+_visahh+"','"+_maxacthuc+"','"+_userid+"','"+sys_agentcode_+"','"+_loaikt+"','"+psdoituongsd+"','"+psdiachipgd+"','"+psdienthoaipgd+"','"+psghichuanh+"','"+ma_kh+"','"+ma_nguoidaidien+"'); ";	
			xs_ = xs_ + " end; "; 		
			
			System.out.println(xs_);
			
			}
			catch (Exception ex) {
			ex.printStackTrace();
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	
	public NEOCommonData saveDataAgent(String _madaily,String _daily,String _diachi,String _dienthoai,String _masothue,String _thanhpho,String _fax, String _tomtat,String _ngayStart,String _ngayExpire,String _email,String _tendangnhap,String _matkhau,String _userip, String _pin)
	{
		String _userid = getUserVar("userID");
		//System.out.println("###############userid###########"+_userid);
		String xs_ = "";
		//System.out.println("###############userid###########"+_madaily);
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PREPAID_FUNCTION.add_agents('"+_madaily+"','"+_daily+"','"+_diachi+"', '"+_dienthoai+"','"+_masothue+"','"+_thanhpho+"','"+_fax+"','"+_tomtat+"','"+_ngayStart+"','"+_ngayExpire+"', '"+_email+"','"+_tendangnhap+"','"+_matkhau+"','"+_userid+"','"+_userip+"','"+_pin+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();
			//System.out.println("#################dinhductoan###########dinhductoan"+rs_.getMaxRows());
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);		
        ei_.setDataSrc("PPS_DBMS");	
		//System.out.println("###############userid###########"+ei_.toString());
		return new NEOCommonData(ei_);
	}
	
	
	
	
	public NEOCommonData saveDataAgentMember(String _isnew,String _madaily,String _dienthoai,String _fax,String _email,String _tendangnhap,String _matkhau,String _trangthai, String _thanhpho,String _diachi,String _userip,String _phuongthuc,String _ghichu)
	{
		String _userid = getUserVar("userID");
		
		if (_userid != null) {
		//System.out.println("###############userid###########"+_userid);
		String xs_ = "";
		//System.out.println("###############userid###########"+_madaily);
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PREPAID_FUNCTION.add_agent_members('"+_isnew+"','"+_dienthoai+"','"+_fax+"', '"+_email+"','"+_tendangnhap+"','"+_matkhau+"','"+_trangthai+"','"+_thanhpho+"','"+_diachi+"','"+_madaily+"','"+_userid+"','"+_userip+"','"+_phuongthuc+"','"+_ghichu+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();
			//System.out.println("#################dinhductoan###########dinhductoan"+rs_.getMaxRows());
		}		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);		
        ei_.setDataSrc("PPS_DBMS");	
		//System.out.println("###############userid###########"+ei_.toString());
		return new NEOCommonData(ei_);
		} else {
			return new NEOCommonData("-1");
		}
	}
	
	// Cac ham lien quan den form agent_member  layTTAgent
	public NEOCommonData layTTAgent(String agentid)
	{
	//	System.out.println("#################dinhductoan###########dinhductoan");
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.getAgentInfo(?); ";
		xs_ = xs_ + " end; ";		

		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		ei_.bindParameter(2,agentid);
		
		RowSet rs_ = null;
		
		System.out.println("#################dinhductoan###########dinhductoan");
		try{
			rs_ = reqRSet(ei_);			
			
		} catch (Exception ex) {
			ex.printStackTrace();			
		}		
		return new NEOCommonData(rs_);
	} 	
	public NEOCommonData layTTMember(String agentid)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.getMember(?); ";
		xs_ = xs_ + " end; ";		
		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		ei_.bindParameter(2,agentid);
		
		RowSet rs_ = null;
		try{
			rs_ = reqRSet(ei_);			
			
		} catch (Exception ex) {
			ex.printStackTrace();
			//System.out.println("#################dinhductoan###########dinhductoan"+rs_.getMaxRows());
		}		
		return new NEOCommonData(rs_);
	} 
	public String docDSAgent(String _tentruycap, String _tendaily) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=prepaid/mod_dsagent";
		url_ = url_ + "&"+CesarCode.encode("tentruycap_")+"="+_tentruycap;		
		url_ = url_ + "&"+CesarCode.encode("tendaily_")+"="+_tendaily;
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	//kiem tra trang thai cua Thue bao
	public NEOCommonData check_TB(String _msisdn)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.check_subscriber_kind('"+_msisdn+"'); ";
		xs_ = xs_ + " end; ";		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	public String docDSLSTB(String _so_tb) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=pttb/prepaid/mod_lstb";
		url_ = url_ + "&"+CesarCode.encode("so_tb_")+"="+_so_tb;				
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	public String docDSLSTTTB(String _so_tb) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=pttb/prepaid/km_hs_sv/mod_lstttb";
		url_ = url_ + "&"+CesarCode.encode("so_tb_")+"="+_so_tb;			
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	
	public NEOCommonData layTTHSSV(String sotb_)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PKG_KHUYENMAI.get_info_dv_hs_sv('"+sotb_+"');";
		xs_ = xs_ + " end; ";		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}	
	public NEOCommonData role_user()
	{		
		String _userid = getUserVar("userID");
		String sys_agentcode_ = getUserVar("sys_agentcode");
		//String _sys_userip = getUserVar("userIp");
	//	String clientIP_ = getRequest().getClientIp();
		//System.out.println("###################### "+ _sys_userip);
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.check_role_user('"+_userid+"','"+sys_agentcode_+"'); ";
		xs_ = xs_ + " end; ";	
	//	bindSqlParam("2",_userid);			
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);		
	}	
	public NEOCommonData LockAgent(String _agentid, String _status,String _userip)
	{		
		String _userid = getUserVar("userID");
		
		//String _sys_userip = getUserVar("userIp");
	//	String clientIP_ = getRequest().getClientIp();
		//System.out.println("###################### "+ _sys_userip);
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.LockAgent('"+_agentid+"','"+_status+"','"+_userid+"','"+_userip+"'); ";
		xs_ = xs_ + " end; ";	
	//	bindSqlParam("2",_userid);			
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);		
	}	
	public NEOCommonData getTTHD(String _agentid)
	{		
		String _userid = getUserVar("userID");
		String _userip = getUserVar("userID");
		//String _sys_userip = getUserVar("userIp");
	//	String clientIP_ = getRequest().getClientIp();
		//System.out.println("###################### "+ _sys_userip);
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.getagentinfo('"+_agentid+"'); ";
		xs_ = xs_ + " end; ";	
	//	bindSqlParam("2",_userid);			
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);		
	}	
	public NEOCommonData doimk_ngdung(String agentid, String pass,String _userip)
	{		
		String _userid = getUserVar("userID");
		
		//String _sys_userip = getUserVar("userIp");
	//	String clientIP_ = getRequest().getClientIp();
		
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.doimk_ngdung('"+agentid+"','"+pass+"','"+_userid+"','"+_userip+"'); ";
		xs_ = xs_ + " end; ";	
		/*bindSqlParam("1",agentid_);			
		bindSqlParam("2",pass);
		bindSqlParam("3",_userid);
		bindSqlParam("4",_userip);*/
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);	
	}
public String layTTMembers( String _madaily) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=prepaid/mod_member";
		url_ = url_ + "&"+CesarCode.encode("madaily_")+"="+_madaily;			
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}	
	
	// cac ham kien quan den form nhap hop dong 
	public NEOCommonData KTAgents(String so_tb)
	{
		String _userid = getUserVar("userID");		
		//String _sys_userip = getUserVar("userIp");
	//	String clientIP_ = getRequest().getClientIp();
		//System.out.println("###################### "+ _sys_userip);
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.check_exist_tb('"+so_tb+"'); ";
		xs_ = xs_ + " end; ";	
	//	bindSqlParam("2",_userid);			
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);
	}
	
	public NEOCommonData saveData151Agent(String _madaily,String _daily,String _diachi,String _dienthoai,String _thanhpho,String _fax, String _tomtat,String _email,String _userip)
	{
		String _userid = getUserVar("userID");
		//System.out.println("###############userid###########"+_userid);
		String xs_ = "";
		//System.out.println("###############userid###########"+_madaily);
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PREPAID_FUNCTION.add_agents_151('"+_madaily+"','"+_daily+"','"+_diachi+"', '"+_dienthoai+"','"+_thanhpho+"','"+_fax+"','"+_tomtat+"', '"+_email+"','"+_userid+"','"+_userip+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();
			//System.out.println("#################dinhductoan###########dinhductoan"+rs_.getMaxRows());
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);		
        ei_.setDataSrc("PPS_DBMS");	
		//System.out.println("###############userid###########"+ei_.toString());
		return new NEOCommonData(ei_);
	}
	// reset mat khau cho khach hang
	public NEOCommonData DoiMK(String so_tb, String kind)
	{
		String _userid = getUserVar("userID");		
		//String _sys_userip = getUserVar("userIp");
	//	String clientIP_ = getRequest().getClientIp();
		//System.out.println("###################### "+ _sys_userip);
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.resetMK('"+_userid+"','"+so_tb+"','"+kind+"'); ";
		xs_ = xs_ + " end; ";	
	//	bindSqlParam("2",_userid);			
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);
	}
	public NEOCommonData check_TB_status(String _msisdn)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.check_sub_status_ccbs('"+_msisdn+"'); ";
		xs_ = xs_ + " end; ";		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	
	
	public String doimk_ngdung(String mkcu_,String mkmoi_,String mkmoi2_)
	{
		String userid_ = getUserVar("userID");

		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := neo.PKG_admin.doimatkhau_nguoidung('"+userid_
					+"', '" +userid_+"','"+ mkcu_
					+"','"  +mkmoi_+"','"+mkmoi2_+"');";
		xs_ = xs_ + " end; ";
		return xs_;
	}	
	public NEOCommonData updateDataAgent(String _madaily,String _daily,String _diachi,String _dienthoai,String _masothue,String _thanhpho,String _fax, String _tomtat,String _ngayStart,String _ngayExpire,String _email,String _tendangnhap,String _matkhau,String _userip, String _pin)
	{
		String _userid = getUserVar("userID");
		//System.out.println("###############userid###########"+_userid);
		String xs_ = "";
		//System.out.println("###############userid###########"+_madaily);
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PREPAID_FUNCTION.updateDataAgent('"+_madaily+"','"+_daily+"','"+_diachi+"', '"+_dienthoai+"','"+_masothue+"','"+_thanhpho+"','"+_fax+"','"+_tomtat+"','"+_ngayStart+"','"+_ngayExpire+"', '"+_email+"','"+_tendangnhap+"','"+_matkhau+"','"+_userid+"','"+_userip+"','"+_pin+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();
			//System.out.println("#################dinhductoan###########dinhductoan"+rs_.getMaxRows());
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);		
        ei_.setDataSrc("PPS_DBMS");	
		//System.out.println("###############userid###########"+ei_.toString());
		return new NEOCommonData(ei_);
	}
	public NEOCommonData check_TB_daily(String _msisdn)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.check_exist_user('"+_msisdn+"'); ";
		xs_ = xs_ + " end; ";		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	
	// Function cho phan dang ky goi hoc sinh - sinh vien
	public String layDSTruongTheoLoaiKH(String userInput) {
		return "/main?"+CesarCode.encode("configFile")+"=PTTB/prepaid/km_hs_sv/ajax_truong"
			+"&"+CesarCode.encode("loaikh_id")+"="+userInput
        ;
  }  
  
  public String layDSTinhTrangTheoLoaiKH(String userInput) {
		return "/main?"+CesarCode.encode("configFile")+"=PTTB/prepaid/km_hs_sv/ajax_tinhtrang"
			+"&"+CesarCode.encode("tinhtrang_id")+"="+userInput
        ;
}
		
public NEOCommonData check_TB_KM(String _msisdn)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PKG_KHUYENMAI.check_subscriber('"+_msisdn+"'); ";
		xs_ = xs_ + " end; ";		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}		
	
 public NEOCommonData laySoSim(String sotb_)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PKG_KHUYENMAI.laytt_sosim('"+sotb_+"'); ";
		xs_ = xs_ + " end; ";		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}	

public NEOCommonData laySoSim1(String sotb_)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PKG_KHUYENMAI.laytt_sosim1('"+sotb_+"'); ";
		xs_ = xs_ + " end; ";		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}		
	
		
	public NEOCommonData saveDataKM(String _sotb,String _sosim,String _tenkhachhang,String _ngaysinh,String _loaigt,String _sogt,String _ngaycap,String _noicap,String _loaikh,String _truong,String _tinhtrang,String _sothe, String _lop, String _capngay,String _quanid, String _phuongid, String _phoid,String _sonha,String _diachi,String _ghichu,String _baoho,String _khoahoc,String _kieuth, String mxt)	
	{
	
		String xs_ = "";	
		String _userid = getUserVar("userID");
		String sys_agentcode_ = getUserVar("sys_agentcode");
	
	//kiem tra  so_gt da duoc dang ky so thue bao ?
		
			xs_ = " begin "
				+ " 	? := PREPAID.PKG_KHUYENMAI.lay_sotb_theo_sogt('"+ _sogt+"'); "
				+ "   commit; "
				+ " end; " ;
		
			String rs_ = null;
			try{
				rs_ = reqValue("PPS_DBMS",xs_);
			} catch (Exception ex) {
				ex.printStackTrace();
				rs_="0";
			}	
		if (_kieuth=="1") {		
			if (!rs_.equals("0")) {
				return new NEOCommonData(rs_);			
			}
		}	
	// end			
		try{		
			xs_ =  " begin ";
			xs_ = xs_ + " 	? := PREPAID.PKG_KHUYENMAI.dangky('"+_sotb+"','"+ _sosim+"','"+_tenkhachhang+"','"+ _ngaysinh+"','"+ _loaigt+"','"+_sogt+"','"+ _ngaycap+"','"+ _noicap+"','"+_loaikh+"','"+_truong+"','"+_tinhtrang+"','"+_sothe+"','"+_lop+"','"+_capngay+"','"+_quanid+"','"+_phuongid+"','"+_phoid+"','"+_sonha+"','"+_diachi+"','"+_userid+"','"+sys_agentcode_+"','"+_ghichu+"','"+_baoho+"','"+_khoahoc+"','"+mxt+"','"+_kieuth+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	
	public NEOCommonData DelDataKM(String _sotb)	
	{
		String xs_ = "";	
		String _userid = getUserVar("userID");
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PKG_KHUYENMAI.huygoi('"+_sotb+"','"+_userid+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	
	public NEOCommonData layTTThueBaoPPS_KM(String so_tb)
	{		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PKG_KHUYENMAI.get_info_dv_hs_sv(?); ";
		xs_ = xs_ + " end; ";	
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		ei_.bindParameter(2,so_tb);
		
		RowSet rs_ = null;
		try{
			rs_ = reqRSet(ei_);			
			
		} catch (Exception ex) {
			ex.printStackTrace();			
		}
		return new NEOCommonData(rs_);
	} 	
	
	public String docDSLSTBKM(String _so_tb) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=pttb/prepaid/km_hs_sv/mod_lstb";
		url_ = url_ + "&"+CesarCode.encode("so_tb_")+"="+_so_tb;				
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	
	public String layDSTruong(String psloaikh,String psMaSo,String psTen
						   ) {
    
	return "/main?"+CesarCode.encode("configFile")+"=pttb/prepaid/km_hs_sv/dstruong/frmdstruong_ajax"
		+"&"+CesarCode.encode("loaikh")+"="+psloaikh
		+"&"+CesarCode.encode("maso")+"="+psMaSo
		+"&"+CesarCode.encode("ten")+"="+psTen	
		;		
    } 
	public String layDSGiayToTheoLoaiKH(String userInput) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/prepaid/ajax_loaigt"
			+"&"+CesarCode.encode("loaikh_id")+"="+userInput
        ;
  }

 
	public NEOCommonData value_dknhantien(			
			String psDaily,
			String psEload
	){
	
	String _userid = getUserVar("userID");
        String xs_ = "begin ?:=prepaid.pps.dk_nhantien('"+        	
			psDaily+"','"+
			"WEB"+"','"+
			"DKNT_"+psEload+"','"+_userid+"'); end;";
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
    } 
	
	public NEOCommonData value_dkmaxacthuc(			
			String psuserid,
			String pssotb
	){
	
	 String xs_ = "begin ?:=prepaid.pps.dangky_xacthuc('"+        	
			psuserid+"','"+
			"1414"+"','"+
			"MXT "+pssotb+"'); end;";
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}	
	
	
	/* Thong tin thue bao tra truoc theo doanh nghiep  */
	public NEOCommonData check_TB_DN(String _msisdn)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.check_subscriber_kind_dn('"+_msisdn+"'); ";
		xs_ = xs_ + " end; ";		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	
	public NEOCommonData layTTThueBaoPPS_DN(String so_tb)
	{		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.get_info_subscribers_dn('"+so_tb+"'); ";
		xs_ = xs_ + " end; ";

		System.out.println(xs_);	
				
		RowSet rs_ = null;
		try{
			rs_ = reqRSet("PPS_DBMS", xs_);			
			
		} catch (Exception ex) {
			ex.printStackTrace();
			//System.out.println("#################dinhductoan###########dinhductoan"+rs_.getMaxRows());
		}
		
		return new NEOCommonData(rs_);
	} 	
	
	/* //OLD
	public NEOCommonData saveDataTB_DN(String _so_tb,String _msisdn,String _tenkhachhang,String _ngaysinh,String _sochungminh,String _hochieu,String _diachi,String _congty,String _nghenghiep,String _gioitinh,String _nuoc,String _email,String _loaigt,String _noicap,String _loaikh,String _ghichu,String _ngaycap,String _diachicty,String _sogiaygt,String _ngaycapggt,String _chucvu,String _nguoiky,String _sogiaykd,String _ngaycapgiaykd,String _donvi_kd,String _maxacthuc)	
	{
		
		String xs_ = "";	
		String _userid = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PREPAID_FUNCTION.add_subscibers_dn_new('"+_so_tb+"','"+_msisdn+"','"+ _tenkhachhang+"', '"+_ngaysinh+"','"+ _sochungminh+"','"+ _hochieu+"','"+_diachi+"','"+ _congty+"','"+ _nghenghiep+"','"+_gioitinh+"','"+_nuoc+"','"+_email+"','"+_loaigt+"','"+_noicap+"','"+_loaikh+"',"+_ghichu+",'"+_ngaycap+"','"+_diachicty+"','"+_sogiaygt+"','"+_ngaycapggt+"','"+_chucvu+"','"+_nguoiky+"','"+_sogiaykd+"','"+_ngaycapgiaykd+"','"+_donvi_kd+"','"+_maxacthuc+"','"+_userid+"','"+matinh_ngdung_+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();
			//System.out.println("#################dinhductoan###########dinhductoan"+rs_.getMaxRows());
			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	*/
	///NEW_SPS
	public NEOCommonData saveDataTB_DN(String _so_tb,String _msisdn,String _tenkhachhang,String _ngaysinh,String _sochungminh,String _hochieu,String _diachi,String _congty,String _nghenghiep,String _gioitinh,String _nuoc,String _email,String _loaigt,String _noicap,String _loaikh,String _ghichu,String _ngaycap,String _diachicty,String _sogiaygt,String _ngaycapggt,String _chucvu,String _nguoiky,String _sogiaykd,String _ngaycapgiaykd,String _donvi_kd,String _maxacthuc)	
	{
		
		String xs_ = "";	
		String _userid = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.dangky_tt_dn.add_subscibers_dn_new('"+_so_tb+"','"+_msisdn+"','"+ _tenkhachhang+"', '"+_ngaysinh+"','"+ _sochungminh+"','"+ _hochieu+"','"+_diachi+"','"+ _congty+"','"+ _nghenghiep+"','"+_gioitinh+"','"+_nuoc+"','"+_email+"','"+_loaigt+"','"+_noicap+"','"+_loaikh+"',"+_ghichu+",'"+_ngaycap+"','"+_diachicty+"','"+_sogiaygt+"','"+_ngaycapggt+"','"+_chucvu+"','"+_nguoiky+"','"+_sogiaykd+"','"+_ngaycapgiaykd+"','"+_donvi_kd+"','"+_maxacthuc+"','"+_userid+"','"+matinh_ngdung_+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();
			//System.out.println("#################dinhductoan###########dinhductoan"+rs_.getMaxRows());
			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}

	public String jcheck_TB_KM(String _msisdn) throws Exception {
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PKG_KHUYENMAI.check_subscriber('"+_msisdn+"'); ";
		xs_ = xs_ + " end; ";			
		return this.reqValue("PPS_DBMS", xs_);
	}
	
	/* ///OLD
	public String jsaveDataKM(String _sotb,String _sosim,String _tenkhachhang,String _ngaysinh,String _loaigt,String _sogt,String _ngaycap,String _noicap,String _loaikh,String _truong,String _tinhtrang,String _sothe, String _lop, String _capngay,String _quanid, String _phuongid, String _phoid,String _sonha,String _diachi,String _ghichu,String _baoho,String _khoahoc, String _kieuth, String mxt, String ip, String agentcode,
			String tenbh, String ngaysinhbh,String loaigtbh,String ngaycapbh,String noicapbh,String sogtbh,String diachibh
	) throws Exception 
	{
		String xs_ = "";	
		String _userid = getUserVar("userID");
		String sys_agentcode_ = getUserVar("sys_agentcode");
		
		xs_ = " begin "
			+ " 	? := PREPAID.PKG_KHUYENMAI.lay_sotb_theo_sogt_2('"+ _sogt+"','"+ _loaikh +"'); "
			+ "   commit; "
			+ " end; " ;
		
		String rs_ = null;
		try{
			rs_ = reqValue("PPS_DBMS", xs_);
		}catch (Exception ex) {
			ex.printStackTrace();
			rs_="0";
		}
		if (_kieuth=="1") {		
			if (!rs_.equals("0")) {
				return rs_;
			}
		}
		xs_ = " begin ";
			xs_ = xs_ + " 	? := PREPAID.hssv_dangky_('"+_sotb+"','"+ _sosim+"','"+ConvertFont.UtoD(_tenkhachhang)+"','"+ _ngaysinh+"','"+ _loaigt+"','"+_sogt+"','"+ _ngaycap+"','"+ _noicap+"','"+_loaikh+"','"+_truong+"','"+_tinhtrang+"','"+_sothe+"','"+_lop+"','"+_capngay+"','"+_quanid+"','"+_phuongid+"','"+_phoid+"','"+_sonha+"','"+ConvertFont.UtoD(_diachi)+"','"+_userid+"','"+sys_agentcode_+"','"+ConvertFont.UtoD(_ghichu)+"','"+_baoho+"','"+_khoahoc+"','"+mxt+"','"+_kieuth+"','"+ip+"','"+agentcode+"','"+ConvertFont.UtoD(tenbh)+"','"+ngaysinhbh+"','"+loaigtbh+"','"+ngaycapbh+"','"+noicapbh+"','"+sogtbh+"','"+ConvertFont.UtoD(diachibh)+"'); ";	
			xs_ = xs_ + " end; "; 		
		System.out.println(xs_);
		return this.reqValue("PPS_DBMS", xs_);		
	}
	*/
	///NEW_SPS
	public String jsaveDataKM(String _sotb,String _sosim,String _tenkhachhang,String _ngaysinh,String _gioitinh,String _loaigt,String _sogt,String _ngaycap,String _noicap,String _loaikh,String _truong,String _tinhtrang,String _sothe, String _lop, String _capngay,String _quanid, String _phuongid, String _phoid,String _sonha,String _diachi,String _ghichu,String _baoho,String _khoahoc, String _kieuth, String mxt, String ip, String agentcode,
			String tenbh, String ngaysinhbh,String loaigtbh,String ngaycapbh,String noicapbh,String sogtbh,String diachibh
	) throws Exception 
	{
		String xs_ = "";	
		String _userid = getUserVar("userID");
		String sys_agentcode_ = getUserVar("sys_agentcode");
		
		xs_ = " begin "
			+ " 	? := PREPAID.PKG_KHUYENMAI.lay_sotb_theo_sogt_2('"+ _sogt+"','"+ _loaikh +"'); "
			+ "   commit; "
			+ " end; " ;
		
		String rs_ = null;
		try{
			rs_ = reqValue("PPS_DBMS", xs_);
		}catch (Exception ex) {
			ex.printStackTrace();
			rs_="0";
		}
		if (_kieuth=="1") {		
			if (!rs_.equals("0")) {
				return rs_;
			}
		}
		xs_ = " begin ";		
		xs_ = xs_ + " 	? := PREPAID.hssv_dangky_sps_ccbs('"+_sotb+"','"+ _sosim+"','"+ConvertFont.UtoD(_tenkhachhang)+"','"+ _ngaysinh+"','"+ _gioitinh+"','"+ _loaigt+"','"+_sogt+"','"+ _ngaycap+"','"+ _noicap+"','"+_loaikh+"','"+_truong+"','"+_tinhtrang+"','"+_sothe+"','"+_lop+"','"+_capngay+"','"+_quanid+"','"+_phuongid+"','"+_phoid+"','"+_sonha+"','"+ConvertFont.UtoD(_diachi)+"','"+_userid+"','"+sys_agentcode_+"','"+ConvertFont.UtoD(_ghichu)+"','"+_baoho+"','"+_khoahoc+"','"+mxt+"','"+_kieuth+"','"+ip+"','"+agentcode+"','"+ConvertFont.UtoD(tenbh)+"','"+ngaysinhbh+"','"+loaigtbh+"','"+ngaycapbh+"','"+noicapbh+"','"+sogtbh+"','"+ConvertFont.UtoD(diachibh)+"'); ";		
			xs_ = xs_ + " end; "; 		
		System.out.println(xs_);
		return this.reqValue("PPS_DBMS", xs_);		
	}	
	////
	/* ///OLD
	public String jDelDataKM(String _sotb) throws Exception 	
	{
		String xs_ = "";	
		String _userid = getUserVar("userID");

		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := PREPAID.PKG_KHUYENMAI.huygoi('"+_sotb+"','"+_userid+"'); ";	
		xs_ = xs_ + " end; "; 		
		return this.reqValue("PPS_DBMS", xs_);
	}
	*/
	///NEW_SPS
	public String jDelDataKM(String _sotb) throws Exception 	
	{
		String xs_ = "";	
		String _userid = getUserVar("userID");

		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := PREPAID.huygoi_hssv_sps_ccbs('"+_sotb+"','"+_userid+"'); ";	
		xs_ = xs_ + " end; "; 		
		return this.reqValue("PPS_DBMS", xs_);
	}
	
	public String jvalue_dkmaxacthuc(			
			String psuserid,
			String pssotb
	) throws Exception {	
		String xs_ = "begin ?:=prepaid.pps.dangky_xacthuc('"+        	
			psuserid+"','"+
			"1414"+"','"+
			"MXT "+pssotb+"'); end;";
		return this.reqValue("PPS_DBMS", xs_);		
	}

	/* --------------------------------- Upload du lieu hoc sinh sinh vien --------------------------------------- */
	public String rec_laytt_kiemtra_hssv(String psUploadID) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=pttb/prepaid/upload_hssv/ajax_kiemtra";
		url_ = url_ + "&"+CesarCode.encode("upload_")+"="+psUploadID;				
		url_ = url_ +"&sid="+Math.random();	
		//System.out.println("##############################: " + url_);
		return url_;		
	}  
	//TrinhHV 24/12/2012
	public String rec_laytt_kiemtra_tt(String psUploadID) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=pttb/prepaid/upload_tt/ajax_kiemtra";
		url_ = url_ + "&"+CesarCode.encode("upload_")+"="+psUploadID;				
		url_ = url_ +"&sid="+Math.random();	
		//System.out.println("##############################: " + url_);
		return url_;		
	}
	//TrinhHV 24/12/2012
	public NEOCommonData value_thuchien_tt_theofile(String _upload, String _eload)	
	{
		String xs_ = "";	
		String _userid = getUserVar("userID");
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PKG_UPLOAD.update_tt_file('"+_upload+"','"+_eload+"','"+_userid+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	// TrinhHV 24/12/2012
  	public String rec_doc_tt_hople(String psUploadID)
	{
		String url_ = "/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=pttb/prepaid/upload_tt/ajax_hople";
  		url_ = url_ + "&"+CesarCode.encode("upload_")+"="+psUploadID;	
		url_ = url_ +"&sid="+Math.random();	
		return url_;
	}
	// TrinhHV 24/12/2012
	public String doc_tt_khonghople(String psUploadID)
	{
		String url_ = "/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=pttb/prepaid/upload_tt/ajax_khonghople";
  		url_ = url_ + "&"+CesarCode.encode("upload_")+"="+psUploadID;	
	 return url_;
	}
	
	// Sua lai cac ban ghi loi	
  	public String rec_doc_hople(String psUploadID)
	{
		String url_ = "/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=pttb/prepaid/upload_hssv/ajax_hople";
  		url_ = url_ + "&"+CesarCode.encode("upload_")+"="+psUploadID;	
		url_ = url_ +"&sid="+Math.random();	
		return url_;
	}
	
  	public String doc_khonghople(String psUploadID)
	{
		String url_ = "/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=pttb/prepaid/upload_hssv/ajax_khonghople";
  		url_ = url_ + "&"+CesarCode.encode("upload_")+"="+psUploadID;	
	 return url_;
	}
	
   	public String doc_nhaplai(String psUploadID)
	{
		String url_ = "/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=pttb/prepaid/upload_hssv/ajax_nhaplai";
  		url_ = url_ + "&"+CesarCode.encode("upload_")+"="+psUploadID;	
		return url_;
	}
	public String doc_tt_nhaplai(String psUploadID)
	{
		String url_ = "/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=pttb/prepaid/upload_tt/ajax_nhaplai";
  		url_ = url_ + "&"+CesarCode.encode("upload_")+"="+psUploadID;	
		return url_;
	}
	
	public NEOCommonData value_reUpload(String psUploadID, String pssotbs, String pstentbs, String pssogts, String psngaysinhs, String psngaycaps,String psnoicaps, String psdiachis,
		String pslops, String pskhoahocs, String pssothes, String psngaycapthes
	)
	{	
		String xs_ = "";	
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PKG_UPLOAD.reupload_dv_hs_sv_file('"+psUploadID+"','"+pssotbs+"','"+pstentbs+"','"+pssogts+"','"+psngaysinhs+"','"+psngaycaps+"','"+psnoicaps+"','"+psdiachis+"','"+pslops+"','"+pskhoahocs+"','"+pssothes+"','"+psngaycapthes+"');";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);	
	}
	/* ///OLD
	public NEOCommonData value_thuchien_hssv_theofile(String _upload, String _eload, String _loaikh, String _truong, String _tinhtrang)	
	{
		String xs_ = "";	
		String _userid = getUserVar("userID");
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PKG_UPLOAD.add_dv_hssv_file('"+_upload+"','"+_eload+"','"+_loaikh+"','"+_truong+"','"+_tinhtrang+"','"+_userid+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	*/
	///NEW_SPS
	public NEOCommonData value_thuchien_hssv_theofile(String _upload, String _eload, String _loaikh, String _truong, String _tinhtrang)	
	{
		String xs_ = "";	
		String _userid = getUserVar("userID");
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PKG_UPLOAD.add_dv_hssv_file_sps_ccbs('"+_upload+"','"+_eload+"','"+_loaikh+"','"+_truong+"','"+_tinhtrang+"','"+_userid+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	///
	public NEOCommonData value_del_upload(String psid, String psfilename)
	{	
		String xs_ = "";	
		String _userid = getUserVar("userID");	
		try{	
		xs_ = xs_ + " begin ";
		//xs_ = xs_ + " 	? := PREPAID.PKG_UPLOAD.xoa_file_image('"+psid+"'); ";	
		xs_ = xs_ + " 	? := PREPAID.xoa_file_image_ccbs('"+psid+"','"+psfilename+"','"+_userid+"'); ";	
		xs_ = xs_ + " end; "; 
		}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);			
	}
  
	public NEOCommonData value_del_err(String pssotb)
	{	
		String xs_ = "";	
		String _userid = getUserVar("userID");
		try{	
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := PREPAID.PKG_UPLOAD.dele_upload_err('"+pssotb+"','"+_userid+"'); ";	
		xs_ = xs_ + " end; "; 
		}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);			
	}
	
	public String layds_file(String pssotb){
	return "/main?"+CesarCode.encode("configFile")+"=pttb/prepaid/ajax_ds_file"
			+"&"+CesarCode.encode("sotb_")+"="+pssotb
			;
  }
  
  //03062017: Ham hien thi file anh ca nhan  
  
  public String layds_file_Personal(String pssotb){
	return "/main?"+CesarCode.encode("configFile")+"=pttb/prepaid/ajax_ds_file_Personal"
			+"&"+CesarCode.encode("sotb_")+"="+pssotb
			;
  }
  
  //22062017: Ham hien thi file anh ca nhan thue bao tra sau
  
  public String layds_file_Personal(String pssotb, String psmakh){
	return "/main?"+CesarCode.encode("configFile")+"=pttb/capnhat/danhba/upload_anh/ajax_ds_file_Personal"
			+"&"+CesarCode.encode("sotb_")+"="+pssotb
			+"&"+CesarCode.encode("makh_")+"="+psmakh
			;
  }
  
  public String layds_file_customer(String psmakh, String psmahd){
	return "/main?"+CesarCode.encode("configFile")+"=pttb/hthd/hdcq/upload_anh/ajax_ds_file_Personal"
			+"&"+CesarCode.encode("makh")+"="+psmakh
			+"&"+CesarCode.encode("mahd")+"="+psmahd
			;
  }
  
   public String layds_file_customer_dn(String psmakh, String psmahd){
	return "/main?"+CesarCode.encode("configFile")+"=pttb/hthd/hdcq/upload_anh/ajax_ds_file_Personal_dn"
			+"&"+CesarCode.encode("makh")+"="+psmakh
			+"&"+CesarCode.encode("mahd")+"="+psmahd
			;
  }
  
   public String layds_file_Personal_full(String pssotb, String psmakh){
	return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_ct/upload_anh/ajax_ds_file_Personal"
			+"&"+CesarCode.encode("sotb_")+"="+pssotb
			+"&"+CesarCode.encode("makh_")+"="+psmakh
			;
  }
  
  //24-11-2016: Bo sung hien thi anh theo cac Folder
  public String layds_file_1(String pssotb){
	return "/main?"+CesarCode.encode("configFile")+"=pttb/prepaid/ajax_ds_file_1"
			+"&"+CesarCode.encode("sotb_")+"="+pssotb
			;
  }
  
  public String layds_file_2(String pssotb){
	return "/main?"+CesarCode.encode("configFile")+"=pttb/prepaid/ajax_ds_file_2"
			+"&"+CesarCode.encode("sotb_")+"="+pssotb
			;
  }
  
  public String layds_file_3(String pssotb){
	return "/main?"+CesarCode.encode("configFile")+"=pttb/prepaid/ajax_ds_file_3"
			+"&"+CesarCode.encode("sotb_")+"="+pssotb
			;
  }
  
  public String jvalue_del_err(String pssotb) throws Exception 	
	{
		String xs_ = "";	
		String _userid = getUserVar("userID");
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := PREPAID.PKG_UPLOAD.dele_upload_err('"+pssotb+"','"+_userid+"'); ";	
		xs_ = xs_ + " end; "; 		
		return this.reqValue("PPS_DBMS", xs_);
	}

	
	public NEOCommonData chk_maxacthuc(String psmxt, String pssotb)
	{	
		String xs_ = "";	
		try{	
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := PREPAID.PPS.check_mxt('"+psmxt+"','"+pssotb+"'); ";	
		xs_ = xs_ + " end; "; 
		}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);			
	}	
	
	public NEOCommonData chk_maxacthuc_image(String psmxt, String pssotb)
	{	
		String xs_ = "";	
		String _userid = getUserVar("userID");
		try{	
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := PREPAID.PREPAID_FUNCTION.check_mxt('"+psmxt+"','"+pssotb+"','"+_userid+"'); ";	
		xs_ = xs_ + " end; "; 
		}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);			
	}	

	public String jchk_maxacthuc(String psmxt, String pssotb) throws Exception 	
	{
		String xs_ = "";			
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := PREPAID.PPS.check_mxt('"+psmxt+"','"+pssotb+"'); ";	
		xs_ = xs_ + " end; "; 		
		return this.reqValue("PPS_DBMS", xs_);
	}	
	public String rec_laytt_kiemtra(String psUploadID) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=prepaid/fileprocess/ajax_kiemtra";
		url_ = url_ + "&"+CesarCode.encode("upload_")+"="+psUploadID;				
		url_ = url_ +"&sid="+Math.random();	
		System.out.println("##############################: " + url_);
		return url_;		
	}
	public NEOCommonData value_thuchien_theofile(String _upload)	
	{
		String xs_ = "";	
		String _userid = getUserVar("userID");
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PKG_UPLOAD.add_members_file('"+_upload+"','"+_userid+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	public NEOCommonData LockAgent(String _agentid, String _status,String _userip,String _ghichu)
	{		
		String _userid = getUserVar("userID");
		
		//String _sys_userip = getUserVar("userIp");
	//	String clientIP_ = getRequest().getClientIp();
		//System.out.println("###################### "+ _sys_userip);
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.LockAgent('"+_agentid+"','"+_status+"','"+_userid+"','"+_userip+"','"+_ghichu+"'); ";
		xs_ = xs_ + " end; ";	
	//	bindSqlParam("2",_userid);			
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);		
	}
	public NEOCommonData lockagent_file(String psuserip)
	{
		String _userid = getUserVar("userID");
		String xs_ = "";
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PREPAID_FUNCTION.lockagent_file('"+_userid+"','"+psuserip+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);		
        ei_.setDataSrc("PPS_DBMS");	
		
		return new NEOCommonData(ei_);
	}
	public NEOCommonData value_huyUpload()
	{
		String _userid = getUserVar("userID");
		String xs_ = "";
		try{		
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.PREPAID_FUNCTION.delete_upload_status('"+_userid+"'); ";	
			xs_ = xs_ + " end; "; 		
			}
			catch (Exception ex) {
			ex.printStackTrace();
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);		
        ei_.setDataSrc("PPS_DBMS");	
		
		return new NEOCommonData(ei_);
	}
	public String docDSLSDL(String _so_tb) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=prepaid/151agent/mod_lsdl";
		url_ = url_ + "&"+CesarCode.encode("so_tb_")+"="+_so_tb;				
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	public NEOCommonData grant_user(String daily,String chk_hc, String chk_tt, String userid,String userip)
	{		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.grant_user('"+daily+"','"+chk_hc+"','"+chk_tt+"','"+userid+"','"+userip+"'); ";
		xs_ = xs_ + " end; ";			
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);		
	}
	//TrinhHV 26/12/2013 cap nhat phan quyen truy cap
	public NEOCommonData grant_user_login(String daily,String chk_hc, String chk_tt, String userid,String userip)
	{		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= PREPAID.PREPAID_FUNCTION.grant_user_login('"+daily+"','"+chk_hc+"','"+chk_tt+"','"+userid+"','"+userip+"'); ";
		xs_ = xs_ + " end; ";			
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);		
	}
	
	public NEOCommonData value_check_idnumber(String psidnumber)
	{	
		String xs_ = "";	
		try{	
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.kiemtra_sogt_3_thuebao('"+psidnumber+"'); ";	
			xs_ = xs_ + " end; "; 
		}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);			
	}
	public NEOCommonData sai_cmt(			
			String psDsSoTB,
			String psCMT
	){
        String s = "begin ?:=prepaid.util.capnhat_cmt_sai('"+        	
			psDsSoTB+"','"+
			psCMT+"','"+			
			getUserVar("userID")+"','"+
			getUserVar("userIP")+"'); end;";
		NEOExecInfo ei_ = new NEOExecInfo(s);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);       
    
	}
	public String docDSLS_IMAGES(String _so_tb) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=pttb/prepaid/mod_ls_image";
		url_ = url_ + "&"+CesarCode.encode("so_tb_")+"="+_so_tb;				
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	public NEOCommonData lay_noicap_cmnd(String psidnumber)
	{	
		String xs_ = "";	
		try{	
			xs_ = xs_ + " begin ";
			//xs_ = xs_ + " 	? := PREPAID.get_cmt_province('"+psidnumber+"'); ";	
			xs_ = xs_ + " 	? := PREPAID.get_cmt_detail('"+psidnumber+"'); ";	
			xs_ = xs_ + " end; "; 
		}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);			
	}
	
	public NEOCommonData laytt_pgd(String userid)
	{	
		String xs_ = "";	
		try{	
			xs_ = xs_ + " begin ";		
			xs_ = xs_ + " 	? := PREPAID.laytt_pgd('"+userid+"'); ";	
			xs_ = xs_ + " end; "; 
		}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);			
	}	
	
	public NEOCommonData check_cmt_pdk(String psidnumber)
	{	
		String xs_ = "";	
		try{	
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.check_cmt_pdk('"+psidnumber+"'); ";	
			xs_ = xs_ + " end; "; 
		}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);			
	}
	public NEOCommonData check_image_dn(String ma_kh, String ma_nguoidaidien)
	{	
		String xs_ = "";	
		try{	
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := PREPAID.check_image_customer(2,'','"+ma_kh+"','"+ma_nguoidaidien+"'); ";	
			xs_ = xs_ + " end; "; 
		}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("PPS_DBMS");			
		return new NEOCommonData(ei_);			
	}
	
	public NEOCommonData get_makh_cqsd(String ma_hd)
	{	
		String xs_ = "";	
		try{	
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.pttb_capnhat.get_makh('"+ma_hd+"'); ";	
			xs_ = xs_ + " end; "; 
		}
			catch (Exception ex) {
			ex.printStackTrace();			
		}
		NEOExecInfo ei_ = new NEOExecInfo(xs_);       
		return new NEOCommonData(ei_);			
	}
	
}