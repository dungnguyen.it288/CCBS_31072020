package sbf;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

import cosaldap.Base64.*;

import java.net.*;
import java.io.*;

public class vinanv extends NEOProcessEx
{

	public NEOCommonData layTTThe(String so_the)
	{
		String so_the_ = so_the.replace("'","''");
		String uid_ = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("matinh_ngdung");

		String xs_ = "";
		xs_ = xs_ + "begin ?:= PKG_LVP.laytt_huythe('"+so_the_+"','"+uid_+"','GPC','"+matinh_ngdung_+"'); end;";
        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("LVP_DBMS");		

		return new NEOCommonData(ei_);
	}
	
	public NEOCommonData huythe(String so_the)
	{
		String so_the_ = so_the.replace("'","''");
		String uid_ = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("matinh_ngdung");
		
		String xs_ = "";
		xs_ = xs_ + "begin ?:= PKG_LVP.capnhat_huythe('"+so_the_+"','"+uid_+"','GPC','"+matinh_ngdung_+"'); end;";

        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("LVP_DBMS");		

		return new NEOCommonData(ei_);
	}	

	public String docDsHuyThe(String serial, String tu_ngay, String den_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dshuythe";
		url_ = url_ + "&"+CesarCode.encode("serial")+"="+serial;
		url_ = url_ + "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay;
		url_ = url_ + "&"+CesarCode.encode("den_ngay")+"="+den_ngay;
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}


	public NEOCommonData dmDV(String so_tb, String dsDvuThaydoi, String ghichu, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String dsDvuThaydoi_ = dsDvuThaydoi.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		String uid_ = uid.replace("'","''");
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.PKG_NVUCSC.dong_mo_ds_dvu('"+so_tb_+"','"+dsDvuThaydoi_+"','"+ghichu_+"','"+uid_+"','"+getUserVar("matinh_ngdung")+"'); ";
		xs_ = xs_ + " end; ";

	
        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	}

	public NEOCommonData chtb(String so_tb, String loai_moi, String tinh_moi, String ghichu, String uid)
	{
		String decloai_moi_ = "";
		String dectinh_moi_ = "";

		decloai_moi_ = loai_moi.trim(); //CesarCode.decode(loai_moi).trim();
		dectinh_moi_ = CesarCode.decode(tinh_moi).trim();

		String so_tb_ = so_tb.replace("'","''");
		String loai_moi_ = loai_moi.replace("'","''");
		String tinh_moi_ = tinh_moi.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		String uid_ = uid.replace("'","''");
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.PKG_NVUCSC.chdoi_lhtb('"+so_tb+"','"+decloai_moi_+"','"+dectinh_moi_+"','"+ghichu+"','"+uid+"','"+getUserVar("matinh_ngdung")+"'); ";
		xs_ = xs_ + " end; ";

        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	}

	public NEOCommonData layds_loaimoi(String so_tb, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String uid_ = uid.replace("'","''");

		String xs_ = "";
		xs_ = xs_ + " <select class=textField name='-tinhmoi' id='-tinhmoi' ";
		xs_ = xs_ + " 	sql='begin ?:=subadmin.pkg_nvucsc.chdoi_lhtb_layds_loai_moi('"+so_tb_+"','"+uid_+"'); end;' ";
		xs_ = xs_ + " 	style='background-color :#f1e8e6'  dbms-font='VN8VN3' ";
		xs_ = xs_ + " 	notusename='true' />";

        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	}
	
	public NEOCommonData layds_tinhmoi(String so_tb, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String uid_ = uid.replace("'","''");
	
		String xs_ = "";
		xs_ = xs_ + " <select class=textField name='-tinhmoi' id='-tinhmoi' ";
		xs_ = xs_ + " 	sql='begin ?:=subadmin.pkg_nvucsc.chdoi_lhtb_layds_tinh_moi('"+so_tb_+"','"+uid_+"'); end;' ";
		xs_ = xs_ + " 	style='background-color :#f1e8e6'  dbms-font='VN8VN3' ";
		xs_ = xs_ + " 	notusename='true' />";

        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	}

	public NEOCommonData dsimtb(String so_tb, String so_msin_moi, int simfree, String ghichu, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String so_msin_moi_ = so_msin_moi.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		String uid_ = uid.replace("'","''");
	
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.PKG_NVUCSC.DOI_SIM_TB('"+so_tb_+"','"+so_msin_moi_+"',"+simfree+",'"+ghichu_+"','"+uid_+"','"+getUserVar("matinh_ngdung")+"'); ";
		xs_ = xs_ + " end; ";

        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	}

	public NEOCommonData chkSMOI(String so_tb, String so_msin_moi)
	{
		String so_tb_ = so_tb.replace("'","''");
		String so_msin_moi_ = so_msin_moi.replace("'","''");
		String matinh_ngdung_ = getUserVar("matinh_ngdung");

		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc.doi_sim_ktra_SIM_moi('"
			+so_tb_+"','"+so_msin_moi_+"','"+matinh_ngdung_+"'); ";
		xs_ = xs_ + " end; ";
		
        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	}

	public NEOCommonData khoitao_AC(String so_tb, String ghichu, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		String uid_ = uid.replace("'","''");

		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc.khoitao_AC('"+so_tb_+"','"+ghichu_+"','"+uid_+"','"+getUserVar("matinh_ngdung")+"'); ";
		//xs_ = xs_ + " 	commit; ";
		xs_ = xs_ + " end; ";

        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	}

	public NEOCommonData khoitao_laiTB(String so_tb, String loai_moi, String tinh_moi, String ghichu, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		String uid_ = uid.replace("'","''");

		String decloai_moi_ = "";
		String dectinh_moi_ = "";

		decloai_moi_ = loai_moi.trim();
		dectinh_moi_ = CesarCode.decode(tinh_moi).trim();
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc.khoitao_laiTB('"+so_tb_+"','"
			+decloai_moi_+"','"+dectinh_moi_+"','"+ghichu_+"','"+uid_+"','"+getUserVar("matinh_ngdung")+"'); ";
		//xs_ = xs_ + " 	commit; ";
		xs_ = xs_ + " end; ";

        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	}

	public NEOCommonData doitenTb(String so_tb, String tentb_moi)
	{
		String so_tb_ = so_tb.replace("'","''");
		String tentb_moi_ = tentb_moi.replace("'","''");
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc.cat_mo_cap_nhat_ten('"+so_tb_+"','"+tentb_moi_+"'); ";
		//xs_ = xs_ + " 	commit; ";
		xs_ = xs_ + " end; ";

        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	}

	public NEOCommonData doiDchiTb(String so_tb, String dchi_moi)
	{
		String so_tb_ = so_tb.replace("'","''");
		String dchi_moi_ = dchi_moi.replace("'","''");

		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc.cat_mo_cap_nhat_dia_chi('"+so_tb+"','"+dchi_moi_+"'); ";
		//xs_ = xs_ + " 	commit; ";
		xs_ = xs_ + " end; ";

        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	}

	public NEOCommonData catmoICOC(String trth, String goidi, String goiden, String so_tb, String ma_lydo, String ghichu, String uid)
	{
		String ghichu_ = ghichu.replace("'","''");
		String decMa_lydo_ = CesarCode.decode(ma_lydo).trim();
	
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc.cat_mo_OC_IC_huy_HD('"+trth+"','"+goidi+"','"+goiden+"','"+so_tb+"','"+decMa_lydo_+"','"+ghichu_+"','"+uid+"','"+getUserVar("matinh_ngdung")+"'); ";
		xs_ = xs_ + "   commit; ";
		xs_ = xs_ + " end; ";

		//System.out.println("catmoICOC:"+xs_);
        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	}

	public NEOCommonData dkyGoiGPRS(String trth, String so_tb, String ma_goi, String ghichu, String uid)
	{
		String ghichu_ = ghichu.replace("'","''");
		String goi_moi_ = "";

		goi_moi_ = CesarCode.decode(ma_goi).trim();
	
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc.dky_goi_gprs('"+trth+"','"+so_tb+"','"+goi_moi_
			+"','"+ghichu_+"','"+uid+"'); ";
			//+"','"+ghichu_+"','"+uid+"','"+getUserVar("matinh_ngdung")+"'); ";
		xs_ = xs_ + "   commit; ";
		xs_ = xs_ + " end; ";

        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	}
	
	public String docDvDky(String so_tb_daydu) {
		//String so_tb_daydu_ = so_tb_daydu.replace("'","''");
		
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dmdv_tb";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	
	public String docDkyDv() {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dkydv";
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}

	public String docDvuTb(String so_tb_daydu) {
		String x_ = "begin ?:= subadmin.PKG_NVUCSC.layds_dvutb(?); end;";
		setUserVar("sqlDvuTb",x_);	
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dvu_tb";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docLsTb(String so_tb_daydu) {
		String x_ = "begin ? := subadmin.PKG_UTILS.LAYDS_LSU_THUEBAO(?); end;";
		setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_lstb";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	public String docLsTb(String so_tb_daydu,String page_num, String page_rec) {
		String x_ = "begin ? := subadmin.PKG_UTILS.LAYDS_LSU_THUEBAO(?,?,?); end;";
		setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_lstb1";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("page_num")+"="+page_num;
		url_ = url_ + "&"+CesarCode.encode("page_rec")+"="+page_rec;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	public NEOCommonData docCuocDaily(String url) {
		String url_= "http://190.10.10.86"+url;
		//System.out.println("================== docCuocDaily:");
		//System.out.println(url_);

		String doc_ = null;
		
		try
		{
			java.net.URL cuocDoc_ = new java.net.URL(url_);
			BufferedReader in = new BufferedReader(
				new InputStreamReader(
				cuocDoc_.openStream()));

			String inputLine;
			StringBuilder sb_ = new StringBuilder();

			while ((inputLine = in.readLine()) != null) {
				//System.out.println("inputLine: "+ inputLine);
				sb_.append(inputLine);
			}
			in.close();		
		
			doc_ = sb_.toString();
		} catch (Exception ex) {
			doc_ = ex.toString();
		}
		//
		return new NEOCommonData(doc_);	
	}
	
	public String docLsNo(String so_tb_daydu, String ma_tinh, String tu_ky, String den_ky, String ma_kh) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_ttlsnotra";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("ma_tinh")+"="+ma_tinh;
		url_ = url_ + "&"+CesarCode.encode("tu_ky")+"="+tu_ky;
		url_ = url_ + "&"+CesarCode.encode("den_ky")+"="+den_ky;
		url_ = url_ + "&"+CesarCode.encode("ma_kh")+"="+ma_kh;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docDsGoiGPRS(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dsgoigprs";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docDsLoaiMoi(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dsloaimoi";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docDsTinhMoi(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dstinhmoi";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docDsLoaiDvuCMTF(int thao_tac) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dsdvu_cmtf";
		url_ = url_ + "&"+CesarCode.encode("thao_tac")+"="+thao_tac;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	public String docDsLoaiHD(String so_tb_daydu, String sp_name) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=xdc/mod_ds_loaihd";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("sp_name")+"=VNSG";
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docDsMaHD(String so_tb_daydu, String ma_loaihd, String sp_name) {
		String dec_ma_loai_hd_  = CesarCode.decode(ma_loaihd);
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=xdc/mod_ds_mahd";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("ma_loaihd")+"="+dec_ma_loai_hd_;
		url_ = url_ + "&"+CesarCode.encode("sp_name")+"=VNSG";
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	public String docDsLoaiKP(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dsloaiKP";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docDsTinhKP(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dstinhKP";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}	
	public NEOCommonData layTTSIM(String sim)
	{
		String sim_ = sim.replace("'","''");
		String uid_ = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("matinh_ngdung");

		String xs_ = "";
		xs_ = xs_ + "begin ?:= subadmin.PKG_NVUCSC.khoitaoAC_ktraSIM('"+sim_+"','"+uid_+"','"+matinh_ngdung_+"'); end;";

        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	}

	public NEOCommonData khoitaoAC(String sim, String ghi_chu)
	{
		String sim_ = sim.replace("'","''");
		String ghi_chu_ = ghi_chu.replace("'","''");
		String uid_ = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("matinh_ngdung");
		
		String xs_ = "";
		xs_ = xs_ + "begin ?:= subadmin.PKG_NVUCSC.khoitaoAC_SIM('"+sim_+"','"
			+ghi_chu_+"','"+uid_+"','"+matinh_ngdung_+"'); end;";

        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	}	

	public String docDkyDv_FullW() {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dkydv_fullw";
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	

	public String docXDCDsMaKH(String so_tb, String ma_tinh) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dsMaKh";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb;
		url_ = url_ + "&"+CesarCode.encode("ma_tinh")+"="+ma_tinh;
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}	
}
