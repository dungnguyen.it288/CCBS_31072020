package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_hdpps
extends NEOProcessEx {
	public void run()
	{
		System.out.println("neo.pttb_hdpps was called");
	}
	
	public String getUrlDSHD(int trangthaihd_id)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdpps_vsms/ajax_layds_hopdong"
		+"&"+CesarCode.encode("trangthaihd_id")+"="+trangthaihd_id
        ;
	}
	
	public String getUrlttHopdongPss(String schema, String ma_hd, String ma_tb, String ttdb, String user_id)
	{
		String s = "begin ? := " + CesarCode.decode(schema) + "PTTB_PPS_VMSM.laytt_tb('"
		+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")
		+"','" + ma_hd + "'"
		+ ",'" + ma_tb + "'"
		+ ",'" + ttdb + "'"
		+");"
		+ " end;";
		return s;
	}
	
	public String capnhat_hdpps(String schema   
        ,String psMA_HD
        ,String pdNGAY_LAPHD
        ,String psMA_TB
        ,String psTEN_KH
        ,String psDIACHI
        ,String piLOAI_GT
        ,String psSO_GT
        ,String pdNGAY_CAP_GT
		,String psNOI_CAP_GT
        ,String pdNGAY_SINH
        ,String psGHI_CHU
        ,String psMAY_CN
		,String psProductID
		,String psProductName
		,String psQuantity
		,String piTIEN_MAY
		,String piVAT_MAY
		,String piCUOC_DV
		,String piVAT_DV
		,String psLoaiTB
		)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "PTTB_PPS_VMSM.capnhat_hd_pps_vsms('"+getUserVar("userID")
					+"','"+getUserVar("sys_agentcode")
					+"','"+getUserVar("sys_dataschema")+"','"
					+psMA_HD + "'"
					+ ",'" + pdNGAY_LAPHD + "'"
					+ ",'" + psMA_TB + "'"
					+ ",'" + psTEN_KH + "'"
					+ "," + psDIACHI + ""
					+ "," + piLOAI_GT
					+ ",'" + psSO_GT + "'"
					+ ",'" + pdNGAY_CAP_GT + "'"
					+ ",'" + psNOI_CAP_GT + "'"					
					+ ",'" + pdNGAY_SINH + "'"
					+ ",'" + psGHI_CHU + "'"
					+ ",'" + psMAY_CN + "'"
					+ ",'" + psProductID + "'"
					+ ",'" + psProductName + "'"
					+ ",'" + psQuantity + "'"
					+ ",'" + piTIEN_MAY + "'"
					+ ",'" + piVAT_MAY + "'"
					+ ",'" + piCUOC_DV + "'"
					+ ",'" + piVAT_DV + "'"
					+ ",'" + psLoaiTB + "'"	
					+ ");"
					+ "end;";			
		System.out.println(s);
		return s;		
	}
	
	public String xoa_HDpps(String schema, String ma_hd)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "PTTB_PPS_VMSM.xoa_hdpps('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_hd + "'" + ");"
		+ " end;";
		System.out.println(s);
		return s;
	}
	

	
}