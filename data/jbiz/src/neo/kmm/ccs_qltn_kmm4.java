package neo.kmm;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import com.svcon.jdbf.*;
import javax.sql.RowSet;
import java.util.zip.*;
import java.io.*;
import java.net.*;
import org.apache.commons.net.*;
import neo.qltn.ftp;
import neo.qltn.FTPupload;
import sun.misc.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.*;
import java.text.*;
import java.nio.charset.StandardCharsets;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;

public class ccs_qltn_kmm4 extends NEOProcessEx{	
/*
public String exportCSV(
		String psthang,
		String pstinh
	){
		String fileName = new String("");
		String file = "";
		try{
			String sql = "";
			String fileUploadDir = getSysConst("FileUploadDir");
			//Lay ten file
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//ddMMyyyyHH
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//ddMMyyyyHH
				
			java.util.Date now = cal.getTime();
			String time = dateFormat.format(now).toString().replace("/","");
			fileName = "Export_RPT_DOTUOI_"+ time+ ".csv";			
			// creat folder to save csv file
			String folderPath = fileUploadDir +"report_news/";
			File f = new File(folderPath);
			if(!f.exists())
				f.mkdir();				
			file = fileUploadDir +"report_news/" +fileName;				
			File file1 = new File(file);
			if (file1.exists()) {
				file1.delete();
			}
			
		//FileWriter writer = new FileWriter(file1);
		
		OutputStream os = new FileOutputStream(file1);
		os.write(239);
		os.write(187);
		os.write(191);
		PrintWriter out = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
		
		try{
			int counter = 0;
			out.print(ConvertFont.decodeW1252("STT,M&#227; t&#7881;nh,Ch&#7911; nh&#243;m, Lo&#7841;i g&#243;i,Lo&#7841;i TB,Account Fiber, Ng&#224;y ho&#224;n c&#244;ng,T&#234;n KH, &#272;&#7883;a ch&#7881; KH,Th&#7901;i gian &#273;&#259;ng k&#253;,M&#227; ng&#432;&#7901;i GT, T&#234;n ng&#432;&#7901;i GT\n"));
			sql = "begin ? := admin_v2.pkg_rpt_sharing.get_data_gdvp_ao("
					+ "'"+pstinh+"',"
					+ "'"+psthang+"'"
					+ ");" 
					+ "end;";	
			System.out.println(sql);				
			RowSet rs = this.reqRSet(sql);	
			while (rs.next()) {				
				counter++;
				out.print(String.valueOf(counter)+"," 
						+ rs.getString("AGENT") + ","
						+ rs.getString("MSISDN") + ","
						+ rs.getString("PACKAGE_NAME") + ","
						+ rs.getString("TYPE_MSISDN") + ","						
						+ rs.getString("ACCOUNT") + ","
						+ rs.getString("FIBER_DATE") + ",\""
						+ rs.getString("ACC_NAME") + "\",\""
						+ rs.getString("ACC_ADD") + "\","
						+ rs.getString("DATE_CREATE") + ","
						+ rs.getString("MA_NGUOI") + ",\""						
						+ rs.getString("TEN_NGUOI") + "\"\n"
				);
			}			
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}finally {
			out.flush();
			out.close();
		}
		return file;
	}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}
	}
*/
public String export_theodoi_kmm(String pschuky, String psmakh, String psmatb, String pskmm, String pschieu, String pslankhoa, String psngaykhoa, String psnguoith, String pstentt) 
	{
		
		String s="";
		String sDataSchema 	= getUserVar("sys_dataschema");	
		String sUserId 		= getUserVar("userID");
		String sFuncSchema 	= getSysConst("FuncSchema");	
		String fileName = new String("");
		String file = "";
		try{
			String sql = "";
			String fileUploadDir = getSysConst("FileUploadDir");
			//Lay ten file
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//ddMMyyyyHH
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//ddMMyyyyHH
				
			java.util.Date now = cal.getTime();
			String time = dateFormat.format(now).toString().replace("/","");
			fileName = "theodoi_kmm_"+ time+ ".txt";			
			// creat folder to save csv file
			String folderPath = fileUploadDir +"report_news/";
			File f = new File(folderPath);
			if(!f.exists())
				f.mkdir();				
			file = fileUploadDir +"report_news/" +fileName;				
			File file1 = new File(file);
			if (file1.exists()) {
				file1.delete();
			}
			
		//FileWriter writer = new FileWriter(file1);
		
		OutputStream os = new FileOutputStream(file1);
		os.write(239);
		os.write(187);
		os.write(191);
		PrintWriter out = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
		
		try{
			int counter = 0;
			
			
			out.print(ConvertFont.decodeW1252("ROW_STT,KMM_ID,MA_KH,MA_TB,NGAYTH,NGUOI_TH,CHIEU,KMM,KYHOADON,TEN_TT\n"));
			sql = "begin ?:=ccs_admin.qln_kmm1.TRACUU_EXPORT('"+pschuky+"','"+psmakh+"','"+psmatb+"','"+pskmm+"','"+pschieu+"','"+pslankhoa+"','"+psngaykhoa+"','"+psnguoith+"','"+pstentt+"','"+sDataSchema+"','"+sUserId+"','2','1','1000'); end;";				
			System.out.println(sql);
			RowSet rs = this.reqRSet(sql);	
			while (rs.next()) {				
				counter++;
				out.print(
						+ rs.getString("ROW_STT") + ","
						+ rs.getString("KMM_ID") + ","
						+ rs.getString("MA_KH") + ","
						+ rs.getString("MA_TB") + ","						
						+ rs.getString("NGAYTH") + ","
						+ rs.getString("NGUOI_TH") + ",\""
						+ rs.getString("CHIEU") + "\",\""
						+ rs.getString("KMM") + "\","
						+ rs.getString("KYHOADON") + ","
						+ rs.getString("TEN_TT") + "\"\n"
				);
			}			
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}finally {
			out.flush();
			out.close();
		}
		out.close();
		os.close();
		return file;
	}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}
	
	
	}
public String export_baocao_kmm(String pschuky, String psmakh, String psmatb, String pskmm, String pschieu, String pslankhoa, String psngaykhoa, String psnguoith, String pstentt) 
	{
		
		String s="";
		String sDataSchema 	= getUserVar("sys_dataschema");	
		String sUserId 		= getUserVar("userID");
		String sFuncSchema 	= getSysConst("FuncSchema");	
		String fileName = new String("");
		String file = "";
		try{
			String sql = "";
			String fileUploadDir = getSysConst("FileUploadDir");
			//Lay ten file
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//ddMMyyyyHH
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//ddMMyyyyHH
				
			java.util.Date now = cal.getTime();
			String time = dateFormat.format(now).toString().replace("/","");
			fileName = "theodoi_kmm_"+ time+ ".txt";			
			// creat folder to save csv file
			String folderPath = fileUploadDir +"report_news/";
			File f = new File(folderPath);
			if(!f.exists())
				f.mkdir();				
			file = fileUploadDir +"report_news/" +fileName;				
			File file1 = new File(file);
			if (file1.exists()) {
				file1.delete();
			}
			
		//FileWriter writer = new FileWriter(file1);
		
		OutputStream os = new FileOutputStream(file1);
		os.write(239);
		os.write(187);
		os.write(191);
		PrintWriter out = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
		
		try{
			int counter = 0;
			
			
			out.print(ConvertFont.decodeW1252("ROW_STT,KMM_ID,MA_KH,MA_TB,NGAYTH,NGUOI_TH,CHIEU,KMM,KYHOADON,TEN_TT\n"));
			sql = "begin ?:=ccs_admin.qln_kmm1.TRACUU_EXPORT('"+pschuky+"','"+psmakh+"','"+psmatb+"','"+pskmm+"','"+pschieu+"','"+pslankhoa+"','"+psngaykhoa+"','"+psnguoith+"','"+pstentt+"','"+sDataSchema+"','"+sUserId+"','2','1','1000'); end;";				
			System.out.println(sql);
			RowSet rs = this.reqRSet(sql);	
			while (rs.next()) {				
				counter++;
				out.print(
						+ rs.getString("ROW_STT") + ","
						+ rs.getString("KMM_ID") + ","
						+ rs.getString("MA_KH") + ","
						+ rs.getString("MA_TB") + ","						
						+ rs.getString("NGAYTH") + ","
						+ rs.getString("NGUOI_TH") + ",\""
						+ rs.getString("CHIEU") + "\",\""
						+ rs.getString("KMM") + "\","
						+ rs.getString("KYHOADON") + ","
						+ rs.getString("TEN_TT") + "\"\n"
				);
			}			
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}finally {
			out.flush();
			out.close();
		}
		out.close();
		os.close();
		return file;
	}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}
	
	
	}
public String nc_tonghop_sms(String pschuky, String pschema,String psdonvi, String psuserid) 
	{
		
		String s="";
		String sDataSchema 	= getUserVar("sys_dataschema");	
		String sUserId 		= getUserVar("userID");
		String sFuncSchema 	= getSysConst("FuncSchema");	
		String fileName = new String("");
		String file = "";
		try{
			String sql = "";
			String fileUploadDir = getSysConst("FileUploadDir");
			//Lay ten file
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//ddMMyyyyHH
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//ddMMyyyyHH
				
			java.util.Date now = cal.getTime();
			String time = dateFormat.format(now).toString().replace("/","");
			fileName = "nc_tonghop_sms_"+ time+ ".txt";			
			// creat folder to save csv file
			String folderPath = fileUploadDir +"report_news/";
			File f = new File(folderPath);
			if(!f.exists())
				f.mkdir();				
			file = fileUploadDir +"report_news/" +fileName;				
			File file1 = new File(file);
			if (file1.exists()) {
				file1.delete();
			}
			
		//FileWriter writer = new FileWriter(file1);
		
		OutputStream os = new FileOutputStream(file1);
		os.write(239);
		os.write(187);
		os.write(191);
		PrintWriter out = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
		
		try{
			int counter = 0;
			
			
			out.print(ConvertFont.decodeW1252("STT,LUOT_NT,USERID,LOGDATE,LOAI_SMS\n"));
			sql = "begin ?:=admin_v2.pkg_baocao_tinnhiem.nc_tonghop_sms('"+pschuky+"','"+pschema+"','"+psdonvi+"','"+psuserid+"'); end;";				
			System.out.println(sql);
			RowSet rs = this.reqRSet(sql);	
			while (rs.next()) {				
				counter++;
				out.print(
						+ rs.getString("ROWNUM") + ","
						+ rs.getString("LUOT_NT") + ","
						+ rs.getString("USERID") + ","
						+ rs.getString("LOGDATE") + ","						
						+ rs.getString("LOAI_SMS")+"\n"
				);
			}			
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}finally {
			out.flush();
			out.close();
		}
		out.close();
		os.close();
		return file;
	}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}
	
	
	}
public String nc_chitiet_sms(String pschuky, String pschema,String psdonvi, String psuserid) 
	{
		
		String s="";
		String sDataSchema 	= getUserVar("sys_dataschema");	
		String sUserId 		= getUserVar("userID");
		String sFuncSchema 	= getSysConst("FuncSchema");	
		String fileName = new String("");
		String file = "";
		try{
			String sql = "";
			String fileUploadDir = getSysConst("FileUploadDir");
			//Lay ten file
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//ddMMyyyyHH
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//ddMMyyyyHH
				
			java.util.Date now = cal.getTime();
			String time = dateFormat.format(now).toString().replace("/","");
			fileName = "nc_chitiet_sms_"+ time+ ".txt";			
			// creat folder to save csv file
			String folderPath = fileUploadDir +"report_news/";
			File f = new File(folderPath);
			if(!f.exists())
				f.mkdir();				
			file = fileUploadDir +"report_news/" +fileName;				
			File file1 = new File(file);
			if (file1.exists()) {
				file1.delete();
			}
			
		//FileWriter writer = new FileWriter(file1);
		
		OutputStream os = new FileOutputStream(file1);
		os.write(239);
		os.write(187);
		os.write(191);
		PrintWriter out = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
		
		try{
			int counter = 0;
			
			
			out.print(ConvertFont.decodeW1252("STT,LUOT_NT,SO_TB,USERID,LOGDATE,LOAI_SMS,NOIDUNG\n"));
			sql = "begin ?:=admin_v2.pkg_baocao_tinnhiem.nc_chitiet_sms('"+pschuky+"','"+pschema+"','"+psdonvi+"','"+psuserid+"'); end;";				
			System.out.println(sql);
			RowSet rs = this.reqRSet(sql);	
			while (rs.next()) {				
				counter++;
				out.print(
						+ rs.getString("ROWNUM") + ","
						+ rs.getString("LUOT_NT") + ","
						+ rs.getString("SO_TB") + ","
						+ rs.getString("USERID") + ","
						+ rs.getString("LOGDATE") + ","	
						+ rs.getString("LOAI_SMS") + ","
						+ rs.getString("NOIDUNG")+"\n"
				);
			}			
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}finally {
			out.flush();
			out.close();
		}
		out.close();
		os.close();
		return file;
	}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}
	
	
	}	
public String td_tonghop_sms(String pschuky, String pschema,String psdonvi, String psuserid,String kieubc) 
	{
		
		String s="";
		String sDataSchema 	= getUserVar("sys_dataschema");	
		String sUserId 		= getUserVar("userID");
		String sFuncSchema 	= getSysConst("FuncSchema");	
		String fileName = new String("");
		String file = "";
		try{
			String sql = "";
			String fileUploadDir = getSysConst("FileUploadDir");
			//Lay ten file
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//ddMMyyyyHH
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//ddMMyyyyHH
				
			java.util.Date now = cal.getTime();
			String time = dateFormat.format(now).toString().replace("/","");
			fileName = "td_tonghop_sms_"+ time+ ".txt";			
			// creat folder to save csv file
			String folderPath = fileUploadDir +"report_news/";
			File f = new File(folderPath);
			if(!f.exists())
				f.mkdir();				
			file = fileUploadDir +"report_news/" +fileName;				
			File file1 = new File(file);
			if (file1.exists()) {
				file1.delete();
			}
			
		//FileWriter writer = new FileWriter(file1);
		
		OutputStream os = new FileOutputStream(file1);
		os.write(239);
		os.write(187);
		os.write(191);
		PrintWriter out = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
		
		try{
			int counter = 0;
			
			
			out.print(ConvertFont.decodeW1252("STT,LUOTKHOAMAY,SOLUONG_TB,LOG_DATE\n"));
			sql = "begin ?:=admin_v2.pkg_baocao_tinnhiem.td_tonghop_sms('"+pschuky+"','"+pschema+"','"+psdonvi+"','"+psuserid+"','"+kieubc+"'); end;";				
			System.out.println(sql);
			RowSet rs = this.reqRSet(sql);	
			while (rs.next()) {				
				counter++;
				out.print(
						+ counter + ","
						+ rs.getString("LUOTKHOAMAY") + ","
						+ rs.getString("SOLUONG_TB") + ","						
						+ rs.getString("LOG_DATE")+"\n"
				);
			}			
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}finally {
			out.flush();
			out.close();
		}
		out.close();
		os.close();
		return file;
	}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}
	
	
	}
public String td_chitiet_sms(String pschuky, String pschema,String psdonvi, String psuserid,String kieubc) 
	{
		
		String s="";
		String sDataSchema 	= getUserVar("sys_dataschema");	
		String sUserId 		= getUserVar("userID");
		String sFuncSchema 	= getSysConst("FuncSchema");	
		String fileName = new String("");
		String file = "";
		try{
			String sql = "";
			String fileUploadDir = getSysConst("FileUploadDir");
			//Lay ten file
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//ddMMyyyyHH
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//ddMMyyyyHH
				
			java.util.Date now = cal.getTime();
			String time = dateFormat.format(now).toString().replace("/","");
			fileName = "td_chitiet_sms_"+ time+ ".txt";			
			// creat folder to save csv file
			String folderPath = fileUploadDir +"report_news/";
			File f = new File(folderPath);
			if(!f.exists())
				f.mkdir();				
			file = fileUploadDir +"report_news/" +fileName;				
			File file1 = new File(file);
			if (file1.exists()) {
				file1.delete();
			}
			
		//FileWriter writer = new FileWriter(file1);
		
		OutputStream os = new FileOutputStream(file1);
		os.write(239);
		os.write(187);
		os.write(191);
		PrintWriter out = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
		
		try{
			int counter = 0;
			
			
			out.print(ConvertFont.decodeW1252("STT,LUOTKHOAMAY,MSISDN,LOG_DATE,CONTENT\n"));
			sql = "begin ?:=admin_v2.pkg_baocao_tinnhiem.td_chitiet_sms('"+pschuky+"','"+pschema+"','"+psdonvi+"','"+psuserid+"','"+kieubc+"'); end;";				
			System.out.println(sql);
			RowSet rs = this.reqRSet(sql);	
			while (rs.next()) {				
				counter++;
				out.print(
						+ counter + ","
						+ rs.getString("LUOTKHOAMAY") + ","
						+ rs.getString("MSISDN") + ","						
						+ rs.getString("LOG_DATE") + ","
						+ rs.getString("CONTENT")+"\n"
				);
			}			
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}finally {
			out.flush();
			out.close();
		}
		out.close();
		os.close();
		return file;
	}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}
	
	
	}
public String td_tonghop_km(String pschuky, String pschema,String psdonvi, String psuserid,String kieubc) 
	{
		
		String s="";
		String sDataSchema 	= getUserVar("sys_dataschema");	
		String sUserId 		= getUserVar("userID");
		String sFuncSchema 	= getSysConst("FuncSchema");	
		String fileName = new String("");
		String file = "";
		try{
			String sql = "";
			String fileUploadDir = getSysConst("FileUploadDir");
			//Lay ten file
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//ddMMyyyyHH
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//ddMMyyyyHH
				
			java.util.Date now = cal.getTime();
			String time = dateFormat.format(now).toString().replace("/","");
			fileName = "td_tonghop_km_"+ time+ ".txt";			
			// creat folder to save csv file
			String folderPath = fileUploadDir +"report_news/";
			File f = new File(folderPath);
			if(!f.exists())
				f.mkdir();				
			file = fileUploadDir +"report_news/" +fileName;				
			File file1 = new File(file);
			if (file1.exists()) {
				file1.delete();
			}
			
		//FileWriter writer = new FileWriter(file1);
		
		OutputStream os = new FileOutputStream(file1);
		os.write(239);
		os.write(187);
		os.write(191);
		PrintWriter out = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
		
		try{
			int counter = 0;
			
			
			out.print(ConvertFont.decodeW1252("STT,LUOTKHOAMAY,SOLUONG_TB,LOG_DATE\n"));
			sql = "begin ?:=admin_v2.pkg_baocao_tinnhiem.td_tonghop_km('"+pschuky+"','"+pschema+"','"+psdonvi+"','"+psuserid+"','"+kieubc+"'); end;";				
			System.out.println(sql);
			RowSet rs = this.reqRSet(sql);	
			while (rs.next()) {				
				counter++;
				out.print(
						+ counter + ","
						+ rs.getString("LUOTKHOAMAY") + ","
						+ rs.getString("SOLUONG_TB") + ","
						+ rs.getString("LOG_DATE")+"\n"
				);
			}			
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}finally {
			out.flush();
			out.close();
		}
		out.close();
		os.close();
		return file;
	}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}
	
	
	}
public String td_chitiet_km(String pschuky, String pschema,String psdonvi, String psuserid,String kieubc) 
	{
		
		String s="";
		String sDataSchema 	= getUserVar("sys_dataschema");	
		String sUserId 		= getUserVar("userID");
		String sFuncSchema 	= getSysConst("FuncSchema");	
		String fileName = new String("");
		String file = "";
		try{
			String sql = "";
			String fileUploadDir = getSysConst("FileUploadDir");
			//Lay ten file
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//ddMMyyyyHH
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//ddMMyyyyHH
				
			java.util.Date now = cal.getTime();
			String time = dateFormat.format(now).toString().replace("/","");
			fileName = "td_chitiet_km_"+ time+ ".txt";			
			// creat folder to save csv file
			String folderPath = fileUploadDir +"report_news/";
			File f = new File(folderPath);
			if(!f.exists())
				f.mkdir();				
			file = fileUploadDir +"report_news/" +fileName;				
			File file1 = new File(file);
			if (file1.exists()) {
				file1.delete();
			}
			
		//FileWriter writer = new FileWriter(file1);
		
		OutputStream os = new FileOutputStream(file1);
		os.write(239);
		os.write(187);
		os.write(191);
		PrintWriter out = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
		
		try{
			int counter = 0;
			
			
			out.print(ConvertFont.decodeW1252("STT,LUOTKHOAMAY,MA_TB,LOG_DATE\n"));
			sql = "begin ?:=admin_v2.pkg_baocao_tinnhiem.td_chitiet_km('"+pschuky+"','"+pschema+"','"+psdonvi+"','"+psuserid+"','"+kieubc+"'); end;";				
			System.out.println(sql);
			RowSet rs = this.reqRSet(sql);	
			while (rs.next()) {				
				counter++;
				out.print(
						+ counter + ","
						+ rs.getString("LUOTKHOAMAY") + ","
						+ rs.getString("MA_TB") + ","
						+ rs.getString("LOG_DATE")+"\n"
				);
			}			
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}finally {
			out.flush();
			out.close();
		}
		out.close();
		os.close();
		return file;
	}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}
	
	
	}	
public String get_km_cauhinh(String pschuky, String pschema,String psdonvi, String psuserid) 
	{
		
		String s="";
		String sDataSchema 	= getUserVar("sys_dataschema");	
		String sUserId 		= getUserVar("userID");
		String sFuncSchema 	= getSysConst("FuncSchema");	
		String fileName = new String("");
		String file = "";
		try{
			String sql = "";
			String fileUploadDir = getSysConst("FileUploadDir");
			//Lay ten file
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//ddMMyyyyHH
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//ddMMyyyyHH
				
			java.util.Date now = cal.getTime();
			String time = dateFormat.format(now).toString().replace("/","");
			fileName = "cauhinh_"+ time+ ".txt";			
			// creat folder to save csv file
			String folderPath = fileUploadDir +"report_news/";
			File f = new File(folderPath);
			if(!f.exists())
				f.mkdir();				
			file = fileUploadDir +"report_news/" +fileName;				
			File file1 = new File(file);
			if (file1.exists()) {
				file1.delete();
			}
			
		//FileWriter writer = new FileWriter(file1);
		
		OutputStream os = new FileOutputStream(file1);
		os.write(239);
		os.write(187);
		os.write(191);
		PrintWriter out = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
		
		try{
			int counter = 0;
			
			
			out.print(ConvertFont.decodeW1252("STT,ID,TEN_DV,MONEY_FROM,MONEY_TO,TN_FROM,TN_TO,LOAIKH_ID,MONTH_LOOP,SMS_NEXT_DATE,KM1C_NEXT_DATE,KM2C_NEXT_DATE,NOCUOC_SMS,KM1C_SMS,KM2C_SMS,MODIFY_BY,MODIFY_DATE\n"));
			sql = "begin ?:=admin_v2.pkg_baocao_tinnhiem.get_km_cauhinh('"+pschuky+"','"+pschema+"','"+psdonvi+"','"+psuserid+"'); end;";				
			System.out.println(sql);
			RowSet rs = this.reqRSet(sql);	
			while (rs.next()) {				
				counter++;
				out.print(
						+ counter + ","
						+ rs.getString("ID") + ","
						+ rs.getString("TEN_DV") + ","
						+ rs.getString("MONEY_FROM") + ","
						+ rs.getString("MONEY_TO") + ","
						+ rs.getString("TN_FROM") + ","
						+ rs.getString("TN_TO") + ","
						+ rs.getString("LOAIKH_ID") + ","
						+ rs.getString("MONTH_LOOP") + ","
						+ rs.getString("SMS_NEXT_DATE") + ","
						+ rs.getString("KM1C_NEXT_DATE") + ","
						+ rs.getString("KM2C_NEXT_DATE") + ","
						+ rs.getString("NOCUOC_SMS") + ","
						+ rs.getString("KM1C_SMS") + ","
						+ rs.getString("KM2C_SMS") + ","
						+ rs.getString("MODIFY_BY") + ","
						+ rs.getString("MODIFY_DATE")+"\n"
				);
			}			
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}finally {
			out.flush();
			out.close();
		}
		out.close();
		os.close();
		return file;
	}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}
	
	
	}	
}