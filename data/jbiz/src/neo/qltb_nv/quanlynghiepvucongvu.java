/*
  -- Create by: natuan 
  -- create date: 17/05/2016
  -- content: quan ly thue bao nghiep vu, cong vu
 */

package neo.qltb_nv;

import neo.smartui.process.*;
import neo.smartui.common.*;
public class quanlynghiepvucongvu extends NEOProcessEx{

   public String register(String type, String msisdn, String registype, String username, String cust_name, String dispatch_number,
						String cust_address, String dispatch_date, String receipt_date, String package_id, String start_time,
						String end_time, String notes, String status, String userip)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_nghiepvu.register_business("
                + "'" + getUserVar("userID") + "'"
                + ",'" + userip + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'"               
				+ ",'" + type + "'"
				+ ",'" + msisdn + "'"
				+ ",'" + registype + "'"
                + ",'" + ConvertFont.UtoD(username) + "'"
                + ",'" + ConvertFont.UtoD(cust_name) + "'"				
                + ",'" + dispatch_number + "'"       
                + ",'" + ConvertFont.UtoD(cust_address) + "'"
                + ",'" + dispatch_date + "'"
				+ ",'" + receipt_date + "'"
				+",'"  + package_id +"'"
                + ",'" + start_time + "'"
                + ",'" + end_time + "'"
                + ",'" + ConvertFont.UtoD(notes) + "'" 
				+ ",'" + status + "'" 
                + ");"
                + "end;";
		try
			{		    
				 result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
	public String checkalo(String msisdn)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_nghiepvu.check_alo("
				+ "'" + msisdn + "'" 
                + ");"
                + "end;";
		try
			{		    
				 result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
	
	public String extend_(String type, String msisdn, String loainv, String goiid, String start_time, String end_time, String notes, String status, String userip)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_nghiepvu.extend_business("
                + "'" + getUserVar("userID") + "'"
                + ",'" + userip + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'"               
				+ ",'" + type + "'"
				+ ",'" + msisdn + "'"	
				+ ",'" + loainv + "'"	
				+ ",'" + goiid + "'"	
                + ",'" + start_time + "'"
                + ",'" + end_time + "'"
                + ",'" + ConvertFont.UtoD(notes) + "'" 
				+ ",'" + status + "'" 
                + ");"
                + "end;";
				
	   System.out.println("NATUAN:"+s);		
       try
			{		    
				 result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
	
	public String update(String msisdn, String username, String cust_name, String cust_address, String socvan, String ngaycvan, String ngaynhcv, String notes, String status, String userip)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_nghiepvu.update_business("
                + "'" + getUserVar("userID") + "'"
                + ",'" + userip + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'"               
				+ ",'" + msisdn + "'"
			    + ",'" + ConvertFont.UtoD(username) + "'"
                + ",'" + ConvertFont.UtoD(cust_name) + "'"				            
                + ",'" + ConvertFont.UtoD(cust_address) + "'"    
				+ ",'" + ConvertFont.UtoD(socvan) + "'"  
				+ ",'" + ngaycvan + "'"  
				+ ",'" + ngaynhcv + "'"  				
                + ",'" + ConvertFont.UtoD(notes) + "'" 
				+ ",'" + status + "'" 				
                + ");"
                + "end;";			
       try
			{		    
				result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
	
	public String delete_(String msisdn, String socv_baimien, String ngaybaimiencv, String ngaybaimien, String notes, String status, String userip)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_nghiepvu.delete_business("
                + "'" + getUserVar("userID") + "'"
                + ",'" + userip + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'"               
				+ ",'" + msisdn + "'"
				+ ",'" + socv_baimien + "'"
				+ ",'" + ngaybaimiencv + "'"
			    + ",'" + ngaybaimien + "'"
                + ",'" + status + "'"				                                      
                + ",'" + ConvertFont.UtoD(notes) + "'" 				
                + ");"
                + "end;";				
	   //System.out.println("NATUAN:"+s);		
       try
			{		    
				result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
	
	public String upload_business
	   (
	     String upload_id, 
		 String upload_type,
		 String package_id,
		 String userid, 
		 String userip
	   )
	   {
	    
		String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_nghiepvu.upload_business("         
                + "'" + upload_id + "'"
                + ",'" + upload_type + "'"
				+ ",'" + package_id + "'"
                + ",'" + userid + "'"
                + ",'" + userip + "'"    
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'"   				
                + ");"
                + "end;";
	System.out.println("NATUAN:"+s);		
       try
			{		    
				 result= this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	   }
//////////////////////////////////
////////////////////////B�n thiet bi 
	
	public String themmoi_thietbi(String loaitb,
								String sImei, 								 
								String soseri, 
								String tientb,
								String tentb, 
								String diachi, 
								String somay, 
								String ngaycn, 
								String ghichu,
								String userip,
								String type_
								)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_cell_iphone.themmoi_thietbi("
                + "'" + getUserVar("userID") + "'"
                + ",'" + userip + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'"             			
				+ ",'" + loaitb + "'"
				+ ",'" + sImei + "'"
				+ ",'" + soseri + "'"				
                + ",'" + tientb + "'"
				//+ ",''"
				+ ",'" + ConvertFont.UtoD(tentb) + "'"
				+ ",'" + ConvertFont.UtoD(diachi) + "'"
				+ ",'" + somay + "'"				
                + ",'" + ngaycn + "'"			
                + ",'" + ConvertFont.UtoD(ghichu) + "'" 
				+ ",'" + type_ + "'"
                + ");"
                + "end;";
		try
			{		    
				 result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
	////cap nhat thiet bi
	public String capnhat_thietbi(String id, 
								String loaitb,
								String sImei, 								 
								String soseri, 
								String tientb,
								String tentb, 
								String diachi, 
								String somay, 
								String ngaycn, 
								String ghichu,
								String userip,
								String type_
								)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_cell_iphone.capnhat_thietbi("
                + "'" + getUserVar("userID") + "'"
                + ",'" + userip + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'" 
				+ ",'" + id + "'"
				+ ",'" + loaitb + "'"
				+ ",'" + sImei + "'"
				+ ",'" + soseri + "'"				
                + ",'" + tientb + "'"
				+ ",'" + ConvertFont.UtoD(tentb) + "'"
				+ ",'" + ConvertFont.UtoD(diachi) + "'"
				+ ",'" + somay + "'"				
                + ",'" + ngaycn + "'"			
                + ",'" + ConvertFont.UtoD(ghichu) + "'" 
				+ ",'" + type_ + "'"
                + ");"
                + "end;";
		try
			{		    
				 result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
	///Xoa thiet bi
	public String xoa_thietbi(String id, 
							 String soseri,
							 String userip,
							 String type_ )
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_cell_iphone.xoa_thietbi("
                + "'" + getUserVar("userID") + "'"
                + ",'" + userip + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'" 
				+ ",'" + id + "'"
				+ ",'" +  soseri + "'"
				+ ",'" + type_ + "'"
                + ");"
                + "end;";
		try
			{		    
				 result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
	////
	public  String lay_loaitb(String loai_tb)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/vinaphoneS/imei_thietbi" +"&"+CesarCode.encode("loai_tb") + "=" + loai_tb;
	}
		////
	public  String lay_giatb(String loai_tb)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/vinaphoneS/gia_thietbi" +"&"+CesarCode.encode("loai_tb") + "=" + loai_tb;
	}
	////
	public  String lay_loaitb_(String loai_tb)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/vinaphoneS/imei_thietbi_" +"&"+CesarCode.encode("loai_tb") + "=" + loai_tb;
	}
		////
	public  String lay_giatb_(String loai_tb)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/vinaphoneS/gia_thietbi_" +"&"+CesarCode.encode("loai_tb") + "=" + loai_tb;
	}
	/////Thiet bi - Goi cuoc
	
	public String themmoi_thietbi_gc(String loaitb,
								String sImei, 								 
								String soseri, 
								String tientb,
								//String goicuoc,
								String tentb, 
								String diachi, 
								String somay, 
								String ngaycn, 
								String ghichu,
								String userip,
								String type_
								)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_cell_iphone.themmoi_thietbi("
                + "'" + getUserVar("userID") + "'"
                + ",'" + userip + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'"             			
				+ ",'" + loaitb + "'"
				+ ",'" + sImei + "'"
				+ ",'" + soseri + "'"				
                + ",'" + tientb + "'"
				//+ ",'" + goicuoc + "'" 
				+ ",'" + ConvertFont.UtoD(tentb) + "'"
				+ ",'" + ConvertFont.UtoD(diachi) + "'"
				+ ",'" + somay + "'"				
                + ",'" + ngaycn + "'"			
                + ",'" + ConvertFont.UtoD(ghichu) + "'" 
				+ ",'" + type_ + "'"
                + ");"
                + "end;";
		try
			{		    
				 result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
	
	//Cap nhat goi cuoc
	public String capnhat_thietbi_gc(String id, 
								String loaitb,
								String sImei, 								 
								String soseri, 
								String tientb,
								String goicuoc,
								String tentb, 
								String diachi, 
								String somay, 
								String ngaycn, 
								String ghichu,
								String userip,
								String type_
								)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_cell_iphone.capnhat_thietbi_gc("
                + "'" + getUserVar("userID") + "'"
                + ",'" + userip + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'" 
				+ ",'" + id + "'"
				+ ",'" + loaitb + "'"
				+ ",'" + sImei + "'"
				+ ",'" + soseri + "'"				
                + ",'" + tientb + "'"
				+ ",'" + goicuoc + "'"
				+ ",'" + ConvertFont.UtoD(tentb) + "'"
				+ ",'" + ConvertFont.UtoD(diachi) + "'"
				+ ",'" + somay + "'"				
                + ",'" + ngaycn + "'"			
                + ",'" + ConvertFont.UtoD(ghichu) + "'" 
				+ ",'" + type_ + "'"
                + ");"
                + "end;";
		try
			{		    
				 result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }	
	
	public String upload_huutuyen
	   (
	     String upload_id, 
		 String upload_type,		 
		 String userip
	   )
	   {
	    
		String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_huu_tuyen.upload_huutuyen("         
                + "'" + upload_id + "'"
                + ",'" + upload_type + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"
                + ",'" + getUserVar("userID") + "'"
                + ",'" + userip + "'"    								
                + ");"
                + "end;";
		//System.out.println("NATUAN:"+s);		
        try
			{		    
				 result= this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	   }
	public String dangky_goicuoc(
								String goicuoc,
								String diachi, 
								String somay, 
								String ngaycn, 
								String ghichu,
								String userip 
								)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_cell_iphone.dangky_goicuoc("
                + "'" + getUserVar("userID") + "'"
                + ",'" + userip + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'"             			
				+ ",'" + goicuoc + "'" 
				+ ",'" + ConvertFont.UtoD(diachi) + "'"
				+ ",'" + somay + "'"				
                + ",'" + ngaycn + "'"			
                + ",'" + ConvertFont.UtoD(ghichu) + "'" 
                + ");"
                + "end;";
		try
			{		    
				 result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
	
	///huy goi
	public String huygoicuoc(String stb, 
							 String package_id,
							 String userip
							  )
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_cell_iphone.huygoicuoc("
                + "'" + getUserVar("userID") + "'"
                + ",'" + userip + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'" 
				+ ",'" + package_id + "'"
				+ ",'" +  stb + "'"
                + ");"
                + "end;";
		try
			{		    
				 result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    } 

public String dangky_goicuocvss(
								String goicuoc,
								String diachi, 
								String somay, 
								String matau,
								String serial,
								String sodtchutau, 
								String ghichu,
								String userip,
								String typeth
								)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_cell_iphone.dangky_goicuocvss("
                + "'" + getUserVar("userID") + "'"
                + ",'" + getUserVar("userIP")  + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'"             			
				+ ",'" + goicuoc + "'" 
				+ ",'" + ConvertFont.UtoD(diachi) + "'"
				+ ",'" + somay + "'"
				+ ",'" + matau + "'"
				+ ",'" + serial + "'"
                + ",'" + sodtchutau + "'"
				+ ",'" + typeth + "'"	
                + ",'" + ConvertFont.UtoD(ghichu) + "'" 
                + ");"
                + "end;";
		try
			{		    
				 result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
	
	public String capnhatvss(
								String goicuoc,
								String diachi, 
								String somay, 
								String matau,
								String serial,
								String sodtchutau, 
								String ghichu,
								String userip
								)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_cell_iphone.capnhatvss("
                + "'" + getUserVar("userID") + "'"
                + ",'" + getUserVar("userIP")  + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'"             			
				+ ",'" + goicuoc + "'" 
				+ ",'" + ConvertFont.UtoD(diachi) + "'"
				+ ",'" + somay + "'"
				+ ",'" + matau + "'"
				+ ",'" + serial + "'"
                + ",'" + sodtchutau + "'" 	
                + ",'" + ConvertFont.UtoD(ghichu) + "'" 
                + ");"
                + "end;";
		try
			{		    
				 result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
		
	public String huygoicuocvss(String stb, 
							 String package_id,
							 String userip
							  )
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_cell_iphone.huygoicuoc_vss("
                + "'" + getUserVar("userID") + "'"
                + ",'" + userip + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'" 
				+ ",'" + package_id + "'"
				+ ",'" +  stb + "'"
                + ");"
                + "end;";
		try
			{		    
				 result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
	
	public String dangky_goicuocIR(
								String goicuoc,
								String diachi, 
								String somay, 								
								String userip 
								)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_cell_iphone.dangky_goicuoc_roaming("
                + "'" + getUserVar("userID") + "'"
                + ",'" + userip + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'"             			
				+ ",'" + goicuoc + "'" 
				+ ",'" + ConvertFont.UtoD(diachi) + "'"
				+ ",'" + somay + "'"
				+ ");"
                + "end;";
		try
			{		    
				 result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
	
	///huy goi
	public String huygoicuocIR(String stb, 
							 String package_id,
							 String userip
							  )
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_cell_iphone.huygoicuoc_roaming("
                + "'" + getUserVar("userID") + "'"
                + ",'" + userip + "'"
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'" 
				+ ",'" + package_id + "'"
				+ ",'" +  stb + "'"
                + ");"
                + "end;";
		try
			{		    
				 result=	this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    } 
	   
}
