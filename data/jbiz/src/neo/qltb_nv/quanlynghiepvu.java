/*
  -- Create by: hientt 
  -- create date: 20/03/2013
  -- content: quan ly thue bao nghiep vu
 */

package neo.qltb_nv;

import neo.smartui.process.*;
import neo.smartui.common.*;
public class quanlynghiepvu extends NEOProcessEx{

	
    public String upload_business
	   (
	     String upload_id, 
		 String upload_type,
		 String userid, 
		 String userip
	   )
	   {
	    
		String result="";
        String s = "begin ? := " 
                + "admin_v2.PK_QLNGHIEPVU.upload_business("         
                + "'" + upload_id + "'"
                + ",'" + upload_type + "'"
                + ",'" + userid + "'"
                + ",'" + userip + "'"              
                + ");"
                + "end;";
		
       try
			{		    
				 result=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	   }
	 public String synonym_data()
	   {
	    
		String result="";
        String s = "begin ? := " 
                + "admin_v2.PK_QLNGHIEPVU.synonym_data_func();"
                + "end;";
		System.out.println("Goi CCBS"+s);
       try
			{		    
				 result=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
		
        return result;	
	   }	
    public String insert( String msisdn,String username,
            String registype,String agent,String cust_name,
            String cust_address,String status,String start_time,
            String end_time,String dispatch_number,String dispatch_date,
            String receipt_date,String notes,String dissalldate, String socv_baimien)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.PK_QLNGHIEPVU.insert_("
                + "'" + getUserVar("userID") + "'"
                + ",'" + getUserVar("userIP") + "'"
                + ",'" + getUserVar("sys_dataschema") + "'"
                + ",'" + msisdn + "'"
                + ",'" + username + "'"
                + ",'" + registype + "'"
                + ",'" + agent + "'"
                + ",'" + cust_name + "'"
                + ",'" + cust_address + "'"
                + ",'" + status + "'"
                + ",'" + start_time + "'"
                + ",'" + end_time + "'"
                + ",'" + dispatch_number + "'"
                + ",'" + dispatch_date + "'"
                + ",'" + receipt_date + "'"
                + ",'" + notes + "'"
                +",'"+dissalldate+"'"
				+",'"+socv_baimien+"'"
                + ");"
                + "end;";
       try
			{		    
				 result=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
    public String update( String id,String msisdn,
			String username,
            String registype,
			String agent,
			String cust_name,
            String cust_address,
			String status,
			String start_time,
            String end_time,
			String dispatch_number,
			String dispatch_date,
            String receipt_date,
			String notes,
			String dissalldate, 
			String socv_baimien
)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.PK_QLNGHIEPVU.update_business("
                + "'" + getUserVar("userID") + "'"
                + ",'" + getUserVar("userIP") + "'"
                + ",'" + getUserVar("sys_dataschema") + "'"
				+ ",'" + id + "'"
                + ",'" + msisdn + "'"
				+ ",'" +  username+ "'"
                + ",'" + registype + "'"
                + ",'" + agent + "'"
                + ",'" + cust_name + "'"
                + ",'" + cust_address + "'"
                + ",'" + status + "'"
                + ",'" + start_time + "'"
                + ",'" + end_time + "'"
                + ",'" + dispatch_number + "'"
                + ",'" + dispatch_date + "'"
                + ",'" + receipt_date + "'"
                + ",'" + notes + "'"
                +",'"+dissalldate+"'"
                +",'"+socv_baimien+"'"
                + ");"
                + "end;";
        try
			{		    
				 result=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }

     public String remove( String msisdn, String type)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pk_qlnghiepvu.delete_business("
                + "'" + getUserVar("userID") + "'"
                + ",'" + getUserVar("userIP") + "'"
                + ",'" + getSysConst("sys_dataschema") + "'"
                + ",'" + msisdn + "'"
				+ ",'" + type + "'"
                + ");"
                + "end;";
       try
			{		    
				 result=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
	
}
