package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class doisim
    extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb_tthd was called");
  }
	public String get_dstb()
	{
	String url_ ="/main";
	url_ = url_ + "?"+CesarCode.encode("configFile")+"=PTTB/laphd/hd_doisim/frmDoiSim_ajax";
	url_ = url_ + "&sid="+Math.random();
	return url_;
	}
  
   public String lay_ma_hd_moi(String schema) {
    return "begin ?:= "+schema+"PTTB_TEST_DONG.lay_ma_hd_moi('"+getUserVar("userID")+"','"+getUserVar(		"sys_agentcode")+"','"+getUserVar("sys_dataschema")+"'); end;";
  }
  
   public String check_sim_exist(String schema,String somay_) {
    return "begin ?:="+schema+"PTTB_TEST_DONG.laytt_somay('"+getUserVar("userID")+"','"+getUserVar(		"sys_agentcode")+"','"+getUserVar("sys_dataschema")+"','"+somay_+"'); end;";
  }
  
   public String check_new_sim_exist(String schema,String sosim_,String somay_) {
    return "begin ?:="+schema+"PTTB_TEST_DONG.lay_sim_tontai('"+getUserVar("userID")+"','"+getUserVar(		"sys_agentcode")+"','"+getUserVar("sys_dataschema")+"','"+sosim_+"','"+somay_+"'); end;";
  }
  
   public String laytt_kh_by_code(String ma_kh_) {
    return "begin ?:="+getSysConst("FuncSchema")+"PTTB_TEST_DONG.laytt_kh_by_code('"+getUserVar("userID")+"','"+getUserVar(		"sys_agentcode")+"','"+getUserVar("sys_dataschema")+"','"+ma_kh_+"'); end;";
  }
  
   public String lay_hd_by_mahd(String schema,String ma_hd_) {
    return "begin ?:="+schema+"PTTB_TEST_DONG.laytt_hd_by_mahd('"+getUserVar("userID")+"','"+getUserVar(		"sys_agentcode")+"','"+getUserVar("sys_dataschema")+"','"+ma_hd_+"'); end;";
  }
  
   public String delete_hd(String schema,String ma_hd_,String so_sim_) {
    return "begin ?:="+schema+"PTTB_TEST_DONG.xoa_hd('"+getUserVar("userID")+"','"+getUserVar(		"sys_agentcode")+"','"+getUserVar("sys_dataschema")+"','"+ma_hd_+"','"+ so_sim_+"'); end;";
  }
  
   public String add_hopdong_doisim(String schema,String ma_hd_	
									,String somay_           
								    ,String sosim_           
									,String simcu_           
								    ,String trangthaihd_id_  
								    ,String ngay_laphd_      
								    ,String ten_tb_          
								    ,String ghichu_  
									,String nguoicn_  									
								    ,String cuahang_lhd_
									,String cuocdv_
									,String vat_
									,String diachitt_
								) 
	{
	String s=    "begin ? := "+schema+"PTTB_TEST_DONG.themmoi_hd_doisim("
									+"'"+   getUserVar("userID")+"'"
									+",'"+  getUserVar("sys_agentcode")+"'"
									+",'"+  getUserVar("sys_dataschema")+"'"
							        +",'"+   ma_hd_			+"'"
							        +",'"+	somay_			+"'"
							        +",'"+	sosim_			+"'"
									+",'"+	simcu_			+"'"
							        +",'"+	trangthaihd_id_ +"'"
							        +",'"+	ngay_laphd_		+"'"
							        +",'"+	ten_tb_			+"'"
							        +",'"+	ghichu_			+"'"
									+",'"+	nguoicn_		+"'"
							        +",'"+	cuahang_lhd_	+"'"								
									+",'"+	cuocdv_	        +"'"								
									+",'"+	vat_	        +"'"	
									+",'"+	diachitt_	    +"'"										
							        +");"
				+"end;" ;							
    return s;

	}
public String capnhat_hd_doisim(String schema,String ma_hd_	,									
								    String sosim_           ,
								    String ngay_laphd_      ,
								    String ghichu_          ,
								    String cuahang_lhd_     ,
									String cuocdv_          ,
									String vat_     		,
									String trangthaihd_id_  ,
									String diachitt_  
									) 
	{
	String s=    "begin ? := "+schema+"PTTB_TEST_DONG.capnhat_hd_doisim("
									+"'"+   getUserVar("userID")+"'"
									+",'"+  getUserVar("sys_agentcode")+"'"
									+",'"+  getUserVar("sys_dataschema")+"'"
							        +",'"+   ma_hd_			+"'"
							        +",'"+	sosim_			+"'"
							        +",'"+	ngay_laphd_		+"'"
							        +",'"+	ghichu_			+"'"
							        +",'"+	cuahang_lhd_	+"'"
									+",'"+	cuocdv_      	+"'"
									+",'"+	vat_        	+"'"
									+",'"+	trangthaihd_id_ +"'"
									+",'"+	diachitt_       +"'"
							        +");"
				+"end;" ;
    return s;

	}
	public String hoanThanhHD_doisim(String schema, String ma_hd)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "pttb_hthd.hoanthanh_hddoisim('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_hd + "'" + ");"
		+ " end;";
		return s;	
	}

}
