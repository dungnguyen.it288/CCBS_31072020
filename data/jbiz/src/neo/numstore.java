package neo;
import neo.smartui.SessionProcess;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;




public class numstore extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("NumStore");		
	}
	
	public String doc_layds_thuebao(
		String psSoTB,
		String psKieuso_ID,
		String psPageNum ,
		String psPageRec
	)
	{
		
		return "/main?" + CesarCode.encode("configFile")+"=dangky/ajax_ds_thuebao"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("kieuso_id") + "=" + psKieuso_ID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	
	public String doc_layth_thuebao(
		String psSoTB,
		String psKieuso_ID,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/laphd/chonso/ajax_th_thuebao"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("kieuso_id") + "=" + psKieuso_ID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=10";
			//+ "&" + CesarCode.encode("page_rec") + "=10" + psPageRec;
	}
	public String doc_layds_khoa(
		String psUserid
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/laphd/chonso/ajax_ds_khoa"
  			+ "&" + CesarCode.encode("userid") + "=" + psUserid;
	}
	
	public String doc_layds_thuebao_1(
		String psSoTB,
		String psKieuso_ID,
		String psSubRange,
		String psStatus,
		String psUserid,
		String psPageNum,
		String psPageRec
	)
	{
		try {
		  boolean blnCheck = SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"doc_layds_thuebao_1", getUserVar("userID"),
					getUserVar("sys_agentcode"),"10|1|30");
		  if(!blnCheck)
				return  "/main?"+CesarCode.encode("configFile")+"=admin/not-access";
		  reqValue("begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
							+psSoTB+"','doc_layds_thuebao_1'," 
							+"'"+psSoTB+";"+psKieuso_ID+";"+psSubRange+";"+psStatus+";"+psPageNum+";"+psPageRec+"','"
							+psUserid+"','"
							+getUserVar("userID")+"','"
							+getEnvInfo().getParserParam("sys_userip")+"','"
							+getUserVar("sys_agentcode")+"'); "
							+ "   commit; "
							+ " end; ");		
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return "/main?" + CesarCode.encode("configFile")+"=pttb/laphd/chonso/ajax_ds_thuebao_1"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("kieuso_id") + "=" + psKieuso_ID
			+ "&" + CesarCode.encode("sub_range") + "=" + psSubRange
			+ "&" + CesarCode.encode("status") + "=" + psStatus
			+ "&" + CesarCode.encode("userid") + "=" + getUserVar("userID")
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=10";			
	}
	
	public String doc_layds_thuebao_dk(
		String psSoTB,
		String psKieuso_ID,
		String psSubRange,
		String psStatus,
		String psUserid,
		String psPageNum,
		String psPageRec
	)
	{
	
		//Ghi log cat mo IC OC TRINHHV 19/01/2014
			try {
				boolean blnCheck = SessionProcess.addSession(getEnvInfo()
						.getParserParam("sys_userip").toString(),
						"doc_layds_thuebao_dk", getUserVar("userID"),
						getUserVar("sys_agentcode"),"10|1|30");
			if(!blnCheck)
				return  "/main?"+CesarCode.encode("configFile")+"=admin/not-access";
				  reqValue("begin "
							+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
									+psSoTB+"','doc_layds_thuebao_dk'," 
									+"'"+psSoTB+";"+psKieuso_ID+";"+psSubRange+";"+psStatus+";"+psPageNum+";"+psPageRec+"','"
									+psUserid+"','"
									+getUserVar("userID")+"','"
									+getEnvInfo().getParserParam("sys_userip")+"','"
									+getUserVar("sys_agentcode")+"'); "
									+ "   commit; "
									+ " end; ");		
					
				} catch (Exception ex) {
					ex.printStackTrace();	
				}
	
		return "/main?" + CesarCode.encode("configFile")+"=pttb/laphd/chonso/ajax_ds_thuebao_dk"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("kieuso_id") + "=" + psKieuso_ID
			+ "&" + CesarCode.encode("sub_range") + "=" + psSubRange
			+ "&" + CesarCode.encode("status") + "=" + psStatus
			+ "&" + CesarCode.encode("userid") + "=" + getUserVar("userID")
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=10";
	}
	
	public String doc_layds_kieuso_new(
		String psSubRange
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/laphd/chonso/ajax_kieuso_new"
  			+ "&" + CesarCode.encode("subrange") + "=" + psSubRange;
	}
	
	public String doc_validate(String psCode)
	{
		return "/jcaptchaValid?j_captcha_response="+psCode;
	}
/*
	Modify : luattd/20.06.2013
	Comment : check xac thuc captcha truoc khi gui lenhdang ky thue bao
*/	
	  
	public String doc_layds_daiso(
		String psSoTB,
		String psSearch,
		String psDaiso_ID,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/numstore/dangky_daiso/ajax_ds_daiso"
  			+ "&" + CesarCode.encode("soluong_tb") + "=" + psSoTB
			+ "&" + CesarCode.encode("search") + "=" + psSearch
  			+ "&" + CesarCode.encode("daiso_id") + "=" + psDaiso_ID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String doc_layth_daiso(
		String psSoTB,
		String psDaiso_ID,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/numstore/dangky_daiso/ajax_th_daiso"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("daiso_id") + "=" + psDaiso_ID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	
	public NEOCommonData dangky_daiso(
		String psIDDaiSo,
		String psDaiSoTB,
		String psSoluongTB,
		String psTen,
		String psDiachi,
		String psTenVietTat,
		String psTenDangKy,
		String psDienthoai,
		String psMaSoThue,
		String psWebsite,
		String psEmail,
		String psKieuLay,
		String psCode,
		String psSchema,
		String psUserID,
		String psUserIP
	){
		/*String s = "begin ?:= dms_public.store.dangky_daiso_admin('"
	 				+psDaiSoTB+"','"
					+psSoluongTB+"','"
					+psIDDaiSo+"','"
	 				+psTen+"','"
	 				+psDiachi+"','"
					+psTenVietTat+"','"
					+psTenDangKy+"','"
	 				+psDienthoai+"','"
	 				+psMaSoThue+"','"
	 				+psWebsite+"','"
	 				+psEmail+"','"
	 				+psCode+"','"
	 				+psSchema+"','"
					+psUserID+"','"
	 				+psUserIP+"'); end;"; */
		String s = "begin ?:= dms_public.pkg_daiso.dangky_daiso_admin('"
	 				+psDaiSoTB+"','"
					+psSoluongTB+"','"
					+psIDDaiSo+"','"
	 				+psTen+"','"
	 				+psDiachi+"','"
					+psTenVietTat+"','"
					+psTenDangKy+"','"
	 				+psDienthoai+"','"
	 				+psMaSoThue+"','"
	 				+psWebsite+"','"
	 				+psEmail+"','"
					+psKieuLay+"','"
	 				+psCode+"','"
	 				+psSchema+"','"
					+psUserID+"','"
	 				+psUserIP+"'); end;"; 			
		NEOExecInfo nei_ = new NEOExecInfo(s);
		nei_.setDataSrc("NUMSTOREDBMS");
		return new NEOCommonData(nei_);
	}
	
	public String doc_layds_chitiet_daiso(
		String psKhodaisoID,
		String psPageNum,
		String psPageRec,
		String pssoluong
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/numstore/chitiet_daiso/ajax_ds_chitiet_daiso"
  			+ "&" + CesarCode.encode("khodaiso_id") + "=" + psKhodaisoID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec
			+ "&" + CesarCode.encode("soluong") + "=" + pssoluong;
	}
	public String doc_layth_chitiet_daiso(
		String psKhodaisoID,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/numstore/chitiet_daiso/ajax_th_chitiet_daiso"
  			+ "&" + CesarCode.encode("khodaiso_id") + "=" + psKhodaisoID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	
	public String doc_layds_province(
		String psArea
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=lapphieu/ajax_province"
  			+ "&" + CesarCode.encode("area") + "=" + psArea;
	}
	
	public String doc_layds_cuahang(
		String psArea,
		String psProvince,
		String PsProvinceName
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=lapphieu/ajax_ds_cuahang"
  			+ "&" + CesarCode.encode("area") + "=" + psArea
  			+ "&" + CesarCode.encode("province") + "=" + psProvince
			+ "&" + CesarCode.encode("name") + "=" + PsProvinceName
			;
	}
	
public String book_range_number(
		String psIDDaiSo,
		String daisogoc,
		String psDaiSoTB,
		String psSoluongTB,
		String psTen,
		String psDiachi,
		String psTenVietTat,
		String psTenDangKy,
		String psDienthoai,
		String psMaSoThue,
		String psWebsite,
		String psEmail,
		String psKieuLay,
		String psThangCK,
		String psTienCK,
		String psTienDC,
		String psSchema,
		String psUserID,
		String psUserIP
	){		
		String s = "begin ?:= admin_v2.pkg_range_number.Book_range_number('"
	 				+psDaiSoTB+"','"
					+daisogoc+"','"
					+psSoluongTB+"','"
					+psIDDaiSo+"','"
	 				+psTen+"','"
	 				+psDiachi+"','"
					+psTenVietTat+"','"
					+psTenDangKy+"','"
	 				+psDienthoai+"','"
	 				+psMaSoThue+"','"
	 				+psWebsite+"','"
	 				+psEmail+"','"
					+psKieuLay+"','"
	 				+psThangCK+"','"
					+psTienCK+"','"
					+psTienDC+"','"
	 				+psSchema+"','"
					+psUserID+"','"
	 				+psUserIP+"'); end;"; 
		String result;
		try	{		    
			result=	this.reqValue("",s);			 
		} 
		catch (Exception ex){            	
			result = ex.getMessage();
		}
		return result;
	}
	public String regist_range_number(
		String msin,
		String so_tb,
		String xacthuc,
		String psDescription, 
		String loai
	){		
		String _userid = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		String s = "begin ?:= admin_v2.pkg_range_number.init_range_number('"
	 				+msin+"','"
					+so_tb+"','"
					+xacthuc+"','"
					+_userid+"','"
					+matinh_ngdung_+"','"
	 				+psDescription+"','"	 				
	 				+loai+"'); end;"; 
		String result;
		try	{		    
			result=	this.reqValue("",s);			 
		} 
		catch (Exception ex){            	
			result = ex.getMessage();
		}
		return result;
	}

	
  public String doc_layds_thuebao_api(
		String psSoTB,
		String psKieuso_ID,
		String psSubRange,
		String psStatus,
		String psUserid,
		String psPageNum,
		String psPageRec
	)
	{
		try {
		  boolean blnCheck = SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"doc_layds_thuebao_api", getUserVar("userID"),
					getUserVar("sys_agentcode"),"10|1|30");
		  if(!blnCheck)
				return  "/main?"+CesarCode.encode("configFile")+"=admin/not-access";
		  reqValue("begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
							+psSoTB+"','doc_layds_thuebao_api'," 
							+"'"+psSoTB+";"+psKieuso_ID+";"+psSubRange+";"+psStatus+";"+psPageNum+";"+psPageRec+"','"
							+psUserid+"','"
							+getUserVar("userID")+"','"
							+getEnvInfo().getParserParam("sys_userip")+"','"
							+getUserVar("sys_agentcode")+"'); "
							+ "   commit; "
							+ " end; ");		
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return "/main?" + CesarCode.encode("configFile")+"=pttb/laphd/chonso/ajax_ds_thuebao_api"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("kieuso_id") + "=" + psKieuso_ID
			+ "&" + CesarCode.encode("sub_range") + "=" + psSubRange
			+ "&" + CesarCode.encode("status") + "=" + psStatus
			+ "&" + CesarCode.encode("userid") + "=" + getUserVar("userID")
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=10";
	}
	
	public String doc_layth_thuebao_api(
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/laphd/chonso/ajax_th_thuebao_api"
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=10";			
	}
	
	public String doc_flag(String sFlag)
	{
		String s = "begin ? := admin_v2.pkg_chonso.lay_flag('"+sFlag+ "'); end;";
		System.out.println(s);
		return s;
	}
		//Kiem tra code file
}