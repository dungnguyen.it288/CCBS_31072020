package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class pttb_diachi
extends NEOProcessEx {	public void run()
	{
		System.out.println("neo.pttb_diachi was called");
	}
	   
   public String getUrlDs_Nhapquan(String pstinh) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/diachi/ajax_ds_nhapquan"
			+"&"+CesarCode.encode("tinh")+"="+pstinh
            ;
   }  	
   public String search_phos(String pstenpho,String piphuong_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/diachi/ajax_search_Pho"
        +"&"+CesarCode.encode("tenpho")+"="+pstenpho
		+"&"+CesarCode.encode("phuong_id")+"="+piphuong_id
        ;
   }  
   public String delete_frmQuans(String schema
									,String psSchema
									,String piQUAN_ID
														
									) {
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_DULIEU.danhmuc_delete_frmQuans('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+psSchema+"',"
       							+piQUAN_ID
								        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;
  }
  public String update_frmQuans(String schema
									,String psSchema
									,String piQUAN_ID
									,String psTENQUAN
									,String piTINH_ID
									) {
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_DULIEU.danhmuc_update_frmQuans('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+psSchema+"',"
		+"'"+piQUAN_ID+"',"
		+"'"+psTENQUAN+"',"
		+"'"+piTINH_ID+"'"
		+");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }  
  
	public String getUrlDs_phuong(String piquan_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/diachi/ajax_ds_nhapPhuong"
        +"&"+CesarCode.encode("quan_id")+"="+piquan_id
        ;
   }  
   public String update_frmPhuongs(String schema
									,String psSchema
									,String piPHUONG_ID
									,String psTENPHUONG
									,String psPREFIX
									,String piQUAN_ID
									,String piMUCCUOC_ID

									) {
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_DULIEU.danhmuc_update_frmPhuongs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+psSchema+"',"
       	+"'"+piPHUONG_ID+"',"
		+"'"+psTENPHUONG+"',"
		+"'"+psPREFIX+"',"
		+piQUAN_ID+","
		+piMUCCUOC_ID
        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }  
  public String delete_frmphuongs(String schema
									,String psSchema
									,String piquan_id
									,String piphuong_id
								) {

    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_DULIEU.danhmuc_delete_frmPhuongs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+psSchema+"',"
       							+piquan_id+","
								+piphuong_id
								+");"
				        +"end;"
				        ;
		System.out.println(s);
    return s;
	}
	
	public String load_cboquan_p(String pstinh) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/diachi/ajax_cboQuanP"
			+"&"+CesarCode.encode("tinh")+"="+pstinh
			;		
   }   
	
	
	public String load_cboquan_pho(String pstinh) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/diachi/ajax_cboQuanPho"
			+"&"+CesarCode.encode("tinh")+"="+pstinh
			;
   }	
   
  public String load_cbophuong(String piquan_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/diachi/ajax_cbophuong"
        +"&"+CesarCode.encode("quan_id")+"="+piquan_id
        ;
   }  
   
   public String load_DSpho(String piphuong_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/diachi/ajax_cboDSpho"
        +"&"+CesarCode.encode("phuong_id")+"="+piphuong_id
        ;
   }  
    public String update_frmphos(String schema
									,String psSchema
									,String piPHUONG_ID
									,String psDSpho_id
									
									) {
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_DULIEU.danhmuc_update_frmPhos('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+psSchema+"',"
       							+piPHUONG_ID+","
								+"'"+psDSpho_id+"'"
								
        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }  
  public String getUrlDsPhos(String piPAGEID,String piREC_PER_PAGE,String pstenpho) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/diachi/ajax_DSnhappho"
			+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
			+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
			+ "&" + CesarCode.encode("tenpho_") + "=" + pstenpho  
            ;
   }  
   
   public String update_frmNhapPho(String schema
									,String psSchema
									,String piPHO_ID
									,String psTENPHO
									
									) {
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_DULIEU.danhmuc_update_frmNhapPho('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+psSchema+"',"
		+"'"+piPHO_ID+"',"
		+"'"+psTENPHO+"'"
				
        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }  
  public String delete_frmNhapPho(String schema
									,String psSchema
									,String piPHO_ID
								) {

    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_DULIEU.danhmuc_delete_frmNhapPho('"+getUserVar("userID")
				+"','"+getUserVar("sys_agentcode")
				+"','"+psSchema+"',"
				+piPHO_ID								
				+");"
				+"end;"
				;
		System.out.println(s);
    return s;
	}
// Lay thong tin pho theo ma tuyen   
   public String layds_pho(String piphuong_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/Matuyen/ajax_ds_nhapPho"    
		+"&"+CesarCode.encode("phuong_id")+"="+piphuong_id
        ;
   }
// Lay thong tin phuong theo ma tuyen   
   	public String layds_phuong(String piquan_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/Matuyen/ajax_ds_nhapPhuong"
        +"&"+CesarCode.encode("quan_id")+"="+piquan_id
        ;
   } 
// Lay thong tin quan theo ma tuyen   
	public String layds_quan(String pstinh) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/Matuyen/ajax_cboQuanP"
			+"&"+CesarCode.encode("tinh")+"="+pstinh
			;		
   } 
	   
}
















