package neo;

import neo.smartui.common.CesarCode;
import neo.smartui.common.ConvertFont;

import neo.smartui.process.*;
import neo.smartui.report.*;
import neo.smartui.cache.*;

import java.util.*;
import javax.sql.*;

public class admin
extends NEOProcessEx {
	public void run() {
		System.out.println("neo.pttb_capnhat was called");
	}
     public String user_display_inform(String userid, String IdInform)
	    {
		    String s="";
			String rs_="";
			try{
		        
				  s= "declare n number; s varchar2(2000); out_ varchar2(200):='OK'; begin "+
			" begin "+		
				
			" s:='insert into admin_v2.user_display_inform (userid)"+
			" values(''"+userid+"'')' ; execute immediate s; commit;"+	
			" exception when others then "+
            " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "+			
			" end; ? :=out_;  end;";	
			rs_ =this.reqValue("", s);	
			s=rs_;
						  
			 
			}
			catch(Exception ex)
			{
			     ex.printStackTrace();
			}
				return s;
		}
    public NEOCommonData clearCachedData(String clsSYSTEM, String clsAGENT, String ma_tinh) {
		String result_ = "0";
		
		NEOCacheFactory cacheFactory_ = NEOCacheFactory.getInstance(getEnvInfo());
		try {
			if (clsSYSTEM.equals("1")) {
				NEOCacheRegion ncr_ = cacheFactory_.getGlobalCache();
				synchronized(ncr_) {
					ncr_.invalidateAllObject();
				}
			}
			
			if (clsAGENT.equals("1")) {
				NEOCacheRegion ncr_ = cacheFactory_.getRegionalCache(ma_tinh); 
				synchronized(ncr_) {
					ncr_.invalidateAllObject();
				}
			}	
			
			result_ = "1";
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		//
		return new NEOCommonData(result_);
	}
	
    public NEOCommonData layds_role(String psMatinhmoi, String userIP) {
		String result_ = null;
		
		String sql_;
		
		try{			
			getEnvInfo().clearUserCachedData();
			
			sql_ = "SELECT a.short_name, a.schema FROM ccs_common.agent a where short_name = '"+psMatinhmoi+"'";
			RowSet  rs_ = reqRSet(sql_);
			if ((rs_ != null) && (rs_.next())) {
				String sAgentCode = rs_.getString(1);		
				String sDataSchema = rs_.getString(2);

				setUserVar("sys_agentcode",sAgentCode);
				setUserVar("sys_dataschema",sDataSchema);
			
				sDataSchema = getUserVar("sys_dataschema");	
				sAgentCode = getUserVar("sys_agentcode");
				System.out.println("new sAgentCode: " + sAgentCode);
				System.out.println("new sDataSchema: " + sDataSchema);
			
				result_ = sAgentCode;
				
				// cap nhat cac bien session
				String sUserId = getUserVar("userID");
				sql_ = "begin ? := qtht.capnhat_matinhngdung('"+sUserId+"','"+psMatinhmoi+"'); end;";
				String kqcn_ = reqValue(sql_);
				
				//cap nhat lai du lieu quyen lam viec cho nguoi dung
				sql_ = "begin ? := qtht.clone_quyenlv_dulieu('"+sUserId+"','"+psMatinhmoi+"','"+userIP+"'); end;";
				kqcn_ = reqValue(sql_);
				
				// cap nhat lai cac bien thuoc tinh nguoi dung moi
				String adminUserId_ = "admin_"+sAgentCode.toLowerCase();
				sql_ ="begin ?:= qtht.layds_thuoctinh_theo_nguoidung('"+adminUserId_+"','"+sDataSchema+"','"+sAgentCode+"'); end;";
				RowSet rs1 =  reqRSet(sql_);		
				while (rs1.next()) {
					String name = rs1.getString("name");
					String value = rs1.getString("value");
				
					if ( (name !=null) && (value!=null) ) {
						name = ConvertFont.DtoU(name.trim());
						value = ConvertFont.DtoU(value.trim());
						setUserVar(name, value);
						System.out.println("name,value: " + name + " .:. " + value);
						
					}
				}
				
				rs_.close();
				rs1.close();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(result_);
	}
	
    public String layds_role(String psFuncSchema 
					        
					        ,String psRoleId 
					        ,String psRoleDescription 
									 ) 
	{
	String sUserId = getUserVar("userID");
	String sDataSchema = getSysConst("sys_dataschema");	
	String sAgentCode = getUserVar("sys_agentcode");
	
    String s=    "begin ? := "+CesarCode.decode(psFuncSchema)+"QTHT.layds_ql_role("
		+"  '"+getUserVar("userID")		
		+"','"+sDataSchema+"',"
		+"','"+psFuncSchema+"',"
		+"','"+sAgentCode      
		+",'"+psRoleId+"'"
		+",'"+psRoleDescription+"'"		
        +");"
        +"end;"
        ;
    
    return s;

  }
  
  
    public String getUrlDSUser(String piPAGEID,String piREC_PER_PAGE,String psUserId, String psBuuCuc, String psDVQL) {
    return "/main?"+CesarCode.encode("configFile")+"=ccbs_qltn/dulieu/user/ajax_dsuser"
        + "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
		+ "&" +CesarCode.encode("psUserId")+"="+psUserId
        + "&" +CesarCode.encode("psBuuCuc")+"="+psBuuCuc
        + "&" +CesarCode.encode("psDVQL")+"="+psDVQL
        ;
  }
  
      public String getDSRole(String psDSUser, String chk, String chkDaPhan) {
    return "/main?"+CesarCode.encode("configFile")+"=admin/frmuser_role_e_data"
        +"&"+CesarCode.encode("psDSUser")+"="+psDSUser
		+"&"+CesarCode.encode("chk")+"="+chk
		+"&"+CesarCode.encode("chkDaPhan")+"="+chkDaPhan
        
        ;
  }
  
  public String phanQuyen(String psdsuser 
					        ,String psdsrole 
							,String psIP 
									 ) 
	{
	String sFuncSchema = getSysConst("FuncSchema");
	String sUserId = getUserVar("userID");
	String sDataSchema = getSysConst("sys_dataschema");	
	String sAgentCode = getUserVar("sys_agentcode");
	
    String s=    "begin ? := "+sFuncSchema+"QTHT.phanquyen_nguoidung("
		+"  '"+psdsrole
		+"','"+psdsuser+"'"
		+",'"+sUserId+"'"
		+",'"+psIP+"'"		
        +");"
        +"end;"   ;
    return s;
   }
    
	public String phanQuyentheoAgent(String psdsuser 
					        ,String psdsagent 
							,String psIP 
							)  throws Exception 	
	{
	String _userid = getUserVar("userID");
		
    String s=    "begin ? := ADMIN_V2.QTHT.phanquyen_user_theo_agent("
		+"  '"+psdsuser
		+"','"+psdsagent+"'"
		+",'"+_userid+"'"
		+",'"+psIP+"'"		
        +");"
        +"end;"   ;
		
    return this.reqValue("", s);
  }
  
  public String CheckNsd(String psUserId) {
	String result="";
	String xs_ = "";
	xs_ = xs_ + " begin ";		
	xs_ = xs_ + " ?:= admin_v2.dggdv.checknsd('"+psUserId+"'); ";
	xs_ = xs_ + " end; ";
	System.out.println(xs_);
	return xs_;
	/*try {		    
		result=	this.reqValue("",xs_);
	} 
	catch (Exception ex) {            	
	    result = ex.getMessage();
	}
    return result;*/
  }

  public String CheckNsdAjax(String psUserId)throws Exception {
     String s=    "begin ? := admin_v2.dggdv.checknsd("		
		+"'"+psUserId+"'"		                               
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
 
  public String phanQuyentheoMenu(String psdsuser 
					        ,String psdsmenu 
							,String psIP 
							) 
	{
	String sFuncSchema = getSysConst("FuncSchema");
	String sUserId = getUserVar("userID");
	String sDataSchema = getSysConst("sys_dataschema");	
	String sAgentCode = getUserVar("sys_agentcode");
	
    String s=    "begin ? := ADMIN_V2.QTHT.phanquyen_user_theo_menu("
		+"  '"+psdsuser
		+"','"+psdsmenu+"'"
		+",'"+sUserId+"'"
		+",'"+psIP+"'"		
        +");"
        +"end;"   ;
    return s;

  }
	
	public String getDSMenu(String psDSUser) {
	return "/main?"+CesarCode.encode("configFile")+"=admin/frmuser_menu_ajax"
        +"&"+CesarCode.encode("userid")+"="+psDSUser
		;
  }
}
