package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class pttb_hddso
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb_hddso was called");
  }

  public String laytt_maTB(String schema,String userInput, String userid) {
       return "begin ? :="+CesarCode.decode(schema)+"PTTB_LAPHD.hddso_laytt_laphd('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }
  
  public String laytt_maKH(String schema,String psMAKH) {
    return "begin ?:="+CesarCode.decode(schema)+"PTTB_TEST_VIET.QuickSearchOnMaKH('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMAKH+"'); end;";
  }
  public String GetCharge(String schema,String loaitb_id) {
    return "begin ?:="+CesarCode.decode(schema)+"PTTB_TEST_VIET.GetCharge_frm_hddso('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+loaitb_id+"'); end;";
  }
  
  
  
  public String getUrlDsHD(String psMAHD) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hddso/ajax_layds_HD"
        +"&"+CesarCode.encode("ma_hd")+"="+psMAHD
        ;
  }
  
  public String laytt_mahd(String schema,String userInput, String userid) {
    return "begin ? :="+CesarCode.decode(schema)+"PTTB_LAPHD.hddso_laytheo_mahd('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }
//ham nay dung de thuc hien doi so cho from hopdong doi so
    
  public 	 String themmoi_hddso( String schema
								,String piLOAITB_ID
								,String psSOMAY
								,String psSOMOI
								,String pdNGAY_LAPHD
								,String piCUOC_DV
								,String psGHICHU
								,String piVAT
								,String piTRANGTHAIHD_ID
								,String piPhuong_id
								,String psuserid								
								,String psMA_KH
								,String psTEN_KH
								,String psDIACHI_KH
								,String psTEN_TT
								,String psDIACHI_TT
								)
								{
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_LAPHD.hddso_themmoi('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+piLOAITB_ID+"'"
									+",'"+psSOMAY+"'"
									+",'"+psSOMOI+"'"
									+",'"+pdNGAY_LAPHD+"'"
									+",'"+piCUOC_DV+"'"
									+",'"+psGHICHU+"'"
									+",'"+piVAT+"'"
									+",'"+piTRANGTHAIHD_ID+"'"									
									+",'"+piPhuong_id+"'"
									+",'"+psuserid+"'"
									+",'"+psMA_KH+"'"
									+",'"+psTEN_KH+"'"
									+",'"+psDIACHI_KH+"'"
									+",'"+psTEN_TT+"'"
									+",'"+psDIACHI_TT+"'"
									+");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }
  
  public 	 String capnhat_hddso( String schema
								,String psGHICHU
								,String piTRANGTHAIHD_ID
								,String psSOMOI
								,String psMAHD
								,String psSOMAY
								,String piLOAITB_ID
								,String piPhuong_id
								,String psUSERID
								)
								{
	    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_LAPHD.hddso_capnhat('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psGHICHU+"'"
									+",'"+piTRANGTHAIHD_ID+"'"
									+",'"+psSOMOI+"'"
									+",'"+psMAHD+"'"
									+",'"+psSOMAY+"'"
									+",'"+piLOAITB_ID+"'"
									+",'"+piPhuong_id+"'"
									+",'"+psUSERID+"'"
									+");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }
  //function nay dung de xoa hopdong doi so
  public String hddso_xoa(String schema
						,String psMA_HD
						,String psMA_TB
						,String psSOMOI
						,String piLOAITB_ID				
						) 

{
		String s=    "begin ? := "+CesarCode.decode(schema)+"PTTB_LAPHD.hddoiso_xoa('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
								+",'"+psMA_TB+"'"
								+",'"+psSOMOI+"'"
								+","+piLOAITB_ID
					+");"
				+"end;"
				;
			System.out.println(s);
			return s;

  }

//thanh toan
  public String hddso_laytt_mahd(String schema,String userInput, String userid) {
    return "begin ? :="+CesarCode.decode(schema)+"PTTB_THTOAN.hddso_laytt_mahd('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }

  public String hddso_laytt_matb(String schema,String userInput, String userid) {
       return "begin ? :="+CesarCode.decode(schema)+"PTTB_THTOAN.hddso_laytt_matb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }
  
  public String thanhtoan_hddso(String schema
									  , String psMA_HD									   
									   , String psUSERID
									   , String psUSERIP
									   , String piChucnang_gachno
									   , String pdNGAY_TT
									   , String pdNGAY_NH
									   , String piHTTT_ID
									   , String piLOAITIEN_ID
									   , String piNGANHANG_ID
									   , String psSOSEC
									   , String psCHUNGTU
									   )
	{


		String s = "begin ? := " + CesarCode.decode(schema) + "pttb_thtoan.hddso_gachno('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + psMA_HD + "'"			
			+ ",'" + psUSERID + "'"
			+ ",'" + psUSERIP + "'"
			+ "," + piChucnang_gachno
			+ ",'" + pdNGAY_TT + "'"
			+ ",'" + pdNGAY_NH + "'"
			+ "," + piHTTT_ID
			+ "," + piLOAITIEN_ID
			+ "," + piNGANHANG_ID
			+ ",'" + psSOSEC + "'"
			+ ",'" + psCHUNGTU + "'"
			+ ");"
			+ "end;"
			;
		return s;

	}
  public String xoaphieu_hddso(String schema
									, String psMA_HD
                                     ,String piPHIEU_ID
									 ,String psKIEUHUY
									 ,String psUSERID
									 ,String psUSERIP
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.hddso_xoaphieu_thanhtoan('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+piPHIEU_ID+"'"
		+",'"+psKIEUHUY+"'"
        +",'"+psUSERID+"'"
		+",'"+psUSERIP+"'"
        +");"
        +"end;"
        ;
    return s;

  }

  public String hddso_laydshd_thtoan(String piLOAIHD) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tthd/hddso/ajax_layds_HD"        
		+"&"+CesarCode.encode("LOAIHD")+"="+piLOAIHD
        ;
  }  

  public String hddso_guicatmo(String schema
									,String psMA_HD
									,String psUSERID									 
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schema)+"PTTB_THTOAN.hddso_guicatmo('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+psUSERID+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  //--------------------------------
}
