package neo.pttb;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.ResultSetMetaData;

import javax.commerce.util.BASE64Decoder;
import javax.sql.RowSet;

import neo.qltn.FTPupload;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class canhuy_tb extends NEOProcessEx {
  
   public String upload_sotb(
		 String ps_chema,
		 String ps_uploadid,
		 String ps_userid                         
  	) throws Exception {
	 
     String s=    "begin ? := "+ps_chema+"pkg_canhuy_tb.regis_msisdn("		
		+"'"+ps_chema+"'"
		+",'"+ps_uploadid+"'"	
		+",'"+ps_userid+"'"	
		+");"
        +"end;";
		return this.reqValue("", s);
	}
   public String can_msisdn(
			 String ps_chema,
			 String ps_dssotb,
			 String ps_agent,
			 String ps_userid,
			 String ps_userip 			 
	  	) throws Exception {
		 
	     String s =  "begin ? := "+ps_chema+"pkg_canhuy_tb.can_msisdn("		
			+"'"+ps_chema+"'"
			+",'"+ps_dssotb+"'"	
			+",'"+ps_agent+"'"	
			+",'"+ps_userid+"'"
			+",'"+ps_userip+"'"
			+");"
	        +"end;";
			return this.reqValue("", s);
	}
	
	public String active_list(
			String psschema,
			String psagent,
			String psds_sotb,
			String psuser_id ,
			String psuser_ip			 
	  	) throws Exception {
		 
	    String s =  "begin ? := "+psschema+"pkg_canhuy_tb.actie_list("		
			+"'"+psagent+"'"
			+",'"+psds_sotb+"'"	
			+",'"+psuser_id+"'"	
			+",'"+psuser_ip+"'"	
			+");"
	        +"end;";
			return this.reqValue("", s);
	}
    
	public String post_ftp_txt(
			 String ps_chema,
			 String ps_tinh,
			 String ps_type,
			 String ps_user_id                         
	  	) throws Exception {
	   
		String s =  "begin ? := "+ps_chema+"pkg_canhuy_tb.post_list_ftp("		
			+"'"+ps_tinh+"'"			
			+");"
	        +"end;";
			return this.reqValue("", s);
	}
	
	public String post_ftp(
			 String ps_chema,
			 String ps_tinh,
			 String ps_type,
			 String ps_user_id                         
	  	) throws Exception {
	   String txt_data="";
		try {			
			
			RowSet rs_ = this.reqRSet("","select a.sql_id, a.sql_text, a.matinh, a.temp_path, " +
					"a.file_name, a.script_file, a.ftp, a.ftp_path, a.username, a.pass " +
					"from ccs_common.data_exports a where a.sql_id=51");
			String ftp_ = "", user_ = "", pass_ = "";
			String ftp_path = "",sql="",file_name="";
			while (rs_.next()) {
				ftp_ = rs_.getString("ftp");
				user_ = rs_.getString("username");
				pass_ = rs_.getString("pass");
				ftp_path = rs_.getString("ftp_path").replace("$schema_user$", ps_tinh);
				file_name = rs_.getString("file_name");
				sql=rs_.getString("sql_text").replace("$agent$",ps_tinh);			
			}
			
			BASE64Decoder decoder = new BASE64Decoder();
			byte[] decodedBytes = decoder.decodeBuffer(pass_);
			String pass_d = new String(decodedBytes);
			
			RowSet rows = this.reqRSet("",sql);
			ResultSetMetaData metas = rows.getMetaData();
			com.svcon.jdbf.JDBField[] fields = new com.svcon.jdbf.JDBField[metas
					.getColumnCount()];
			for (int i = 1; i <= metas.getColumnCount(); i++) {
				String cname = metas.getColumnName(i);
				char ctype = new export_data().GetType(metas.getColumnTypeName(i));
				if (cname.length() > 10) cname = cname.substring(0, 10);
				int clength = metas.getColumnDisplaySize(i);
				if (ctype == 'N') clength = 20;
				else if (ctype == 'D') clength = 8;
				else if (clength >= 254) clength = 253;
				fields[i - 1] = new com.svcon.jdbf.JDBField(cname, ctype,
						clength, 0);
			}
			String f = getSysConst("FileUploadDir").replace("\\", "/").toLowerCase()+"list_msisdn";
			com.svcon.jdbf.DBFWriter writer = new com.svcon.jdbf.DBFWriter(f
					+ ".dbf", fields);
			Object[] record = new Object[metas.getColumnCount()];
			while (rows.next()) {
				txt_data += rows.getString("msisdn") + "\n";
				for (int j = 1; j <= metas.getColumnCount(); j++) {
					if (new export_data().GetType(metas.getColumnTypeName(j)) == 'N') {
						try {
							record[j - 1] = new Integer(rows.getString(metas.getColumnName(j)).substring(0, 253));
						} catch (Exception iex) {}
					} else {
						try {
							record[j - 1] = (rows.getString(metas.getColumnName(j)));
						} catch (Exception cex) {}
					}
				}
				writer.addRecord(record);
			}
			writer.close();
			new export_data().doZip(f + ".dbf", f + ".zip");

			if(ps_type.equals("1")) {
				System.out.println(f + ".txt");
		  		BufferedWriter output = new BufferedWriter(new FileWriter(f + ".txt"));
				output.write(txt_data);
		   		output.flush();output.close();
				return f + ".txt";
			}

			new FTPupload(ftp_, user_, pass_d, f + ".zip", (ftp_path+file_name+".zip").replace("\\", "/").toLowerCase());
			return (ftp_path +file_name+".zip").replace("\\", "/").toLowerCase();
		} catch (Exception ex) {
			return ex.getMessage();
		}
	}
   public String chuanhoa_sql(String str)
   {
	   String s = str.replace("$schema$","").replace("$pstungay$", "").replace("$psdenngay$", "").replace("$pssoluong$", "").replace("$psagent_code$", "");
	   return s;
   }
}















