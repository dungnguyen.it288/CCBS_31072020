package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class thuebao_cts extends NEOProcessEx {
	public String new_thuebao_cts(
		String pschema,
		String pma_tb,		
		String pso_sim,
		String ploaitb,
		String pngay_sinh,
		String ploaigt_id,
		String pso_gt,
		String pngay_cap,
		String pnoi_cap,
		String pten_tb,
		String pten_tt,
		String pma_nv,
		String pma_t,
		String pdiachi_ct,
		String pdiachi_tt,
		String pdoituong_id,
		String pma_bc,
		String pdiadiemtt_id,
		String pmst,
		String ptaikhoan,
		String pchuyenkhoan_id,
		String pnguoi_gt,
		String pnganhang_id,
		String pghi_chu,
		String pemail_bc,
		String pinchitiet,
		String ploaidcbc_id,
		String pkieubc_id,
		String pdiachi_bc,
		String pinhd_id,
		String pemail_hd,
		String pdienthoai_lh,
		String pdiachi_hd,
		String pnv_hd,			
		String pquanct,
		String pphuongct,
		String pphoct,
		String psonhact,
		String pquantt,
		String pphuongtt,
		String pphott,
		String psonhatt,
		String pquanld,
		String pphuongld,
		String pphold,
		String psonhald,
		String psysagent,
		String pdataschema,			
		String pagentcode,
		String puserid,
		String puserip,
		String ps_diachi_tr,
		String ps_doituong_sd,
		String ps_quoc_tich
		) throws Exception {
			// begin ? :=admin_v2.pk_thuebao_cts.new_thuebao_cts(
			String s ="begin ? :=admin_v2.pk_thuebao_cts_duongdv.new_thuebao_cts("
				+"'"+pschema+"',"				
				+"'"+pdataschema+"',"
				+"'"+ConvertFont.UtoD(pma_tb)+"',"	
				+"'"+pso_sim+"',"
				+"'"+ploaitb+"',"
				+"'"+pngay_sinh+"',"
				+"'"+ploaigt_id+"',"
				+"'"+ConvertFont.UtoD(pso_gt)+"',"
				+"'"+pngay_cap+"',"
				+"'"+ConvertFont.UtoD(pnoi_cap)+"',"
				+"'"+ConvertFont.UtoD(pten_tb)+"',"
				+"'"+ConvertFont.UtoD(pten_tt)+"',"
				+"'"+ConvertFont.UtoD(pma_nv)+"',"
				+"'"+ConvertFont.UtoD(pma_t)+"',"
				+"'"+ConvertFont.UtoD(pdiachi_ct)+"',"
				+"'"+ConvertFont.UtoD(pdiachi_tt)+"',"	
				+"'"+pdoituong_id+"',"
				+"'"+ConvertFont.UtoD(pma_bc)+"',"
				+"'"+pdiadiemtt_id+"',"
				+"'"+ConvertFont.UtoD(pmst)+"',"
				+"'"+ConvertFont.UtoD(ptaikhoan)+"',"
				+"'"+pchuyenkhoan_id+"',"
				+"'"+ConvertFont.UtoD(pnguoi_gt)+"',"
				+"'"+pnganhang_id+"',"
				+"'"+ConvertFont.UtoD(pghi_chu)+"',"
				+"'"+ConvertFont.UtoD(pemail_bc)+"',"
				+"'"+pinchitiet+"',"
				+"'"+ploaidcbc_id+"',"
				+"'"+pkieubc_id+"',"
				+"'"+ConvertFont.UtoD(pdiachi_bc)+"',"
				+"'"+pinhd_id+"',"
				+"'"+ConvertFont.UtoD(pemail_hd)+"',"
				+"'"+ConvertFont.UtoD(pdienthoai_lh)+"',"
				+"'"+ConvertFont.UtoD(pdiachi_hd)+"',"
				+"'"+pnv_hd+"',"
				+"'"+pquanct+"',"
				+"'"+pphuongct+"',"
				+"'"+pphoct+"',"
				+"'"+psonhact+"',"
				+"'"+pquantt+"',"
				+"'"+pphuongtt+"',"
				+"'"+pphott+"',"
				+"'"+psonhatt+"',"
				+"'"+pquanld+"',"
				+"'"+pphuongld+"',"
				+"'"+pphold+"',"
				+"'"+psonhald+"',"	
				+"'"+psysagent+"',"
				+"'"+pagentcode+"',"				
				+"'"+getUserVar("userID")+"',"
				+"'"+puserip+"',"
				
				// Bo xung (duongdv)
				+ "'" + ps_diachi_tr + "',"
				+ "'" + ps_doituong_sd + "',"
				+ "'" + ps_quoc_tich + "'"
				// End bo xung
				
				+"); end;";
		System.out.println(s);
		System.out.println("Dao Van Duong");
       return this.reqValue("", s);
	}
	public String del_thuebao_cts(
		String pschema,
		String pid,
		String pagent,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=admin_v2.pk_thuebao_cts.del_thuebao_cts("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+pagent+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String regis_ihd_thuebao_cts(
		String pschema,
		String pid,
		String pinhd,
		String pagent,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=admin_v2.pk_thuebao_cts.regis_ihd_msisdn("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+pinhd+"',"
				+"'"+pagent+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String complete_msisdn(
		String pschema,
		String pid,
		String pagent,
		String puserid,
		String puserip) throws Exception {
			// String s ="begin ? :=admin_v2.pk_thuebao_cts.complete_msisdn("
			// String s ="begin ? :=admin_v2.pkg_contact_info.complete_msisdn("
			String s ="begin ? :=admin_v2.pk_thuebao_cts_duongdv.complete_msisdn("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+pagent+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String get_unit(String pschema,
		String pagent,
		String puserid,
		String pbuucuc)throws Exception {
			String s ="begin ? :=admin_v2.pk_thuebao_cts.get_management_unit("
				+"'"+pschema+"',"				
				+"'"+pagent+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+pbuucuc+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String edit_thuebao_cts(
	String pschema,
		String pthuebao_id,
		String pma_tb,
		String pso_sim,
		String ploaitb_id,		
		String pngay_sinh,
		String ploaigt_id,
		String pso_gt,
		String pngay_cap,
		String pnoi_cap,
		String pten_tb,
		String pten_tt,
		String pma_nv,
		String pma_t,
		String pdiachi_ct,
		String pdiachi_tt,
		String pdoituong_id,
		String pma_bc,
		String pdiadiemtt_id,
		String pmst,
		String ptaikhoan,
		String pchuyenkhoan_id,
		String pnguoi_gt,
		String pnganhang_id,
		String pghi_chu,
		String pemail_bc,
		String pinchitiet,
		String ploaidcbc_id,
		String pkieubc_id,
		String pdiachi_bc,
		String pinhd_id,
		String pemail_hd,
		String pdienthoai_lh,
		String pdiachi_hd,
		String pnv_hd,
		String pquanct,
		String pphuongct,
		String pphoct,
		String psonhact,
		String pquantt,
		String pphuongtt,
		String pphott,
		String psonhatt,
		String pquanld,
		String pphuongld,
		String pphold,
		String psonhald,
		String psysagent,
		String pdataschema,			
		String pagentcode,
		String puserid,
		String puserip,
		String ps_diachi_tr,
		String ps_doituong_sd,
		String ps_quoc_tich
		) throws Exception {
			// String s ="begin ? :=admin_v2.pk_thuebao_cts.edit_thuebao_cts("
			String s ="begin ? :=admin_v2.pk_thuebao_cts_duongdv.edit_thuebao_cts("
				+"'"+pschema+"',"
				+"'"+pdataschema+"',"
				+"'"+pthuebao_id+"',"
				+"'"+ConvertFont.UtoD(pma_tb)+"',"
				+"'"+ConvertFont.UtoD(pso_sim)+"',"
				+"'"+ploaitb_id+"',"				
				+"'"+pngay_sinh+"',"
				+"'"+ploaigt_id+"',"
				+"'"+ConvertFont.UtoD(pso_gt)+"',"
				+"'"+pngay_cap+"',"
				+"'"+ConvertFont.UtoD(pnoi_cap)+"',"
				+"'"+ConvertFont.UtoD(pten_tb)+"',"
				+"'"+ConvertFont.UtoD(pten_tt)+"',"
				+"'"+ConvertFont.UtoD(pma_nv)+"',"
				+"'"+ConvertFont.UtoD(pma_t)+"',"
				+"'"+ConvertFont.UtoD(pdiachi_ct)+"',"
				+"'"+ConvertFont.UtoD(pdiachi_tt)+"',"		
				+"'"+pdoituong_id+"',"
				+"'"+ConvertFont.UtoD(pma_bc)+"',"
				+"'"+pdiadiemtt_id+"',"
				+"'"+ConvertFont.UtoD(pmst)+"',"
				+"'"+ConvertFont.UtoD(ptaikhoan)+"',"
				+"'"+pchuyenkhoan_id+"',"
				+"'"+ConvertFont.UtoD(pnguoi_gt)+"',"
				+"'"+pnganhang_id+"',"
				+"'"+ConvertFont.UtoD(pghi_chu)+"',"
				+"'"+ConvertFont.UtoD(pemail_bc)+"',"
				+"'"+pinchitiet+"',"
				+"'"+ploaidcbc_id+"',"
				+"'"+pkieubc_id+"',"
				+"'"+ConvertFont.UtoD(pdiachi_bc)+"',"
				+"'"+pinhd_id+"',"
				+"'"+ConvertFont.UtoD(pemail_hd)+"',"
				+"'"+ConvertFont.UtoD(pdienthoai_lh)+"',"
				+"'"+ConvertFont.UtoD(pdiachi_hd)+"',"
				+"'"+pnv_hd+"',"
				+"'"+pquanct+"',"
				+"'"+pphuongct+"',"
				+"'"+pphoct+"',"
				+"'"+psonhact+"',"
				+"'"+pquantt+"',"
				+"'"+pphuongtt+"',"
				+"'"+pphott+"',"
				+"'"+psonhatt+"',"
				+"'"+pquanld+"',"
				+"'"+pphuongld+"',"
				+"'"+pphold+"',"
				+"'"+psonhald+"',"	
				+"'"+psysagent+"',"
				+"'"+pagentcode+"',"
				+"'"+getUserVar("userID")+"',"
				+"'" + puserip + "',"
				+ "'" + ps_diachi_tr + "',"
				+ "'" + ps_doituong_sd + "',"
				+ "'" + ps_quoc_tich + "'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String get_code_employee(	     
		 String schema 
		,String psDataSchema   	
		,String psDiadiemId
		,String piQuanId 
		,String piphuongId                                         		                      	
		,String piPhoId
		,String piHinhthuc_ck	
  	)throws Exception {
     String s=    "begin ? := "+schema+"PTTB_LAPHD.layds_manv_theophuong("		
		+"'"+psDataSchema+"'"		                               
		+","+psDiadiemId
		+","+piQuanId
		+","+piphuongId		
		+","+piPhoId
		+","+piHinhthuc_ck
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	public String layDSNhanvien(String piPAGEID,String piREC_PER_PAGE,String psTenVT,String psTenNH,String psTinh) 
	{    
		return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_ct/dm_hopdong/frmdm_hopdong_ajax"
			+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
			+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
			+"&"+CesarCode.encode("TenVT")+"="+psTenVT
			+"&"+CesarCode.encode("TenNH")+"="+psTenNH	
			+"&"+CesarCode.encode("matinh")+"="+psTinh
			;		
    }
	
	public String check_change_province(	     
		 String psMatb 
  	)throws Exception {
     String s=    "begin ? := admin_v2.check_debit_older("		
		+"'"+psMatb+"'"		                               
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
}