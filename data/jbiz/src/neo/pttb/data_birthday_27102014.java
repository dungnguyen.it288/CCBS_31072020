package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class data_birthday extends NEOProcessEx {

	public String new_data_birthday(
		String pmsidn,
		String pmonth_birthday,
		String pagent,
		String ptype_promotion,
		String pnote,
		String psuserid,
		String psuserip
		) throws Exception {
			String s ="begin ? :=admin_v2.pkg_data_birthday.new_data_birthday_func("
				+"'"+"admin_v2."+"',"
				+"'"+psuserid   +"',"
				+"'"+psuserip   +"',"
				+"'"+pmsidn+"',"
				+"'"+pmonth_birthday+"',"
				+"'"+pagent+"',"
				+"'"+ptype_promotion+"',"
				+"'"+pnote+"'"				
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_data_birthday(String listId) throws Exception {
		System.out.println("111111111");	
		String s ="begin ? :=admin_v2.pkg_data_birthday.del_data_birthday_func("
				+"'"+"admin_v2."+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+getUserVar("userIP")+"',"				
				+"'"+listId+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	

	public String edit_data_birthday(
		String pmsidn,
		String pmonth_birthday,
		String pagent,
		String ptype_promotion,
		String pnote,
		String pid,
		String psuserid,
		String psuserip) throws Exception {
			String s ="begin ? :=admin_v2.pkg_data_birthday.edit_data_birthday_func("
				+"'"+"admin_v2."+"',"
				+"'"+psuserid+"',"
				+"'"+psuserip+"',"
				+"'"+pmsidn+"',"
				+"'"+pmonth_birthday+"',"
				+"'"+pagent+"',"
				+"'"+ptype_promotion+"',"
				+"'"+pnote+"',"
				+"'"+pid+"'"					
				+"); end;";
		System.out.println(s);
		//return s;
		return this.reqValue("", s);
	}
	public String accept_upload_birthday(	String psUploadId,
											String psAgentCode, 
											String psTypeUpload, 
											String psuserid,
											String psuserip)throws Exception {
			String s ="begin ? :=admin_v2.pkg_data_birthday.accept_upload_birthday_func("				
								+"'"+ psUploadId    +"',"
								+"'"+ psAgentCode   +"',"
								+"'"+ psTypeUpload  +"',"
								+"'"+ psuserid      +"',"
								+"'"+ psuserip      +"'"
								+"); end;";
			System.out.println(s);			
			return this.reqValue("", s);
	}
	public String accept_upload_birthsms(	String psUploadId,
											String psAgentCode, 
											String psuserid,
											String psuserip)throws Exception {
			String s ="begin ? :=admin_v2.pkg_data_birthday.accept_upload_birthsms_func("				
								+"'"+ psUploadId    +"',"
								+"'"+ psAgentCode   +"',"
								+"'"+ psuserid      +"',"
								+"'"+ psuserip      +"'"
								+"); end;";
			System.out.println(s);			
			return this.reqValue("", s);
	}
}