package neo.pttb;
import javax.sql.RowSet;

import neo.ccbs_export;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class upload_package extends NEOProcessEx {

	public NEOCommonData rec_upload_khuyenmai(String psUploadID){
	String s  ="begin ?:= ccs_common.pkg_upload_package.laytt_kiemtra_khuyenmai('"+psUploadID+"'); end;";

	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
	}
	public NEOCommonData value_accept_upload_khuyenmai(String psUploadID){				
		String s=  "begin ?:= admin_v2.pkg_upload_package.accept_upload_khuyenmai("
			+"'"+ psUploadID+"'); end;";			
	     NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("NEOPortalDBMS");
	 	return new NEOCommonData(nei_);
	}
	
	
	public NEOCommonData layds_upload_error(String uploadId, String agent){
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=pttb/upload_package/ds_upload_error&" + CesarCode.encode("uploadId") + "="+uploadId+"&agent="+agent;
		NEOExecInfo nei_ = new NEOExecInfo(url_);
 		nei_.setDataSrc("NEOPortalDBMS");
    	return new NEOCommonData(nei_);
	}	
		
	public String export_dsupload_error(String uploadId) {	  
		  	  	
	    try {
	    	String s = "begin ?:=admin_v2.pkg_upload_package.layds_upload_error(" 
				+ "'" + uploadId + "'"								
				+ ");end;";
	    	
			RowSet rowSet = reqRSet("NEOPortalDBMS",s);
			
			String fileUploadDir = getSysConst("FileUploadDir");
			String ccs = getUserVar("sys_dataschema");
			ccs = ccs.replace(".", "");
			fileUploadDir = fileUploadDir + "Export_DSKM" + "\\" + ccs;
			String fileName = "dskm_upload_error.xls";
			
			Runtime.getRuntime().exec("cmd /C mkdir " + fileUploadDir);
			
			String fileOutput = fileUploadDir + "\\" + fileName;

			ccbs_export.exportToExcel(fileOutput, fileName, rowSet);
			
			String ret = "begin ?:='" + "{fileurl:\"" + fileOutput.replace("\\", "\\\\") + "\",filename:\"" + fileName +"\"}" + "'; end;";
			
			return ret;
		
	    } catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}	

}