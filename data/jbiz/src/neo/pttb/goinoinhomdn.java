package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;
import neo.qltn.ftp;
import neo.qltn.FTPupload;
import javax.sql.RowSet;
import java.io.*;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSetMetaData;
import neo.smartui.common.CesarCode;
import neo.smartui.report.*;

public class goinoinhomdn extends NEOProcessEx {
	public String add_member_group(
		String psMsisnd,
		String psGroupId,
		String psPackageId,		
		String psUserid,
		String psUserip,
		String psAgentCode) throws Exception {
			String s ="begin ? :=admin_v2.pkg_noinhomdn.add_member_group("
				+"'"+psMsisnd+"',"
				+"'"+psGroupId+"',"
				+"'"+psPackageId+"',"
				+"'"+psUserid+"',"
				+"'"+psUserip+"',"
				+"'"+psAgentCode+"'"				
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_member_group(
		String psMsisnd,
		String psGroupId,
		String psPackageId,		
		String psUserid,
		String psUserip,
		String psAgentCode) throws Exception {
			String s ="begin ? :=admin_v2.pkg_noinhomdn.del_member_group("
				+"'"+psMsisnd+"',"
				+"'"+psGroupId+"',"
				+"'"+psPackageId+"',"
				+"'"+psUserid+"',"
				+"'"+psUserip+"',"
				+"'"+psAgentCode+"',"				
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public NEOCommonData rec_upload_khuyenmai(String psUploadID){
	String s  ="begin ?:= admin_v2.pkg_noinhomdn.laytt_kiemtra_nndn('"+psUploadID+"'); end;";

	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	// nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
	}
	
	public NEOCommonData value_accept_upload_khuyenmai(String psUploadID, String use_schema, String com_schema, String psuserip){				
		String s=  "begin ?:= admin_v2.pkg_noinhomdn.accept_upload_nndn("
			+"'"+ psUploadID+"',"	
			+"'"+use_schema+ "'," 				
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";
			
	     NEOExecInfo nei_ = new NEOExecInfo(s);
	
	 	return new NEOCommonData(nei_);
	}
	
	
	
	public NEOCommonData layds_upload_error(String uploadId, String agent){
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=pttb/goinoinhomDN/ds_upload_error&" + CesarCode.encode("uploadId") + "="+uploadId+"&agent="+agent;
		NEOExecInfo nei_ = new NEOExecInfo(url_); 		
    	return new NEOCommonData(nei_);
	}
	

	
}