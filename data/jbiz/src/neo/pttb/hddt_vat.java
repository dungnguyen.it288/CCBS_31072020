package neo.pttb;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.sql.RowSet;

public class hddt_vat extends NEOProcessEx 
{
	public void run() 
	{
		System.out.println("------------------------------------------>Bat dau chay hoa don dien tu VAT<-------------------------------------------");
	}
	
	public String check_in_bn(int id_hddt, String func) {
		String sql = "begin ?:= admin_v2.pkg_hddt_vat.check_in_bn('"+id_hddt+"','"+func+"'); end;";
		return sql;
	}
	
	public String dieuchinh_tien(String id,String sophieu,String tt_hoadon,String sotien,String loaitang) throws Exception {
		 String s=    "begin ?:= admin_v2.pkg_hddt_vat.dieuchinh_tien("
			+"'"+id+"'"
			+",'"+sophieu+"'"
			+",'"+getUserVar("userID")+"'"	
			+",'"+getEnvInfo().getParserParam("sys_userip")+"'"	
			+",'"+getUserVar("sys_agentcode")+"'"
			+",'"+loaitang+"'"
			+",'"+sotien+"'"
			+",'"+tt_hoadon+"'"	
			+");"
			+"end;"
			;
		System.out.println(s);
		return s;
	}
	
	public String thaythe_ttkh(String id,String sophieu, String makh, String tenkh, String diachi, String dtlh, String mst, String quan, String phuong, String pho, String tt_hoadon,String sonha) throws Exception {
		 String s=    "begin ?:= admin_v2.pkg_hddt_vat.thaythe_ttkh("
			+"'"+id+"'"
			+",'"+sophieu+"'"
			+",'"+makh+"'"
			+",'"+ConvertFont.UtoD(tenkh)+"'"
			+",'"+ConvertFont.UtoD(diachi)+"'"			
			+",'"+dtlh+"'"
			+",'"+mst+"'"	
			+",'"+quan+"'"	
			+",'"+phuong+"'"	
			+",'"+pho+"'"	
			+",'"+ConvertFont.UtoD(sonha)+"'"	
			+",'"+getUserVar("userID")+"'"	
			+",'"+getEnvInfo().getParserParam("sys_userip")+"'"	
			+",'"+getUserVar("sys_dataschema")+"'"	
			+",'"+getUserVar("sys_agentcode")+"'"	
			+","+tt_hoadon+""	
			+");"
			+"end;"
			;
		System.out.println(s);
		return s;
	}
	
	public String dieuchinh_ttkh(String id,String sophieu, String makh, String tenkh, String diachi, String dtlh, String mst, String quan, String phuong, String pho, String tt_hoadon,String sonha) throws Exception {
		 String s=    "begin ?:= admin_v2.pkg_hddt_vat.dieuchinh_ttkh("
			+"'"+id+"'"
			+",'"+sophieu+"'"
			+",'"+makh+"'"
			+",'"+ConvertFont.UtoD(tenkh)+"'"
			+",'"+ConvertFont.UtoD(diachi)+"'"			
			+",'"+dtlh+"'"
			+",'"+mst+"'"	
			+",'"+quan+"'"	
			+",'"+phuong+"'"	
			+",'"+pho+"'"	
			+",'"+ConvertFont.UtoD(sonha)+"'"	
			+",'"+getUserVar("userID")+"'"	
			+",'"+getEnvInfo().getParserParam("sys_userip")+"'"	
			+",'"+getUserVar("sys_dataschema")+"'"	
			+",'"+getUserVar("sys_agentcode")+"'"	
			+",'"+tt_hoadon+"'"
			+");"
			+"end;"
			;
		System.out.println(s);
		return s;
	}
	
	public String lay_phieu(String mau, String serial, String sophieu) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/dieuchinh_hddt/search_hddt"
			+"&"+CesarCode.encode("mau")+"="+mau
			+"&"+CesarCode.encode("serial")+"="+serial
			+"&"+CesarCode.encode("phieu")+"="+sophieu
			;
	}
	
	public String lay_dsphieu(String pattern, String serial, String ma_hd, String func, String so_tb) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/dieuchinh_hddt/search_hddt_ajax"
			+"&"+CesarCode.encode("pattern")+"="+pattern
			+"&"+CesarCode.encode("serial")+"="+serial
			+"&"+CesarCode.encode("ma_hd")+"="+ma_hd
			+"&"+CesarCode.encode("func")+"="+func
			+"&"+CesarCode.encode("so_tb")+"="+so_tb
			;
	}
	
	public String lay_serial(String psMau) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/ajax_serial"
			+"&"+CesarCode.encode("mau")+"="+psMau
			;
	}
	
	public String show_hddt(String key, String func) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/hddt_view"
			+"&"+CesarCode.encode("key")+"="+key
			+"&"+CesarCode.encode("loai_hd")+"="+func
			;
	}
	
	public String get_ds_hddt(String ma_kh, String func, String pattern, String serial, String sophieu) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/capnhat/khachhang/view_history_ajax"
			+"&"+CesarCode.encode("ma_kh")+"="+ma_kh
			+"&"+CesarCode.encode("func")+"="+func
			+"&"+CesarCode.encode("pattern")+"="+pattern
			+"&"+CesarCode.encode("serial")+"="+serial
			+"&"+CesarCode.encode("sophieu")+"="+sophieu
			;
	}
	
	public String get_tinhtrang_cd(String key, String func, String agentcode, String userid, String userip) {
		String sql = "begin ?:= admin_v2.pkg_hddt_vat.convert_hddt('"
			+key
			+"','"+func
			+"','"+userid
			+"','"+userip
			+"','"+agentcode
			+"'); end;";
		return sql;
	}
	
	public String dieuchinh_ttkh(String id_hddt, String userid, String agentcode, String userip) {
		String sql = "begin ?:= admin_v2.pkg_hddt_vat.dc_thongtinkh('"
			+id_hddt
			+"','"+userid
			+"','"+agentcode
			+"','"+userip
			+"'); end;";
		return sql;
	}
	
	public String getPortalLink(String key, String agentcode, String userid, String func) {
		String sql = "begin ?:= admin_v2.pkg_hddt_vat.getPortalLink('"+key+"','"+agentcode+"','"+userid+"','"+func+"'); end;";
		return sql;
	}
	
	public String show_hdcd(String idHDDT) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/dieuchinh_hddt/hdcd_view"
			+"&"+CesarCode.encode("idHDDT")+"="+idHDDT
			;
	}
	
	public String view_hddt_history(String msisdn, String func) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/dieuchinh_hddt/search/view_history_ajax"
			+"&"+CesarCode.encode("key")+"="+msisdn
			+"&"+CesarCode.encode("func")+"="+func
			;
	}
	
	public String lay_mau() {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/ajax_mau";
	}
	
	// xuat hoa don dien tu thu tien truoc goi cuoc
	public String xuat_tttgc (String id, String dataschema, String agentcode, String userid, String userip, String action, String id_hddt)
  	{		
		String s="begin ?:=  admin_v2.pkg_hddt_vat.xuat_tttgc('"
							+ dataschema		
							+"','"+agentcode
							+"','"+userid
							+"','"+userip
							+"','"+id
							+"','"+action
							+"','"+id_hddt
							+"');"
							+" end;"
							;       
		System.out.println(s);
		return s;
	}
	
	public String xuat_ttttb (String msisdn, String dataschema, String agentcode, String userid, String userip, String action, String id_hddt)
  	{		
		String s="begin ?:=  admin_v2.pkg_hddt_vat.xuat_ttttb('"		
							+dataschema
							+"','"+userid
							+"','"+userip
							+"','"+msisdn
							+"','"+agentcode
							+"','"+action
							+"','"+id_hddt
							+"');"
							+" end;"
							;       
		System.out.println(s);
		return s;
	}
	
	public String xuat_tphmm (String dataschema, String dsphieu_ids, String userid, String userip, String matinh, String action, String id_hddt)
  	{		
		String s="begin ?:=  admin_v2.pkg_hddt_vat.xuat_tphmm('"		
							+dataschema
							+"','"+dsphieu_ids
							+"','"+userid
							+"','"+userip
							+"','"+matinh
							+"','"+action
							+"','"+id_hddt
							+"');"
							+" end;"
							;       
		System.out.println(s);
		return s;
	}
	
	public String xuat_tpcqsd (String dataschema, String dsphieu_ids, String userid, String userip, String matinh, String action, String id_hddt)
  	{		
		String s="begin ?:=  admin_v2.pkg_hddt_vat.xuat_tpcqsd('"		
							+dataschema
							+"','"+dsphieu_ids
							+"','"+userid
							+"','"+userip
							+"','"+matinh
							+"','"+action
							+"','"+id_hddt
							+"');"
							+" end;"
							;       
		System.out.println(s);
		return s;
	}
	
	public String huy_hddt_manual (String dataschema, String agentcode, String userid, String userip, String id_hddt, String func)
  	{		
		String s="begin ?:=  admin_v2.pkg_hddt_vat.huy_hddt_manual('"		
							+dataschema
							+"','"+agentcode
							+"','"+userid
							+"','"+userip
							+"','"+id_hddt
							+"','"+func
							+"');"
							+" end;"
							;       
		System.out.println(s);
		return s;
	}
	
	public String xuat_tpmuatb (String dataschema, String phieu_id, String ma_hd, String userid, String userip, String matinh, String action, String id_hddt)
  	{		
		String s="begin ?:=  admin_v2.pkg_hddt_vat.xuat_tpmuatb('"		
							+dataschema
							+"','"+phieu_id
							+"','"+ma_hd
							+"','"+userid
							+"','"+userip
							+"','"+matinh
							+"','"+action
							+"','"+id_hddt
							+"');"
							+" end;"
							;       
		System.out.println(s);
		return s;
	}
	
	public String them_mau_serial(
		  String psuserid
		, String psuserip
		, String psmatinh
		, String pstenmau
		, String psserial
		, String psmin_value
		, String psmax_value
		, String psmota
  	) throws Exception {
		 String s=    "begin ? := admin_v2.pkg_hddt_vat.capnhat_mau_serial("	
			+" '"+pstenmau+"'"
			+",'"+psserial+"'"
			+",'"+psmatinh+"'"
			+",'"+psmin_value+"'"
			+",'"+psmax_value+"'"
			+",'"+ConvertFont.UtoD(psmota)+"'"
			+",'"+psuserid+"'"			
			+",'"+psuserip+"'"						
			+");"
			+"end;"
			;
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String them_mau(
		  String psuserid
		, String psuserip
		, String psmatinh
		, String pstenmau
  	) throws Exception {
		 String s=    "begin ? := admin_v2.pkg_hddt_vat.update_pattern("	
			+" '"+pstenmau+"'"
			+",'"+psuserid+"'"
			+",'"+psuserip+"'"
			+",'"+getUserVar("sys_agentcode")+"'"	
			+");"
			+"end;"
			;
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String call_sync(
		  String psuserid
		, String psuserip
		, String psmatinh
  	) throws Exception {
		 String s= "begin ? := admin_v2.pkg_hddt_vat.call_sync("
			+"'"+getUserVar("sys_agentcode")+"'"
			+",'"+psuserid+"'"
			+",'"+psuserip+"'"
			+");"
			+"end;"
			;
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String xoa_mau(
		  String psuserid
		, String psuserip
		, String psid
		, String psmatinh
  	) throws Exception {
		 String s=    "begin ? := admin_v2.pkg_hddt_vat.xoa_mau("	
			+" '"+psid+"'"
			+",'"+psmatinh+"'"	
			+",'"+psuserid+"'"			
			+",'"+psuserip+"'"						
			+");"
			+"end;"
			;
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String xoa_serial(
		  String psuserid
		, String psuserip
		, String pstenmau
		, String psserial
		, String psmatinh
  	) throws Exception {
		 String s=    "begin ? := admin_v2.pkg_hddt_vat.xoa_serial("	
			+" '"+pstenmau+"'"
			+",'"+psserial+"'"
			+",'"+psmatinh+"'"
			+",'"+psuserid+"'"			
			+",'"+psuserip+"'"						
			+");"
			+"end;"
			;
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String create_soap_view_hddt(String fkey, String user_name,String user_pass,String pattern) {
		String request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
				+ "<soapenv:Header/>"
				+ "<soapenv:Body>"
				+ "<tem:getInvViewFkeyNoPayPattern>"
					+ "<tem:fkey>"+fkey+"</tem:fkey>"
					+ "<tem:userName>"+user_name+"</tem:userName>"
					+ "<tem:userPass>"+user_pass+"</tem:userPass>"
					+ "<tem:pattern>"+pattern+"</tem:pattern>"
				+ "</tem:getInvViewFkeyNoPayPattern>"
			    + "</soapenv:Body>"
			+ "</soapenv:Envelope>";
		return request;
	}
}
