package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class khuyenmai extends NEOProcessEx 
{
	public void run() 
	{
		System.out.println("neo.khuyen_mai was called");
	}

	public  String laypromotion(String dispatch)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/promotion/subrecovery/promotion_config" +"&"+CesarCode.encode("dispatch") + "=" + dispatch;
	}
	public  String lay_loaitb(String dispatch)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/manage_equipment/bg_thietbi/loaitb_config" +"&"+CesarCode.encode("dispatch") + "=" + dispatch;
	}
	public  String lay_tienthem(String tienthem)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/manage_equipment/bg_thietbi/tienthem_config" +"&"+CesarCode.encode("tienthem") + "=" + tienthem;
	}
	////
	public  String laydatcoc(String dispatch)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/manage_equipment/bg_thietbi/datcoc_config" +"&"+CesarCode.encode("dispatch") + "=" + dispatch;
	}
	////
	
	public String regPro_tb(  
							String dataschema,  	
							String agentcode,  											
							String userid,
							String loaitb,
							String tienthem,
							String congvan,
							String datcoc,
							String chiphi,
							String matb,
							String ghichu)
  	{		
    String s=    "begin ?:=  admin_v2.pkg_qltb_new.reg_pro_tb('"
							+ dataschema		
							+"','"+agentcode
							+"','"+userid
							+"','"+loaitb
							+"','"+tienthem
							+"','"+congvan
							+"','"+datcoc
							+"','"+chiphi
							+"','"+matb
							+"','"+ghichu
							+"');"
							+" end;"
							;        
    System.out.println(s); 		
	 return s;  
  }
  ///
   /*public String del_tb(  String dataschema,  	
							String agentcode,  											
							String userid,
							String id)
  	{		
    String s=    "begin ?:=  admin_v2.pkg_qltb_new.del_tb('"
							+ dataschema		
							+"','"+agentcode
							+"','"+userid
							+"','"+id
							+"');"
							+" end;"
							;        
    System.out.println(s); 		
	 return s;  
  }
   ///
  public String check_tb(  String dataschema,  	
							String agentcode,  											
							String userid,
							String congvan,
							String loaitb,
							String datcoc,
							String matb)
  	{		
    String s=    "begin ?:=  admin_v2.pkg_qltb_new.check_dk_km('"
							+ dataschema		
							+"','"+agentcode
							+"','"+congvan
							+"','"+loaitb
							+"','"+datcoc
							+"','"+matb
							+"','"+userid
							+"');"
							+" end;"
							;        
    System.out.println(s); 		
	 return s;  
  }
  ///
  

   public String getLst_tb( String matb)
	{
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=pttb/manage_equipment/bg_thietbi/ajax_subrecovery&" + CesarCode.encode(" matb") + "=" + matb;
		return url_;
	}
	public  String lay_danhmuc()
	{
		return "/main?"+CesarCode.encode("configFile")+"=code/category/danhmuc_config";
	}
	*/
	public String del_tb(  String dataschema,  	
							String agentcode,  											
							String userid,
							String id)
  	{		
    String s=    "begin ?:=  admin_v2.pkg_qltb_new.del_tb('"
							+ dataschema		
							+"','"+agentcode
							+"','"+userid
							+"','"+id
							+"');"
							+" end;"
							;        
    System.out.println(s); 		
	 return s;  
  }
   ///
  public String check_tb(  String dataschema,  	
							String agentcode,  											
							String userid,
							String congvan,
							String loaitb,
							String datcoc,
							String matb)
  	{		
    String s=    "begin ?:=  admin_v2.pkg_qltb_new.check_dk_km('"
							+ dataschema		
							+"','"+agentcode
							+"','"+congvan
							+"','"+loaitb
							+"','"+datcoc
							+"','"+matb
							+"','"+userid
							+"');"
							+" end;"
							;        
    System.out.println(s); 		
	 return s;  
  }
  ///
   public String getLst_tb( String matb)
	{
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=pttb/manage_equipment/bg_thietbi/ajax_subrecovery&" + CesarCode.encode(" matb") + "=" + matb;
		return url_;
	}
	public  String lay_danhmuc()
	{
		return "/main?"+CesarCode.encode("configFile")+"=code/category/danhmuc_config";
	}
	///
   public String getLst_tb_log( String matb)
	{
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=pttb/manage_equipment/bg_thietbi/ajax_subrecovery_log&" + CesarCode.encode(" matb") + "=" + matb;
		return url_;
	}
	////
	public String updatePro_tb(  
							String dataschema,  	
							String agentcode,  											
							String userid,
							String id_tb,
							String loaitb,
							String tienthem,
							String congvan,
							String datcoc,
							String chiphi,
							String matb,
							String ghichu)
  	{		
    String s=    "begin ?:=  admin_v2.pkg_qltb_new.update_pro_tb('"
							+ dataschema		
							+"','"+agentcode
							+"','"+userid
							+"','"+id_tb
							+"','"+loaitb
							+"','"+tienthem
							+"','"+congvan
							+"','"+datcoc
							+"','"+chiphi
							+"','"+matb
							+"','"+ghichu
							+"');"
							+" end;"
							;        
    System.out.println(s); 		
	 return s;  
  }
	 ////
	public String regProkm
	(
		String dataschema,  	
		String agentcode,  											
		String userid,
		String promotion,
		String htkm,
		String matb,
		String description
	)
  	{
		String s= "begin ?:= admin_v2.pkg_goi.reg_pro_km('"+dataschema+"','"+agentcode+"','"+userid+"','"+promotion+"','"+htkm+"','"+matb+"','"+description+"'"+");" +" end;"  ;        
		System.out.println(s); 		
		return s;  
	}
	
	 public String getLstProRegHis
	(
		String matb
	)
	{
		return "/main?"+CesarCode.encode("configFile")+"=PTTB/promotion/subrecovery/ajax_subrecovery" +"&"+CesarCode.encode("matb") + "=" + matb;
	}
	
	public String chkProReg
	(
		String dataschema,  
		String agentcode,  
		String userid,
		String matb,
		String dotkm,
		String htkm		
	)
  	{
		String s= "begin ?:= admin_v2.pkg_goi.chk_pro_reg_km('"+dataschema+"', '"+agentcode+"', '"+userid+"', '"+matb+"', '"+dotkm+"', '"+htkm+"'"+");  end;";        
		
		System.out.println(s);
		return s; 
	}

}

