package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class dangkygoinhom
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb_laphd was called");
  }


  
   public String layDSGoiNhom(
		 String psSOMAY                         
		
  	) {
	 String sUserId= getUserVar("userID");
	 String sAgentCode = getUserVar("sys_agentcode");
	 String sDataSchema = getUserVar("sys_dataschema");
	 String sFuncSchema = getSysConst("FuncSchema");
	 String sCommonSchema = getSysConst("CommonSchema");
	 
     String s=    "begin ? := "+sFuncSchema+"PKG_DANGKYGOINHOM.layds_goi_nhom("		
		+"'"+sFuncSchema+"'"
		+",'"+sCommonSchema+"'"                                  
		+",'"+psSOMAY+"'"                                  
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
  
     public String themMoiNhom(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String sCommonSchema
	 
		, String tenNhom
		, String dsmay 
		, String loaiNhom 
		, String ngayDK 
		, String ghiChu
		, String capnhat
		, String chuNhom 
		, String pikhuyenmai
		, String pikhuyenmai_cn
  	) {
	 //String sUserId= getUserVar("userID");
	 //String sAgentCode = getUserVar("sys_agentcode");
	 //String sDataSchema = getUserVar("sys_dataschema");
	 //String sFuncSchema = getSysConst("FuncSchema");
	 //String sCommonSchema = getSysConst("CommonSchema");
	 
     String s=    "begin ? := "+sFuncSchema+"PKG_DANGKYGOINHOM.them_moi_nhom("		
	 
		+" '"+sFuncSchema+"'"
		+",'"+sCommonSchema+"'"                                  
		+",'"+sUserId+"'"                                  
		+",'"+sAgentCode+"'"                                  
		
		+",'"+tenNhom+"'"
		+",'"+dsmay+"'"
		+",'"+loaiNhom+"'"
		+",'"+ngayDK+"'"
		+",'"+ghiChu+"'"		
		+",'"+capnhat+"'"
		+",'"+chuNhom+"'"
		+",'"+pikhuyenmai+"'"
		+",'"+pikhuyenmai_cn+"'"
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
	
	public String capNhatNhom(
    	  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String sCommonSchema
		
		,String	tenNhom
		,String	idNhom 
		,String	loaiNhom 
		,String	ngayDK 
		,String dsMayThem 
		,String	ghiChu
		, String chuNhom 
  	) {
	// String sUserId= getUserVar("userID");
	// String sAgentCode = getUserVar("sys_agentcode");
	// String sDataSchema = getUserVar("sys_dataschema");
	// String sFuncSchema = getSysConst("FuncSchema");
	// String sCommonSchema = getSysConst("CommonSchema");
	 
     String s=    "begin ? := "+sFuncSchema+"PKG_DANGKYGOINHOM.cap_nhat_nhom("		
	 
		+" '"+sFuncSchema+"'"
		+",'"+sCommonSchema+"'"                                  
		+",'"+sUserId+"'"                                  
		+",'"+sAgentCode+"'"                                  
		
		+",'"+tenNhom+"'"
		+",'"+idNhom+"'"
		+",'"+loaiNhom+"'"
		+",'"+ngayDK+"'"
		+",'"+dsMayThem+"'"
		+",'"+ghiChu+"'"		
		+",'"+chuNhom+"'"	
		
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
  
   public String tachNhom(
		 String	sSomay, String sLoaiNhom
		
  	) {
	 String sUserId= getUserVar("userID");
	 String sAgentCode = getUserVar("sys_agentcode");
	 String sDataSchema = getUserVar("sys_dataschema");
	 String sFuncSchema = getSysConst("FuncSchema");
	 String sCommonSchema = getSysConst("CommonSchema");
	 
     String s=    "begin ? := "+sFuncSchema+"PKG_DANGKYGOINHOM.tach_nhom("	
	 	+" '"+sFuncSchema+"'"
		+",'"+sCommonSchema+"'"                                  
		+",'"+sUserId+"'"                                  
		+",'"+sAgentCode+"'"                                  
		+",'"+sSomay+"'"
		+",'"+sLoaiNhom+"'"
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
   
   
  public String layDSTachNhom(String somay){
 	
 	 return "/main?"+CesarCode.encode("configFile")+"=PTTB/laphd/dangkygoinhom/dsthuebao_ajax"
		+"&"+CesarCode.encode("somay")+"="+somay;
 }
	public String themMoi_dn(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String	dsmay 
		, String	ngayDK 
		, String	ghiChu
		, String capnhat
		, String chuNhom 
  	) {

	 
     String s=    "begin ? := "+sFuncSchema+"PKG_DANGKYGOINHOM.themmoi_nhom_doanhnghiep("		
	 
		+" '"+sDataSchema+"'"                                
		+",'"+sUserId+"'"                                  
		+",'"+sAgentCode+"'"                                  
		+",'"+dsmay+"'"
		+",'"+ngayDK+"'"
		+",'"+ghiChu+"'"		
		+",'"+capnhat+"'"
		+",'"+chuNhom+"'"	
		+");"
        +"end;"
        ;
		System.out.println("Test :" +s);
    return s;
	}

	
	 public String layLSGoiNhom(String so_tb){
 	
 	 return "/main?"+CesarCode.encode("configFile")+"=PTTB/laphd/dangkygoinhom/lichsu_ajax"
		+"&"+CesarCode.encode("so_tb")+"="+so_tb;
 }
	public String layDSGoiNhom_dn(
		 String psSOMAY                         
		
  	) {
	 String sDataSchema = getUserVar("sys_dataschema");
	 String sFuncSchema = getSysConst("FuncSchema"); 
     String s=    "begin ? := "+sFuncSchema+"PKG_DANGKYGOINHOM.layds_nhom_dn("	
		+"'"+sDataSchema+"'"
		+",'"+psSOMAY+"'"                               
		+");"
        +"end;"
        ;
		System.out.println("luattd :" +s);
    return s;
	}
     public String nhom_doanvien(
		  String sUserId
		, String sUserIP
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String sCommonSchema
		, String dsmay
		, String loaiNhom 
		, String ngayDK 
		, String ghiChu
		, String capnhat
		, String chuNhom
		, String idNhom		
  	) {
	 
     String s=    "begin ? := admin_v2.pkg_doanvien.dk_nhom_doanvien("			 
		+" '"+sFuncSchema+"'"
		+",'"+sCommonSchema+"'"                                  
		+",'"+sUserId+"'"    
		+",'"+sUserIP+"'"		
		+",'"+sAgentCode+"'"                                  

		+",'"+dsmay+"'"
		+",'"+loaiNhom+"'"
		+",'"+ngayDK+"'"
		+",'"+ghiChu+"'"		
		+",'"+capnhat+"'"
		+",'"+chuNhom+"'"	
		+",'"+idNhom+"'"		
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
     public String huy_nhom_doanvien(
		  String sUserId
		, String sUserIP
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String sCommonSchema
		, String idNhom		
  	) {
	 
     String s=    "begin ? := admin_v2.pkg_doanvien.huy_nhom_doanvien("			 
		+" '"+sFuncSchema+"'"
		+",'"+sCommonSchema+"'"                                  
		+",'"+sUserId+"'"    
		+",'"+sUserIP+"'"		
		+",'"+sAgentCode+"'"	
		+",'"+idNhom+"'"		
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
	public String layDSnhom_doanvien(
		 String psSOMAY
  	) {
	 String sUserId= getUserVar("userID");
	 String sAgentCode = getUserVar("sys_agentcode");
	 String sDataSchema = getUserVar("sys_dataschema");
	 String sFuncSchema = getSysConst("FuncSchema");
	 String sCommonSchema = getSysConst("CommonSchema");
	 
     String s=    "begin ? := admin_v2.pkg_doanvien.layds_nhom_doanvien("		
		+"'"+sFuncSchema+"'"
		+",'"+sCommonSchema+"'"                                  
		+",'"+psSOMAY+"'"
		+",'"+sAgentCode+"'"
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
	public String layLSnhom_doanvien(String so_tb){
 	
 	 return "/main?"+CesarCode.encode("configFile")+"=PTTB/laphd/nhomdoanvien/lichsu_ajax"
		+"&"+CesarCode.encode("so_tb")+"="+so_tb;
	}	
	
}














