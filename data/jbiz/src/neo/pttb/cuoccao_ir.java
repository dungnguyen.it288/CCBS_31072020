package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class cuoccao_ir extends NEOProcessEx {
	public String new_cuoccao_ir(
		String pmsisdn,
		String pbl_id,
		String psms,
		String plock,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_cuoccao.new_cuoccao_ir("
				+"'"+ConvertFont.UtoD(pmsisdn)+"',"
				+"'"+pbl_id+"',"
				+"'"+psms+"',"
				+"'"+plock+"',"
				+"'"+getUserVar("sys_agentcode")+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	
	public String update_cuoccao_ir(
		String pmsisdn,
		String pbl_id,
		String psms,
		String plock,		
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_cuoccao.update_cuoccao_ir("
				+"'"+ConvertFont.UtoD(pmsisdn)+"',"
				+"'"+pbl_id+"',"
				+"'"+psms+"',"
				+"'"+plock+"',"				
				+"'"+getUserVar("sys_agentcode")+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	
	public String delete_cuoccao_ir(
		String pmsisdn,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_cuoccao.delete_cuoccao_ir("
				+"'"+ConvertFont.UtoD(pmsisdn)+"',"
				+"'"+getUserVar("sys_agentcode")+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}	
}