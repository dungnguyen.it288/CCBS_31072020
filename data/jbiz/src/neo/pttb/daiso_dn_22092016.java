package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;
public class daiso_dn extends NEOProcessEx{
	
	public String capnhat_phanquyen(
			  String ps_schema
			, String ps_idnhom
			, String ps_daisos
			, String ps_userid
			, String ps_note
			, String ps_agentcode
			, String ps_user_cn
			, String ps_ip_cn
	  	) throws Exception {
		 
	     String s=    "begin ? := "+ps_schema+"pkg_daiso_dn.capnhat_phanquyen("	
			+"'"+ps_schema+"'"
			+",'"+ps_idnhom+"'"
			+",'"+ps_daisos+"'"                           
			+",'"+ps_userid+"'"         
			+",'"+ps_note+"'"  
			+",'"+ps_agentcode+"'"
			+",'"+ps_user_cn+"'"		
			+",'"+ps_ip_cn+"'"				
			+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
	public String xoa_phanquyen(
			  String ps_schema
			, String ps_idnhom
			, String ps_user_cn
			, String ps_ip_cn
	  	) throws Exception {
		 
	     String s=    "begin ? := "+ps_schema+"pkg_daiso_dn.xoa_phanquyen("	
			+"'"+ps_schema+"'"
			+",'"+ps_idnhom+"'"
			+",'"+ps_user_cn+"'"		
			+",'"+ps_ip_cn+"'"				
			+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
}
