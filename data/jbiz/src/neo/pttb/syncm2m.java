package neo.pttb;
import neo.smartui.process.*;
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.*;
import javax.sql.*;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import java.util.concurrent.TimeUnit;

public class syncm2m extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("SyncM2M");		
	}
	
	public String sync_subscriber_m2m(String psMsisnd, String psUserId, String psUserIp) throws Exception{
		
		String sHost="10.156.7.9";
		String sPort="8088";
		String sMethod="core/synchronize/SynchronizeMsimInfoUpdate";
		String s= "begin ? :=admin_v2.pkg_sync_m2m.sync_sub_m2m('"
			 + psMsisnd     +"','"			 
			 + psUserId  	+"','"
			 + psUserIp
			 +"');"
			 +"end;"
			;
		String res_ =  this.reqValue("", s);
       
		String[] param =res_.split("\\|");
		if (param[0].equals("1")) {
			
			String xml = postJson(param[1],sHost,sPort,sMethod);
			
			String s= "begin ? :=admin_v2.pkg_sync_m2m.add_log_sync('"
			 + psMsisnd     +"','"			 
			 + param[1]  	+"','"
			 + xml
			 +"');"
			 +"end;"
			;
			res_ =  this.reqValue("", s);
			if (xml.equals("1|SUCCESS")) {
				return "1";
			}
			else if (xml.indexOf("0|Msisdn not exist") >=0) 
			{
				return "Thue bao chua duoc tao tren he thong M2M";
			}
			else {
				return "Co loi trong qua trinh ket noi den he thong M2M";
			}
			
		}
		else  return  res_;
	}
	
	public String upload_m2m_msisdn(String psUploadId, String psagentcode, String psUserId, String psUserIp) {
		System.out.println("---------------------------da vao");
		String sHost="10.156.7.9";
		String sPort="8088";
		String sMethod="core/synchronize/SynchronizeMsimInfoUpdate";
		String s= "begin ? :=admin_v2.pkg_sync_m2m.get_data_sync('"
			 + psUploadId     +"','"
			 + psagentcode
			 +"');"
			 +"end;"
			;
		System.out.println(s);
		String s_="";
		try{
			RowSet rowSet = this.reqRSet(s);
            while (rowSet.next()) {                
                String json = rowSet.getString("data_m2m");
				System.out.println(json);
				String sub_ = 	rowSet.getString("msisdn");
				String xml = postJson(json,sHost,sPort,sMethod);
				System.out.println(xml);
				s= "begin ? :=admin_v2.pkg_sync_m2m.add_log_sync('"
					 + sub_     +"','"			 
					 + json  	+"','"
					 + xml
					 +"');"
					 +"end;"
					;
				String res_  =  this.reqValue("", s);
				
				if (!xml.equals("1|SUCCESS")) {
					s_="begin ? :=admin_v2.pkg_sync_m2m.update_msisdn_err('"
					 + psUploadId  	+"','"
					 + sub_  +"');"
					 +"end;"
					;
					res_  =  this.reqValue("", s_);
				}
				
				
			}
			return "1";			
		}catch(Exception e){
			return "Co loi khi thuc hien "+e.getMessage();
		}
		
       
		
	}	
	public static String postJson( String Request,String Host,String Port,String method)
    {
        StringBuffer buf = new StringBuffer();
        try {        
            URL u = new URL("http://"+Host+":"+Port+"/"+method);
            HttpURLConnection con = (HttpURLConnection)u.openConnection();
            con.setConnectTimeout(5000); 
            
            con.setRequestProperty("Content-Type","application/json;charset=utf-8");        
            con.setDoOutput(true);
            con.setDoInput(true);
            con.connect();
            
            OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
            writer.write(Request);
            writer.flush();
            BufferedReader  reader ;
            if(con.getResponseCode() == 200){
              reader = new BufferedReader(new
              InputStreamReader(con.getInputStream()));
            }else{
              reader = new BufferedReader(new
              InputStreamReader(con.getErrorStream()));
            }            
            String line ;
            while( (line = reader.readLine()) != null )
            buf.append(line);
            
            reader.close();
            return buf.toString();            
        }catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
	
}