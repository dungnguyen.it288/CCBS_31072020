package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class helpdesk extends NEOProcessEx {
	public String new_role(		
		String pschema,
		String puserid,
		String puserip,
		String pListUser,
		String prole,
		String pdept) throws Exception {
			String s ="begin ? :="+pschema+"pkg_helpdesk.new_role_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+pListUser+"',"				
				+"'"+prole+"',"
				+"'"+pdept+"'"									
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String update_role(		
		String pschema,
		String puserid,
		String puserip,
		String puser_id,
		String prole,
		String pdept) throws Exception {
			String s ="begin ? :="+pschema+"pkg_helpdesk.update_role_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+puser_id+"',"				
				+"'"+prole+"',"
				+"'"+pdept+"'"									
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	
	public String update_role_detail(		
		String pschema,
		String puserid,
		String puserip,
		String puser_id,
		String proles) throws Exception {
			String s ="begin ? :="+pschema+"pkg_helpdesk.update_role_detail_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+puser_id+"',"				
				+"'"+proles+"'"									
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}

	public String delete_role(		
		String pschema,
		String puserid,
		String puserip,
		String puser_id) throws Exception {
			String s ="begin ? :="+pschema+"pkg_helpdesk.delete_role_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+puser_id+"'"							
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String delete_all_role(		
		String pschema,
		String puserid,
		String puserip,
		String proles) throws Exception {
			String s ="begin ? :="+pschema+"pkg_helpdesk.delete_all_role_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+proles+"'"							
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_upload_file(		
		String psdatachema,
		String puserid,
		String puploadid) throws Exception {
			String s ="begin ? :=admin_v2.pkg_complaint.del_file_upload("
				+"'"+puploadid+"',"
				+"'"+puserid+"',"
				+"'"+psdatachema+"'"											
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	} 
	public String assign_complaint(		
		String psdatachema,
		String puserid,
		String puserip,
		String plistcode,
		String passigner,
		String pcontent,
		String pNote,
		String pListUpload) throws Exception {
			String s ="begin ? :=admin_v2.pkg_complaint.assign_complaint_func("
				+"'"+plistcode+"',"
				+"'"+passigner+"',"
				+"'"+ConvertFont.decodeW1252(pcontent)+"',"
				+"'"+ConvertFont.decodeW1252(pNote)+"',"
				+"'"+pListUpload+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"
				+"'"+psdatachema+"'"											
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	} 
	public String resolve_complaint(		
		String psdatachema,
		String puserid,
		String puserip,
		String plistcode,
		String pcontent,
		String pNote,
		String pListUpload) throws Exception {
			String s ="begin ? :=admin_v2.pkg_complaint.resolve_complaint_func("
				+"'"+ConvertFont.decodeW1252(pcontent)+"',"
				+"'"+ConvertFont.decodeW1252(pNote)+"',"
				+"'"+pListUpload+"',"
				+"'"+plistcode+"',"				
				+"'"+puserid+"',"
				+"'"+puserip+"',"
				+"'"+psdatachema+"'"											
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	} 
	public String delete_complaint(		
		String psdatachema,
		String puserid,
		String puserip,
		String plistcode) throws Exception {
			String s ="begin ? :=admin_v2.pkg_complaint.del_complaint_func("
				+"'"+plistcode+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"
				+"'"+psdatachema+"'"											
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	} 
	//tiep nhan
	public String new_reception(		
		String pschema,
		String puserid,
		String puserip,
		String pagent,
		String pmsisdn,
		String pcustomer,
		String pemail,
		String pmobile,
		String pemailDGV,
		String pmobileDGV,
		String pname,
		String paddress,
		String ptype_id,
		String pforms_id,
		String ppriority,
		String pstart_time,
		String pend_time,
		String passigner,		
		String pcontent,
		String ppublic_start,
		String ppublic_end,
		String pagent_public,
		String pdescription,
		String pstatus,
		String pupload_id) throws Exception {
			String s ="begin ? :="+pschema+"pkg_complaint.new_complaint_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+pagent+"',"				
				+"'"+pmsisdn+"',"				
				+"'"+pcustomer+"',"
				+"'"+pemail+"',"
				+"'"+pmobile+"',"				
				+"'"+pemailDGV+"',"				
				+"'"+pmobileDGV+"',"				
				+"'"+ConvertFont.decodeW1252(pname)+"',"
				+"'"+ConvertFont.decodeW1252(paddress)+"',"
				+"'"+ptype_id+"',"
				+"'"+pforms_id+"',"
				+"'"+ppriority+"',"
				+"'"+pstart_time+"',"
				+"'"+pend_time+"',"
				+"'"+passigner+"',"				
				+"'"+ConvertFont.decodeW1252(pcontent)+"',"
				+"'"+ppublic_start+"',"
				+"'"+ppublic_end+"',"
				+"'"+pagent_public+"',"		
				+"'"+ConvertFont.decodeW1252(pdescription)+"',"											
				+"'"+pstatus+"',"											
				+"'"+pupload_id+"'"											
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String update_reception(		
		String pschema,
		String puserid,
		String puserip,
		String pagent,
		String pmsisdn,
		String pcustomer,
		String pemail,
		String pmobile,
		String pemailDGV,
		String pmobileDGV,
		String pcode,
		String pname,
		String paddress,
		String ptype_id,
		String pforms_id,
		String ppriority,
		String pstart_time,
		String pend_time,
		String passigner,		
		String pcontent,
		String ppublic_start,
		String ppublic_end,
		String pagent_public,
		String pdescription,
		String pstatus,
		String pupload_id,
		String pid) throws Exception {
			String s ="begin ? :="+pschema+"pkg_complaint.update_complaint_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"					
				+"'"+pagent+"',"					
				+"'"+pmsisdn+"',"				
				+"'"+pcustomer+"',"
				+"'"+pemail+"',"
				+"'"+pmobile+"',"				
				+"'"+pemailDGV+"',"				
				+"'"+pmobileDGV+"',"				
				+"'"+pcode+"',"				
				+"'"+ConvertFont.decodeW1252(pname)+"',"
				+"'"+ConvertFont.decodeW1252(paddress)+"',"
				+"'"+ptype_id+"',"
				+"'"+pforms_id+"',"
				+"'"+ppriority+"',"
				+"'"+pstart_time+"',"
				+"'"+pend_time+"',"
				+"'"+passigner+"',"				
				+"'"+ConvertFont.decodeW1252(pcontent)+"',"
				+"'"+ppublic_start+"',"
				+"'"+ppublic_end+"',"
				+"'"+pagent_public+"',"		
				+"'"+ConvertFont.decodeW1252(pdescription)+"',"										
				+"'"+pstatus+"',"										
				+"'"+pupload_id+"',"										
				+"'"+pid+"'"										
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String delete_complaint_reception(		
		String pschema,
		String puserid,
		String puserip,
		String pid) throws Exception {
			String s ="begin ? :="+pschema+"pkg_complaint.delete_conplaint_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+pid+"'"							
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String check_user(		
		String pschema,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+pschema+"pkg_complaint.check_user_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"									
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String get_code(		
		String pschema,
		String puserid,
		String puserip,
		String pagent) throws Exception {
			String s ="begin ? :="+pschema+"pkg_complaint.get_code_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+pagent+"'"							
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String update_priority(		
		String pschema,
		String puserid,
		String puserip,
		String plistId,
		String plistValue) throws Exception {
			String s ="begin ? :="+pschema+"pkg_helpdesk.update_priority("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+plistId+"',"
				+"'"+plistValue+"'"									
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String new_complaint_type(
		String pschema,
		String pcontent,
		String pcode,
		String pstatus,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=admin_v2.pkg_complaint.new_complaint_type("
				+"'"+pschema+"',"
				+"'"+ConvertFont.decodeW1252(pcontent)+"',"
				+"'"+pcode+"',"
				+"'"+pstatus+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_complaint_type(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=admin_v2.pkg_complaint.del_complaint_type("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_complaint_type(
	String pschema,
		String pid,
		String pcontent,
		String pcode,
		String pstatus,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=admin_v2.pkg_complaint.edit_complaint_type("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+ConvertFont.decodeW1252(pcontent)+"',"
				+"'"+pcode+"',"
				+"'"+pstatus+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	//error_log
	public String new_error_log(
		String pschema,
		String puserid,
		String puserip,
		String perror_date,		
		String perror_type,
		String powner,
		String pcontent,
		String pnote) throws Exception {
			String s ="begin ? :="+pschema+"pkg_complaint.new_error_log_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"
				+"'"+perror_date+"',"				
				+"'"+perror_type+"',"
				+"'"+powner+"',"
				+"'"+ConvertFont.decodeW1252(pcontent)+"',"
				+"'"+ConvertFont.decodeW1252(pnote)+"'"		
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_error_log(
		String pschema,		
		String puserid,
		String puserip,
		String pid) throws Exception {
			String s ="begin ? :="+pschema+"pkg_complaint.del_error_log_func("
				+"'"+pschema+"',"				
				+"'"+puserid+"',"
				+"'"+puserip+"',"
				+"'"+pid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_error_log(
		String pschema,
		String puserid,
		String puserip,
		String pid,
		String perror_date,		
		String perror_type,
		String powner,
		String pcontent,
		String pnote) throws Exception {
			String s ="begin ? :="+pschema+"pkg_complaint.edit_error_log_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"
				+"'"+pid+"',"
				+"'"+perror_date+"',"				
				+"'"+perror_type+"',"
				+"'"+powner+"',"
				+"'"+ConvertFont.decodeW1252(pcontent)+"',"
				+"'"+ConvertFont.decodeW1252(pnote)+"'"				
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	//giai phap kho
	 public String insert_solution(String tieude,String noidung,String yeucau,String userid,String userip,String agent,String idupload)
    {
        System.out.println("kkkk");
        String result="";
            String s="begin ? :=admin_v2.pkg_complaint.new_complaint_solution("
                                                            +"'"+tieude+"'"
                                                            +",'"+noidung+"'"
                                                            +",'"+yeucau+"'"
                                                            +",'"+userid+"'"
                                                            +",'"+userip+"'"
															+",'"+agent+"'"
															+",'"+idupload+"'"
                                                            +");"+"end;";
             try {
            result = reqValue(s);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
            return result;
    }
	public String update_solution(String tieude,String noidung,String yeucau,String userid,String userip,String code,String idupload,String agent,String idsl)
    {
        System.out.println("kkkk");
        String result="";
            String s="begin ? :=admin_v2.pkg_complaint.edit_complaint_solution("
                                                            +"'"+tieude+"'"
                                                            +",'"+noidung+"'"
                                                            +",'"+yeucau+"'"
                                                            +",'"+userid+"'"
                                                            +",'"+userip+"'"
															+",'"+code+"'"
															+",'"+idupload+"'"
															+",'"+agent+"'"
															+",'"+idsl+"'"
                                                            +");"+"end;";
             try {
            result = reqValue(s);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
            return result;
    }
	
	public String remove_solution(String idsl,String codefile)
    {
        System.out.println("kkkk");
        String result="";
            String s="begin ? :=admin_v2.pkg_complaint.delete_complaitn_solution("
															+"'"+idsl+"'"
															+",'"+codefile+"'"
                                                            +");"+"end;";
             try {
            result = reqValue(s);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
            return result;
    }	
}