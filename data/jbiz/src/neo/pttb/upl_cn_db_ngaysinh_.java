
package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;



public class upl_cn_db_ngaysinh extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb.upl_cn_db_ngaysinh was called");
  }
	public String rec_tonghop( String upload_id) 
 		{
		String s= "select count(1)-count(kiemtra) hople,count(1) tong from ccs_common.upload_capnhat_ngaysinh a where a.upload_id = "+upload_id;
        return s;
  }
	public String rec_tonghop_blacklist( String upload_id) 
 		{
		String s= "select count(1)-count(kiemtra) hople,count(1) tong from ccs_common.upload_blacklist a where a.upload_id = "+upload_id;
        return s;
  }
  public String doc_hople(
	String dataschema, 
			String upload_id)
  	{ 
	 	String url_="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=upload/capnhat_db_ngaysinh/ajax_hople";
		url_ = url_ + "&"+CesarCode.encode("data_schema")+"="+dataschema;	
		url_ = url_ + "&"+CesarCode.encode("upload_id")+"="+upload_id;	    
	  return url_;
  	}
	public String doc_hople_blacklist(
	String dataschema, 
			String upload_id)
  	{ 
	 	String url_="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=upload/capnhat_ds_blacklist/ajax_hople";
		url_ = url_ + "&"+CesarCode.encode("data_schema")+"="+dataschema;	
		url_ = url_ + "&"+CesarCode.encode("upload_id")+"="+upload_id;	    
	  return url_;
  	}
	public String doc_khonghople(
   			String dataschema,
			String upload_id)
  	{ 
		String url_="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=upload/capnhat_db_ngaysinh/ajax_khonghople";	
		url_ = url_ + "&"+CesarCode.encode("data_schema")+"="+dataschema;	
		url_ = url_ + "&"+CesarCode.encode("upload_id")+"="+upload_id;	    
		return url_;		  
  	}
	public String doc_khonghople_blacklist(
   			String dataschema,
			String upload_id)
  	{ 
		String url_="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=upload/capnhat_ds_blacklist/ajax_khonghople";	
		url_ = url_ + "&"+CesarCode.encode("data_schema")+"="+dataschema;	
		url_ = url_ + "&"+CesarCode.encode("upload_id")+"="+upload_id;	    
		return url_;		  
  	}
	public String capnhat_db_ngaysinh(
		String dataschema,
		String funcschema,
		String upload_id		
	)
	{ 
		return "begin ? :=" + funcschema + "pkg_upload.capnhat_db_ngaysinh("
			+"'"+upload_id+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";	
	}
  
	public String rec_tonghop_vnpt( String upload_id) 
 		{
		String s= "select count(1)-count(kiemtra) hople,count(1) tong from ccs_common.upload_km_vnpt a where a.upload_id = "+upload_id;
        return s;
  }
	public String doc_hople_vnpt(
	String dataschema, 
			String upload_id)
  	{ 
	 	String url_="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=upload/capnhat_km_vnpt/ajax_hople";
		url_ = url_ + "&"+CesarCode.encode("data_schema")+"="+dataschema;	
		url_ = url_ + "&"+CesarCode.encode("upload_id")+"="+upload_id;	    
	  return url_;
  	}
	public String doc_khonghople_vnpt(
   			String dataschema,
			String upload_id)
  	{ 
		String url_="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=upload/capnhat_km_vnpt/ajax_khonghople";	
		url_ = url_ + "&"+CesarCode.encode("data_schema")+"="+dataschema;	
		url_ = url_ + "&"+CesarCode.encode("upload_id")+"="+upload_id;	    
		return url_;		  
  	}
	public String capnhat_km_vnpt(
		String dataschema,
		String funcschema,
		String upload_id		
	)
	{ 
		return "begin ? :=" + funcschema + "pkg_upload.capnhat_km_vnpt("
			+"'"+upload_id+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";	
	}
}