package neo.pttb;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import cosaldap.Base64.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;
import java.net.*;
import java.io.*;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

public class dk_goi_tichhop
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb.dk_goi_tichhop was called");
  }   
	public String them_moi_dd(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String dsmay 				
		, String chunhom
		, String ghiChu 		
		, String sIP		
  	) throws Exception {

     String s=    "begin ? := "+FuncSchema+"pkg_regis_data.them_moi_dd("	
		+" '"+loaiGoi+"'"
		+",'"+dsmay+"'"                           
		+",'"+chunhom+"'"  
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}	
	
public String cap_nhat_dd(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String sIP	
		, String loaiGoi
		, String idGoi			
		, String chunhom
		, String dsmay1
		, String dsmay2
		, String dsmay3		
		, String ghiChu 	
  	) throws Exception {

     String s=    "begin ? := "+FuncSchema+"pkg_qltb.cap_nhat_dd("	
		+" '"+loaiGoi+"'"
		+",'"+idGoi+"'"
		+",'"+dsmay1+"'"                           
		+",'"+dsmay2+"'"
		+",'"+dsmay3+"'"
		+",'"+chunhom+"'"	
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"
		+",'"+sIP+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"							
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}

public String huy_goi_dd(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String idGoi
		, String Loaigoi
		, String ghiChu
	    , String sMay_cn		
  	) throws Exception {
		
     String s=    "begin ? := "+sFuncSchema+"pkg_regis_data.huy_goi_dd("		
		+" '"+sDataSchema+"'"                                                             
		+",'"+getUserVar("sys_agentcode")+"'" 
		+",'"+getUserVar("userID")+"'"  
		+",'"+sMay_cn+"'"
		+",'"+idGoi+"'"
		+",'"+Loaigoi+"'"
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}	
     public String them_moi(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String dsmay 				
		, String chunhom
		, String ngay_dk
		, String ghiChu 		
		, String sIP		
  	) throws Exception {
    // String s=    "begin ? := "+FuncSchema+"pkg_regis_data.them_moi("	
	 String s=    "begin ? := "+FuncSchema+"pkg_regis_data_intergrate.them_moi("	
		+" '"+loaiGoi+"'"
		+",'"+dsmay+"'"                           
		+",'"+chunhom+"'"      
		+",'"+ngay_dk+"'"    		
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	
	
	public String cap_nhat(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String sIP	
		, String loaiGoi
		, String idGoi			
		, String chunhom
		, String ngay_dk
		, String dsmay 	
		, String ghiChu 
		, String kieu
		, String loaigoitach		
  	) throws Exception {

    // String s=    "begin ? := "+FuncSchema+"pkg_regis_data.cap_nhat("	
	 String s=    "begin ? := "+FuncSchema+"pkg_api_data.cap_nhat("	
		+" '"+loaiGoi+"'"
		+",'"+idGoi+"'"
		+",'"+dsmay+"'"                           
		+",'"+chunhom+"'"  
		+",'"+ngay_dk+"'"   
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+kieu+"'"
		+",'"+loaigoitach+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"		
		+",'"+sIP+"'"			
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	 public String upload_business_annam
	(
	 String upload_id,
	 String type_id,
	 String dispatch_id,
	 String package_id,
	 String userid, 
	 String userip
	)
	{
		
		String sHost="10.156.3.61";
		String sPort="81";
		String sMethod="mapi/g";
		
		String s= "begin ? :=admin_v2.pkg_regis_intergrate.get_data_upload('"
			 + upload_id     +"','"
			 + dispatch_id     +"','1','"
			 + userid
			 +"');"
			 +"end;"
			;
		System.out.println(s);// lay ds chu nhom 
		String keymsisdn="", submsisdn="", regis_sts="", sub_sts="", json="",xml="",s_member="", sub_pkg_id="",updsts="", res_updsts="";
		JSONParser parser = new JSONParser();
		JSONObject obj=null;
		String res_="",resStr="",errstr_="";
		try{
			RowSet rowSet = this.reqRSet(s);
            while (rowSet.next()) {                
                keymsisdn = rowSet.getString("msisdn");
				regis_sts  = rowSet.getString("regis_sts");
			}
			s_member= "begin ? :=admin_v2.pkg_regis_intergrate.get_data_upload('"
			  + upload_id     +"','"
			 + dispatch_id     +"','0','"
			 + userid
			 +"');"
			 +"end;"
			;
			String next="1";
			if (type_id.equals("1")){// neu la dang ky				
				//neu chua co nhom thi thuc hien dang ky chu nhom 
				if (regis_sts.equals("0")) {
					json="{\"service\":\"annam/flow\",\"flow_name\":\"reg_register\",\"msisdn\":\""+keymsisdn+"\",\"dispatch_id\":\""+dispatch_id+"\",\"packageid\":\""+package_id+"\",\"ngaydk\":\"\",\"note\":\"Upload DS\",\"userid\":\""+getUserVar("userID")+"\",\"agent\":\""+getUserVar("sys_agentcode")+"\",\"schema\":\""+getUserVar("sys_dataschema")+"\",\"ip\":\""+getUserVar("userIP")+"\",\"loaidk\":\"0\"}";
					System.out.println(json);
					xml = postJson(json,sHost,sPort,sMethod);
					System.out.println(xml);
					
					obj = (JSONObject) parser.parse(xml);
					 
					String resultCode = (String) obj.get("error_code");
					if (resultCode.equals("0")) 
					{
						next="1";
					}
					else {
						next="0";
						resStr = (String) obj.get("message");
						errstr_="begin ? :=admin_v2.pkg_regis_intergrate.update_msisdn_err('"
							 + upload_id  	+"','"
							 + userid  	+"','"
							 + dispatch_id  	+"','"
							 + keymsisdn  	+"','"
							 + resStr  	+"','"
							 + type_id  	+"','1');"
							 +"end;"
							;
						res_  =  this.reqValue("", errstr_);
						
						return "Loi dang ky chu nhom :" +resStr;
					}
					
				}
				if (next.equals("1")) {
					//neu dang ky chu nhom thanh cong dang ky thanh vien				
					rowSet = this.reqRSet(s_member);
					while (rowSet.next()) {    //add thanh vien            
						submsisdn = rowSet.getString("msisdn");
						sub_sts  = rowSet.getString("regis_sts");
						xml="";
						String res_sub="";
						if (sub_sts.equals("0")){
							json="{\"service\":\"annam/flow\",\"flow_name\":\"grpmng_add_member\",\"msisdn\":\""+keymsisdn+"\",\"member\":\""+submsisdn+"\",\"dispatch_id\":\""+dispatch_id+"\",\"packageid\":\""+package_id+"\",\"ngaydk\":\"\",\"note\":\"Upload DS\",\"userid\":\""+getUserVar("userID")+"\",\"agent\":\""+getUserVar("sys_agentcode")+"\",\"schema\":\""+getUserVar("sys_dataschema")+"\",\"ip\":\""+getUserVar("userIP")+"\",\"loaidk\":\"0\"}";
							System.out.println(json);
							xml = postJson(json,sHost,sPort,sMethod);
							obj = (JSONObject) parser.parse(xml);
							String subErrCode=(String) obj.get("error_code");
							System.out.println(xml); 
							if (!subErrCode.equals("0"))
							{
								resStr = (String) obj.get("message");
								errstr_="begin ? :=admin_v2.pkg_regis_intergrate.update_msisdn_err('"
									 + upload_id  	+"','"
									 + userid  	    +"','"
									 + dispatch_id  	+"','"
									 + submsisdn  	+"','"
									 + resStr  	+"','"
									 + type_id  	+"','0');"
									 +"end;"
									;
								res_  =  this.reqValue("", errstr_);
							}
							
						}
						else {
							resStr = "Thue bao da co goi";
							errstr_="begin ? :=admin_v2.pkg_regis_intergrate.update_msisdn_err('"
									 + upload_id  	+"','"
									 + userid  	    +"','"
									 + dispatch_id  	+"','"
									 + submsisdn  	+"','"
									 + resStr  	+"','"
									 + type_id  	+"','0');"
									 +"end;"
									;
							res_  =  this.reqValue("", errstr_);
						}
					 }
				}
				else {
					updsts="begin ? :=admin_v2.pkg_regis_intergrate.imp_result_upload('"
							 + upload_id  	+"','"
							 + type_id  	+"','2','"							 
							 + "Loi DK chu nhom " +resStr  	+"','"
							 + keymsisdn  	+"','"
							 + dispatch_id  	+"','"
							 + userid+ "');"
							 +"end;"
							;
						res_updsts  =  this.reqValue("", updsts);
					return "Loi dang ky chu nhom " ;
				}
				
				
			}
			else {//neu la huy
				//huy thanh vien
				rowSet = this.reqRSet(s_member);
				int counttv=0;
				while (rowSet.next()) {                
					counttv=counttv+1;
					submsisdn = rowSet.getString("msisdn");
					sub_sts  = rowSet.getString("regis_sts");
					xml="";
					String res_sub="";
					if (sub_sts.equals("1")){
						json="{\"service\":\"annam/flow\",\"flow_name\":\"grpmng_delete_member\",\"msisdn\":\""+keymsisdn+"\",\"member\":\""+submsisdn+"\",\"dispatch_id\":\""+dispatch_id+"\",\"packageid\":\""+package_id+"\",\"ngaydk\":\"\",\"note\":\"Upload DS\",\"userid\":\""+getUserVar("userID")+"\",\"agent\":\""+getUserVar("sys_agentcode")+"\",\"schema\":\""+getUserVar("sys_dataschema")+"\",\"ip\":\""+getUserVar("userIP")+"\",\"loaidk\":\"0\"}";
						System.out.println(json);
						xml = postJson(json,sHost,sPort,sMethod);
						obj = (JSONObject) parser.parse(xml);
						String subErrCode=(String) obj.get("error_code");
						System.out.println(xml); 
						if (!subErrCode.equals("0"))
						{
							resStr = (String) obj.get("message");
							errstr_="begin ? :=admin_v2.pkg_regis_intergrate.update_msisdn_err('"
								 + upload_id  	+"','"
								 + userid  	    +"','"
								 + dispatch_id  	+"','"
								 + submsisdn  	+"','"
								 + resStr  	+"','"
								 + type_id  	+"','0');"
								 +"end;"
								;
							res_  =  this.reqValue("", errstr_);
						}
						
					}
					else {
							resStr = "Thue bao khong co goi";
							errstr_="begin ? :=admin_v2.pkg_regis_intergrate.update_msisdn_err('"
									 + upload_id  	+"','"
									 + userid  	    +"','"
									 + dispatch_id  	+"','"
									 + submsisdn  	+"','"
									 + resStr  	+"','"
									 + type_id  	+"','0');"
									 +"end;"
									;
							res_  =  this.reqValue("", errstr_);
					}
				}
				System.out.println(" So luong thanh vien " +counttv); 
				System.out.println(" ton ai chu nhom  " +regis_sts);
				if ((counttv == 0)&&(regis_sts.equals("1"))	)//neu ban ghi upload co minh chu nhom thi thuc hien huy ca nhom
				{
					//thuc hien huy danh sach thanh vien con ton tai roi huy chu nhom
					s_member ="begin ? :=admin_v2.pkg_regis_intergrate.get_member_dispatch('"
								 +  dispatch_id    +"','"
								 +  keymsisdn   +"','"
								 + userid
								 +"');"
								 +"end;"
								;
					rowSet = this.reqRSet(s_member);					
					while (rowSet.next()) {
						submsisdn = rowSet.getString("msisdn");
						sub_pkg_id  = rowSet.getString("package_id");
						json="{\"service\":\"annam/flow\",\"flow_name\":\"grpmng_delete_member\",\"msisdn\":\""+keymsisdn+"\",\"member\":\""+submsisdn+"\",\"dispatch_id\":\""+dispatch_id+"\",\"packageid\":\""+sub_pkg_id+"\",\"ngaydk\":\"\",\"note\":\"Upload DS\",\"userid\":\""+getUserVar("userID")+"\",\"agent\":\""+getUserVar("sys_agentcode")+"\",\"schema\":\""+getUserVar("sys_dataschema")+"\",\"ip\":\""+getUserVar("userIP")+"\",\"loaidk\":\"0\"}";
						System.out.println(json);
						xml = postJson(json,sHost,sPort,sMethod);
						obj = (JSONObject) parser.parse(xml);
						String subErrCode=(String) obj.get("error_code");
						System.out.println(xml); 
						if (!subErrCode.equals("0"))
						{
							resStr = "Loi huy thanh vien "+submsisdn + ":" + (String) obj.get("message");
							errstr_="begin ? :=admin_v2.pkg_regis_intergrate.update_msisdn_err('"
								 + upload_id  	+"','"
								 + userid  	    +"','"
								 + dispatch_id  	+"','"
								 + keymsisdn  	+"','"
								 + resStr  	+"','"
								 + type_id  	+"','1');"
								 +"end;"
								;
							res_  =  this.reqValue("", errstr_);
							next="0";
							return "Co loi khi huy so thanh vien "+submsisdn + ":" +resStr;	
						}	
					}
					if (next.equals("1")) {
						json="{\"service\":\"annam/flow\",\"flow_name\":\"dereg_deregister\",\"msisdn\":\""+keymsisdn+"\",\"dispatch_id\":\""+dispatch_id+"\",\"packageid\":\""+package_id+"\",\"ngaydk\":\"\",\"note\":\"Upload DS\",\"userid\":\""+getUserVar("userID")+"\",\"agent\":\""+getUserVar("sys_agentcode")+"\",\"schema\":\""+getUserVar("sys_dataschema")+"\",\"ip\":\""+getUserVar("userIP")+"\",\"loaidk\":\"0\"}";
						System.out.println(json);
						xml = postJson(json,sHost,sPort,sMethod);
						System.out.println(xml);
						
						obj = (JSONObject) parser.parse(xml);
						 
						String resultCode = (String) obj.get("error_code");
						if (!resultCode.equals("0")) {
					
							next="0";
							resStr = (String) obj.get("message");
							errstr_="begin ? :=admin_v2.pkg_regis_intergrate.update_msisdn_err('"
								 + upload_id  	+"','"
								 + userid  	    +"','"
								 + dispatch_id  	+"','"
								 + keymsisdn  	+"','"
								 + resStr  	+"','"
								 + type_id  	+"','1');"
								 +"end;"
								;
							res_  =  this.reqValue("", errstr_);
							return "Loi huy chu nhom :" +resStr;
						}
					}
					else {
						updsts="begin ? :=admin_v2.pkg_regis_intergrate.imp_result_upload('"
							 + upload_id  	+"','"
							 + type_id  	+"','2','"							 
							 + "Loi huy thanh vien"  	+"','"
							 + keymsisdn  	+"','"
							 + dispatch_id  	+"','"
							 + userid+ "');"
							 +"end;"
							;
						res_updsts  =  this.reqValue("", updsts);
						return "Co loi khi huy thanh vien, vui long kiem tra lai" ;
					}
				}
				else if ((counttv == 0)&&(regis_sts.equals("0")))
				{
					resStr = "Chu nhom khong ton tai";
					errstr_="begin ? :=admin_v2.pkg_regis_intergrate.update_msisdn_err('"
								 + upload_id  	+"','"
								 + userid  	    +"','"
								 + dispatch_id  	+"','"
								 + keymsisdn  	+"','"
								 + resStr  	+"','"
								 + type_id  	+"','1');"
								 +"end;"
								;
					res_  =  this.reqValue("", errstr_);
					return "Chu nhom khong ton tai de thuc hien huy";
				}
			}
			
		updsts="begin ? :=admin_v2.pkg_regis_intergrate.imp_result_upload('"
							 + upload_id  	+"','"
							 + type_id  	+"','1','"							 
							 + "Thuc hien thanh cong"  	+"','"
							 + keymsisdn  	+"','"
							 + dispatch_id  	+"','"
							 + userid+ "');"
							 +"end;"
							;
		res_updsts  =  this.reqValue("", updsts);	
					
		}catch(Exception e){
			updsts="begin ? :=admin_v2.pkg_regis_intergrate.imp_result_upload('"
							 + upload_id  	+"','"
							 + type_id  	+"','2','"							 
							 + "Loi thuc hien"  	+"','"
							 + keymsisdn  	+"','"
							 + dispatch_id  	+"','"
							 + userid+ "');"
							 +"end;"
							;
			res_updsts  =  this.reqValue("", updsts);
			return "Co loi khi thuc hien "+e.getMessage();
		}
		
		return "1";	
	}
	 public String get_result_upload
	(
		 String upload_id
		
	)
	{
		String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_regis_intergrate.get_result_upload("         
                + "'" + upload_id + "'"			 				
                + ");"
                + "end;";
			
		   try
				{		    
					 result= this.reqValue("",s);			 
				} 
			catch (Exception ex) 
				{            	
				   result = ex.getMessage();
				}
			return result;	
	}		
	public static String postJson( String Request,String Host,String Port,String method)
    {
        StringBuffer buf = new StringBuffer();
        try {        
            URL u = new URL("http://"+Host+":"+Port+"/"+method);
            HttpURLConnection con = (HttpURLConnection)u.openConnection();
            con.setConnectTimeout(5000); 
            
            con.setRequestProperty("Content-Type","application/json;charset=utf-8");        
            con.setDoOutput(true);
            con.setDoInput(true);
            con.connect();
            
            OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
            writer.write(Request);
            writer.flush();
            BufferedReader  reader ;
            if(con.getResponseCode() == 200){
              reader = new BufferedReader(new
              InputStreamReader(con.getInputStream()));
            }else{
              reader = new BufferedReader(new
              InputStreamReader(con.getErrorStream()));
            }            
            String line ;
            while( (line = reader.readLine()) != null )
            buf.append(line);
            
            reader.close();
            return buf.toString();            
        }catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
	public String chuyenloai_goi(
	    	  String sUserId
			, String sAgentCode
			, String sDataSchema
			, String FuncSchema
			, String sIP	
			, String loaiGoi
			, String loaiGoi_cu
			, String idGoi			
			, String chunhom1
			, String chunhom2
			, String dsmay 	
			, String ghiChu 
	  	) throws Exception {
		String s=    "begin ? := "+FuncSchema+"pkg_regis_data.chuyenloai_goi("	     
			+" '"+loaiGoi+"'"
			+",'"+loaiGoi_cu+"'"
			+",'"+idGoi+"'"
			+",'"+dsmay+"'"                           
			+",'"+chunhom1+"'"  
			+",'"+chunhom2+"'" 			
			+",'"+ConvertFont.UtoD(ghiChu)+"'"
			+",'"+getUserVar("userID")+"'"		
			+",'"+getUserVar("sys_agentcode")+"'"
			+",'"+sDataSchema+"'"		
			+",'"+sIP+"'"	
			+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
	
	public String huy_goi(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String idGoi
		, String Loaigoi
		, String ghiChu
	    , String sMay_cn		
  	) throws Exception {
		
    // String s=    "begin ? := "+sFuncSchema+"pkg_regis_data.huy_goi("		
	 String s=    "begin ? := "+sFuncSchema+"pkg_regis_data_intergrate.huy_goi("		
		+" '"+sDataSchema+"'"                                                             
		+",'"+getUserVar("sys_agentcode")+"'" 
		+",'"+getUserVar("userID")+"'"  
		+",'"+sMay_cn+"'"
		+",'"+idGoi+"'"
		+",'"+Loaigoi+"'"
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	} 
	

	public String ghep_goi(
	    	  String sUserId
			, String sAgentCode
			, String sDataSchema
			, String FuncSchema
			, String sIP	
			, String loaiGoi
			, String idGoi_cu	
			, String idGoi_moi
			, String chunhom_cu
			, String chunhom_moi
			, String dsmay 	
			, String ghiChu 					
	  	) throws Exception {
	    // String s=    "begin ? := "+FuncSchema+"pkg_regis_data.ghep_goi("
		 String s=    "begin ? := "+FuncSchema+"pkg_api_data.ghep_goi("
			+" '"+loaiGoi+"'"
			+",'"+idGoi_cu+"'"
			+",'"+idGoi_moi+"'"
			+",'"+dsmay+"'"                           
			+",'"+chunhom_cu+"'"  
			+",'"+chunhom_moi+"'" 			
			+",'"+ConvertFont.UtoD(ghiChu)+"'"
			+",'"+getUserVar("userID")+"'"		
			+",'"+getUserVar("sys_agentcode")+"'"
			+",'"+sDataSchema+"'"		
			+",'"+sIP+"'"	
			+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
	
	public String ds_kieuth( String sCVid ) throws Exception {
	     String s=    "begin ? := admin_v2.pkg_regis_data.layds_dieukien(" +sCVid+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
		
	public String upload_business
	   (
	     String upload_id,
		 String type_id,
		 String dispatch_id,
		 String package_id,
		 String userid, 
		 String userip
	   )
	   {
	    
		String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_regis_data_intergrate.upload_to_queue("         
                + "'" + upload_id + "'"
				+ ",'" + type_id + "'"
                + ",'" + dispatch_id + "'"
				+ ",'" + package_id + "'"
                + ",'" + userid + "'"
                + ",'" + userip + "'"    
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'"   				
                + ");"
                + "end;";
	System.out.println("ThuyVTT2:"+s);		
       try
			{		    
				 result= this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	   }
}














