package neo.pttb;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import cosaldap.Base64.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;


public class SRV2585 extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb.SRV2585 was called");
  }   
	public String them_moi_cn(
		  String sUserId
		, String sIP
		, String tb_cn
		, String pkg_id  		
		, String ghichu
  	) throws Exception {
	System.out.println("------------------------Dang ky---------------------");
     String s=    "begin ? := admin_v2.pkg_srv_2585.them_moi_cn("	
		+" '"+sUserId+"'"
		+",'"+sIP+"'"                           
		+",'"+tb_cn+"'"  
		+",'"+pkg_id+"'"
		+",'"+ConvertFont.UtoD(ghichu)+"'"		
		+");"
        +"end;"
        ;
	System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String thay_doi_cn(
		  String sUserId
		, String sIP
		, String tb_cn_cu
		, String tb_cn_moi  		
		, String ghichu
  	) throws Exception {
	System.out.println("------------------------Dang ky---------------------");
     String s=    "begin ? := admin_v2.pkg_srv_2585.thay_chu_nhom("	
		+" '"+sUserId+"'"
		+",'"+sIP+"'"                           
		+",'"+tb_cn_cu+"'"  
		+",'"+tb_cn_moi+"'"
		+",'"+ConvertFont.UtoD(ghichu)+"'"		
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	public String them_moi_tv(
		  String sUserId
		, String sIP
		, String tb_cn
		, String tb_tv  		
		, String ghichu		
  	) throws Exception {
	System.out.println("------------------------Dang ky---------------------");
     String s=    "begin ? := admin_v2.pkg_srv_2585.them_moi_tv("	
		+" '"+sUserId+"'"
		+",'"+sIP+"'"                           
		+",'"+tb_cn+"'"  
		+",'"+tb_tv+"'"
		+",'"+ConvertFont.UtoD(ghichu)+"'"	
		+",'"+0+"'"		
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	public String excute_file_upload(
		String uploadId
		, String sUserId
		, String sIP
		, String type
  	) throws Exception {
	System.out.println("------------------------Thuc hien cap nhat/xoa----------------------");
	String s = "";
	if (type.equals("1")) {
		 s=    "begin ? := admin_v2.pkg_srv_2585.th_dk_upload("	
			+" '"+uploadId+"'"
			+",'"+sUserId+"'"
			+",'"+sIP+"'"		
			+");"
			+"end;"
			;
	}else {
		 s=    "begin ? := admin_v2.pkg_srv_2585.th_huy_upload("	
		+" '"+uploadId+"'"
		+",'"+sUserId+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
	}
		return this.reqValue("", s);
	}

	public String xoa_tb(
		  String sUserId
		, String sIP
		, String tb_cn	
		, String tb_tv
		, String key_msisdn
		, String pstype 	
  	) throws Exception {
	System.out.println("------------------------Xoa----------------------");
     String s=    "begin ? := admin_v2.pkg_srv_2585.xoa_tb("	
		+" '"+sUserId+"'"
		+",'"+sIP+"'"
		+",'"+tb_cn+"'" 
		+",'"+tb_tv+"'"		
		+",'"+key_msisdn+"'"
		+",'"+pstype+"'"
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	public String reg(
		  String sUserId
		, String sIP
		, String tb_cn
		, String tb_tv  
		, String pkg_id
		, String ghichu		
  	) throws Exception {
	System.out.println("------------------------Dang ky---------------------");
     String s=    "begin ? := admin_v2.pkg_srv_2585.reg("	
		+" '"+sUserId+"'"
		+",'"+sIP+"'"                           
		+",'"+tb_cn+"'"  
		+",'"+tb_tv+"'"
		+",'"+pkg_id+"'"
		+",'"+ConvertFont.UtoD(ghichu)+"'"
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	public String replace(
		  String sUserId
		, String sIP
		, String tb_cn_cu
		, String tb_cn_moi  		
		, String ghichu
  	) throws Exception {
	System.out.println("------------------------Thay chuong nhom---------------------");
     String s=    "begin ? := admin_v2.pkg_srv_2585.thay_chu_nhom("	
		+" '"+sUserId+"'"
		+",'"+sIP+"'"                           
		+",'"+tb_cn_cu+"'"  
		+",'"+tb_cn_moi+"'"
		+",'"+ConvertFont.UtoD(ghichu)+"'"		
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	public String del_cn(
		  String sUserId
		, String sIP
		, String tb_cn
  	) throws Exception {
	System.out.println("------------------------Xoa chu nhom---------------------");
     String s=    "begin ? := admin_v2.pkg_srv_2585.xoa_tb("	
		+" '"+sUserId+"'"
		+",'"+sIP+"'"                           
		+",'"+tb_cn+"'"  
		+",''"
		+",'"+1+"'" 
		+",'"+1+"'" 
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	public String del_tv(
		  String sUserId
		, String sIP
		, String tb_cn
		, String tb_tvs
  	) throws Exception {
	System.out.println("------------------------Xoa chu nhom---------------------");
     String s=    "begin ? := admin_v2.pkg_srv_2585.xoa_tv("	
		+" '"+sUserId+"'"
		+",'"+sIP+"'"                           
		+",'"+tb_cn+"'"  
		+",'"+tb_tvs+"'"
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
}














