package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class ttmp_po
extends NEOProcessEx {
	 public void run() {
		System.out.println("neo.pttb.goi_truyen_solieu was called");
	 }


  
    public String thuboihoan_ck(
		  String psMsisdn
		, String psTienBH 
		, String psMaKH
		, String psTienNum
		, String psTienNumTT
		, String psTienPKG
		, String psTienPKGTT
		, String psTienPromo
		, String psTienPromoTT
		, String psTienDDDN
		, String psTienDDDNTT
		, String psTienCKTM
		, String psTienCKTMTT
		, String psTienBHTT		
		, String psLydo
		, String psAgentCode
		, String psUserID
		, String psUserIP 				
			
  	) throws Exception {
	 
		String s=    "begin ? := admin_v2.mnp_boihoan_ck.thu_tien_bhck("	
		+" '"+psMsisdn+"'"
		+",'"+psTienBH+"'"
		+",'"+psMaKH+"'"
		+",'"+ psTienNum+"'"
		+",'"+ psTienNumTT+"'"
		+",'"+ psTienPKG+"'"
		+",'"+ psTienPKGTT+"'"
		+",'"+ psTienPromo+"'"
		+",'"+ psTienPromoTT+"'"
		+",'"+ psTienDDDN+"'"
		+",'"+ psTienDDDNTT+"'"
		+",'"+ psTienCKTM+"'"
		+",'"+ psTienCKTMTT+"'"
		+",'"+psTienBHTT+"'"
		+",'"+psLydo+"'"
		+",'"+psAgentCode+"'"
		+",'"+psUserID+"'"		
		+",'"+psUserIP+"'"
		+");"
        +"end;"
        ;
		System.out.println(s);
		
		return this.reqValue("", s);
	}
	
	 public String huyphieu_bh(
		  String psMsisdn
		, String psPhieuid	
		, String psAgentCode
		, String psUserID
		, String psUserIP 				
			
  	) throws Exception {
	 
		String s=    "begin ? := admin_v2.mnp_boihoan_ck.huy_tien_bhck("	
		+" '"+psMsisdn+"'"
		+",'"+psPhieuid+"'"		
		+",'"+psAgentCode+"'"
		+",'"+psUserID+"'"		
		+",'"+psUserIP+"'"
		+");"
        +"end;"
        ;
		System.out.println(s);
		
		return this.reqValue("", s);
	}
	
	public String huyck(
		  String psMsisdn
		, String psPhieuId
		, String ActiveDate 
		, String psSTSNum
		, String psNumCK
		, String psNumMonth
		, String psSTSPkg		
		, String psSTSDDDN
		, String psDDDNRegisDate
		, String psSTSCKTM
		, String psSTSPromo		
		, String psAgentCode
		, String psUserID
		, String psUserIP 				
			
  	) throws Exception {
		//luu log boi hoan ccbs
		String str="";
		String re_="1";
		if (psSTSNum.equals("1")) {
			str=    "begin ? := admin_v2.mnp_boihoan_ck.log_chonso_ck("	
					+" '"+psMsisdn+"'"
					+",'"+psPhieuId+"'"		
					+",'"+psNumCK+"'"
					+",'"+psNumMonth+"'"
					+");"
					+"end;"
					;
			re_=this.reqValue("", str);
			System.out.println(re_);
			
		}
		if (re_.equals("1")) {
			if (psSTSPkg.equals("1")) {
				str=    "begin ? := admin_v2.mnp_boihoan_ck.log_goi_ck("	
						+" '"+psMsisdn+"'"
						+",'"+psPhieuId+"'"		
						+",'"+psAgentCode+"'"					
						+");"
						+"end;"
						;
				re_=this.reqValue("", str);
				System.out.println(re_);
			}
			if (re_.equals("1")) {
				if (psSTSPromo.equals("1")) {
					str=    "begin ? := admin_v2.mnp_boihoan_ck.log_goi_km("	
								+" '"+psMsisdn+"'"
								+",'"+psPhieuId+"'"		
								+",'"+psAgentCode+"'"					
								+");"
								+"end;"
								;
					re_=this.reqValue("", str);
					System.out.println(re_);
					
				}
				if (re_.equals("1")) {
					
					if (psSTSDDDN.equals("1")) {
						str=    "begin ? := ccs_common.mnp_boihoan_ck.log_dddn("	
								+" '"+psMsisdn+"'"					
								+",'"+psPhieuId+"'"		
								+",'"+psAgentCode+"'"
								+",'"+psDDDNRegisDate+"'"
								+");"
								+"end;"
								;
						re_=this.reqValue("VNPBilling", str);
						System.out.println(re_);
						
					}
					if (re_.equals("1")) {
							if (psSTSCKTM.equals("1")) {
							str=    "begin ? := ccs_common.mnp_boihoan_ck.log_cktm("	
									+" '"+psMsisdn+"'"
									+",'"+ActiveDate+"'"
									+",'"+psPhieuId+"'"		
									+",'"+psAgentCode+"'"					
									+");"
									+"end;"
									;
							re_=this.reqValue("VNPBilling", str);
							System.out.println(re_);
							
						}
					}else return "Loi insert log DDDN";
					
				}else return "Loi insert log goi KM";
			
			}
			else return "Loi insert log goi CK";
			
			
		}
		else return "Loi insert log NUMSTORE";
		
	    if (re_.equals("1")) {
			String s=    "begin ? := admin_v2.mnp_boihoan_ck.huy_ck_mnp("	
			+" '"+psMsisdn+"'"
			+",'"+ActiveDate+"'"		
			+",'"+psAgentCode+"'"
			+",'"+psUserID+"'"		
			+",'"+psUserIP+"'"
			+");"
			+"end;"
			;
			System.out.println(s);
			
			return this.reqValue("", s);
		}
		else return "Loi insert log CKTM";
		
	}
	
}














