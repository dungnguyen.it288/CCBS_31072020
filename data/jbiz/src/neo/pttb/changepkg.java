package neo.pttb;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import cosaldap.Base64.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;


public class changepkg
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb.dk_goi was called");
  }   
	
	
	public String cap_nhat(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String sIP	
		, String loaiGoi
		, String idGoi
		, String dsmaymoi 	
		, String ghiChu
		, String dsmaycu		
  	) throws Exception {

	 String s=    "begin ? := "+FuncSchema+"pkg_data_change.chuyen_goi("	
		+" '"+loaiGoi+"'"
		+",'"+idGoi+"'"
		+",'"+dsmaymoi+"'"                           
		+",'"+dsmaycu+"'"  
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"		
		+",'"+sIP+"'"			
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	public String cap_nhat_mailinh(
		  String uploadId	
		, String idGoi
		, String sIP
		, String sDataSchema
  	) throws Exception {

	 String s=    "begin ? := admin_v2.pkg_change_mailinh.chuyengoi_mailinh_hcm("	
		+" '"+uploadId+"'"
		+",'"+idGoi+"'" 
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"		
		+",'"+sIP+"'"			
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	public String cn_thuebao_thuchien(
		  int uploadId	
		, int column_id
  	) throws Exception {
	 String s=    "begin ? := admin_v2.pkg_capnhat_thuebao.thuc_hien("	
		+uploadId
		+","+column_id	
		+",'"+getUserVar("userID")+"'"	
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+getUserVar("sys_dataschema")+"'"				
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
}














