package neo.pttb;
import neo.smartui.common.*;
import neo.smartui.process.*;

import java.util.*;
import javax.sql.*;
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import neo.smartui.common.CesarCode;
import neo.smartui.report.*;
import com.svcon.jdbf.*;
import javax.sql.RowSet;
import sun.misc.*;
public class ecosytem extends NEOProcessEx {
    
	public String add_sub (	 
			String pc_userid,
			String pc_agentcode	,
			String pc_dispatchid,
			String pc_serviceid,
			String pc_listsub,
			String pc_datereg,			
			String pc_userip )
	throws Exception {
		String result =null;
		String loai ="";
		String custid ="";
		// Kiem tra xem la dky hay update:
		
		
		try {
			String body ="";			
				
			long transid = System.currentTimeMillis();
			String api_rs ="";
			String resultCode ="";
			String resultMsg ="";
			String subsuccess="";
			String unsuccess="";				
			String[] arrOfStr = pc_listsub.split(",");
			for (int i=0;i<arrOfStr.length;i++)
			{
				body ="{\"service\":\"package_register\",\"trans_id\":\""+transid+"\",\"promotion\":\"0\",\"msisdn\":\""+arrOfStr[i]+"\",\"action\":\"REG\",\"service_code\":\""+pc_serviceid+"\",\"channel\":\"CCBS\",\"note\":\"REG:"+pc_userid+"-"+pc_userip+"\"}";
				System.out.println(body);
				String api_reg = apiJsonPost(body);
				System.out.println(api_reg);
				JSONParser parser = new JSONParser();
				JSONObject obj = (JSONObject) parser.parse(api_reg);
				
				JSONObject resObj = (JSONObject) obj.get("result");
				resultMsg = (String) resObj.get("error_desc");
				resultCode = (String) resObj.get("error_code");
				
				if(resultCode.equals("0")) {
					subsuccess=subsuccess+arrOfStr[i]+",";
				}
				else 
				{
					unsuccess=unsuccess+arrOfStr[i]+":"+resultMsg+",";
				}
			}
			if (unsuccess.equals(""))
			{
				return "Thuc hien dang ky thanh cong";
			}
			if (subsuccess.equals(""))
			{
				return "Thuc hien khong thanh cong:"+unsuccess;
			}
			return "DS thue bao thanh cong:"+subsuccess+"; DS khong thanh cong:"+unsuccess;			
				
			
		}catch (Exception ex){				
			System.out.println("Exception 2 : " + ex.getMessage());
			ex.printStackTrace();
			result=ex.getMessage();
		}
		return result;
    } 
	
	public String del_sub (	 
			String pc_userid,
			String pc_agentcode	,
			String pc_dispatchid,
			String pc_serviceid,
			String pc_listsub,
			String pc_datereg,			
			String pc_userip )
	throws Exception {
		String result =null;
		String loai ="";
		String custid ="";
		// Kiem tra xem la dky hay update:
		
		
		try {
			String body ="";			
				
			long transid = System.currentTimeMillis();
			String api_rs ="";
			String resultCode ="";
			String resultMsg ="";
			String subsuccess="";
			String unsuccess="";				
			String[] arrOfStr = pc_listsub.split(",");
			for (int i=0;i<arrOfStr.length;i++)
			{
				body ="{\"service\":\"package_register\",\"trans_id\":\""+transid+"\",\"promotion\":\"0\",\"msisdn\":\""+arrOfStr[i]+"\",\"action\":\"UNREG\",\"service_code\":\""+pc_serviceid+"\",\"channel\":\"CCBS\",\"note\":\"UNREG:"+pc_userid+"-"+pc_userip+"\"}";
				System.out.println(body);
				String api_del = apiJsonPost(body);
				System.out.println(api_del);
				JSONParser parser = new JSONParser();
				JSONObject obj = (JSONObject) parser.parse(api_del);
				
				JSONObject resObj = (JSONObject) obj.get("result");
				resultMsg = (String) resObj.get("error_desc");
				resultCode = (String) resObj.get("error_code");
				
				if(resultCode.equals("0")) {
					subsuccess=subsuccess+arrOfStr[i]+",";
				}
				else 
				{
					unsuccess=unsuccess+arrOfStr[i]+":"+resultMsg+",";
				}
			}
			if (unsuccess.equals(""))
			{
				return "Thuc hien huy thanh cong";
			}
			if (subsuccess.equals(""))
			{
				return "Thuc hien huy khong thanh cong:"+unsuccess;
			}
			return "DS thue bao huy thanh cong:"+subsuccess+"; DS huy khong thanh cong:"+unsuccess;			
				
			
		}catch (Exception ex){				
			System.out.println("Exception 2 : " + ex.getMessage());
			ex.printStackTrace();
			result=ex.getMessage();
		}
		return result;
    } 
	public String subinfo (	 
			String pc_userid,
			String pc_agentcode	,		
			String pc_msisdn,				
			String pc_userip )
	throws Exception {
		String result =null;
		String loai ="";
		String custid ="";
		// Kiem tra xem la dky hay update:
		
		
		try {
			String body ="";			
				
			long transid = System.currentTimeMillis();
			String api_rs ="";
			String resultCode ="";
			String resultMsg ="";
			String res_="";						
		
			body ="{\"service\":\"sub_info\",\"trans_id\":\""+transid+"\",\"msisdn\":\""+pc_msisdn+"\"}";
			System.out.println(body);
			String api_get = apiJsonPost(body);
			System.out.println(api_get);
			JSONParser parser = new JSONParser();
			JSONObject obj = (JSONObject) parser.parse(api_get);
			JSONObject resObj = (JSONObject) obj.get("result");
			resultCode  = (String) resObj.get("error_code");
			resultMsg  = (String) resObj.get("error_desc");
			if (resultCode.equals("0"))
			{
				
				JSONArray array = (JSONArray) resObj.get("data");
				res_="<table width=100% cellpadding=1 cellspacing=1 >";
				res_=res_+"<tr class=tableHeadBg><td width=20% align=center class=\"title\">S&#7889; TB</td><td width=30% class=\"title\" align=center>M&#227; d&#7883;ch v&#7909;</td><td width=25% class=\"title\" align=center>Ng&#224;y b&#7855;t &#273;&#7847;u</td><td width=25% class=\"title\" align=center>Ng&#224;y k&#7871;t th&#250;c</td></tr>";
				
				for (int i = 0; i < array.size(); i++)
				{
					JSONObject jsonEntry = (JSONObject) array.get(i);
					res_=res_+"<tr onclick=\"fillsubinfo('"+(String) jsonEntry.get("MSISDN")+"','"+(String) jsonEntry.get("SERVICE_CODE")+"','"+(String) jsonEntry.get("EXPIRE_TIME")+"')\"><td width=20% align=center>"+(String) jsonEntry.get("MSISDN")+"</td><td width=30% align=center>"+(String) jsonEntry.get("SERVICE_CODE")+"</td><td width=25% align=center>"+(String) jsonEntry.get("START_TIME")+"</td><td width=25% align=center>"+(String) jsonEntry.get("EXPIRE_TIME")+"</td></tr>";
				}
				res_=res_+"</table>";
				
				return res_;
				
			}
			
			
			return api_get;
			
				
			
		}catch (Exception ex){				
			System.out.println("Exception 2 : " + ex.getMessage());
			ex.printStackTrace();
			result=ex.getMessage();
		}
		return result;
    } 
	public String getdispatchid ( String userid,
							String agentcode,
							String service						
							) 
					throws Exception{ 
         
        String s = "begin ? := admin_v2.pkg_regis_intergrate.get_dispatch_by_service("
                + "'" + service + "'"
				+ ",'" + agentcode + "'"
                + ");"
                + "end;";
        System.out.println(s);
		String result = this.reqValue("", s);
		
		return result;
			
		
    }
	
	
	
	private String getValue(String content, String key) {
	
		String temp;
		try {
			JSONParser jsonParser = new JSONParser();	            
			JSONObject jsonObject = (JSONObject) jsonParser.parse(content);
			temp = (String) jsonObject.get(key);
		}
		catch(Exception ex){
			ex.printStackTrace();
			temp ="";
		}
		return temp;

	}	

	public String apiJsonPost(String input) {
		String urlHttp = "http://10.211.22.42:8080/mediagw/g/ecosystem";
		StringBuffer buf = new StringBuffer();
        try {        
            URL u = new URL(urlHttp);
            HttpURLConnection con = (HttpURLConnection)u.openConnection();
            con.setConnectTimeout(5000); 
            
            con.setRequestProperty("Content-Type","application/json;charset=utf-8");        
            con.setDoOutput(true);
            con.setDoInput(true);
            con.connect();
            
            OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
            writer.write(input);
            writer.flush();
            BufferedReader  reader ;
            if(con.getResponseCode() == 200){
              reader = new BufferedReader(new
              InputStreamReader(con.getInputStream()));
            }else{
              reader = new BufferedReader(new
              InputStreamReader(con.getErrorStream()));
            }            
            String line ;
            while( (line = reader.readLine()) != null )
            buf.append(line);
            
            reader.close();
            String resp= buf.toString();          
        
			return resp;

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}

	}
		
	  
	
}
