package neo.pttb;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import cosaldap.Base64.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;
import java.io.*;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class hddt_doisim extends NEOProcessEx {

    public void run() {
        System.out.println("hddt_doisim was called");
    }
	
    public String thu_phi(String ma_tinh,
            String so_tb, String sim_cu, String sim_moi,
            String so_tien, String nguoi_thu, String ghi_chu, String schemauser ){
		
        String xs_ = "";
        xs_ = xs_ + " begin ";
        xs_ = xs_ + " 	?:= admin_v2.hddt_doisim.thu_phi_doisim('"+ma_tinh+"','"+so_tb+"',"+sim_cu+",'"+sim_moi+"','"+so_tien+"','"+nguoi_thu+"','"+ConvertFont.UtoD(ghi_chu)+"','"+schemauser+"'); ";
        xs_ = xs_ + " end; ";

        System.out.println(xs_);
        return xs_;
    }

	public String xuat_hddt(String psschema,
            String phieu_id, String action, String ma_tinh,
            String userid, String userip, String sim_cu, String sim_moi,
            String so_tien, String so_tb, String ten_tb, String diachi_tb ){
		
        String xs_ = "";
        xs_ = xs_ + " begin ";
        xs_ = xs_ + " 	?:= admin_v2.pkg_hddt_vat.xuat_doi_sim('"+psschema+"','"+phieu_id+"','"+action+"','"+ma_tinh+"','"+userid+"','"+userip+"','"+sim_cu+"','"+sim_moi+"','"+so_tien+"','"+so_tb+"','"+ten_tb+"','"+diachi_tb+"'); ";
        xs_ = xs_ + " end; ";

        System.out.println(xs_);
        return xs_;
    }
	
	public String view_ds_doisim(String matinh, String so_tb, String user_id) {
		return "/main?"+CesarCode.encode("configFile")+"=vinacore/hddt_doisim/ds_sim_ajax"
			+"&"+CesarCode.encode("so_tb")+"="+so_tb
			+"&"+CesarCode.encode("matinh")+"="+matinh
			+"&"+CesarCode.encode("user_id")+"="+user_id
			;
	}
	
	public String get_hoadon_dt_khs_id(String phieu_id, String ma_tinh) {
		
        String xs_ = "";
        xs_ = xs_ + " begin ";
        xs_ = xs_ + " 	?:= admin_v2.hddt_doisim.get_hoadon_dt_khs_id('"+phieu_id+"','"+ma_tinh+"'); ";
        xs_ = xs_ + " end; ";

        System.out.println(xs_);
        return xs_;
    }

}
