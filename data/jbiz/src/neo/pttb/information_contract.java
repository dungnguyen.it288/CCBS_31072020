package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class information_contract extends NEOProcessEx {
	public String new_information_contract(
		String pschema,		
		String pzip,
		String precipients,
		String precipient_code,
		String pphone,
		String pemail,
		String paddress,
		String pagent,	
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=admin_v2.pk_thuebao_cts.new_information_contract("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(pzip)+"',"
				+"'"+ConvertFont.UtoD(precipients)+"',"
				+"'"+ConvertFont.UtoD(precipient_code)+"',"
				+"'"+pphone+"',"
				+"'"+pemail+"',"
				+"'"+ConvertFont.UtoD(paddress)+"',"
				+"'"+pagent+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_information_contract(
		String pschema,
		String pid,
		String pagent,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=admin_v2.pk_thuebao_cts.del_information_contract("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+pagent+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_information_contract(
	String pschema,
		String pid,	
		String pzip,
		String precipients,
		String precipient_code,
		String pphone,
		String pemail,
		String paddress,		
		String pagent,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=admin_v2.pk_thuebao_cts.edit_information_contract("
				+"'"+pschema+"',"
				+"'"+pid+"',"				
				+"'"+ConvertFont.UtoD(pzip)+"',"
				+"'"+ConvertFont.UtoD(precipients)+"',"
				+"'"+ConvertFont.UtoD(precipient_code)+"',"
				+"'"+pphone+"',"
				+"'"+pemail+"',"
				+"'"+ConvertFont.UtoD(paddress)+"',"
				+"'"+pagent+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
}