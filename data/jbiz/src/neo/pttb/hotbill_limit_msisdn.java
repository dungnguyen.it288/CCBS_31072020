package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class hotbill_limit_msisdn extends NEOProcessEx {
	public String new_hotbill_limit_msisdn(
		String pschema,
		String pmsisdn,
		String pbl_id,
		String psubject,
		String pcreate_date,
		String pcreate_by,
		String pupdate_date,
		String pupdate_by,
		String pmonth_live,
		String pstatus,
		String psms_war,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_hotbill.new_hotbill_limit_msisdn("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(pmsisdn)+"',"
				+"'"+pbl_id+"',"
				+"'"+psubject+"',"
				+"'"+pcreate_date+"',"
				+"'"+ConvertFont.UtoD(pcreate_by)+"',"
				+"'"+pupdate_date+"',"
				+"'"+ConvertFont.UtoD(pupdate_by)+"',"
				+"'"+pmonth_live+"',"
				+"'"+pstatus+"',"
				+"'"+psms_war+"',"
				+"'"+getUserVar("sys_agentcode")+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_hotbill_limit_msisdn(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_hotbill.del_hotbill_limit_msisdn("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_hotbill_limit_msisdn(
	String pschema,
		String pid,
		String pmsisdn,
		String pbl_id,
		String psubject,
		String pcreate_date,
		String pcreate_by,
		String pupdate_date,
		String pupdate_by,
		String pmonth_live,
		String pstatus,
		String psms_war,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_hotbill.edit_hotbill_limit_msisdn("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pmsisdn)+"',"
				+"'"+pbl_id+"',"
				+"'"+psubject+"',"
				+"'"+pcreate_date+"',"
				+"'"+ConvertFont.UtoD(pcreate_by)+"',"
				+"'"+pupdate_date+"',"
				+"'"+ConvertFont.UtoD(pupdate_by)+"',"
				+"'"+pmonth_live+"',"
				+"'"+pstatus+"',"
				+"'"+psms_war+"',"
				+"'"+getUserVar("sys_agentcode")+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String accept_msisdn (								
									String upload_id,
									String psAgentCode,
									String psUploadType,
									String psUserId,
									String psuserip
	)throws Exception
	{ 
		String s = "begin ? :=admin_v2.pkg_upload.accept_upload_hotbill_limit("
			+"'"+upload_id+"',"
			+"'"+getUserVar("sys_agentcode")+"',"
			+"'"+psUploadType+"',"
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";	
		System.out.println(s);
		return this.reqValue("",s);
	}
	public String update_hotbill_status_job(
		String pschema,
		String pStatus,		
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_hotbill.update_hotbill_status_job("
				+"'"+pschema+"',"
				+"'"+pStatus+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String exe_hotbill_job(
		String pschema,	
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_hotbill.exe_hotbill_job("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String update_status_hight_hotbill(
											String psmsisdn,
											String psstatus,
											String psid,
											String psuserip
											 ) throws Exception {
		String s ="begin ? :=ADMIN_V2.pkg_alo.update_status_hight_hotbill("
				+"'"+getUserVar("sys_agentcode")+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+psuserip+"',"
				+"'"+psmsisdn+"',"
				+"'"+psstatus+"',"
				+"'"+psid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
}