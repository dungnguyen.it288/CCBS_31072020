package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class infoSearch
extends NEOProcessEx {
	public void run()
	{
		System.out.println("neo.pttb.infoSearch was called");
	}

	
   	public String layds_lichsu_capnhat(	
										String piPAGEID,
										String piREC_PER_PAGE,
										String psnguoith,
										String pstungay,
										String psdenngay
    							   		) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tracuu/lichsu_thaotac/ajax_ketqua_capnhat"
		+"&"+CesarCode.encode("sys_pageid") +"="+piPAGEID
		+"&"+CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
		+"&"+CesarCode.encode("nguoi_dung") + "=" + psnguoith
		+"&"+CesarCode.encode("tu_ngay") + "=" + pstungay
		+"&"+CesarCode.encode("den_ngay") + "=" + psdenngay		
        ;
	}
	
	public NEOCommonData getlichsu_capnhat(
									   String psschema,
									   String psuserid,
									   String psuser,
									   String tungay,
									   String denngay									   
									   ){

	String xs_="";	
	psschema = getUserVar("sys_dataschema");	
	xs_ = xs_ + "begin ? := CCS_ADMIN.PTTB_TRACUU.excel_dsls_capnhat('" 
															+ psschema  +"','"
															+ psuserid  +"','"
															+ psuser  +"','"
															+ tungay +"','"															
															+ denngay +"'); end;";
										
	NEOExecInfo ei_ = new NEOExecInfo(xs_);
    return  new NEOCommonData(ei_);
   }    
}