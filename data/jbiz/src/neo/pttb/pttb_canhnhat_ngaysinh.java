package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_canhnhat_ngaysinh extends NEOProcessEx
{
	public void run()
	{
		System.out.println("QLTN Gach no");
		//funcschema=getSysConst("FuncSchema");
    	//dataschema=getUserVar("sys_dataschema");
    	//userid=getUserVar("userID");
	}
	public String doc_hople(
                   String psKyhoadon,
                   String psLantra
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=PTTB/hthd/cn_ngaysinh/ajax_hople"
  			+ "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
                        + "&" + CesarCode.encode("lantra") + "=" + psLantra;
	}
    public String doc_khonghople(
                   String psKyhoadon,
                   String psLantra
        )
        {
                return "/main?" + CesarCode.encode("configFile")+"=PTTB/hthd/cn_ngaysinh/ajax_khonghople"
                        + "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
                        + "&" + CesarCode.encode("lantra") + "=" + psLantra;
        }
    public String doc_nhaplai(
                   String psKyhoadon,
                   String psLantra
        )
        {
                return "/main?" + CesarCode.encode("configFile")+"=PTTB/hthd/cn_ngaysinh/ajax_nhaplai"
                        + "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
                        + "&" + CesarCode.encode("lantra") + "=" + psLantra;
        }
	public String rec_tonghop(
                   String psKyhoadon,
                   String psLantra
        )
	{
	 	String a= "select  count(decode(invalid,1,invalid,null)) c1, count(decode(invalid,2,invalid,null))"+
				 "  c2 from ccs_common.update_birthday where lanupload="+psLantra;
        System.out.println(a);
               return a;
	}


	public String Value_xacnhanDL(
		String psKyhoadon,
		String psLantra

	)
	{
	String return_="begin ? := "+getSysConst("FuncSchema")+"lib_tiemnk.cnngaysinh_xndl("
			+"'"+psKyhoadon+"',"
			+"'"+psLantra+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(return_);
	return return_;

	}

public String value_reUpload(
	  String psKyhoadon,
	  String psLantra,
        String psma_kh,
        String ngaysinh
      	
	)
	{
		String return_="begin ? := "+getSysConst("FuncSchema")+"lib_tiemnk.reupload_cnngaysinh_file("
			+"'"+psKyhoadon+"',"
			+"'"+psLantra+"',"
			+"'"+psma_kh+"',"
			+"'"+ngaysinh+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(return_);
		return return_;
	}



	public String value_huyUpload(
		String psKyhoadon,
		String psLantra)
	{
		String return_= "declare re_ varchar2(30000); n number; begin begin \n"
					   +"   select count(*) into n from "+getUserVar("sys_dataschema")+"ct_tra_file_"+psKyhoadon
					   +"         where phieu_id is not null and lantra_id="+psLantra+"; \n"
					   +"   if n>0 then re_:='Lan Upload da co phieu gach no vao he thong'; \n "
					   +"   else "
					   +"	 delete from "+getUserVar("sys_dataschema")+"ct_tra_file_"+psKyhoadon
					   +"        where lantra_id="+psLantra+";"
					   +"   re_:=1; end if; "
					   +" exception when others then "
					   +"   re_:='huyUpload|Error: '||TO_CHAR(SQLCODE)||': '||SQLERRM;"
					   +" end; ? := re_; end;";
	    System.out.println(return_);
		return return_;
	}
	public String value_reUpload(
        String psKyhoadon,
        String psLantra,
        String psMa_khs,
        String psMa_tbs,
        String psDonvis,
        String psTientra,
        String psNgayTTs,
        String psHttts,
        String psNguoigachs
	)
	{
		String return_="begin ? := "+getSysConst("FuncSchema")+"qltn_tracuu.reupload_gachnofile("
			+"'"+psKyhoadon+"',"
			+"'"+psLantra+"',"
			+"'"+psMa_khs+"',"
			+"'"+psMa_tbs+"',"
			+"'"+psDonvis+"',"
			+"'"+psTientra+"',"
			+"'"+psNgayTTs+"',"
			+"'"+psHttts+"',"
			+"'"+psNguoigachs+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(return_);
		return return_;
	}
	public String value_gachnofile(
		String psKyhoadon,
		String psLantra,
		String psMomay
	)
	{
		String return_="begin ? := "+getSysConst("FuncSchema")+"qltn_thtoan.thanhtoan6("
			+"'"+psKyhoadon+"',"
			+"'"+psLantra+"',"
			+"'"+psMomay+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(return_);
		return return_;
	}
	public String value_status(
		String psKyhoadon,
		String psLantra
	)
	{
		String return_="begin ? := "+getSysConst("FuncSchema")+"qltn_thtoan.laytt_gachnofile("
			+"'"+psKyhoadon+"',"
			+"'"+psLantra+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(return_);
		return return_;
	}
}
