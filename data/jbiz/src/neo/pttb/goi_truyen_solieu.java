package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class goi_truyen_solieu
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb.goi_truyen_solieu was called");
  }


  
   public String layDSGoiNhom(
		 String psSOMAY ,
		 String psloai                         
		
  	) {
	 String sUserId= getUserVar("userID");
	 String sAgentCode = getUserVar("sys_agentcode");
	 String sDataSchema = getUserVar("sys_dataschema");
	 String sFuncSchema = getSysConst("FuncSchema");
	 String sCommonSchema = getSysConst("CommonSchema");
	 
     String s=    "begin ? := "+sFuncSchema+"PKG_GOI_TRUYEN_SOLIEU.layds_goi_nhom("		
		+"'"+psSOMAY+"'"
		+",'"+psloai+"'"	
		+",'"+sAgentCode+"'"	
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
  
     public String themMoiNhom(
		  String sUserId
		, String sAgentCode
		, String sFuncSchema
		, String sdataSchema
		, String loaiNhom
		, String dsmay 		
		, String ngayDK 
		, String cmt
		, String sogt 
		, String ngayHH	
		, String ghiChu 
		, String chunhom
		, String dsmay_2	
		, String may_cn		
  	) {
	 
     String s=    "begin ? := "+sFuncSchema+"PKG_GOI_TRUYEN_SOLIEU.them_moi_nhom("	
		+" '"+sdataSchema+"'"
		+",'"+sUserId+"'"                           
		+",'"+sAgentCode+"'"                                  		
		+",'"+loaiNhom+"'"
		+",'"+dsmay+"'"
		+",'"+ngayDK+"'"
		+",'"+ngayHH+"'"
		+",'"+cmt+"'"
		+",'"+sogt+"'"
		+",'"+ghiChu+"'"				
		+",'"+may_cn+"'"
		+",'"+chunhom+"'"
		+",'"+dsmay_2+"'"	
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
	
	public String capNhatNhom(
    	  String sUserId
		, String sAgentCode
		, String sFuncSchema
		, String sdataSchema
		, String loaiNhom
		, String dsmay 		
		, String ngayDK 
		, String cmt
		, String sogt 
		, String ngayHH	
		, String ghiChu  
		, String may_cn	 
		, String goi_id	
  	) {

	 
     String s=    "begin ? := "+sFuncSchema+"PKG_GOI_TRUYEN_SOLIEU.cap_nhat_nhom("	
		+" '"+sdataSchema+"'"
		+",'"+sUserId+"'"                           
		+",'"+sAgentCode+"'"                                  		
		+",'"+loaiNhom+"'"
		+",'"+dsmay+"'"
		+",'"+ngayDK+"'"
		+",'"+ngayHH+"'"
		+",'"+cmt+"'"
		+",'"+sogt+"'"
		+",'"+ghiChu+"'"		
		+",'"+may_cn+"'"
		+",'"+goi_id+"'"
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
	
	public String capNhatNhom_alo(
    	  String sUserId
		, String sAgentCode
		, String sFuncSchema
		, String sdataSchema
		, String may_cn	 
		, String loaiNhom
		, String goi_id	
		, String chu_nhom
		, String alo_1	
		, String alo_2			
		, String bs_alo_1	
		, String bs_alo_2		

  	) {

	 
     String s=    "begin ? := "+sFuncSchema+"PKG_GOI_TRUYEN_SOLIEU.cap_nhat_nhom_alo("	
		+" '"+sdataSchema+"'"
		+",'"+sUserId+"'"                           
		+",'"+sAgentCode+"'"
		+",'"+may_cn+"'"		
		+",'"+loaiNhom+"'"
		+",'"+goi_id+"'"
		+",'"+chu_nhom+"'"
		+",'"+alo_1+"'"
		+",'"+alo_2+"'"
		+",'"+bs_alo_1+"'"
		+",'"+bs_alo_2+"'"									
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
	
   public String tachNhom(
		 String	sSomay
		
  	) {
	 String sUserId= getUserVar("userID");
	 String sAgentCode = getUserVar("sys_agentcode");
	 String sDataSchema = getUserVar("sys_dataschema");
	 String sFuncSchema = getSysConst("FuncSchema");
	 String sCommonSchema = getSysConst("CommonSchema");
	 
     String s=    "begin ? := "+sFuncSchema+"PKG_NHOMVNPT.tach_nhom("		
	 
		+" '"+sFuncSchema+"'"
		+",'"+sCommonSchema+"'"                                  
		+",'"+sUserId+"'"                                  
		+",'"+sAgentCode+"'"                                  
		
		+",'"+sSomay+"'"
	
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
   
   
  public String layDSTachNhom(String somay){
 	
 	 return "/main?"+CesarCode.encode("configFile")+"=PTTB/laphd/dangkygoinhom/dsthuebao_ajax"
		+"&"+CesarCode.encode("somay")+"="+somay;
 }
	public String themMoi_dn(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String	dsmay 
		, String	ngayDK 
		, String	ghiChu
		, String capnhat
		, String chuNhom 
  	) {

	 
     String s=    "begin ? := "+sFuncSchema+"PKG_NHOMVNPT.themmoi_nhom_doanhnghiep("		
	 
		+" '"+sDataSchema+"'"                                
		+",'"+sUserId+"'"                                  
		+",'"+sAgentCode+"'"                                  
		+",'"+dsmay+"'"
		+",'"+ngayDK+"'"
		+",'"+ghiChu+"'"		
		+",'"+capnhat+"'"
		+",'"+chuNhom+"'"	
		+");"
        +"end;"
        ;
		System.out.println("Test :" +s);
    return s;
	}

	
 public String LSNhom_Somay(String so_tb){
 	
 	 return "/main?"+CesarCode.encode("configFile")+"=PTTB/laphd/goi_truyen_solieu/lichsu_ajax"
		+"&"+CesarCode.encode("so_tb")+"="+so_tb;
 }
 
 public String LSNhom_Somay_(String so_tb){
 	
 	 return "/main?"+CesarCode.encode("configFile")+"=PTTB/laphd/goi_truyen_solieu_/lichsu_ajax"
		+"&"+CesarCode.encode("so_tb")+"="+so_tb;
 }
	public String layDSGoiNhom_dn(
		 String psSOMAY                         
		
  	) {
	 String sDataSchema = getUserVar("sys_dataschema");
	 String sFuncSchema = getSysConst("FuncSchema"); 
     String s=    "begin ? := "+sFuncSchema+"PKG_NHOMVNPT.layds_nhom_dn("	
		+"'"+sDataSchema+"'"
		+",'"+psSOMAY+"'"                               
		+");"
        +"end;"
        ;
		System.out.println("luattd :" +s);
    return s;
	}
	public String huynhom(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String sCommonSchema	 
		, String idnhom
		, String loainhom
	    , String may_cn		
  	) {

	 
     String s=    "begin ? := "+sFuncSchema+"PKG_GOI_TRUYEN_SOLIEU.huy_nhom("		
	 
		+" '"+sCommonSchema+"'"                             
		+",'"+sUserId+"'"                                  
		+",'"+sAgentCode+"'"                                  		
		+",'"+idnhom+"'"
		+",'"+loainhom+"'"
		+",'"+may_cn+"'"		
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
}














