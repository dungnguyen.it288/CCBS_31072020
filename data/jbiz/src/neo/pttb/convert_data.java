package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSetMetaData;

import javax.sql.RowSet;

import neo.smartui.common.CesarCode;
import neo.smartui.process.NEOProcessEx;
import neo.smartui.report.NEOCommonData;
import neo.smartui.report.NEOExecInfo;

public class convert_data extends NEOProcessEx {
	  public void run() {
    System.out.println("neo.promo_data was called");
    }
	public String create_promo ( String psschema ,
		                                String psNamepromo,
										String psTable,
										String psTablebil,
										String psFunc,
										String psFuncbil,
										String psStatus,
										String psUserid
		                              )
	{
		 psTable= psTable.replace("'","''");
		 String s=    "begin ? := admin_v2.promo_data.create_promotion('"
						+ getUserVar("sys_dataschema")  	
						+"','"+getUserVar("userID")
						+"','"+psNamepromo+"'"
						+",'"+psTable +"'"
						+",'"+psTablebil +"'"	
						+",'"+psFunc +"'"
						+",'"+psFuncbil +"'"	
						+",'"+psStatus  +"'"
						+");"
						+"end;"
						;
       System.out.println(s);
	   NEOExecInfo nei_ = new NEOExecInfo(s);
	   return s;  

	}
		public String update_promo (    String psschema ,
										String psIdpromo,
		                                String psNamepromo,
										String psTable,
										String psTablebil,
										String psFunc,
										String psFuncbil,
										String psStatus,
										String psUserid
		                              )
	{
		 psTable= psTable.replace("'","''");
		 String s=    "begin ? := admin_v2.promo_data.update_promotion('"
						+ getUserVar("sys_dataschema")  	
						+"','"+getUserVar("userID")
						+"','"+psIdpromo+"'"
						+",'"+psNamepromo+"'"
						+",'"+psTable +"'"
						+",'"+psTablebil +"'"	
						+",'"+psFunc +"'"
						+",'"+psFuncbil +"'"	
						+",'"+psStatus  +"'"
						+");"
						+"end;"
						;
       System.out.println(s);
	   NEOExecInfo nei_ = new NEOExecInfo(s);
	   return s;  

	}
	public String delete_promo (    String psschema ,
									String psIdpromo,
									String psUserid
		                              )
	{
	   String s=    "begin ? := admin_v2.promo_data.delete_promotion('"
						+ getUserVar("sys_dataschema")  	
						+"','"+getUserVar("userID")
						+"','"+psIdpromo+"'"
						+");"
						+"end;"
						;
	   System.out.println(s);
	   NEOExecInfo nei_ = new NEOExecInfo(s);
	   return s; 
	}
	public String promotion_list()
	{
		String s= "/main?"+CesarCode.encode("configFile")+"=ccbs_promotion/convert_data/promotion_list_ajax" ;
		return s;
	}
	public String get_promo_list( String sType, String psMonth)
	{
		String s;
		s= "/main?" + CesarCode.encode("configFile")+"=ccbs_promotion/convert_data/promo_list_step1_ajax"
  			+ "&" + CesarCode.encode("psType") + "=" + sType
			+ "&" + CesarCode.encode("psMonth") + "=" + psMonth;
		System.out.println("ngoc" + s);
		return s;
	}
	public String get_promo_list_not( String sType, String psMonth)
	{
		String s;
		s= "/main?" + CesarCode.encode("configFile")+"=ccbs_promotion/convert_data/promo_list_step2_ajax"
  			+ "&" + CesarCode.encode("psType") + "=" + sType
			+ "&" + CesarCode.encode("psMonth") + "=" + psMonth;
		System.out.println("ngoc" + s);
		return s;
	}
	public String get_promo_list_check()
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_promotion/convert_data/promo_list_check";
	}
	public NEOCommonData get_data_promo(
									String psLogId,
									String psListPromo,
									String psMonth,
									String psUser,
									String psIp,
									String psBk
									 ) {

    String s=    "begin ? := ccs_common.convert_data.get_promo_data('"
						+psLogId
						+"','"+psListPromo
						+"','"+psMonth+"'"
						+",'"+psUser +"'"
						+",'"+psIp +"'"	
						+",'"+psBk +"'"
						+");"
						+"end;"        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
	}  
	
	public String get_promo_by_id(String psPromoId
	)
	{
	    String s=    "begin ? := admin_v2.promo_data.promotion_by_id("
						+psPromoId
						+");"
						+"end;"
						;
       System.out.println(s);
	   NEOExecInfo nei_ = new NEOExecInfo(s);
	   return s;   
	}
	//// Lay ID thuc hien 
	public NEOCommonData get_logId() {
			String s=    "begin ? := ccs_common.convert_data.get_log_id;end;" ;    
			System.out.println(s); 	
			NEOExecInfo nei_ = new NEOExecInfo(s);
			nei_.setDataSrc("VNPBilling");
			return new NEOCommonData(nei_);
	}  
	////
	public String get_log_data(String psLogID)
	{
       String s = "/main?" + CesarCode.encode("configFile")+"=ccbs_promotion/convert_data/promo_log_list"
	   + "&" + CesarCode.encode("LogID") + "=" + psLogID; 
	   return s;
	}
	////
	public String get_result_test(  String psMoth,
										String psPromoID,
										String psType,
										String psUser
	)
	{
       String s = "/main?" + CesarCode.encode("configFile")+"=ccbs_promotion/convert_data/promo_result_test"
	   + "&" + CesarCode.encode("psMoth") + "=" + psMoth
	   + "&" + CesarCode.encode("psPromoID") + "=" + psPromoID
	   + "&" + CesarCode.encode("psType") + "=" + psType; 
	   return s;   
	}
	public String get_promo_list_user_check()
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_promotion/convert_data/promo_list_user_permis";
	}
	public String get_user()
	{
       String s = "/main?" + CesarCode.encode("configFile")+"=ccbs_promotion/convert_data/promo_list_user"
	   ; 
	   return s;   
	}
	public String allow_user(String psschema ,
							String psUserPer,
							String psPromoList,
							String psConfigPer,							
							String psUserid)
	{
		String s=    "begin ? := admin_v2.promo_data.set_user_permission('"
						+psschema
						+"','"+psUserPer
						+"','"+psPromoList
						+"','"+psConfigPer 
						+"','"+psUserid 
						+"');"
						+"end;"
						;
       System.out.println(s);
	   NEOExecInfo nei_ = new NEOExecInfo(s);
	   return s;
	}
	//Check theo so luong thue bao convert
	public String get_check( String chuky, String pscheck)
	{
		String s;
		s= "/main?" + CesarCode.encode("configFile")+"=ccbs_promotion/convert_data/promo_listcheck"
  			+ "&" + CesarCode.encode("pscheck") + "=" + pscheck
			+ "&" + CesarCode.encode("psMonth") + "=" + chuky;
		System.out.println("ngoc" + s);
		return s;
		
	}
	//Check theo so tien theo thue bao convert
	public String get_check_tien( String chuky, String pscheck)
	{
		String s;
		s= "/main?" + CesarCode.encode("configFile")+"=ccbs_promotion/convert_data/promo_listcheck"
  			+ "&" + CesarCode.encode("pscheck") + "=" + pscheck
			+ "&" + CesarCode.encode("psMonth") + "=" + chuky;
		System.out.println("ngoc" + s);
		return s;
		
	}
	
	public NEOCommonData connectDB() {
			String s=    "begin ? := ccs_common.convert_data.connectDB; end;" ;    
			System.out.println(s); 	
			NEOExecInfo nei_ = new NEOExecInfo(s);
			nei_.setDataSrc("VNPBilling");
			return new NEOCommonData(nei_);
	}  
	public String get_laydskm(String check_money)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_promotion/convert_data/dskm"
			+"&"+CesarCode.encode("check_money")+"="+check_money;
	}
}