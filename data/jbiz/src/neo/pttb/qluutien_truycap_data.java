package neo.pttb;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import cosaldap.Base64.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;


public class qluutien_truycap_data
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb.qluutien_truycap_data was called");
  }   
	public String cap_nhat(
		  String sUserId
		, String sIP
		, String msisdn
		, String muc_uutien 				
		, String tu_ngay
		, String den_ngay 		
		, String ghichu		
  	) throws Exception {
	System.out.println("------------------------Cap nhat---------------------");
     String s=    "begin ? := admin_v2.pkg_uutien_truycap_data.cap_nhat("	
		+" '"+sUserId+"'"
		+",'"+sIP+"'"                           
		+",'"+msisdn+"'"  
		+",'"+muc_uutien+"'"
		+",'"+tu_ngay+"'"		
		+",'"+den_ngay+"'"
		+",'"+ConvertFont.UtoD(ghichu)+"'"		
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}	
	
	public String excute_file_upload(
		String uploadId
		, String sUserId
		, String sIP 	
  	) throws Exception {
	System.out.println("------------------------Thuc hien cap nhat/xoa----------------------");
     String s=    "begin ? := admin_v2.pkg_uutien_truycap_data.dky_uutien_truycap_data("	
		+" '"+uploadId+"'"
		+",'"+sUserId+"'"
		+",'"+sIP+"'"					
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}

	public String xoa(
		  String sUserId
		, String sIP
		, String msisdn		
		, String ghiChu 	
  	) throws Exception {
	System.out.println("------------------------Xoa----------------------");
     String s=    "begin ? := admin_v2.pkg_uutien_truycap_data.xoa("	
		+" '"+sUserId+"'"
		+",'"+sIP+"'"
		+",'"+msisdn+"'"                           
		+",'"+ConvertFont.UtoD(ghiChu)+"'"						
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	public String checkRole(
		  String sUserId	
  	) throws Exception {
	System.out.println("------------------------Cap nhat---------------------");
     String s=    "begin ? := admin_v2.pkg_uutien_truycap_data.check_role_user("	
		+" '"+sUserId+"'"	
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	public String loadthongtintb(
		  String sUserId
		, String sIP
		, String msisdn			
  	) throws Exception {
	System.out.println("------------------------Load thong tin tb----------------------");
     String s=    "begin ? := admin_v2.pkg_uutien_truycap_data.loadthongtintb("	
		+" '"+sUserId+"'"
		+",'"+sIP+"'"
		+",'"+msisdn+"'"                           					
		+");"
        +"end;"
        ;
		String rs = this.reqValue("", s);
		System.out.println("-------------RS: " + rs);
		return rs;
	}
	
}














