package neo.pttb;
import neo.smartui.common.*;
import neo.smartui.process.*;

import java.util.*;
import javax.sql.*;
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import neo.smartui.common.CesarCode;
import neo.smartui.report.*;
import com.svcon.jdbf.*;
import javax.sql.RowSet;
import sun.misc.*;
import java.net.HttpURLConnection;
import java.net.URL;
public class RegKaro extends NEOProcessEx {
    // Dang Ky:
	public String add_karo(	 
							String pc_sotb,
							String pc_goi,
							String pc_madn,
							String pc_tendn,
							String pc_nguoidaidien,
							String pc_cmt_dd,
							String pc_sdt_dd,
							String pc_ngaysinh_dd,
							String pc_ma_so_thue,
							String pc_diachi,
							String pc_nganhang_id,
							String pc_tai_khoan,
							String pc_loaikh_id,
							String pc_nghenghiep,
							String pc_ghi_chu,
							String pc_loai,
							String pc_cuoc_ld,
							String pc_cuoc_th,
							String pc_userid,
							String pc_userip,
							String pc_agentcode       )
		throws Exception {
			String result =null;
			String loai ="";
				
		String s = " begin ? := admin_v2.pk_subscriber_karo.sf_regis_karo("
                + "'" + pc_sotb + "'"
				+ ",'"+ pc_goi + "'"
                + ",'"+ pc_madn + "'" 
                + ",'"+ConvertFont.UtoD(pc_tendn) +"'"
				+ ",'"+ConvertFont.UtoD(pc_nguoidaidien) +"'"
				+ ",'"+pc_cmt_dd +"'"
				+ ",'"+pc_sdt_dd   +"'" 
				+ ",'"+pc_ngaysinh_dd   +"'"
				+ ",'"+pc_ma_so_thue   +"'"
				+ ",'"+ConvertFont.UtoD(pc_diachi)  +"'"
				+ ",'"+pc_nganhang_id +"'" 
				+ ",'"+pc_tai_khoan  +"'"
				+ ",'"+pc_loaikh_id  +"'"
				+ ",'"+ConvertFont.UtoD(pc_nghenghiep)  +"'"
				+ ",'"+ConvertFont.UtoD(pc_ghi_chu)+"'" 
				+ ",'"+pc_loai  +"'"
				+ ",'"+pc_cuoc_ld  +"'"
				+ ",'"+pc_cuoc_th  +"'"
				+ ",'"+pc_userid   +"'" 
				+ ",'"+pc_userip   +"'" 
				+ ",'"+pc_agentcode  +"'" 
                + ");"
                + "end;";
		System.out.println(s);
        result = this.reqValue("", s);
		System.out.println("--------------->>>result = "+result);
		
		if(result.equals("1")){
		try {
			String body ="";			
			Date dNow = new Date( );
			String i_company_code ="";
			String i_delegate_number ="";
			String i_package_id ="";
			SimpleDateFormat ft = new SimpleDateFormat ("dd/MM/yyyy");
			if (pc_goi.equals("3277")){ i_package_id = "BASIC";}
			else if (pc_goi.equals("3278")){i_package_id = "ADVANCE";}
			//new SimpleDateFormat ("yyyy:MM:dd hh:mm:ss");
			
			String TimeStamp = ft.format(dNow);			
			System.out.println("--------------->>>i_package_id = "+i_package_id);
			String api_rs ="";
			
			body = "{\"ma_dn\":\""+pc_madn+"\",\"name\":\""+pc_tendn+"\",\"addr\":\""+pc_diachi+"\",\"so_tai_khoan\":\""+pc_tai_khoan+"\",\"ngan_hang\":\""+pc_ma_so_thue+"\",\"dd_ho_ten\":\""+pc_nguoidaidien+"\",\"dd_ngay_sinh\":\""+pc_ngaysinh_dd			
			 +"\",\"dd_mobile\":\""+pc_sdt_dd+"\",\"nganh_nghe\":\""+pc_nghenghiep+"\",\"package_id\":\""+i_package_id+"\",\"master\":\""+pc_sotb+"\",\"create_date\":\""+TimeStamp+"\"}"; 
			System.out.println("--------------->>>body = "+body);			
		    api_rs = apiPost(body,"createBusiness");
			System.out.println("--------------->>>api_rs = "+api_rs);
			try { i_company_code = getValue(api_rs,"company_code","int"); } 
			catch (Exception ex){i_company_code = "";}
			try { i_delegate_number = getValue(api_rs,"delegate_number","str"); } 
			catch (Exception ex){i_delegate_number = "";}			
			System.out.println("--------------->>>i_company_code = "+i_company_code);			
			System.out.println("--------------->>>i_delegate_number = "+i_delegate_number);
			String sx_="";
			if(i_company_code.equals("") || i_delegate_number.equals(""))
				{
						return "Dang ky thanh cong! Chua dong bo duoc doanh nghiep sang Karo";
				}
			else{
				
				sx_ ="begin ? :=admin_v2.pk_subscriber_karo.capnhat_karo('"+pc_madn+"','"+i_company_code+"','"+i_delegate_number+"'); end;";
					this.reqValue("", sx_);					
			}
		
			}catch (Exception ex){				
				System.out.println("Exception 2 : " + ex.getMessage());
			ex.printStackTrace();
			}	
		
		}
		
		return result;
    } 
	
	public String add_karo_bk(	 
							String pc_sotb,
							String pc_goi,
							String pc_madn,
							String pc_tendn,
							String pc_nguoidaidien,
							String pc_cmt_dd,
							String pc_sdt_dd,
							String pc_ngaysinh_dd,
							String pc_ma_so_thue,
							String pc_diachi,
							String pc_nganhang_id,
							String pc_tai_khoan,
							String pc_loaikh_id,
							String pc_nghenghiep,
							String pc_ghi_chu,
							String pc_loai,
							String pc_cuoc_ld,
							String pc_cuoc_th,
							String pc_userid,
							String pc_userip,
							String pc_agentcode       )
		throws Exception {
				String result =null;
				String loai ="";			
				String body ="";			
				Date dNow = new Date( );
				String i_company_code ="";
				String i_delegate_number ="";
				String i_package_id ="";
				SimpleDateFormat ft = new SimpleDateFormat ("dd/MM/yyyy");
				if (pc_goi.equals("3277")){ i_package_id = "BASIC";}
				else if (pc_goi.equals("3278")){i_package_id = "ADVANCE";}
				//new SimpleDateFormat ("yyyy:MM:dd hh:mm:ss");
				
				String TimeStamp = ft.format(dNow);			
				System.out.println("--------------->>>i_package_id = "+i_package_id);
				String api_rs ="";
				
				body = "{\"ma_dn\":\""+pc_madn+"\",\"name\":\""+pc_tendn+"\",\"addr\":\""+pc_diachi+"\",\"so_tai_khoan\":\""+pc_tai_khoan+"\",\"ngan_hang\":\""+pc_ma_so_thue+"\",\"dd_ho_ten\":\""+pc_nguoidaidien+"\",\"dd_ngay_sinh\":\""+pc_ngaysinh_dd			
				+"\",\"dd_mobile\":\""+pc_sdt_dd+"\",\"nganh_nghe\":\""+pc_nghenghiep+"\",\"package_id\":\""+i_package_id+"\",\"master\":\""+pc_sotb+"\",\"create_date\":\""+TimeStamp+"\"}"; System.out.println("--------------->>>body = "+body);			
				api_rs = apiPost(body,"createBusiness");
				System.out.println("--------------->>>api_rs = "+api_rs);
				try { i_company_code = getValue(api_rs,"company_code","int"); } 
				catch (Exception ex){i_company_code = "";}
				try { i_delegate_number = getValue(api_rs,"delegate_number","str"); } 
				catch (Exception ex){i_delegate_number = "";}			
				System.out.println("--------------->>>i_company_code = "+i_company_code);			
				System.out.println("--------------->>>i_delegate_number = "+i_delegate_number);
				String sx_="";
					if(i_company_code.equals("") || i_delegate_number.equals(""))
						{
								return "Loi khoi tao doanh nghiep Karo";
						}
					else{
						try {
								String s = " begin ? := admin_v2.pk_subscriber_karo.sf_regis_karo("
										+ "'" + pc_sotb + "'"
										+ ",'"+ pc_goi + "'"
										+ ",'"+ pc_madn + "'" 
										+ ",'"+ConvertFont.UtoD(pc_tendn) +"'"
										+ ",'"+ConvertFont.UtoD(pc_nguoidaidien) +"'"
										+ ",'"+pc_cmt_dd +"'"
										+ ",'"+pc_sdt_dd   +"'" 
										+ ",'"+pc_ngaysinh_dd   +"'"
										+ ",'"+pc_ma_so_thue   +"'"
										+ ",'"+ConvertFont.UtoD(pc_diachi)  +"'"
										+ ",'"+pc_nganhang_id +"'" 
										+ ",'"+pc_tai_khoan  +"'"
										+ ",'"+pc_loaikh_id  +"'"
										+ ",'"+ConvertFont.UtoD(pc_nghenghiep)  +"'"
										+ ",'"+ConvertFont.UtoD(pc_ghi_chu)+"'" 
										+ ",'"+pc_loai  +"'"
										+ ",'"+pc_cuoc_ld  +"'"
										+ ",'"+pc_cuoc_th  +"'"
										+ ",'"+i_company_code  +"'"
										+ ",'"+i_delegate_number  +"'"				
										+ ",'"+pc_userid   +"'" 
										+ ",'"+pc_userip   +"'" 
										+ ",'"+pc_agentcode  +"'" 
										+ ");"
										+ "end;";
							System.out.println(s);
							result = this.reqValue("", s);
							System.out.println("--------------->>>result = "+result);
						}catch (Exception ex){				
							System.out.println("Exception 2 : " + ex.getMessage());
							ex.printStackTrace();
						}						
										
					}			
		
		return result;
    } 
	
	
	//cap nhat:
	
	 public String edit_karo(	 
							String pc_madn,
							String pc_goi,
							String pc_sotb,							
							String pc_userid,
							String pc_userip,
							String pc_agentcode       )
		throws Exception {
			String result =null;
			String update =null;		
			String body ="";			
			String i_package_id ="";			
			if (pc_goi.equals("3277")){ i_package_id = "2";}
			else if (pc_goi.equals("3278")){i_package_id = "1";}					
			System.out.println("--------------->>>i_package_id = "+i_package_id);
			String api_rs ="";
			
			body = "{\"company_code\":\""+pc_madn+"\",\"delegate_number\":\""+pc_sotb+"\",\"type\":\"2\",\"sub_type\":\""+i_package_id+"\"}"; 
			System.out.println("--------------->>>body = "+body);			
		    api_rs = apiPost(body,"updateBusiness");
			//returnCode
			System.out.println("--------------->>>api_rs = "+api_rs);
			try { update = getValue(api_rs,"returnCode","int"); } 
			catch (Exception ex){update = "";}		
			System.out.println("--------------->>>update = "+update);
			if (update.equals("0")){
			try {	
					String s = " begin ? := admin_v2.pk_subscriber_karo.sf_edit_karo("
									+ "'" + pc_madn + "'"
									+ ",'"+ pc_goi + "'"
									+ ",'"+ pc_sotb + "'"                 
									+ ",'"+pc_userid   +"'" 
									+ ",'"+pc_userip   +"'" 
									+ ",'"+pc_agentcode  +"'" 
									+ ");"
									+ "end;";
						System.out.println(s);
						result = this.reqValue("", s);						
						System.out.println("--------------->>>result = "+result);
						//return result;
				}catch (Exception ex){				
					System.out.println("Exception 2 : " + ex.getMessage());
					ex.printStackTrace();
				}
			}
			else 
			{ return "Loi update API"; }
		
		return result;
    } 
	
	//Huy dich vu	
	/*
	public String Del_karo_bk ( String pc_sotb,
							String pc_madn,
							String goicuoc,
							String matinh,							
							String UserIP ) 
					throws Exception{ 
			
			String result = "";			
			String update =null;		
			String body ="";			
			String api_rs ="";
			
			body = "{\"company_code\":\""+pc_madn+"\",\"delegate_number\":\""+pc_sotb+"\",\"type\":\"1\",\"sub_type\":\"2\"}"; 
			System.out.println("--------------->>>body = "+body);			
		    api_rs = apiPost(body,"updateBusiness");
			//returnCode
			System.out.println("--------------->>>api_rs = "+api_rs);
			try { update = getValue(api_rs,"returnCode","int"); } 
			catch (Exception ex){update = "";}		
			System.out.println("--------------->>>update = "+update);
			if (update.equals("0")){
				try {		
					String s = "begin ? := admin_v2.pk_subscriber_karo.sf_delete_karo("
						+ "'" + pc_sotb + "'"
						+ ",'" + pc_madn + "'"
						+ ",'" + goicuoc + "'"
						+ ",'" + getUserVar("userID") + "'"
						+ ",'" + UserIP + "'"
						+ ",'" + matinh + "'" 
						+ ");"
						+ "end;";
					System.out.println(s);
					result = this.reqValue("", s);		
				}catch (Exception ex){				
					System.out.println("Exception 2 : " + ex.getMessage());
					ex.printStackTrace();
				}
			}
			else 
			{ return "Loi ko goi dc API Karo"; }		
		return result;
    } 
	*/
	public String Del_karo ( String pc_sotb,
							String pc_madn,
							String goicuoc,
							String matinh,							
							String UserIP ) 
					throws Exception{ 
			
			String result = "";			
			String update =null;		
			String body ="";			
			String api_rs ="";
			
			String s = "begin ? := admin_v2.pk_subscriber_karo.sf_delete_karo("
						+ "'" + pc_sotb + "'"
						+ ",'" + pc_madn + "'"
						+ ",'" + goicuoc + "'"
						+ ",'" + getUserVar("userID") + "'"
						+ ",'" + UserIP + "'"
						+ ",'" + matinh + "'" 
						+ ");"
						+ "end;";
					System.out.println(s);
					result = this.reqValue("", s);			
			
			if (result.equals("1")){
				try {
						
					body = "{\"company_code\":\""+pc_madn+"\",\"delegate_number\":\""+pc_sotb+"\",\"type\":\"1\",\"sub_type\":\"2\"}"; 
				System.out.println("--------------->>>body = "+body);			
				api_rs = apiPost(body,"updateBusiness");
				//returnCode
				System.out.println("--------------->>>api_rs = "+api_rs);
				try { update = getValue(api_rs,"returnCode","int"); } 
				catch (Exception ex){update = "";}		
				System.out.println("--------------->>>update = "+update);
				if (!update.equals("0")){
					return "Huy thanh cong! Chua dong bo duoc doanh nghiep sang Karo";
				}
							
				}catch (Exception ex){				
					System.out.println("Exception 2 : " + ex.getMessage());
					ex.printStackTrace();
				}
			}				
		return result;
    } 
	
	private String getValue(String content, String key , String type) {
		String result = content;
		int count = result.indexOf(key, 0);
		if (count < 1) {
			return "";
		}
		if (type.equals("int")) {
				result = result.substring(result.indexOf("\":", count) + 2, result.indexOf(",", count));
			}
			if (type.equals("str")) {
				result = result.substring(result.indexOf("\":\"", count) +3 , result.lastIndexOf("\""));
				}
		return result;	

	}	
// API POST ket noi Karo http://10.193.120.7:9180/3rd/api/v1.0/
		
	public static String apiPost(String input, String method) {
				String urlHttp="http://10.193.120.7:9180/3rd/api/v1.0/";
		        StringBuffer jsonString;
		        try {
		            URL url = new URL(urlHttp + method);
		            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		            conn.setDoOutput(true);
		            conn.setRequestMethod("POST");
		            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

		            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
		            writer.write(input);
		            writer.close();
		            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		            jsonString = new StringBuffer();
		            String line;
		            while ((line = br.readLine()) != null) {
		                jsonString.append(line);
		            }
		            br.close();
		            conn.disconnect();

		        } catch (Exception e) {
		            throw new RuntimeException(e.getMessage());
		        }
		        return jsonString.toString();
		}
	  
	
}
