package neo.pttb;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import cosaldap.Base64.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;
import java.io.*;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.ResultSetMetaData;
import javax.sql.RowSet;
public class cauhinh_goikm extends NEOProcessEx {
  public void run() {
    System.out.println("neo.goi_khuyenmai was called");
  }
  	public String add_PkgDispatch(String dispatch_name  ,
								  String agent_name      ,
								  String sub_min         ,
								  String sub_max         ,
								  String is_main_group   ,
								  String subtype_main_id ,
								  String subtype_main_name,
								  String main_group       ,
								  String date_from        ,
								  String date_to          ,
								  String status           ,
								  String person_operator  ,
								  String note             ,
								  String dispatch_tmp_id  ,
								  String name_group       ,
								  String link_document    ,
								  String link_dispatch    ,
								  String psCVGH    ,
								  String txtEx    ,
								  String txtEx_name )throws Exception 
	{
	String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.add_pkg_dispatch('"+ConvertFont.UtoD(dispatch_name)  +"','"+
																	  agent_name      +"','"+
																	  sub_min          +"','"+
																	  sub_max          +"','"+
																	  is_main_group    +"','"+
																	  subtype_main_id  +"','"+
																	  subtype_main_name +"','"+
																	  main_group        +"','"+
																	  date_from         +"','"+
																	  date_to           +"','"+
																	  status            +"','"+
																	  person_operator   +"','"+
																	  ConvertFont.UtoD(note)              +"','"+
																	  dispatch_tmp_id   +"','"+
																	  ConvertFont.UtoD(name_group)        +"','"+
																	  link_document     +"','"+
															          link_dispatch     +"','"+
																	  psCVGH    +"','"+
																	  txtEx    +"','"+
																	  txtEx_name +"','"+   
																	  getUserVar("userIP") 
																	  + "'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
	  	public String add_PkgDispatch_tmp(String dispatch_id,
								  String dispatch_name  ,
								  String agent_name      ,
								  String sub_min         ,
								  String sub_max         ,
								  String is_main_group   ,
								  String subtype_main_id ,
								  String subtype_main_name,
								  String main_group       ,
								  String date_from        ,
								  String date_to          ,
								  String status           ,
								  String person_operator  ,
								  String note             ,
								  String dispatch_tmp_id  ,
								  String name_group       ,
								  String link_document    ,
								  String link_dispatch    ,
								  String psCVGH    ,
								  String txtEx    ,
								  String txtEx_name    )throws Exception 
	{
	String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.add_pkg_dispatch_tmp('"+ dispatch_id +"','"+
																	  ConvertFont.UtoD(dispatch_name)  +"','"+
																	  agent_name      +"','"+
																	  sub_min          +"','"+
																	  sub_max          +"','"+
																	  is_main_group    +"','"+
																	  subtype_main_id  +"','"+
																	  subtype_main_name +"','"+
																	  main_group        +"','"+
																	  date_from         +"','"+
																	  date_to           +"','"+
																	  status            +"','"+
																	  person_operator   +"','"+
																	  ConvertFont.UtoD(note)              +"','"+
																	  dispatch_tmp_id   +"','"+
																	  ConvertFont.UtoD(name_group)        +"','"+
																	  link_document     +"','"+
															          link_dispatch     +"','"+
																	  psCVGH    +"','"+
																	  txtEx    +"','"+
																	  txtEx_name +"','"+ 
																	  getUserVar("userIP") 
																	  + "'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
	public String edit_PkgDispatch(String dispatch_id   ,
								  String dispatch_name  ,
								  String agent_name      ,
								  String sub_min         ,
								  String sub_max         ,
								  String is_main_group   ,
								  String subtype_main_id ,
								  String subtype_main_name,
								  String main_group       ,
								  String date_from        ,
								  String date_to          ,
								  String status           ,
								  String person_operator  ,
								  String note             ,
								  String dispatch_tmp_id  ,
								  String name_group      ,
								  String link_document    ,
								  String link_dispatch   ,
								  String psCVGH    ,
								  String txtEx    ,
								  String txtEx_name     )throws Exception 
	{
	String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.edit_pkg_dispatch('"+dispatch_id  +"','"+
																	  ConvertFont.UtoD(dispatch_name)  +"','"+
																	  agent_name      +"','"+
																	  sub_min          +"','"+
																	  sub_max          +"','"+
																	  is_main_group    +"','"+
																	  subtype_main_id  +"','"+
																	  subtype_main_name +"','"+
																	  main_group        +"','"+
																	  date_from         +"','"+
																	  date_to           +"','"+
																	  status            +"','"+
																	  person_operator   +"','"+
																	  ConvertFont.UtoD(note)              +"','"+
																	  dispatch_tmp_id   +"','"+
																	  ConvertFont.UtoD(name_group)        +"','"+
																	  link_document     +"','"+
															          link_dispatch     +"','"+
																	  psCVGH    +"','"+
																	  txtEx    +"','"+
																	  txtEx_name +"','"+ 
																	  getUserVar("userIP")  
																	  + "'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
	public String del_PkgDispatch(String dispatch_id ) throws Exception 
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.del_pkg_dispatch('"+  dispatch_id           +"','"+
																	  +  getUserVar("userID")  +"','"+
																	  +  getUserVar("userIP")  
																	  + "'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
	public String add_PromoPackage(String package_name      ,
										String subtype_name                   ,
										String	agent_name                     ,
										String	date_from                      ,
										String	date_to                        ,
										String	package_min                    ,
										String	package_max                    ,
										String	package_promo                  ,
										String	new_contract                   ,
										String	old_contract                   ,
										String	is_main_group                  ,
										String	package_tmp_id                 ,
										String	status                         ,
										String	manipulate                     ,
										String  package_trans                  ,
										String	reg_package_add                ,
										String	reg_day_from_1                 ,
										String	reg_day_to_1                   ,
										String	reg_day_per_1                  ,
										String	reg_day_from_2                 ,
										String	reg_day_to_2                   ,
										String	reg_day_per_2                  ,
										String	can_day_from_1                 ,
										String	can_day_to_1                   ,
										String	can_day_per_1                  ,
										String	can_day_from_2                 ,
										String	can_day_to_2                   ,
										String	can_day_per_2                  ,
										String	package_fee                    ,
										String	register_type                  ,
										String	condiction_ext                 ,
										String	main_group                     ,
										String	note                           ,
										String	subtype_id                     ,
										String	psDN                           ,
										String  active_date                    ,
										String  chance_date                    ,
										String  recovery_date                  ,
										String  trans_date                     ,
										String  dispatch_id                    ,
										String  isRegis		                   ,
										String  psLimitMinus                   ,
										String  psTypepkg                      ,
										String  psStylepkg                     ,
										String  psMonthUse                     ,
										String  psSMS                 	       ,
										String  psLoaiAlo) throws Exception 
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.add_promo_package('"+ConvertFont.UtoD(package_name) +"','"+
																		subtype_name +"','"+                  
																		agent_name   +"','"+                  
																		date_from    +"','"+                  
																		date_to      +"','"+                  
																		package_min  +"','"+                  
																		package_max  +"','"+ 
																		package_promo +"','"+ 																		
																		new_contract +"','"+                  
																		old_contract +"','"+                  
																		is_main_group+"','"+                  
																		package_tmp_id  +"','"+               
																		status       +"','"+                  
																		manipulate   +"','"+                  
																		package_trans+"','"+                  
																		reg_package_add   +"','"+             
																		reg_day_from_1    +"','"+             
																		reg_day_to_1      +"','"+             
																		reg_day_per_1     +"','"+             
																		reg_day_from_2    +"','"+             
																		reg_day_to_2      +"','"+              
																		reg_day_per_2     +"','"+             
																		can_day_from_1    +"','"+             
																		can_day_to_1      +"','"+             
																		can_day_per_1     +"','"+             
																		can_day_from_2    +"','"+             
																		can_day_to_2      +"','"+             
																		can_day_per_2     +"','"+             
																		package_fee       +"','"+             
																		register_type     +"','"+             
																		condiction_ext    +"','"+             
																		getUserVar("userID")  +"','"+ 
																		ConvertFont.UtoD(note)  			  +"','"+ 
																		subtype_id  	  +"','"+ 
																		main_group  	  +"','"+ 
																		psDN  	          +"','"+ 
																		active_date  	  +"','"+ 
																		chance_date  	  +"','"+ 
																		recovery_date  	  +"','"+ 
																		trans_date        +"','"+
																		dispatch_id       +"','"+
																		isRegis      	  +"','"+ 
																		psLimitMinus  	  +"','"+ 
																		psTypepkg         +"','"+
																		psStylepkg        +"','"+
																		psMonthUse        +"','"+
																		psSMS		 	  +"','"+
																		psLoaiAlo 		  + "'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
	public String edit_PromoPackage(String package_id                         ,
										String package_name                   ,
										String subtype_name                   ,
										String	agent_name                     ,
										String	date_from                      ,
										String	date_to                        ,
										String	package_min                    ,
										String	package_max                    ,
										String	package_promo                  ,
										String	new_contract                   ,
										String	old_contract                   ,
										String	is_main_group                  ,
										String	package_tmp_id                 ,
										String	status                         ,
										String	manipulate                     ,
										String  package_trans                  ,
										String	reg_package_add                ,
										String	reg_day_from_1                 ,
										String	reg_day_to_1                   ,
										String	reg_day_per_1                  ,
										String	reg_day_from_2                 ,
										String	reg_day_to_2                   ,
										String	reg_day_per_2                  ,
										String	can_day_from_1                 ,
										String	can_day_to_1                   ,
										String	can_day_per_1                  ,
										String	can_day_from_2                 ,
										String	can_day_to_2                   ,
										String	can_day_per_2                  ,
										String	package_fee                    ,
										String	register_type                  ,
										String	condiction_ext                 ,
										String	main_group                     ,
										String	note                           ,
										String	subtype_id                     ,
										String	psDN                           ,
										String  active_date                    ,
										String  chance_date                    ,
										String  recovery_date                  ,
										String  trans_date                     ,
										String  dispatch_id                    ,
										String  isRegis		                   ,
										String  psLimitMinus                   ,
										String  psTypepkg                      ,
										String  psStylepkg					   ,
										String  psMonthUse                     ,
										String  psSMS                 	,
										String  psLoaiAlo) throws Exception
	{
	String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.edit_promo_package('"+package_id  +"','"+
																		ConvertFont.UtoD(package_name) +"','"+
																		subtype_name +"','"+                  
																		agent_name   +"','"+                  
																		date_from    +"','"+                  
																		date_to      +"','"+                  
																		package_min  +"','"+                  
																		package_max  +"','"+
																		package_promo +"','"+ 																		
																		new_contract +"','"+                  
																		old_contract +"','"+                  
																		is_main_group+"','"+                  
																		package_tmp_id  +"','"+               
																		status       +"','"+                  
																		manipulate   +"','"+                  
																		package_trans+"','"+                  
																		reg_package_add   +"','"+             
																		reg_day_from_1    +"','"+             
																		reg_day_to_1      +"','"+             
																		reg_day_per_1     +"','"+             
																		reg_day_from_2    +"','"+             
																		reg_day_to_2      +"','"+              
																		reg_day_per_2     +"','"+             
																		can_day_from_1    +"','"+             
																		can_day_to_1      +"','"+             
																		can_day_per_1     +"','"+             
																		can_day_from_2    +"','"+             
																		can_day_to_2      +"','"+             
																		can_day_per_2     +"','"+             
																		package_fee       +"','"+             
																		register_type     +"','"+             
																		condiction_ext    +"','"+             
																		getUserVar("userID")   +"','"+ 
																		ConvertFont.UtoD(note) +"','"+ 
																		subtype_id  	  +"','"+ 
																		main_group  	  +"','"+ 
																		psDN              +"','"+ 
																		active_date  	  +"','"+ 
																		chance_date  	  +"','"+ 
																		recovery_date  	  +"','"+ 
																		trans_date        +"','"+ 
																		dispatch_id  	  +"','"+
																		isRegis      	  +"','"+ 
																		psLimitMinus  	  +"','"+ 
																		psTypepkg         +"','"+
																		psStylepkg		  +"','"+
																		psMonthUse        +"','"+
																		psSMS             +"','"+
																		psLoaiAlo         + "'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
	public String del_PromoPackage (String package_id) throws Exception
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.del_promo_package('"+  package_id           +"','"+
																	  +  getUserVar("userID")  +"','"+
																	  +  getUserVar("userIP")  
																	  + "'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
	public String add_DisPackage (  String package_id        ,
									String pkg_discount_name    ,
									String discount_type_id     ,
									String discount_number_1    ,
									String discount_number_2    ,
									String effect_time_start    ,
									String apply_time_month     ,
									String usage_money_min      ,
									String package_promo_max    ,
									String person_operator      ,
									String note                 ) throws Exception 
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.add_discount_package('"+  package_id         +"','"+
																			 ConvertFont.UtoD(pkg_discount_name)  +"','"+
																			 discount_type_id   +"','"+
																			 discount_number_1  +"','"+
																			 discount_number_2  +"','"+
																			 effect_time_start  +"','"+
																			 apply_time_month   +"','"+
																			 usage_money_min    +"','"+
																			 package_promo_max  +"','"+
																			 person_operator    +"','"+
																			 ConvertFont.UtoD(note)               + "'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
		public String edit_DisPackage ( String pkg_discount_id      ,
										String package_id           ,
										String pkg_discount_name    ,
										String discount_type_id     ,
										String discount_number_1    ,
										String discount_number_2    ,
										String effect_time_start    ,
										String apply_time_month     ,
										String usage_money_min      ,
										String package_promo_max    ,
										String person_operator      ,
										String note                 ) throws Exception 
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.edit_discount_package('"+ pkg_discount_id    +"','"+
																			 package_id         +"','"+
																			 ConvertFont.UtoD(pkg_discount_name)  +"','"+
																			 discount_type_id   +"','"+
																			 discount_number_1  +"','"+
																			 discount_number_2  +"','"+
																			 effect_time_start  +"','"+
																			 apply_time_month   +"','"+
																			 usage_money_min    +"','"+
																			 package_promo_max  +"','"+
																			 person_operator    +"','"+
																			 ConvertFont.UtoD(note)               + "'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
		public String del_DisPackage (  String package_id ) throws Exception 
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.del_discount_package('"+  package_id   + "'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
	public String add_ConPacExp ( String package_id                   ,
									  String path_exp                     ,
									  String file_name_exp                ,
									  String file_type_exp_id             ,
									  String time_exp_start               ,
									  String default_data_exp             ,
									  String circle_exp                   ,
									  String sql_exp                      ,
									  String person_operator              ,
                                      String note                         ) throws Exception 
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.add_ConfigPacExport('"+ package_id           +"','"+
																	  ConvertFont.UtoD(path_exp)                  +"','"+
																	  file_name_exp             +"','"+
																	  file_type_exp_id          +"','"+
																	  time_exp_start            +"','"+
																	  default_data_exp          +"','"+
																	  circle_exp                +"','"+
																	  sql_exp                   +"','"+
																	  person_operator           +"','"+
																	  ConvertFont.UtoD(note)                      +"'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
		public String edit_ConPacExp (String package_exp_id               ,
									  String package_id                   ,
									  String path_exp                     ,
									  String file_name_exp                ,
									  String file_type_exp_id             ,
									  String time_exp_start               ,
									  String default_data_exp             ,
									  String circle_exp                   ,
									  String sql_exp                      ,
									  String person_operator              ,
                                      String note                          ) throws Exception 
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.edit_configPacExport('"+package_exp_id             +"','"+
																		   package_id                 +"','"+
																		   ConvertFont.UtoD(path_exp)                   +"','"+
																		   file_name_exp              +"','"+
																		   file_type_exp_id           +"','"+
																		   time_exp_start             +"','"+
																		   default_data_exp           +"','"+
																		   circle_exp                 +"','"+
																		   sql_exp                    +"','"+
																		   person_operator            +"','"+
																		   ConvertFont.UtoD(note)                       + "'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
		public String del_ConPacExp (  String package_id ) throws Exception 
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.del_configPacExport('"+  package_id   + "'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
	public String add_PkgPermission (String   package_id,
									String    operations,
									String    operations_name,
									String    role_id,
									String    effect_from,
									String    effect_to,
									String    effect_month,
									String    app_month  ) throws Exception 
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.add_pkg_permission('"+ package_id           	+"','"+
																		  operations          	+"','"+
																		  ConvertFont.UtoD(operations_name)          	+"','"+
																	  role_id             		+"','"+
																	  effect_from          		+"','"+
																	  effect_to            		+"','"+
																	  effect_month          	+"','"+
																	  app_month                	+"','"+
																	  getUserVar("userID")      +"','"+
																	  getUserVar("userIP")      +"'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
		public String edit_PkgPermission(String 	per_id,
									    String   	package_id,
										String    operations,
										String    operations_name,
										String    role_id,
										String    effect_from,
										String    effect_to,
										String    effect_month,
										String    app_month  ) throws Exception 
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.edit_pkg_permission('"+ per_id					+"','"+
																		package_id           		+"','"+
																		operations          		+"','"+
																		ConvertFont.UtoD(operations_name)          	+"','"+
																		role_id             		+"','"+
																		effect_from          		+"','"+
																		effect_to            		+"','"+
																		effect_month          		+"','"+
																		app_month                	+"','"+
																		getUserVar("userID")     	+"','"+
																		getUserVar("userIP")      	+"'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
		public String del_PkgPermission(  String per_id ) throws Exception 
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.del_pkg_permission('"+  per_id   					+"','"+
																		   getUserVar("userID")     	+"','"+
																		   getUserVar("userIP") 		+ "'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
			public String exp_PkgData(String package_id ) throws Exception 
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.promo_package.export_pkg_data('"+  package_id   				+"','"+
																		getUserVar("userID")     	+"','"+
																		getUserVar("userIP") 		+ "'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("", xs_);
	}
 }