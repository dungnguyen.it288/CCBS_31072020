package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class common
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.common was called");
  }
 
    public String layDScboUser(String psBCT) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/common/ajax_users"
        +"&"+CesarCode.encode("psUsers")+"="+psBCT
        ;
  }
 
    public String layDScboBuuCucThu(String psDVQL) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/common/ajax_cbobuucucthu"
        +"&"+CesarCode.encode("psDVQL")+"="+psDVQL
        ;
  }
  
   public String layDSBuuCucThu(String psDVQL) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/common/ajax_buucucthu"
        +"&"+CesarCode.encode("psDVQL")+"="+psDVQL
        ;
  }
      public String layDSQuayThu(String psBuuCucThu) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/common/ajax_quaythu"
        +"&"+CesarCode.encode("psBuuCucThu")+"="+psBuuCucThu
        ;
  }
  
}
