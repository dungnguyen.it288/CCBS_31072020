package neo.pttb;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSetMetaData;
import java.util.StringTokenizer;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.commerce.util.BASE64Decoder;
import javax.sql.RowSet;

import neo.qltn.FTPupload;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class export_data extends NEOProcessEx{
		public String export_dbf(
						String pschema,
						String pstable,
						String pscolumn,
						String puserid,
						String puserip) throws Exception {
			String return_ = "";
			try {
				
				RowSet rs_ = this.reqRSet("","SELECT ftp,username,pass,ftp_path,schema FROM "
						+ pschema+ "tables_exports where table_name='"+pstable+"'");
				String ftp_ = "", user_ = "", pass_ = "";
				String ftp_path = "",schema="";
				while (rs_.next()) {
					ftp_ = rs_.getString("ftp");
					user_ = rs_.getString("username");
					pass_ = rs_.getString("pass");
					ftp_path = rs_.getString("ftp_path");
					schema = rs_.getString("schema");
				}
				BASE64Decoder decoder = new BASE64Decoder();
				byte[] decodedBytes = decoder.decodeBuffer(pass_);
				String pass_d = new String(decodedBytes);
				
				String s = "begin ?:= " + pschema
				+ "pkg_export_data.get_table_export_data('" + pschema
				+ "','" + schema + "','" + pstable + "','" + pscolumn 
				+ "','" + puserid + "','" + puserip + "'); end;";
				RowSet rows = this.reqRSet("", s);
				System.out.println(s);
				ResultSetMetaData metas = rows.getMetaData();
				com.svcon.jdbf.JDBField[] fields = new com.svcon.jdbf.JDBField[metas
						.getColumnCount()];
				for (int i = 1; i <= metas.getColumnCount(); i++) {
					String cname = metas.getColumnName(i);
					char ctype = GetType(metas.getColumnTypeName(i));
					if (cname.length() > 10)
						cname = cname.substring(0, 10);
					int clength = metas.getColumnDisplaySize(i);
					if (ctype == 'N')
						clength = 20;
					else if (ctype == 'D')
						clength = 8;
					else if (clength >= 254)
						clength = 253;
					fields[i - 1] = new com.svcon.jdbf.JDBField(cname, ctype,
							clength, 0);
				}
				String f = getSysConst("FileUploadDir").replace("\\", "/").toLowerCase()+pstable;
				com.svcon.jdbf.DBFWriter writer = new com.svcon.jdbf.DBFWriter(f
						+ ".dbf", fields);
				System.out.println("File path:" + f);
				System.out.println(metas.getColumnCount());
				Object[] record = new Object[metas.getColumnCount()];
				while (rows.next()) {
					for (int j = 1; j <= metas.getColumnCount(); j++) {
						if (GetType(metas.getColumnTypeName(j)) == 'N') {
							try {
								record[j - 1] = new Integer(rows.getString(metas.getColumnName(j)).substring(0, 253));
							} catch (Exception iex) {
								// record[j-1]=0;
							}
						} else {
							try {
								record[j - 1] = (rows.getString(metas.getColumnName(j)));
							} catch (Exception cex) {
								
							}
						}
					}
					writer.addRecord(record);
				}
				writer.close();
				System.out.println(ftp_+"  "+user_+ "   " + pass_d);
				doZip(f + ".dbf", f + ".zip");
				new FTPupload(ftp_, user_, pass_d, f + ".zip", (ftp_path+pstable+".zip").replace("\\", "/").toLowerCase());
				return "1";
			} catch (Exception ex) {
				return ex.getMessage();
			}
		}
		public String export_txt(
				String pschema,
				String pstable,
				String pscolumn,
				String puserid,
				String puserip) throws Exception {
					String s ="begin ? :="+pschema+"pkg_export_data.export_data("
						+"'"+pschema+"',"
						+"'"+pstable+"',"
						+"'"+pscolumn+"',"
						+"'"+puserid+"',"
						+"'"+puserip+"'); end;";
				System.out.println(s);
				return this.reqValue("", s);
			}
		public void doZip(String filename, String zipfilename) throws IOException {
			try {
				FileInputStream fis = new FileInputStream(filename);
				File file = new File(filename);
				byte[] buf = new byte[fis.available()];
				fis.read(buf, 0, buf.length);

				CRC32 crc = new CRC32();
				ZipOutputStream s = new ZipOutputStream(
						(OutputStream) new FileOutputStream(zipfilename));
				s.setLevel(6);
				ZipEntry entry = new ZipEntry(file.getName());
				entry.setSize((long) buf.length);
				crc.reset();
				crc.update(buf);
				entry.setCrc(crc.getValue());
				s.putNextEntry(entry);
				s.write(buf, 0, buf.length);
				s.finish();
				s.close();
				fis.close();
				// file.delete();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public char GetType(String types) {
			char return_value = 'C';
			if (types == "VARCHAR2")
				return_value = 'C';
			else if (types == "NUMBER")
				return_value = 'N';
			else if (types == "DATE")
				return_value = 'N';
			return return_value;
		}
}
