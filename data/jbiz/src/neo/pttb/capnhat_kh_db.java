package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;

public class capnhat_kh_db
    extends NEOProcessEx {
  public void run() {
    System.out.println("neo.capnhat_kh_db was called");
  }

public  String laytt_thuebao_theo_somay(String psFuncSchema,String psUserId , String psMaTinhNgDung, String  psSchemaData,String psInput)
   		{
   			String s="begin ?:= admin_v2.pttb_capnhat.laytt_thuebao("
						+"'"+psUserId +"',"
						+"'"+psMaTinhNgDung+"',"
						+"'"+psSchemaData	+"',"
						+"'"+psInput+"'"
					+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
   			
public  String lay_so_thuebao_cua_khachhang(String psFuncSchema,String psUserId , String psMaTinhNgDung, String  psSchemaData, String psInput)
   		{
   			String s="begin ?:= "+psFuncSchema+"pttb_capnhat.lay_so_thuebao_cua_khachhang("
						+"'"+psUserId +"',"
						+"'"+psMaTinhNgDung+"',"
						+"'"+psSchemaData	+"',"
						+"'"+psInput+"'"
					+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
     
   
public  String laytt_kh(String psFuncSchema,String psUserId , String psMaTinhNgDung, String  psSchemaData, String  psColumn,String psInput)
   		{
   			String s="begin ?:= "+psFuncSchema+"pttb_capnhat.laytt_kh("
						+"'"+psUserId +"',"
						+"'"+psMaTinhNgDung+"',"
						+"'"+psSchemaData	+"',"
						+"'"+psColumn +"',"
						+"'"+psInput+"'"
					+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
  
  public  String laytt_kh_new(String psFuncSchema,String psUserId , String psMaTinhNgDung, String  psSchemaData, String  psColumn,String psInput)
   		{
   			String s="begin ?:= admin_v2.pttb_capnhat.laytt_kh("
						+"'"+psUserId +"',"
						+"'"+psMaTinhNgDung+"',"
						+"'"+psSchemaData	+"',"
						+"'"+psColumn +"',"
						+"'"+psInput+"'"
					+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
			
  public  String laytt_so_khachhang_trong_cq(String psFuncSchema,String psUserId , String psMaTinhNgDung, String  psSchemaData, String psInput)
   		{
   			String s="begin ?:= "+psFuncSchema+"pttb_capnhat.laytt_so_khachhang_trong_cq("
						+"'"+psUserId +"',"
						+"'"+psMaTinhNgDung+"',"
						+"'"+psSchemaData	+"',"
						+"'"+psInput+"'"
					+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
  

  /*public  String capNhatKhachHang(	
								 String sUserid
								,String sMaTinh
								,String sDataSchema
								,String sFunSchema
								
								,String sMaKHCu
								,String sMaKH
								,String sMaCQ
								,String sCoQuan
								,String sTenTT
								,String sSoDaiDien
								,String sNguoiDaiDien
								,String sDienThoaiLH
								,String sPHAI
								,String sEmail
								,String sNgaySinh
								,String sSoGiayTo
								,String sNoiCapGT
								,String sNgayCapGT
								,String sSoGT1
								,String sNoiCapGT1
								,String sNgayCapGT1
								,String suanct_id
								,String shuongct_id
								,String shoct_id
								,String sonhact_id
								,String sDiaChiCT
								,String sMSThue
								,String sTaiKhoan
								,String suantt_id
								,String shuongtt_id
								,String shott_id
								,String sonhatt_id
								,String sDiaChiTT
								,String sDangKyTV
								,String sDangKyDB
								,String sKHRR
								,String sNguoiCN
								,String sMayCN
								,String sMaNV
								,String sTenNV
								,String sGhiChu
								,String sLoaiGTID
								,String sLoaiGTID1
								,String sLoaiKH
								,String sDiaDiemTT
								,String sNganHang
								,String sChuyenKhoan
								,String sDonViQL
								,String sMaBC
								,String sKHLon
								,String sUuTien
								,String sNganhNghe
								,String sDoiDVQLTB
)
   		{
   			String s="begin ?:= "+sFunSchema+"pttb_capnhat.capnhat_kh_pttb1("
						+"'" +getUserVar("userID") +"'"
						+",'"+getUserVar("sys_agentcode")+"'"
						+",'"+getUserVar("sys_dataschema")	+"'"
					
					    +",'"+sMaKHCu+"'"
						+",'"+sMaKH+"'"
						+",'"+sMaCQ+"'"
						+",'"+sCoQuan+"'"
						+",'"+sTenTT+"'"
						+",'"+sSoDaiDien+"'"
						+",'"+sNguoiDaiDien+"'"
						+",'"+sDienThoaiLH+"'"
						+",'"+sPHAI+"'"
						+",'"+sEmail+"'"
						+",'"+sNgaySinh+"'"
						+",'"+sSoGiayTo+"'"
						+",'"+sNoiCapGT+"'"
						+",'"+sNgayCapGT+"'"
						+",'"+sSoGT1+"'"
						+",'"+sNoiCapGT1+"'"
						+",'"+sNgayCapGT1+"'"
						+",'"+suanct_id+"'"
						+",'"+shuongct_id+"'"
						+",'"+shoct_id+"'"
						+",'"+sonhact_id+"'"
						+",'"+sDiaChiCT+"'"
						+",'"+sMSThue+"'"
						+",'"+sTaiKhoan+"'"
						+",'"+suantt_id+"'"
						+",'"+shuongtt_id+"'"
						+",'"+shott_id+"'"
						+",'"+sonhatt_id+"'"
						+",'"+sDiaChiTT+"'"
						+",'"+sDangKyTV+"'"
						+",'"+sDangKyDB+"'"
						+",'"+sKHRR+"'"
						+",'"+sNguoiCN+"'"
						+",'"+sMayCN+"'"
						+",'"+sMaNV+"'"
						+",'"+sTenNV+"'"
						+",'"+sGhiChu+"'"
						+",'"+sLoaiGTID+"'"
						+",'"+sLoaiGTID1+"'"
						+",'"+sLoaiKH+"'"
						+",'"+sDiaDiemTT+"'"
						+",'"+sNganHang+"'"
						+",'"+sChuyenKhoan+"'"
						+",'"+sDonViQL+"'"
						+",'"+sMaBC+"'"
						+",'"+sKHLon+"'"
						+",'"+sUuTien+"'"
						+",'"+sNganhNghe+"'"
						+",'"+sDoiDVQLTB+"'"
						

						+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
  */
   public  String capNhatKhachHang(	
								 String sUserid
								,String sMaTinh
								,String sDataSchema
								,String sFunSchema
								
								,String sMaKHCu
								,String sMaKH
								,String sMaCQ
								,String sCoQuan
								,String sTenTT
								,String sSoDaiDien
								,String sNguoiDaiDien
								,String sDienThoaiLH
								,String sPHAI
								,String sEmail
								,String sNgaySinh
								,String sSoGiayTo
								,String sNoiCapGT
								,String sNgayCapGT
								,String sSoGT1
								,String sNoiCapGT1
								,String sNgayCapGT1
								,String suanct_id
								,String shuongct_id
								,String shoct_id
								,String sonhact_id
								,String sDiaChiCT
								,String sMSThue
								,String sTaiKhoan
								,String suantt_id
								,String shuongtt_id
								,String shott_id
								,String sonhatt_id
								,String sDiaChiTT
								,String sDangKyTV
								,String sDangKyDB
								,String sKHRR
								,String sNguoiCN
								,String sMayCN
								,String sMaNV
								,String sTenNV
								,String sGhiChu
								,String sLoaiGTID
								,String sLoaiGTID1
								,String sLoaiKH
								,String sDiaDiemTT
								,String sNganHang
								,String sChuyenKhoan
								,String sDonViQL
								,String sMaBC
								,String sKHLon
								,String sUuTien
								,String sNganhNghe
								,String sDoiDVQLTB	
								,String sMaT
								,String sTenT
								,String sKyTen
								,String sNguoi_gt	
								,String sMa_ns								
)
   		{
   		//	String s="begin ?:= "+sFunSchema+"pttb_capnhat.capnhat_kh_pttb1("
			String s="begin ?:= admin_v2.pkg_contact_info.capnhat_kh_pttb1("
						+"'" +getUserVar("userID") +"'"
						+",'"+getUserVar("sys_agentcode")+"'"
						+",'"+getUserVar("sys_dataschema")	+"'"	
						
					    +",'"+sMaKHCu+"'"
						+",'"+sMaKH+"'"
						+",'"+sMaCQ+"'"
						+",'"+sCoQuan+"'"
						+",'"+sTenTT+"'"
						+",'"+sSoDaiDien+"'"
						+",'"+sNguoiDaiDien+"'"
						+",'"+sDienThoaiLH+"'"
						+",'"+sPHAI+"'"
						+",'"+sEmail+"'"
						+",'"+sNgaySinh+"'"
						+",'"+sSoGiayTo+"'"
						+",'"+sNoiCapGT+"'"
						+",'"+sNgayCapGT+"'"
						+",'"+sSoGT1+"'"
						+",'"+sNoiCapGT1+"'"
						+",'"+sNgayCapGT1+"'"
						+",'"+suanct_id+"'"
						+",'"+shuongct_id+"'"
						+",'"+shoct_id+"'"
						+",'"+sonhact_id+"'"
						+",'"+sDiaChiCT+"'"
						+",'"+sMSThue+"'"
						+",'"+sTaiKhoan+"'"
						+",'"+suantt_id+"'"
						+",'"+shuongtt_id+"'"
						+",'"+shott_id+"'"
						+",'"+sonhatt_id+"'"
						+",'"+sDiaChiTT+"'"
						+",'"+sDangKyTV+"'"
						+",'"+sDangKyDB+"'"
						+",'"+sKHRR+"'"
						+",'"+sNguoiCN+"'"
						+",'"+sMayCN+"'"
						+",'"+sMaNV+"'"
						+",'"+sTenNV+"'"
						+",'"+sGhiChu+"'"
						+",'"+sLoaiGTID+"'"
						+",'"+sLoaiGTID1+"'"
						+",'"+sLoaiKH+"'"
						+",'"+sDiaDiemTT+"'"
						+",'"+sNganHang+"'"
						+",'"+sChuyenKhoan+"'"
						+",'"+sDonViQL+"'"
						+",'"+sMaBC+"'"
						+",'"+sKHLon+"'"
						+",'"+sUuTien+"'"
						+",'"+sNganhNghe+"'"
						+",'"+sDoiDVQLTB+"'"	
						+",'"+sMaT+"'"
						+",'"+sTenT+"'"
						+",'"+sKyTen+"'"
						+",'"+sNguoi_gt+"'"
						+",'"+sMa_ns+"'"						
						+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
			
	public  String capNhatKhachHang_New(	
								 String sUserid
								,String sMaTinh
								,String sDataSchema
								,String sFunSchema
								
								,String sMaKHCu
								,String sMaKH
								,String sMaCQ
								,String sCoQuan
								,String sTenTT
								,String sSoDaiDien
								,String sNguoiDaiDien
								,String sDienThoaiLH
								,String sPHAI
								,String sEmail
								,String sNgaySinh
								,String sSoGiayTo
								,String sNoiCapGT
								,String sNgayCapGT
								,String sSoGT1
								,String sNoiCapGT1
								,String sNgayCapGT1
								,String suanct_id
								,String shuongct_id
								,String shoct_id
								,String sonhact_id
								,String sDiaChiCT
								,String sMSThue
								,String sTaiKhoan
								,String suantt_id
								,String shuongtt_id
								,String shott_id
								,String sonhatt_id
								,String sDiaChiTT
								,String sDangKyTV
								,String sDangKyDB
								,String sKHRR
								,String sNguoiCN
								,String sMayCN
								,String sMaNV
								,String sTenNV
								,String sGhiChu
								,String sLoaiGTID
								,String sLoaiGTID1
								,String sLoaiKH
								,String sDiaDiemTT
								,String sNganHang
								,String sChuyenKhoan
								,String sDonViQL
								,String sMaBC
								,String sKHLon
								,String sUuTien
								,String sNganhNghe
								,String sDoiDVQLTB	
								,String sMaT
								,String sTenT
								,String sKyTen
								,String sNguoi_gt	
								,String sMa_ns
								,String lydoanh
)
   		{
   		//	String s="begin ?:= "+sFunSchema+"pttb_capnhat.capnhat_kh_pttb1("
			String s="begin ?:= admin_v2.pkg_contact_info.capnhat_kh_pttb_new("
						+"'" +getUserVar("userID") +"'"
						+",'"+getUserVar("sys_agentcode")+"'"
						+",'"+getUserVar("sys_dataschema")	+"'"	
						
					    +",'"+sMaKHCu+"'"
						+",'"+sMaKH+"'"
						+",'"+sMaCQ+"'"
						+",'"+sCoQuan+"'"
						+",'"+sTenTT+"'"
						+",'"+sSoDaiDien+"'"
						+",'"+sNguoiDaiDien+"'"
						+",'"+sDienThoaiLH+"'"
						+",'"+sPHAI+"'"
						+",'"+sEmail+"'"
						+",'"+sNgaySinh+"'"
						+",'"+sSoGiayTo+"'"
						+",'"+sNoiCapGT+"'"
						+",'"+sNgayCapGT+"'"
						+",'"+sSoGT1+"'"
						+",'"+sNoiCapGT1+"'"
						+",'"+sNgayCapGT1+"'"
						+",'"+suanct_id+"'"
						+",'"+shuongct_id+"'"
						+",'"+shoct_id+"'"
						+",'"+sonhact_id+"'"
						+",'"+sDiaChiCT+"'"
						+",'"+sMSThue+"'"
						+",'"+sTaiKhoan+"'"
						+",'"+suantt_id+"'"
						+",'"+shuongtt_id+"'"
						+",'"+shott_id+"'"
						+",'"+sonhatt_id+"'"
						+",'"+sDiaChiTT+"'"
						+",'"+sDangKyTV+"'"
						+",'"+sDangKyDB+"'"
						+",'"+sKHRR+"'"
						+",'"+sNguoiCN+"'"
						+",'"+sMayCN+"'"
						+",'"+sMaNV+"'"
						+",'"+sTenNV+"'"
						+",'"+sGhiChu+"'"
						+",'"+sLoaiGTID+"'"
						+",'"+sLoaiGTID1+"'"
						+",'"+sLoaiKH+"'"
						+",'"+sDiaDiemTT+"'"
						+",'"+sNganHang+"'"
						+",'"+sChuyenKhoan+"'"
						+",'"+sDonViQL+"'"
						+",'"+sMaBC+"'"
						+",'"+sKHLon+"'"
						+",'"+sUuTien+"'"
						+",'"+sNganhNghe+"'"
						+",'"+sDoiDVQLTB+"'"	
						+",'"+sMaT+"'"
						+",'"+sTenT+"'"
						+",'"+sKyTen+"'"
						+",'"+sNguoi_gt+"'"
						+",'"+sMa_ns+"'"						
						+","+lydoanh+""
						+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
  
    public  String capNhatDanhBa(	
								 String sUserid
								,String sMaTinh
								,String sDataSchema
								,String sFunSchema				
								,String sSOMAY
								,String sMA_KH
								,String sTEN_TB
								,String sTEN_DB
								,String sCUOC_TB
								,String sTYLE_VAT
								,String sINCHITIET
								,String sTRACUU_DB
								,String sDANGKY_DB
								,String sTHUTU_IN
								,String sNGAY_LD
								,String sNGAY_SN
								,String sNGAY_THUHOI
								,String sNGAY_CN
								,String sMAY_CN
								,String sNGUOI_CN
								,String sKHACHHANG_CU
								,String squan_id
								,String sphuong_id
								,String spho_id
								,String sso_nha
								,String sDIACHI
								,String sGHICHU
								,String sDONVIQL_ID
								,String sMA_BC
								,String sDOITUONG_ID
								,String sLOAITB_ID
								,String sTRANGTHAI_ID								
								,String piloaidcbc
								,String  pikieubc
								,String pinvpt
								,String pinvql
								,String pimabaocuoc
								,String sNGAY_SINH
								,String sEmail
								)
   		{
   			
			
			//String s="begin ?:= "+sFunSchema+"pttb_capnhat.capnhat_db_pttb("
			String s="begin ?:= admin_V2.pkg_contact_info.capnhat_db_pttb("
						+"'" +getUserVar("userID") +"'"
						+",'"+getUserVar("sys_agentcode")+"'"
						+",'"+getUserVar("sys_dataschema")	+"'"					
						+",'"+sSOMAY+"'"
						+",'"+sMA_KH+"'"
						+",'"+sTEN_TB+"'"
						+",'"+sTEN_DB+"'"
						+",'"+sCUOC_TB+"'"
						+",'"+sTYLE_VAT+"'"
						+",'"+sINCHITIET+"'"
						+",'"+sTRACUU_DB+"'"
						+",'"+sDANGKY_DB+"'"
						+",'"+sTHUTU_IN+"'"
						+",'"+sNGAY_LD+"'"
						+",'"+sNGAY_SN+"'"
						+",'"+sNGAY_THUHOI+"'"
						+",'"+sNGAY_CN+"'"
						+",'"+sMAY_CN+"'"
						+",'"+sNGUOI_CN+"'"
						+",'"+sKHACHHANG_CU+"'"
						+",'"+squan_id+"'"
						+",'"+sphuong_id+"'"
						+",'"+spho_id+"'"
						+",'"+sso_nha+"'"
						+",'"+sDIACHI+"'"
						+",'"+sGHICHU+"'"
						+",'"+sDONVIQL_ID+"'"
						+",'"+sMA_BC+"'"
						+",'"+sDOITUONG_ID+"'"
						+",'"+sLOAITB_ID+"'"
						+",'"+sTRANGTHAI_ID+"'"
						+",'"+piloaidcbc +"'"
						+",'"+pikieubc +"'"
						+",'"+pinvpt +"'"
						+",'"+pinvql +"'"
						+",'"+pimabaocuoc+"'"
						+",'"+sNGAY_SINH+"'"
						+",'"+sEmail+"'"

			+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
			
	//26/06/2017: Cap nhat danh ba theo ND49
	
	public  String capNhatDanhBa_New(	
								 String sUserid
								,String sMaTinh
								,String sDataSchema
								,String sFunSchema				
								,String sSOMAY
								,String sMA_KH
								,String sTEN_TB
								,String sTEN_DB
								,String sCUOC_TB
								,String sTYLE_VAT
								,String sINCHITIET
								,String sTRACUU_DB
								,String sDANGKY_DB
								,String sTHUTU_IN
								,String sNGAY_LD
								,String sNGAY_SN
								,String sNGAY_THUHOI
								,String sNGAY_CN
								,String sMAY_CN
								,String sNGUOI_CN
								,String sKHACHHANG_CU
								,String squan_id
								,String sphuong_id
								,String spho_id
								,String sso_nha
								,String sDIACHI
								,String sGHICHU
								,String sDONVIQL_ID
								,String sMA_BC
								,String sDOITUONG_ID
								,String sLOAITB_ID
								,String sTRANGTHAI_ID								
								,String piloaidcbc
								,String  pikieubc
								,String pinvpt
								,String pinvql
								,String pimabaocuoc
								,String sNGAY_SINH
								,String sEmail
								,String quoctich
								,String doituongsd
								,String loaigt
								,String sogt
								,String noicapgt
								,String ngaycapgt
								,String diachitr								
								)
   		{
   						
			//String s="begin ?:= "+sFunSchema+"pttb_capnhat.capnhat_db_pttb("
			String s="begin ?:= admin_V2.pkg_contact_info.capnhat_db_pttb_new("
						+"'" +getUserVar("userID") +"'"
						+",'"+getUserVar("sys_agentcode")+"'"
						+",'"+getUserVar("sys_dataschema")	+"'"					
						+",'"+sSOMAY+"'"
						+",'"+sMA_KH+"'"
						+",'"+sTEN_TB+"'"
						+",'"+sTEN_DB+"'"
						+",'"+sCUOC_TB+"'"
						+",'"+sTYLE_VAT+"'"
						+",'"+sINCHITIET+"'"
						+",'"+sTRACUU_DB+"'"
						+",'"+sDANGKY_DB+"'"
						+",'"+sTHUTU_IN+"'"
						+",'"+sNGAY_LD+"'"
						+",'"+sNGAY_SN+"'"
						+",'"+sNGAY_THUHOI+"'"
						+",'"+sNGAY_CN+"'"
						+",'"+sMAY_CN+"'"
						+",'"+sNGUOI_CN+"'"
						+",'"+sKHACHHANG_CU+"'"
						+",'"+squan_id+"'"
						+",'"+sphuong_id+"'"
						+",'"+spho_id+"'"
						+",'"+sso_nha+"'"
						+",'"+sDIACHI+"'"
						+",'"+sGHICHU+"'"
						+",'"+sDONVIQL_ID+"'"
						+",'"+sMA_BC+"'"
						+",'"+sDOITUONG_ID+"'"
						+",'"+sLOAITB_ID+"'"
						+",'"+sTRANGTHAI_ID+"'"
						+",'"+piloaidcbc +"'"
						+",'"+pikieubc +"'"
						+",'"+pinvpt +"'"
						+",'"+pinvql +"'"
						+",'"+pimabaocuoc+"'"
						+",'"+sNGAY_SINH+"'"
						+",'"+sEmail+"'"
						+","+quoctich+""
						+","+doituongsd+""
						+","+loaigt+""
						+",'"+sogt+"'"
						+",'"+noicapgt+"'"
						+",'"+ngaycapgt+"'"
						+",'"+diachitr+"'"

			+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
  
    public String layds_tb(String sInput) {
    return "/main?"+CesarCode.encode("configFile")+"=PTTB/capnhat/khachhang/ajax_layds_tb_trong_kh"
        +"&"+CesarCode.encode("sInput")+"="+sInput
        ;
  }
  
      public String layds_tb_cua_coquan(String sInput) {
    return "/main?"+CesarCode.encode("configFile")+"=PTTB/capnhat/danhba/ajax_layds_tbdd_trong_cq"
        +"&"+CesarCode.encode("somay")+"="+sInput
        ;
  }
  
  //Doi the ma bi mat. Ngay 28/09/2017
  public String kiemtra_the(String psSerial)
   	{
   		String s="begin ?:= admin_v2.pkg_doithe.kiemtra_the("
				+"'"+getUserVar("userID") +"',"
				+"'"+getUserVar("sys_agentcode")+"',"				
				+"'"+psSerial+"'"
				+") ; end;" ;
		return s;
   	}
	
  public String laytt_the(String  psSerial)
   	{
   		String s="begin ?:= admin_v2.pkg_doithe.laytt_the("
				+"'"+getUserVar("userID") +"',"
				+"'"+getUserVar("sys_agentcode")+"',"				
				+"'"+ psSerial+"'"
				+") ; end;" ;
		return s;
   	}
	
	public String capnhat_the(String psSerial,String psSerialNew,String psEload,String psHoTen,String psGhiChu,String psIp)
   	{
   		String s="begin ?:= admin_v2.pkg_doithe.capnhat_the("
				+"'"+psSerial+"',"
				+"'"+psSerialNew+"',"
				+"'"+psEload+"',"
				+"'"+psHoTen+"',"
				+"'"+psGhiChu+"',"				
				+"'"+getUserVar("userID")+"',"
				+"'"+psIp+"',"
				+"'"+getUserVar("sys_agentcode")+"'"
				+") ; end;" ;
		return s;
   	}
	
	public String upload_doithe(String psupload_id,String psuserip)
	{
		String result="";
		String s=  "begin ? := admin_v2.pkg_doithe.upload_doithe('"+psupload_id+"','"+getUserVar("sys_agentcode")+"','"+getUserVar("userID")+"','"+psuserip+"'); end;";
		System.out.println(s);
	try{		    
		 result= this.reqValue("",s);			 
	}catch (Exception ex){            	
		result = ex.getMessage();
	}
		return result;	
	}  
  
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
