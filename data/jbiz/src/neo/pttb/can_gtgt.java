package neo.pttb;
import neo.smartui.process.*;
import neo.smartui.common.*;
import neo.smartui.report.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;

public class can_gtgt extends NEOProcessEx 
{
	public void run() 
	{
		System.out.println("------------------------------------------>Bat dau chay hoa don dien tu VAT<-------------------------------------------");
	}
	
	
	public String check_err(String res) {
		try {
			String err_code = res.substring(res.indexOf("<error>") + ("<error>").length(), res.indexOf("</error>"));
			if (!err_code.equals("0")) {
				//return res.substring(res.indexOf("<error_desc>") + ("<error_desc>").length(),
						//res.indexOf("</error_desc>"));
				return "Loi";
			}
		} catch (Exception e) {
			return "Loi";
		}
		return "OK";
	}

	public String getDocFromUrl(String url) {
		String content = "";
		URL url_;
		try {
			url_ = new URL(url);
			URLConnection conn = url_.openConnection();
			BufferedReader bf = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line = "";
			while ((line = bf.readLine()) != null) {
				content += line;
			}
		} catch (Exception e) {
			e.printStackTrace();
			content = "Loi goi WS";
		}
		return content;
	}

	public HashMap getServices(String res) {
		HashMap services = new HashMap();
		// Lay total package
		int total_pkg = 0;
		total_pkg = Integer.parseInt(res.substring(res.indexOf("<total_package>") + ("<total_package>").length(),
				res.indexOf("</total_package>")));

		// Lay ds service
		int curr_index = 0;
		String service = "";
		String status = "";
		String package_id = "";
		for (int i = 0; i < total_pkg; i++) {
			status = res.substring(res.indexOf("<status>", curr_index) + ("<status>").length(),
					res.indexOf("</status>", curr_index));
			if (status.equals("1")) {
				service = res.substring(res.indexOf("<service>", curr_index) + ("<service>").length(),
						res.indexOf("</service>", curr_index));
				package_id = res.substring(res.indexOf("<packageid>", curr_index) + ("<packageid>").length(),
						res.indexOf("</packageid>", curr_index));
				if (services.get(service) != null) {
					services.put(service, services.get(service) + "#" + package_id);
				} else {
					services.put(service,package_id);
				}
			}
			curr_index = res.indexOf("</package>", curr_index) + ("</package>").length();
		}
		return services;
	}

	public String genHtlmView(HashMap services) {
		String html_view = "";
		Iterator iterator = services.keySet().iterator();
		while (iterator.hasNext()) {
			String service = (String) iterator.next();
			if (service.equals("MCA") || service.equals("RINGTUNES")) {
				html_view = "<input checked=\"true\" type = \"checkbox\" name=\"gtgt\" value=\"" + service + ":" + services.get(service) + "\" /> " + service + "<br />" + html_view;
			} else {
				html_view += "<input checked=\"true\" type = \"checkbox\" name=\"gtgt\" value=\"" + service + ":" + services.get(service) + "\" /> " + service + "<br />";
			}
		}
		
		if (!html_view.equals("")) {
			html_view += "<br /><input align=\"center\" onclick=\"javascript:window.opener.huy_dichvu_gtgt();\" type=\"button\" value=\"Huy DV\" />";
		} else {
			html_view = "Thue bao khong co dich vu GTGT";
		}

		return html_view;
	}
	
	public String getDocFromUrl_2(String url) {
		String content = "";
		URL url_;
		try {
			url_ = new URL(url);
			URLConnection conn = url_.openConnection();
			BufferedReader bf = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line = "";
			while ((line = bf.readLine()) != null) {
				content += line;
			}
		} catch (Exception e) {
			e.printStackTrace();
			content = "Loi goi WS";
		}
		return content;
	}
	
	public NEOCommonData get_ds_service(String msisdn) {
		String view = "";
		// Lay request id
		try {
			String s = "begin ?:= admin_v2.pkg_can_gtgt.getReqSeq(); end;";
			String request_id = this.reqValue("", s);
			String url = "http://10.1.10.173/vascmd/vasprovisioning/api?name=getstatusvas&requestid=" + request_id + "&msisdn=" + msisdn + "&application=CCBS&channel=API&username=" + getUserVar("userID") + "&userip=" + getEnvInfo().getParserParam("sys_userip");
			System.out.println(url);
			String response = getDocFromUrl_2(url);
			System.out.println(response);
			// Kiem tra ket qua goi
			String check = "";
			if (!(check = check_err(response)).equals("OK")) {
				return new NEOCommonData(check);
			}
			
			// Lay danh sach service
			System.out.println(response);
			HashMap services = getServices(response);
			
			// Lay view hien thi
			view = genHtlmView(services);
			view = "<br /><input type=\"checkbox\" id=\"check_all_id\" checked=\"true\" align=\"center\" onchange=\"javascript:window.opener.check_all_gtgt();\"> Check all  <br />" +view;
		} catch (Exception e) {
			view = "Loi thuc hien. Vui long lien he HTKT";
		}
		view = "Thue bao: " + msisdn + "<br />" + view;
		return (new NEOCommonData(view));
	}
	
	public NEOCommonData huy_services(String msisdn, String services) {
		String view = "";
		// Lay request id
		String ds_err = "";
		try {
			String s = "begin ?:= admin_v2.pkg_can_gtgt.getReqSeq(); end;";
			String request_id = this.reqValue("", s);
			String[] arr_service = services.split(",");
			String check_rs = "";
			for (int i = 0; i < arr_service.length; i++) {
				String service = arr_service[i].substring(0,arr_service[i].indexOf(":"));
				String[] package_ids = arr_service[i].substring(arr_service[i].indexOf(":") + 1).split("#");
				ds_err += service;
				for (int j = 0; j < package_ids.length; j++) {
					//String url = "http://10.1.10.173/vascmd/vasprovisioning/api?name=unsubscribeall&requestid=" + request_id + "&msisdn=" + msisdn + "&service=" + arr_service[i] + "&reason=ccbs&application=CCBS&username=" + getUserVar("userID") + "&userip=" + getEnvInfo().getParserParam("sys_userip") + "&channel=API";
					String url = "http://10.1.10.173/vascmd/vasprovisioning/api?name=unsubscribe&requestid=" + request_id + "&msisdn=" + msisdn + "&service=" + service + "&package=" + package_ids[j] + "&policy=0&note=ccbs&application=CCBS&channel=API&username=" + getUserVar("userID") + "&userip=" + getEnvInfo().getParserParam("sys_userip");
					String response = getDocFromUrl_2(url);
					check_rs = check_err(response);
					ds_err += "|" + package_ids[j] + ":" + check_rs;
					
					// Ghi log ket qua
					this.reqValue("", "begin ?:= admin_v2.pkg_can_gtgt.write_log(" + request_id + ",'" + msisdn + "','" + service + "|" + package_ids[j] + "','" + url + "','" + response + "'); end;");
				}
				ds_err += "<br />";
				
			}
		} catch (Exception e) {
			view = "Loi thuc hien. Vui long lien he HTKT";
		}
		view = "Ket qua thuc hien: " + msisdn + "<br />" + view + ds_err;
		return (new NEOCommonData(view));
	}
}

