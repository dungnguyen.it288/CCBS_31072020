package neo.pttb;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import cosaldap.Base64.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;


public class tinnhiem extends NEOProcessEx {
	public void run() {
		System.out.println("neo.pttb.tinnhiem was called");
	}
  
	public String get_ds_cauhinh(String donviql) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/km_tinnhiem/ajax_ds_cauhinh"
				+"&"+CesarCode.encode("ds_dvql") +"="+donviql;
	}
	
	public String get_ds_donvi(String matinh) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/km_tinnhiem/phanquyen/ajax_ds_donvi"
				+"&"+CesarCode.encode("matinh") +"="+matinh;
	}
	
	public String get_donviql(String schema) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/km_tinnhiem/bao_cao/ajax_ds_donvi"
					+"&"+CesarCode.encode("schema") +"="+schema;
	}
	
	public String get_log_nc(String donviql_id) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/km_tinnhiem/nhan_cong/ajax_log_cauhinh"
					+"&"+CesarCode.encode("donviql_id") +"="+donviql_id;
	}
	
	public String get_log(int donviql_id,int type) {
		String url = "";
		if (type == 1) {
			url = "/main?"+CesarCode.encode("configFile")+"=pttb/km_tinnhiem/ajax_log_cauhinh"
					+"&"+CesarCode.encode("donviql_id") +"="+donviql_id;
		} else {
			url = "/main?"+CesarCode.encode("configFile")+"=pttb/km_tinnhiem/ajax_run_log"
					+"&"+CesarCode.encode("donviql_id") +"="+donviql_id;
		}
		return url;
	}
	
	public String get_ds_thuebao(String notu, String noden, String diemtu, String diemden, String hd_tu, String hd_den, String dk_tu, String dk_den, String ma_nv, String ma_kh, String chuky, String buucuc, String tuyen, String ds_loaikh, String donviql_id,String type) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/km_tinnhiem/nhan_cong/ajax_ds_thuebao"
				+"&"+CesarCode.encode("notu") +"="+notu
				+"&"+CesarCode.encode("noden") +"="+noden
				+"&"+CesarCode.encode("diemtu") +"="+diemtu
				+"&"+CesarCode.encode("diemden") +"="+diemden
				+"&"+CesarCode.encode("ds_loaikh") +"="+ds_loaikh
				+"&"+CesarCode.encode("ds_buucuc") +"="+buucuc
				+"&"+CesarCode.encode("ds_tuyen") +"="+tuyen
				+"&"+CesarCode.encode("chuky") +"="+chuky
				+"&"+CesarCode.encode("ma_nv") +"="+ma_nv
				+"&"+CesarCode.encode("ma_kh") +"="+ma_kh
				+"&"+CesarCode.encode("ds_buucuc") +"="+buucuc
				+"&"+CesarCode.encode("ds_tuyen") +"="+tuyen
				+"&"+CesarCode.encode("hd_tu") +"="+hd_tu
				+"&"+CesarCode.encode("hd_den") +"="+hd_den
				+"&"+CesarCode.encode("dk_tu") +"="+dk_tu
				+"&"+CesarCode.encode("dk_den") +"="+dk_den
				+"&"+CesarCode.encode("kieu") +"="+type
				+"&"+CesarCode.encode("donviql_id") +"="+donviql_id;
	}
	
	public String capnhat (
		int id,
		int notu,
		String noden,
		int diemtu,
		int diemden,
		String ngay_km1c,
		String ngay_km2c,
		String ngay_nt,
		String km1c_sms,
		String km2c_sms,
		String nocuoc_sms,
		String ds_donvi,
		String ghichu,
		String ngay_nt1c,
		String ngay_nt2c,
		String status,
		int sms_tu,
		String sms_den
	) throws Exception {
		String sql = "begin ?:= admin_v2.pkg_kmnt_tn.update_config("
			+ id + ","
			+ "'" + getUserVar("sys_agentcode") + "','"
			+ ds_donvi + "','"
			+ ngay_km1c + "','"
			+ ngay_km2c + "','"
			+ ngay_nt + "','"
			+ notu + "','"
			+ noden + "','"
			+ diemtu + "','"
			+ diemden + "','"
			+ km1c_sms + "','"
			+ km2c_sms + "','"
			+ nocuoc_sms + "','"
			+ ConvertFont.UtoD(ghichu) + "','"
			+ ngay_nt1c + "','"
			+ ngay_nt2c + "',"
			+ status + ",'"
			+ getUserVar("userID") + "','"
			+ getEnvInfo().getParserParam("sys_userip") + "','"
			+ sms_tu + "','"
			+ sms_den
			+ "'); end;";
		System.out.println(sql);
		return sql;
	}
	
	public String themmoi (
		int notu,
		String noden,
		int diemtu,
		int diemden,
		String ngay_km1c,
		String ngay_km2c,
		String ngay_nt,
		String km1c_sms,
		String km2c_sms,
		String nocuoc_sms,
		String ds_donvi,
		String ghichu,
		String ngay_nt1c,
		String ngay_nt2c,
		String status,
		int sms_tu,
		String sms_den
	) throws Exception {
		String sql = "begin ?:= admin_v2.pkg_kmnt_tn.add_config("
			+ "'" + getUserVar("sys_agentcode") + "','"
			+ ds_donvi + "','"
			+ ngay_km1c + "','"
			+ ngay_km2c + "','"
			+ ngay_nt + "','"
			+ notu + "','"
			+ noden + "','"
			+ diemtu + "','"
			+ diemden + "','"
			+ km1c_sms + "','"
			+ km2c_sms + "','"
			+ nocuoc_sms + "','"
			+ ConvertFont.UtoD(ghichu) + "','"
			+ ngay_nt1c + "','"
			+ ngay_nt2c + "',"
			+ status + ",'"
			+ getUserVar("userID") + "','"
			+ getEnvInfo().getParserParam("sys_userip") + "','"
			+ sms_tu + "','"
			+ sms_den
			+ "'); end;";
		return sql;
	}
	
	public String gui_sms_nc(String dstb, int kieu, String chuky, String noidung) {
		String sql = "begin ?:= admin_v2.pkg_kmnt_tn.gui_sms_nc('"
			+ dstb + "','"
			+ kieu + "','"
			+ chuky + "','"
			+ noidung + "','"
			+ getUserVar("sys_agentcode") + "','"
			+ getUserVar("userID") + "','"
			+ getEnvInfo().getParserParam("sys_userip") + "'); end;";
		return sql;
	}
	
	public String dis_all() {
		String sql = "begin ?:= admin_v2.pkg_kmnt_tn.set_disable_all('"
			+ getUserVar("userID") + "','"
			+ getEnvInfo().getParserParam("sys_userip") + "','"
			+ getUserVar("sys_agentcode") + "'); end;";
		return sql;
	}
	
	public String xoa_cauhinh(int ch_id) {
		String sql = "begin ?:= admin_v2.pkg_kmnt_tn.del_config('"
			+ ch_id + "','"
			+ getUserVar("sys_agentcode") + "','"
			+ getUserVar("userID") + "','"
			+ getEnvInfo().getParserParam("sys_userip") + "'); end;";
		return sql;
	}
	
	public String get_tt_cauhinh(int ch_id) {
		String sql = "begin ?:= admin_v2.pkg_kmnt_tn.get_tt_cauhinh('" + ch_id + "','" + getUserVar("sys_dataschema") + "'); end;";
		return sql;
	}
	
	public String get_ds_user_cf(String donviql_id, String ma_tinh) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/km_tinnhiem/ajax_ds_user"
				+"&"+CesarCode.encode("donviql_id") +"="+donviql_id
				+"&"+CesarCode.encode("ma_tinh") +"="+ma_tinh;
	}
	
	public String get_kq_sms(int id) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/km_tinnhiem/nhan_cong/ajax_kq_sms"
				+"&"+CesarCode.encode("id") +"="+id;
	}
	
	public String delete_user(int id) {
		String sql = "begin ?:= admin_v2.pkg_kmnt_tn.del_user('"
				+ id + "','"
				+ getUserVar("sys_agentcode") + "','"
				+ getUserVar("userID") + "','"
				+ getEnvInfo().getParserParam("sys_userip") + "'); end;";
		return sql;
	}
	
	public String capnhat_user(String donviql_id, String userid, String ma_tinh, int cap_user, String lien_he) {
		System.out.println("aaaaaaaaaaaaaaaaaaaaaaaa");
		String sql = "begin ?:= admin_v2.pkg_kmnt_tn.add_user('"
				+ userid + "','"
				+ donviql_id + "','"
				+ ma_tinh + "','"
				+ cap_user + "','"
				+ lien_he + "','"
				+ getUserVar("userID") + "','"
				+ getEnvInfo().getParserParam("sys_userip") + "'); end;";
		return sql;
	}
	
	public String tao_km_nhancong(String notu, String noden, String diemtu, String diemden, String hd_tu, String hd_den, String dk_tu, String dk_den, String sms, String ghichu, String ma_nv, String ma_kh, String chuky, String buucuc, String tuyen, String ds_loaikh, String donviql_id,String type) {
		String sql = "begin ?:= admin_v2.pkg_kmnt_tn.tao_ds_khoa_nc('"
			+ notu + "','"
			+ noden + "','"
			+ diemtu + "','"
			+ diemden + "','"
			+ ConvertFont.UtoD(sms) + "','"
			+ donviql_id + "','"
			+ ds_loaikh + "','"
			+ ConvertFont.UtoD(ghichu) + "','"
			+ type + "','"
			+ chuky + "','"
			+ buucuc + "','"
			+ tuyen + "','"
			+ ma_nv + "','"
			+ hd_tu + "','"
			+ hd_den + "','"
			+ dk_tu + "','"
			+ dk_den + "','"
			+ ma_kh + "','"
			+ getUserVar("userID") + "','"
			+ getEnvInfo().getParserParam("sys_userip") + "','"
			+ getUserVar("sys_dataschema") + "'); end;";
		return sql;
	}
}











