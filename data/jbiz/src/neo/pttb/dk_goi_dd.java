package neo.pttb;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import cosaldap.Base64.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;


public class dk_goi_dd
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb.dk_goi was called");
  }   
	public String them_moi_dd(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String dsmay 				
		, String chunhom
		, String ghiChu 		
		, String sIP		
  	) throws Exception {
		
	String seq = get_sq_dd();
	String[] somayarr = dsmay.split(",");
	String[] chunhomarr = chunhom.split(",");
	
	String tmp1 = "OK";
	int n = somayarr.length;
	int nn = chunhomarr.length;
	
	for (int i =0 ; i < n; i++)
	{
	  tmp1 = themmoi_dd_log(somayarr[i],loaiGoi,ghiChu, seq, "0");
		
	}
	
	for (int j = 0 ; j < nn; j++)
	{
		String tmp2 = themmoi_dd_log(chunhomarr[j],loaiGoi,ghiChu, seq, "1");
	}
	
	//Dang ky dai dien huong cuoc
	
		String s=    "begin ? := "+FuncSchema+"pkg_regis_dd.them_moi_dd("	
		+" '"+loaiGoi+"'"
		+",'"+seq+"'"                           
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
		
		return this.reqValue("", s);		
}	
public String them_moi_dd_enew(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String loaipkid
		, String dsmay 				
		, String chunhom
		, String ghiChu
		, String UploadID
		, String sIP		
  	) throws Exception {
		
	String seq = get_sq_dd();
	String[] somayarr = dsmay.split(",");
	String[] chunhomarr = chunhom.split(",");
	
	String tmp1 = "OK";
	int n = somayarr.length;
	int nn = chunhomarr.length;
	if (UploadID.equals("0")) {
		for (int i =0 ; i < n; i++)
		{
		  tmp1 = themmoi_dd_log(somayarr[i],loaiGoi,ghiChu, seq, "0");
			
		}
	}
	int dem = 0;
	String dem_s ="0";
	for (int j = 0 ; j < nn; j++)
	{
		dem = j+1;
		dem_s = ""+dem+"";
		String tmp2 = themmoi_dd_log(chunhomarr[j],loaiGoi,ghiChu, seq, dem_s);
	}
	
	//Dang ky dai dien huong cuoc
	
		String s=    "begin ? := "+FuncSchema+"pkg_group_ezcom.them_moi_dd("	
		+" '"+loaiGoi+"'"
		+",'"+UploadID+"'" 
		+",'"+seq+"'"   
		+",'"+loaipkid+"'" 		
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
		
		return this.reqValue("", s);		
}	
public String get_sq_dd() throws Exception
{
	String seq = "begin ? := admin_v2.pkg_regis_dd.get_seq_dd();"
        +"end;";
		
	return this.reqValue("", seq);
}

public String themmoi_dd_log(String dsmay, String loaiGoi, String ghiChu, String seq, String type) throws Exception
{
	
		//String[] somayarr = dsmay.split(",");
	   
		String tmp = "begin ? := admin_v2.pkg_regis_dd.them_moi_dd_log("	
			+" '"+loaiGoi+"'"                         
			+",'"+dsmay+"'"                           
			+",'"+type+"'"  
			+",'"+ConvertFont.UtoD(ghiChu)+"'"
			+",'"+getUserVar("userID")+"'"		
			+",'"+getUserVar("sys_agentcode")+"'"
			+",'"+seq+"');"
			+"end;"
			;
	return this.reqValue("", tmp);
}

public String cap_nhat_dd(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String sIP	
		, String loaiGoi
		, String idGoi			
		, String chunhom
		, String dsmay1
		, String dsmay2
		, String dsmay3		
		, String ghiChu 	
  	) throws Exception {

     String s=    "begin ? := "+FuncSchema+"pkg_qltb.cap_nhat_dd("	
		+" '"+loaiGoi+"'"
		+",'"+idGoi+"'"
		+",'"+dsmay1+"'"                           
		+",'"+dsmay2+"'"
		+",'"+dsmay3+"'"
		+",'"+chunhom+"'"	
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"
		+",'"+sIP+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"							
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}

public String huy_goi_dd(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String idGoi
		, String Loaigoi
		, String ghiChu
	    , String sMay_cn		
  	) throws Exception {
		
     String s=    "begin ? := "+sFuncSchema+"pkg_regis_data.huy_goi_dd("		
		+" '"+sDataSchema+"'"                                                             
		+",'"+getUserVar("sys_agentcode")+"'" 
		+",'"+getUserVar("userID")+"'"  
		+",'"+sMay_cn+"'"
		+",'"+idGoi+"'"
		+",'"+Loaigoi+"'"
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
public String huy_goi_tb_enew(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String dsmay 				
		, String chunhom
		, String ghiChu
        , String UploadID  		
		, String sIP		
  	) throws Exception {
		
	String seq = get_sq_dd();
	String[] somayarr = dsmay.split(",");
	String[] chunhomarr = chunhom.split(",");
	
	String tmp1 = "OK";
	int n = somayarr.length;
	int nn = chunhomarr.length;
	if (UploadID.equals("0")) {
		for (int i =0 ; i < n; i++)
		{
		  tmp1 = themmoi_dd_log(somayarr[i],loaiGoi,ghiChu, seq, "0");
			
		}
	}
	
	for (int j = 0 ; j < nn; j++)
	{
		String tmp2 = themmoi_dd_log(chunhomarr[j],loaiGoi,ghiChu, seq, "1");
	}
	
	//Dang ky dai dien huong cuoc
	
		String s=    "begin ? := "+FuncSchema+"pkg_group_ezcom.huy_goi_dd_tb("	
		+" '"+loaiGoi+"'"
		+",'"+seq+"'"                           
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"
		+",'"+UploadID+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
		
		return this.reqValue("", s);		
}	
     public String them_moi(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String dsmay 				
		, String chunhom
		, String ngay_dk
		, String ghiChu 		
		, String sIP		
  	) throws Exception {
    // String s=    "begin ? := "+FuncSchema+"pkg_regis_data.them_moi("	
	 String s=    "begin ? := "+FuncSchema+"pkg_api_data.them_moi("	
		+" '"+loaiGoi+"'"
		+",'"+dsmay+"'"                           
		+",'"+chunhom+"'"      
		+",'"+ngay_dk+"'"    		
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	
	
	public String cap_nhat(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String sIP	
		, String loaiGoi
		, String idGoi			
		, String chunhom
		, String ngay_dk
		, String dsmay 	
		, String ghiChu 
		, String kieu
		, String loaigoitach		
  	) throws Exception {

    // String s=    "begin ? := "+FuncSchema+"pkg_regis_data.cap_nhat("	
	 String s=    "begin ? := "+FuncSchema+"pkg_api_data.cap_nhat("	
		+" '"+loaiGoi+"'"
		+",'"+idGoi+"'"
		+",'"+dsmay+"'"                           
		+",'"+chunhom+"'"  
		+",'"+ngay_dk+"'"   
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+kieu+"'"
		+",'"+loaigoitach+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"		
		+",'"+sIP+"'"			
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	public String chuyenloai_goi(
	    	  String sUserId
			, String sAgentCode
			, String sDataSchema
			, String FuncSchema
			, String sIP	
			, String loaiGoi
			, String loaiGoi_cu
			, String idGoi			
			, String chunhom1
			, String chunhom2
			, String dsmay 	
			, String ghiChu 
	  	) throws Exception {
		String s=    "begin ? := "+FuncSchema+"pkg_regis_data.chuyenloai_goi("	     
			+" '"+loaiGoi+"'"
			+",'"+loaiGoi_cu+"'"
			+",'"+idGoi+"'"
			+",'"+dsmay+"'"                           
			+",'"+chunhom1+"'"  
			+",'"+chunhom2+"'" 			
			+",'"+ConvertFont.UtoD(ghiChu)+"'"
			+",'"+getUserVar("userID")+"'"		
			+",'"+getUserVar("sys_agentcode")+"'"
			+",'"+sDataSchema+"'"		
			+",'"+sIP+"'"	
			+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
	
	public String huy_goi(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String idGoi
		, String Loaigoi
		, String ghiChu
	    , String sMay_cn		
  	) throws Exception {
		
    // String s=    "begin ? := "+sFuncSchema+"pkg_regis_data.huy_goi("		
	 String s=    "begin ? := "+sFuncSchema+"pkg_api_data.huy_goi("		
		+" '"+sDataSchema+"'"                                                             
		+",'"+getUserVar("sys_agentcode")+"'" 
		+",'"+getUserVar("userID")+"'"  
		+",'"+sMay_cn+"'"
		+",'"+idGoi+"'"
		+",'"+Loaigoi+"'"
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	} 
	

	public String ghep_goi(
	    	  String sUserId
			, String sAgentCode
			, String sDataSchema
			, String FuncSchema
			, String sIP	
			, String loaiGoi
			, String idGoi_cu	
			, String idGoi_moi
			, String chunhom_cu
			, String chunhom_moi
			, String dsmay 	
			, String ghiChu 					
	  	) throws Exception {
	    // String s=    "begin ? := "+FuncSchema+"pkg_regis_data.ghep_goi("
		 String s=    "begin ? := "+FuncSchema+"pkg_api_data.ghep_goi("
			+" '"+loaiGoi+"'"
			+",'"+idGoi_cu+"'"
			+",'"+idGoi_moi+"'"
			+",'"+dsmay+"'"                           
			+",'"+chunhom_cu+"'"  
			+",'"+chunhom_moi+"'" 			
			+",'"+ConvertFont.UtoD(ghiChu)+"'"
			+",'"+getUserVar("userID")+"'"		
			+",'"+getUserVar("sys_agentcode")+"'"
			+",'"+sDataSchema+"'"		
			+",'"+sIP+"'"	
			+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
	
	public String ds_kieuth( String sCVid ) throws Exception {
	     String s=    "begin ? := admin_v2.pkg_regis_data.layds_dieukien(" +sCVid+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
		
///// upcode 20/09/2016
public String them_moi_dd_check(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String dsmay 				
		, String chunhom
		, String ghiChu 		
		, String sIP		
		) throws Exception {
			
		String seq = get_sq_dd();
		String[] somayarr = dsmay.split(",");
		String[] chunhomarr = chunhom.split(",");
		String count_="";
		String tmp1 = "OK";
		int n = somayarr.length;
		int nn = chunhomarr.length;
		
		for (int i =0 ; i < n; i++)
		{
		  tmp1 = themmoi_dd_log(somayarr[i],loaiGoi,ghiChu, seq, "0");
			
		}
		
		/*for (int j = 0 ; j < nn; j++)
		{
			String tmp2 = themmoi_dd_log(chunhomarr[j],loaiGoi,ghiChu, seq, "1");
		}*/
		count_ = check_sum(seq);
		return count_;//this.reqValue("", tmp1);	
	}
	public String check_sum(String seq) throws Exception
	{
		
			String tmp = "begin ? := admin_v2.pkg_regis_dd.check_sum("	
				+"'"+getUserVar("userID")+"'"		
				+",'"+getUserVar("sys_agentcode")+"'"
				+",'"+seq+"');"
				+"end;"
				;
		return this.reqValue("", tmp);
	}
	public String check_sum_ezcom(String UploadId,String seq,String loaigoidk) throws Exception
	{
		
			String tmp = "begin ? := admin_v2.pkg_group_ezcom.check_sum_V2("	
				+"'"+getUserVar("userID")+"'"		
				+",'"+getUserVar("sys_agentcode")+"'"				
				+",'"+seq+"'"
				+",'"+UploadId+"'"
				+",'"+loaigoidk+"');"
				+"end;"
				;
		return this.reqValue("", tmp);
	}
	public String check_group_ezcom(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String loaiGoidk
		, String dsmay 				
		, String chunhom
		, String ghiChu
		, String uploadID
		, String sIP		
		) throws Exception {
			
		String seq = get_sq_dd();
		String[] somayarr = dsmay.split(",");
		String[] chunhomarr = chunhom.split(",");
		String count_="";
		String tmp1 = "OK";
		int n = somayarr.length;
		int nn = chunhomarr.length;
		if (uploadID.equals("0")) {
			for (int i =0 ; i < n; i++)
			{
			  tmp1 = themmoi_dd_log(somayarr[i],loaiGoi,ghiChu, seq, "0");
				
			}
		
			for (int j = 0 ; j < nn; j++)
			{
				String tmp2 = themmoi_dd_log(chunhomarr[j],loaiGoi,ghiChu, seq, "1");
			}
		}
		if (uploadID.equals("0")) {		
			count_ = check_sum_ezcom("0",seq,loaiGoidk);
		} else {
			count_ = check_sum_ezcom(uploadID,seq,loaiGoidk);			
		}

		return count_;//this.reqValue("", tmp1);	
	}
	public String huy_goi_dd_tb(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String dsmay 				
		, String chunhom
		, String ghiChu 		
		, String sIP		
  	) throws Exception {
		
	String seq = get_sq_dd();
	String[] somayarr = dsmay.split(",");
	String[] chunhomarr = chunhom.split(",");
	
	String tmp1 = "OK";
	int n = somayarr.length;
	int nn = chunhomarr.length;
	
	for (int i =0 ; i < n; i++)
	{
	  tmp1 = themmoi_dd_log(somayarr[i],loaiGoi,ghiChu, seq, "0");
		
	}
	
	for (int j = 0 ; j < nn; j++)
	{
		String tmp2 = themmoi_dd_log(chunhomarr[j],loaiGoi,ghiChu, seq, "1");
	}
	
	//Dang ky dai dien huong cuoc
	
		String s=    "begin ? := "+FuncSchema+"pkg_regis_dd.huy_goi_dd_tb("	
		+" '"+loaiGoi+"'"
		+",'"+seq+"'"                           
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
		
		return this.reqValue("", s);		
}
	public String bosung_dd(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String dsmay 				
		, String chunhom
		, String ghiChu 		
		, String sIP		
  	) throws Exception {
		
	String seq = get_sq_dd();
	String[] somayarr = dsmay.split(",");
	String[] chunhomarr = chunhom.split(",");
	
	String tmp1 = "OK";
	int n = somayarr.length;
	int nn = chunhomarr.length;
	
	for (int i =0 ; i < n; i++)
	{
	  tmp1 = themmoi_dd_log(somayarr[i],loaiGoi,ghiChu, seq, "0");
		
	}
	
	for (int j = 0 ; j < nn; j++)
	{
		String tmp2 = themmoi_dd_log(chunhomarr[j],loaiGoi,ghiChu, seq, "1");
	}
	
	//Dang ky dai dien huong cuoc
	
		String s=    "begin ? := "+FuncSchema+"pkg_regis_dd.bosung_dd("	
		+" '"+loaiGoi+"'"
		+",'"+seq+"'"                           
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
		
		return this.reqValue("", s);		
}
public String bosung_dd_enew(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String loaipkid
		, String dsmay 				
		, String chunhom
		, String ghiChu
		, String UploadID
		, String sIP		
  	) throws Exception {
		
	String seq = get_sq_dd();
	String[] somayarr = dsmay.split(",");
	String[] chunhomarr = chunhom.split(",");
	
	String tmp1 = "OK";
	int n = somayarr.length;
	int nn = chunhomarr.length;
	if (UploadID.equals("0")) {
		for (int i =0 ; i < n; i++)
		{
		  tmp1 = themmoi_dd_log(somayarr[i],loaiGoi,ghiChu, seq, "0");
			
		}
	}
	int dem = 0;
	String dem_s ="0";
	
	for (int j = 0 ; j < nn; j++)
	{
		dem = j+1;
		dem_s = ""+dem+"";
		String tmp2 = themmoi_dd_log(chunhomarr[j],loaiGoi,ghiChu, seq, dem_s);
	}
	
	//Dang ky dai dien huong cuoc
	
		String s=    "begin ? := "+FuncSchema+"pkg_group_ezcom.bosung_dd("	
		+" '"+loaiGoi+"'"
		+",'"+seq+"'"                           
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"
		+",'"+UploadID+"'"
		+",'"+sIP+"'"
		+",'"+loaipkid+"'"
		+");"
        +"end;"
        ;
		
		return this.reqValue("", s);		
}

//Bo sung dang ky dai dien huong data:

//Check sluong TB
public String them_moi_data_check(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String dsmay 				
		, String chunhom
		, String ghiChu 		
		, String sIP		
		) throws Exception {
			
		String seq = get_sq_dd();
		String[] somayarr = dsmay.split(",");
		String[] chunhomarr = chunhom.split(",");
		String count_="";
		String tmp1 = "OK";
		int n = somayarr.length;
		int nn = chunhomarr.length;
		
		for (int i =0 ; i < n; i++)
		{
		  tmp1 = themmoi_dd_log(somayarr[i],loaiGoi,ghiChu, seq, "0");
			
		}
		int dem = 0;
		String dem_s ="0";
	
		for (int j = 0 ; j < nn; j++)
		{
			dem = j+1;
			dem_s = ""+dem+"";
			String tmp2 = themmoi_dd_log(chunhomarr[j],loaiGoi,ghiChu, seq, dem_s);
		}		
		count_ = check_sum(seq);
		return count_;//this.reqValue("", tmp1);	
	}
//Dang ky dai dien huong data
public String them_moi_data(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String dsmay 				
		, String chunhom
		, String ghiChu 		
		, String sIP		
  	) throws Exception {
		
	String seq = get_sq_dd();
	String[] somayarr = dsmay.split(",");
	String[] chunhomarr = chunhom.split(",");
	
	String tmp1 = "OK";
	int n = somayarr.length;
	int nn = chunhomarr.length;
	
	for (int i =0 ; i < n; i++)
	{
	  tmp1 = themmoi_dd_log(somayarr[i],loaiGoi,ghiChu, seq, "0");
		
	}
	int dem = 0;
	String dem_s ="0";
	for (int j = 0 ; j < nn; j++)
	{
		dem = j+1;
		dem_s = ""+dem+"";
		String tmp2 = themmoi_dd_log(chunhomarr[j],loaiGoi,ghiChu, seq, dem_s);
	}
	
	
	//Dang ky dai dien huong cuoc
	
		String s=    "begin ? := "+FuncSchema+"pkg_group_data.them_moi_dd("	
		+" '"+loaiGoi+"'"
		+",'"+seq+"'"                           
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
		
		return this.reqValue("", s);		
}

// Cap nhat dai dien huong data
public String cap_nhat_data(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String sIP
		, String loaiGoi
		, String sIDnhom 				
		, String chunhom
		, String ghiChu 		
				
  	) throws Exception {
		
	String seq = get_sq_dd();
	//String[] somayarr = dsmay.split(",");
	String[] chunhomarr = chunhom.split(",");
	
	String tmp1 = "OK";
	//int n = somayarr.length;
	int nn = chunhomarr.length;		
	int dem = 0;
	String dem_s ="0";
	for (int j = 0 ; j < nn; j++)
	{
		dem = j+1;
		dem_s = ""+dem+"";
		String tmp2 = themmoi_dd_log(chunhomarr[j],loaiGoi,ghiChu, seq, dem_s);
	}
	//Dang ky dai dien huong cuoc
	
		String s=    "begin ? := "+FuncSchema+"pkg_group_data.capnhat_dd("	
		+" '"+loaiGoi+"'"
		+",'"+seq+"'" 
		+",'"+sIDnhom+"'" 		
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
		
		return this.reqValue("", s);		
}
//// end		
}














