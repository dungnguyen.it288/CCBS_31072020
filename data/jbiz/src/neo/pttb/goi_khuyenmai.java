package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class goi_khuyenmai extends NEOProcessEx {
  public void run() {
    System.out.println("neo.goi_khuyenmai was called");
  }
  	public NEOCommonData ds_cv_km_goi(int pikieu)
	{
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=PTTB/laphd/khuyenmai_goi/ds_km_congvans&" + CesarCode.encode("iType") + "=" +pikieu;
		NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
	}
	public NEOCommonData themmoi_tbkm_dacbiet(	 String psschema,
  										 String psschemaCommon,   
  										 String psma_tb
										,String pikieugoi_id
										,String pikmht_id
										,String pikmcv_id
										,String pskieuld_id										
										,String psghichu
										,String pdngay_ld									
										,String pdngay_in_db
										,String psnguoi_cn
										,String psmay_cn
									
									)
  	{
  			
    String s=    "begin ? := admin_v2.pkg_goi.themmoi_tbkm('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	                   
        +"','"+psma_tb+"'"
		+","+pikieugoi_id
        +","+pikmht_id
		+","+pikmcv_id		
        +","+pskieuld_id
        +",'"+psghichu 
		+"','"+pdngay_ld 
	    +"','"+pdngay_in_db 
		+"','"+psnguoi_cn 
		+"','"+psmay_cn 
        +"');"
        +"end;"
        ;
     System.out.println(s);
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 //	nei_.setDataSrc("VNPBilling");
	return new NEOCommonData(nei_);  
  	}
	public NEOCommonData xoa_tbkm_dacbiet( String psschema, 
										   String psschemaCommon, 	 
 										   String psma_tb,
										   String pikieugoi_id,
										   String pikmht_id)
  	{
  			
    String s=    "begin ? := admin_v2.pkg_goi.xoa_tbkm('"+getUserVar("userID")
		+"','"+getUserVar("sys_dataschema")+"'"
        +",'"+psma_tb+"'"
		+","+pikieugoi_id
        +","+pikmht_id      
        +");"
        +"end;"
        ;        
    System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	// 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);  
  }
    public String layds_ls_dangkygoikm(String psschema , String CommonSchema, String ma_tb,String psKieu)
  	{
	return "/main?"+CesarCode.encode("configFile")+"=PTTB/laphd/khuyenmai_goi/ls_khuyenmai_goi_ajax"
   				+"&"+CesarCode.encode("ma_tb") + "=" + ma_tb
				+"&"+CesarCode.encode("pskieu") + "=" + psKieu;
  	}
    public NEOCommonData laytt_tb_dacbiet ( String func_schema,
 										 String schemaCommon,
 										 String ma_tb,
										 String pskieu,
 										 String khuyenmai_id) 
 										{
   String s= "begin ?:=admin_v2.pkg_goi.laytt_tb_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"','"+ma_tb+"'"
		+","+pskieu
		+","+khuyenmai_id
		+");end;";
	
	NEOExecInfo nei_ = new NEOExecInfo(s);
	//nei_.setDataSrc("VNPBilling");
	return new NEOCommonData(nei_);  
  }
 }