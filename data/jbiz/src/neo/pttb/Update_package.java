package neo.pttb;
import neo.smartui.process.*;
import neo.smartui.common.*;

public class Update_package extends NEOProcessEx {
	public String change_font(
				String str_change
								)
	{
		String s ="";
		System.out.println("-------------Thuyvttt"+s);
		 try {
			if (!str_change.equals("")){
				String[] str_temp = str_change.split("$");
				//System.out.println("so phan tu str = "+str_temp.length);
				for (int j=0; j<str_temp.length; j++){
					String temp1 = str_temp[j];
					String id_ = temp1.substring(0,temp1.indexOf(":"));
					String str_ = temp1.substring(temp1.indexOf(":")+1);
					s = s+id_+":"+ConvertFont.UtoD(str_)+"$";
				}
				s = s.substring(0,s.length()-1);
			} else {
				s = str_change;
			}
			return s;
		} catch(Exception e){
			return "error:"+e;
		}		
		
	} 
	public String add_goi(
		String dispatch_id_src			,
		String package_id_src  	 		,
		String package_id_dst  	 		,		
		String Description       		,
		String date_from 		        ,
		String date_to 		            ,
		String Time_begin 		        ,
		String Circle          	 		,
		String enabled         	 		,
		String is_gdv         	 		,
		String is_camket         	 	,
		String is_sms					,
		String syntax          	 		,
		String prefix_sms_caller		,
		String sms_quangba		,
		String sms_notif_1     	 		,
		String sms_notif_2     	 		,
		String sms_confirm_success    	,
		String sms_confirm_notsuccess	,
		String package_id_add  	 		,
		String type						,
		String puserid) throws Exception {
			Description = change_font(Description);
			//System.out.println("-------------Thuyvttt ---change Des = "+Description);
			sms_notif_1 = change_font(sms_notif_1);
			//System.out.println("-------------Thuyvttt ---change sms_notif_1 = "+sms_notif_1);
			sms_notif_2 = change_font(sms_notif_2);
			//System.out.println("-------------Thuyvttt ---change sms_notif_2 = "+sms_notif_2);
			sms_confirm_success = change_font(sms_confirm_success);
			//System.out.println("-------------Thuyvttt ---change sms_confirm_success = "+sms_confirm_success);
			sms_confirm_notsuccess = change_font(sms_confirm_notsuccess);
			//System.out.println("-------------Thuyvttt ---change sms_confirm_notsuccess = "+sms_confirm_notsuccess);
			
			String s ="begin ? :=ADMIN_V2.pkg_upgrade_setting.add_goi("
						+"'"+dispatch_id_src		+"',"
						+"'"+package_id_src  	 	+"',"
						+"'"+package_id_dst  	 	+"',"		
						+"'"+Description     		+"',"
						+"'"+date_from 		        +"',"
						+"'"+date_to 		        +"',"
						+"'"+Time_begin 		    +"',"
						+"'"+Circle          	 	+"',"
						+"'"+enabled         	 	+"',"
						+"'"+is_gdv         	 	+"',"
						+"'"+is_camket         	 	+"',"
						+"'"+is_sms					+"',"
						+"'"+syntax          	 	+"',"
						+"'"+prefix_sms_caller		+"',"
						+"'"+sms_quangba		+"',"
						+"'"+sms_notif_1    	 		+"',"
						+"'"+sms_notif_2     	 		+"',"
						+"'"+sms_confirm_success   	+"',"
						+"'"+sms_confirm_notsuccess	+"',"
						+"'"+package_id_add  	 	+"',"
						+type  	 	+"," // 1: loai them moi, 2: sua doi
						+"'"+getUserVar("userID")+"',"
						+"'"+getUserVar("userIP")+"'"  
						+"); end;";
			
		System.out.println(s);
       return this.reqValue("", s);
	}
	
	public String xoa_goi(		
		String idGoi
		) throws Exception {		
			String s ="begin ? :=ADMIN_V2.pkg_upgrade_setting.xoa_goi("
				+idGoi+","
				+"'"+getUserVar("userID")+"',"
				+"'"+getUserVar("userIP")+"'"  				
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	
	public String edit(String idGoi)
	{
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=pttb/dkygoi/upgrade_setting/ds_goicuoc_edit";
		url_ = url_ + "&"+CesarCode.encode("id_edit")+"="+idGoi;
		return url_;
	}
	
	
}