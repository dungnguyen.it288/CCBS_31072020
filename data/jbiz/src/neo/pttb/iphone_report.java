package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class iphone_report
    extends NEOProcessEx {
  public void run() {
    System.out.println("neo.report was called");
  }
	public  String layds_goicuoc(String hopdong_id)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/iphone/layds_goicuoc"
   				+"&"+CesarCode.encode("hd_id") + "=" + hopdong_id;
   			}
	public  String change_buucuc_donvi(String donvi_ql){
   		return "/main?"+CesarCode.encode("configFile")+"=report/iphone/ds_buucuc_data"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   	}
	public  String layds_giaodich(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/iphone/param_user"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   			}
	
	public  String check_role(String uid)
	{

		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " ? := PTTB_IPHONE_REPORT.check_role('"+uid+"'); ";
		xs_ = xs_ + " end; ";

		return xs_ ;
	}
//-------------export goi cuoc hssv-----------------------------------------------------------------
    public NEOCommonData getGoicuoc_hssv(String psschema,
                                        String psuser,
										String donvi_ql,
									    String buucuc,
									    String nguoi_th,
									    String psma_tinh,									   
									    String tungay,
									    String denngay,									   
									    String goi_cuoc,									   
									    String kieu_bc
									   ){

	String xs_="";	
	String psuserid = getUserVar("userID"); 
	xs_ = xs_ + "begin ? := ADMIN_V2.PTTB_BAOCAO.layds_bc_hssv_xls('" 
															+ psschema +"','"
															+ donvi_ql +"','"
															+ buucuc +"','"
															+ nguoi_th +"','"
															+ psma_tinh +"','"
															+ tungay +"','"
															+ denngay +"','"															
															+ goi_cuoc +"','"
															+ kieu_bc +"'); end;";
											
	NEOExecInfo ei_ = new NEOExecInfo(xs_);
    return  new NEOCommonData(ei_);
   }  	
}
