package neo.pttb;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import cosaldap.Base64.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;
import java.io.*;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.ResultSetMetaData;
import javax.sql.RowSet;

public class blacklist_ir extends NEOProcessEx {

    public void run() {
        System.out.println("blacklist_ir was called");
    }

    public String add(String stb, String userupdate, String services, String packages, String matinh
    )
            throws Exception {
		
        String xs_ = "";
        xs_ = xs_ + " begin ";
        xs_ = xs_ + " 	?:= admin_v2.blacklist_ir_config.add_blacklist_ir('"+ stb + "','"+ userupdate + "','"+ ConvertFont.UtoD(services) + "','"+ ConvertFont.UtoD(packages) + "','" + matinh+ "'); ";
        xs_ = xs_ + " end; ";

        System.out.println(xs_);
        return this.reqValue("", xs_);
    }
	
    public String edit(String stb, String userupdate, String pastuserupdate, String pasttimeupdate, String paststb, String services, String packages, String matinh
    )
            throws Exception {
		String xs_ = "";
        xs_ = xs_ + " begin ";
        xs_ = xs_ + " 	?:= admin_v2.blacklist_ir_config.edit_blacklist_ir('" + stb + "','"
                + userupdate + "','"
                + pastuserupdate + "','"
                + pasttimeupdate + "','"
                + paststb + "','"
				+ ConvertFont.UtoD(services) + "','"
				+ ConvertFont.UtoD(packages) + "','"
				+ matinh
                + "'); ";
        xs_ = xs_ + " end; ";

        System.out.println(xs_);
        return this.reqValue("", xs_);
    }
	
	public String del(String stb, String userupdate, String services, String packages)
            throws Exception {
        String xs_ = "";
        xs_ = xs_ + " begin ";
        xs_ = xs_ + " 	?:= admin_v2.blacklist_ir_config.del_blacklist_ir('"+ stb + "','"+ userupdate + "','"+ ConvertFont.UtoD(services) + "','"+ ConvertFont.UtoD(packages) + "'); ";
        xs_ = xs_ + " end; ";

        System.out.println(xs_);
        return this.reqValue("", xs_);
    }

}
