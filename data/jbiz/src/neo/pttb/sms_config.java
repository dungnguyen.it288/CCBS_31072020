package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class sms_config extends NEOProcessEx {
	public String new_sms(		
		String pschema,
		String puserid,
		String puserip,
		String plistmsisdn,
		String pagent,
		String psms_type,
		String pcontent,
		String pstatus) throws Exception {
			String s ="begin ? :="+pschema+"pkg_config_sms.new_sms_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+plistmsisdn+"',"				
				+"'"+pagent+"',"
				+"'"+psms_type+"',"
				+"'"+pcontent+"',"
				+"'"+pstatus+"'"									
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String update_sms(		
		String pschema,
		String puserid,
		String puserip,
		String pid,
		String psms_type,
		String pcontent,
		String pstatus) throws Exception {
			String s ="begin ? :="+pschema+"pkg_config_sms.update_sms_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+pid+"',"				
				+"'"+psms_type+"',"
				+"'"+pcontent+"',"
				+"'"+pstatus+"'"									
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String accept_upload_sms(		
		String psUpload_id,
		String psContent,
		String psSms_type,
		String psuserid,
		String psuserip) throws Exception {
			String s ="begin ? :=admin_v2.pkg_config_sms.accept_upload_sms_func("
				+"'"+psUpload_id+"',"
				+"'"+psContent+"',"
				+"'"+psSms_type+"',"				
				+"'"+psuserid+"',"				
				+"'"+psuserip+"'"								
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String accept_data_sms(	String psUploadId,
											String psAgentCode, 
											String psTypeUpload, 
											String psuserid,
											String psuserip,
											String noidung,
											String dauso,
											String typedata)throws Exception {
			String s ="begin ? :=admin_v2.pkg_sms_config.accept_upload_sms_func("				
								+"'"+ psUploadId    +"',"
								+"'"+ psAgentCode   +"',"
								+"'"+ psTypeUpload  +"',"
								+"'"+ psuserid      +"',"
								+"'"+ psuserip      +"',"
								+"'"+ noidung      +"',"
								+"'"+ dauso      +"',"
								+"'"+ typedata      +"'"
								+"); end;";
			System.out.println(s);			
			return this.reqValue("", s);
	}
	public String addnew_sms(		
		String puserid,
		String puserip,
		String pagent,
		String usershema,
		String table,
		String pcontent,
		String pcolumn,
		String pstatus,
		String pstypeid,
		String pscot,
		String pstypedata) throws Exception {
			String s ="begin ? :=admin_v2.pkg_sms_config.addnew_sms_func("
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+pagent+"',"				
				+"'"+usershema+"',"
				+"'"+table+"',"
				+"'"+pcontent+"',"
				+"'"+pcolumn+"',"
				+"'"+pstatus+"',"
                +"'"+pstypeid+"',"
                +"'"+pscot+"',"
				+"'"+pstypedata+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String send_all_sms(		
		String pstatus,
		String pstypedata) throws Exception {
			String s ="begin ? :=admin_v2.pkg_sms_config.send_sms_all("
				+"'"+pstatus+"',"
                +"'"+pstypedata+"'"				
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String send_id_sms(		
		String psid,
		String pstatus) throws Exception {
			String s ="begin ? :=admin_v2.pkg_sms_config.send_sms_id("
				+"'"+psid+"',"
                +"'"+pstatus+"'"				
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String send_check_sms(		
		String psid,
		String pstatus) throws Exception {
			String s ="begin ? :=admin_v2.pkg_sms_config.send_sms_list_id("
				+"'"+psid+"',"
                +"'"+pstatus+"'"				
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	
	public String new_sms_upload(		
		String pschema,
		String puserid,
		String puserip,
		String plistmsisdn,
		String pagent,
		String psms_type,
		String pcontent,
		String pstatus,
		String pstypedata) throws Exception {
			String s ="begin ? :="+pschema+"pkg_sms_config.new_sms_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+plistmsisdn+"',"				
				+"'"+pagent+"',"
				+"'"+psms_type+"',"
				+"'"+pcontent+"',"
				+"'"+pstatus+"',"
                 +"'"+pstypedata+"'"				
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String update_sms_upload(		
		String pschema,
		String puserid,
		String puserip,
		String pid,
		String psms_type,
		String pcontent,
		String pstatus) throws Exception {
			String s ="begin ? :="+pschema+"pkg_sms_config.update_sms_func("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+pid+"',"				
				+"'"+psms_type+"',"
				+"'"+pcontent+"',"
				+"'"+pstatus+"'"									
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
}