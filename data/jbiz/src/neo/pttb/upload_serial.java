package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;



public class upload_serial extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb.upload_serial was called");
  }
	public String rec_tonghop( String upload_id) 
 		{
		String s= "select count(1)-count(kiemtra) hople,count(1) tong from admin_v2.UPLOAD_SERIAL a where a.upload_id = "+upload_id;
        return s;
  }
	
  public String doc_hople(
			String dataschema, 
			String upload_id)
  	{ 
	 	String url_="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=pttb/upload_serial/ajax_hople";
		url_ = url_ + "&"+CesarCode.encode("data_schema")+"="+dataschema;	
		url_ = url_ + "&"+CesarCode.encode("upload_id")+"="+upload_id;	    
	  return url_;
  	}
	
	public String doc_khonghople(
   			String dataschema,
			String upload_id)
  	{ 
		String url_="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=pttb/upload_serial/ajax_khonghople";	
		url_ = url_ + "&"+CesarCode.encode("data_schema")+"="+dataschema;	
		url_ = url_ + "&"+CesarCode.encode("upload_id")+"="+upload_id;	    
		return url_;		  
  	}
	
	public String accept_serial(
		String funcschema,
		String upload_id,
		String psUser_ip,
		String psHoten,
		String psDienthoai	
	)
	{ 
		return "begin ? :=" + funcschema + "pkg_upload.accept_upload("
			+"'"+upload_id+"',"
			+"'"+getUserVar("sys_agentcode")+"',"
			+"'"+getUserVar("userID")+"',"
			+"'"+psUser_ip+"',"
			+"'"+psHoten+"',"
			+"'"+psDienthoai+"'); end;";	
	}
	public NEOCommonData layTTThe(String so_the)
	{
		String so_the_ = so_the.replace("'","''");
		String uid_ = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");

		String xs_ = "";
		xs_ = xs_ + "begin ?:= PKG_LVP.laytt_doithe('"+so_the_+"','"+uid_+"','GPC','"+matinh_ngdung_+"'); end;";
        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("LVP_DBMS");		

		return new NEOCommonData(ei_);
	}
	public NEOCommonData doithe(String so_the, String ten_dl, String so_dt)
	{
		String so_the_ = so_the.replace("'","''");
		String ten_dl_ = ten_dl.replace("'","''");
		String so_dt_ = so_dt.replace("'","''");
		
		String uid_ = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		
		String xs_ = "";
		xs_ = xs_ + "begin ?:= PKG_LVP.capnhat_doithe('"+so_the_+"','"+uid_+"','GPC','"+matinh_ngdung_+"','"+ten_dl_+"','"+so_dt_+"'); end;";

        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("LVP_DBMS");		

		return new NEOCommonData(ei_);
	}	

	public String docDsDoiThe(String serial, String tu_ngay, String den_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=pttb/upload_serial/mod_dshuythe";
		url_ = url_ + "&"+CesarCode.encode("serial")+"="+serial;
		url_ = url_ + "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay;
		url_ = url_ + "&"+CesarCode.encode("den_ngay")+"="+den_ngay;
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}	
	
}