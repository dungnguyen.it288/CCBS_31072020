package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class dangkynhomvnpt
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb.dangkynhomvnpt was called");
  }

 
   public String layDSGoiNhom(
		 String psSOMAY                         
		
  	) {
	 String sUserId= getUserVar("userID");
	 String sAgentCode = getUserVar("sys_agentcode");
	 String sDataSchema = getUserVar("sys_dataschema");
	 String sFuncSchema = getSysConst("FuncSchema");
	 String sCommonSchema = getSysConst("CommonSchema");
	 
     String s=    "begin ? := "+sFuncSchema+"PKG_NHOMVNPT_NEW.layds_goi_nhom("		
		+"'"+sFuncSchema+"'"
		+",'"+sCommonSchema+"'"                                  
		+",'"+psSOMAY+"'"  
		+",'"+sAgentCode+"'"	
		+");"
        +"end;"
        ;
    // System.out.println(s);
    return s;
	}
  
 public String themMoiNhom(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String sCommonSchema
	 
		, String tenNhom
		, String dsmay 
		, String loaiNhom 
		, String ngayDK 
		, String ghiChu
		, String capnhat
		, String chuNhom	    
		, String chuky	
		, String may_cn		
  	) {

	 
     String s=    "begin ? := "+sFuncSchema+"PKG_NHOMVNPT_NEW.them_moi_nhom("		
	 
		+" '"+sFuncSchema+"'"
		+",'"+sCommonSchema+"'"                                  
		+",'"+sUserId+"'"                                  
		+",'"+sAgentCode+"'"                                  
		
		+",'"+tenNhom+"'"
		+",'"+dsmay+"'"
		+",'"+loaiNhom+"'"
		+",'"+ngayDK+"'"
		+",'"+ghiChu+"'"		
		+",'"+capnhat+"'"
		+",'"+chuNhom+"'"	
		+",'"+chuky+"'"	
		+",'"+may_cn+"'"	        	
		+");"
        +"end;"
        ;
    //System.out.println(s);
    return s;
	}
	
public String capNhatNhom(
    	  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String sCommonSchema
		
		,String	tenNhom
		,String	idNhom 
		,String	ds_codinh 
		,String	ngayDK 
		,String dsMayThem 
		,String	ghiChu
		,String chuNhom 
		,String may_cn 
		,String type 
  	) {

	 
     String s=    "begin ? := "+sFuncSchema+"PKG_NHOMVNPT_NEW.cap_nhat("		
	 
		+" '"+sFuncSchema+"'"
		+",'"+sCommonSchema+"'"                                  
		+",'"+sUserId+"'"                                  
		+",'"+sAgentCode+"'"                                  		
		+",'"+tenNhom+"'"
		+",'"+idNhom+"'"
		+",'"+ds_codinh+"'"
		+",'"+ngayDK+"'"
		+",'"+dsMayThem+"'"
		+",'"+ghiChu+"'"		
		+",'"+chuNhom+"'"	
		+",'"+may_cn+"'"
		+",'"+type+"'"
		+");"
        +"end;"
        ;
    //System.out.println(s);
    return s;
	}
  
   public String tachNhom(
		 String	sSomay
		
  	) {
	 String sUserId= getUserVar("userID");
	 String sAgentCode = getUserVar("sys_agentcode");
	 String sDataSchema = getUserVar("sys_dataschema");
	 String sFuncSchema = getSysConst("FuncSchema");
	 String sCommonSchema = getSysConst("CommonSchema");
	 
     String s=    "begin ? := "+sFuncSchema+"PKG_NHOMVNPT_NEW.tach_nhom("		
	 
		+" '"+sFuncSchema+"'"
		+",'"+sCommonSchema+"'"                                  
		+",'"+sUserId+"'"                                  
		+",'"+sAgentCode+"'"                                  
		
		+",'"+sSomay+"'"
	
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
   
   
  public String layDSTachNhom(String somay){
 	
 	 return "/main?"+CesarCode.encode("configFile")+"=PTTB/laphd/dangkygoinhom/dsthuebao_ajax"
		+"&"+CesarCode.encode("somay")+"="+somay;
 }

 public String themMoi_dn(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String	dsmay 
		, String	ngayDK 
		, String	ghiChu
		, String capnhat
		, String chuNhom 
  	) {

	 
     String s=    "begin ? := "+sFuncSchema+"PKG_NHOMVNPT.themmoi_nhom_doanhnghiep("		
	 
		+" '"+sDataSchema+"'"                                
		+",'"+sUserId+"'"                                  
		+",'"+sAgentCode+"'"                                  
		+",'"+dsmay+"'"
		+",'"+ngayDK+"'"
		+",'"+ghiChu+"'"		
		+",'"+capnhat+"'"
		+",'"+chuNhom+"'"	
		+");"
        +"end;"
        ;
    //System.out.println("Test :" +s);
    return s;
	}

	
 public String LSNhom_Somay(String so_tb){
 	
 	 return "/main?"+CesarCode.encode("configFile")+"=PTTB/laphd/nhom_vnpt/lichsu_ajax"
		+"&"+CesarCode.encode("so_tb")+"="+so_tb;
 }

 public String layDSGoiNhom_dn(
		 String psSOMAY                         
		
  	) {
	 String sDataSchema = getUserVar("sys_dataschema");
	 String sFuncSchema = getSysConst("FuncSchema"); 
     String s=    "begin ? := "+sFuncSchema+"PKG_NHOMVNPT.layds_nhom_dn("	
		+"'"+sDataSchema+"'"
		+",'"+psSOMAY+"'"                               
		+");"
        +"end;"
        ;
    //System.out.println("luattd :" +s);
    return s;
   }

public String huynhom(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String sCommonSchema	 
		, String idnhom
	    , String may_cn		
  	) {

	 
     String s=    "begin ? := "+sFuncSchema+"PKG_NHOMVNPT_NEW.huy_nhom("		
	 
		+" '"+sFuncSchema+"'"
		+",'"+sCommonSchema+"'"                                  
		+",'"+sUserId+"'"                                  
		+",'"+sAgentCode+"'"                                  		
		+",'"+idnhom+"'"
		+",'"+may_cn+"'"		
		+");"
        +"end;"
        ;
    //System.out.println(s);
    return s;
	}
}














