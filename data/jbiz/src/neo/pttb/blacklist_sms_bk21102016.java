package neo.pttb;
import neo.smartui.process.*;
import neo.smartui.common.*;

public class blacklist_sms extends NEOProcessEx {
	 public void run() {
		    System.out.println("neo.pttb.bl was called");
		  }
	public String new_blacklist_sms(
		String pschema,
		String pmsisdn,
		String ptype_id,
		String pagent_code,
		String pcreate_by,
		String pnote,
		String puserid,
		String puserip) throws Exception {
		pnote= pnote.replace("'","''");
			String s ="begin ? :=ADMIN_V2.pkg_sms.new_blacklist_sms("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(pmsisdn)+"',"
				+"'"+ptype_id+"',"
				+"'"+ConvertFont.UtoD(pagent_code)+"',"
				+"'"+ConvertFont.UtoD(pcreate_by)+"',"
				+"'"+ConvertFont.UtoD(pnote)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_blacklist_sms(
		String pschema,
		String pid,
		String pstype,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_sms.del_blacklist_sms("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+pstype+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_blacklist_sms(
	String pschema,
		String pid,
		String pmsisdn,
		String ptype_id,
		String pagent_code,
		String pcreate_by,
		String pcreate_date,
		String pnote,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_sms.edit_blacklist_sms("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pmsisdn)+"',"
				+"'"+ptype_id+"',"
				+"'"+ConvertFont.UtoD(pagent_code)+"',"
				+"'"+ConvertFont.UtoD(pcreate_by)+"',"
				+"'"+pcreate_date+"',"
				+"'"+ConvertFont.UtoD(pnote)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}

	public String xuly_tb_bl (								
									String upload_id,
									String funcschema,
									String funcCommon,
									String psuserip
	)throws Exception
	{ 
		String s = "begin ? :=admin_v2.pkg_upload.accept_upload_bl_sms("
			+"'"+upload_id+"',"
			+"'"+getUserVar("sys_agentcode")+"',"
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";	
		System.out.println(s);
		return this.reqValue("",s);
	}
	public String phanquyen_ds(
		String users, String khos,String psmenu,String userip, String agentcode
	) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_sms.gant_role_blacklist("
				+"'"+users+"',"
				+"'"+khos+"',"
				+"'"+psmenu+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+userip+"',"
				+"'"+agentcode+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String check_role(
		String users, String psMenu,String psDS
	) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_sms.check_role_backlist("
				+"'"+users+"',"
				+"'"+psMenu+"',"
				+"'"+psDS+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}

public String new_user_fixed(
		String pcontent,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.qtht.new_fixeduser("
				+"'"+pcontent+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_user_fixed(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.qtht.del_user_fixed("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	/*public String accept_data_contact (								
									String upload_id,
									String psAgentCode,
									String psUserId,
									String psuserip
	)throws Exception
	{ 
		String s = "begin ? :=admin_v2.pkg_upload.accept_upload_data_contact("
			+"'"+upload_id+"',"
			+"'"+getUserVar("sys_agentcode")+"',"
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";	
		System.out.println(s);
		return this.reqValue("",s);
	}	*/
	public String accept_data_contact (								
									String upload_id,
									String psAgentCode,
									String psUserId,
									String psuserip
	)throws Exception
	{ 
		String s = "begin ? :=admin_v2.pkg_upload.accept_upload_datacont_func("
			+"'"+upload_id+"',"
			+"'"+getUserVar("sys_agentcode")+"',"
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";	
		System.out.println(s);
		return this.reqValue("",s);
	}
	
}