package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class alo_doanh_nghiep
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb.goi_truyen_solieu was called");
  }


   public String LSNhom_Somay(String so_tb){
 	
 	 return "/main?"+CesarCode.encode("configFile")+"=PTTB/laphd/alo_doanh_nghiep/lichsu_ajax"
		+"&"+CesarCode.encode("so_tb")+"="+so_tb;
 }
   public String layDSGoiNhom(
		 String psSOMAY ,
		 String psloai                         
		
  	) {
	 String sUserId= getUserVar("userID");
	 String sAgentCode = getUserVar("sys_agentcode");
	 String sDataSchema = getUserVar("sys_dataschema");
	 String sFuncSchema = getSysConst("FuncSchema");
	 String sCommonSchema = getSysConst("CommonSchema");
	 
     String s=    "begin ? := "+sFuncSchema+"PKG_ALO_DN.layds_goi_nhom("		
		+"'"+psSOMAY+"'"
		+",'"+psloai+"'"	
		+",'"+sAgentCode+"'"	
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
  
     public String themMoiNhom(
		  String sUserId
		, String sAgentCode
		, String sFuncSchema
		, String sdataSchema
		, String loaiNhom
		, String dsmay 				
		, String ghiChu 
		, String chunhom
		, String dsmay_2	
		, String dsVip
		, String dsVip_2
		, String may_cn		
  	) {
	 
     String s=    "begin ? := admin_v2.pkg_goi.them_moi_nhom("	
		+" '"+sdataSchema+"'"
		+",'"+sUserId+"'"                           
		+",'"+sAgentCode+"'"                                  		
		+",'"+loaiNhom+"'"
		+",'"+dsmay+"'"		
		+",'"+ghiChu+"'"				
		+",'"+may_cn+"'"
		+",'"+chunhom+"'"
		+",'"+dsmay_2+"'"	
		+",'"+dsVip+"'"
		+",'"+dsVip_2+"'"
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
	
	
	
	public String capNhatNhom_alo(
    	  String sUserId
		, String sAgentCode
		, String sFuncSchema
		, String sdataSchema
		, String may_cn	 
		, String loaiNhom
		, String goi_id	
		, String chu_nhom
		, String alo_1	
		, String alo_2			
		, String vip250		
		, String vip350	
		, String ds_tachnhom	
  	) {

	 
     String s=    "begin ? := admin_v2.pkg_goi.cap_nhat_nhom_alo("	
		+" '"+sdataSchema+"'"
		+",'"+sUserId+"'"                           
		+",'"+sAgentCode+"'"
		+",'"+may_cn+"'"		
		+",'"+loaiNhom+"'"
		+",'"+goi_id+"'"
		+",'"+chu_nhom+"'"
		+",'"+alo_1+"'"
		+",'"+alo_2+"'"
		+",'"+vip250+"'"
		+",'"+vip350+"'"
		+",'"+ds_tachnhom+"'"	
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
	
   
	
	public String huynhom(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String sCommonSchema	 
		, String idnhom
		, String loainhom
	    , String may_cn		
  	) {

	 
     String s=    "begin ? := admin_v2.pkg_goi.huy_nhom("		
	 
		+" '"+sCommonSchema+"'"                             
		+",'"+sUserId+"'"                                  
		+",'"+sAgentCode+"'"                                  		
		+",'"+idnhom+"'"
		+",'"+loainhom+"'"
		+",'"+may_cn+"'"		
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
}














