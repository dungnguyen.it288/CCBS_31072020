package neo.pttb;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import cosaldap.Base64.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException; 
import neo.qlsp.*;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.NodeList;
import neo.tracuu_tb;

public class dk_goi
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb.dk_goi was called");
  }   
	public String them_moi_dd(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String dsmay 				
		, String chunhom
		, String ghiChu 		
		, String sIP		
  	) throws Exception {

     String s=    "begin ? := "+FuncSchema+"pkg_regis_data.them_moi_dd("	
		+" '"+loaiGoi+"'"
		+",'"+dsmay+"'"                           
		+",'"+chunhom+"'"  
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}	
	
public String cap_nhat_dd(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String sIP	
		, String loaiGoi
		, String idGoi			
		, String chunhom
		, String dsmay1
		, String dsmay2
		, String dsmay3		
		, String ghiChu 	
  	) throws Exception {

     String s=    "begin ? := "+FuncSchema+"pkg_qltb.cap_nhat_dd("	
		+" '"+loaiGoi+"'"
		+",'"+idGoi+"'"
		+",'"+dsmay1+"'"                           
		+",'"+dsmay2+"'"
		+",'"+dsmay3+"'"
		+",'"+chunhom+"'"	
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"
		+",'"+sIP+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"							
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}

public String huy_goi_dd(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String idGoi
		, String Loaigoi
		, String ghiChu
	    , String sMay_cn		
  	) throws Exception {
		
     String s=    "begin ? := "+sFuncSchema+"pkg_regis_data.huy_goi_dd("		
		+" '"+sDataSchema+"'"                                                             
		+",'"+getUserVar("sys_agentcode")+"'" 
		+",'"+getUserVar("userID")+"'"  
		+",'"+sMay_cn+"'"
		+",'"+idGoi+"'"
		+",'"+Loaigoi+"'"
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}	
     public String them_moi(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String dsmay 				
		, String chunhom
		, String ngay_dk
		, String ghiChu 		
		, String sIP		
  	) throws Exception {
    // String s=    "begin ? := "+FuncSchema+"pkg_regis_data.them_moi("	
	 String s=    "begin ? := "+FuncSchema+"pkg_api_data.them_moi("	
		+" '"+loaiGoi+"'"
		+",'"+dsmay+"'"                           
		+",'"+chunhom+"'"      
		+",'"+ngay_dk+"'"    		
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	
	
	public String cap_nhat(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String sIP	
		, String loaiGoi
		, String idGoi			
		, String chunhom
		, String ngay_dk
		, String dsmay 	
		, String ghiChu 
		, String kieu
		, String loaigoitach		
  	) throws Exception {

    // String s=    "begin ? := "+FuncSchema+"pkg_regis_data.cap_nhat("	
	 String s=    "begin ? := "+FuncSchema+"pkg_api_data.cap_nhat("	
		+" '"+loaiGoi+"'"
		+",'"+idGoi+"'"
		+",'"+dsmay+"'"                           
		+",'"+chunhom+"'"  
		+",'"+ngay_dk+"'"   
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+kieu+"'"
		+",'"+loaigoitach+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"		
		+",'"+sIP+"'"			
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	public String chuyenloai_goi(
	    	  String sUserId
			, String sAgentCode
			, String sDataSchema
			, String FuncSchema
			, String sIP	
			, String loaiGoi
			, String loaiGoi_cu
			, String idGoi			
			, String chunhom1
			, String chunhom2
			, String dsmay 	
			, String ghiChu 
	  	) throws Exception {
		String s=    "begin ? := "+FuncSchema+"pkg_regis_data.chuyenloai_goi("	     
			+" '"+loaiGoi+"'"
			+",'"+loaiGoi_cu+"'"
			+",'"+idGoi+"'"
			+",'"+dsmay+"'"                           
			+",'"+chunhom1+"'"  
			+",'"+chunhom2+"'" 			
			+",'"+ConvertFont.UtoD(ghiChu)+"'"
			+",'"+getUserVar("userID")+"'"		
			+",'"+getUserVar("sys_agentcode")+"'"
			+",'"+sDataSchema+"'"		
			+",'"+sIP+"'"	
			+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
	
	public String huy_goi(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String idGoi
		, String Loaigoi
		, String ghiChu
	    , String sMay_cn		
  	) throws Exception {
		
    // String s=    "begin ? := "+sFuncSchema+"pkg_regis_data.huy_goi("		
	 String s=    "begin ? := "+sFuncSchema+"pkg_api_data.huy_goi("		
		+" '"+sDataSchema+"'"                                                             
		+",'"+getUserVar("sys_agentcode")+"'" 
		+",'"+getUserVar("userID")+"'"  
		+",'"+sMay_cn+"'"
		+",'"+idGoi+"'"
		+",'"+Loaigoi+"'"
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	} 
	

	public String ghep_goi(
	    	  String sUserId
			, String sAgentCode
			, String sDataSchema
			, String FuncSchema
			, String sIP	
			, String loaiGoi
			, String idGoi_cu	
			, String idGoi_moi
			, String chunhom_cu
			, String chunhom_moi
			, String dsmay 	
			, String ghiChu 					
	  	) throws Exception {
	    // String s=    "begin ? := "+FuncSchema+"pkg_regis_data.ghep_goi("
		 String s=    "begin ? := "+FuncSchema+"pkg_api_data.ghep_goi("
			+" '"+loaiGoi+"'"
			+",'"+idGoi_cu+"'"
			+",'"+idGoi_moi+"'"
			+",'"+dsmay+"'"                           
			+",'"+chunhom_cu+"'"  
			+",'"+chunhom_moi+"'" 			
			+",'"+ConvertFont.UtoD(ghiChu)+"'"
			+",'"+getUserVar("userID")+"'"		
			+",'"+getUserVar("sys_agentcode")+"'"
			+",'"+sDataSchema+"'"		
			+",'"+sIP+"'"	
			+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
	
	public String ds_kieuth( String sCVid ) throws Exception {
	     String s=    "begin ? := admin_v2.pkg_regis_data.layds_dieukien(" +sCVid+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
	public String getSession(){
		String session = "";
		File file = new File (this.getSysConst("ConfigCompiledDir")+"pttb/laphd/chonso/session_api.neo");
		if (!file.exists() || file.lastModified()<System.currentTimeMillis()-50000){
			boolean del = true;
			if (file.exists()){
				del=file.delete();
			}			
			session = Get_API_Search.login();			
			if (del){
				try {
					FileUtils.writeStringToFile(file,session);
				} catch (IOException e) {
					session = e.getMessage();
				}
			}
		}else{
			try {
				session = FileUtils.readFileToString(file);
			} catch (IOException e) {}
		}
		return session;
	}
	
	public String get_promo(String psSoTB){
		try{
			String session = getSession();
			System.out.println("THUYVTTT---------------- session = "+session);
			StringBuffer ret = new StringBuffer();
			String jsonString = Get_API_Search.checkKM(session,psSoTB,"SPS_PRODUCT_MI_BIG70_0K1M");	
			
			//String tpl = FileUtils.readFileToString(new File(this.getSysConst("ConfigDir")+"vinacore/ajax_danhsach_lichsu_3gs.htm"));
			JSONParser jsonParser = new JSONParser();	            
			JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonString);	                           
		    System.out.println("THUYVTTT---------------- here ");       
			JSONArray jsonArray = (JSONArray) jsonObject.get("result");
			String data=""; 
			String result="";
			data += (String) jsonArray.get(5);
			data = data.substring(0,data.indexOf("|"));
			if (data.equals("49"))
				result ="1";
			else result ="0";
			System.out.println("THUYVTTT---------------- data "+result);        
			return result;
			
		} catch (Exception e) {
		    e.printStackTrace();
			return "eRRR";
		}		
	}
	
	public String put_out(String psSoTB, String timeStart, String timeEnd){
		try{
			String result="";
			if(psSoTB.indexOf("$") >=0) {
				String s[] = psSoTB.split("$");
				for (int i = 0; i < s.length; i++){
					String s1[] = s[i].split(":");
					String goi = s1[0];
					if (goi.indexOf("3163") <0 &&goi.indexOf("3164")<0&&goi.indexOf("3165") <0&&goi.indexOf("3166")<0&&goi.indexOf("3167") <0){
						String s2[] = s1[1].split(",");
						for(int n = 0; n < s2.length; n++){
							result = put_promo(s2[n],timeStart,timeEnd);
						}
					}
				}
			} else { 
				String s1[] = psSoTB.split(":");
				String goi = s1[0];
				if (goi.indexOf("3163") <0 &&goi.indexOf("3164")<0&&goi.indexOf("3165") <0&&goi.indexOf("3166")<0&&goi.indexOf("3167") <0){
					String s2[] = s1[1].split(",");
					for(int n = 0; n < s2.length; n++){
						result = put_promo(s2[n],timeStart,timeEnd);
					}
				}
			}
			return result;
		} catch (Exception e) {
		    e.printStackTrace();
			return "eRRR";
		}
	}
	
	public String  put_promo(String psSoTB, String timeStart, String timeEnd){
		try{
			String session = getSession();
			String jsonString = Get_API_Search.putKMbig70(session,psSoTB,timeStart,timeEnd);	
			JSONParser jsonParser = new JSONParser();	            
			JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonString);	                           
			String result = jsonObject.get("error_code").toString();
			System.out.println("THUYVTTT1---------------- data "+result);        
			return (result == null ? "1" : result);
		} catch (Exception e) {
		    e.printStackTrace();
			return "eRRR";
		}		
	}	
	public String tracuu_key(
		String type
		,String  keyword
		)
	{
		System.out.println("keyword="+keyword);	
		String key_ = ConvertFont.UtoD(keyword);
		//key_ = ConvertFont.W1252toUTF8(key_);
		return "/main?" + CesarCode.encode("configFile") + "=pttb/laphd/dk_goicuoc_/ajax_congvan_search" 
			+ "&" + CesarCode.encode("type") + "=" + type
			+ "&" + CesarCode.encode("keyword") + "=" +  key_ 
			;
	}

}














