package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class pkg_config_flexible extends NEOProcessEx {
	public String new_pkg_config_flexible(
		String pschema,
		String ppackage_name,
		String pdate_from,
		String pdate_to,
		String pstatus,
		String pdescription,
		String pvoice_nm,
		String pvoice_ngm,
		String pvoice_trn,
		String psms_nm,
		String psms_ngm,
		String psms_trn,
		String pdata,
		String pcycle,
		String pprice,
		String psum_price,
		String package_min,
		String package_max,
		String manipulate,
		String is_regis,
		String register_type,
		String puser_create)
		{//CesarCode.decode("mau")
		String result="";
			String s ="begin ? :=ADMIN_V2.PKG_PACKAGE_CONFIG_FLEXIBLE.new_pkg_config_flexible("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(ppackage_name)+"',"
				+"'"+pdate_from+"',"
				+"'"+pdate_to+"',"
				+"'"+pstatus+"',"
				+"'"+ConvertFont.UtoD(pdescription)+"',"
				+"'"+pvoice_nm+"',"
				+"'"+pvoice_ngm+"',"
				+"'"+pvoice_trn+"',"
				+"'"+psms_nm+"',"
				+"'"+psms_ngm+"',"
				+"'"+psms_trn+"',"
				+"'"+pdata+"',"
				+"'"+pcycle+"',"
				+"'"+ConvertFont.UtoD(pprice)+"',"
				+"'"+ConvertFont.UtoD(psum_price)+"',"
				+"'"+package_min+"',"
				+"'"+package_max+"',"
				+"'"+ConvertFont.UtoD(manipulate)+"',"
				+"'"+is_regis+"',"
				+"'"+ConvertFont.UtoD(register_type)+"',"
				+"'"+ConvertFont.UtoD(puser_create)+"'"
				+"); end;";
		System.out.println(s);
		try{		    
			result=	this.reqValue("",s);
		}catch (Exception ex){            	
		    result = ex.getMessage();
		}
        return result;
	}
	public String del_pkg_config_flexible(
		String pschema,
		String pid,
		String puser_delete) 
		{
		String result="";
			String s ="begin ? :=ADMIN_V2.PKG_PACKAGE_CONFIG_FLEXIBLE.del_pkg_config_flexible("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puser_delete+"'"
				+"); end;";
		System.out.println(s);
		try{		    
			result=	this.reqValue("",s);
		}catch (Exception ex){            	
		    result = ex.getMessage();
		}
        return result;
	}
	public String edit_pkg_config_flexible(
		String pschema,
		String pid,
		String ppackage_name,
		String pdate_from,
		String pdate_to,
		String pstatus,
		String pdescription,
		String pvoice_nm,
		String pvoice_ngm,
		String pvoice_trn,
		String psms_nm,
		String psms_ngm,
		String psms_trn,
		String pdata,
		String pcycle,
		String pprice,
		String psum_price,
		String package_min,
		String package_max,
		String manipulate,
		String is_regis,
		String register_type,
		String puser_update)
		{
		String result="";
			String s ="begin ? :=ADMIN_V2.PKG_PACKAGE_CONFIG_FLEXIBLE.edit_pkg_config_flexible("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(ppackage_name)+"',"
				+"'"+pdate_from+"',"
				+"'"+pdate_to+"',"
				+"'"+pstatus+"',"
				+"'"+ConvertFont.UtoD(pdescription)+"',"
				+"'"+pvoice_nm+"',"
				+"'"+pvoice_ngm+"',"
				+"'"+pvoice_trn+"',"
				+"'"+psms_nm+"',"
				+"'"+psms_ngm+"',"
				+"'"+psms_trn+"',"
				+"'"+pdata+"',"
				+"'"+pcycle+"',"
				+"'"+ConvertFont.UtoD(pprice)+"',"
				+"'"+ConvertFont.UtoD(psum_price)+"',"
				+"'"+package_min+"',"
				+"'"+package_max+"',"
				+"'"+ConvertFont.UtoD(manipulate)+"',"
				+"'"+is_regis+"',"
				+"'"+ConvertFont.UtoD(register_type)+"',"
				+"'"+ConvertFont.UtoD(puser_update)+"'"
				+"); end;";
		System.out.println(s);
		try{		    
			result=	this.reqValue("",s);
		}catch (Exception ex){            	
		    result = ex.getMessage();
			}
        return result;
	}
	public String get_price(
		String pschema,
		String ppackage_id) 
		{
		String result="";
			String s ="begin ? :=ADMIN_V2.PKG_PACKAGE_CONFIG_FLEXIBLE.get_price("
				+"'"+pschema+"',"
				+"'"+ppackage_id+"'"
				+"); end;";
		System.out.println(s);
		try{		    
			result=	this.reqValue("",s);
		}catch (Exception ex){            	
		    result = ex.getMessage();
		}
        return result;
	}
	public String get_pkg_data(String pkg_id,String user_id,String combo) throws Exception {
		String result="";
		String s="begin ?:= ADMIN_V2.PKG_PACKAGE_CONFIG_FLEXIBLE.get_pkg_data("	
			+"'" + pkg_id + "',"
			+"'" + user_id + "',"
			+"'" + combo + "'"
			+");"
			+"end;";
		System.out.println(+s);
		try{		    
			result=	this.reqValue("",s);
		}catch (Exception ex){            	
		    result = ex.getMessage();
		}
		return result;
	}
	public String get_pkg_coban(String pkg_id) throws Exception {
		String result="";
		String s="begin ?:= ADMIN_V2.PKG_PACKAGE_CONFIG_FLEXIBLE.get_pkg_coban("	
			+"'" + pkg_id + "'"
			+");"
			+"end;";
		System.out.println(+s);
		try{		    
			result=	this.reqValue("",s);
		}catch (Exception ex){            	
		    result = ex.getMessage();
		}
		return result;
	}
}