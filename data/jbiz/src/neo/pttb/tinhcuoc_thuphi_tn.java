package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class tinhcuoc_thuphi_tn extends NEOProcessEx {
	public String new_tinhcuoc_thuphi_tn(
		String pschema,
		String pprovince,
		String pdate_join,
		String pcost,
		String pdate_start,
		String pdate_end,
		String puser_create,
		String pnote)
		{
		String result="";
			String s ="begin ? :=ADMIN_V2.pkg_tinhcuoc_thuphi_tn.new_tinhcuoc_thuphi_tn("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(pprovince)+"',"
				+"'"+pdate_join+"',"
				+"'"+ConvertFont.UtoD(pcost)+"',"
				+"'"+pdate_start+"',"
				+"'"+pdate_end+"',"
				+"'"+puser_create+"',"
				+"'"+ConvertFont.UtoD(pnote)+"'"
				+"); end;";
		System.out.println(s);
		try{		    
			result=	this.reqValue("",s);
		}catch (Exception ex){            	
		    result = ex.getMessage();
		}
        return result;
	}
	public String del_tinhcuoc_thuphi_tn(
		String pschema,
		String pid,
		String puser_delete) 
		{
		String result="";
			String s ="begin ? :=ADMIN_V2.pkg_tinhcuoc_thuphi_tn.del_tinhcuoc_thuphi_tn("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puser_delete+"'"
				+"); end;";
		System.out.println(s);
		try{		    
			result=	this.reqValue("",s);
		}catch (Exception ex){            	
		    result = ex.getMessage();
		}
        return result;
	}
	public String edit_tinhcuoc_thuphi_tn(
	String pschema,
		String pid,
		String pprovince,
		String pdate_join,
		String pcost,
		String pdate_start,
		String pdate_end,
		String puser_update,
		String pnote)
		{
		String result="";
			String s ="begin ? :=ADMIN_V2.pkg_tinhcuoc_thuphi_tn.edit_tinhcuoc_thuphi_tn("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pprovince)+"',"
				+"'"+pdate_join+"',"
				+"'"+ConvertFont.UtoD(pcost)+"',"
				+"'"+pdate_start+"',"
				+"'"+pdate_end+"',"
				+"'"+ConvertFont.UtoD(puser_update)+"',"
				+"'"+ConvertFont.UtoD(pnote)+"'"
				+"); end;";
		System.out.println(s);
		try{		    
			result=	this.reqValue("",s);
		}catch (Exception ex){            	
		    result = ex.getMessage();
		}
        return result;
	}
}