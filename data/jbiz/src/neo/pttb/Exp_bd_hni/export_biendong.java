package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import javax.sql.RowSet;
import java.util.Date;
import java.sql.SQLException;
import com.svcon.jdbf.*;
import java.text.SimpleDateFormat;
import java.util.zip.*;
import java.io.*;
import java.util.ArrayList;

public class export_biendong
    extends NEOProcessEx {
    	
  //Khai bao cac hang so lien quan export bien dong
  private static final  int BIEN_DONG_LDM =1 ; 			 //bien dong lap dat moi
  private static final  int BIEN_DONG_TL =2 ; 			 //bien dong thanh ly
  private static final  int TONG_HOP_THANH_TOAN_LDM =3 ; //bien dong thanh toan lap dat moi
  private static final  int TONG_HOP_THANH_TOAN_DV =4 ; //bien dong thanh toan dich vu
  
  
  //Mang luu tru cau truc bang bien dong lap dat moi  
  private static final	int ICOLUMN_NUM_LDM = 22;   //Tong so column trong bang bien dong
  String[] sColumnNameLDM = {"MA_KH","SO_MAY","TEN_CQ","DC_TT","MA_BC","NGAY_HD","MA_CQ","LOAI_GIAY","SO_GIAY","NOI_CAP","NGAY_CAP","N_HET_HAN","DOITUONG","LOAI_MAY","HT_TT","MA_SO","MA_NH","SOTAIKHOAN","MA_SO_THUE","NGAY_HT","USER_HT","GHI_CHU"};
  char[] sColumnTypeLDM = {'C','C','C','C','C','D','C','C','C','C','D','D','C','C','C','C','C','C','C','D','C','C'};
  int[] iColumnLengthLDM = {20,15,120,120,10,8,8,3,20,40,8,8,4,4,2,22,9,30,14,8,40,100};
  
  
  //Mang luu tru cau truc bang bien dong thanh ly
  private static final	int ICOLUMN_NUM_TL = 7;   //Tong so column trong bang bien dong
  String[] sColumnNameTL = {"MA_SO","MA_CQ","SO_MAY","NGAY_THAO","GIO_THAO","MA_BC","sUSER"};
  char[] sColumnTypeTL = {'C','C','C','D','C','C','C'};
  int[] iColumnLengthTL = {20,20,15,8,10,10,40};
  
  //Mang luu tru cau truc tong hop bien dong thanh toan
  private static final	int ICOLUMN_NUM_THTT = 17;   //Tong so column trong bang tong hop bien dong thanh toan
  String[] sColumnNameTHTT = {"NGAY_HD","NGAY","HOPDONG_ID","SO_MAY","MA_YC","MA_LOAI_YC","TEN_KH","DIA_CHI","CHUA_VAT","THUE_VAT","CHIET_KHAU","SO_DU_TK","TIEN_KM","PHIEU_ID","NGUOI_GT","MA_USER","MA_BC"};
  char[] sColumnTypeTHTT = {'D','D','C','C','C','C','C','C','N','N','N','N','N','N','C','C','C'};
  int[] iColumnLengthTHTT = {8,8,20,15,10,10,80,120,10,10,10,10,10,10,40,40,4};
  
  //Mang luu tru cau truc tong hop bien dong thanh toan dich vu

  private static final	int ICOLUMN_NUM_DV = 22;   //Tong so column trong bang tong hop bien dong thanh toan

   String[] sColumnNameDV = {"NGAY_HD","NGAY","HOPDONG_ID","SO_MAY","MA_YC","MA_LOAI_YC","TEN_KH","DIA_CHI","CHUA_VAT","THUE_VAT","CHIET_KHAU","SO_DU_TK","TIEN_KM","PHIEU_ID","NGUOI_GT","MA_USER","MA_BC","DK_DV","H_DV","LOAI_TB","DONVIQL_ID","DICHVU"};

char[] sColumnTypeDV = {'D','D','C','C','C','C','C','C','N','N','N','N','N','N','C','C','C','C','C','C','C','C'};
  int[] iColumnLengthDV = {8,8,20,15,10,10,80,120,10,10,10,10,10,10,40,40,4,10,10,10,10,80};
  
  ArrayList alAll = new ArrayList(11);
  ArrayList alDMName = new ArrayList(11);
  
   
 
    	
  public void run() {
    System.out.println("neo.pttb.export_biendong was called");
    
  }
  
  public String getListDanhMuc(){
 	
 	 return "/main?"+CesarCode.encode("configFile")+"=PTTB/imp_exp/export/danhmuc_ajax";
 }
 
  public String getDataExport(String fromDate, String toDate, int iBienDong ,String  sListDM){
 			SimpleDateFormat sdf= new SimpleDateFormat("ddMMyyyy");
        	java.util.Date date = new java.util.Date();
        	String sDate= sdf.format(date);
        	
 			String fud = getSysConst("FileExportDir") ; 	
 			
	
			String sTemp ="";
			try{
				 sTemp=writeDbf( fromDate,  toDate,  iBienDong ,  sListDM, fud );
				 if (!isEmpty(sTemp))
				 		doZipForder(fud,sDate+".zip",sTemp.split(","));
				 	
				 sTemp= "/main?" + CesarCode.encode("configFile")+"=pttb/imp_exp/export/link_download_ajax"
				 			 + "&" + CesarCode.encode("dirDL") + "="
				 			 +"<b><a href='/ccbs/_download/pttb/"+sDate+".zip'>Click vao day de lay du lieu </a></b>" ;  	
				 
				 			
			}
			catch (Exception e){
				e.printStackTrace();							
				sTemp= "/main?" + CesarCode.encode("configFile")+"=pttb/imp_exp/export/link_download_ajax"
				 			 + "&" + CesarCode.encode("dirDL") + "="
				 			 +"<b>Co loi khi Export du lieu.</b>>" ;  	
			}
			
			return sTemp;	
} 


private String writeDbf(String fromDate, String toDate, int iBienDong ,String  sListDM, String fud  ) throws Exception
    	 {
    	String sFileList="";
    	String sReturn="";
    	
    	//Thiet lap cau hinh cac bang danh muc
    	if (setupTableStruct()==0)
    		return "";
    	
    	// Export bien dong lap dat moi
    	if (iBienDong == BIEN_DONG_LDM) {
    		sReturn = expBienDongLDM( fromDate, toDate ,fud );
    		if (!isEmpty(sReturn)) 
    			sFileList= sReturn;	    
    	}    	
    	
        // Export bien dong thanh ly
    	if (iBienDong == BIEN_DONG_TL) {
    		sReturn = expBienDongTL( fromDate, toDate ,fud );	    		
    		if (!isEmpty(sReturn)) 
    			sFileList = sReturn;	    		
    	}    
    		
    	// Export thong tin thanh toan
    	if (iBienDong == TONG_HOP_THANH_TOAN_LDM) {
    		sReturn = exThongTinTT( fromDate, toDate ,fud );	    		
    		if (!isEmpty(sReturn)) 
    			sFileList = sReturn;	    		
    	}    
    	
		// Export bien dong dich vu
		if (iBienDong == TONG_HOP_THANH_TOAN_DV) {
    		sReturn = exThongTinDV( fromDate, toDate ,fud );	    		
    		if (!isEmpty(sReturn)) 
    			sFileList = sReturn;	    		
    	} 
				
    	System.out.print("test : "+	sFileList );
    	String[] aListDM = sListDM.split(",");
    	
    	for (int i =0; i< aListDM.length; i++){
    		
    		int iTemp= alDMName.indexOf(aListDM[i]);
    		if (iTemp!=-1){
    			String sTmp =expDM(iTemp,fud);
    			if (!isEmpty(sTmp)) {
    				sFileList=sFileList + ","+ sTmp;    			
    			}   			
    		}
    			
    	}
    	
  	           
        return sFileList;
        
}
  
private String exThongTinTT(String fromDate , String toDate, String fud ) throws JDBFException,Exception, SQLException
 {
 		SimpleDateFormat sdf= new SimpleDateFormat("ddMMyyyy");
        java.util.Date date = new java.util.Date();
        String sDate= sdf.format(date);
        
 		String dirExp = fud +getUserVar("sys_agentcode").toLowerCase()+"_thanhtoan_"+sDate+".dbf";
 	 		
		DBFWriter dw= null;
		
		String sSql="begin ?:="+getSysConst("FuncSchema")+"PTTB_IMPORT_EXPORT.layds_export_dulieu_tinh("
							   +"'"+getUserVar("userID")+"',"
							   +"'"+getUserVar("sys_agentcode")+"',"
							   +"'"+getUserVar("sys_dataschema")+"',"
							   +"'"+fromDate+"',"
							   +"'"+toDate+"',3); end;";							   
    	

    	try{
    		RowSet rs= reqRSet(sSql);
	    	Object[] obj= new Object[ICOLUMN_NUM_THTT];	    	
	    	
	    	JDBField[] fields= new JDBField[ICOLUMN_NUM_THTT]; 
	    	
	    	for (int i = 0; i<ICOLUMN_NUM_THTT; i++){
	    		fields[i] = new JDBField(sColumnNameTHTT[i] ,sColumnTypeTHTT[i], iColumnLengthTHTT[i], 0 );	    		
	    	}
	    	
	        dw = new DBFWriter (dirExp, fields);
	        while (rs.next()){	        	
	  				for (int j=0;j<ICOLUMN_NUM_THTT;j++){	  					
	  					if (sColumnTypeTHTT[j]=='C')
	  						obj[j]=rs.getString(sColumnNameTHTT[j]);
						if (sColumnTypeTHTT[j]=='D')
	  						obj[j]=rs.getDate(sColumnNameTHTT[j]);	
	  					if (sColumnTypeTHTT[j]=='N')
	  						obj[j]=new Integer(rs.getInt(sColumnNameTHTT[j]));	
	  								
	  				}
	  				dw.addRecord(obj);
	  			}
	  			
	  		dw.close();
	               
        
        }
        
       
        catch(JDBFException je){
        	je.printStackTrace();
        	dw.close();
        	return "";
        }
        
        return getUserVar("sys_agentcode").toLowerCase()+"_thanhtoan_"+sDate+".dbf";
        
}
 
private String exThongTinDV(String fromDate , String toDate, String fud ) throws JDBFException,Exception, SQLException
 {
 		SimpleDateFormat sdf= new SimpleDateFormat("ddMMyyyy");
        java.util.Date date = new java.util.Date();
        String sDate= sdf.format(date);
        
 		String dirExp = fud +getUserVar("sys_agentcode").toLowerCase()+"_dichvu_"+sDate+".dbf";
 	 		
		DBFWriter dw= null;
		
		String sSql="begin ?:="+getSysConst("FuncSchema")+"PTTB_IMPORT_EXPORT.layds_export_dulieu_tinh("
							   +"'"+getUserVar("userID")+"',"
							   +"'"+getUserVar("sys_agentcode")+"',"
							   +"'"+getUserVar("sys_dataschema")+"',"
							   +"'"+fromDate+"',"
							   +"'"+toDate+"',4); end;";							   
    	

    	try{
    		RowSet rs= reqRSet(sSql);
	    	Object[] obj= new Object[ICOLUMN_NUM_DV];	    	
	    	
	    	JDBField[] fields= new JDBField[ICOLUMN_NUM_DV]; 
	    	
	    	for (int i = 0; i<ICOLUMN_NUM_DV; i++){
	    		fields[i] = new JDBField(sColumnNameDV[i] ,sColumnTypeDV[i], iColumnLengthDV[i], 0 );	    		
	    	}
	    	
	        dw = new DBFWriter (dirExp, fields);
	        while (rs.next()){	        	
	  				for (int j=0;j<ICOLUMN_NUM_DV;j++){	  					
	  					if (sColumnTypeDV[j]=='C')
	  						obj[j]=rs.getString(sColumnNameDV[j]);
						if (sColumnTypeDV[j]=='D')
	  						obj[j]=rs.getDate(sColumnNameDV[j]);	
	  					if (sColumnTypeDV[j]=='N')
	  						obj[j]=new Integer(rs.getInt(sColumnNameDV[j]));	
	  								
	  				}
	  				dw.addRecord(obj);
	  			}
	  			
	  		dw.close();
	               
        
        }
        
       
        catch(JDBFException je){
        	je.printStackTrace();
        	dw.close();
        	return "";
        }
        
        return getUserVar("sys_agentcode").toLowerCase()+"_dichvu_"+sDate+".dbf";
        
}  
 
private String expBienDongLDM(String fromDate , String toDate, String fud ) throws JDBFException,Exception, SQLException
 {
 		SimpleDateFormat sdf= new SimpleDateFormat("ddMMyyyy");
        java.util.Date date = new java.util.Date();
        String sDate= sdf.format(date);
        
 		String dirExp = fud +getUserVar("sys_agentcode").toLowerCase()+"_db_"+sDate+".dbf";
 	 		
		DBFWriter dw= null;
		
		String sSql="begin ?:="+getSysConst("FuncSchema")+"PTTB_IMPORT_EXPORT.layds_export_dulieu_tinh("
							   +"'"+getUserVar("userID")+"',"
							   +"'"+getUserVar("sys_agentcode")+"',"
							   +"'"+getUserVar("sys_dataschema")+"',"
							   +"'"+fromDate+"',"
							   +"'"+toDate+"',1); end;";							   
    	

    	try{
    		RowSet rs= reqRSet(sSql);
	    	Object[] obj= new Object[ICOLUMN_NUM_LDM];	    	
	    	
	    	JDBField[] fields= new JDBField[ICOLUMN_NUM_LDM]; 
	    	
	    	for (int i = 0; i<ICOLUMN_NUM_LDM; i++){
	    		fields[i] = new JDBField(sColumnNameLDM[i] ,sColumnTypeLDM[i], iColumnLengthLDM[i], 0 );	    		
	    	}
	    	
	        dw = new DBFWriter (dirExp, fields);
	        while (rs.next()){	        	
	  				for (int j=0;j<ICOLUMN_NUM_LDM;j++){	  					
	  					if (sColumnTypeLDM[j]=='C')
	  						obj[j]=rs.getString(sColumnNameLDM[j]);
						if (sColumnTypeLDM[j]=='D')
	  						obj[j]=rs.getDate(sColumnNameLDM[j]);			
	  								
	  				}
	  				dw.addRecord(obj);
	  			}
	  			
	  		dw.close();
	               
        
        }
        
       
        catch(JDBFException je){
        	je.printStackTrace();
        	dw.close();
        	return "";
        }
        
        return getUserVar("sys_agentcode").toLowerCase()+"_db_"+sDate+".dbf";
        
}

private String expBienDongTL(String fromDate , String toDate, String fud ) throws JDBFException,Exception, SQLException
 {
 		SimpleDateFormat sdf= new SimpleDateFormat("ddMMyyyy");
        java.util.Date date = new java.util.Date();
        String sDate= sdf.format(date);
        
 		String dirExp = fud +getUserVar("sys_agentcode").toLowerCase()+"_bd_thanhly_"+sDate+".dbf";
 		
		DBFWriter dw= null;
		
		String sSql="begin ?:="+getSysConst("FuncSchema")+"PTTB_IMPORT_EXPORT.layds_export_dulieu_tinh("
							   +"'"+getUserVar("userID")+"',"
							   +"'"+getUserVar("sys_agentcode")+"',"
							   +"'"+getUserVar("sys_dataschema")+"',"
							   +"'"+fromDate+"',"
							   +"'"+toDate+"',2); end;";							   
    	

    	try{
    		RowSet rs= reqRSet(sSql);
	    	Object[] obj= new Object[ICOLUMN_NUM_TL];	    	
	    	
	    	JDBField[] fields= new JDBField[ICOLUMN_NUM_TL]; 
	    	
	    	for (int i = 0; i<ICOLUMN_NUM_TL; i++){
	    		fields[i] = new JDBField(sColumnNameTL[i] ,sColumnTypeTL[i], iColumnLengthTL[i], 0 );	    		
	    	}
	    	
	        dw = new DBFWriter (dirExp, fields);
	        while (rs.next()){	        	
	  				for (int j=0;j<ICOLUMN_NUM_TL;j++){	  					
	  					if (sColumnTypeTL[j]=='C')
	  						obj[j]=rs.getString(sColumnNameTL[j]);
						if (sColumnTypeTL[j]=='D')
	  						obj[j]=rs.getDate(sColumnNameTL[j]);			
	  								
	  				}
	  				dw.addRecord(obj);
	  			}
	  			
	  		dw.close();
	            
        
        }
        
       
        catch(JDBFException je){
        	je.printStackTrace();
        	dw.close();
        	return "";
        }
        
                  
        return getUserVar("sys_agentcode").toLowerCase()+"_bd_thanhly_"+sDate+".dbf";
        
}

private String expDM(int iDM,  String fud ) throws JDBFException,Exception, SQLException
 {
        ArrayList alTemp =(ArrayList)alAll.get(iDM);
        
        String tableName = (String)alTemp.get(0);   //Ten bang
  		String[] sColumnNameDM = (String[])alTemp.get(1) ;   //Ten truong
  		char[] sColumnTypeDM = (char[])alTemp.get(2);		//Kieu truong 
  		int[] iColumnLengthDM = (int[])alTemp.get(3);		//Do dai truong
        int columnNum =iColumnLengthDM.length; 
        
 		String dirExp = fud +getUserVar("sys_agentcode").toLowerCase()+"_"+ alTemp.get(0)+".dbf";
 		
		DBFWriter dw= null;
		
		String sSql="begin ?:="+getSysConst("FuncSchema")+"PTTB_IMPORT_EXPORT.layds_export_danhmuc("
							   +"'"+getUserVar("userID")+"',"
							   +"'"+getUserVar("sys_agentcode")+"',"
							   +"'"+getUserVar("sys_dataschema")+"',"							   
							   +(iDM+1)+"); end;";							   
    	

    	try{
    		RowSet rs= reqRSet(sSql);
    		
	    	Object[] obj= new Object[columnNum];	    	
	    	
	    	JDBField[] fields= new JDBField[columnNum]; 
	    	
	    	for (int i = 0; i<columnNum; i++){
	    		fields[i] = new JDBField(sColumnNameDM[i] ,sColumnTypeDM[i], iColumnLengthDM[i], 0 );	    		
	    	}
	    	
	        dw = new DBFWriter (dirExp, fields);
	        while (rs.next()){	        	
	  				for (int j=0;j<columnNum;j++){	  					
	  					if (sColumnTypeDM[j]=='C')
	  						obj[j]=rs.getString(sColumnNameDM[j]);
						if (sColumnTypeDM[j]=='D')
	  						obj[j]=rs.getDate(sColumnNameDM[j]);	
	  					if (sColumnTypeDM[j]=='N')
	  						obj[j]=new Integer(rs.getString(sColumnNameDM[j]));			
	  										
	  								
	  				}
	  				
	  				dw.addRecord(obj);
	  			}
	  			
	  		dw.close();
	            
        
        }
        
       
        catch(JDBFException je){
        	je.printStackTrace();
        	dw.close();
        	return "";
        }
        
                  
        return getUserVar("sys_agentcode").toLowerCase()+"_"+ alTemp.get(0)+".dbf";
        
}
    
private int doZipForder(String sDirFoderIn, String sZipFileName,  String[] sFileList)
  {
  try
     {
     File inFolder=new File(sDirFoderIn);
     File outFolder=new File(sDirFoderIn+sZipFileName);
	 
     ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(outFolder)));
	 
     BufferedInputStream in = null;
     byte[] data    = new byte[1000];
     String files[] = sFileList ;
     for (int i=0; i<files.length; i++)
      {	 
      in = new BufferedInputStream(new FileInputStream(inFolder.getPath() + "/" + files[i]), 1000);       
	  
	 
	  out.putNextEntry(new ZipEntry(files[i])); 
      int count;
      while((count = in.read(data,0,1000)) != -1)
      {
           out.write(data, 0, count);
          }
      out.closeEntry();
      }
      out.flush();
      out.close();
      return 1;
      }
      catch(Exception e)
         {
              e.printStackTrace();
              return 0;
          } 
     }

private int setupTableStruct (){
	try{
				
		alDMName.add("chkBuuCucThu");
		alDMName.add("chkLoaiHinhTB");
		alDMName.add("chkDoiTuong");
		alDMName.add("chkNhanVienTC");
		alDMName.add("chkNganHang");
		alDMName.add("chkLoaiKhachHang");
		alDMName.add("chkLoaiGT");
		alDMName.add("chkKHLon");
		alDMName.add("chkHinhThucTT");
		alDMName.add("chkMaLYC");
		alDMName.add("chkMaYC");
	
	
		ArrayList al =null;
    	// 1.Thiet lap cau hinh bang buu cuc thu
    	al = new ArrayList(4);
	    al.add("buucucthus");
	    al.add(new String[]{"MA_BC","TENBUUCUC","DIACHI","SODIENTHOA","SOFAX","NGUOILIENH","MAQUAY","DONVIQL_ID","LOAI_BC","TEN_TAT"});
	    al.add(new char[]{'C','C','C','C','C','C','C','N','N','C'});
	    al.add(new int[]{10,200,253,20,20,200,10,5,5,200});	    
	    alAll.add(al);
	    
	    //2.Thiet lap cau hinh bang laoi hinh thue bao
    	al = new ArrayList(4);
	    al.add("loaihinh_tbs");
	    al.add(new String[]{"LOAITB_ID", "TENLOAI" , "VIETTAT" , "MALOAITB"});
	    al.add(new char[]{'N','C','C','C'});
	    al.add(new int[]{5,200,200,10});	    
	    alAll.add(al);	 
	    
	    //3.Thiet lap cau hinh bang doi tuong
    	al = new ArrayList(4);
	    al.add("doituongs");
	    al.add(new String[]{"DT_ID","TEN_DT","VAT_LD"});
	    al.add(new char[]{'N','C','C'});
	    al.add(new int[]{5,200,12});	    
	    alAll.add(al);   

	    //4.Thiet lap cau hinh bang nhan vien thu cuoc
    	al = new ArrayList(4);
	    al.add("nhanvien_tcs");
	    al.add(new String[]{"MA_NV", "TEN_NV", "DIACHI","MA_BC"});
	    al.add(new char[]{'C','C','C','C'});
	    al.add(new int[]{10,200,253,10});	    
	    alAll.add(al);   
    	
    	//5.Thiet lap cau hinh bang ngan hang
    	al = new ArrayList(4);
	    al.add("nganhangs");
	    al.add(new String[]{"TENVIETTAT","TEN","DIACHI","TAIKHOAN","SODIENTHOA","SOFAX","NGUOILIENH","NHANG_ID"});
	    al.add(new char[]{'C','C','C','C','C','C','C','N',});
	    al.add(new int[]{50,200,253,20,20,20,100,5});	    
	    alAll.add(al);   
    	
    	//6.Thiet lap cau hinh bang loai khach hang
    	al = new ArrayList(4);
	    al.add("loai_khs");
	    al.add(new String[]{"LOAIKH_ID","LOAIKH","MUCUUTIEN"});
	    al.add(new char[]{'N','C','N'});
	    al.add(new int[]{5,200,5});	    
	    alAll.add(al);   
	    
	    //7.Thiet lap cau hinh bang loai giay to
    	al = new ArrayList(4);
	    al.add("loai_gts");
	    al.add(new String[]{"LOAIGT_ID","TENLOAI"});
	    al.add(new char[]{'N','C',});
	    al.add(new int[]{5,200});	    
	    alAll.add(al);   
	    
	    //8.Thiet lap cau hinh bang loai khach hang lon
    	al = new ArrayList(4);
	    al.add("loai_khls");
	    al.add(new String[]{"KHLON_ID","TEN_LOAI"});
	    al.add(new char[]{'N','C',});
	    al.add(new int[]{5,200});	    
	    alAll.add(al);
	    
	    //9.Thiet lap cau hinh bang hinh thuc thanh toan
    	al = new ArrayList(4);
	    al.add("hinhthuc_tts");
	    al.add(new String[]{"HTTT_ID","HINHTHUC"});
	    al.add(new char[]{'N','C',});
	    al.add(new int[]{5,200});	    
	    alAll.add(al);
	    
	    //9.Thiet lap cau hinh bang ma loai yeu cau
    	al = new ArrayList(2);
	    al.add("maloai_ycs");
	    al.add(new String[]{"ID","NAME"});
	    al.add(new char[]{'C','C',});
	    al.add(new int[]{8,30});	    
	    alAll.add(al);
	    
	    //9.Thiet lap cau hinh bang loai yeu cau
    	al = new ArrayList(3);
	    al.add("loai_ycs");
	    al.add(new String[]{"ID_LOAIYC","ID","NAME"});
	    al.add(new char[]{'C','C','C'});
	    al.add(new int[]{8,8,30});	    
	    alAll.add(al);
	    
	    }
	 catch(Exception e) {
	 	e.printStackTrace();
	 	return 0;
	 }
	 return 1;
	
}

private boolean isEmpty(String str)
{
	return (str == null) || (str.length()==0)?true:false;
}
}
 

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
