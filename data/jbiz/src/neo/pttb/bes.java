package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class bes extends NEOProcessEx {
  public void run() {
    System.out.println("neo.khuyen_mai was called");
  }

 public String getLst_RegHis( String matb)
	{
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=PTTB/dkygoi/bes/ajax_bes&" + CesarCode.encode(" matb") + "=" + matb;
		return url_;
	}		
  
  public String chkProReg(String funcschema, 
									    String dataschema,										
										String agentcode,  
 										String userid,
										String matb,
										String type
										)
  	{
  			
    String s=    "begin ?:= "+funcschema+"pkg_goi.chk_pro_reg('"
		+ dataschema
		+"','"+agentcode
		+"','"+getUserVar("userID")
        +"','"+matb
		+"','"+type+"'"
        +");  end;"
        ;        
    System.out.println(s); 		
	
  	return s;  
  }
  
   public String regVip(   String funcschema, 
									    String dataschema,
									    String agentcode,  											
 										String userid,
										String promotion,
										String matb,
										String description)
  	{
  			
    //String s=    "begin ?:= "+funcschema+"pkg_goi.reg_package_bes('"
	String s=    "begin ?:= "+funcschema+"pkg_api_data.reg_package_bes('"
		+ dataschema		
		+"','"+agentcode
		+"','"+getUserVar("userID")
        +"','"+promotion
		+"','"+matb
		+"','"+description+"'"
        +");"
        +" end;"
        ;        
    System.out.println(s); 		
	 return s;  
  }
  
    public String canVip(String funcschema, 
									    String dataschema,
										String agentcode, 
 										String userid,
										String promotion,
										String matb,
										String description)
  	{
  			
    //String s=    "begin ?:= "+funcschema+"pkg_goi.can_package_bes('"
	String s=    "begin ?:= "+funcschema+"pkg_api_data.can_package_bes('"
		+ dataschema
		+"','"+agentcode
		+"','"+getUserVar("userID")
        +"','"+promotion
		+"','"+matb
		+"','"+description+"'"
        +");"
        +" end;"
        ;        
    System.out.println(s); 		
	
  	return s;  
  }
 
}

