package neo.pttb;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import cosaldap.Base64.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;


public class dk_goi_tichhop_tratruoc
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb.dk_goi_tichhop was called");
  }   
	public String them_moi_dd(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String dsmay 				
		, String chunhom
		, String ghiChu 		
		, String sIP		
  	) throws Exception {

     String s=    "begin ? := "+FuncSchema+"pkg_regis_data.them_moi_dd("	
		+" '"+loaiGoi+"'"
		+",'"+dsmay+"'"                           
		+",'"+chunhom+"'"  
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}	
	
public String cap_nhat_dd(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String sIP	
		, String loaiGoi
		, String idGoi			
		, String chunhom
		, String dsmay1
		, String dsmay2
		, String dsmay3		
		, String ghiChu 	
  	) throws Exception {

     String s=    "begin ? := "+FuncSchema+"pkg_qltb.cap_nhat_dd("	
		+" '"+loaiGoi+"'"
		+",'"+idGoi+"'"
		+",'"+dsmay1+"'"                           
		+",'"+dsmay2+"'"
		+",'"+dsmay3+"'"
		+",'"+chunhom+"'"	
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"
		+",'"+sIP+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"							
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}

public String huy_goi_dd(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String idGoi
		, String Loaigoi
		, String ghiChu
	    , String sMay_cn		
  	) throws Exception {
		
     String s=    "begin ? := "+sFuncSchema+"pkg_regis_data.huy_goi_dd("		
		+" '"+sDataSchema+"'"                                                             
		+",'"+getUserVar("sys_agentcode")+"'" 
		+",'"+getUserVar("userID")+"'"  
		+",'"+sMay_cn+"'"
		+",'"+idGoi+"'"
		+",'"+Loaigoi+"'"
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}	
     public String them_moi(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String dsmay 				
		, String chunhom
		, String ngay_dk
		, String ghiChu 		
		, String sIP		
  	) throws Exception {
    // String s=    "begin ? := "+FuncSchema+"pkg_regis_data.them_moi("	
	 String s=    "begin ? := "+FuncSchema+"pkg_regis_data_intergrate.them_moi("	
		+" '"+loaiGoi+"'"
		+",'"+dsmay+"'"                           
		+",'"+chunhom+"'"      
		+",'"+ngay_dk+"'"    		
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	
	
	public String cap_nhat(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String sIP	
		, String loaiGoi
		, String idGoi			
		, String chunhom
		, String ngay_dk
		, String dsmay 	
		, String ghiChu 
		, String kieu
		, String loaigoitach		
  	) throws Exception {

    // String s=    "begin ? := "+FuncSchema+"pkg_regis_data.cap_nhat("	
	 String s=    "begin ? := "+FuncSchema+"pkg_api_data.cap_nhat("	
		+" '"+loaiGoi+"'"
		+",'"+idGoi+"'"
		+",'"+dsmay+"'"                           
		+",'"+chunhom+"'"  
		+",'"+ngay_dk+"'"   
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+kieu+"'"
		+",'"+loaigoitach+"'"
		+",'"+getUserVar("userID")+"'"		
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+sDataSchema+"'"		
		+",'"+sIP+"'"			
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	public String chuyenloai_goi(
	    	  String sUserId
			, String sAgentCode
			, String sDataSchema
			, String FuncSchema
			, String sIP	
			, String loaiGoi
			, String loaiGoi_cu
			, String idGoi			
			, String chunhom1
			, String chunhom2
			, String dsmay 	
			, String ghiChu 
	  	) throws Exception {
		String s=    "begin ? := "+FuncSchema+"pkg_regis_data.chuyenloai_goi("	     
			+" '"+loaiGoi+"'"
			+",'"+loaiGoi_cu+"'"
			+",'"+idGoi+"'"
			+",'"+dsmay+"'"                           
			+",'"+chunhom1+"'"  
			+",'"+chunhom2+"'" 			
			+",'"+ConvertFont.UtoD(ghiChu)+"'"
			+",'"+getUserVar("userID")+"'"		
			+",'"+getUserVar("sys_agentcode")+"'"
			+",'"+sDataSchema+"'"		
			+",'"+sIP+"'"	
			+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
	
	public String huy_goi(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String idGoi
		, String Loaigoi
		, String ghiChu
	    , String sMay_cn		
  	) throws Exception {
		
    // String s=    "begin ? := "+sFuncSchema+"pkg_regis_data.huy_goi("		
	 String s=    "begin ? := "+sFuncSchema+"pkg_regis_data_intergrate.huy_goi("		
		+" '"+sDataSchema+"'"                                                             
		+",'"+getUserVar("sys_agentcode")+"'" 
		+",'"+getUserVar("userID")+"'"  
		+",'"+sMay_cn+"'"
		+",'"+idGoi+"'"
		+",'"+Loaigoi+"'"
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	} 
	

	public String ghep_goi(
	    	  String sUserId
			, String sAgentCode
			, String sDataSchema
			, String FuncSchema
			, String sIP	
			, String loaiGoi
			, String idGoi_cu	
			, String idGoi_moi
			, String chunhom_cu
			, String chunhom_moi
			, String dsmay 	
			, String ghiChu 					
	  	) throws Exception {
	    // String s=    "begin ? := "+FuncSchema+"pkg_regis_data.ghep_goi("
		 String s=    "begin ? := "+FuncSchema+"pkg_api_data.ghep_goi("
			+" '"+loaiGoi+"'"
			+",'"+idGoi_cu+"'"
			+",'"+idGoi_moi+"'"
			+",'"+dsmay+"'"                           
			+",'"+chunhom_cu+"'"  
			+",'"+chunhom_moi+"'" 			
			+",'"+ConvertFont.UtoD(ghiChu)+"'"
			+",'"+getUserVar("userID")+"'"		
			+",'"+getUserVar("sys_agentcode")+"'"
			+",'"+sDataSchema+"'"		
			+",'"+sIP+"'"	
			+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
	
	public String ds_kieuth( String sCVid ) throws Exception {
	     String s=    "begin ? := admin_v2.pkg_regis_data_inter_prepaid.layds_dieukien(" +sCVid+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
		
	public String upload_business
	   (
	     String upload_id,
		 String type_id,
		 String dispatch_id,
		 String package_id,
		 String userid, 
		 String userip
	   )
	   {
	    
		String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_regis_data_inter_prepaid.upload_business("         
                + "'" + upload_id + "'"
				+ ",'" + type_id + "'"
                + ",'" + dispatch_id + "'"
				+ ",'" + package_id + "'"
                + ",'" + userid + "'"
                + ",'" + userip + "'"    
				+ ",'" + getUserVar("sys_agentcode") + "'"               
                + ",'" + getUserVar("sys_dataschema") + "'"   				
                + ");"
                + "end;";
	System.out.println("ThuyVTT2:"+s);		
       try
			{		    
				 result= this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	   }
	   
	//Hoecn 20170714 ham dang ky goi x   
	public String them_goi_phu
	   (
		 String dispatch_id,
		 String package_id,
		 String package_x_id,
		 String chunhom,
		 String ghichu,
		 String userid, 
		 String userip
	   )
	   {
		String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_regis_data_inter_prepaid.them_goi_phu("
                + "'" + dispatch_id + "'"
				+ ",'" + package_id + "'"
				+ ",'" + package_x_id + "'"
				+ ",'" + chunhom + "'"
				+ ",'" + ghichu + "'"
                + ",'" + userid + "'" 
				+ ",'" + getUserVar("sys_agentcode") + "'"
                + ",'" + getUserVar("sys_dataschema") + "'"
                + ",'" + userip + "'"  				
                + ");"
                + "end;";
	System.out.println("Hoecn:"+s);		
       try
			{		    
				 result= this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	   }
}














