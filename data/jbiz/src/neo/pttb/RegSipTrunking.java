package neo.pttb;
import neo.smartui.common.*;
import neo.smartui.process.*;


public class RegSipTrunking extends NEOProcessEx {

 public String add_stk(String pc_sotb, String pc_loaikh, String pc_tenkh, String pc_daidien, String pc_sdt_lienhe, String pc_diachi, String pc_masothue,
            String pc_giaydkkd, String pc_nganhang, String pc_taikhoan, String pc_socf, String pc_ghichu, String pc_loai, String pc_userip, String pc_nguoi_gt) throws Exception 
	{     
		String s = "begin ? := admin_v2.pk_subscriber_stk.sf_regis_stk("                            
                + "'" + pc_sotb + "'"
                + ",'" + pc_loaikh + "'"
                + ",'" + ConvertFont.UtoD(pc_tenkh) + "'"
                + ",'" + ConvertFont.UtoD(pc_daidien) + "'"
                + ",'" + pc_sdt_lienhe + "'"
                + ",'" + ConvertFont.UtoD(pc_diachi) + "'"
                + ",'" + pc_masothue + "'"
				+ ",'" + ConvertFont.UtoD(pc_giaydkkd) + "'"
                + ",'" + pc_nganhang + "'"
                + ",'" + pc_taikhoan + "'"
                + ",'" + pc_socf + "'"
                + ",'" + ConvertFont.UtoD(pc_ghichu) + "'"
                + ",'" + pc_loai + "'"              
				+ ",'" + getUserVar("userID") + "'"
				+ ",'" + pc_userip + "'"				
                + ",'" +getUserVar("sys_agentcode")+"'"
				+ ",'" + pc_nguoi_gt + "'"				
                + ");"
                + "end;";

        return this.reqValue("", s);
    } 

	public String del_stk(String pc_sotb, String pc_userip) throws Exception{           
			
        String result = "";
        String s = "begin ? := admin_v2.pk_subscriber_stk.sf_delete_stk("
                + "'" + pc_sotb + "'"	
				+ ",'" + getUserVar("userID") + "'"
                + ",'" + pc_userip + "'"
				+ ",'" + getUserVar("sys_agentcode")+"'"   
                + ");"
                + "end;";
        
		return this.reqValue("", s);
    } 

}
