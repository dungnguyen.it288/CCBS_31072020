package neo.pttb;
import neo.smartui.common.*;
import neo.smartui.process.*;


public class RegMeg extends NEOProcessEx {
      public String add_meg( String MSISDN, String registype, String tradetype,
            String psPkgExt, String licenseno, String email, String DATEOFISSUE,
            String ISSUEDBY, String FAX, String website, String REPRESENTATIVE,
            String PHONE_CONTACT, String billingname, String CUSTNAME, String CUSTADDRESS,
            String BILLINGADDESS, String PLACE_PAYMENT, String bank_transfer,
            String TAXCODE, String ACCOUNTCODE, String BANKID, String custtype, 
            String DESCRIPTION, String CUSTMANAGER, String admintel, String adminemail,
			String psServices, String psPosition, String psAdminPosition,
			String userid, String Userip, String psloai) throws Exception {          
			

        //String s = "begin ? := admin_v2.pk_subscriber_meg.sf_add_meg("
		String s = "begin ? := admin_v2.pk_subscriber_meg_sps.sf_add_meg("
                + "'" + getUserVar("userID") + "'"
                + ",'" + Userip + "'"
                + ",'"+getUserVar("sys_agentcode")+"'"                
                + ",'" + MSISDN + "'"
                + ",'" + registype + "'"
                + ",'" + ConvertFont.UtoD(tradetype) + "'"
                + ",'" + psPkgExt + "'"
                + ",'" + licenseno + "'"
                + ",'" + email + "'"
                + ",'" + DATEOFISSUE + "'"
				+ ",'" + ConvertFont.UtoD(ISSUEDBY) + "'"
                + ",'" + FAX + "'"
                + ",'" + website + "'"
                + ",'" + ConvertFont.UtoD(REPRESENTATIVE) + "'"
                + ",'" + PHONE_CONTACT + "'"
                + ",'" + ConvertFont.UtoD(billingname) + "'"
                + ",'" + ConvertFont.UtoD(CUSTNAME) + "'"
                + ",'" + ConvertFont.UtoD(CUSTADDRESS) + "'"
                + ",'" + ConvertFont.UtoD(BILLINGADDESS) + "'"
                + ",'" + PLACE_PAYMENT + "'"
                + ",'" + bank_transfer + "'"
                + ",'" + TAXCODE + "'"
                + ",'" + ACCOUNTCODE + "'"
                + ",'" + BANKID + "'"
                + ",'" + custtype + "'"
                + ",'" + ConvertFont.UtoD(DESCRIPTION) + "'"
                + ",'" + ConvertFont.UtoD(CUSTMANAGER) + "'"
                + ",'" + admintel + "'"
                + ",'" + adminemail + "'"
				+ ",'" + psServices + "'"
				+ ",'" + ConvertFont.UtoD(psPosition) + "'"
				+ ",'" + ConvertFont.UtoD(psAdminPosition) + "'"
				+ ",'" + psloai + "'"
                + ");"
                + "end;";

        return this.reqValue("", s);
    } 

	public String edit_meg(String MSISDN, String custid, String registype, String tradetype,
            String psPkgExt, String licenseno, String email, String DATEOFISSUE,
            String ISSUEDBY, String FAX, String website, String REPRESENTATIVE,
            String PHONE_CONTACT, String billingname, String CUSTNAME, String CUSTADDRESS,
            String BILLINGADDESS, String PLACE_PAYMENT, String bank_transfer,
            String TAXCODE, String ACCOUNTCODE, String BANKID, String custype, 
            String DESCRIPTION, String CUSTMANAGER, String admintel, String adminemail,
			String psServices, String psPosition, String psAdminPosition,
			String psuserid,String psuserip,String psLoai) throws Exception {        
			
        String s = "begin ? := admin_v2.pk_subscriber_meg_sps.sf_edit_meg("
                + "'" + getUserVar("userID") + "'"
                + ",'"+ psuserip + "'"
                + ",'"+getUserVar("sys_agentcode")+"'"              
                + ",'" + MSISDN + "'"
				+ ",'" + custid + "'"
                + ",'" + registype + "'"
                + ",'" + ConvertFont.UtoD(tradetype) + "'"
                + ",'" + psPkgExt + "'"
                + ",'" + licenseno + "'"
                + ",'" + email + "'"
                + ",'" + DATEOFISSUE + "'"
				+ ",'" + ConvertFont.UtoD(ISSUEDBY) + "'"
                + ",'" + FAX + "'"
                + ",'" + website + "'"
                + ",'" + ConvertFont.UtoD(REPRESENTATIVE) + "'"
                + ",'" + PHONE_CONTACT + "'"
                + ",'" + ConvertFont.UtoD(billingname) + "'"
                + ",'" + ConvertFont.UtoD(CUSTNAME) + "'"
                + ",'" + ConvertFont.UtoD(CUSTADDRESS) + "'"
                + ",'" + ConvertFont.UtoD(BILLINGADDESS) + "'"
                + ",'" + PLACE_PAYMENT + "'"
                + ",'" + bank_transfer + "'"
                + ",'" + TAXCODE + "'"
                + ",'" + ACCOUNTCODE + "'"
                + ",'" + BANKID + "'"
                + ",'" + custype + "'"
                + ",'" + ConvertFont.UtoD(DESCRIPTION) + "'"
                + ",'" + ConvertFont.UtoD(CUSTMANAGER) + "'"
                + ",'" + admintel + "'"
                + ",'" + adminemail + "'"
				+ ",'" + psServices + "'"
				+ ",'" + ConvertFont.UtoD(psPosition) + "'"
				+ ",'" + ConvertFont.UtoD(psAdminPosition) + "'"
				+ ",'" + psLoai + "'"				
                + ");"
                + "end;";
				
        return this.reqValue("", s);
    } 
		
	public String del_meg(String MSISDN) throws Exception{           
			
        String result = "";
        String s = "begin ? := admin_v2.pk_subscriber_meg_sps.deleteMeg("
                + "'" + getUserVar("userID") + "'"
                + ",'" + getUserVar("userIP") + "'"
                + ",'ccs_common.'"                
                + ",'" + MSISDN + "'"				
                + ");"
                + "end;";
        
		return this.reqValue("", s);
    } 

	public String edit_submeg( String extension, String sub_msisdn, String package_type,
            String users, String parts, String position,String hddid,String psuserid, String psuserip) throws Exception{
           
        String s = "begin ? := admin_v2.pk_subscriber_meg_sps.sf_edit_submeg("
                + "'" + getUserVar("userID") + "'"
                + ",'" + psuserip + "'"
                + ",'ccs_common.'"                   
                + ",'" + extension + "'"
                + ",'" + sub_msisdn + "'"
                + ",'" + package_type + "'"
                + ",'" + ConvertFont.UtoD(users) + "'"
                + ",'" + ConvertFont.UtoD(parts) + "'"
                + ",'" + ConvertFont.UtoD(position) + "'"
				+ ",'" + hddid + "'"
                + ");"
                + " end;";
        return this.reqValue("", s);
    }
	public String add_submeg(String extension, String sub_msisdn, String package_type,
            String users, String parts, String position,String psid,String psuserid, String psuserip) throws Exception {

        String s = "begin ? := admin_v2.pk_subscriber_meg_sps.sf_add_submeg("
                + "'" + psid + "'"
				+ ",'" + getUserVar("userID") + "'"
                + ",'" + psuserip + "'" 
				+ ",'ccs_common.'"
                + ",'" + extension + "'"
                + ",'" + sub_msisdn + "'"
                + ",'" + package_type + "'"
                + ",'" + ConvertFont.UtoD(users) + "'"
                + ",'" + ConvertFont.UtoD(parts) + "'"
                + ",'" + ConvertFont.UtoD(position) + "'"
                + ");"
                + " end;";
        return this.reqValue("", s);
    }
	public String active_subagent(String pstype,String psid,String psuserid, String psuserip) throws Exception {

        String s = "begin ? := admin_v2.pk_subscriber_meg_sps.deleteSubMeg("
                + "'" + pstype + "'"
				+ ",'" + psid + "'"
				+ ",'ccs_common.'"  
				+ ",'" + getUserVar("userID") + "'"
                + ",'" + psuserip + "'" 				            
                + ");"
                + " end;";
        return this.reqValue("", s);
    }
	public String active_agent(String pstype,String psid,String psuserid, String psuserip) throws Exception {

        String s = "begin ? := admin_v2.pk_subscriber_meg_sps.sf_set_active_meg("
                + "'" + pstype + "'"
				+ ",'" + psid + "'"
				+ ",'ccs_common.'"  
				+ ",'" + getUserVar("userID") + "'"
                + ",'" + psuserip + "'" 				            
                + ");"
                + " end;";
        return this.reqValue("", s);
    }
	
}
