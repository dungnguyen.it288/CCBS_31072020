package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class giao_may
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.giao_may was called");
  }
  
    public  String inphieu_bangiao (String psloai_hd,String psma_hd)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=PTTB/giao_may/jphieuthu" 										
					+"&"+CesarCode.encode("chukyno")+"="+psloai_hd
					+"&"+CesarCode.encode("phieu_id")+"="+psma_hd;
   			}
  
  
    public  String bangiao_sanpham (String psFuncSchema, String psUserId, String psUserIP, String psschema , String psagentcode
									 ,String psloai_hd, String psma_hd)
   		{
   			//String s="begin ?:= admin_v2.pttb_hmm.bangiao_sanpham("
			String s="begin ?:= admin_v2.pkg_api_interface.bangiao_sanpham("
						+"'"+getUserVar("userID") 		+"',"
						+"'"+psUserIP 		+"',"
						+"'"+psschema		+"',"
						+"'"+psagentcode	+"',"		
						+"'"+psloai_hd 		+"',"
						+"'"+psma_hd		+"'"	
						+") ; end;" ;
						
			return s;
   			}
  
   public  String capnhat_sanpham_id (String psFuncSchema, String psUserId, String psUserIP , String psschema , String psagentcode
									 ,String psloai_hd, String psma_hd, String pssomay, String psImei , String psProductID)
   		{
   			String s="begin ?:= "+psFuncSchema+"PTTB_VSMS.capnhat_sanpham_id("
						+"'"+getUserVar("userID") 		+"',"
						+"'"+psUserIP 		+"',"
						
						+"'"+psschema		+"',"
						+"'"+psagentcode	+"',"		
						+"'"+psloai_hd 		+"',"
						
						+"'"+psma_hd		+"',"	
						+"'"+pssomay		+"',"	
						
						+"'"+psImei 		+"',"
						+"'"+psProductID	+"'"
						+") ; end;" ;
			System.out.println(s);
			System.out.println(s);
			System.out.println(s);
			System.out.println(s);
			System.out.println(s);
			
			return s;
   			}
 
  public  String layds_linhkien (String psFuncSchema,String psUserId , String psMaTinhNgDung, String  psschema,String psloai_hd,String psma_hd)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=PTTB/giao_may/giao_may_ajax_dslk" 
					+"&"+CesarCode.encode("loaihd")+"="+psloai_hd
					+"&"+CesarCode.encode("ma_hd")+"="+ psma_hd;
   			}
 
 public  String laytt_hopdong (String psFuncSchema,String psUserId , String psMaTinhNgDung, String  psschema,String psloai_hd,String psma_hd, String psLookupBy)
   		{
   			String s="begin ?:= "+psFuncSchema+"pttb_vsms.laytt_hd_datt("
						+"'"+getUserVar("userID") +"',"
						+"'"+psMaTinhNgDung+"',"
						+"'"+psschema	+"',"
						+"'"+psloai_hd+"',"
						+"'"+psma_hd+"',"
						+"'"+psLookupBy+"'"
					+") ; end;" ;
			return s;
   			}
   			
 
  
}
