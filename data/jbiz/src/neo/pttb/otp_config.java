package neo.pttb;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import cosaldap.Base64.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;
import java.io.*;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.ResultSetMetaData;
import javax.sql.RowSet;

public class otp_config extends NEOProcessEx {

    public void run() {
        System.out.println("neo.goi_khuyenmai was called");
    }

    public String add(String userid,
            String trangthai, String userupdate
    )
            throws Exception {
		
        String xs_ = "";
        xs_ = xs_ + " begin ";
        xs_ = xs_ + " 	?:= admin_v2.otp_config.add_otp_config('" + ConvertFont.UtoD(userid) + "'," + trangthai + ",'"+ userupdate + "'); ";
        xs_ = xs_ + " end; ";

        System.out.println(xs_);
        return this.reqValue("", xs_);
    }

    public String edit(String userid,
            String trangthai, String userupdate
    )
            throws Exception {
        String xs_ = "";
        xs_ = xs_ + " begin ";
        xs_ = xs_ + " 	?:= admin_v2.otp_config.edit_otp_config('" + ConvertFont.UtoD(userid) + "','"+ trangthai + "','"+ userupdate + "'); ";
        xs_ = xs_ + " end; ";

        System.out.println(xs_);
        return this.reqValue("", xs_);
    }
	
	public String del(String userid,
            String trangthai, String userupdate)
            throws Exception {
        String xs_ = "";
        xs_ = xs_ + " begin ";
        xs_ = xs_ + " 	?:= admin_v2.otp_config.del_otp_config('" + ConvertFont.UtoD(userid) + "','"+ trangthai + "','"+ userupdate + "'); ";
        xs_ = xs_ + " end; ";

        System.out.println(xs_);
        return this.reqValue("", xs_);
    }

}
