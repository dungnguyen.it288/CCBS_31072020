
package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;



public class khuyen_mai extends NEOProcessEx {
  public void run() {
    System.out.println("neo.khuyen_mai was called");
  }

 public String getLstProRegHis( String matb)
	{
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=PTTB/promotion/subrecovery/ajax_subrecovery&" + CesarCode.encode(" matb") + "=" + matb;
		return url_;
	}		
  
  public String chkProReg(   String funcschema, 
									    String dataschema,  
										String agentcode,  
 										String userid,
										String matb)
  	{
  			
    //String s=    "begin ?:= admin_v2.pkg_goi.chk_pro_reg('"
	 String s=    "begin ?:= admin_v2.pkg_api_data.chk_pro_reg('"
		+ dataschema
		+"','"+agentcode
		+"','"+userid
        +"','"+matb+"'"
        +");  end;"
        ;        
    System.out.println(s); 		
	
  	return s;  
  }
  
   public String regPro(   String funcschema, 
									    String dataschema,  	
									    String agentcode,  											
 										String userid,
										String promotion,
										String matb,
										String description)
  	{
  			
    String s=    "begin ?:= admin_v2.pkg_goi.reg_pro('"
		+ dataschema		
		+"','"+agentcode
		+"','"+userid
        +"','"+promotion
		+"','"+matb
		+"','"+description+"'"
        +");"
        +" end;"
        ;        
    System.out.println(s); 		
	 return s;  
  }
  
    public String canPro(String funcschema, 
									    String dataschema,  										
										String agentcode, 
 										String userid,
										String promotion,
										String matb,
										String description)
  	{
  			
    String s=    "begin ?:= "+CesarCode.decode(funcschema)+"pttb_km.can_pro('"
		+ dataschema
		+"','"+agentcode
		+"','"+userid
        +"','"+promotion
		+"','"+matb
		+"','"+description+"'"
        +");"
        +" end;"
        ;        
    System.out.println(s); 		
	
  	return s;  
  }
   
  
  
 public NEOCommonData xoa_tbkm_dacbiet( String psschema, 
 										String psschemaCommon, 	 
 										String psma_tb
										,String pikmht_id)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschema)+"pttb_km.xoa_tbkm_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_dataschema")+"'"
        +",'"+psma_tb+"'"
        +","+pikmht_id      
        +");"
        +"end;"
        ;        
    System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	// 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);  
  }
  public String rec_tonghop(        String funcschema,
 										 String chukyno,
 										 String lantra) 
 										{
 String a= "select "+
                         "count(*)-count(invalid) hople,count(invalid) khonghople,"+
                         "count(decode(invalid,1,invalid,null)) c1,"+
                         "count(decode(invalid,2,invalid,null)) c2,"+
                         "count(decode(invalid,3,invalid,null)) c3,"+
                         "count(decode(invalid,4,invalid,null)) c4,"+
                         "count(decode(invalid,5,invalid,null)) c5,"+
                         "count(decode(invalid,6,invalid,null)) c6,"+
                         "count(decode(invalid,7,invalid,null)) c7,"+
                         "count(decode(invalid,8,invalid,null)) c8 "+
                         " from "+ getUserVar("sys_dataschema")+"UPLOAD_FILE_DK_"+chukyno+" where lantra_id="+lantra;

               return a;
  }
	
   public String value_dangkyfile(
		String psKyhoadon,
		String psLantra		
	)
	{ 
		return "begin ? := ccs_admin.pttb_km.dk_goikm_file("
			+"'"+psKyhoadon+"',"
			+"'"+psLantra+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";	
	}
  public String doc_hople(
   			String dataschema, 
			String chukyno, 
			String lantra)
  	{ 
	 	String url_="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=PTTB/laphd/khuyenmai_goi/dk_theofile/ajax_hople";	
		url_ = url_ + "&"+CesarCode.encode("chukyno")+"="+chukyno;
		url_ = url_ + "&"+CesarCode.encode("lantra")+"="+lantra;	    
	  return url_;
  	}
  	 public String doc_khonghople(
   			String dataschema, 
			String chukyno, 
			String lantra)
  	{ 
	String url_="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+
		"=PTTB/laphd/khuyenmai_goi/dk_theofile/ajax_khonghople";	
		url_ = url_ + "&"+CesarCode.encode("chukyno")+"="+chukyno;
		url_ = url_ + "&"+CesarCode.encode("lantra")+"="+lantra;	    
	  return url_;		  
  	}
 	
	public NEOCommonData capnhat_tbkm_dacbiet(
										 String psschema,
  										 String psschemaCommon,   
  										 String psma_tb
										,String pikmht_id
										,String pikmcv_id
										,String pskieuld_id										
										,String psghichu
										,String pdngay_ld									
										,String pdngay_in_db
										,String psnguoi_cn
										,String psmay_cn
									)
  	{
  			
    String s=    "begin ? := "+ CesarCode.decode(psschema) +"pttb_km.capnhat_tbkm_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
         +",'"+psma_tb+"'"
        +","+pikmht_id
		+","+pikmcv_id
        +","+pskieuld_id
        +","+psghichu	
		+","+pdngay_ld
	    +","+pdngay_in_db
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;";
		
    		System.out.println(s); 		
			NEOExecInfo nei_ = new NEOExecInfo(s);
	 		//nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
  }   
 
	
 public NEOCommonData themmoi_tbkm_dacbiet(	 String psschema,
  										 String psschemaCommon,   
  										 String psma_tb
										,String pikmht_id
										,String pikmcv_id
										,String pskieuld_id										
										,String psghichu
										,String pdngay_ld									
										,String pdngay_in_db
										,String psnguoi_cn
										,String psmay_cn
									
									)
  	{
  			
    String s=    "begin ? := admin_v2.pkg_goi.themmoi_tbkm_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
        +",'"+psma_tb+"'"
        +","+pikmht_id
		+","+pikmcv_id		
        +","+pskieuld_id
        +","+psghichu	
		+","+pdngay_ld
	    +","+pdngay_in_db
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
     System.out.println(s);
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 //	nei_.setDataSrc("VNPBilling");
	return new NEOCommonData(nei_);  
  	}

public NEOCommonData laytt_tb_dacbiet ( String func_schema,
 										 String schemaCommon,
 										 String ma_tb,
 										 String khuyenmai_id) 
 										{
   String s= "begin ?:="+func_schema+"pttb_km.laytt_tb_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"','"+ma_tb+"'"
		+","+khuyenmai_id
		+");end;";
	
	NEOExecInfo nei_ = new NEOExecInfo(s);
	//nei_.setDataSrc("VNPBilling");
	return new NEOCommonData(nei_);  
  }
public NEOCommonData laytt_kh_dacbiet ( String func_schema,
 										 String schemaCommon,
 										 String ma_tb,
 										 String khuyenmai_id) 
 										{
   String s= "begin ?:="+func_schema+"pttb_km.laytt_kh_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"','"+ma_tb+"'"
		+","+khuyenmai_id
		+");end;";
	
	NEOExecInfo nei_ = new NEOExecInfo(s);
	//nei_.setDataSrc("VNPBilling");
	return new NEOCommonData(nei_);  
  }
  
		
	public NEOCommonData ds_htkm_tbdbs(int pikmcv_id)
	{
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=PTTB/laphd/khuyenmai_goi/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" +pikmcv_id;
		NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
	}	


//-------------------------------------------------------------------------
  
//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData laytt_kmid_congvans(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmcv_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_congvans('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmcv_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }



//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData laytt_kmid_dk_cktms(
 										 String func_schema,
 										 String schemaCommon,
 										 String km_dk_cktm_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_dk_cktms('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+km_dk_cktm_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }






//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData laytt_kmid_cktms(
 										 String func_schema,
 										 String schemaCommon,
 										 String km_cktm_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_cktms('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+km_cktm_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }
  
  
  


//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData laytt_kmid_cktts(
 										 String func_schema,
 										 String schemaCommon,
 										 String km_cktt_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_cktts('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+km_cktt_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }
  
  
  
  
  

//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData laytt_kmid_dk_cktts(
 										 String func_schema,
 										 String schemaCommon,
 										 String km_dk_cktt_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_dk_cktts('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+km_dk_cktt_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }




  
//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData layds_km_cachgiam_id(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmcg_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.layds_km_cachgiam_id('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmcg_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }



  
//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData layds_km_cachgiam_area_id(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmcg_area_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.layds_km_cachgiam_area_id('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmcg_area_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }




//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData laytt_kmid_dieukiens(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmdk_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_dieukiens('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmdk_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }



//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData laytt_kmid_hinhthucs(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmht_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_hinhthucs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmht_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }


public NEOCommonData layds_km_hinhthucs( String kmcv_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_congvans/km_hinhthucs_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmcv_id")+"="+kmcv_id+"&sid="+Math.random();
		
		System.out.println(url_); 	    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  
  
  

    
   public NEOCommonData layds_km_congvans( String radDangAD)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_congvans/km_congvans_ajax";	
		url_ = url_ + "&"+CesarCode.encode("ds_congvans")+"="+radDangAD+"&sid="+Math.random();
		
		System.out.println(url_); 	    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  
    public NEOCommonData layds_km_area_ids( String radDangAD)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_area_codes/km_area_ids_ajax";	
		url_ = url_ + "&"+CesarCode.encode("ds_area_id")+"="+radDangAD+"&sid="+Math.random();
		
		System.out.println(url_); 	    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  
  	
  	
   public NEOCommonData layds_kmid_congvans(
   			String pikmcv_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_congvans/km_hinhthucs_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmcv_id")+"="+pikmcv_id+"&sid="+Math.random();
			    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  	
  	
  	
   public NEOCommonData layds_kmid_hinhthucs(
   			String pikmht_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_cachgiams/km_cachgiams_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmht_id+"&sid="+Math.random();
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  	
  	
  	
   public NEOCommonData layds_id_hinhthuc_dk_cktts(
   			String pikmht_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_ckhau_tts/km_dk_ckhau_tts_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmht_id+"&sid="+Math.random();
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  	
  	
  	
   public NEOCommonData layds_id_hinhthuc_dk_cktms(
   			String pikmht_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_ckhau_tms/km_dk_ckhau_tms_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmht_id+"&sid="+Math.random();
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  	
  	
  		
  	
   public NEOCommonData layds_id_hinhthuc_cktts(
   			String pikmht_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_ckhau_tts/km_ckhau_tts_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmht_id+"&sid="+Math.random();
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  	
  	
  	
   public NEOCommonData layds_id_hinhthuc_cktms(
   			String pikmht_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_ckhau_tms/km_ckhau_tms_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmht_id+"&sid="+Math.random();
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  	
  	
 
 
 
   public NEOCommonData layds_kmid_hinhthuc_areas(
   			String pikmht_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_cachgiam_areas/km_cachgiam_areas_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmht_id+"&sid="+Math.random();
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
 
  	
   public NEOCommonData layds_kmid_dieukiens(
   			String pikmht_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_dieukiens/km_dieukiens_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmht_id+"&sid="+Math.random();
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  	 	
  	
  	
  public NEOCommonData ds_hinthucs(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/common/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}

  public NEOCommonData ds_dieukiens_ht(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_dieukiens/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
	
 	public NEOCommonData ds_hinthucs_area(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_cachgiam_areas/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
		
		public NEOCommonData ds_hinthucs_direction(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_cachgiam_directions/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
		
 public NEOCommonData ds_dieukiens_direction(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_cachgiam_directions/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
	
	public NEOCommonData ds_dieukiens_htt(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_uploads/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
		
		
		public NEOCommonData ds_htkm_dk_cktts(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_ckhau_tts/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
	
				public NEOCommonData ds_htkm_tbs(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_danhsach_tbs/km_danhsach_tbs/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
		
		public NEOCommonData ds_htkm_dk_cktms(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_ckhau_tms/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
	
  	
  		public NEOCommonData ds_htkm_cktts(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_ckhau_tts/ds_km_hinhthuc_cktts&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
	
		public NEOCommonData ds_htkm_cktms(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_ckhau_tms/ds_km_hinhthuc_cktms&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
	
	
  	
   public NEOCommonData layds_kmid_direct_codes(
   			String pikmcv_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_direction_codes/km_direct_details_ajax";	
		url_ = url_ + "&"+CesarCode.encode("km_kmtc_id")+"="+pikmcv_id+"&sid="+Math.random();
			    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  	
  	
   public NEOCommonData layds_kmid_area_codes(
   			String pikmcv_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_area_codes/km_area_codes_ajax";	
		url_ = url_ + "&"+CesarCode.encode("km_area_code_id")+"="+pikmcv_id+"&sid="+Math.random();
			    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  	
  	
   public NEOCommonData layds_kmid_areacode_details(
   			String pikmcv_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_area_codes/km_areacode_details_ajax";	
		url_ = url_ + "&"+CesarCode.encode("areacodes")+"="+pikmcv_id+"&sid="+Math.random();
			    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  	
  
  public NEOCommonData layds_cachgiam_id_tinhs(
 										 String func_schema,
 										 String schemaCommon,
 										 String cachgiam_id) 
 										{
   String s= "begin ?:="+schemaCommon+"khuyenmai.layds_cachgiam_id_tinhs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"'," +cachgiam_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }


  public NEOCommonData themmoi_km_congvans(
  							String  schema,
  							String  schemaCommon  																	  						   			        
					        ,String psten_congvan
							,String psngay_batdau
							,String psngayy_kt
							,String pscon_hieuluc
							,String psnoidung
							,String  psnguoi_cn,
  							String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_congvans('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"','"+psten_congvan+"'"                      
        +","+psngay_batdau
        +","+psngayy_kt
        +",'"+pscon_hieuluc+"'"  
        +",'"+psnoidung+"'"
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
 

  
  public NEOCommonData capnhat_km_congvans(
  							String  schema,
  							String  schemaCommon,  
  							String  pikmcv_id																  						   			        
					       	,String psten_congvan
							,String psngay_batdau
							,String psngayy_kt
							,String pscon_hieuluc
							,String psnoidung
							,String  psnguoi_cn
  							,String  psmay_cn				
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_congvans('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmcv_id+",'"+psten_congvan+"'"                         
        +","+psngay_batdau
        +","+psngayy_kt
        +",'"+pscon_hieuluc+"'"  
		+",'"+psnoidung+"'"
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
     System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
    
  public NEOCommonData themmoi_km_hinhthucs(
  							String  schema,
  							String  schemaCommon,
  							String  pikmcv_id																	  						   			        
					       ,String  psten_hinhthuc
							,String psloaiht_id
							,String pscamket_sdlt
							,String pstinhtao_ds
							,String psghichu
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_hinhthucs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmcv_id
		+",'"+psten_hinhthuc+"'"
        +","+psloaiht_id
        +","+pscamket_sdlt
        +","+pstinhtao_ds
		+",'"+psghichu
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_hinhthucs(
  							String  schema,
  							String  schemaCommon, 
  							String  pikmcv_id  
  							,String  pikmht_id																	  						   			        
					       ,String  psten_hinhthuc
							,String psloaiht_id
							,String pscamket_sdlt
							,String pstinhtao_ds
							,String psghichu
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_hinhthucs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmcv_id+","+pikmht_id                    
        +",'"+psten_hinhthuc        
        +"',"+psloaiht_id
        +","+pscamket_sdlt
        +","+pstinhtao_ds
        +",'"+psghichu+"'"		
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_hinhthucs(
  							String  schema,
  							String  schemaCommon,   
  							String  pikmcv_id,	  																					  						   			        
					       	String  pikmht_id						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_hinhthucs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmcv_id
		+","+pikmht_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  
  //Thong tin ve cach giam cuoc. cachgiams
  public NEOCommonData themmoi_km_cachgiams(
  							String  schema,
  							String  schemaCommon 						
							,String sten_cachgiam							
							,String sso_thang
							,String sapdung_sauthanghm
							,String stien_kieuld1
							,String stien_kieuld2
							,String stien_kieuld3
							,String stien_kieuld4
						//	,String s_apdungdt_id
						//	,String sdoiduongs
							,String s_apdungkmtc_id
							,String skhoanmuctcs
							,String stienkm_duochuong
							,String stien_truocthue
							,String sghichu
							,String skmcv_id
							,String skmht_id
							,String shinhthuctt_id
							,String skieugiam_id	
							,String  psnguoi_cn
							,String  psmay_cn) 
   {
   	   String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_cachgiams('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"'"
		+",'"+sten_cachgiam
		+"','"+sso_thang
		+"','"+sapdung_sauthanghm
		+"','"+stien_kieuld1
		+"','"+stien_kieuld2
		+"','"+stien_kieuld3
		+"','"+stien_kieuld4
	//	+"','"+s_apdungdt_id
	//	+"','"+sdoiduongs
		+"','"+s_apdungkmtc_id
		+"','"+skhoanmuctcs
		+"','"+stienkm_duochuong
		+"','"+stien_truocthue
		+"','"+sghichu
		+"','"+skmcv_id
		+"','"+skmht_id
		+"','"+shinhthuctt_id
		+"','"+skieugiam_id	
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_cachgiams(
  							String  schema,
  							String  schemaCommon 
  							,String pikmcg_id						
							,String sten_cachgiam							
							,String sso_thang
							,String sapdung_sauthanghm
							,String stien_kieuld1
							,String stien_kieuld2
							,String stien_kieuld3
							,String stien_kieuld4
							//,String s_apdungdt_id
						//	,String sdoiduongs
							,String s_apdungkmtc_id
							,String skhoanmuctcs
							,String stienkm_duochuong
							,String stien_truocthue
							,String sghichu
							,String skmcv_id
							,String skmht_id
							,String shinhthuctt_id
							,String skieugiam_id	
							,String  psnguoi_cn
							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_cachgiams('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmcg_id                   
       	+",'"+sten_cachgiam
		+"','"+sso_thang
		+"','"+sapdung_sauthanghm
		+"','"+stien_kieuld1
		+"','"+stien_kieuld2
		+"','"+stien_kieuld3
		+"','"+stien_kieuld4
	//	+"','"+s_apdungdt_id
	//	+"','"+sdoiduongs
		+"','"+s_apdungkmtc_id
		+"','"+skhoanmuctcs
		+"','"+stienkm_duochuong
		+"','"+stien_truocthue
		+"','"+sghichu
		+"','"+skmcv_id
		+"','"+skmht_id
		+"','"+shinhthuctt_id
		+"','"+skieugiam_id	
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_cachgiams(
  							String  schema,
  							String  schemaCommon,   
  							String  pikmht_id,	  																					  						   			        
					       	String  pikmcg_id						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_cachgiams('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmht_id
		+","+pikmcg_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  //--Ket thuc xu ly ve cach giam cuoc dien thoai.
  
  
  //Thong tin ve dieu kien lay danh sach khuyen mai
  public NEOCommonData themmoi_km_dieukiens(
  							String  schema,
  							String  schemaCommon 						
							,String psten_dieukien
							,String psdoituongs	
							,String psngay_kt
							,String psghichu
							,String pskmkt_id
							,String pskmht_id
							,String  psnguoi_cn
							,String  psmay_cn) 
   {
   	   String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_dieukiens('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"'"
		+",'"+psten_dieukien
		+"','"+psdoituongs
		+"',"+psngay_kt
		+",'"+psghichu
		+"','"+pskmkt_id
		+"','"+pskmht_id
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_dieukiens(
  							String  schema,
  							String  schemaCommon 
  							,String  pskmdk_id						
							,String psten_dieukien
							,String psdoituongs		
							,String psngay_kt
							,String psghichu
							,String pskmkt_id
							,String pskmht_id
							,String  psnguoi_cn
							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_dieukiens('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pskmdk_id                   
       	+",'"+psten_dieukien
        +"','"+psdoituongs
		+"',"+psngay_kt
		+",'"+psghichu
		+"','"+pskmkt_id
		+"','"+pskmht_id
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_dieukiens(
  							String  schema,
  							String  schemaCommon,   
  							String  pikmht_id,	  																					  						   			        
					       	String  pikmdk_id						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_dieukiens('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmht_id
		+","+pikmdk_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  //--Ket thuc xu ly ve dieukien lay danh sach.
  
	public NEOCommonData ds_nhanvien(int donviql_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=khuyenmai/baocao/danhsach_tien_kms/ds_nhanviens&" + CesarCode.encode("donviql_id") + "=" + donviql_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}
	
	public NEOCommonData ds_nhanvien_inphieu(int donviql_id)
	{
	    	String url_ = "/main?"; 
	    	url_ = url_+ CesarCode.encode("configFile") + "=khuyenmai/baocao/in_hoadon_kms/ds_nhanviens&" + CesarCode.encode("donviql_id") + "=" + donviql_id;
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}
  
  public NEOCommonData laytt_ngay_apdung(   String func_schema,
											String schemaCommon,
											String kmht_id,
											String kmcv_id)
	{
		String s = "begin ?:=" + schemaCommon + "promotions.laytt_ngay_apdung('" + getUserVar("userID")
			 + "','" + getUserVar("sys_agentcode")
			 + "','" + schemaCommon
			 + "','" + getUserVar("sys_dataschema")
			 + "'," + kmht_id
			 + "," + kmcv_id
			 + ");end;";
		System.out.println(s); 
		
		NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}
  
  
  /*Nhu thanh the:
  *chuc nang: lay thong tin thue bao khuyen mai.
  *phuc vu nut tim kiem
  *ngay_pt: 09/10/2008. 
  						
  */
    public NEOCommonData layds_tb_khuyenmais
								(String piPAGEID,
    							   String piREC_PER_PAGE,    							
    							   String ma_tb,
    							   String ngay_apdung, 
    							   String ngay_kt,   							  
    							   String khuyenmai_id,
    							   String trangthai_id) {
    String  s= "/main?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_danhsach_tbs/km_danhsach_tbs/km_danhsach_tbs_ajax"
		+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
		+ "&" +CesarCode.encode("ma_tb")+"="+ma_tb
		+ "&" +CesarCode.encode("ngay_apdung")+"="+ngay_apdung
		+ "&" +CesarCode.encode("ngay_kt")+"="+ngay_kt
		+ "&" +CesarCode.encode("khuyenmai_id")+"="+khuyenmai_id
		+ "&" +CesarCode.encode("trangthai_id")+"="+trangthai_id
        ;   
	System.out.println(s); 
		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_); 
     
  }
  
  
  public NEOCommonData themmoi_ds_khuyenmais(	 
										 String psschema,
  										 String psschemaCommon,   
  										 String psma_tb  
										,String pikmht_id
										,String pikmcv_id										
										,String psghichu
										,String psnguoi_cn
										,String psmay_cn
									
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"promotions.themmoi_ds_khuyenmais('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
        +",'"+psma_tb+"'"
        +","+pikmht_id 
        +","+pikmcv_id      
		+","+psghichu
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_); 
    
  			
  	}
	
public NEOCommonData capnhat_ds_khuyenmais(
     String psschema
	,String psschemaCommon  
	,String psdsSomay   									
	,String psdsKMID
	,String psdsApdung
	,String psghichu
	,String psnguoi_cn
	,String psmay_cn
									
 ){
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"promotions.capnhat_ds_khuyenmais('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)
        +"','"+psdsKMID
        +"','"+psdsApdung
        +"','"+psdsSomay       
		+"',"+psghichu
		+",'"+psnguoi_cn
		+"','"+psmay_cn
        +"');"
        +"end;";
        
     System.out.println(s); 		
  	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);    
  }  
  
  
  public NEOCommonData laytt_khuyenmais(    String func_schema,
											String schemaCommon,
											String khuyenmai_id)
	{
		String s = "begin ?:=" + schemaCommon + "khuyenmai.laytt_khuyenmais('" + getUserVar("userID")
			 + "','" + getUserVar("sys_agentcode")
			 + "','" + schemaCommon
			 + "','" + getUserVar("sys_dataschema")
			 + "'," + khuyenmai_id
			 + ");end;";

		 System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}

	
	
  
  
	
  
  
//khuyen mai theo ma vungs
	
    
  public NEOCommonData themmoi_km_cachgiam_areas(
  							String  schema,
  							String  schemaCommon
  								
							,String sten_cachgiam
							,String ssophut_tinnhan
							,String ssotien_phaitra
								
							,String sso_thang
							,String sapdung_sauthanghm
							,String s_cachtru
								
							,String sid_khuyenmais
							,String stu_ngay
							,String sden_ngay
								
							,String sghichu
							//,String skmcv_id
							,String skmht_id
							,String shinhthuctt_id
							,String skieugiam_id
								
									,String psnguoi_cn
										,String psmay_cn					
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_cachgiam_areas('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"'"
		+",'"+sten_cachgiam+"'"
        +",'"+ssophut_tinnhan
        +"','"+ssotien_phaitra
        	
        +"','"+sso_thang        	
        +"','"+sapdung_sauthanghm
        +"','"+s_cachtru
        	
		+"','"+sid_khuyenmais
		+"','"+stu_ngay			
		+"','"+sden_ngay
			
		+"','"+sghichu
		//+"','"+skmcv_id
		+"','"+skmht_id
		+"','"+shinhthuctt_id
			
		+"','"+skieugiam_id
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_cachgiam_areas(
  							String  schema,
  							String  schemaCommon, 
  							String  pikmcg_area_id  
  							,String sten_cachgiam
							,String ssophut_tinnhan
							,String ssotien_phaitra
								
							,String sso_thang
							,String sapdung_sauthanghm
							,String s_cachtru
								
							,String sid_khuyenmais
							,String stu_ngay
							,String sden_ngay
								
							,String sghichu
							//,String skmcv_id
							,String skmht_id
							,String shinhthuctt_id
							,String skieugiam_id
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_cachgiam_areas('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmcg_area_id                    
       	+",'"+sten_cachgiam+"'"
        +",'"+ssophut_tinnhan
        +"','"+ssotien_phaitra
        	
        +"','"+sso_thang        	
        +"','"+sapdung_sauthanghm
        +"','"+s_cachtru
        	
		+"','"+sid_khuyenmais
		+"','"+stu_ngay			
		+"','"+sden_ngay
			
		+"','"+sghichu
		//+"','"+skmcv_id
		+"','"+skmht_id
		+"','"+shinhthuctt_id
			
		+"','"+skieugiam_id
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_cachgiam_areas(
  							String  schema,
  							String  schemaCommon,   
  							String  pikmht_id,	  																					  						   			        
					       	String  pikmcg_area_id						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_cachgiam_areas('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmht_id
		+","+pikmcg_area_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  
//Ket thuc khuyen mai theo ma vungs

//khuyen mai theo khoan muc tinh cuoc
	
    
  public NEOCommonData themmoi_km_cachgiam_directions(
  							String  schema,
  							String  schemaCommon
  								
							,String sten_cachgiam
							,String pstien_phantram
							,String ssotien_phaitra
								
							,String sso_thang
							,String sapdung_sauthanghm
						//	,String s_cachtru
								
							,String sid_khuyenmais
						//	,String stu_ngay
						//	,String sden_ngay
								
							,String sghichu
							//,String skmcv_id
							,String skmht_id
							,String shinhthuctt_id
							,String skieugiam_id
								
									,String psnguoi_cn
										,String psmay_cn					
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_cachgiam_directions('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"'"
		+",'"+sten_cachgiam+"'"
        +",'"+pstien_phantram
        +"','"+ssotien_phaitra
        	
        +"','"+sso_thang        	
        +"','"+sapdung_sauthanghm
       // +"','"+s_cachtru
        	
		+"','"+sid_khuyenmais
	//	+"','"+stu_ngay			
	//	+"','"+sden_ngay
			
		+"','"+sghichu
		//+"','"+skmcv_id
		+"','"+skmht_id
		+"','"+shinhthuctt_id
			
		+"','"+skieugiam_id
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_cachgiam_directions(
  							String  schema,
  							String  schemaCommon, 
  							String  pikmcg_direction_id  
  							,String sten_cachgiam
							,String pstien_phantram
							,String ssotien_phaitra
								
							,String sso_thang
							,String sapdung_sauthanghm
						//	,String s_cachtru
								
							,String sid_khuyenmais
							//,String stu_ngay
						//	,String sden_ngay
								
							,String sghichu
							//,String skmcv_id
							,String skmht_id
							,String shinhthuctt_id
							,String skieugiam_id
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_cachgiam_directions('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmcg_direction_id                    
       	+",'"+sten_cachgiam+"'"
        +",'"+pstien_phantram
        +"','"+ssotien_phaitra
        	
        +"','"+sso_thang        	
        +"','"+sapdung_sauthanghm
       // +"','"+s_cachtru
        	
		+"','"+sid_khuyenmais
	//	+"','"+stu_ngay			
	//	+"','"+sden_ngay
			
		+"','"+sghichu
		//+"','"+skmcv_id
		+"','"+skmht_id
		+"','"+shinhthuctt_id
			
		+"','"+skieugiam_id
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_cachgiam_directions(
  							String  schema,
  							String  schemaCommon,   
  							String  pikmht_id,	  																					  						   			        
					       	String  pikmcg_direction_id						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_cachgiam_directions('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmht_id
		+","+pikmcg_direction_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  	
  	
  	 
 public NEOCommonData layds_km_cachgiam_direction_id(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmcg_direction_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.layds_km_cachgiam_direction_id('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmcg_direction_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }
  
  
   
   public NEOCommonData layds_kmid_hinhthuc_directions(
   			String pikmht_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_cachgiam_directions/km_cachgiam_directions_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmht_id+"&sid="+Math.random();
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  
//Ket thuc khuyen mai theo khoan muc tinh cuoc    

//Bat dau xu ly ID khuyen mai theo dieu huong dien thoai.
	
    
   public NEOCommonData layds_km_direct_codes( String radDangAD)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_direction_codes/km_direct_codes_ajax";	
		url_ = url_ + "&"+CesarCode.encode("ds_direct_codes")+"="+radDangAD+"&sid="+Math.random();
		
		System.out.println(url_); 	    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  
  
  
   public NEOCommonData layds_km_direct_details( String radDangAD)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_direction_codes/km_direct_details_ajax";	
		url_ = url_ + "&"+CesarCode.encode("ds_direct_details")+"="+radDangAD+"&sid="+Math.random();
		
		System.out.println(url_); 	    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  
   public NEOCommonData laytt_kmid_direct_codes(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmcv_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_direct_codes('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmcv_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }
  
     public NEOCommonData laytt_kmid_area_ids(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmcv_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_area_ids('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmcv_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }
  
  
  
  
  
  
   public NEOCommonData laytt_kmid_direct_details(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmcv_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_direct_details('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmcv_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }
  
  
  
  
  public NEOCommonData themmoi_km_direct_codes(
  							String  schema,
  							String  schemaCommon, 																	  						   			        
					        String name,
							String picon_hieuluc,		
							String psNoiDungKM
							,String  psnguoi_cn,
  							String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_direct_codes('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"','"+name+"'"                      
        +","+picon_hieuluc
        +",'"+psNoiDungKM
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
 

  
  public NEOCommonData capnhat_km_direct_codes(
  							String  schema,
  							String  schemaCommon,  
  							String  id,															  						   			        
					        String name,
							String picon_hieuluc,		
							String psNoiDungKM
							,String  psnguoi_cn
  							,String  psmay_cn				
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_direct_codes('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+id+",'"+name+"'"                         
        +","+picon_hieuluc
        +",'"+psNoiDungKM
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
     System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  	
  	  
    
  public NEOCommonData themmoi_km_direct_details(
  							String  schema,
  							String  schemaCommon,
  							String  pikm_kmtc_id,
  							String	txtname_1,
							String pskmtc_id,
							String pshuongdt_id,
							String psghichu
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_direct_details('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikm_kmtc_id		
        +",'"+txtname_1
        +"','"+pskmtc_id
        +"','"+pshuongdt_id
		+"','"+psghichu
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_direct_details(
  							String  schema,
  							String  schemaCommon, 
  							String  id,
  							String  pikm_kmtc_id,
  							String	txtname_1,
							String pskmtc_id,
							String pshuongdt_id,
							String psghichu
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_direct_details('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+id+","+pikm_kmtc_id                    
    	+",'"+txtname_1
        +"','"+pskmtc_id
        +"','"+pshuongdt_id
		+"','"+psghichu
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_direct_details(
  							String  schema,
  							String  schemaCommon,   
  							String  id,	  																					  						   			        
					       	String  pikm_kmtc_id						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_direct_details('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+id
		+","+pikm_kmtc_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  
  
  
//Ket thuc xu ly ID khuyen mai theo huong dien thoai.  


  
  
  public NEOCommonData themmoi_km_area_ids(
  							String  schema,
  							String  schemaCommon, 																	  						   			        
					        String name,
							String picon_hieuluc,		
							String psNoiDungKM
							,String  psnguoi_cn,
  							String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_area_ids('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"','"+name+"'"                      
        +","+picon_hieuluc
        +",'"+psNoiDungKM
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
 

  
  public NEOCommonData capnhat_km_area_ids(
  							String  schema,
  							String  schemaCommon,  
  							String  id,															  						   			        
					        String name,
							String picon_hieuluc,		
							String psNoiDungKM
							,String  psnguoi_cn
  							,String  psmay_cn				
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_area_ids('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+id+",'"+name+"'"                         
        +","+picon_hieuluc
        +",'"+psNoiDungKM
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
     System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  	

  public NEOCommonData xoa_km_areacode_details(
  							String  schema,
  							String  schemaCommon,   
  							String  km_area_code_id,	  																					  						   			        
					       	String  psareacode						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_areacode_details('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+km_area_code_id
		+",'"+psareacode       
        +"');"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  	  	  
 
   public NEOCommonData them_km_areacode_details(
  							String  schema,
  							String  schemaCommon,   
  							String  km_area_code_id,	  																					  						   			        
					       	String  psareacode						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.them_km_areacode_details('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+km_area_code_id
		+",'"+psareacode       
        +"');"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   	  	  


    
  public NEOCommonData themmoi_km_dk_cktms(
  							String  schema,
  							String  schemaCommon,
  							String  pskmht_id
  							,String pstxtten_dk_cktm  
							,String pstxtds_makh  
							,String pstxtdoituongs  
							,String psid_khuyenmais  
							,String pshinhthuctt_id  
							,String pskmtc_idkhuyenmai  
							,String pstxttienno_canduoi  
							,String pstxttienno_cantren  
							,String pschkgma_kh 
							,String pschkck_luytien 
							,String  psNoiDungKM 
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_dk_cktms('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pskmht_id
		+",'"+pstxtten_dk_cktm+"'"
        +",'"+pstxtds_makh
        +"','"+pstxtdoituongs
        +"','"+psid_khuyenmais
        +"',"+pshinhthuctt_id
        +","+pskmtc_idkhuyenmai
        +",'"+pstxttienno_canduoi
       	+"','"+pstxttienno_cantren
        +"',"+pschkgma_kh
        +","+pschkck_luytien
		+",'"+psNoiDungKM
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_dk_cktms(
  							String  schema,
  							String  schemaCommon, 
  							String  pikm_dk_cktm_id  
  							,String  pskmht_id
  							,String pstxtten_dk_cktm  
							,String pstxtds_makh  
							,String pstxtdoituongs  
							,String psid_khuyenmais  
							,String pshinhthuctt_id  
							,String pskmtc_idkhuyenmai  
								
							,String pstxttienno_canduoi  
							,String pstxttienno_cantren  
							,String pschkgma_kh 
							,String pschkck_luytien 
							,String  psNoiDungKM 
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_dk_cktms('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikm_dk_cktm_id+","+pskmht_id                    
       	+",'"+pstxtten_dk_cktm+"'"
        +",'"+pstxtds_makh
        +"','"+pstxtdoituongs
           +"','"+psid_khuyenmais
        +"',"+pshinhthuctt_id
        +","+pskmtc_idkhuyenmai
        +",'"+pstxttienno_canduoi
       	+"','"+pstxttienno_cantren
        +"',"+pschkgma_kh
        +","+pschkck_luytien
		+",'"+psNoiDungKM
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_cktms(
  							String  schema,
  							String  schemaCommon    						
  							,String pikm_dk_cktm_id 
  									,String  pskmht_id 					
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_dk_cktms('"+getUserVar("userID")
							+"','"+getUserVar("sys_agentcode")		
							+"','"+CesarCode.decode(schemaCommon)
							+"','"+getUserVar("sys_dataschema")+"',"+pikm_dk_cktm_id
							+","+pskmht_id       
							+");"
							+"end;"
							;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  
  
   public NEOCommonData themmoi_km_cktms(
  							String  schema,
  							String  schemaCommon,
  							String  pskmht_id
							,String psten_cktm
							,String pstilegiam_tiengiam
						//	,String psid_khuyenmais
							,String pstien_canduoi
							,String pstien_cantren
							,String pssonamhd_canduoi
							,String pssonamhd_cantren
							,String psslmay_canduoi
							,String psslmay_cantren
						//	,String pshinhthuctt_id
							,String pskieugiam_id
							,String  psNoiDungKM 
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_cktms('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pskmht_id
		+",'"+psten_cktm+"'"
        +",'"+pstilegiam_tiengiam
      //  +"','"+psid_khuyenmais
        +"','"+pstien_canduoi
       	+"','"+pstien_cantren
       	+"','"+pssonamhd_canduoi
       	+"','"+pssonamhd_cantren
       		
        +"','"+psslmay_canduoi
        +"','"+psslmay_cantren 
      //  +"',"+pshinhthuctt_id
       	+"',"+pskieugiam_id        	
		+",'"+psNoiDungKM
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_cktms(
  							String  schema,
  							String  schemaCommon, 
  							String  pikm_cktm_id  
  							,String  pskmht_id
  							,String psten_cktm
							,String pstilegiam_tiengiam
						//	,String psid_khuyenmais
							,String pstien_canduoi
							,String pstien_cantren
							,String pssonamhd_canduoi
							,String pssonamhd_cantren
							,String psslmay_canduoi
							,String psslmay_cantren
						//	,String pshinhthuctt_id
							,String pskieugiam_id
							,String  psNoiDungKM 
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_cktms('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikm_cktm_id+","+pskmht_id                    
      	+",'"+psten_cktm+"'"
        +",'"+pstilegiam_tiengiam
       // +"','"+psid_khuyenmais
        +"','"+pstien_canduoi
       	+"','"+pstien_cantren
       	+"','"+pssonamhd_canduoi
       	+"','"+pssonamhd_cantren
        +"','"+psslmay_canduoi
        +"','"+psslmay_cantren 
      //  +"',"+pshinhthuctt_id
       	+"',"+pskieugiam_id        	
		+",'"+psNoiDungKM
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_ext_cktms(
  							String  schema,
  							String  schemaCommon  
  						
  							,String pikm_dk_cktm_id 
  									,String  pskmht_id 					
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_cktms('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikm_dk_cktm_id
		+","+pskmht_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  
  //////////////////thanh toan
  public NEOCommonData themmoi_km_dk_cktts(
  							String  schema,
  							String  schemaCommon,
  							String  pskmht_id
  							,String psten_dieukien
							,String psds_makh
							,String psdoituongs
							,String pshinhthuctts
							,String pskieuno_id
							,String psnoidung	
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_dk_cktts('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pskmht_id
		+",'"+psten_dieukien+"'"
        +",'"+psds_makh
        +"','"+psdoituongs
        +"','"+pshinhthuctts
       	+"','"+pskieuno_id
		+"','"+psnoidung
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_dk_cktts(
  							String  schema,
  							String  schemaCommon, 
  							String  pikm_cktm_id  
  							,String  pskmht_id
  							,String psten_dieukien
							,String psds_makh
							,String psdoituongs
							,String pshinhthuctts
							,String pskieuno_id
							,String psnoidung
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_dk_cktts('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikm_cktm_id+","+pskmht_id                    
      	+",'"+psten_dieukien+"'"
        +",'"+psds_makh
        +"','"+psdoituongs
        +"','"+pshinhthuctts
       	+"','"+pskieuno_id
		+"','"+psnoidung
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_cktts(
  							String  schema,
  							String  schemaCommon    						
  							,String pikm_cktm_id 
  									,String  pskmht_id 					
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_dk_cktts('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikm_cktm_id
		+","+pskmht_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  
  
   public NEOCommonData themmoi_km_cktts(
  							String  schema,
  							String  schemaCommon,
  							String  pskmht_id					   	    
							,String psten_cktt
							,String pstilegiam_tiengiam
							,String pstien_canduoi
							,String pstien_cantren
							,String pskhoanmuctcs
							,String pscbohinhthuctt_id
							,String pscbokieugiam_id
							,String pscbokieutru_id
							,String psghichu
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_cktts('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pskmht_id
		+",'"+psten_cktt+"'"
        +",'"+pstilegiam_tiengiam
        +"','"+pstien_canduoi
        +"','"+pstien_cantren
       	+"','"+pskhoanmuctcs
       	+"',"+pscbohinhthuctt_id
       	+","+pscbokieugiam_id       		
        +","+pscbokieutru_id
		+",'"+psghichu
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_cktts(
  							String  schema,
  							String  schemaCommon,
  								String  pskmht_id	 
  							,String pskm_cktt_id
							,String psten_cktt
							,String pstilegiam_tiengiam
							,String pstien_canduoi
							,String pstien_cantren
							,String pskhoanmuctcs
							,String pscbohinhthuctt_id
							,String pscbokieugiam_id
							,String pscbokieutru_id
							,String psghichu
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_cktts('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pskm_cktt_id+","+pskmht_id                    
     	+",'"+psten_cktt+"'"
        +",'"+pstilegiam_tiengiam
        +"','"+pstien_canduoi
        +"','"+pstien_cantren
       	+"','"+pskhoanmuctcs
       	+"',"+pscbohinhthuctt_id
       	+","+pscbokieugiam_id       		
        +","+pscbokieutru_id
		+",'"+psghichu
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_ext_cktts(
  							String  schema,
  							String  schemaCommon  
  						
  							,String pikm_cktm_id 
  									,String  pskmht_id 					
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_cktts('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikm_cktm_id
		+","+pskmht_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  	
	 public String lay_ds_tbkm( String func_schema,
 									String psSomay,
    							   	String psNgaycn,    							
 	    							String psNguoicn,
									String psKmcu,
    								String psTentt, 
    								String psSogt,   							  
    								String psDiachi,
    								String pspage_num,
									String pspage_rec
									) 
 {
   String s= "begin ?:="+func_schema+"pttb_km.lay_ds_tbkm('"+getUserVar("userID")
		+"','"+getUserVar("sys_dataschema")+"'"
		+",'"+psSomay+"'"
		+",'"+psNgaycn+"'"
		+",'"+psNguoicn+"'"
		+",'"+psKmcu+"'"
		+",'"+psTentt+"'"
		+",'"+psSogt+"'"
		+",'"+psDiachi+"'"		
		+",'"+pspage_num+"'"
		+",'"+pspage_rec
		+"');end;";
	System.out.println("lyly:"+s);
	
	return s;
  }   	
   // insert vao bang lichsu_km
    public String capnhat_tbkm( String func_schema,
 									String psMatb,
    							   	String psIdkmcu,    							
 	    							String psIdkmmoi,
									String psNgaycn,
    								String psGhichu,  
									String psSerial,    							
 	    							String psImei,
    								String pspage_num,
									String pspage_rec
									) 
 {
   String s= "begin ?:="+ func_schema +"pttb_km.capnhat_tbkm('"+getUserVar("userID")
		+"','"+getUserVar("sys_dataschema")+"'"
		+",'"+psMatb+"'"
		+",'"+psIdkmcu+"'"
		+",'"+psIdkmmoi+"'"
		+",'"+psNgaycn+"'"
		+",'"+psGhichu+"'"
		+",'"+psSerial+"'"
		+",'"+psImei+"'"
		+",'"+pspage_num+"'"
		+",'"+pspage_rec
		+"');end;";
	
	return s;
  }  
  public String xoa_tbkm_id( String func_schema,
 									String psMatb,
    							   	String psIdkmcu,    							
 	    							String psIdkmmoi,
									String psNgaycn,
    								String psGhichu,   							  
    								String pspage_num,
									String pspage_rec
									) 
 {
   String s= "begin ?:="+ func_schema +"pttb_km.xoa_tbkm_id('"+getUserVar("userID")
		+"','"+getUserVar("sys_dataschema")+"'"
		+",'"+psMatb+"'"
		+",'"+psIdkmcu+"'"
		+",'"+psIdkmmoi+"'"
		+",'"+psNgaycn+"'"
		+",'"+psGhichu+"'"
		+",'"+pspage_num+"'"
		+",'"+pspage_rec
		+"');end;";
	
	return s;
  }  
  
  
	public String lay_dsls_tbkm(String psSomay,
    							   	String pspage_num,
									String pspage_rec
									) {
    return "/main?"+CesarCode.encode("configFile")+"=PTTB/hthd/capnhat_km/ajax_capnhat_km"
		+"&"+CesarCode.encode("ma_tb") + "=" + psSomay
		+"&"+CesarCode.encode("page_num") + "=" + pspage_num
		+"&"+CesarCode.encode("page_rec") + "=" + pspage_rec
        ;
	}
	 //lan anh
  
   public String lay_ds_tbdc(  String userid,
   							   String data_schema,
 							   String psMaTB,
    						   String psKieuDC	
						) 
 {
   String s= "begin ?:=admin_v2.PTTB_TRACUU.laytt_khachhang_datcoc('"+userid
		+"','"+data_schema+"'"
		+",'"+psMaTB+"'"
		+",'"+psKieuDC	
		+"');end;";
	System.out.println("la:"+s); 
	return s;
  }   	
  //cap nhat tien dat coc
   public String capnhat_tbdc( String userid,
   							   String data_schema,
 							   String psMaTB,
    						   String psKieuDC,
							   String psTienDC,
    						   String psGhichu,
							   String psMahd,
							   String psTienDC_cu
									) 
 {
   String s= "begin ?:=admin_v2.pttb_chucnang.capnhat_khachhang_datcoc('"+userid
		+"','"+data_schema+"'"
		+",'"+psMaTB+"'"
		+",'"+psKieuDC+"'"
		+",'"+psTienDC+"'"
		+",'"+psGhichu+"'"
		+",'"+psMahd+"'"
		+",'"+psTienDC_cu
		+"');end;";
	System.out.println("la:"+s);
	return s;
  } 
  //them ngay kich hoat
  public String value_themngay_kh(String psDsTB,
                                  String userid
	){
	 String s= "begin ?:=ccs_admin.BCTK_NUMSTORE.them_ngay_kh('"+psDsTB
		+"','"+userid
		+"');end;";
	System.out.println("la:"+s);
	return s;
    }
  // hien thi lich su cap nhat

	
	public  String lay_dsls_tbdc(String psMaTB, String psKieuDC){
   		return "/main?"+CesarCode.encode("configFile")+"=PTTB/hthd/capnhat_tiendatcoc/ajax_capnhat_dc"
			    +"&"+CesarCode.encode("psMaTB") + "=" + psMaTB
   				+"&"+CesarCode.encode("psKieuDC") + "=" + psKieuDC;
   	}	
  
    
}

