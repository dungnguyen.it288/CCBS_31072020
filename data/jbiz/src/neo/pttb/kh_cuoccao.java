package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class kh_cuoccao
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.kh_cuoccao was called");
  }


 public String layDSKHCuocCao( 		   
	                    
	       String psKyHoaDon       ,
	       String psKieuTheoDoi    ,// theo tieu chi tim kiem, 2.Danh sach da co 3.Dang theo doi va tieu chi
	       String psSoMay          , 
	       String psMaCQ		   ,
	       String psTienCuocNong   ,
	       String psTienKHRR       ,
	       String psTienHKTinh     ,
		   String psTienNhanTin     ,
	       String psTienHMM        
 	
 	){
 	
 	 return "/main?"+CesarCode.encode("configFile")+"=pttb/chucnang/kh_cuoc_cao/ajax_dskh"
		+"&"+CesarCode.encode("psKyHoaDon")+"="+psKyHoaDon
		+"&"+CesarCode.encode("psKieuTheoDoi")+"="+psKieuTheoDoi
		+"&"+CesarCode.encode("psSoMay")+"="+psSoMay
		+"&"+CesarCode.encode("psMaCQ")+"="+psMaCQ
		+"&"+CesarCode.encode("psTienCuocNong")+"="+psTienCuocNong
		+"&"+CesarCode.encode("psTienKHRR")+"="+psTienKHRR
		+"&"+CesarCode.encode("psTienHKTinh")+"="+psTienHKTinh
		+"&"+CesarCode.encode("psTienNhanTin")+"="+psTienNhanTin
		+"&"+CesarCode.encode("psTienHMM")+"="+psTienHMM;
 }
  
 public String taoDSKHCuocCao( 		   
	                    
	       String psUserId       ,
	       String psDataSchema    ,
	       String psUserIp          , 
	       
	       String psFuncSchema   ,
	       String psCKN   ,
	       
		   String psDSLoaiKH   ,
	       String psDSSoMay       
 	
 	){
 	
 	  String s=    "begin ? := "+psFuncSchema+"PTTB_CUOCCAO.taods_kh_cuoccao("		
	 
		+" '"+psUserId+"'"
		+",'"+psDataSchema+"'"                                  
		+",'"+psUserIp+"'"                                  
		+",'"+psFuncSchema+"'"                                  
		
		+",'"+psCKN+"'"
		+",'"+psDSLoaiKH+"'"
		+",'"+psDSSoMay+"'"
	
		+");"
        +"end;"
        ;
return s;
 }
  
  
 public String xoaDSKHCuocCao( 		   
	                    
	       String psUserId       ,
	       String psDataSchema    ,
	       String psUserIp          , 
	       
	       String psFuncSchema   ,
	       String psCKN   ,
	       
	       String psDSSoMay       
 	
 	){
 	
 	  String s=    "begin ? := "+psFuncSchema+"PTTB_CUOCCAO.xoads_kh_cuoccao("		
	 
		+" '"+psUserId+"'"
		+",'"+psDataSchema+"'"                                  
		+",'"+psUserIp+"'"                                  
		+",'"+psFuncSchema+"'"                                  
		
		+",'"+psCKN+"'"
		+",'"+psDSSoMay+"'"
	
		+");"
        +"end;"
        ;
return s;
 }
  
  
  
}














