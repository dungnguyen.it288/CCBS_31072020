package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pkg_service extends NEOProcessEx {
  public void run() {
    System.out.println(" goi_alo_gt was called");
  }
  public String addService(String sAgentCode,
						   String psServiceList,
						   String psMsisdn,
						   String psDes) throws Exception
	{
	 	String s = "begin ? := admin_v2.pttb_dichvu.addServicePro('" 
			 + getUserVar("sys_dataschema")     +"','"
			 + sAgentCode 	     			    +"','"			 
			 + getUserVar("userID")  			+"','"
			 + psServiceList 	     			+"','"
			 + psMsisdn                         +"','"
			 + psDes 	     			
			 +"'); end;";
		System.out.println("addService "+s);
		return this.reqValue("",s);
	}
  public String getMsisdnInfo(String psMsisdn)
	{
	String s=    "admin_v2.pttb_dichvu.get_msisdn_info('"
			 + getUserVar("sys_dataschema")     +"','"			 
			 + getUserVar("userID")  			+"','"
			 + psMsisdn
			 +"');"
			 +"end;"
			;
       System.out.println(s);
	   NEOExecInfo nei_ = new NEOExecInfo(s);
	   return s; 
	}
}