package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class data_exports extends NEOProcessEx {
	public String new_data_exports(
		String pschema,
		String psql_name,
		String psql_text,
		String temp_path,
		String file_name,
		String pmatinh,
		String pstatus,
		String pType,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pk_data_exports.new_data_exports("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(psql_name)+"',"
				+"'"+ConvertFont.UtoD(psql_text)+"',"
				+"'"+temp_path+"',"
				+"'"+file_name+"',"
				+"'"+pmatinh+"',"
				+"'"+pstatus+"',"
				+"'"+pType+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_data_exports(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pk_data_exports.del_data_exports("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_data_exports(
	String pschema,
		String psql_id,
		String psql_name,
		String psql_text,
		String temp_path,
		String file_name,
		String pmatinh,
		String pstatus,
		String pType,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pk_data_exports.edit_data_exports("
				+"'"+pschema+"',"
				+"'"+psql_id+"',"
				+"'"+ConvertFont.UtoD(psql_name)+"',"
				+"'"+ConvertFont.UtoD(psql_text)+"',"
				+"'"+temp_path+"',"
				+"'"+file_name+"',"
				+"'"+pmatinh+"',"
				+"'"+pstatus+"',"
				+"'"+pType+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
}