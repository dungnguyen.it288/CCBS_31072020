package neo.pttb;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import cosaldap.Base64.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;


public class dk_goi
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb.dk_goi was called");
  }   
	public String them_moi_dd(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String dsmay 				
		, String chunhom
		, String ghiChu 		
		, String sIP		
  	) throws Exception {

     String s=    "begin ? := "+FuncSchema+"pkg_regis_data.them_moi_dd("	
		+" '"+loaiGoi+"'"
		+",'"+dsmay+"'"                           
		+",'"+chunhom+"'"  
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+sUserId+"'"		
		+",'"+sAgentCode+"'"
		+",'"+sDataSchema+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}	
	
public String cap_nhat_dd(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String sIP	
		, String loaiGoi
		, String idGoi			
		, String chunhom
		, String dsmay 	
		, String ghiChu 	
  	) throws Exception {

     String s=    "begin ? := "+FuncSchema+"pkg_regis_data.cap_nhat_dd("	
		+" '"+loaiGoi+"'"
		+",'"+idGoi+"'"
		+",'"+dsmay+"'"                           
		+",'"+chunhom+"'"	
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+sUserId+"'"
		+",'"+sIP+"'"		
		+",'"+sAgentCode+"'"
		+",'"+sDataSchema+"'"							
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}

public String huy_goi_dd(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String idGoi
		, String Loaigoi
		, String ghiChu
	    , String sMay_cn		
  	) throws Exception {
		
     String s=    "begin ? := "+sFuncSchema+"pkg_regis_data.huy_goi_dd("		
		+" '"+sDataSchema+"'"                                                             
		+",'"+sAgentCode+"'" 
		+",'"+sUserId+"'"  
		+",'"+sMay_cn+"'"
		+",'"+idGoi+"'"
		+",'"+Loaigoi+"'"
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}	
     public String them_moi(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String loaiGoi
		, String dsmay 				
		, String chunhom
		, String ngay_dk
		, String ghiChu 		
		, String sIP		
  	) throws Exception {
     String s=    "begin ? := "+FuncSchema+"pkg_regis_data.them_moi("	
		+" '"+loaiGoi+"'"
		+",'"+dsmay+"'"                           
		+",'"+chunhom+"'"      
		+",'"+ngay_dk+"'"    		
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+sUserId+"'"		
		+",'"+sAgentCode+"'"
		+",'"+sDataSchema+"'"
		+",'"+sIP+"'"		
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	
	
	public String cap_nhat(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String FuncSchema
		, String sIP	
		, String loaiGoi
		, String idGoi			
		, String chunhom
		, String ngay_dk
		, String dsmay 	
		, String ghiChu 
		, String kieu
		, String loaigoitach		
  	) throws Exception {

     String s=    "begin ? := "+FuncSchema+"pkg_regis_data.cap_nhat("	
		+" '"+loaiGoi+"'"
		+",'"+idGoi+"'"
		+",'"+dsmay+"'"                           
		+",'"+chunhom+"'"  
		+",'"+ngay_dk+"'"   
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+",'"+kieu+"'"
		+",'"+loaigoitach+"'"
		+",'"+sUserId+"'"		
		+",'"+sAgentCode+"'"
		+",'"+sDataSchema+"'"		
		+",'"+sIP+"'"			
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	public String chuyenloai_goi(
	    	  String sUserId
			, String sAgentCode
			, String sDataSchema
			, String FuncSchema
			, String sIP	
			, String loaiGoi
			, String loaiGoi_cu
			, String idGoi			
			, String chunhom1
			, String chunhom2
			, String dsmay 	
			, String ghiChu 
	  	) throws Exception {
	     String s=    "begin ? := "+FuncSchema+"pkg_regis_data.chuyenloai_goi("
			+" '"+loaiGoi+"'"
			+",'"+loaiGoi_cu+"'"
			+",'"+idGoi+"'"
			+",'"+dsmay+"'"                           
			+",'"+chunhom1+"'"  
			+",'"+chunhom2+"'" 			
			+",'"+ConvertFont.UtoD(ghiChu)+"'"
			+",'"+sUserId+"'"		
			+",'"+sAgentCode+"'"
			+",'"+sDataSchema+"'"		
			+",'"+sIP+"'"	
			+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}

	public String huy_goi(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String idGoi
		, String Loaigoi
		, String ghiChu
	    , String sMay_cn		
  	) throws Exception {
		
     String s=    "begin ? := "+sFuncSchema+"pkg_regis_data.huy_goi("		
		+" '"+sDataSchema+"'"                                                             
		+",'"+sAgentCode+"'" 
		+",'"+sUserId+"'"  
		+",'"+sMay_cn+"'"
		+",'"+idGoi+"'"
		+",'"+Loaigoi+"'"
		+",'"+ConvertFont.UtoD(ghiChu)+"'"
		+");"
        +"end;"
        ;
		return this.reqValue("", s);
	}
	
	public String ghep_goi(
	    	  String sUserId
			, String sAgentCode
			, String sDataSchema
			, String FuncSchema
			, String sIP	
			, String loaiGoi
			, String idGoi_cu	
			, String idGoi_moi
			, String chunhom_cu
			, String chunhom_moi
			, String dsmay 	
			, String ghiChu 					
	  	) throws Exception {
	     String s=    "begin ? := "+FuncSchema+"pkg_regis_data.ghep_goi("
			+" '"+loaiGoi+"'"
			+",'"+idGoi_cu+"'"
			+",'"+idGoi_moi+"'"
			+",'"+dsmay+"'"                           
			+",'"+chunhom_cu+"'"  
			+",'"+chunhom_moi+"'" 			
			+",'"+ConvertFont.UtoD(ghiChu)+"'"
			+",'"+sUserId+"'"		
			+",'"+sAgentCode+"'"
			+",'"+sDataSchema+"'"		
			+",'"+sIP+"'"	
			+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
}














