package neo.pttb;
import javax.sql.RowSet;

import neo.ccbs_export;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class upload_khuyenmai extends NEOProcessEx {

	public NEOCommonData rec_upload_khuyenmai(String psUploadID) {
		String s = "begin ?:= admin_v2.pttb_chucnang.laytt_kiemtra_khuyenmai('" + psUploadID
				+ "'); end;";

		System.out.println(s);
		NEOExecInfo nei_ = new NEOExecInfo(s);
		nei_.setDataSrc("NEOPortalDBMS");
		return new NEOCommonData(nei_);
	}
	
	public NEOCommonData value_accept_upload_khuyenmai(String psUploadID, String use_schema, String com_schema, String psuserip, int isOverride){				
		String s=  "begin ?:= admin_v2.pttb_chucnang.accept_upload_khuyenmai("
			+"'"+ psUploadID+"',"
			+isOverride+","
			+"'"+use_schema+ "'," 
			+"'"+com_schema+ "'," 			
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";
			
	     NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("NEOPortalDBMS");
	 	return new NEOCommonData(nei_);
	}
	
	
	public NEOCommonData layds_upload_error(String uploadId, String agent){
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=pttb/upload_km/ds_upload_error&" + CesarCode.encode("uploadId") + "="+uploadId+"&agent="+agent;
		NEOExecInfo nei_ = new NEOExecInfo(url_);
 		nei_.setDataSrc("NEOPortalDBMS");
    	return new NEOCommonData(nei_);
	}	
		
	public String export_dsupload_error(String uploadId, String agent,String dataSchema, String comSchema) {	  
		  	  	
	    try {
	    	String s = "begin ?:=admin_v2.pttb_chucnang.layds_upload_error(" 
				+ "'" + uploadId + "'"
				+ ",'" + dataSchema + "'"
				+ ",'" + comSchema + "'"
				+ ",'" + agent + "'"				
				+ ");end;";
	    	
			RowSet rowSet = reqRSet("NEOPortalDBMS",s);
			
			String fileUploadDir = getSysConst("FileUploadDir");
			String ccs = getUserVar("sys_dataschema");
			ccs = ccs.replace(".", "");
			fileUploadDir = fileUploadDir + "Export_DSKM" + "\\" + ccs;
			String fileName = "dskm_upload_error.xls";
			
			Runtime.getRuntime().exec("cmd /C mkdir " + fileUploadDir);
			
			String fileOutput = fileUploadDir + "\\" + fileName;

			ccbs_export.exportToExcel(fileOutput, fileName, rowSet);
			
			String ret = "begin ?:='" + "{fileurl:\"" + fileOutput.replace("\\", "\\\\") + "\",filename:\"" + fileName +"\"}" + "'; end;";
			
			return ret;
		
	    } catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}	

}