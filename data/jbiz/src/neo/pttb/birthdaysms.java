package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;
import neo.qltn.ftp;
import neo.qltn.FTPupload;
import javax.sql.RowSet;
import java.io.*;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSetMetaData;

public class birthdaysms extends NEOProcessEx {
	public String new_birthday_group_cus(
		String pschema,
		String pgroup_name,
		String pstatus,
		String pgift_val,
		String pnote,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.new_birthday_group_cus("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(pgroup_name)+"',"
				+"'"+pstatus+"',"
				+"'"+pgift_val+"',"
				+"'"+ConvertFont.UtoD(pnote)+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_birthday_group_cus(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.del_birthday_group_cus("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_birthday_group_cus(
	String pschema,
		String pid,
		String pgroup_name,
		String pstatus,
		String pgift_val,
		String pnote,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.edit_birthday_group_cus("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pgroup_name)+"',"
				+"'"+pstatus+"',"
				+"'"+pgift_val+"',"
				+"'"+ConvertFont.UtoD(pnote)+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String new_birthday_estimate(
		String pschema,
		String pmsisdn,
		String pagent,
		String ptype_bill,
		String ptime_action,	
		String pmodify_date,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.new_birthday_estimate("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(pmsisdn)+"',"
				+"'"+ConvertFont.UtoD(pagent)+"',"
				+"'"+ptype_bill+"',"
				+"'"+ConvertFont.UtoD(ptime_action)+"',"			
				+"'"+pmodify_date+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_birthday_estimate(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.del_birthday_estimate("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_birthday_estimate(
	String pschema,
		String pid,
		String pmsisdn,
		String pagent,
		String ptype_bill,
		String ptime_action,
		String pmodify_date,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.edit_birthday_estimate("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pmsisdn)+"',"
				+"'"+ConvertFont.UtoD(pagent)+"',"
				+"'"+ptype_bill+"',"
				+"'"+ConvertFont.UtoD(ptime_action)+"',"			
				+"'"+pmodify_date+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String accept_upload_estimate(
		String puploadid,
		String puploadType,
		String pagent,		
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.accept_upload_estimate("
				+"'"+puploadid+"',"	
				+"'"+puploadType+"',"		
				+"'"+ConvertFont.UtoD(pagent)+"',"				
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String accept_upload_birthday(
		String puploadid,
		String puploadType,
		String pagent,		
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.accept_upload_birthday("
				+"'"+puploadid+"',"		
				+"'"+puploadType+"',"					
				+"'"+ConvertFont.UtoD(pagent)+"',"				
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String accept_upload_birthday_vtt(
		String puploadid,
		String puploadType,
		String pagent,		
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.accept_upload_birthday_vtt("
				+"'"+puploadid+"',"		
				+"'"+puploadType+"',"					
				+"'"+ConvertFont.UtoD(pagent)+"',"				
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String accept_upload_vtt_gift(
		String puploadid,
		String puploadType,
		String pagent,		
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.accept_upload_vtt_gift("
				+"'"+puploadid+"',"		
				+"'"+puploadType+"',"					
				+"'"+ConvertFont.UtoD(pagent)+"',"				
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	/*public String ftp_birthday_careplus(String area,
										String agentcode,
										String billfrom,
										String billto,
										String billrow,									
										String psmonth,
										String userid,
										String userip)throws Exception 
	{
		String s ="begin ? :=ADMIN_V2.pkg_birthday.get_data_careplus("
				+"'"+area+"',"		
				+"'"+agentcode+"',"					
				+"'"+billfrom+"',"				
				+"'"+billto+"',"
				+"'"+billrow+"',"
				+"'0',"
				+"'0',"
				+"'0',"
				+"'"+userid+"',"
				+"'"+userip+"'"
				+"); end;";
		RowSet rs = reqRSet("",s);
		String fileUploadDir = getSysConst("FileUploadDir");
		String ccs = getUserVar("sys_dataschema");
		ccs = ccs.replace(".", "");
		fileUploadDir = fileUploadDir + "Export_DSKM" + "\\" + ccs;
		String fileName = "cmsn_careplus_"+psmonth+"_"+userid+".txt";
		String file = fileUploadDir +"\\"+fileName;
		String destfile = "CMSN/" +fileName;
		exportToTextFile(file, rs);
		new FTPupload("190.10.10.86:8181", "luattd", "123456", file , destfile);
		String str ="begin ? :=ADMIN_V2.pkg_birthday.set_data_careplus("
				+"'"+area+"',"		
				+"'"+agentcode+"',"					
				+"'"+billfrom+"',"				
				+"'"+billto+"',"
				+"'"+billrow+"',"		
				+"'"+userid+"',"
				+"'"+userip+"'"
				+"); end;";
		return this.reqValue("", str);
	}*/
	
	public String ftp_birthday_careplus(String area,
										String agentcode,
										String cusList,								
										String psmonth,
										String userid,
										String userip)throws Exception 
	{
		String s ="begin ? :=ADMIN_V2.pkg_birthday.get_data_careplus("
				+"'"+area+"',"		
				+"'"+agentcode+"',"					
				+"'"+cusList+"',"							
				+"'0',"
				+"'0',"
				+"'0',"
				+"'"+userid+"',"
				+"'"+userip+"'"
				+"); end;";
		RowSet rs = reqRSet("",s);
		String fileUploadDir = getSysConst("FileUploadDir");
		String ccs = getUserVar("sys_dataschema");
		ccs = ccs.replace(".", "");
		fileUploadDir = fileUploadDir + "Export_DSKM" + "\\" + ccs;
		String fileName = "cmsn_careplus_"+psmonth+"_"+userid+".txt";
		String file = fileUploadDir +"\\"+fileName;
		String destfile = "CMSN/" +fileName;
		exportToTextFile(file, rs);
		new FTPupload("190.10.10.86:8181", "luattd", "123456", file , destfile);
		String str ="begin ? :=ADMIN_V2.pkg_birthday.set_data_careplus("
				+"'"+area+"',"		
				+"'"+agentcode+"',"					
				+"'"+cusList+"',"		
				+"'"+userid+"',"
				+"'"+userip+"'"
				+"); end;";
		return this.reqValue("", str);
	}
	public static void exportToTextFile(String outputFile, RowSet rowSet) throws Exception {
		File file = new File(outputFile);

		if (file.exists()) {
			file.delete();
		}
	//	FileOutputStream fos = new FileOutputStream(file, true);
		FileWriter fw = new FileWriter(file);  
        BufferedWriter bw = new BufferedWriter(fw);  
		
		ResultSetMetaData rsMeta = rowSet.getMetaData();
		int nColumn = rsMeta.getColumnCount();		
		int nRow = 0;
		rowSet.beforeFirst();
		int count = 0;
		try {
			while (rowSet.next()) {
			count++;
			String tmp="";
			for (int j = 0; j < nColumn; j++) {
				tmp = tmp + rowSet.getString(j+1)+",";				
			} 
			tmp = tmp.substring(0,tmp.length() -1);
			System.out.print(tmp);
			bw.write(tmp);			
			bw.newLine();
		}
		}
		finally {
			bw.flush();	
			bw.close();
			fw.close();
		
		}
	}
	public String new_birthday_config(
		String pschema,
		String pconfig_name,
		String pdate_from,
		String pdate_to,
		String prole_list,
		String pstatus,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.new_birthday_config("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(pconfig_name)+"',"
				+"'"+pdate_from+"',"
				+"'"+pdate_to+"',"
				+"'"+ConvertFont.UtoD(prole_list)+"',"
				+"'"+pstatus+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_birthday_config(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.del_birthday_config("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_birthday_config(
	String pschema,
		String pid,
		String pconfig_name,
		String pdate_from,
		String pdate_to,
		String prole_list,
		String pstatus,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.edit_birthday_config("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pconfig_name)+"',"
				+"'"+pdate_from+"',"
				+"'"+pdate_to+"',"
				+"'"+ConvertFont.UtoD(prole_list)+"',"
				+"'"+pstatus+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
						
	public String check_upload_birthday_vtt(
		String puploadid) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday_2.check_birthday_vtt("
				+"'"+puploadid+"'"
				+"); end;";		
		return this.reqValue("", s);
	}	

public String check_upload_birthday_vtt_gift(
		String puploadid) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday_2.check_birthday_vtt_gift("
				+"'"+puploadid+"'"
				+"); end;";
				return this.reqValue("", s);
	}	
}