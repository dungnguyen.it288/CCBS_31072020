package neo;

import neo.smartui.process.NEOProcessEx;

import neo.NEOConvert.HSSF.*;
import java.io.*;
public class ExcellConvert extends NEOProcessEx 
{
	String SERVERNAME = "10.27.30.11";
  	String SID = "neo";
  	String PORT = "1521";
  	String PASSWORD = "a";
	public String test()
	  {
	  	return "begin ?:= DEBUG_SLA_ADMIN.RETURNFUNCS.StringReturnFunc('abc!'); end;";
	  }
	public String OracleToExcell(String varUser,String varSchema,String varTableName,String varFileName)
		throws IOException,java.sql.SQLException
	{
		OracTableToExcell obj = new OracTableToExcell();
		obj.setServerName(SERVERNAME);
		obj.setPort(PORT);
		obj.setSID(SID);
		obj.setUser(varUser.substring(0,varUser.length()-1));		
		obj.setSchema(varSchema);
		obj.setPassword(PASSWORD);
		obj.setTableName(varTableName);
		obj.setFileName(varFileName);
		obj.setSelsql("select rownum rn,dm.* from "+varSchema+"."+varTableName+" dm order by don_vi,ma_cq");

		String str = "begin ?:= " + varUser + "RETURNFUNCS.StringReturnFunc('" + varFileName+":"+obj.run() + "'); end;";

		return str;
	}
}
