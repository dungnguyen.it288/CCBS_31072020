package neo.pttb;
import neo.smartui.common.*;
import neo.smartui.process.*;

import java.util.*;
import javax.sql.*;
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import neo.smartui.common.CesarCode;
import neo.smartui.report.*;
import com.svcon.jdbf.*;
import javax.sql.RowSet;
import sun.misc.*;
public class RegVcc extends NEOProcessEx {
    
	public String add_vcc (	 
			String pc_sotb,
			String pc_goi			 ,
			String pc_packageid			 ,
			String pc_tencoquan      ,
			String pc_nguoidaidien   ,
			String pc_sdt_lienhe     ,
			String pc_diachi         ,
			String pc_fax            ,
			String pc_website        ,
			String pc_email        ,
			String pc_ma_so_thue     ,
			String pc_giay_dkkd      ,
			String l_ngaycap_dkkd    ,
			String pc_noicap         ,
			String pc_nganhang_id    ,
			String pc_tai_khoan      ,
			String pc_diachitt       ,
			String pc_hinhthuctt     ,
			String pc_kieubaocuoc    ,
			String pc_dia_chi_ld     ,
			String pc_loaikh_id      ,
			String pc_amdmin_ten     ,
			String pc_amdmin_sdt     ,
			String pc_amdmin_email   ,
			String pc_amdmin_chucdanh,
			String pc_USERNUMBER ,
			String pc_EXTNUMBER ,
			String pc_softphone ,
			String pc_nghenghiep,
			String pc_ghi_chu         ,
			String pc_loai            ,
			String pc_tileCuocKT      , 
			String pc_tileCuocUser    , //cuoc userr
			String pc_tileCuocmayLe      ,	
			String pc_tileSoftphone   ,
			String pc_tileCuocChinh    ,					
			String pc_tilecuocnm      , 
			String pc_tilecuocngm    , 			
			String pc_userid           ,
			String pc_userip           ,
			String pc_agentcode        )
	throws Exception {
		String result =null;
		String loai ="";
		String custid ="";
		// Kiem tra xem la dky hay update:
		
		String s = " begin ? := admin_v2.pk_subscriber_vcc.get_cusid("
								+ "'" + pc_sotb + "'"
								+ ",'"+pc_userid   +"'" 
								+ ",'"+pc_userip   +"'" 
								+ ",'"+pc_agentcode  +"'" 
								+ ");"
								+ "end;";
		System.out.println(s);
        result = this.reqValue("", s);
		
		System.out.println("-----THUYVTTT >>>>>>>>>>>>>>>>>>>>>>>>>Ket qua kiem tra result---->>>>>>> vcc ----------------= "+result);
		System.out.println("--------------->>>Ket qua kiem tra ret---->>>>>>> vcc ----------------= "+result.indexOf("|"));
		String ret =result.substring(0,result.indexOf("|")+1).trim();
		System.out.println("--------------->>>Ket qua kiem tra ret---->>>>>>> vcc ----------------= "+ret);
		if (ret.indexOf("1|") >=0 ||ret.indexOf("2|") >=0) {
			if (ret.indexOf("1|")>=0) {
				loai = "1";	
			} else {
				loai ="3";
			}
			
		    custid = result.substring(result.indexOf("|")+1);
			
			System.out.println("--------------->>>custid---->>>>>>> vcc ----------------= "+custid);
			
			System.out.println("--------------->>>Goi dang ky VCC-------------------- --------------------------------------------");
			try {
				String body ="";			
				Date dNow = new Date( );
				SimpleDateFormat ft = 
				new SimpleDateFormat ("yyyy:MM:dd hh:mm:ss");
				String TimeStamp = ft.format(dNow);			
				
				String api_rs ="";
				String resultCode ="";
				String resultMsg ="";					 	
							
				body = "?TimeStamp="+TimeStamp+"&ActionType="+loai+"&FeeNumber="+pc_sotb+"&CustID="+custid+"&CustName="+ConvertFont.decodeW1252(pc_tencoquan)
						+"&CustAddress="+ConvertFont.decodeW1252(pc_diachi)
						+"&CustManager="+ConvertFont.decodeW1252(pc_amdmin_ten)
						+"&Position="+ConvertFont.decodeW1252(pc_amdmin_chucdanh)
						+"&LicenseNo="+ConvertFont.decodeW1252(pc_giay_dkkd)
						+"&DateOfIssue="+l_ngaycap_dkkd
						+"&IssuedBy="+ConvertFont.decodeW1252(pc_noicap)
						+"&TaxCode="+pc_ma_so_thue +"&CustTelNum="+pc_sotb
						+"&CustEmail="+ ConvertFont.decodeW1252(pc_email)
						+"&AdminTel="+pc_amdmin_sdt 
						+"&AdminEmail="+ConvertFont.decodeW1252(pc_amdmin_email) 
						+"&PackageName="+ pc_goi+"&UserNumber="+pc_USERNUMBER+"&ExtNumber="+pc_EXTNUMBER+"&AppNumber="+pc_softphone; 			
				
				System.out.println("--------------->>>body = "+body);	
				api_rs = apiPost(body);
				System.out.println("--------------->>>api_rs = "+api_rs);
				 
				try { resultCode = getValue(api_rs,"resultCode"); } 
					catch (Exception ex){resultCode = "";}
				try { resultMsg = getValue(api_rs,"resultMsg"); } 
					catch (Exception ex){
						ex.printStackTrace();
						System.out.println("--------------->>> api_rs resultMsg loi vcc= "+resultMsg+"-"+ex);
						resultMsg = "";}
				System.out.println("--------------->>> api_rs resultMsg vcc = "+resultMsg);
				System.out.println("--------------->>> api_rs resultCode vcc = "+resultCode);
				
				if(resultCode.equals("00000"))
				{	
					s = " begin ? := admin_v2.pk_subscriber_vcc.sf_regis_vcc("
						+ "'" + custid + "'"
						+ ",'" + pc_sotb + "'"
						+ ",'"+ pc_goi + "'"
						+ ",'"+ pc_packageid + "'"			
						+ ",'"+ ConvertFont.UtoD(pc_tencoquan) + "'" 
						+ ",'"+ConvertFont.UtoD(pc_nguoidaidien) +"'" 
						+ ",'"+pc_sdt_lienhe +"'" 
						+ ",'"+ConvertFont.UtoD(pc_diachi)  +"'" 
						+ ",'"+pc_fax   +"'" 
						+ ",'"+pc_website   +"'"
						+ ",'"+ConvertFont.UtoD(pc_email)  +"'"				
						+ ",'"+pc_ma_so_thue +"'" 
						+ ",'"+ConvertFont.UtoD(pc_giay_dkkd)  +"'" 
						+ ",'"+l_ngaycap_dkkd +"'" 
						+ ",'"+ConvertFont.UtoD(pc_noicap )  +"'" 
						+ ",'"+pc_nganhang_id +"'" 
						+ ",'"+pc_tai_khoan  +"'" 
						+ ",'"+ConvertFont.UtoD(pc_diachitt ) +"'" 
						+ ",'"+pc_hinhthuctt +"'" 
						+ ",'"+pc_kieubaocuoc +"'" 
						+ ",'"+ConvertFont.UtoD(pc_dia_chi_ld) +"'" 
						+ ",'"+pc_loaikh_id  +"'" 
						+ ",'"+ConvertFont.UtoD(pc_amdmin_ten) +"'" 
						+ ",'"+pc_amdmin_sdt +"'" 
						+ ",'"+ConvertFont.UtoD(pc_amdmin_email)  +"'" 
						+ ",'"+ConvertFont.UtoD(pc_amdmin_chucdanh) +"'" 
						+ ",'"+pc_USERNUMBER +"'" 
						+ ",'"+pc_EXTNUMBER  +"'" 
						+ ",'"+pc_softphone  +"'" 
						+ ",'"+ConvertFont.UtoD(pc_nghenghiep)  +"'"
						+ ",'"+ConvertFont.UtoD(pc_ghi_chu)+"'" 
						+ ",'"+pc_loai  +"'" 
						+ ",'"+pc_tileCuocKT  +"'" 
						+ ",'"+pc_tileCuocUser  +"'" 
						+ ",'"+pc_tileCuocmayLe  +"'" 
						+ ",'"+pc_tileCuocChinh+"'"
						+ ",'"+pc_tileSoftphone+"'"				
						+ ",'"+pc_tilecuocnm  +"'" 
						+ ",'"+pc_tilecuocngm  +"'" 
						+ ",'"+pc_userid   +"'" 
						+ ",'"+pc_userip   +"'" 
						+ ",'"+pc_agentcode  +"'" 
						+ ");"
						+ "end;";
					System.out.println(s);
					result = this.reqValue("", s);
					System.out.println("--------------->>>result = ....dang ky tu OCS DB="+result);
					if(result.equals("1")){						
						String sx_="";			 
						sx_ ="begin ? :=admin_v2.pk_subscriber_vcc.capnhat_api('"+pc_sotb+"',1); end;";
						this.reqValue("", sx_);	
					} else {
						if (loai.equals("1")) {
							//Huy VCC 
							//sleep 1 phut de he thong vcc dong bo
							Thread.sleep(10000);
							TimeStamp = ft.format(dNow);	
							String body_del ="{\"TimeStamp\":\""+TimeStamp+"\",\"ActionType\":\"2\",\"FeeNumber\":\""+pc_sotb+"\",\"CustID\":\""+custid+"\",\"CustTelNum\":\""+pc_sotb+"\"}";
							System.out.println(body_del);
							String api_del = apiJsonPost(body_del);
							System.out.println(api_del);
							TimeStamp = ft.format(dNow);
							body_del ="{\"TimeStamp\":\""+TimeStamp+"\",\"ActionType\":\"7\",\"FeeNumber\":\""+pc_sotb+"\",\"CustID\":\""+custid+"\",\"CustTelNum\":\""+pc_sotb+"\"}";
							
							api_del = apiJsonPost(body_del);	
							
							return result;
						}
						
					}
				}
				else{				 
					result = resultMsg;
				}			
			}catch (Exception ex){				
				System.out.println("Exception 2 : " + ex.getMessage());
				ex.printStackTrace();
			}	
		
		} else {			
			return result;
		}

		System.out.println("--------------->>>Ket qua......................dang ky VCC-------------------> = "+result);
		return result;
    } 
	
	public String add_vcc_thuy (	 
			String pc_sotb,
			String pc_goi			 ,
			String pc_packageid			 ,
			String pc_tencoquan      ,
			String pc_nguoidaidien   ,
			String pc_sdt_lienhe     ,
			String pc_diachi         ,
			String pc_fax            ,
			String pc_website        ,
			String pc_email        ,
			String pc_ma_so_thue     ,
			String pc_giay_dkkd      ,
			String l_ngaycap_dkkd    ,
			String pc_noicap         ,
			String pc_nganhang_id    ,
			String pc_tai_khoan      ,
			String pc_diachitt       ,
			String pc_hinhthuctt     ,
			String pc_kieubaocuoc    ,
			String pc_dia_chi_ld     ,
			String pc_loaikh_id      ,
			String pc_amdmin_ten     ,
			String pc_amdmin_sdt     ,
			String pc_amdmin_email   ,
			String pc_amdmin_chucdanh,
			String pc_USERNUMBER ,
			String pc_EXTNUMBER ,
			String pc_softphone ,
			String pc_nghenghiep,
			String pc_ghi_chu         ,
			String pc_loai            ,
			String pc_tileCuocKT      , 
			String pc_tileCuocUser    , //cuoc userr
			String pc_tileCuocmayLe      ,	
			String pc_tileSoftphone   ,
			String pc_tileCuocChinh    ,					
			String pc_tilecuocnm      , 
			String pc_tilecuocngm    , 			
			String pc_userid           ,
			String pc_userip           ,
			String pc_agentcode        )
	throws Exception {
		String result =null;
		String loai ="";
		//String custid ="";
		// Kiem tra xem la dky hay update:
		RowSet count = this.reqRSet("SELECT count(1) count_ FROM admin_v2.subscriber_vcc where so_tb ='"+pc_sotb+"' and vcc_status is not null ");
			while (count.next()){
				int count_ = count.getInt("count_");
				System.out.println("--------------->>>count_ vcc ----------------= "+count_);
				// Them moi:
				if(count_ < 1){
					loai = "1";										
				}
				//Cap nhat
				else{ loai ="3"; 				
				}				
					
				}				
		
		String s = " begin ? := admin_v2.pk_subscriber_vcc.sf_regis_vcc("
                + "'" + pc_sotb + "'"
				+ ",'"+ pc_goi + "'"
				+ ",'"+ pc_packageid + "'"			
                + ",'"+ ConvertFont.UtoD(pc_tencoquan) + "'" 
                + ",'"+ConvertFont.UtoD(pc_nguoidaidien) +"'" 
				+ ",'"+pc_sdt_lienhe +"'" 
				+ ",'"+ConvertFont.UtoD(pc_diachi)  +"'" 
				+ ",'"+pc_fax   +"'" 
				+ ",'"+pc_website   +"'"
				+ ",'"+ConvertFont.UtoD(pc_email)  +"'"				
				+ ",'"+pc_ma_so_thue +"'" 
				+ ",'"+ConvertFont.UtoD(pc_giay_dkkd)  +"'" 
				+ ",'"+l_ngaycap_dkkd +"'" 
				+ ",'"+ConvertFont.UtoD(pc_noicap )  +"'" 
				+ ",'"+pc_nganhang_id +"'" 
				+ ",'"+pc_tai_khoan  +"'" 
				+ ",'"+ConvertFont.UtoD(pc_diachitt ) +"'" 
				+ ",'"+pc_hinhthuctt +"'" 
				+ ",'"+pc_kieubaocuoc +"'" 
				+ ",'"+ConvertFont.UtoD(pc_dia_chi_ld) +"'" 
				+ ",'"+pc_loaikh_id  +"'" 
				+ ",'"+ConvertFont.UtoD(pc_amdmin_ten) +"'" 
				+ ",'"+pc_amdmin_sdt +"'" 
				+ ",'"+ConvertFont.UtoD(pc_amdmin_email)  +"'" 
				+ ",'"+ConvertFont.UtoD(pc_amdmin_chucdanh) +"'" 
				+ ",'"+pc_USERNUMBER +"'" 
				+ ",'"+pc_EXTNUMBER  +"'" 
				+ ",'"+pc_softphone  +"'" 
				+ ",'"+ConvertFont.UtoD(pc_nghenghiep)  +"'"
				+ ",'"+ConvertFont.UtoD(pc_ghi_chu)+"'" 
				+ ",'"+pc_loai  +"'" 
				+ ",'"+pc_tileCuocKT  +"'" 
				+ ",'"+pc_tileCuocUser  +"'" 
				+ ",'"+pc_tileCuocmayLe  +"'" 
				+ ",'"+pc_tileCuocChinh+"'"
				+ ",'"+pc_tileSoftphone+"'"				
				+ ",'"+pc_tilecuocnm  +"'" 
				+ ",'"+pc_tilecuocngm  +"'" 
				+ ",'"+pc_userid   +"'" 
				+ ",'"+pc_userip   +"'" 
				+ ",'"+pc_agentcode  +"'" 
                + ");"
                + "end;";
		System.out.println(s);
        result = this.reqValue("", s);
		System.out.println("--------------->>>result = ....dang ky tu OCS DB="+result);
		if(result.equals("1")){
		try {
			String body ="";			
			Date dNow = new Date( );
			SimpleDateFormat ft = 
			new SimpleDateFormat ("yyyy:MM:dd hh:mm:ss");
			String TimeStamp = ft.format(dNow);			
			
			String api_rs ="";
			String resultCode ="";
			String resultMsg ="";
				RowSet rs_ = this.reqRSet("SELECT custid FROM admin_v2.subscriber_vcc where so_tb ="+pc_sotb);
				System.out.println("--------------->>>rs_ lay cusid cua VCC = "+rs_);
				String custid ="";
			while (rs_.next()){
				custid = rs_.getString("custid");
				System.out.println("--------------->>>custid = "+custid);
			}			
			 			
			body = "?TimeStamp="+TimeStamp+"&ActionType="+loai+"&FeeNumber="+pc_sotb+"&CustID="+custid+"&CustName="+ConvertFont.decodeW1252(pc_tencoquan)
					+"&CustAddress="+ConvertFont.decodeW1252(pc_diachi)
					+"&CustManager="+ConvertFont.decodeW1252(pc_amdmin_ten)
					+"&Position="+ConvertFont.decodeW1252(pc_amdmin_chucdanh)
					+"&LicenseNo="+ConvertFont.decodeW1252(pc_giay_dkkd)
					+"&DateOfIssue="+l_ngaycap_dkkd
					+"&IssuedBy="+ConvertFont.decodeW1252(pc_noicap)
					+"&TaxCode="+pc_ma_so_thue +"&CustTelNum="+pc_sotb
					+"&CustEmail="+ ConvertFont.decodeW1252(pc_email)
					+"&AdminTel="+pc_amdmin_sdt 
					+"&AdminEmail="+ConvertFont.decodeW1252(pc_amdmin_email) 
					+"&PackageName="+ pc_goi+"&UserNumber="+pc_USERNUMBER+"&ExtNumber="+pc_EXTNUMBER+"&AppNumber="+pc_softphone; 			
		 	
			System.out.println("--------------->>>body = "+body);	
		    api_rs = apiPost(body);
			System.out.println("--------------->>>api_rs = "+api_rs);
			 
			try { resultCode = getValue(api_rs,"resultCode"); } 
				catch (Exception ex){resultCode = "";}
			try { resultMsg = getValue(api_rs,"resultMsg"); } 
				catch (Exception ex){
					ex.printStackTrace();
					System.out.println("--------------->>> api_rs resultMsg loi vcc= "+resultMsg+"-"+ex);
					resultMsg = "";}
			System.out.println("--------------->>> api_rs resultMsg vcc = "+resultMsg);
			System.out.println("--------------->>> api_rs resultCode vcc = "+resultCode);
			String sx_="";
			if(resultCode.equals("00000"))
				{
				sx_ ="begin ? :=admin_v2.pk_subscriber_vcc.capnhat_api('"+pc_sotb+"',1); end;";
				this.reqValue("", sx_);	
				}
			else{
				//sx_ ="begin ? :=admin_v2.pk_subscriber_vcc.capnhat_api('"+pc_sotb+"',0); end;";
				//this.reqValue("", sx_);
				if (loai.equals("1")){
					String s1 = "begin ? := admin_v2.pk_subscriber_vcc.sf_delete_vcc("
					+ "'" + pc_sotb + "'"
					+ ",'" + getUserVar("userID") + "'"
					+ ",'" + getUserVar("userIP") + "'" //UserIP
					+ ",'" + pc_agentcode + "'" 
					+ ");"
					+ "end;";
					result = this.reqValue("", s1);
				}
				result = resultMsg;
			}			
			}catch (Exception ex){				
				System.out.println("Exception 2 : " + ex.getMessage());
			ex.printStackTrace();
			}	
		
		}	

		System.out.println("--------------->>>resultMsg dang ky VCC-------------------> = "+result);
		return result;
    } 
	
	
	public String add_vcc_bk(	 
			String pc_sotb,
			String pc_goi			 ,
			String pc_packageid			 ,
			String pc_tencoquan      ,
			String pc_nguoidaidien   ,
			String pc_sdt_lienhe     ,
			String pc_diachi         ,
			String pc_fax            ,
			String pc_website        ,
			String pc_email        ,
			String pc_ma_so_thue     ,
			String pc_giay_dkkd      ,
			String l_ngaycap_dkkd    ,
			String pc_noicap         ,
			String pc_nganhang_id    ,
			String pc_tai_khoan      ,
			String pc_diachitt       ,
			String pc_hinhthuctt     ,
			String pc_kieubaocuoc    ,
			String pc_dia_chi_ld     ,
			String pc_loaikh_id      ,
			String pc_amdmin_ten     ,
			String pc_amdmin_sdt     ,
			String pc_amdmin_email   ,
			String pc_amdmin_chucdanh,
			String pc_USERNUMBER ,
			String pc_EXTNUMBER ,
			String pc_softphone ,
			String pc_nghenghiep,
			String pc_ghi_chu         ,
			String pc_loai            , 
			String pc_userid           ,
			String pc_userip           ,
			String pc_agentcode        )
	throws Exception {
		String result =null;
		String loai ="";
		String custid ="";
		// Kiem tra xem la dky hay update:
		RowSet count = this.reqRSet("SELECT count(1) count_ FROM admin_v2.subscriber_vcc where so_tb ="+pc_sotb);
			while (count.next()){
				int count_ = count.getInt("count_");
				System.out.println("--------------->>>count_ = "+count_);
				// Them moi:
				if(count_ < 1){
					loai = "1";
					String sql_ ="begin ? :=ccs_common.lay_giatri_sequence ('seq_cusid'); end;";
				custid = this.reqValue("", sql_);						
				}
				//Cap nhat
				else{ 
				loai ="3"; 
				RowSet rs_ = this.reqRSet("SELECT custid FROM admin_v2.subscriber_vcc where so_tb ="+pc_sotb);
				System.out.println("--------------->>>rs_ = "+rs_);				
				while (rs_.next()){
				custid = rs_.getString("custid");
				System.out.println("--------------->>>custid = "+custid);
				}
					
				}
				
			}
		 String body ="";			
			Date dNow = new Date( );
			SimpleDateFormat ft = 
			new SimpleDateFormat ("yyyy:MM:dd hh:mm:ss");
			String TimeStamp = ft.format(dNow);	
			String api_rs ="";
			String resultCode ="";
			String resultMsg ="";
				/*
				RowSet rs_ = this.reqRSet("SELECT custid FROM admin_v2.subscriber_vcc where so_tb ="+pc_sotb);
				System.out.println("--------------->>>rs_ = "+rs_);
				String custid ="";
			while (rs_.next()){
				custid = rs_.getString("custid");
				System.out.println("--------------->>>custid = "+custid);
				
			}
			*/			
			body = "?TimeStamp="+TimeStamp+"&ActionType="+loai+"&FeeNumber="+pc_sotb+"&CustID="+custid+"&CustName="+pc_nguoidaidien+"&CustAddress="+pc_diachi+"&CustManager="+pc_amdmin_ten+"&Position="+pc_amdmin_chucdanh+"&LicenseNo="+pc_giay_dkkd+"&DateOfIssue="+l_ngaycap_dkkd+"&IssuedBy="+pc_noicap+"&TaxCode="+pc_ma_so_thue +"&CustTelNum="+pc_sotb+"&CustEmail="+pc_email+"&AdminTel="+pc_amdmin_sdt +"&AdminEmail="+pc_amdmin_email +"&PackageName="+ pc_goi+"&UserNumber="+pc_USERNUMBER+"&ExtNumber="+pc_EXTNUMBER+"&AppNumber="+pc_softphone; 			
			System.out.println("--------------->>>body = "+body);			
		    api_rs = apiPost(body);
			System.out.println("--------------->>>api_rs = "+api_rs);
			/*
			String resultCode = getValue(api_rs,"resultCode");
			System.out.println("--------------->>>resultCode = "+resultCode);
			String resultMsg = getValue(api_rs,"resultMsg");
			System.out.println("--------------->>>resultMsg = "+resultMsg);
			*/
			try { resultCode = getValue(api_rs,"resultCode"); } 
				catch (Exception ex){resultCode = "";}
			try { resultMsg = getValue(api_rs,"resultMsg"); } 
				catch (Exception ex){resultMsg = "";}
			System.out.println("--------------->>>resultCode = "+resultCode);
			System.out.println("--------------->>>resultMsg = "+resultMsg);			
			if(resultCode.equals("00000"))
				{
				try {
			String s = " begin ? := admin_v2.pk_subscriber_vcc.sf_regis_vcc("
                + "'" + pc_sotb + "'"
				+ ",'"+ pc_goi + "'"
				+ ",'"+ pc_packageid + "'"			
                + ",'"+ ConvertFont.UtoD(pc_tencoquan) + "'" 
                + ",'"+ConvertFont.UtoD(pc_nguoidaidien) +"'" 
				+ ",'"+pc_sdt_lienhe +"'" 
				+ ",'"+ConvertFont.UtoD(pc_diachi)  +"'" 
				+ ",'"+pc_fax   +"'" 
				+ ",'"+pc_website   +"'"
				+ ",'"+ConvertFont.UtoD(pc_email)  +"'"				
				+ ",'"+pc_ma_so_thue +"'" 
				+ ",'"+ConvertFont.UtoD(pc_giay_dkkd)  +"'" 
				+ ",'"+l_ngaycap_dkkd +"'" 
				+ ",'"+ConvertFont.UtoD(pc_noicap )  +"'" 
				+ ",'"+pc_nganhang_id +"'" 
				+ ",'"+pc_tai_khoan  +"'" 
				+ ",'"+ConvertFont.UtoD(pc_diachitt ) +"'" 
				+ ",'"+pc_hinhthuctt +"'" 
				+ ",'"+pc_kieubaocuoc +"'" 
				+ ",'"+ConvertFont.UtoD(pc_dia_chi_ld) +"'" 
				+ ",'"+pc_loaikh_id  +"'" 
				+ ",'"+ConvertFont.UtoD(pc_amdmin_ten) +"'" 
				+ ",'"+pc_amdmin_sdt +"'" 
				+ ",'"+ConvertFont.UtoD(pc_amdmin_email)  +"'" 
				+ ",'"+ConvertFont.UtoD(pc_amdmin_chucdanh) +"'" 
				+ ",'"+pc_USERNUMBER +"'" 
				+ ",'"+pc_EXTNUMBER  +"'" 
				+ ",'"+pc_softphone  +"'" 
				+ ",'"+ConvertFont.UtoD(pc_nghenghiep)  +"'"
				+ ",'"+ConvertFont.UtoD(pc_ghi_chu)+"'" 
				+ ",'"+pc_loai  +"'" 
				+ ",'"+pc_userid   +"'" 
				+ ",'"+pc_userip   +"'" 
				+ ",'"+pc_agentcode  +"'"
				+ ",'"+custid  +"'" 
                + ");"
                + "end;";
				System.out.println(s);
				result = this.reqValue("", s);
				System.out.println("--------------->>>result = "+result);				
			}catch (Exception ex){				
				System.out.println("Exception 2 : " + ex.getMessage());
			ex.printStackTrace();
			}	
		
			}	
				//sx_ ="begin ? :=admin_v2.pk_subscriber_vcc.capnhat_api('"+pc_sotb+"',1); end;";
				//this.reqValue("", sx_);					
			else{
				//sx_ ="begin ? :=admin_v2.pk_subscriber_vcc.capnhat_api('"+pc_sotb+"',0); end;";
				//this.reqValue("", sx_);
				result = resultMsg;
			}	
					
		return result;
    } 
	
		
	// Cap nhat goi thanh vien:
	public String Update_package_vcc ( String MSISDN,
							String package_id,
							String doigiamgianm,
							String ngoaimang_check,
							String giamgianm,
							String giamgiangm,
							String UserID,
							String agent_code,
							String UserIP) 
					throws Exception{ 
        String result = "";		
        String s = "begin ? := admin_v2.pk_subscriber_vcc.update_package_vcc("
                + "'" + MSISDN + "'"
				+ ",'" + package_id + "'" 
				+ ",'" + doigiamgianm + "'"
				+ ",'" + ngoaimang_check + "'"
				+ ",'" + giamgianm + "'"
				+ ",'" + giamgiangm + "'"				
                + ",'" + UserID + "'" 
				+ ",'" + agent_code + "'" 
				+ ",'" + UserIP + "'" 
                + ");"
                + "end;";
        System.out.println(s);
		result = this.reqValue("", s);		
		return result;
    } 
	// Xoa goi theo ID:
	public String Del_package_vcc ( String psID,
							String package_id,
							String msisdn,
							String UserID,
							String agent_code,
							String UserIP) 
					throws Exception{ 
        String result = "";		
        String s = "begin ? := admin_v2.pk_subscriber_vcc.huy_goi_theo_id("
                + "'" + psID + "'"
				+ ",'" + package_id + "'"
				+ ",'" + msisdn + "'"
                + ",'" + UserID + "'" 
				+ ",'" + agent_code + "'" 
				+ ",'" + UserIP + "'" 
                + ");"
                + "end;";
        System.out.println(s);
		result = this.reqValue("", s);		
		return result;
    } 
	//tam ngung dich vu VCC
	public String deactive_vcc ( String MSISDN,
							String matinh,
							String UserID,
							String UserIP ) 
					throws Exception{ 
        String result = "";
		String custid ="";
		RowSet rs_ = this.reqRSet("SELECT custid FROM admin_v2.subscriber_vcc where so_tb ="+MSISDN);
				System.out.println("--------------->>>rs_ = "+rs_);				
			while (rs_.next()){
				custid = rs_.getString("custid");
				System.out.println("--------------->>>custid = "+custid);
			}
        String s = "begin ? := admin_v2.pk_subscriber_vcc.sf_deactive_vcc("
                + "'" + MSISDN + "'"
				+ ",'" + getUserVar("userID") + "'"
                + ",'" + UserIP + "'"
                + ",'" + matinh + "'" 
                + ");"
                + "end;";
        System.out.println(s);
		result = this.reqValue("", s);
		if(result.equals("1")){
		try {
			Date dNow = new Date( );
			SimpleDateFormat ft = 
			new SimpleDateFormat ("yyyy:MM:dd hh:mm:ss");
			String TimeStamp = ft.format(dNow);
			//String body_del ="\"TimeStamp\":\""+TimeStamp+"\",\"ActionType\":\"2\",\"FeeNumber\":\""+MSISDN+"\",\"CustID\":\""+custid+"\",\"CustTelNum\":\""+MSISDN+"\"";
			String body_del ="?TimeStamp="+TimeStamp+"&ActionType=4&FeeNumber="+MSISDN+"&CustID="+custid+"&CustTelNum="+MSISDN;
			 
			String api_del = apiPost(body_del);			
			}catch (Exception ex){				
				System.out.println("Exception 2 : " + ex.getMessage());
			ex.printStackTrace();
			}
		}
		return result;
    } 
	//active dich vu
	public String active_vcc ( String MSISDN,
							String matinh,
							String UserID,
							String UserIP ) 
					throws Exception{ 
        String result = "";
		String custid ="";
		RowSet rs_ = this.reqRSet("SELECT custid FROM admin_v2.subscriber_vcc where so_tb ="+MSISDN);
				System.out.println("--------------->>>rs_ = "+rs_);				
			while (rs_.next()){
				custid = rs_.getString("custid");
				System.out.println("--------------->>>custid = "+custid);
			}
        String s = "begin ? := admin_v2.pk_subscriber_vcc.sf_active_vcc("
                + "'" + MSISDN + "'"
				+ ",'" + getUserVar("userID") + "'"
                + ",'" + UserIP + "'"
                + ",'" + matinh + "'" 
                + ");"
                + "end;";
        System.out.println(s);
		result = this.reqValue("", s);
		if(result.equals("1")){
		try {
			Date dNow = new Date( );
			SimpleDateFormat ft = 
			new SimpleDateFormat ("yyyy:MM:dd hh:mm:ss");
			String TimeStamp = ft.format(dNow);
			//String body_del ="\"TimeStamp\":\""+TimeStamp+"\",\"ActionType\":\"2\",\"FeeNumber\":\""+MSISDN+"\",\"CustID\":\""+custid+"\",\"CustTelNum\":\""+MSISDN+"\"";
			String body_del ="?TimeStamp="+TimeStamp+"&ActionType=5&FeeNumber="+MSISDN+"&CustID="+custid+"&CustTelNum="+MSISDN;
			String api_del = apiPost(body_del);			
			}catch (Exception ex){				
				System.out.println("Exception 2 : " + ex.getMessage());
			ex.printStackTrace();
			}
		}
		return result;
    }
	//Huy dich vu	
	public String Del_vcc ( String MSISDN,
							String matinh,
							String UserIP ) 
					throws Exception{ 
        String result = "";
		String custid ="";
		RowSet rs_ = this.reqRSet("SELECT custid FROM admin_v2.subscriber_vcc where so_tb ="+MSISDN);
				System.out.println("--------------->>>rs_ = "+rs_);				
			while (rs_.next()){
				custid = rs_.getString("custid");
				System.out.println("--------------->>>custid = "+custid);
			}
        String s = "begin ? := admin_v2.pk_subscriber_vcc.sf_delete_vcc("
                + "'" + MSISDN + "'"
				+ ",'" + getUserVar("userID") + "'"
                + ",'" + UserIP + "'"
                + ",'" + matinh + "'" 
                + ");"
                + "end;";
        System.out.println(s);
		result = this.reqValue("", s);
		if(result.equals("1")){
		try {
			Date dNow = new Date( );
			SimpleDateFormat ft = 
			new SimpleDateFormat ("yyyy:MM:dd hh:mm:ss");
			String TimeStamp = ft.format(dNow);
			//String body_del ="\"TimeStamp\":\""+TimeStamp+"\",\"ActionType\":\"2\",\"FeeNumber\":\""+MSISDN+"\",\"CustID\":\""+custid+"\",\"CustTelNum\":\""+MSISDN+"\"";
			String body_del ="?TimeStamp="+TimeStamp+"&ActionType=2&FeeNumber="+MSISDN+"&CustID="+custid+"&CustTelNum="+MSISDN;
			String api_del = apiPost(body_del);			
			}catch (Exception ex){				
				System.out.println("Exception 2 : " + ex.getMessage());
			ex.printStackTrace();
			}
		}
		return result;
    } 
		public String bsdanhmuc_vcc ( String MSISDN,
							String danhmuc,
							String giatien,
							String khoiluong,
							String kycuoc,
							String tilevnp,
							String tiledoitac,
							String hieuluc,
							String ghichu,
							String UserID,
							String UserIP,
							String matinh							
							) 
					throws Exception{ 
         
        String s = "begin ? := admin_v2.pk_subscriber_vcc.sf_bsdanhmuc_vcc("
                + "'" + MSISDN + "'"
				+ ",'" + danhmuc + "'"
				+ ",'" + giatien + "'"
				+ ",'" + khoiluong + "'"
				+ ",'" + kycuoc + "'"
				+ ",'" + tilevnp + "'"
				+ ",'" + tiledoitac + "'"
				+ ",'" + hieuluc + "'"
				+ ",'" + ghichu + "'"
				+ ",'" + getUserVar("userID") + "'"
                + ",'" + UserIP + "'"  
                + ");"
                + "end;";
        System.out.println(s);
		String result = this.reqValue("", s);
		if (result.indexOf("OK")>=0)
		{
			if (danhmuc=="5") {
				String[] arrOfStr = result.split("\\|");
				Date dNow = new Date( );
				SimpleDateFormat ft = 
					new SimpleDateFormat ("yyyy:MM:dd hh:mm:ss");
				String TimeStamp = ft.format(dNow);	
				String recordMonth="0";
				if 	(hieuluc.equals("1")) 
				{
					recordMonth=khoiluong;
				}
				String body_upd ="{\"TimeStamp\":\""+TimeStamp+"\",\"ActionType\":\"3\",\"FeeNumber\":\""+MSISDN+"\",\"PackageName\":\""+ arrOfStr[1]+"\",\"RecordMonth\":\""+recordMonth+"\",\"CustTelNum\":\""+MSISDN+"\"}";
				System.out.println(body_upd);
				String api_udp = apiJsonPost(body_upd);
				System.out.println(api_udp);
			}			
			return "OK";
		}
		else 
		{
			System.out.println("Ket qua goi sf_bsdanhmuc_vcc ="+result); 
			return result;
		}
		
    }
	public String check_vcc_sys(String pc_msisdn, String pc_userid ,String pc_userip,String pc_agentcode)throws Exception 
	{
		String result="";	
		try {
				String body ="";			
				Date dNow = new Date( );
				SimpleDateFormat ft = 
				new SimpleDateFormat ("yyyy:MM:dd hh:mm:ss");
				String TimeStamp = ft.format(dNow);			
				String typeAction="6";
				String api_rs ="";
				String resultCode ="";
				String resultMsg ="";
				String subs= "0"+pc_msisdn.substring(2);
						
				body = "?TimeStamp="+TimeStamp+"&ActionType="+typeAction+"&FeeNumber="+subs+"&subs="+subs; 					
				api_rs = apiPost(body);			
				result = api_rs; 				
			}catch (Exception ex){				
				System.out.println("Exception 2 : " + ex.getMessage());
				ex.printStackTrace();
				result="";
			}
			return result;
		
	}
	public String del_vcc_sys(String pc_msisdn, String pc_userid ,String pc_userip,String pc_agentcode)throws Exception 
	{
		String result="";	
		int count_=1;
		try {
				RowSet count = this.reqRSet("SELECT count(1) count_ FROM admin_v2.subscriber_vcc where so_tb ='"+pc_msisdn+"' and vcc_status is not null ");
				while (count.next()){
					count_ = count.getInt("count_");
					
				}
				if (count_== 0) {				
				
					String body ="";			
					Date dNow = new Date( );
					SimpleDateFormat ft = 
					new SimpleDateFormat ("yyyy:MM:dd hh:mm:ss");
					String TimeStamp = ft.format(dNow);			
					String typeAction="6";
					String api_rs ="";
					String resultCode ="";
					String resultMsg ="";
					String subs= "0"+pc_msisdn.substring(2);
							
					body = "?TimeStamp="+TimeStamp+"&ActionType="+typeAction+"&FeeNumber="+subs+"&subs="+subs; 			
										
					api_rs = apiPost(body);
										
					JSONParser parser = new JSONParser();
					JSONObject obj = (JSONObject) parser.parse(api_rs);
					 
					resultCode = (String) obj.get("resultCode");
					if(resultCode.equals("00000")) {
						try {
								JSONObject resObj = (JSONObject) obj.get("resultMsg");
								String cusid = (String) resObj.get("CustID"); 
								
								JSONArray array = (JSONArray) resObj.get("HotlineNums");
								
								String status="";
								if (array.size() >0) {
									JSONObject object =	(JSONObject)array.get(0);
									status= (String) object.get("Status");
									
									if (status.equals("active")) {																	
										dNow = new Date( );							
										TimeStamp = ft.format(dNow);							
										String body_del ="?TimeStamp="+TimeStamp+"&ActionType=2&FeeNumber="+pc_msisdn+"&CustID="+cusid+"&CustTelNum="+pc_msisdn;
										String api_del = apiPost(body_del);											
										obj = (JSONObject) parser.parse(api_del);
									
										resultCode = (String) obj.get("resultCode");
										if(resultCode.equals("00000")) {
											result="1"; 
										}
										else {
											result=(String)obj.get("resultMsg"); 
										}
									}
									else {
										result="3";
									}
								}
								else {
									
									result="3";
								}
								
								
							}catch (Exception ex){				
								System.out.println("Exception 2 : " + ex.getMessage());
								ex.printStackTrace();
								result=""; 
							}
		
					}else {
						result="3";
					}
				
					
				}
				else {
					result="2";
				}
			}catch (Exception ex){				
				System.out.println("Exception 2 : " + ex.getMessage());
				ex.printStackTrace();
				result="";
			}
			return result;
		
	}
	public String cndanhmuc_vcc ( String MSISDN,
							String danhmuc,
							String giatien,
							String khoiluong,
							String kycuoc,
							String tilevnp,
							String tiledoitac,
							String hieuluc,
							String ghichu,								
							String UserID,
							String UserIP,
							String matinh							
							) 
					throws Exception{ 
         
        String s = "begin ? := admin_v2.pk_subscriber_vcc.sf_cndanhmuc_vcc("
                + "'" + MSISDN + "'"
				+ ",'" + danhmuc + "'"
				+ ",'" + giatien + "'"
				+ ",'" + khoiluong + "'"
				+ ",'" + kycuoc + "'"
				+ ",'" + tilevnp + "'"
				+ ",'" + tiledoitac + "'"
				+ ",'" + hieuluc + "'"
				+ ",'" + ghichu + "'"
				+ ",'" + getUserVar("userID") + "'"
                + ",'" + UserIP + "'"  
                + ");"
                + "end;";
        System.out.println(s);
		String result = this.reqValue("", s);
		
		if (result.indexOf("OK")>=0)
		{
			if (danhmuc=="5") {
				String[] arrOfStr = result.split("\\|");
				
				Date dNow = new Date( );
				SimpleDateFormat ft = 
					new SimpleDateFormat ("yyyy:MM:dd hh:mm:ss");
				String TimeStamp = ft.format(dNow);	
				String recordMonth="0";
				if 	(hieuluc.equals("1")) 
				{
					recordMonth=khoiluong;
				}
				String body_upd ="{\"TimeStamp\":\""+TimeStamp+"\",\"ActionType\":\"3\",\"FeeNumber\":\""+MSISDN+"\",\"PackageName\":\""+ arrOfStr[1]+"\",\"RecordMonth\":\""+recordMonth+"\",\"CustTelNum\":\""+MSISDN+"\"}";
				System.out.println(body_upd);
				String api_udp = apiJsonPost(body_upd);
				System.out.println(api_udp);
			}
			return "OK";
		}
		else {
			System.out.println("Ket qua goi sf_bsdanhmuc_vcc ="+result); 
			return result;
		} 
			
		
    }
	private String getValue(String content, String key) {
		/* String result = content;
		int count = result.indexOf(key, 0);
		System.out.println("Chi muc cua key : " + key+" - count ="+count+" - length ="+content.length());
		if (count < 1) {
			return "";
		}
		result = result.substring(result.indexOf(":\"", count) + 2, result.indexOf("\"", count));
		return result;	*/
		String temp;
		try {
			JSONParser jsonParser = new JSONParser();	            
			JSONObject jsonObject = (JSONObject) jsonParser.parse(content);
			temp = (String) jsonObject.get(key);
		}
		catch(Exception ex){
			ex.printStackTrace();
			temp ="";
		}
		return temp;

	}	
// API ket noi VCC 10.211.26.247:9393/api/registerAccount
	
	

	public String apiPost(String input) {
		String urlHttp = "http://10.211.26.247:9393/api/registerAccount";
		try {
			byte[] postData = input.getBytes("UTF-8");
			//byte[] postData = input.getBytes();
			int postDataLength = postData.length;
			// URL url = new URL(urlHttp + method);
			URL url = new URL(urlHttp );
			HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
			httpcon.setDoOutput(true);

			// add reuqest header
			httpcon.setRequestMethod("POST");
			httpcon.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			httpcon.setRequestProperty("charset", "utf-8");
			httpcon.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			// httpcon.setUseCaches( false );
			OutputStream os = httpcon.getOutputStream();
			os.write(postData);
			//os.write(input.getBytes());
			os.flush();
			// Send post request
			int responseCode = httpcon.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + urlHttp);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(httpcon.getInputStream()));
			String inputLine;
			String resp = "";
			while ((inputLine = in.readLine()) != null) {
				resp += (inputLine);
			}
			System.out.println("Response  : " + resp);
			in.close();
			// String kq =getValue(resp,"SYORI_STS");
			// String id_member =getValue(resp,"KAIIN_NO");
			// String out = kq+":"+id_member;
			return resp;

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}

	}
	public String apiJsonPost(String input) {
		String urlHttp = "http://10.211.26.247:9393/api/registerAccount";
		StringBuffer buf = new StringBuffer();
        try {        
            URL u = new URL(urlHttp);
            HttpURLConnection con = (HttpURLConnection)u.openConnection();
            con.setConnectTimeout(5000); 
            
            con.setRequestProperty("Content-Type","application/json;charset=utf-8");        
            con.setDoOutput(true);
            con.setDoInput(true);
            con.connect();
            
            OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
            writer.write(input);
            writer.flush();
            BufferedReader  reader ;
            if(con.getResponseCode() == 200){
              reader = new BufferedReader(new
              InputStreamReader(con.getInputStream()));
            }else{
              reader = new BufferedReader(new
              InputStreamReader(con.getErrorStream()));
            }            
            String line ;
            while( (line = reader.readLine()) != null )
            buf.append(line);
            
            reader.close();
            String resp= buf.toString();          
        
			return resp;

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}

	}
		
	  
	
}
