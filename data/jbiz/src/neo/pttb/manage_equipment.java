package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class manage_equipment extends NEOProcessEx {

	public String new_msisdn_catelory(
		String psDataSchema,
		String psAgentCode,
		String psMsisdnList,
		String psCateloryList,
		String psDispatch,
		String psUserid,
		String psUserip
		) throws Exception {
			String s ="begin ? :=admin_v2.pkg_manage_catelory.new_msisdn_catelory_func("
				+"'"+psDataSchema     +"',"
				+"'"+psAgentCode      +"',"
				+"'"+psMsisdnList     +"',"
				+"'"+psCateloryList     +"',"
				+"'"+psDispatch     +"',"
				+"'"+psUserid         +"',"
				+"'"+psUserip         +"'"					
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	
	public String new_category(
		String pschema,
		String plist_id,
		String ptype_code,
		String pcodes,
		String pagent,
		String pprice,	
		String pmoney,		
		String pstatus,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_manage_catelory.new_category("
				+"'"+pschema+"',"
				+"'"+plist_id+"',"
				+"'"+ConvertFont.UtoD(ptype_code)+"',"
				+"'"+ConvertFont.UtoD(pcodes)+"',"
				+"'"+ConvertFont.UtoD(pagent)+"',"
				+"'"+pprice+"',"
				+"'"+pmoney+"',"				
				+"'"+pstatus+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_category(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_manage_catelory.del_category("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_category(
	String pschema,
		String pcategory_id,
		String plist_id,
		String ptype_code,
		String pcodes,
		String pagent,
		String pprice,
		String pmoney,
		String pstatus,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_manage_catelory.edit_category("
				+"'"+pschema+"',"
				+"'"+plist_id+"',"
				+"'"+pcategory_id+"',"				
				+"'"+ConvertFont.UtoD(ptype_code)+"',"
				+"'"+ConvertFont.UtoD(pcodes)+"',"
				+"'"+ConvertFont.UtoD(pagent)+"',"
				+"'"+pprice+"',"
				+"'"+pmoney+"',"
				+"'"+pstatus+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
}