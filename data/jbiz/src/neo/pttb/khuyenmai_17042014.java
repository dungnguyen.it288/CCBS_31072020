package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;



public class khuyenmai extends NEOProcessEx 
{
	public void run() 
	{
		System.out.println("neo.khuyen_mai was called");
	}

	public  String laypromotion(String dispatch)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/promotion/subrecovery/promotion_config" +"&"+CesarCode.encode("dispatch") + "=" + dispatch;
	}
	
	public String regProkm
	(
		String dataschema,  	
		String agentcode,  											
		String userid,
		String promotion,
		String htkm,
		String matb,
		String description
	)
  	{
		String s= "begin ?:= admin_v2.pkg_goi.reg_pro_km('"+dataschema+"','"+agentcode+"','"+userid+"','"+promotion+"','"+htkm+"','"+matb+"','"+description+"'"+");" +" end;"  ;        
		System.out.println(s); 		
		return s;  
	}
	
	 public String getLstProRegHis
	(
		String matb
	)
	{
		return "/main?"+CesarCode.encode("configFile")+"=PTTB/promotion/subrecovery/ajax_subrecovery" +"&"+CesarCode.encode("matb") + "=" + matb;
	}
	
	public String chkProReg
	(
		String dataschema,  
		String agentcode,  
		String userid,
		String matb,
		String dotkm,
		String htkm		
	)
  	{
		String s= "begin ?:= admin_v2.pkg_goi.chk_pro_reg_km('"+dataschema+"', '"+agentcode+"', '"+userid+"', '"+matb+"', '"+dotkm+"', '"+htkm+"'"+");  end;";        
		
		System.out.println(s);
		return s; 
	}	

}

