
package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;



public class upl_kh_trungthanh extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb.upl_kh_trungthanh was called");
  }
	public String rec_tonghop(
 										 String upload_id) 
 										{
		String s= "select count(1)-count(kiemtra) hople,count(1) tong from ccs_common.UPLOAD_KH_TRUNGTHANH a where a.upload_id = "+upload_id;
        return s;
  }
  
  public String doc_hople(
	String dataschema, 
			String upload_id)
  	{ 
	 	String url_="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=upload/upload_kh_trungthanh/ajax_hople";
		url_ = url_ + "&"+CesarCode.encode("data_schema")+"="+dataschema;	
		url_ = url_ + "&"+CesarCode.encode("upload_id")+"="+upload_id;	    
	  return url_;
  	}
	
	public String doc_khonghople(
   			String dataschema,
			String upload_id)
  	{ 
		String url_="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=upload/upload_kh_trungthanh/ajax_khonghople";	
		url_ = url_ + "&"+CesarCode.encode("data_schema")+"="+dataschema;	
		url_ = url_ + "&"+CesarCode.encode("upload_id")+"="+upload_id;	    
		return url_;		  
  	}
	
	public String doc_huy(
   			String dataschema,
			String upload_id)
  	{ 
		String url_="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=upload/upload_kh_trungthanh/ajax_huy";	
		url_ = url_ + "&"+CesarCode.encode("data_schema")+"="+dataschema;	
		url_ = url_ + "&"+CesarCode.encode("upload_id")+"="+upload_id;	    
		return url_;		  
  	}
	
	public String doc_capnhat(
   			String dataschema,
			String upload_id)
  	{ 
		String url_="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=upload/upload_kh_trungthanh/ajax_capnhat";	
		url_ = url_ + "&"+CesarCode.encode("data_schema")+"="+dataschema;	
		url_ = url_ + "&"+CesarCode.encode("upload_id")+"="+upload_id;	    
		return url_;		  
  	}
	
	public String upload_kh_trungthanh(
		String dataschema,
		String funcschema,
		String upload_id		
	)
	{ 
		return "begin ? :=" + funcschema + "pkg_upload.upload_kh_trungthanh("
			+"'"+upload_id+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";	
	}
  
  
}