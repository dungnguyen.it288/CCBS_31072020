package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class alo_doanh_nghiep2
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb.goi_truyen_solieu was called");
  }
  
   public String layDSGoiNhom(
		 String psSOMAY,
		 String psloai                         
  	) throws Exception {
	 String sUserId= getUserVar("userID");
	 String sAgentCode = getUserVar("sys_agentcode");
	 String sDataSchema = getUserVar("sys_dataschema");
	 String sFuncSchema = getSysConst("ADMIN_V2");
	 String sCommonSchema = getSysConst("CommonSchema");
	 
     String s=    "begin ? := "+sFuncSchema+"PKG_ALO_DN_V2.layds_goi_nhom("		
		+"'"+psSOMAY+"'"
		+",'"+psloai+"'"	
		+",'"+sAgentCode+"'"	
		+");"
        +"end;"
        ;
		System.out.println(s);
		return this.reqValue("", s);
	}
  
     public String themMoiNhom(
		  String sUserId
		, String sAgentCode
		, String sFuncSchema
		, String sdataSchema
		, String loaiNhom
		, String dsmay 				
		, String ghiChu 
		, String chunhom
		, String dsmay_2	
		, String dsVip
		, String dsVip_2
		, String may_cn		
  	) throws Exception {
     String s=    "begin ? := "+sFuncSchema+"PKG_ALO_DN_V2.them_moi_nhom("	
		+" '"+sdataSchema+"'"
		+",'"+sUserId+"'"                           
		+",'"+sAgentCode+"'"                                  		
		+",'"+loaiNhom+"'"
		+",'"+dsmay+"'"		
		+",'"+ghiChu+"'"				
		+",'"+may_cn+"'"
		+",'"+chunhom+"'"
		+",'"+dsmay_2+"'"	
		+",'"+dsVip+"'"
		+",'"+dsVip_2+"'"
		+");"
        +"end;"
        ;
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	
	
	public String capNhatNhom_alo(
    	  String sUserId
		, String sAgentCode
		, String sFuncSchema
		, String sdataSchema
		, String may_cn	 
		, String loaiNhom
		, String goi_id	
		, String chu_nhom
		, String alo_1	
		, String alo_2			
		, String vip250		
		, String vip350	
		, String ds_tachnhom	
  	) throws Exception {

     String s=    "begin ? := "+sFuncSchema+"PKG_ALO_DN_V2.cap_nhat_nhom_alo("	
		+" '"+sdataSchema+"'"
		+",'"+sUserId+"'"                           
		+",'"+sAgentCode+"'"
		+",'"+may_cn+"'"		
		+",'"+loaiNhom+"'"
		+",'"+goi_id+"'"
		+",'"+chu_nhom+"'"
		+",'"+alo_1+"'"
		+",'"+alo_2+"'"
		+",'"+vip250+"'"
		+",'"+vip350+"'"
		+",'"+ds_tachnhom+"'"	
		+");"
        +"end;"
        ;
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String ThemSoVaoNhom_alo(
	    	  String sUserId
			, String sAgentCode
			, String sFuncSchema
			, String sdataSchema
			, String may_cn	 
			, String loaiNhom
			, String goi_id	
			, String chu_nhom	
			, String ds_sotb	
	  	) throws Exception {
	     String s=    "begin ? := "+sFuncSchema+"PKG_ALO_DN_V2.themsovaonhom_alo("	
			+" '"+sUserId+"'"
			+",'"+sAgentCode+"'"                           
			+",'"+sFuncSchema+"'"
			+",'"+sdataSchema+"'"
			+",'"+may_cn+"'"		
			+",'"+loaiNhom+"'"
			+",'"+goi_id+"'"
			+",'"+chu_nhom+"'"
			+",'"+ds_sotb+"'"
			+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
	
	public String huynhom(
		  String sUserId
		, String sAgentCode
		, String sDataSchema
		, String sFuncSchema
		, String sCommonSchema	 
		, String idnhom
		, String loainhom
	    , String may_cn		
  	) throws Exception {
     String s=    "begin ? := "+sFuncSchema+"PKG_ALO_DN_V2.huy_nhom("		
		+" '"+sCommonSchema+"'"                             
		+",'"+sUserId+"'"                                  
		+",'"+sAgentCode+"'"                                  		
		+",'"+idnhom+"'"
		+",'"+loainhom+"'"
		+",'"+may_cn+"'"		
		+");"
        +"end;"
        ;
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String capnhat_loaigoi(
			  String sUserId
			, String sAgentCode
			, String sDataSchema
			, String sFuncSchema
			, String idnhom
			, String loainhom
			, String ds_alo1
			, String ds_alo2
			, String ds_vip1
			, String ds_vip2
		    , String may_cn		
	  	) throws Exception {
	     String s=    "begin ? := "+sFuncSchema+"PKG_ALO_DN_V2.capnhat_loaigoi("		                          
			+"'"+sUserId+"'"                                  
			+",'"+sAgentCode+"'"    
			+",'"+sFuncSchema+"'"   
			+",'"+sDataSchema+"'"   
			+",'"+idnhom+"'"
			+",'"+loainhom+"'"
			+",'"+ds_alo1+"'"
			+",'"+ds_alo2+"'"
			+",'"+ds_vip1+"'"
			+",'"+ds_vip2+"'"
			+",'"+may_cn+"'"		
			+");"
	        +"end;"
	        ;
			System.out.println(s);
			return this.reqValue("", s);
		}
}














