package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pre_payment extends NEOProcessEx 
{
	public void run() 
	{
		System.out.println("neo.khuyen_mai was called");
	}

	public String laytt_thuebaokm
	(  
		String dataschema,  	
		String agentcode,  											
		String psDispatch,
		String psmatb,
		String psuser
	)
  	{		
		String s="begin ?:=  admin_v2.pkg_prepayment.laytt_thuebaokm_v2('"
							+ dataschema		
							+"','"+agentcode
							+"','"+psDispatch
							+"','"+psmatb
							+"','"+psuser
							+"');"
							+" end;"
							;       
		System.out.println(s); 	
		return s;  
	}
	public String laytt_thuebaokm_v2
	(  
		String dataschema,  	
		String agentcode,  											
		String psDispatch,
		String psmatb,
		String psuser
	)
  	{		
		String s="begin ?:=  admin_v2.pkg_prepayment.laytt_thuebaokm_v2('"
							+ dataschema		
							+"','"+agentcode
							+"','"+psDispatch
							+"','"+psmatb
							+"','"+psuser
							+"');"
							+" end;"
							;       
		System.out.println(s); 	
		return s;  
	}
	public String laytt_goicuockm
	(  
		String dataschema,  	
		String agentcode,  											
		String psDispatch,
		String matb,
		String user
	)
  	{		
		String s="begin ?:=  admin_v2.pkg_prepayment.laytt_goicuockm('"
							+ dataschema		
							+"','"+agentcode
							+"','"+psDispatch
							+"','"+matb
							+"','"+user
							+"');"
							+" end;"
							;        
		System.out.println(s); 		
		return s;  
	}
	public String laytt_goicuoc_dongtruoc
	(
		String dispath, String muctien
	)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/pre_payment/ajax_goicuoc"
   				+"&"+CesarCode.encode("dis") + "=" + dispath
				+"&"+CesarCode.encode("muctien") + "=" + muctien			
   					;
	}
	
	
	public String dangkydatcoc
	(  
		String dataschema,  	
		String agentcode,  											
		String psDispatch,
		String psPackage_src,
		String pspkg,
		String matb,
		String user,
		String ghichu
	)
  	{		
		String s="begin ?:=  admin_v2.pkg_prepayment.dangky('"
							+ dataschema		
							+"','"+agentcode
							+"','"+psDispatch
							+"','"+psPackage_src
							+"','"+pspkg
							+"','"+matb
							+"','"+user
							+"','"+ghichu
							+"');"
							+" end;"
							;        
		System.out.println(s); 		
		return s;  
	}
	public String check_dk
	(  
		String dataschema,  	
		String agentcode,  											
		String psDispatch,
		String pspkg,
		String matb,
		String user
	)
  	{		
		String s="begin ?:=  admin_v2.pkg_prepayment.check_dk('"
							+ dataschema		
							+"','"+agentcode
							+"','"+psDispatch
							+"','"+pspkg
							+"','"+matb
							+"','"+user
							+"');"
							+" end;"
							;        
		System.out.println(s); 		
		return s;  
	}
	
   public String view_package_in_subs( String matb)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/pre_payment/ajax_package_in_subs"
   				+"&"+CesarCode.encode("matb") + "=" + matb			
   					;
	}
    public String view_pre_payment_online( String matb)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/pre_payment/ajax_prepayment_online"
   				+"&"+CesarCode.encode("matb") + "=" + matb			
   					;
	}
	public String view_pre_payment_log( String matb)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/pre_payment/ajax_prepayment_log"
   				+"&"+CesarCode.encode("matb") + "=" + matb			
   					;
	}
   public String view_pre_payment_log1( String matb)
	{
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=pttb/pre_payment/get_pre_payment_log&" + CesarCode.encode(" matb") + "=" + matb;
		return url_;
		
	}

	public String del_tb(  String dataschema,  	
							String agentcode,  											
							String userid,
							String id)
  	{		
    String s=    "begin ?:=  admin_v2.pkg_qltb_new.del_tb('"
							+ dataschema		
							+"','"+agentcode
							+"','"+userid
							+"','"+id
							+"');"
							+" end;"
							;        
    System.out.println(s); 		
	 return s;  
  }
  
	public String delete_prepayment
	(  
		String dataschema,  	
		String agentcode,  											
		String psId_package_data,		
		String matb,
		String user,
		String userip,
		String ghichu,
		int type
	)
  	{		
		String s="begin ?:=  admin_v2.pkg_prepayment.delete_prepayment('"
							+ dataschema		
							+"','"+agentcode
							+"','"+psId_package_data
							+"','"+matb
							+"','"+user
							+"','"+userip
							+"','"+ghichu
							+"','"+type
							+"');"
							+" end;"
							;        
		System.out.println(s); 		
		return s;  
	}
}

