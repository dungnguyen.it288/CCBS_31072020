package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class config_message extends NEOProcessEx {
	public String new_config_message(
		String pschema,
		String psiddata,
		String pstatus,
		String pstart_date,
		String pend_date,
		String pstart_time,
		String pend_time,
		String pnext_time,
		String pexception_date,
		String pexception_time,
		String pagent_config,
		String pmoney_from,
		String pmoney_to,		
		String pmonth_app,
		String ptime_retry,
		String pdate_first_month,
		String pagent,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=admin_v2.pk_config_message.new_config_message("
				+"'"+pschema+"',"
				+"'"+psiddata+"',"
				+"'"+pstatus+"',"
				+"'"+pstart_date+"',"
				+"'"+pend_date+"',"
				+"'"+pstart_time+"',"
				+"'"+pend_time+"',"
				+"'"+pnext_time+"',"				
				+"'"+pexception_date+"',"
				+"'"+pexception_time+"',"
				+"'"+pagent_config+"',"
				+"'"+pmoney_from+"',"
				+"'"+pmoney_to+"',"				
				+"'"+pmonth_app+"',"
				+"'"+ptime_retry+"',"
				+"'"+pdate_first_month+"',"
				+"'"+pagent+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println("demo " +s);
       return this.reqValue("", s);
	}
	public String del_config_message(
		String pschema,
		String pid,
		String pagent,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=admin_v2.pk_config_message.del_config_message("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+pagent+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_config_message(
	String pschema,
		String psid,
		String psdata,
		String pstatus,
		String pstart_date,
		String pend_date,
		String pstart_time,
		String pend_time,
		String pnext_time,
		String pexception_date,
		String pexception_time,
		String pmoney_from,
		String pmoney_to,
		String pagent_config,
		String pmonth_app,
		String ptime_retry,
		String pdate_first_month,
		String pagent,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=admin_v2.pk_config_message.edit_config_message("
				+"'"+pschema+"',"
				+"'"+psid+"',"
				+"'"+psdata+"',"				
				+"'"+pstatus+"',"
				+"'"+pstart_date+"',"
				+"'"+pend_date+"',"
				+"'"+pstart_time+"',"
				+"'"+pend_time+"',"
				+"'"+pnext_time+"',"
				+"'"+pexception_date+"',"
				+"'"+pexception_time+"',"
				+"'"+pmoney_from+"',"
				+"'"+pmoney_to+"',"
				+"'"+pagent_config+"',"
				+"'"+pmonth_app+"',"
				+"'"+ptime_retry+"',"
				+"'"+pdate_first_month+"',"
				+"'"+pagent+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String new_config_process(
		String pschema,
		String psiddata,
		String pstatus,
		String pnext_time,
		String pstart_date,
		String pend_date,
		String pstart_time,
		String pend_time,
		String ploop_time,
		String pexception_date,
		String pexception_time,
		String pagent_config,
		String pmoney_from,
		String pmoney_to,		
		String pmonth_app,
		String ptime_retry,
		String pdate_first_month,
		String pagent,
		String puserid,
		String puserip,
		String pspecialdate,
		String pExcepMin,
		String pExcepMax) throws Exception {
			String s ="begin ? :=admin_v2.pkg_config_process.new_config_process("
				+"'"+pschema+"',"
				+"'"+psiddata+"',"
				+"'"+pstatus+"',"
				+"'"+pnext_time+"',"
				+"'"+pstart_date+"',"
				+"'"+pend_date+"',"
				+"'"+pstart_time+"',"
				+"'"+pend_time+"',"
				+"'"+ploop_time.replace("'", "''")+"',"				
				+"'"+pexception_date+"',"
				+"'"+pexception_time+"',"
				+"'"+pagent_config+"',"
				+"'"+pmoney_from+"',"
				+"'"+pmoney_to+"',"				
				+"'"+pmonth_app+"',"
				+"'"+ptime_retry+"',"
				+"'"+pdate_first_month+"',"
				+"'"+pagent+"',"
				+"'"+pspecialdate+"',"
				+"'"+pExcepMin+"',"
				+"'"+pExcepMax+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println("demo " +s);
       return this.reqValue("", s);
	}
	public String del_config_process(
		String pschema,
		String pid,
		String pagent,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=admin_v2.pkg_config_process.del_config_process("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+pagent+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_config_process(
	String pschema,
		String psid,
		String psdata,
		String pstatus,
		String pnext_time,
		String pstart_date,
		String pend_date,
		String pstart_time,
		String pend_time,
		String ploop_time,
		String pexception_date,
		String pexception_time,
		String pagent_config,
		String pmoney_from,
		String pmoney_to,		
		String pmonth_app,
		String ptime_retry,
		String pdate_first_month,
		String pagent,
		String puserid,
		String puserip,
		String pspecialdate,
		String pExcepMin,
		String pExcepMax) throws Exception {
			String s ="begin ? :=admin_v2.pkg_config_process.edit_config_process("
				+"'"+pschema+"',"
				+"'"+psid+"',"
				+"'"+psdata+"',"				
				+"'"+pstatus+"',"
				+"'"+pnext_time+"',"
				+"'"+pstart_date+"',"
				+"'"+pend_date+"',"
				+"'"+pstart_time+"',"
				+"'"+pend_time+"',"
				+"'"+ploop_time.replace("'", "''")+"',"
				+"'"+pexception_date+"',"
				+"'"+pexception_time+"',"
				+"'"+pagent_config+"',"
				+"'"+pmoney_from+"',"
				+"'"+pmoney_to+"',"				
				+"'"+pmonth_app+"',"
				+"'"+ptime_retry+"',"
				+"'"+pdate_first_month+"',"
				+"'"+pagent+"',"
				+"'"+pspecialdate+"',"
				+"'"+pExcepMin+"',"
				+"'"+pExcepMax+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_config_agent(	String psconfigid,
										String psvalue,
										String psdataschema,
										String puserid,
										String puserip) throws Exception
	{
		String s ="begin ? :=admin_v2.pkg_config_process.edit_config_agent("			
				+"'"+psconfigid+"',"
				+"'"+ConvertFont.UtoD(psvalue)+"',"				
				+"'"+psdataschema+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String new_data_sms(
			String psproccess,	
			String psagent,
			String pscollection,
			String puserid,
			String puserip) throws Exception {
				String s ="begin ? :=admin_v2.pk_config_message.create_list_sms("
						+"'"+psproccess+"',"
						+"'"+psagent+"',"	
						+"'"+pscollection+"',"
						+"'"+puserid+"',"
						+"'"+puserip+"'"
						+"); end;";			
			return this.reqValue("", s);
		}
		
	public String do_sms(
			String psmsisdn,
			String psproccess,	
			String psagent,			
			String puserid,
			String puserip) throws Exception {
				String s ="begin ? :=admin_v2.pk_config_message.proccess_sms("
						+"'"+psmsisdn+"',"
						+"'"+psproccess+"',"
						+"'"+psagent+"',"											
						+"'"+puserid+"',"
						+"'"+puserip+"'"
						+"); end;";			
			return this.reqValue("", s);
		}
	public String new_notification(
		String pdata_type,
		String pnotification_type,
		String pusers,		
		String puserid,
		String puserip
		) throws Exception {
			String s ="begin ?:=admin_v2.pkg_config_process.new_notification_func("
				+"'"+"admin_v2."+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"
				+"'"+pdata_type+"',"
				+"'"+pnotification_type+"',"
				+"'"+pusers+"'"											
				+"); end;";
       return this.reqValue("", s);
	}
	public String update_notification(
		String pdata_type,
		String pnotification_type,
		String pusers,		
		String puserid,
		String puserip,
		String pid
		) throws Exception {
			String s ="begin ?:=admin_v2.pkg_config_process.update_notification_func("
				+"'"+"admin_v2."+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"
				+"'"+pdata_type+"',"
				+"'"+pnotification_type+"',"
				+"'"+pusers+"',"				
				+"'"+pid+"'"									
				+"); end;";
       return this.reqValue("", s);
	}
	
	public String detele_notification(
		String pdata_type,
		String pnotification_type,
		String pusers,	
		String puserid,
		String puserip,
		String pid
		) throws Exception {
			String s ="begin ?:=admin_v2.pkg_config_process.delete_notification_func("	
				+"'"+puserid+"',"
				+"'"+puserip+"',"
				+"'"+pdata_type+"',"
				+"'"+pnotification_type+"',"
				+"'"+pusers+"',"				
				+"'"+pid+"'"									
				+"); end;";
       return this.reqValue("", s);
	}
}