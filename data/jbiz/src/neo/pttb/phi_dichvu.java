package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class phi_dichvu extends NEOProcessEx {
	public String new_dichvu_cuochm(
		String pschema,
		String pmuc_cuoc,
		String pma_dv,	
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pttb_dichvu.new_dichvu_cuochm("
				+"'"+pschema+"',"
				+"'"+pmuc_cuoc+"',"
				+"'"+pma_dv+"',"				
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_dichvu_cuochm(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pttb_dichvu.del_dichvu_cuochm("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String new_dichvu_cuocdc(
		String pschema,
		String pmuc_cuoc,
		String pma_dv,	
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pttb_dichvu.new_dichvu_cuocdc("
				+"'"+pschema+"',"
				+"'"+pmuc_cuoc+"',"
				+"'"+pma_dv+"',"				
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_dichvu_cuocdc(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pttb_dichvu.del_dichvu_cuocdc("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String new_dichvu_thuphi_data(
		String pschema,
		String pmsisdn,
		String pma_dv,
		String ploaicuoc_id,
		String pmuccuoc_id,
		String pnote,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pttb_dichvu.new_dichvu_thuphi_data("
				+"'"+getUserVar("sys_agentcode")+"',"
				+"'"+ConvertFont.UtoD(pmsisdn)+"',"
				+"'"+ConvertFont.UtoD(pma_dv)+"',"
				+"'"+ploaicuoc_id+"',"
				+"'"+pmuccuoc_id+"',"
				+"'"+ConvertFont.UtoD(pnote)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_dichvu_thuphi_data(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pttb_dichvu.del_dichvu_thuphi_data("
				+"'"+getUserVar("sys_agentcode")+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_dichvu_thuphi_data(
	String pschema,
		String pid,
		String pmsisdn,
		String pma_dv,
		String ploaicuoc_id,
		String pmuccuoc_id,
		String pnote,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pttb_dichvu.edit_dichvu_thuphi_data("
				+"'"+getUserVar("sys_agentcode")+"',"
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pmsisdn)+"',"
				+"'"+ConvertFont.UtoD(pma_dv)+"',"
				+"'"+ploaicuoc_id+"',"
				+"'"+pmuccuoc_id+"',"
				+"'"+ConvertFont.UtoD(pnote)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
}