package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import neo.qlsp.*;

public class dangky_4g extends NEOProcessEx {
  public void run() {
    System.out.println("neo.prepayment was called");
  }
  
  /* Thuc hien dang ky 4G */
  public String kiemtra_thuebao(String psso_tb){
    String s=  "begin ? := admin_v2.pkg_dangky_4g.check_4g('"+psso_tb+"'); end;";
    return s;
  }

public String dangky(String pssotb
						,String pssosim                      
						,String psghichu
						,String psuserip
						){
	String session = getSession();
	String simtype="";	
	try {		
		String getSimInfo_=Connect.getSimInfo(session,pssosim);				
		getSimInfo_=getSimInfo_.replace("||","#");
		String[] array_str = getSimInfo_.split("#");	
		simtype  = array_str[3];			
	} catch (Exception ex) {}
	
    String s=  "begin ? := admin_v2.pkg_dangky_4g.dangky_4g('"+pssotb+"','"+simtype+"','"+psghichu+"','"+getUserVar("userID")+"','"+getUserVar("sys_agentcode")+"','"+getUserVar("sys_dataschema")+"','"+psuserip+"'); end;";
    //System.out.println(s);
	return s;
}

public String huy(String pssotb
				,String psghichu
				,String psuserip
	){
	String s=  "begin ? := admin_v2.pkg_dangky_4g.huy_4g('"+pssotb+"','"+psghichu+"','"+getUserVar("userID")+"','"+getUserVar("sys_agentcode")+"','"+getUserVar("sys_dataschema")+"','"+psuserip+"'); end;";
	System.out.println(s);
	return s;
}
  
public String upload_dk(String psupload_id
						,String psuserip
						){
	String result="";
	String s=  "begin ? := admin_v2.pkg_dangky_4g.upload_4g('"+psupload_id+"','"+getUserVar("sys_agentcode")+"','"+getUserVar("userID")+"','"+psuserip+"'); end;";
    System.out.println(s);
	 try
			{		    
				 result= this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
}  

public String getSession(){
	String session = "";
	File file = new File (this.getSysConst("ConfigCompiledDir")+"pttb/laphd/chonso/session.neo");
	if (!file.exists() || file.lastModified()<System.currentTimeMillis()-50000){
			boolean del = true;
			if (file.exists()){
				del=file.delete();
			}
			session = Connect.login();
			if (del){
				try {
					FileUtils.writeStringToFile(file,session);
				} catch (IOException e) {
					session = e.getMessage();
				}
			}
		}else{
			try {
				session = FileUtils.readFileToString(file);
			} catch (IOException e) {}
		}
		return session;
}
	
}