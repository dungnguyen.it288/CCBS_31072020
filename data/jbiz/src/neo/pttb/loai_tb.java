package neo.pttb;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import cosaldap.Base64.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;
/*
import neo.smartui.common.CesarCode;
import neo.smartui.common.ConvertFont;
import neo.smartui.process.*;
import neo.smartui.report.*;
*/

public class loai_tb extends NEOProcessEx {
	public String new_loai_tb(
			String pschema,
			String psma_loai,
			String pten_loai,
			String ploai_tb,
			String pten,
			String ptra_sau,
			String pthu_tu,
			String pnhom,
			String pthu_nghiem,
			String puserid,
			String puserip) throws Exception {
				String s ="begin ? := admin_v2.pk_loai_tb.new_loai_tb("
					+"'"+pschema+"',"
					+"'"+ConvertFont.UtoD(psma_loai)+"',"
					+"'"+ConvertFont.UtoD(pten_loai)+"',"
					+"'"+ConvertFont.UtoD(ploai_tb)+"',"
					+"'"+ConvertFont.UtoD(pten)+"',"
					+"'"+ptra_sau+"',"
					+"'"+pthu_tu+"',"
					+"'"+ConvertFont.UtoD(pnhom)+"',"
					+"'"+pthu_nghiem+"',"
					+"'"+puserid+"',"
					+"'"+puserid+"'"
					+"); end;";
			
	       return this.reqValue("", s);
		}
		public String del_loai_tb(
			String pschema,
			String pid,
			String puserid,
			String puserip) throws Exception {
				String s ="begin ? := admin_v2.pk_loai_tb.del_loai_tb("
					+"'"+pschema+"',"
					+"'"+pid+"',"
					+"'"+puserid+"',"
					+"'"+puserid+"'"
					+"); end;";
		
			return this.reqValue("", s);
		}
		public String edit_loai_tb(
			String pschema,
			String pma_loai,
			String pten_loai,
			String ploai_tb,
			String pten,
			String ptra_sau,
			String pthu_tu,
			String pnhom,
			String pthu_nghiem,
			String puserid,
			String puserip) throws Exception {
				String s ="begin ? := admin_v2.pk_loai_tb.edit_loai_tb("
					+"'"+pschema+"',"
					+"'"+pma_loai+"',"
					+"'"+ConvertFont.UtoD(pten_loai)+"',"
					+"'"+ConvertFont.UtoD(ploai_tb)+"',"
					+"'"+ConvertFont.UtoD(pten)+"',"
					+"'"+ptra_sau+"',"
					+"'"+pthu_tu+"',"
					+"'"+ConvertFont.UtoD(pnhom)+"',"
					+"'"+pthu_nghiem+"',"
					+"'"+puserid+"',"
					+"'"+puserid+"'"
					+"); end;";
			return this.reqValue("", s);
		}
		
		public String cn_dv_chuyendoi(
			String psLoaiB,
			String psMa_dv,
			String puserid,
			String puserip) throws Exception {
				String s ="begin ? := neo.pkg_config.regis_change_type("
					+"'"+psLoaiB+"',"
					+"'"+psMa_dv+"',"			
					+"'"+puserid+"',"
					+"'"+puserid+"'"
					+"); end;";			
			return this.reqValue("CMDVOracleDS", s);
		}
		
		public String cn_dichvu(
			String psLoaiTB,
			String psMa_dv,
			String puserid,
			String puserip) throws Exception {
				String s ="begin ? := neo.pkg_config.regis_services("
					+"'"+psLoaiTB+"',"	
					+"'"+psMa_dv+"',"
					+"'"+puserid+"',"
					+"'"+puserip+"'"
					+"); end;";					
			return this.reqValue("CMDVOracleDS", s);
		}
}
