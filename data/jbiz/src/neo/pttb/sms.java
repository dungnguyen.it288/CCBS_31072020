package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class sms extends NEOProcessEx{
	public String new_config_sms(
			String pschema,
			String psSql,
			String psdata,
			String pnote,
			String pparameter,
			String pstatus,
			String puserid,
			String puserip) throws Exception {
				String s ="begin ? :="+pschema+"pkg_sms.new_config_sms("
					+"'"+pschema+"',"
					+"'"+psSql+"',"
					+"'"+psdata+"',"
					+"'"+pnote+"',"
					+"'"+pparameter+"',"
					+"'"+pstatus+"',"
					+"'"+puserid+"',"
					+"'"+puserip+"'"
					+"); end;";
			System.out.println(s);
	       return this.reqValue("", s);
		}
		public String del_config_sms(
			String pschema,
			String pid,
			String puserid,
			String puserip) throws Exception {
				String s ="begin ? :="+pschema+"pkg_sms.del_config_sms("
					+"'"+pschema+"',"
					+"'"+pid+"',"
					+"'"+puserid+"',"
					+"'"+puserip+"'"
					+"); end;";
			System.out.println(s);
			return this.reqValue("", s);
		}
		public String edit_config_sms(
			String pschema,	
			String psSql,
			String psdata,
			String pid,
			String pnote,
			String pparameter,
			String pstatus,
			String puserid,
			String puserip) throws Exception {
				String s ="begin ? :="+pschema+"pkg_sms.edit_config_sms("
						+"'"+pschema+"',"
					+"'"+psSql+"',"						
					+"'"+psdata+"',"
					+"'"+pid+"',"
					+"'"+pnote+"',"
					+"'"+pparameter+"',"
					+"'"+pstatus+"',"
					+"'"+puserid+"',"
					+"'"+puserip+"'"
					+"); end;";
			System.out.println(s);
			return this.reqValue("", s);
		}
}
