package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class doanhnghiep extends NEOProcessEx {
	public String new_doanhnghiep(		
		String pschema,
		String puserid,
		String puserip,
		String pdataschema,
		String pten,
		String pdiachi,
		String pngay_tl,
		String pnguoi_dd,
		String pso_dd,
		String pquan_id,
		String pphuong_id,
		String pngaysinh) throws Exception {
			String s ="begin ? :="+pschema+"pkg_doanhnghieps.new_doanhnghiep_funct("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+pdataschema+"',"				
				+"'"+ConvertFont.UtoD(pten)+"',"
				+"'"+pngay_tl+"',"
				+"'"+ConvertFont.UtoD(pnguoi_dd)+"',"
				+"'"+pngaysinh+"',"
				+"'"+pso_dd+"',"
				+"'"+pquan_id+"',"
				+"'"+pphuong_id+"',"
				+"'"+ConvertFont.UtoD(pdiachi)+"'"						
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	
	public String edit_doanhnghiep(
		String pschema,
		String puserid,
		String puserip,
		String pdataschema,
		String pten,
		String pdiachi,
		String pngay_tl,
		String pnguoi_dd,
		String pso_dd,
		String pquan_id,
		String pphuong_id,
		String pngaysinh,
		String pma_dn) throws Exception {
			String s ="begin ? :="+pschema+"pkg_doanhnghieps.edit_doanhnghiep_funct("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"
				+"'"+pdataschema+"',"
				+"'"+ConvertFont.UtoD(pten)+"',"
				+"'"+pngay_tl+"',"
				+"'"+ConvertFont.UtoD(pnguoi_dd)+"',"
				+"'"+pngaysinh+"',"
				+"'"+pso_dd+"',"
				+"'"+pquan_id+"',"
				+"'"+pphuong_id+"',"
				+"'"+ConvertFont.UtoD(pdiachi)+"',"
				+"'"+pma_dn+"'"			
				+"); end;";
		System.out.println(s);
		//return s;
		return this.reqValue("", s);
	}
	public String delete_dn(String pschema,
		String puserid,
		String puserip,
		String pdataschema,
		String plist) throws Exception
	{
		String s ="begin ? :="+pschema+"pkg_doanhnghieps.delete_doanhnghiep("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"
				+"'"+pdataschema+"',"				
				+"'"+plist+"'"						
				+"); end;";
		System.out.println(s);
		//return s;
		return this.reqValue("", s);
	}
	//khach hang
	public String new_khachhang(		
		String pschema,
		String puserid,
		String puserip,
		String pdataschema,
		String pma_kh,
		String pma_dn
		) throws Exception {
			String s ="begin ? :="+pschema+"pkg_doanhnghieps.new_dn_kh_funct("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"								
				+"'"+pdataschema+"',"								
				+"'"+pma_dn+"',"				
				+"'"+pma_kh+"'"						
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String edit_khachhang(
		String pschema,
		String puserid,
		String puserip,
		String pma_dn,
		String pma_kh,
		String pid) throws Exception {
			String s ="begin ? :="+pschema+"pkg_doanhnghieps.edit_dn_kh_funct("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+pma_dn+"',"
				+"'"+pma_kh+"',"				
				+"'"+pid+"'"			
				+"); end;";
		System.out.println(s);
		//return s;
		return this.reqValue("", s);
	}
	public String delete_khachhang(String pschema,
		String puserid,
		String puserip,
		String pdataschema,
		String plist) throws Exception
	{
		String s ="begin ? :="+pschema+"pkg_doanhnghieps.delete_kh("
				+"'"+pschema+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"',"				
				+"'"+pdataschema+"',"				
				+"'"+plist+"'"						
				+"); end;";
		System.out.println(s);
		//return s;
		return this.reqValue("", s);
	}
	
}