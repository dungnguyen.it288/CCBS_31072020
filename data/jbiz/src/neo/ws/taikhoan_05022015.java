package neo.ws;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.*;
import javax.xml.namespace.QName;
import org.apache.axis.encoding.XMLType;
import java.text.SimpleDateFormat;
import java.util.Date;

public class taikhoan extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("QLTN Gach no");
		//funcschema=getSysConst("FuncSchema");
    	//dataschema=getUserVar("sys_dataschema");
    	//userid=getUserVar("userID");		
	}
	
	public String getTKOnline(String pSothuebao, String ID){
		String ret="";
		try {
        	String endpoint = "http://192.9.210.236:8080/axis/services/PPSViNaPhone?wsdl";
        	Service  service = new Service();
       		Call     call    = (Call) service.createCall();
       		
       		call.addParameter("in0", XMLType.XSD_LONG, javax.xml.rpc.ParameterMode.IN);
       		call.addParameter("in1", XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN);
       		call.addParameter("in2", XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN);
       		
       		call.setReturnType( XMLType.XSD_STRING  );
  
        	call.setTargetEndpointAddress( new java.net.URL(endpoint) );
        	call.setOperationName( "getSubscriber" );
			
			SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyyMMddHHmm");
            Date date = new Date();
            String cur_=simpledateformat.format(date);
						
        	ret = (String) call.invoke( new Object[] { new Long(ID) , pSothuebao , cur_ } );
  
      	} catch (Exception e) {
        	System.err.println(e.toString());
      	}
      	return ret;
    }
	///NEW_SPS
	public String rec_taikhoan(
    	String pSothuebao
    ){ 
		String val_ = ""; 		
		String s = "begin ? :=admin_v2.pkg_api_hlr.get_balance_pps('"+pSothuebao+"'); end;";   		
		
		try
			{		    
				 val_=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       val_ = ex.getMessage();
			}
		return val_;
	}
	/* ///OLD
	public String rec_taikhoan(
    	String pSothuebao
    ){
    	
    	String id_="948261186";
    	try {
    		id_=reqValue("begin ?:=getSeq('TK_ONLINE'); end;");
    	} catch (Exception ex){}
		String ret = getTKOnline(pSothuebao,id_);		
    	ret=ret.replace('|','#');
		String[] aRet = ret.split("#");
		
		return "select to_char('"+aRet[3]+"','fm999,999,999,999.00') tkchinh, to_char('"+aRet[4]+"','fm999,999,999,999.00') tkphu , to_char('"+aRet[5]+"','fm999,999,999,999.00') tkphu1, to_char('"+aRet[6]+"','fm999,999,999,999.00') tkphu2 from dual";
		
	}
	*/
	
}