package neo.ws;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.*;
import javax.xml.namespace.QName;
import org.apache.axis.encoding.XMLType;
import java.text.SimpleDateFormat;
import java.util.Date;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import java.util.*;
import javax.sql.*;

public class WSDL
{	
	
	String ppsWSDL_ = "http://192.9.210.236:8080/axis/services/PPSViNaPhone?wsdl";
	String gpWSDL_  = "http://192.9.210.236:8080/axis/services/gphone?wsdl";
	String tgWSDL_	= "http://10.156.4.125:8080/axis/services/spi?wsdl";
	
	public void WSDL () {
	
	}
	
	public String getDate(){
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        String cur_=simpledateformat.format(date);
        return cur_;
	}
	
	public String vnpLaytt_Thuebao(String pSothuebao, String pID, String pDate){
		String ret="";
		try {
        	String endpoint = ppsWSDL_;
        	Service  service = new Service();
        	Call     call    = (Call) service.createCall();
        	
       		
       		call.addParameter("in0", XMLType.XSD_LONG, javax.xml.rpc.ParameterMode.IN);
       		call.addParameter("in1", XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN);
       		call.addParameter("in2", XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN);
       		
       		call.setReturnType( XMLType.XSD_STRING  );
        	call.setTargetEndpointAddress( new java.net.URL(endpoint) );
        	call.setOperationName( "getSubscriber" );			
						
        	ret = (String) call.invoke( new Object[] { new Long(pID) , pSothuebao , pDate } );
      	} catch (Exception e) {
        	System.err.println(e.toString());
      	}
      	return ret;
    }
    
    public String gpLaytt_Thuebao(String pSothuebao, String pID, String pDate){
		String ret="";
		try {
        	String endpoint = gpWSDL_;
        	Service  service = new Service();
       		Call     call    = (Call) service.createCall();

       		
       		call.addParameter("in0", XMLType.XSD_LONG, javax.xml.rpc.ParameterMode.IN);
       		call.addParameter("in1", XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN);
       		call.addParameter("in2", XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN);
       		
       		call.setReturnType( XMLType.XSD_STRING  );
        	call.setTargetEndpointAddress( new java.net.URL(endpoint) );
        	call.setOperationName( "getSubscriber" );			
						
        	ret = (String) call.invoke( new Object[] { new Long(pID) , pSothuebao , pDate } );
			
      	} catch (Exception e) {
        	System.err.println(e.toString());
      	}
      	return ret;
    }
    
    public String dangky3G(String pID, String pSothuebao, String pMaDV, String psMethodName){
		String ret="";
		try {
	
        	String endpoint = tgWSDL_;
        	Service  service = new Service();
			
       		Call     call    = (Call) service.createCall();
		
       		call.setUsername("neo");
        	call.setPassword("neo#1");

			String date_ = getDate();
    	       		
       		call.addParameter("in0", XMLType.XSD_LONG, javax.xml.rpc.ParameterMode.IN);
       		call.addParameter("in1", XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN);
       		call.addParameter("in2", XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN);
       		call.addParameter("in3", XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN);
			
       		call.setReturnType( XMLType.XSD_STRING  );
        	call.setTargetEndpointAddress( new java.net.URL(endpoint) );
			
        	call.setOperationName( psMethodName );			
			
        	ret = (String) call.invoke( new Object[] { new Long(pID) , pSothuebao, pMaDV, date_ } );
			

      	} catch (Exception e) {
			e.printStackTrace();
      	}
      	return ret;
    }
	public static void main(String [] args){
    	WSDL wsdl=new WSDL();
    	
    	String a="";
    	//a=wsdl.dangky3G("84913235623","MI_M10","1");
    	System.out.println(a);
    }
}