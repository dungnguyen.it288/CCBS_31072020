package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class pttb_danhmuc
extends NEOProcessEx {	public void run()
	{
		System.out.println("neo.pttb_yeucau_ks was called");
	}
	   
   public String getUrlDsBangGia(String userInput,String piloaihinh) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/Danhmuc/ajax_ds_nhapgiaHD"
        +"&"+CesarCode.encode("userinput")+"="+userInput
		+"&"+CesarCode.encode("loaihinh")+"="+piloaihinh
        ;
   }
   
   public String tracuu_blacklist( String piPAGEID,
    							   	String piREC_PER_PAGE,    							
 	    							String psso_tb,
									String psso_gt
								   )
	{
		return "/main?" + CesarCode.encode("configFile") + "=qltn/dulieu/giayto/ajax_ds_giayto"
			+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
			+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
			+ "&" + CesarCode.encode("so_tb") + "=" + psso_tb
			+ "&" + CesarCode.encode("so_gt") + "=" + psso_gt
			;
	}
			//Lay danh sach nguoi ky hop dong
	public String ds_nguoikyhdong(String piPAGEID,String piREC_PER_PAGE,String piuserInput)
	{
		String s = "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/nguoikyhdong/ajax_ds_nguoikyhdong"
		+ "&" + CesarCode.encode("sys_pageid") +"="+ piPAGEID
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
		+ "&" + CesarCode.encode("userInput") + "=" + piuserInput
		;
		//System.out.println(s);
		return s;
	}

	public String update_nguoikyhdong(String schema
			,String piINSERTORUPDATE
			,String psMANGUOI
			,String psTENNGUOI
			,String piLOAIGT_ID
			,String psSOGT
			,String psNGAYGT
			,String psNOICAPGT
			,String psDIENTHOAILH
			,String psDIACHI
			,String piDONVIQL
		)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "PTTB_CAPNHAT.capnhat_nguoikyhdong('"+getUserVar("userID")
					+"','"+getUserVar("sys_agentcode")
					+"','"+getUserVar("sys_dataschema")+"',"
					+ piINSERTORUPDATE
					+ ",'" + psMANGUOI + "'"
					+ ",'" + psTENNGUOI + "'"
					+ ",'" + piLOAIGT_ID + "'"
					+ ",'" + psSOGT + "'"
					+ ",'" + psNGAYGT + "'"
					+ ",'" + psNOICAPGT + "'"
					+ ",'" + psDIENTHOAILH + "'"
					+ ",'" + psDIACHI + "'"
					+ ",'" + piDONVIQL + "'"
					+ ");"
					+ "end;";
		//System.out.println(s);
		return s;
	}

	public String del_nguoikyhdong(String schema,String manguoi)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "PTTB_CAPNHAT.xoa_nguoikyhdong('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + manguoi + "'"
		+ ");"
		+ " end;";
		//System.out.println(s);
		return s;
	}
 /*
	Ngay sua doi: 10/06/2009
    Nguoi sua: Luattd
    Chuyen sang package: PTTB_TEST_VIET--> PTTB_CAPNHAT	  
 */    
 public String update_frmGiaHD(String schema
									,String piMAHD_ID
									,String piLOAITB_ID
									,String piGIATIEN
									,String piVAT
									,String piLOAITIEN_ID
									,String piKIEULD_ID
								
									) {
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_DULIEU.danhmuc_update_frmGiaHD('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       							+piMAHD_ID+","
								+piLOAITB_ID+","
								+piGIATIEN+","
								+piVAT+","
								+piLOAITIEN_ID+","
								+piKIEULD_ID

																
        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }

 // Nhan vien xu ly cuoc
    public String laydanhsach_nv() {
		return "/main?"+CesarCode.encode("configFile")+"=qltn/dulieu/nhanvien_xlc/ajax_ds_nhanvien";
	}
	
 
  public String update_nhanvien(String schema
								,String piDONVIQL_ID
								,String piMANV_ID
								,String psTENNV
								,String psLANHDAO
								, String CHUCDANH
								
							  ) {

    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.update_nhanvien('"+getUserVar("userID")
			+"','"+getUserVar("sys_agentcode")
			+"','"+getUserVar("sys_dataschema")+"',"
        +"'"+piDONVIQL_ID+"',"
		+"'"+piMANV_ID+"',"
		+"'"+psTENNV+"',"		
		+"'"+psLANHDAO+"',"	
		 +"'"+CHUCDANH+"'"
        +");"
        +"end;"
        ;
    
    return s;
  }
  //Xoa nhan vien
 public String delete_nhanvien(String schema,String piMANV_ID) 
 { String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.xoa_nhanvien('"+getUserVar("userID")
			+"','"+getUserVar("sys_agentcode")
			+"','"+getUserVar("sys_dataschema")+"',"
       		+piMANV_ID
			+");"
			+"end;"
			;
		
		return s;
  }    
// End 
 /*
	Ngay sua doi: 10/06/2009
    Nguoi sua: Luattd
    Chuyen sang package: PTTB_TEST_VIET--> PTTB_CAPNHAT	  
 */ 
  public String delete_frmGiaHD(String schema
									,String piMAHD_ID
									,String piLOAITB_ID
									,String piLOAITIEN_ID
									,String piKIEULD_ID
								
									) {
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_DULIEU.danhmuc_delete_frmGiaHD('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       							+piMAHD_ID+","
								+piLOAITB_ID+","
								+piLOAITIEN_ID+","
								+piKIEULD_ID

																
        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }
  
/*Ngay sua doi: 2007/12/06
      Nguoi sua: TuanNA
 */  
	public String getUrlDsDMThietbi() {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/Danhmuc/giathietbi/ajax_ds_DMgiaTB";
	}     
   
 /*Ngay sua doi: 2007/12/06
      Nguoi sua: TuanNA
      Chuyen sang package: PTTB_TEST_VIET--> PTTB_CAPNHAT	  
 */
	public String update_frmGiaTB(String schema
									,String piDICHVUVT_ID
									,String piTHIETBI_ID
									,String psTEN_THIETBI
									) {

    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.danhmuc_update_frmdm_thietbi('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       	+piDICHVUVT_ID+","
		+"'"+piTHIETBI_ID+"',"
		+"'"+psTEN_THIETBI+"'"		
        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }  
 
 /*Ngay sua doi: 2007/12/06
      Nguoi sua: TuanNA
     Chuyen sang package: PTTB_TEST_VIET--> PTTB_CAPNHAT		  
 */
 public String delete_frmGiaTB(String schema,String piDICHVUVT_ID,String piTHIETBI_ID) 
 { String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.danhmuc_delete_frmDM_ThietBi('"+getUserVar("userID")
			+"','"+getUserVar("sys_agentcode")
			+"','"+getUserVar("sys_dataschema")+"',"
       		+piDICHVUVT_ID+","
			+piTHIETBI_ID
			+");"
			+"end;"
			;
		System.out.println(s);
		return s;
  }
  
 /*Ngay sua doi: 2007/12/06
      Nguoi sua: TuanNA
 */
 public String load_cboloaitb() {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/giathietbi/ajax_cboloaitb";
   }   
  /******************************************************************************/
  
  /*Ngay sua doi: 2007/12/07
      Nguoi sua: TuanNA
     Bang gia thiet bi
 */ 
  public String getUrlDsbanggiaTB(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/Danhmuc/giathietbi/ajax_banggiaTB"
        +"&"+CesarCode.encode("userinput")+"="+userInput
        ;
   }  
   
 /*Ngay sua doi: 2007/12/07
      Nguoi sua: TuanNA
     Chuyen sang package: PTTB_TEST_VIET--> PTTB_CAPNHAT		
	 */  
  public String update_frm_banggiatb(String schema
									,String pichungloai_id
									,String pstenloai
									,String pigia_thd									
									,String pivat_thd
									,String pigia_nhd
									,String pivat_nhd
									,String pithietbi_id
									,String pidonviql_id									
									) {																
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.danhmuc_update_frm_banggiatb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       							+"'"+pichungloai_id +"',"
								+"'"+pstenloai +"',"
								+"'"+pigia_thd +"',"
								+"'"+pivat_thd +"',"
								+"'"+pigia_nhd +"',"
								+"'"+pivat_nhd +"',"
								+"'"+pithietbi_id+"',"
								+"'"+pidonviql_id+"'"
								+");"
								+"end;"
								;
		System.out.println(s);
    return s;
  }  
  
  /*Ngay sua doi: 2007/12/07
      Nguoi sua: TuanNA
     Chuyen sang package: PTTB_TEST_VIET--> PTTB_CAPNHAT		
	 */   
 public String delete_frmbanggiatb(String schema
									,String pichungloai_id									
									,String pithietbi_id									
									) {
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.danhmuc_delete_frmbanggiatb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
		+"'"+pichungloai_id +"',"
		+"'"+pithietbi_id+"'"														
		+");"
		+"end;"
		;
	System.out.println(s);
    return s;
  }  
 //---------------------------------------------------  

	public String getUrlDs_Nhapquan() {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/Danhmuc/ajax_ds_nhapquan"
            ;
   }  	
   public String search_phos(String pstenpho,String piphuong_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/Danhmuc/ajax_search_Pho"
        +"&"+CesarCode.encode("tenpho")+"="+pstenpho
		+"&"+CesarCode.encode("phuong_id")+"="+piphuong_id
        ;
   }  
   public String delete_frmQuans(String schema
									,String piQUAN_ID
														
									) {
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.danhmuc_delete_frmQuans('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       							+piQUAN_ID
								        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;
  }
  public String update_frmQuans(String schema
									,String piQUAN_ID
									,String psTENQUAN
									,String piTINH_ID
									) {
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.danhmuc_update_frmQuans('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       							+piQUAN_ID+","
								+"'"+psTENQUAN+"',"
								+"'"+piTINH_ID+"'"
		        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }  
  
  public String getUrlDsPhong(String psMacq) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/ajax_phongban"
		+"&"+CesarCode.encode("macq") + "=" + psMacq
        ;
	}
	
	
	public String Insert_frmphongban(String schema
									,String psMacq
									,String psPhong
									,String pstenp
									
									) {
	 String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.Danhmuc_Insert_frmcoquan('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMacq+"',"
								+"'"+psPhong+"',"
								+"'"+pstenp+"'"
								
        +");"
        +"end;"
        ;

    
		System.out.println(s);
    return s;
	}
	
	public String update_frmphongban(String schema
									,String psMacq
									,String psPhong
									,String phong
									,String pstenp
		
									) {
	 String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.Danhmuc_update_frmcoquan('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMacq+"',"
								+"'"+psPhong+"',"
								+"'"+phong+"',"
								+"'"+pstenp+"'"
								
        +");"
        +"end;"
        ;

    
		System.out.println(s);
    return s;
	}
	
	public String delete_frmphongban(String schema
									,String psMacq
									,String psPhong
									) {
	 String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.Danhmuc_Delete_frmcoquan('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMacq+"',"
								+"'"+psPhong+"'"
								+");"
        +"end;"
        ;

    
		System.out.println(s);
    return s;
	}
	
	public String getUrlDs_phuong(String piquan_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/Danhmuc/ajax_ds_nhapPhuong"
        +"&"+CesarCode.encode("quan_id")+"="+piquan_id
        ;
   }  
   public String update_frmPhuongs(String schema
									,String piPHUONG_ID
									,String psTENPHUONG
									,String psPREFIX
									,String piQUAN_ID
									,String piMUCCUOC_ID

									) {
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.danhmuc_update_frmPhuongs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       							+piPHUONG_ID+","
								+"'"+psTENPHUONG+"',"
								+"'"+psPREFIX+"',"
								+piQUAN_ID+","
								+piMUCCUOC_ID

        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }  
  public String delete_frmphuongs(String schema
									,String piquan_id
									,String piphuong_id
								) {

    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.danhmuc_delete_frmPhuongs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       							+piquan_id+","
								+piphuong_id
								+");"
				        +"end;"
				        ;
		System.out.println(s);
    return s;
	}
  public String load_cbophuong(String piquan_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/Danhmuc/ajax_cbophuong"
        +"&"+CesarCode.encode("quan_id")+"="+piquan_id
        ;
   }  
   
   public String load_DSpho(String piphuong_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/Danhmuc/ajax_cboDSpho"
        +"&"+CesarCode.encode("phuong_id")+"="+piphuong_id
        ;
   }  
    public String update_frmphos(String schema
									,String piPHUONG_ID
									,String psDSpho_id
									
									) {
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.danhmuc_update_frmPhos('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       							+piPHUONG_ID+","
								+"'"+psDSpho_id+"'"
								
        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }  
  public String getUrlDsPhos() {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/Danhmuc/ajax_DSnhappho"
                ;
   }  
   
   public String update_frmNhapPho(String schema
									,String piPHO_ID
									,String psTENPHO
									
									) {
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.danhmuc_update_frmNhapPho('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       							+piPHO_ID+","
								+"'"+psTENPHO+"'"
				
        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }  
  public String delete_frmNhapPho(String schema
									,String piPHO_ID
								) {

    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.danhmuc_delete_frmNhapPho('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       							+piPHO_ID
								
								+");"
				        +"end;"
				        ;
		System.out.println(s);
    return s;
	}
//Lay danh sach phan loai kho so
  public String layds_so_available(int piloaiso,String sSQL)
{
    String s= "/main?"+CesarCode.encode("configFile")+"=pttb/Danhmuc/ajax_cboAvailableList"
					  +"&"+CesarCode.encode("loai_so")+"="+piloaiso 
					  +"&"+CesarCode.encode("sSQL")+"=" + sSQL 
					  + "&sid="+Math.random();  					  		  					  
		System.out.println(s);
    return s;
	}
		

public String layds_so_phanloai(int piloaiso,String sSQL) 
{
    String s= "/main?"+CesarCode.encode("configFile")+"=pttb/Danhmuc/ajax_DSPhanLoai"
					+"&"+CesarCode.encode("loai_so")+"="+piloaiso 									 
					+"&"+CesarCode.encode("sSQL")+"=" + sSQL 					 					
					+ "&sid="+Math.random();
					   
		System.out.println(s);
		return s;
	} 
	
 public String phanloaikhoso(String schema
							,int piLOAISO
							,int piDVQL_ID
							,int piTRAMVT_ID
							,String psSOMAYS
							) {
	
    String s= "begin ? := "+schema+"PTTB_TEST_DONG.phanloaikhoso('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       							+	   piLOAISO +","
								+	   piDVQL_ID +","
								+	   piTRAMVT_ID +","
								+"'("+  psSOMAYS +")'"
								+");"
								+"end;";								

    System.out.println(s);
    return s;

  }  

  //Edit by Luattd	
   //Date: 22/10/2007
    public String layDSPhuongTheoQuan(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/frmCapNhatNVTC_ajax_phuong"
        +"&"+CesarCode.encode("quanid")+"="+userInput
        ;
  }  
  
   public String layDSPhoTheoPhuong(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/frmCapNhatNVTC_ajax_pho"
        +"&"+CesarCode.encode("phuongid")+"="+userInput
        ;
  }  
  
   /* Lay danh sach ma nhan vien theo lich su thay doi*/
   public String lay_ls_thaydoi_manv(String psUserInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/frmLS_ThayDoi_MaNV_ajax"
		+"&"+CesarCode.encode("makh")+"="+psUserInput
        ;
  }
  
   public String layDSKH(String piPAGEID,String piREC_PER_PAGE,String psmakh, String pstenkh,String psquan,String psphuong,String pspho, String psmabuucuc, String pi_manv, String pi_mabc) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/frmCapNhatNVTC_ajax_DSKH"
		+"&"+CesarCode.encode("sys_pageid") +"="+ piPAGEID	           
		+"&"+CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE        
	    +"&"+CesarCode.encode("makh")+"="+psmakh
		+"&"+CesarCode.encode("tenkh")+"="+pstenkh
		+"&"+CesarCode.encode("quan")+"="+psquan
		+"&"+CesarCode.encode("phuong")+"="+psphuong
		+"&"+CesarCode.encode("pho")+"="+pspho
		+"&"+CesarCode.encode("buucucthu")+"="+psmabuucuc
		+"&"+CesarCode.encode("ma_nv")+"="+pi_manv
		+"&"+CesarCode.encode("ma_bc")+"="+pi_mabc
        ;
  }  
  // Them buucuc quan ly khi gan nhan vien thu cuoc - HNI
	public String capnhatNVTC(String psMaKH
							 ,String psMaNV
							 ,String psMaT
							 ,String psTTTuyen
							 ,String psbuucuc
							 ,String pscapnhat
							 ,String psDataSchema
							 ,String psFuncSchema
							 ,String psUserId
							 ,String psUserIp
							) {
	
    String s= "begin ? := admin_v2.pttb_chucnang.capnhatNVTC('"+psMaKH
																 +"','"+psMaNV
																 +"','"+psMaT
																 +"','"+psTTTuyen
																 +"','"+psbuucuc
																 +"','"+pscapnhat
																 +"','"+psDataSchema
																 +"','"+psUserId
																 +"','"+psUserIp +"'"
							
															+");"
															+"end;";								
   
    return s;
  }    
  
  //Sua lai don vi quan ly. Ngay 29/01/2008
  public String getUrlDsDMDonViQL() {
		return "/main?"+CesarCode.encode("configFile")+"=qltn/dulieu/donvi_ql/ajax_ds_donviql";
	}
	
  //Update & Insert don vi quan ly	
  public String update_donviql(String schema
								,String piDONVIQL_ID
								,String psTEN_DV
								,String psLANHDAO
								,String psTAIKHOAN
								,String psMS_THUE
								,String piNGANHANG_ID
								,String piCHUCDANH_ID
								,String psTEN_RPT
								,String piNHOMDV_ID
								,String psTEN_BIEN
								,String piINDANHBA
								,String psTEN_HD
								,String psDIACHI_HD
								,String psDIENTHOAI
								,String psTEN_DAI
								,String psFAX
								,String piQUAN_ID
								,String psGPKD
								,String psNOICAP_GPKD
								,String psNGAYCAP_GPKD
								
								
							  ) {

    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.update_donviql('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       	+"'"+piDONVIQL_ID+"',"
		+"'"+psTEN_DV+"',"		
		+"'"+psLANHDAO+"',"		
		+"'"+psTAIKHOAN+"',"		
		+"'"+psMS_THUE+"',"		
		+"'"+piNGANHANG_ID+"',"		
		+"'"+piCHUCDANH_ID+"',"		
		+"'"+psTEN_RPT+"',"		
		+"'"+piNHOMDV_ID+"',"		
		+"'"+psTEN_BIEN+"',"		
		+"'"+piINDANHBA+"',"		
		+"'"+psTEN_HD+"',"		
		+"'"+psDIACHI_HD+"',"		
		+"'"+psDIENTHOAI+"',"		
		+"'"+psTEN_DAI+"',"		
		+"'"+psFAX+"',"				
		+"'"+piQUAN_ID+"',"
		+"'"+psGPKD+"',"
		+"'"+psNOICAP_GPKD+"',"
		+"'"+psNGAYCAP_GPKD+"'"
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;
  }
//Xoa don vi quan ly
 public String delete_donviql(String schema,String piDONVIQL_ID) 
 { String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.xoa_donviql('"+getUserVar("userID")
			+"','"+getUserVar("sys_agentcode")
			+"','"+getUserVar("sys_dataschema")+"',"
       		+piDONVIQL_ID
			+");"
			+"end;"
			;
		System.out.println(s);
		return s;
  }  
	
//Sua lai buu cuc thu. Ngay 29/01/2008
  public String getUrlDsDMBuuCucThu() {
		return "/main?"+CesarCode.encode("configFile")+"=qltn/dulieu/buucucthu/ajax_ds_buucucthu";
	}

//Update & Insert buu cuc thu	
  public String update_buucucthu(String schema
								,String piMA_BC
								,String psTEN_BC
								,String psDIACHI
								,String psDIENTHOAI
								,String psFAX
								,String psDVTHU_ID
								,String psNGUOILIENHE
								,String piTCBC_ID
								,String piDEBTYPE
								,String psMAQUAY
								,String piDONVIQL_ID
								,String piLOAI_BC
								,String psTEN_VIET_TAT								
								,String piQUAN_ID
							  ) {

    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.update_buucucthu('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       	+"'"+piMA_BC+"',"
		+"'"+psTEN_BC+"',"		
		+"'"+psDIACHI+"',"		
		+"'"+psDIENTHOAI+"',"		
		+"'"+psFAX+"',"		
		+"'"+psDVTHU_ID+"',"		
		+"'"+psNGUOILIENHE+"',"		
		+"'"+piTCBC_ID+"',"		
		+"'"+piDEBTYPE+"',"		
		+"'"+psMAQUAY+"',"		
		+"'"+piDONVIQL_ID+"',"		
		+"'"+piLOAI_BC+"',"		
		+"'"+psTEN_VIET_TAT+"',"							
		+"'"+piQUAN_ID+"'"		
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;
  }	
	
//Xoa buu cuc thu
	public String delete_buucucthu(String schema,String piMA_BC) 
	{ String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.xoa_buucucthu('"+getUserVar("userID")
			+"','"+getUserVar("sys_agentcode")
			+"','"+getUserVar("sys_dataschema")
			+"','"+piMA_BC
			+"');"
			+"end;"
			;
		//System.out.println(s);
		return s;
	}  		
	
//Sua lai ngan hang. Ngay 12/02/2008
  public String getUrlDsDMnganhang() {
		return "/main?"+CesarCode.encode("configFile")+"=qltn/dulieu/nganhang/ajax_ds_nganhang";
	}	

//Update & Insert ngan hang	
  public String update_nganhang(String schema
								,String piNGANHANG_ID
								,String psTENVIETTAT
								,String psTEN
								,String psDIACHI
								,String psTAIKHOAN
								,String psSODIENTHOAI
								,String psSOFAX
								,String psNGUOILIENHE
								,String piDEBTYPE
								,String piLOAI_NH								
							  ) {

    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.update_nganhang('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       	+"'"+piNGANHANG_ID+"',"
		+"'"+psTENVIETTAT+"',"		
		+"'"+psTEN+"',"		
		+"'"+psDIACHI+"',"		
		+"'"+psTAIKHOAN+"',"		
		+"'"+psSODIENTHOAI+"',"		
		+"'"+psSOFAX+"',"		
		+"'"+psNGUOILIENHE+"',"		
		+"'"+piDEBTYPE+"',"											
		+"'"+piLOAI_NH+"'"		
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;
  }	
	
//Xoa ngan hang
	public String delete_nganhang(String schema,String piNGANHANG_ID) 
	{ String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.xoa_nganhang('"+getUserVar("userID")
			+"','"+getUserVar("sys_agentcode")
			+"','"+getUserVar("sys_dataschema")+"',"
       		+piNGANHANG_ID
			+");"
			+"end;"
			;
		//System.out.println(s);
		return s;
	}  	
	
//Sua lai khu vuc tinh cuoc. Ngay 13/02/2008
  public String getUrlDsDMkhuvuctc() {
		return "/main?"+CesarCode.encode("configFile")+"=qltn/dulieu/khuvuc_tc/ajax_ds_khuvuc_tc";
	}	
	
	//Update & Insert khu vuc tinh cuoc	
  public String update_khuvuctc(String schema
								,String piKHUVUC_ID
								,String psTEN_KV
								,String piQUAN_ID								
							  ) {

    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.update_khuvuctc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       	+"'"+piKHUVUC_ID+"',"
		+"'"+psTEN_KV+"',"														
		+"'"+piQUAN_ID+"'"		
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;
  }	
	
//Xoa  khu vuc tinh cuoc
	public String delete_khuvuctc(String schema,String piKHUVUC_ID) 
	{ String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.xoa_khuvuctc('"+getUserVar("userID")
			+"','"+getUserVar("sys_agentcode")
			+"','"+getUserVar("sys_dataschema")+"',"
       		+piKHUVUC_ID
			+");"
			+"end;"
			;
		//System.out.println(s);
		return s;
	} 
	
	/* Cap nhat danh muc ten sep*/
 public String get_ds_tensep() {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/ds_sep/danhmuc_tenseps_ajax";
	}	
		
//Update & Insert danh muc ten sep
 public String update_tensep(String schema,String psSchema,String piDONVIQL_ID,String psTEN_SEP,String piCHUCDANH_ID)
 {
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.update_danhmuctensep('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+psSchema+"',"		
       	+"'"+piDONVIQL_ID+"',"
		+"'"+psTEN_SEP+"',"														
		+"'"+piCHUCDANH_ID+"'"		
        +");"
        +"end;"
        ;    
    return s;
  }	
  
//Xoa  danh muc ten sep
	public String delete_tensep(String schema,String psSchema,String piDONVIQL_ID,String piCHUCDANH_ID) 
	{ String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.xoa_danhmuctensep('"+getUserVar("userID")
			+"','"+getUserVar("sys_agentcode")
			+"','"+psSchema+"',"		
			+"'"+piDONVIQL_ID+"',"
			+"'"+piCHUCDANH_ID+"'"		
			+");"
			+"end;"
			;
		//System.out.println(s);
		return s;
	} 
	
	//Lay danh sach nguoi van dong
	public String ds_nguoivandong(String piPAGEID,String piREC_PER_PAGE,String piuserInput) 
	{
		String s = "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/nguoivandong/ajax_ds_nguoivandong"
		+ "&" + CesarCode.encode("sys_pageid") +"="+ piPAGEID	           
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE 
		+ "&" + CesarCode.encode("userInput") + "=" + piuserInput 		
		;
		//System.out.println(s);
		return s;
	}  
	
	public String update_nguoivandong(String schema   
			,String piINSERTORUPDATE
			,String psMANGUOI
			,String psTENNGUOI
			,String piLOAIGT_ID
			,String psSOGT
			,String psNGAYGT
			,String psNOICAPGT
			,String psDIENTHOAILH
			,String psDIACHI
			,String piDONVIQL
		)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "PTTB_CAPNHAT.capnhat_nguoivandong('"+getUserVar("userID")
					+"','"+getUserVar("sys_agentcode")
					+"','"+getUserVar("sys_dataschema")+"',"
					+ piINSERTORUPDATE					
					+ ",'" + psMANGUOI + "'"
					+ ",'" + psTENNGUOI + "'"
					+ ",'" + piLOAIGT_ID + "'"
					+ ",'" + psSOGT + "'"		
					+ ",'" + psNGAYGT + "'"					
					+ ",'" + psNOICAPGT + "'"					
					+ ",'" + psDIENTHOAILH + "'"					
					+ ",'" + psDIACHI + "'"					
					+ ",'" + piDONVIQL + "'"					
					+ ");"
					+ "end;";			
		//System.out.println(s);
		return s;		
	}
	
	public String del_nguoivandong(String schema,String manguoi)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "PTTB_CAPNHAT.xoa_nguoivandong('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + manguoi + "'" 
		+ ");"
		+ " end;";
		//System.out.println(s);
		return s;
	}
		  //update ma tuyen
    public String update_ma_t(String schema
								,String QUAN_ID
								,String PHUONG_ID
								,String PHO_ID
								,String MA_T
								
							  ) {

    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.update_mat('"+getUserVar("sys_dataschema")+"','"+getUserVar("sys_agentcode")
		+"','"+QUAN_ID+"',"
		+"'"+PHUONG_ID+"',"
		+"'"+PHO_ID+"',"		
		+"'"+MA_T+"'"	
        +");"
        +"end;"
        ;
    
    return s;
  }
    //Xoa ma tuyen
	public String delete_ma_t(String schema,String PHUONG_ID,String MA_T) 
 { String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.xoa_mat('"+PHUONG_ID+"','"+MA_T+"'"	
			+");"
			+"end;"
			;
		
		return s;
  } 
   // Nhan vien thu cuoc
    public String laydanhsach_ma_t(String piquan_id) {
		return "/main?"+CesarCode.encode("configFile")+"=PTTB/danhmuc/Matuyen/ajax_ds_ma_t"
		        +"&"+CesarCode.encode("quan_id")+"="+piquan_id
        ;
	}
    public String layds_kh_hd( String  psinput,String psma_tb,String psloai_hd) {	
    return "/main?"+CesarCode.encode("configFile")+"=PTTB/danhmuc/kq_phat_hd/kq_phat_hopdong_ajax_DSKH"
		+"&"+CesarCode.encode("psinput")+"="+psinput
		+"&"+CesarCode.encode("psma_tb")+"="+psma_tb
		+"&"+CesarCode.encode("psloai_hd")+"="+psloai_hd;
  }  
     //giay to
  public String getUrlDsGT() {
		return "/main?"+CesarCode.encode("configFile")+"=qltn/dulieu/giayto/ajax_ds_giayto";
	}	

//Update & Insert giayto	
  public String update_giayto(String schema
								,String piGT_ID
								,String psSO_GT
								,String psNGAYCAP_GT
								,String psSO_GT1
								,String psNGAYCAP_GT1							
								,String psDIEN_THOAI
								,String psLY_DO	
								,String psSO_MAY
							  ) {
	    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.update_giayto('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       	+"'"+piGT_ID+"',"
		+"'"+psSO_GT+"',"		
		+"'"+psNGAYCAP_GT+"',"		
		+"'"+psSO_GT1+"',"		
		+"'"+psNGAYCAP_GT1+"',"		
		+"'"+psDIEN_THOAI+"',"
		+"'"+psLY_DO+"',"
		+"'"+psSO_MAY+"'"
        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;
  }	
	
//Xoa 
		public String delete_giayto(String schema,String piGT_ID) 
	{ String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_CAPNHAT.xoa_giayto('"+getUserVar("userID")
			+"','"+getUserVar("sys_agentcode")
			+"','"+getUserVar("sys_dataschema")+"',"
       		+piGT_ID
			+");"
			+"end;"
			;
		//System.out.println(s);
		return s;
	}  	 
	
	public NEOCommonData getSQL_dulieu_danhmuc() 
	{

	String s="";
	String sDataSchema = getUserVar("sys_dataschema");	
	String sUserId = getUserVar("userID");
	String sFuncSchema = getSysConst("FuncSchema");		
	String sAgentCode = getUserVar("sys_agentcode");
	
		s = "begin ?:="+sFuncSchema+"pttb_capnhat.ds_blacklist('"+sDataSchema+"','"
																		+sUserId 	+"','"
																		+sAgentCode +"'); end;";

	
	NEOExecInfo ei_ = new NEOExecInfo(s);

    return  new NEOCommonData(ei_);
  }
  public NEOCommonData getSQL_dulieu_nguoidung() 
	{

	String s="";
	String sDataSchema = getUserVar("sys_dataschema");	
	String sUserId = getUserVar("userID");
	String sFuncSchema = getSysConst("FuncSchema");		
	String sAgentCode = getUserVar("sys_agentcode");
	
	s = "begin ?:="+sFuncSchema+"PTTB_IMPORT_EXPORT.layds_export_dulieu_user('"+sDataSchema+"'); end;";	
	NEOExecInfo ei_ = new NEOExecInfo(s);

    return  new NEOCommonData(ei_);
  }
  	public  NEOCommonData getSQL_role_nguoidung()
	  {
   	String s="";
	String sDataSchema = getUserVar("sys_dataschema");
		String sUserId = getUserVar("userID");
	String sFuncSchema = getSysConst("FuncSchema");		
	String sAgentCode = getUserVar("sys_agentcode");	
	
	s = "begin ?:="+sFuncSchema+"PTTB_IMPORT_EXPORT.layds_export_role_user('"+sDataSchema+"'); end;";	
	NEOExecInfo ei_ = new NEOExecInfo(s);
	return  new NEOCommonData(ei_);
   	  }
	
	public  NEOCommonData getSQL_menu_nguoidung()
	{
		String s="";
		String sDataSchema = getUserVar("sys_dataschema");
			String sUserId = getUserVar("userID");
		String sFuncSchema = getSysConst("FuncSchema");		
		String sAgentCode = getUserVar("sys_agentcode");	
		
		s = "begin ?:="+sFuncSchema+"PTTB_IMPORT_EXPORT.layds_export_menu_user('"+sDataSchema+"'); end;";	
		NEOExecInfo ei_ = new NEOExecInfo(s);
		return  new NEOCommonData(ei_);
   	  }  
	
	public NEOCommonData getSQL_dulieu_nguoivd() 
	{

	String s="";
	String sDataSchema = getUserVar("sys_dataschema");	
	String sUserId = getUserVar("userID");
	String sFuncSchema = getSysConst("FuncSchema");		
	String sAgentCode = getUserVar("sys_agentcode");
	
	s = "begin ?:="+sFuncSchema+"PTTB_IMPORT_EXPORT.layds_export_dulieu_nguoivd('"+sDataSchema+"'); end;";	
	NEOExecInfo ei_ = new NEOExecInfo(s);

    return  new NEOCommonData(ei_);
  }
	public String layds_htkm() {	
    return "/main?"+CesarCode.encode("configFile")+"=PTTB/laphd/dangkygoinhom/ds_htkm_ajax";
  }  
    public String lay_ls_ListKH(String psUserInput) {
    return "/main?"+CesarCode.encode("configFile")+"=vinacore/List_dskh_ajax"
		+"&"+CesarCode.encode("matb")+"="+psUserInput
        ;
	}
}
















