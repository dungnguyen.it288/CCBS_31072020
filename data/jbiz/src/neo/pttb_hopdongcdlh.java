package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_hopdongcdlh
extends NEOProcessEx {
	public void run()
	{
		System.out.println("neo.pttb_hopdongcdlh was called");
	}
	public String getUrlttThueBao(String schema, String ma_hd, String ma_tb, String user_id)
	{
		String s = "begin ? := " + CesarCode.decode(schema) + "pttb_laphd.laytt_tb_cdlh('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_hd + "'"
		+ ",'" + ma_tb + "') ;"
		
		+ " end;";
		System.out.println(s);
		
		return s;
	}
	
	public String layds_hopdong(int trangthaihd_id)
	{
	return "/main?" + CesarCode.encode("configFile") + "=pttb/laphd/chuyendoi_lh/ajax_layds_hopdong"
		+ "&" + CesarCode.encode("trangthaihd_id") + "="+trangthaihd_id ;			
	}
	
	public String getUrlcdlh(String dichvuvt_id, int loaitb)
	{
		return "/main?" + CesarCode.encode("configFile") + "=pttb/laphd/chuyendoi_lh/ajax_loaihinh_tb"
			+ "&" + CesarCode.encode("dichvuvt_id") + "=" +dichvuvt_id
			+ "&" + CesarCode.encode("loaitb") + "=" + loaitb
			;			
	}
	public String hoanThanhHD_cdlh(String schema, String ma_hd)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "pttb_hthd.hoanthanh_hdcdlh('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_hd + "'" + ");"
		+ " end;";
		System.out.println(s);
		return s;	
	}
	public String xoaHD_cdlh(String schema, String ma_hd)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "pttb_laphd.xoa_hdcdlh('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_hd + "'" + ");"
		+ " end;";
		
		return s;
	}
	public String capnhatHDcdlh(String schema,
			String psMA_HD 
			,String piLOAITB_MOI 
			,String psSOMAY 
			,String pdNGAY_LAPHD 
			,String psGHICHU 
			,String piLOAITB_CU
			, String psUSER_ID 
			, String psTEN_TB 
			, String piQUANTB_ID 
			, String piPHUONGTB_ID 
			, String piPHOTB_ID 
			, String psSONHATB 
			, String psDIACHITB 
			, String psMAKH 
			, String psTENKH 
			, String psDIACHIKH 
		)
	{			
		String s = "begin ? := " + CesarCode.decode(schema) + "PTTB_LAPHD.capnhat_hdcdlh('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"
			+ psMA_HD + "'"
			+ "," + piLOAITB_MOI
			+ ",'" + psSOMAY + "'"
			+ ",'" + pdNGAY_LAPHD + "'"
			+ ","+ psGHICHU
			+ "," + piLOAITB_CU + ""
			+ ",'" + psUSER_ID + "'"
			+ ",'" + psTEN_TB + "'"
			+ "," + piQUANTB_ID
			+ "," + piPHUONGTB_ID
			+ "," + piPHOTB_ID
			+ ",'" + psSONHATB + "'"
			+ ",'" + psDIACHITB + "'"
			+ ",'" + psMAKH + "'"
			+ ",'" + psTENKH + "'"
			+ ",'" + psDIACHIKH + "'"
			+ ");"
			+ "end;";			
		System.out.println(s);
		return s;		
	}
}