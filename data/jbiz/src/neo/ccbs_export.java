package neo;

import java.io.*;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSetMetaData;

import javax.sql.RowSet;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ccbs_export {
	public static void exportToExcel(String outputFile, String fileName, RowSet rowSet) throws Exception {
		File file = new File(outputFile);

		if (file.exists()) {
			file.delete();
		}

		FileOutputStream fos = new FileOutputStream(file, true);
		
		ResultSetMetaData rsMeta = rowSet.getMetaData();
		int nColumn = rsMeta.getColumnCount();		
		int nRow = 0;
		rowSet.last();
		nRow = rowSet.getRow();
		rowSet.beforeFirst();
		
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = null;
		HSSFRow titleRow = null;
		HSSFRow row = null;
		HSSFCell cell = null;
		
		int nSheet = 0;
		int nRowPerSheet = 60000;
		if(nRow % nRowPerSheet == 0) {
			nSheet = nRow % nRowPerSheet;			
		} else {
			nSheet = (nRow / nRowPerSheet) + 1;
		}
		
		int nRowTmp = 0;
		String rowType = null;
		String shName = fileName.substring(0, fileName.lastIndexOf("."));
		for (int i = 1; i <= nSheet; i++) {
			sheet = workbook.createSheet(shName + "_" + i);
			
			nRowTmp = 0;
			titleRow = sheet.createRow(nRowTmp);
			for (int k = 0; k < nColumn; k++) {
				cell = titleRow.createCell(k);
				cell.setCellValue(rsMeta.getColumnName(k + 1));
			}
			
			while (rowSet.next()) {
				nRowTmp++;
				row = sheet.createRow(nRowTmp);
				
				for (int j = 0; j < nColumn; j++) {
					rowType = rsMeta.getColumnTypeName(j + 1);
					cell = row.createCell(j);
					if("NUMBER".equals(rowType)) {
						cell.setCellValue(rowSet.getDouble(j + 1));
					} else if("DATE".equals(rowType)) {
						cell.setCellValue(rowSet.getDate(j + 1));
					} else {
						cell.setCellValue(rowSet.getString(j + 1));
					}
				}
				
				if(nRowTmp == nRowPerSheet) {
					break;
				}
			}
			
			for (int k = 0; k < nColumn; k++) {
				sheet.autoSizeColumn((short) (k));
			}
		}
		workbook.write(fos);
		fos.flush();
		fos.close();
	}
	public static void exportToTextFile(String outputFile, String fileName, RowSet rowSet) throws Exception {
		File file = new File(outputFile);

		if (file.exists()) {
			file.delete();
		}
	//	FileOutputStream fos = new FileOutputStream(file, true);
		FileWriter fw = new FileWriter(file);  
        BufferedWriter bw = new BufferedWriter(fw);  
		
		ResultSetMetaData rsMeta = rowSet.getMetaData();
		int nColumn = rsMeta.getColumnCount();		
		int nRow = 0;
		rowSet.beforeFirst();
		int count = 0;
		try {
			while (rowSet.next()) {
			count++;
			String tmp="";
			for (int j = 0; j < nColumn; j++) {
				tmp = tmp + rowSet.getString(j+1);				
			} 
			
			System.out.print(tmp);
			bw.write(tmp);			
			bw.newLine();
		}
		}
		finally {
			bw.flush();	
			bw.close();
			fw.close();
		
		}
	}
}
