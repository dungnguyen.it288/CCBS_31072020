package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class gqkn
extends NEOProcessEx {

  public void run() {
    System.out.println("neo.gqkn was called");
  }
  
  public String laytt_thuebao(String func_schema, String data_schema ,String agent_schema , String user_id,
							String userInput,String userInput1) {
    return "begin ?:="+func_schema+"gqkn.laytt_thuebao("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"','" 
			+userInput+"','"+userInput1+"'); end;";
  }
  
  public String laytt_theo_makh(String func_schema, String data_schema ,String agent_schema , String user_id,
							String userInput,String userInput1) {
    return "begin ?:="+func_schema+"gqkn.laytt_theo_makh("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"','" 
			+userInput+"','"+userInput1+"'); end;";
  }
	
  public String laytt_theo_makn(String func_schema, String data_schema ,String agent_schema , String user_id,
							String userInput) {
    return "begin ?:="+func_schema+"gqkn.laytt_theo_makn("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"','" 	
			+userInput+"'); end;";
  }


  public String xoaKN(String func_schema, String data_schema ,String agent_schema , String user_id 
						,String psMA_KN                         
						,String psROWID                        
									 ) {
    String s=    "begin ? := "+func_schema+"gqkn.xoa_khieunai("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"'" 
			+",'"+psMA_KN+"'"                         
			+",'"+psROWID+"'"
        +");"
        +"end;"
        ;
    return s;

  }
    public String themmoiKN(String func_schema, String data_schema ,String agent_schema , String user_id
									,String psMA_KN ,String psMA_TB
									,String psMA_KH ,String pdNGAY_KN ,String psNOI_DUNG
									,String pdNGAYHEN_TL ,String psGP_KP ,String psGHICHU 
									,String piLOAIKN_ID ,String piHINHTHUC_ID
									,String psDIENTHOAI_LH ,String psNHANVIEN_TIEPNHAN
									,String psNOIDUNG_XULY
									,String psTEN_KH ,String psDIACHI_KH , String piNHANGQ,String psmanv,String psmat
									 ) {

   String s=    "begin ? := "+func_schema+"gqkn.themmoi_khieunai("
 			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"'," 	   
		+"'"+psMA_KN+"',"
		+"'"+psMA_TB+"',"
		+"'"+psMA_KH+"',"
		+pdNGAY_KN+","
		+"'"+psNOI_DUNG+"',"
		+pdNGAYHEN_TL+","
		+"'"+psGP_KP+"',"
		+"'"+psGHICHU+"',"
		+piLOAIKN_ID+","
		+piHINHTHUC_ID+","
		+"'"+psDIENTHOAI_LH+"',"
		+"'"+psNHANVIEN_TIEPNHAN+"',"
		+"'"+psNOIDUNG_XULY+"',"
		+"'"+psTEN_KH+"',"
		+"'"+psDIACHI_KH+"',"
		+piNHANGQ
		+",'"+psmanv+"',"
		+"'"+psmat+"'"
        +");"
        +"end;"
        ;
	System.out.println("neo.gqkn was called :"+s);	
    return s;

  } 
  

  public String layds_khieunai(String psTHONGTIN,String piLOAI_TK, String piTRANGTHAI) {
    return "/main?"+CesarCode.encode("configFile")+"=gqkn/gqkn/gq_ajax_kn_chua_gq"
        +"&"+CesarCode.encode("THONGTIN")+"="+psTHONGTIN
		+"&"+CesarCode.encode("LOAI_TK")+"="+piLOAI_TK
		+"&"+CesarCode.encode("TRANGTHAI")+"="+piTRANGTHAI
        ;
  }    


  public String laytt_khieunai(String func_schema, String data_schema ,
								String agent_schema , String user_id ,String userInput) {
    return "begin ?:="+func_schema+"gqkn.layds_khieunai("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"'" 
			+",'"+userInput+"','3','-1'); end;";
  }

  public String capnhatGQ(String func_schema, String data_schema ,String agent_schema , String user_id 
									,String pdNGAY_TL ,String psNOIDUNG_XULY
									,String psNHANVIEN_GIAIQUYET ,String psSOVANBAN_TRALOI ,String pdNGAY_XULY
									,String psMA_KN 
									 ) {

   String s=    "begin ? := "+func_schema+"gqkn.capnhat_giaiquyet("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"'" 

    	+","+pdNGAY_TL+","
		+"'"+psNOIDUNG_XULY+"',"
		+"'"+psNHANVIEN_GIAIQUYET+"',"
		+"'"+psSOVANBAN_TRALOI+"',"
		+pdNGAY_XULY+","
		+"'"+psMA_KN+"'"
        +");"
        +"end;"
        ;
    return s;

  } 
    public String giaiquyetKN(String func_schema, String data_schema ,String agent_schema , String user_id 
									,String psNOIDUNG_XULY
									,String psNHANVIEN_GIAIQUYET ,String psSOVANBAN_TRALOI ,String psTHOIHANTOIDA
									,String pdNGAY_XULY ,String pdNGAY_TL
									,String psMA_KN
									,String piChk
									,String pssotien
									 ) {

   String s=    "begin ? := "+func_schema+"gqkn.giaiquyet_khieunai("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"'," 

		+"'"+psNOIDUNG_XULY+"',"
		+"'"+psNHANVIEN_GIAIQUYET+"',"
		+"'"+psSOVANBAN_TRALOI+"',"
		+"'"+psTHOIHANTOIDA+"',"
		+pdNGAY_XULY+","
    	+pdNGAY_TL+","
		+"'"+psMA_KN+"',"
		+piChk+","
		+"'"+pssotien+"'"
        +");"
        +" end;"        ;
		
		System.out.println(s);
    return s;

  }
//String func_schema, String data_schema ,String agent_schema , String user_id ,
  public String Danhsach_KN(int ptt_moi, int ptt_pc)
	{
     		String url_ ="/main";
			url_ = url_ + "?"+CesarCode.encode("configFile")+"=GQKN/xlt/xlt_dskn_ajax";
			url_ = url_ + "&"+CesarCode.encode("tt_moi")+"="+ptt_moi;
			url_ = url_ + "&"+CesarCode.encode("tt_pc")+"="+ptt_pc;
			url_ = url_ +"&sid="+Math.random();
		return url_;
	}

	public String ChitietKN(String func_schema, String data_schema ,String agent_schema , String user_id ,String pmaKN)
	{
		String sSQL=    "begin ? := "+func_schema+"GQKN.LayttKN("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"'" 
			+",'" + pmaKN + "'"
			+");"
			+ " end;";
		return sSQL;
	}
	
	public String XulytonKN(String func_schema, String data_schema ,String agent_schema , String user_id ,String pmaKN, String pNguyennhan)
	{
		String str = "begin ? := "+func_schema+"GQKN.Ton_KN("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"'" 
			+",'"+pmaKN+"'" 
			+",'"+pNguyennhan+"'"
        +");"
        +"end;"
        ;
		return str;
	}
	public String XulyhuytonKN(String func_schema, String data_schema ,String agent_schema , String user_id , String pmaKN)
	{
		String str = "begin ? := "+func_schema+"GQKN.Huy_KN("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"'" 
			+",'"+pmaKN+"'"                         
        +");"
        +"end;"
        ;
		return str;
	}
	
	 public String tracuu_kn(	String piChkMa_KN, String psMA_KN, String piChkMa_TB, String psMA_TB,
								String piChkMa_KH, String psMA_KH, String piChkTRANG_THAI, String piTrang_thai,
								String piChkKN_ngay, String pdNgay_KN, String piChkTu_ngay, String pdTu_ngay,
								String pdDen_ngay, String piChkHT_KN, String piHinh_thuc, String piChkLOAI_KN,
								String piLoai_KN, String piChkNGUOI_TN, String psNguoi_TN, String piChkNGUOI_GQ,
								String psNguoi_GQ){

		return "/main?"+CesarCode.encode("configFile")+"=gqkn/tracuu/tracuu_ajax_thongtin"
				+"&"+CesarCode.encode("ChkMa_KN")+"="+piChkMa_KN
				+"&"+CesarCode.encode("MA_KN")+"="+psMA_KN
				+"&"+CesarCode.encode("ChkMa_TB")+"="+piChkMa_TB
				+"&"+CesarCode.encode("MA_TB")+"="+psMA_TB
				+"&"+CesarCode.encode("ChkMa_KH")+"="+piChkMa_KH
				+"&"+CesarCode.encode("MA_KH")+"="+psMA_KH 	
				+"&"+CesarCode.encode("ChkTRANG_THAI")+"="+piChkTRANG_THAI					
				+"&"+CesarCode.encode("TRANGTHAI")+"="+piTrang_thai
				+"&"+CesarCode.encode("ChkKN_ngay")+"="+piChkKN_ngay					
				+"&"+CesarCode.encode("NGAY_KN")+"="+pdNgay_KN
				+"&"+CesarCode.encode("ChkTu_ngay")+"="+piChkTu_ngay					
				+"&"+CesarCode.encode("TU_NGAY")+"="+pdTu_ngay				
				+"&"+CesarCode.encode("DEN_NGAY")+"="+pdDen_ngay
				+"&"+CesarCode.encode("ChkHT_KN")+"="+piChkHT_KN				
				+"&"+CesarCode.encode("HINHTHUC_ID")+"="+piHinh_thuc
				+"&"+CesarCode.encode("ChkLOAI_KN")+"="+piChkLOAI_KN	
				+"&"+CesarCode.encode("LOAIKN_ID")+"="+piLoai_KN
				+"&"+CesarCode.encode("ChkNGUOI_TN")+"="+piChkNGUOI_TN	
				+"&"+CesarCode.encode("Nguoi_TN")+"="+psNguoi_TN 		
				+"&"+CesarCode.encode("ChkNGUOI_GQ")+"="+piChkNGUOI_GQ	
				+"&"+CesarCode.encode("Nguoi_GQ")+"="+psNguoi_GQ
				; 			
	 }

	  public String layds_tracuu(String psTHONGTIN,String piOPTION){
		return"/main?"+CesarCode.encode("configFile")+"=pttb/gqkn/tracuu/tracuu_ajax"
				+"&"+CesarCode.encode("THONGTIN")+"="+psTHONGTIN
				+"&"+CesarCode.encode("LOAI_TK")+"="+piOPTION
				;
	  }
	  
	  
	  public String layds_khieunai_ton(String psTHONGTIN,String piLOAI_TK, String piTRANGTHAI) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/gqkn/gq_ajax_kn_ton"
			+"&"+CesarCode.encode("THONGTIN")+"="+psTHONGTIN
			+"&"+CesarCode.encode("LOAI_TK")+"="+piLOAI_TK
			+"&"+CesarCode.encode("TRANGTHAI")+"="+piTRANGTHAI
			;
	  }  

	public String lay_dskn(String trangthai_kn)
	{
	String url_ ="/main";
	url_ = url_ + "?"+CesarCode.encode("configFile")+"=gqkn/pc/phancong_kn_ajax";
	url_ = url_ +"&"+CesarCode.encode("trangthai_kn")+"=" + trangthai_kn ;
	url_ = url_ + "&sid="+Math.random();
	return url_;
	}
   public String lay_dskn_by_makn(String func_schema, String data_schema ,String agent_schema , String user_id , String ma_kn) {
    return "begin ?:= "+ func_schema + "GQKN.layds_khieunai("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"','" 
		+ma_kn+"',3,-1); end;";
  }
  
  public String laytt_theo_matb(String func_schema, String data_schema ,String agent_schema , String user_id , String ma_tb) {
    String s= "begin ?:= "+ func_schema + "GQKN.layds_khieunai("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"','" 
		+ma_tb+"',1,-1); end;";


return s;
       
  }
	public String phancong_kn(String func_schema, String data_schema ,String agent_schema , String user_id ,
							 String ma_kn,
							 String nguoiGiaiQuyet, 
							 String thoiHanToiDa_XL,
							 String huongGiaiQuyet
							 ) {
    String s =    "begin ? := "+ func_schema + "GQKN.phancong_kn("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"','" 
		+ ma_kn + "','"	
        + nguoiGiaiQuyet 
        +"','" + thoiHanToiDa_XL +"'"
        +",'" + huongGiaiQuyet  +"'"
        +");"
        +"end;";
    return s;
  } 
  
 //Khieu nai nhan vien
  public String layds_nhanvien() {
    return "/main?"+CesarCode.encode("configFile")+"=gqkn/dulieu/ajax_themnhanvien"
        ;
  }
  public String themnhanvien(
  	String psten, 
  	String pspass, 
  	String pitiepnhan, 
  	String pigiaiquyet, 
  	String piphancong,
  	String psuser, 
  	String pixulyton, 
  	String piquanly,
	String piGpc) {
    return "begin ?:="+getSysConst("FuncSchema")+"gqkn.themmoi_nhanvien("
			+"'"+psten+"'"
			+",'"+pspass+"'"
			+","+pitiepnhan
			+","+pigiaiquyet
			+","+piphancong
			+",'"+psuser+"'"
			+","+pixulyton
			+","+piquanly
			+","+piGpc
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'); end;";
  }
  
  public String capnhat_nhanvien(
  	String ids,
  	String tiepnhan,
  	String giaiquyet,
  	String phancong,
  	String xulyton,
  	String quanlynv,
	String gpc
  	) {
    return "begin ?:="+getSysConst("FuncSchema")+"gqkn.capnhat_nhanvien("
			+"'"+ids+"'," 
			+"'"+tiepnhan+"'," 
			+"'"+giaiquyet+"',"
			+"'"+phancong+"',"
			+"'"+xulyton+"',"
			+"'"+quanlynv+"',"
			+"'"+gpc+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
  }
  public String capnhat_ten(
  	String id,
  	String tencu,
  	String tenmoi
  	) {
    return "begin ?:="+getSysConst("FuncSchema")+"gqkn.capnhat_ten("
			+"'"+id+"'," 
			+"'"+tencu+"'," 
			+"'"+tenmoi+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
  }
  public String xoa_nhanvien(String func_schema, String data_schema ,String agent_schema , String user_id,
							String piid) {
    return "begin ?:="+func_schema+"gqkn.xoa_nhanvien("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"'"
			+","+piid		
			+"); end;";
  } 
    public String getUrlDsDMKhieunai() {
		return "/main?"+CesarCode.encode("configFile")+"=gqkn/dulieu/ajax_ds_DMKN";
	}
	public String update_loaikn(String schema
									,String piLOAIKN_ID
									,String psTENLOAI_KN
									) {

    String s= "begin ? := "+CesarCode.decode(schema)+"GQKN.update_loaikn('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       	+"'"+piLOAIKN_ID+"',"
		+"'"+psTENLOAI_KN+"'"		
        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }
  public String delete_loaikhieunai(String schema,String piLOAIKN_ID) 
 { String s= "begin ? := "+CesarCode.decode(schema)+"GQKN.xoa_loaikn('"+getUserVar("userID")
			+"','"+getUserVar("sys_agentcode")
			+"','"+getUserVar("sys_dataschema")+"',"
       		+piLOAIKN_ID
			+");"
			+"end;"
			;
		System.out.println(s);
		return s;
  } 
  public String layds_congvan(String psma_kn, String psma_tinh){
	return "/main?"+CesarCode.encode("configFile")+"=gqkn/congvan/ajax_ds_congvan"
			+"&"+CesarCode.encode("ma_kn")+"="+psma_kn
			+"&"+CesarCode.encode("ma_tinh")+"="+psma_tinh;
  }
  
  public String value_xoa_congvan(String psupload_id, String psma_tinh){
	return "begin ?:="+getSysConst("FuncSchema")+"GQKN.xoa_file_uploads('"+psupload_id+"','"+psma_tinh+"','"+getUserVar("userID")+"','"+getUserVar("userIP")+"'); end;";
  }

}
