package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_hthdld
extends NEOProcessEx {
	public void run()
	{
		System.out.println("neo.pttb_hthdld was called");
	}
	public String getUrlTTHopdongld(String schema, String ma_hd, String hd_or_tb)
	{
		String s = "begin ? := " + CesarCode.decode(schema) + "PTTB_TEST_KIEN.laytt_hdld_hoanthanh("				
		+ "'" + ma_hd + "'"
		+ "," + hd_or_tb
		+");"
		+ " end;";
		return s;
	}
	public String hoanThanhHopdongTL(String schema, String ma_hd, String ngay_ht)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "CCBS.hoanthanh_hdtl("
		+ "'" + ma_hd + "'"
		+ ",'" + ngay_ht + "'"
		+ ");"
		+ " end;";
		System.out.println(s);
		return s;	
	}
	public String xoaYeucau_ks(String schema, String psYeucauks_id)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "pttb_test_kien.xoa_yeucauks("
		+ "'" + psYeucauks_id + "'" + ");"
		+ " end;";
		System.out.println(s);
		return s;
	}
	public String capnhatHDLD_hoanthanh(String schema,  
         String psMA_HD 
        ,String psCOQUAN 
        ,String psNGUOI_DD 
		,String psCHUCDANH 
		,String psDIENTHOAI_LH 
        ,String piQUAN_ID 
		,String piPHUONG_ID 
		,String piPHO_ID
		,String psSO_NHA 
        ,String psDIACHI_CT
 
        ,String psCMT 
		,String psNGAYCAP_CMT 
		,String psNOICAP_CMT 
        ,String psHOKHAU 
		,String psNGAYCAP_HOKHAU 
		,String psNOICAP_HOKHAU 
        ,String psSODAIDIEN 
        ,String piLOAIKH_ID 
        ,String piKHLON_ID 
        ,String piNGANHNGHE_ID 
        ,String piUUTIEN_ID 
        ,String piDANGKY_DB 
        ,String piDANGKY_TV 
        ,String piDIADIEMTT_ID 
        ,String psMA_BC 
        ,String psTEN_TT 
		,String psDIACHI_TT 
        ,String piQUANTT_ID 
		,String piPHUONGTT_ID 
		,String piPHOTT_ID 
		,String psSOTT_NHA 
        ,String psMS_THUE 
        ,String piNGANHANG_ID 
        ,String psTAIKHOAN 
        ,String pdNGAY_LAPHD
        ,String psMA_NV 
        ,String psGHICHU 
        ,String psTEN_TB 
        ,String psTEN_DB 
        ,String piQUANLD_ID 
        ,String piPHUONGLD_ID 
        ,String piPHOLD_ID 
        ,String psSONHALD 
        ,String psDIACHILD 
        ,String piTRAMVT_ID 
        ,String piDONVIQL_ID 
        ,String piDONVITC_ID 
        ,String piTHUTU_IN 
        ,String psSOMAY_TN 
        ,String piNGUOINHAN 
        ,String piDANGKY_DBTB 
        ,String piTRACUU_DB 
        ,String piINCHITIET 
        ,String piGIAPDANH 
        ,String piCUOCNH_ID 
        ,String piLOAIDAY_ID 
        ,String piSLDAY 
        ,String pdNGAY_HT
        ,String piTHUEBAO_ID 
        ,String psGHICHUTB 
        ,String psMA_TB
		,String piDICHVUVT_ID 
		)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "CCBS.capnhat_hdld_hoanthanh('"
					+ psMA_HD + "'"
					+ ",'" + psCOQUAN + "'"
					+ ",'" + psNGUOI_DD + "'"
					+ ",'" + psCHUCDANH + "'"
					+ ",'" + psDIENTHOAI_LH + "'"
					+ "," + piQUAN_ID
					+ "," + piPHUONG_ID
					+ "," + piPHO_ID
					+ ",'" + psSO_NHA + "'"
					+ ",'" + psDIACHI_CT + "'"
					+ ",'" + psCMT + "'"
					+ ",'" + psNGAYCAP_CMT + "'"
					+ ",'" + psNOICAP_CMT + "'"
					+ ",'" + psHOKHAU + "'"
					+ ",'" + psNGAYCAP_HOKHAU + "'"
					+ ",'" + psNOICAP_HOKHAU + "'"
					+ ",'" + psSODAIDIEN + "'"
					+ "," + piLOAIKH_ID
					+ "," + piKHLON_ID
					+ "," + piNGANHNGHE_ID
					+ "," + piUUTIEN_ID
					+ "," + piDANGKY_DB
					+ "," + piDANGKY_TV
					+ "," + piDIADIEMTT_ID
					+ ",'" + psMA_BC + "'"
					+ ",'" + psTEN_TT + "'"
					+ ",'" + psDIACHI_TT + "'"
					+ "," + piQUANTT_ID
					+ "," + piPHUONGTT_ID
					+ "," + piPHOTT_ID
					+ ",'" + psSOTT_NHA + "'"
					+ ",'" + psMS_THUE + "'"
					+ "," + piNGANHANG_ID
					+ ",'" + psTAIKHOAN + "'"
					+ ",'" + pdNGAY_LAPHD + "'"
					+ ",'" + psMA_NV + "'"
					+ ",'" + psGHICHU + "'"
					+ ",'" + psTEN_TB + "'"
					+ ",'" + psTEN_DB + "'"
					+ "," + piQUANLD_ID 
					+ "," + piPHUONGLD_ID
					+ "," + piPHOLD_ID
					+ ",'" + psSONHALD + "'"
					+ ",'" + psDIACHILD + "'"
					+ "," + piTRAMVT_ID
					+ "," + piDONVIQL_ID
					+ "," + piDONVITC_ID
					+ "," + piTHUTU_IN
					+ ",'" + psSOMAY_TN + "'"
					+ "," + piNGUOINHAN
					+ "," + piDANGKY_DBTB
					+ "," + piTRACUU_DB
					+ "," + piINCHITIET
					+ "," + piGIAPDANH
					+ "," + piCUOCNH_ID
					+ "," + piLOAIDAY_ID
					+ "," + piSLDAY
					+ ",'" + pdNGAY_HT + "'"
					+ "," + piTHUEBAO_ID
					+ ",'" + psGHICHUTB + "'"
					+ ",'" + psMA_TB + "'"
					+ "," + piDICHVUVT_ID
					+ ");"
					+ "end;";			
		System.out.println(s);
		return s;		
	}
}