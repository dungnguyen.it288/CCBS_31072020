package neo;
import neo.smartui.process.*;
import sun.misc.BASE64Encoder;
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class Get_Data3g extends NEOProcessEx 
 {
	
	public static NodeList getXmlNodes(String xml, String path){
		try{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(new InputSource(new StringReader(xml)));
			return document.getDocumentElement().getElementsByTagName(path);
		}catch(Exception e){
			return null;
		}
    }
	//1. Ham ket noi API tra cuu 3g
	public static String postSOAP(String Request, String method){
		String Host="10.156.4.125";
		String Port="8899";
		String Uri="/axis/services/qos";
		String username="neo";
		String password="neo#1";
		StringBuffer buf = new StringBuffer();	    
	    try {
	    	URL url = new URL("http://"+Host+":"+Port+"/"+Uri);
			System.out.println("URL: " + url);			
	        URLConnection urlc = url.openConnection();
	        urlc.setConnectTimeout(8000);
			BASE64Encoder enc = new sun.misc.BASE64Encoder();
			String userpassword = username + ":" + password;
			String encodedAuthorization = enc.encode(userpassword.getBytes());
			urlc.setRequestProperty("Authorization", "Basic " + encodedAuthorization);
	        urlc.setRequestProperty("Content-Type","text/xml");
	        urlc.setRequestProperty("SOAPAction", Uri +"/"+method);	        
	        urlc.setDoOutput(true);
	        urlc.setDoInput(true);
	        OutputStreamWriter writer = new OutputStreamWriter( urlc.getOutputStream() );
	        writer.write( Request );
	        writer.flush();
	        BufferedReader in = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
	        String res_line;
	        while ((res_line = in.readLine()) != null)
	            buf.append(res_line);
	        in.close();
	        return buf.toString();
	    }catch (Exception e) {
	        e.printStackTrace();
	        return e.getMessage();
	    }
	  }
	
	//2. Ham login
	/*
	public static String login(){
		 try{
			 String reques_="<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:NumberStore\"><soapenv:Header/><soapenv:Body><urn:login soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"><login xsi:type=\"xsd:string\">ccbs</login><password xsi:type=\"xsd:string\">vnpnum@1235</password></urn:login></soapenv:Body></soapenv:Envelope>";
  		 	 String response_= postSOAP(reques_,"login");
			 return response_.substring(response_.indexOf("<sessionID xsi:type=\"xsd:string\">") + ("<sessionID xsi:type=\"xsd:string\">").length(),response_.indexOf("</sessionID>"));			 				
		 }catch(Exception e){
			   return e.getMessage(); 
		 }
	}	*/
	
	//4. Ham lay thong tin
	public static String GetDataHighBWRemain( String psmsin){
		try{		       
			
			String reques_= "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:Webservice\"><soapenv:Header/><soapenv:Body><urn:qosGetDataHighBWRemain soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
			reques_= reques_ + "<in0 xsi:type=\"xsd:long\">0</in0>";			
			reques_= reques_ + "<in1 xsi:type=\"soapenc:string\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">"+psmsin+"</in1>";
	        reques_= reques_ + "</urn:qosGetDataHighBWRemain></soapenv:Body></soapenv:Envelope>";
			System.out.println("Chuoi: " + reques_);
	        String response_ =postSOAP(reques_,"qosGetDataHighBWRemain");
			System.out.println("##############################: " + response_);
			
			String return_ = response_.substring(response_.indexOf("<qosGetDataHighBWRemainReturn xsi:type=\"xsd:long\">") + ("<qosGetDataHighBWRemainReturn xsi:type=\"xsd:long\">").length(),response_.indexOf("</qosGetDataHighBWRemainReturn>"));
			return return_;
		}catch(Exception e){
			return e.getMessage();
		}		
	}
	
}