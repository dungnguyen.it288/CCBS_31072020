package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public  class taoyeucauinhoadon extends NEOProcessEx
{
	public void run()
	{
		System.out.println("taoyeucauinhoadon was called");
	}
		
	public NEOCommonData themyeucau(String uname,
							String type, 
							String Thangin, 
							String ThangtrenBK, 
							String Ngayrahd,
							String Ngaybd, 
							String Ngaykt, 
							String chuky, 
							String tump, 
							String denmp, 
							String tumatt,
							String denmatt, 
							String userchoice,
							String Objchoice, 
							String matkhau,
							String motvaimatt, 
							String motvaism, 
							String motvaimp, 
							String uschema,
							String typeCreate,
							String service_name
							
							)
	{
		String insert_sql="begin ? := CCS_COMMON.INHOADON.themmoi_yeucau('"
							+uname+	"','"
							+CesarCode.decode(type)+"','"
							+Thangin+"','"
							+ThangtrenBK+"','"
							+Ngayrahd+"','"
							+Ngaybd+"','"
							+Ngaykt+"','"
							+chuky+"','"
							+tump+"','"
							+denmp+"','"
							+tumatt+"','"
							+denmatt+"','"
							+CesarCode.decode(userchoice)+"','"
							+Objchoice+"','"
							+matkhau+"','"
							+motvaimatt+"','"
							+motvaism+"','"
							+motvaimp+"','"
							+CesarCode.decode(uschema)+"','"
							+typeCreate+"','"
							+service_name+"');"
							+"end;" ;
		
	    
		NEOExecInfo nei_ = new NEOExecInfo(insert_sql);
        nei_.setDataSrc("VNPBilling");
        return new NEOCommonData(nei_);

	
	}
	
	
	public NEOCommonData themyeucaucuocthuebao(String uname,
							 String type, 
							 String Thangin, 
							 String userchoice,
							 String Objchoice, 
							 String matkhau,
							 String uschema,
							 String typeCreate,
							 String service_name
							
							)
	{
		String insert_sql="begin ? := CCS_COMMON.INHOADON.themmoi_yeucau_cuoctb('"
							+uname+	"','"
							+CesarCode.decode(type)+"','"
							+Thangin+"','"
							+CesarCode.decode(userchoice)+"','"
							+CesarCode.decode(Objchoice)+"','"
							+matkhau+"','"
							+CesarCode.decode(uschema)+"','"
							+typeCreate+"','"
							+service_name+"');"
							+"end;" ;
		
	    
		NEOExecInfo nei_ = new NEOExecInfo(insert_sql);
        nei_.setDataSrc("VNPBilling");
        return new NEOCommonData(nei_);

	
	}
	
	
	public NEOCommonData xoa_yeucau(String yeucauid,String uschema)
	{
		String delete_sql="begin ? := CCS_COMMON.INHOADON.xoayeucau('"
										+yeucauid+"','"+uschema+"');"+"end;";
		NEOExecInfo nei_ = new NEOExecInfo(delete_sql);
        nei_.setDataSrc("VNPBilling");
        return new NEOCommonData(nei_);

	}
	
	public String getRequest(String schema,String name, String bankeid)
	{
		String get_req=" SELECT * from ccs_common.yeucau_ihd where nguoi_yc='"+name+"' and banke_id='"+CesarCode.decode(bankeid)+"' and trangthai_id='5'";
		return get_req;
	}
	
	public NEOCommonData themyeucau_ct(String uname,
								String type, 
								String Thangin, 
								String ThangtrenBK, 
								String Ngayrahd,
								String Ngaybd, 
								String Ngaykt, 
								String chuky,
								String tusm,
								String densm, 
								String tump, 
								String denmp, 
								String tumatt,
								String denmatt, 
								String userchoice, 													String objChoice,
								String khoanmucChoice,
								String matkhau,
								String motvaimatt, 
								String motvaism, 
								String motvaimp, 
								String uschema,
								String typeCreate,
								String convertfont,
								String service_name
							)
	{
		String ct_sql="begin ? := CCS_COMMON.INHOADON.themmoi_yeucau_ct('"
							+uname+"','"
							+CesarCode.decode(type)+"','"
							+Thangin+"','"
							+ThangtrenBK+"','"
							+Ngayrahd+"','"
							+Ngaybd+"','"
							+Ngaykt+"','"
							+chuky+"','"
							+tusm+"','"
							+densm+"','"
							+tump+"','"
							+denmp+"','"
							+tumatt+"','"
							+denmatt+"','"
							+CesarCode.decode(userchoice)+"','"
							+objChoice+"','"
							+CesarCode.decode(khoanmucChoice)+"','"
							+matkhau+"','"
							+motvaimatt+"','"
							+motvaism+"','"
							+motvaimp+"','"
							+CesarCode.decode(uschema)+"','"
							+typeCreate+"','"
							+convertfont+"','"
							+service_name+"');"
							+"end;" ;
		
	    
		NEOExecInfo nei_ = new NEOExecInfo(ct_sql);
        nei_.setDataSrc("VNPBilling");
        return new NEOCommonData(nei_);
		
	}
    
    public NEOCommonData Insert_KM(String uname,
							String type, 
							String Thangin, 
							String ThangtrenBK, 
							String Ngayrahd,
							String Ngaybd, 
							String Ngaykt, 
							String chuky, 
							String tump, 
							String denmp, 
							String tumatt,
							String denmatt, 
							String userchoice,
							String Objchoice, 
							String matkhau,
							String motvaimatt, 
							String motvaism, 
							String motvaimp, 
							String uschema,
							String typeCreate,
							String service_name
							
							)
	{
		String insert_sql="begin ? := CCS_COMMON.INHOADON.Insert_KM('"
							+uname+	"','"
							+CesarCode.decode(type)+"','"
							+Thangin+"','"
							+ThangtrenBK+"','"
							+Ngayrahd+"','"
							+Ngaybd+"','"
							+Ngaykt+"','"
							+chuky+"','"
							+tump+"','"
							+denmp+"','"
							+tumatt+"','"
							+denmatt+"','"
							+CesarCode.decode(userchoice)+"','"
							+Objchoice+"','"
							+matkhau+"','"
							+motvaimatt+"','"
							+motvaism+"','"
							+motvaimp+"','"
							+CesarCode.decode(uschema)+"','"
							+typeCreate+"','"
							+service_name+"');"
							+"end;" ;
		
	    
		NEOExecInfo nei_ = new NEOExecInfo(insert_sql);
        nei_.setDataSrc("VNPBilling");
        return new NEOCommonData(nei_);

	
	}
	
    /*Ham thuc hien them moi yeu cau in Datapost*/
    	public NEOCommonData themyeucaudatapost(String uname,
							 String type, 
							 String Thangin, 
							 String uschema,
							 String matkhau,
							 String service_name
							)
	{
		String insert_sql="begin ? := CCS_COMMON.INHOADON.themmoi_yeucau_datapost('"
							+uname+	"','"
							+CesarCode.decode(type)+"','"
							+Thangin+"','"
							+CesarCode.decode(uschema)+"','"
							+matkhau+"','"
							+service_name+"');"
							+"end;" ;
		
	    
		NEOExecInfo nei_ = new NEOExecInfo(insert_sql);
        nei_.setDataSrc("VNPBilling");
        return new NEOCommonData(nei_);

	
	}

  /*Ham thuc hien them moi yeu cau in Datapost*/
    	public NEOCommonData themyeucaudatapostfoxpro(String uname,
							 String type, 
							 String Thangin, 
							 String uschema,
							 String matkhau,
							 String service_name
							)
	{
		String insert_sql="begin ? := CCS_COMMON.INHOADON.themmoi_yeucau_datapostFoxpro('"
							+uname+	"','"
							+CesarCode.decode(type)+"','"
							+Thangin+"','"
							+CesarCode.decode(uschema)+"','"
							+matkhau+"','"
							+service_name+"');"
							+"end;" ;
		
	    
		NEOExecInfo nei_ = new NEOExecInfo(insert_sql);
        nei_.setDataSrc("VNPBilling");
        return new NEOCommonData(nei_);

	
	}

    //Trang thai in hoa don VAT
    public String getForm()
    {
    	String url_="/main?"+CesarCode.encode("configFile")+"=inhd/frm_trangthai";    	
    	return url_;
    }
 //Trang thai KM
    public String getFormKM()
    {
    	String url_="/main?"+CesarCode.encode("configFile")+"=inhd/KhuyenMai/frm_trangthaiKM";    	
    	return url_;
    }
	//Trang thai in hoa don ct
	public String getForm1()
    {
    
    	String url_="/main?"+CesarCode.encode("configFile")+"=inhd/frm_trangthai_ct";    	
    	return url_;
    }
    //Trang thai in datapost
    public String getFormDataPost()
    {
    	String url_="/main?"+CesarCode.encode("configFile")+"=inhd/frm_trangthai01";    	
    	return url_;
    }
 //Trang thai in datapost
    public String getFormDataPostFoxPro()
    {
    	String url_="/main?"+CesarCode.encode("configFile")+"=inhd/OracleToFox/frm_trangthaiFox";    	
    	return url_;
    }
	//Trang thai in hoa don ct
	public String getFormcuocTB()
    {
    
    	String url_="/main?"+CesarCode.encode("configFile")+"=inhd/CuocTB/frm_trangthaicuoctb";    	
    	return url_;
    }
	//Lay trang thai cua cac chi tiet phieu cap dai ly
	public String getFormCTC_DL()
    {
    
    	String url_="/main?"+CesarCode.encode("configFile")+"=inhd/VTSG/Dailythucuoc/frm_trangthai_dl";    	
    	return url_;
    }
	//Lay trang thai cua cac phieu bao cuoc cap dai ly
	public String getFormPBC_DL()
    {
    
    	String url_="/main?"+CesarCode.encode("configFile")+"=inhd/VTSG/Dailythucuoc/frm_trangthai_dl_pbc";    	
    	return url_;
    }

    

}