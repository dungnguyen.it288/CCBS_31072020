package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class admin_numstore extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("NumStore");		
	}
	
	public String doc_layds_phanloai(
		String psKieuso,
		String psUploadID
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=numstore/phanloai/ajax_phanloai"
  			+ "&" + CesarCode.encode("kieuso_id") + "=" + psKieuso
  			+ "&" + CesarCode.encode("upload_id") + "=" + psUploadID;
	}
	
	public String doc_layds_chitiet(
		String psKieuso
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=numstore/phanloai/ajax_chitiet"
  			+ "&" + CesarCode.encode("kieuso_id") + "=" + psKieuso;
	}
	
	public NEOCommonData value_nhapkho_nhancong
	       (String psdsSoTB, String psKieuso, String psUserIP)
	{		
	 	String s=  "begin ?:=dms_admin.admin.nhapkho_nhancong('"
	 				+psdsSoTB+"','"
	 				+psKieuso+"','"
	 				+"numstore.','"
	 				+getUserVar("userID")+"','"
	 				+psUserIP+"'); end;";	 
		NEOExecInfo ei_ = new NEOExecInfo(s);
        ei_.setDataSrc("NUMSTOREDBMS");	
		return new NEOCommonData(ei_);
	}	
		
	public NEOCommonData value_huy_nhancong
	       (String psdsSoTB, String psUserIP)
	{		
	 	String s=  "begin ?:=dms_admin.admin.huy_tu_kho_nhancong('"
	 				+psdsSoTB+"','"
	 				+"numstore.','"
	 				+getUserVar("userID")+"','"
	 				+psUserIP+"'); end;";	 
		NEOExecInfo ei_ = new NEOExecInfo(s);
        ei_.setDataSrc("NUMSTOREDBMS");	
		return new NEOCommonData(ei_);
	}
		
	public String rec_upload_nhapkho
	       (String psUploadID)
	{		
	 	return  "begin ?:=numstore.laytt_upload_file('"
	 				+psUploadID+"'); end;";	 
	}
	
	public String value_accept_upload_nhapkho
	       (String psUploadID, String psUserIP)
	{		
	 	return  "begin ?:=numstore.chapnhan_upload_file('"
	 				+psUploadID+"','"
	 				+getUserVar("userID")+"','"
	 				+psUserIP+"'); end;";	 
	}
	
	public NEOCommonData value_phanloai
	       (String psDsKieuso, String psUploadID, String psUserIP)
	{		
	 	String s =  "begin ?:=dms_admin.admin.admin_phankho_thuebao('"
	 				+psDsKieuso+"','"
	 				+psUploadID+"','"
	 				+"numstore.','"
	 				+getUserVar("userID")+"','"
	 				+psUserIP+"'); end;";	 
		NEOExecInfo ei_ = new NEOExecInfo(s);
        ei_.setDataSrc("NUMSTOREDBMS");	
		return new NEOCommonData(ei_);
	}
	
	public NEOCommonData value_phanloai_chitiet
	       (String psDsTB, String psUploadID, String psUserIP)
	{		
	 	String s ="begin ?:=dms_admin.admin.admin_phankho_nhancong('"
	 				+psDsTB+"','"
	 				+psUploadID+"','"
	 				+"numstore.','"
	 				+getUserVar("userID")+"','"
	 				+psUserIP+"'); end;";	 
		NEOExecInfo ei_ = new NEOExecInfo(s);
        ei_.setDataSrc("NUMSTOREDBMS");	
		return new NEOCommonData(ei_);
	}
}