package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;
public class xuat_hoadon extends NEOProcessEx{
    public String filebath(String chuki,String kieuhoadon,String dangfile,String tinh,String mucxuat)
    {
        String result="";
        String s = " begin ? :="+"admin_v2.aq_invoice.batch_invoice("
                        + "'" + getUserVar("userID") + "'"
                        +",'"+chuki+"'"
                        +",'"+kieuhoadon+"'"
						+",'"+dangfile+"'"
                        +",'"+tinh+"'"
						+",'"+mucxuat+"'"
                        + ");"
                        +"end; ";
         try {
            result=reqValue(s);
        } catch (Exception e) {
        }
		System.out.println(s);
        return result;
    }
	
	public String filecancel(String tinh,String dulieu)
    {
        String result="";
        String s = " begin ? :="+"admin_v2.aq_invoice.cancel_invoice("
                        + "'" + getUserVar("userID") + "'"
                        +",'"+tinh+"'"
                        +",'"+dulieu+"'"						
                        + ");"
                        +"end; ";
         try {
            result=reqValue(s);
        } catch (Exception e) {
        }
		System.out.println(s);
        return result;
    }
	
    public String filemanual(String chuki,String kieufile,String kieuhoadon,String tinh,String donvi,String buucuc,String nhanvien,String khachhang,String muc_uutien)
    {
        String result="";
        String s = " begin ? :="+"admin_v2.aq_invoice.manual_invoice("
                        + "'" + getUserVar("userID") + "'"
                        +",'"+chuki+"'"
						+",'"+kieufile+"'"
                        +",'"+kieuhoadon+"'"
                        +",'"+tinh+"'"
                        +",'"+donvi+"'"
                        +",'"+buucuc+"'"
                        +",'"+nhanvien+"'"
                        +",'"+khachhang+"'"
						+",'"+muc_uutien+"'"
                        + ");"
                        +"end; ";
         try {
            result=reqValue(s);
        } catch (Exception e) {
        }
        return result;
    }
}
