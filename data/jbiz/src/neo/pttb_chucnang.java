package neo;
import neo.smartui.common.CesarCode;

import neo.smartui.process.*;
import neo.smartui.report.*;


public class pttb_chucnang
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb_capnhat was called");
  }
  
   public String layds_xly_TB_thly(String piPAGEID,String piREC_PER_PAGE,String piloaihinhvt
							,String pithdiemthly,String mahd,String matb
							,String ngaytl,String trangthaihd_id,String pihinhthuc_tl) 
	{
    return "/main?"+CesarCode.encode("configFile")+"=PTTB/chucnang/ajax_layds_xly_tb_thly"
		+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
        + "&" +CesarCode.encode("cboloaihinhvt")+"="+piloaihinhvt
		+ "&" +CesarCode.encode("thdiemthly")+"="+pithdiemthly
		+ "&" +CesarCode.encode("mahd")+"="+mahd
		+ "&" +CesarCode.encode("matb")+"="+matb
		+ "&" +CesarCode.encode("ngaytl")+"="+ngaytl
		+ "&" +CesarCode.encode("trangthaihd_id")+"="+trangthaihd_id
		+ "&" +CesarCode.encode("hinhthuc_tl")+"="+pihinhthuc_tl
        ;
  }
  
  public String xoakhoiDB(String schema, String cboloaihinhvt, String txtsomay, String txtmaHD) {


    String s="begin ? := "+CesarCode.decode(schema)+"PTTB_HTHD.xlyTB_xoakhoiDB('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+cboloaihinhvt+"'"
        +",'"+txtsomay+"'"
        +",'"+txtmaHD+"'"
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;
  }
  
 public String phuchoiTB(String schema, String cboloaihinhvt, String thdiemthly, String txtsomay
                                     ,String ngayph
                                     ,String txtmaHD) {
    String s="begin ? := "+CesarCode.decode(schema)+"PTTB_HTHD.xlyTB_phuc_hoi('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+cboloaihinhvt+"'"
		+",'"+thdiemthly+"'"
        +",'"+txtsomay+"'"
		+",'"+ngayph+"'"
        +",'"+txtmaHD+"'"
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;
  }
public String layds_frmLSTB(String piPAGEID,String piREC_PER_PAGE,String txtSoMay,String txtMaKH,String txtTenKH,String cboloaihinhvt) {
    return "/main?"+CesarCode.encode("configFile")+"=PTTB/chucnang/ajax_layds_frmLSTB"
        + "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE 
		+"&"+CesarCode.encode("SoMay")+"="+txtSoMay
		+"&"+CesarCode.encode("makh")+"="+txtMaKH
		+"&"+CesarCode.encode("tenkh")+"="+txtTenKH
		+"&"+CesarCode.encode("cboloaihinhvt")+"="+cboloaihinhvt
        ;
  }  
  
  public String getUrlDs_frmTCmoDV(String  pdichvuvt_id ,String pchkLapDat,String pTHANG) {
    return "/main?"+CesarCode.encode("configFile")+"=PTTB/chucnang/ajax_layds_frmTCmoDV"
        +"&"+CesarCode.encode("dichvuvt_id")+"="+pdichvuvt_id
		+"&"+CesarCode.encode("chkLapDat")+"="+pchkLapDat
		+"&"+CesarCode.encode("THANG")+"="+pTHANG
        ;
  }
  

  public String cmd_chapnhan(String schema, String sDsMahd, String sDsDichvu_id
							,String sDsMahd0
							, String  sDsDichvu_id0
                            ,String chkLapDat) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.chap_nhan_frmTCmoDV('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+sDsMahd+"'"
        +",'"+sDsDichvu_id+"'"
		+",'"+sDsMahd0+"'"
        +",'"+sDsDichvu_id0+"'"
		+",'"+chkLapDat+"'"
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;

  }

   /* Sua doi ngay 2007/11/23
	Nguoi sua: TuanNA */	
	public String layDS_TBTCDB(String piPAGEID,String piREC_PER_PAGE,String pidichvuvt_id) {	
		return "/main?"+CesarCode.encode("configFile")+"=pttb/chucnang/ajax_DSThueBaoDB"
		+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
        + "&" + CesarCode.encode("dichvuvt_id")+"="+pidichvuvt_id
		//+"&"+CesarCode.encode("user_id")+"="+ps_userid
        ;
   }  
	
	/* Sua doi ngay 2007/11/23
	Nguoi sua: TuanNA */	
	public String laytt_TBtinhcuocDB(String schema,String pidichvuvt_id,String psMATB) 
	{
     String s=   "begin ? := "+ schema +"PTTB_DULIEU.laytt_TBtinhcuocDB('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+pidichvuvt_id+"'"
		+",'"+psMATB+"'"			
		+");"
		+"end;"
		;		
		System.out.println(s);
		return s;	
	}

	/* Sua doi ngay 2007/11/23
	Nguoi sua: TuanNA */	
	public String update_tinhcuocTBDB(String schema
							, String pidichvuvt_id, String psMATB 
							,String PSCUOC_TB, String  pstinh_cuoc
                            ) 
		{
    String s=    "begin ? := "+CesarCode.decode(schema)+"PTTB_DULIEU.update_TBtinhcuocDB('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+pidichvuvt_id+"'"
        +",'"+psMATB+"'"
		+",'"+PSCUOC_TB+"'"
        +",'"+pstinh_cuoc+"'"
	    +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;
  }
  
	/* Sua doi ngay 2007/11/23
	Nguoi sua: TuanNA */	
    public String delete_tinhcuocTBDB(String schema, String pidichvuvt_id, String psMATB) 
		{
	String s=    "begin ? := "+CesarCode.decode(schema)+"PTTB_DULIEU.delete_TBtinhcuocDB('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+pidichvuvt_id+"'"
        +",'"+psMATB+"'"		
	    +");"
        +"end;"
        ;
	    //System.out.println(s);
    return s;

  }

  //update by DONGND 
   //Date: 2007.11.08
    public String layDSNVTC(String piPAGEID,String piREC_PER_PAGE,String psMaNV
						   ,String psTenNV
						   ,String psNguoiBL
						   ,String psMaBC) {
    
	return "/main?"+CesarCode.encode("configFile")+"=quangbinh/frmNhanVienTC_ajaxDSNV"
        +"&"+CesarCode.encode("sys_pageid") +"="+ piPAGEID	           
		+"&"+CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
		+"&"+CesarCode.encode("MaNV")+"="+psMaNV
		+"&"+CesarCode.encode("TenNV")+"="+psTenNV
		+"&"+CesarCode.encode("NguoiBL")+"="+psNguoiBL
		+"&"+CesarCode.encode("MaBC")+"="+psMaBC;
    }    
	
	// Xoa thue bao khoi danh ba
	// Ngay cap nhat: 2007.12.03
	// TUANNA
	/*public String xoa_tb_danhba(String schema,String pisomay, String psUserIp)
	{
		String s="begin ? := "+CesarCode.decode(schema)+"PTTB_HTHD.xoa_tb_danhba('"+getUserVar("userID")+"','"
																				   +getUserVar("sys_agentcode")
																				   +"','"+getUserVar("sys_dataschema")
																				   +"','"+pisomay
																				   +"','"+psUserIp+"'); end;";
		System.out.println(s);
		return s;
	}*/
	
	public String xoa_tb_danhba(String schema,String pisomay, String psUserIp)
	{
		String s="begin ? := admin_v2.pttb_hthd.xoa_tb_danhba('"+getUserVar("userID")+"','"
																				   +getUserVar("sys_agentcode")
																				   +"','"+getUserVar("sys_dataschema")
																				   +"','"+pisomay
																				   +"','"+psUserIp+"'); end;";
		System.out.println(s);
		return s;
	}
	
  //Date: 2008.04.03
    public String layDSTuyen(String piPAGEID,String piREC_PER_PAGE,String psMaT
						   ,String psTenT
						   ,String psMaBC
						   ,String psMaNV
						   ) {
    
	return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/frmTuyenThu_ajax"
		+"&"+CesarCode.encode("sys_pageid") +"="+ piPAGEID	           
		+"&"+CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
        +"&"+CesarCode.encode("MaT")+"="+psMaT
		+"&"+CesarCode.encode("TenT")+"="+psTenT
		+"&"+CesarCode.encode("MaBC")+"="+psMaBC
		+"&"+CesarCode.encode("MaNV")+"="+psMaNV
		;
    }    
	
	// khoi phuc thue bao xoa khoi danh ba
	// Ngay cap nhat: 2008.07.31
	// TUANNA
	public String khoiphuc_danhba(String schema,String pisomay)
	{
		String s="begin ? := admin_v2.pttb_chucnang.khoiphuc_thuebao('"+getUserVar("userID")+"','"+getUserVar("sys_agentcode")+"','"+getUserVar("sys_dataschema")+"','"+pisomay+"'); end;";
		System.out.println(s);
		return s;
	}
	
   //update by NATUAN
   //Date: 2008.12.11
    public String layDSNganHang(String piPAGEID,String piREC_PER_PAGE,String psTenVT
						   ,String psTenNH
						   ) {
    
	return "/main?"+CesarCode.encode("configFile")+"=pttb/tracuu/nganhang/frmNganHang_ajax"
		+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
        +"&"+CesarCode.encode("TenVT")+"="+psTenVT
		+"&"+CesarCode.encode("TenNH")+"="+psTenNH	
		;		
    }    
	// Create by LUATTD : 06/03/2009
  
    public String layDSNguoi_tthi(String piPAGEID,String piREC_PER_PAGE,String psTenVT
						   ,String psTenTT
						   ) {
    
	return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld/nguoivandong/frmNguoi_vd_ajax"
		+"&"+CesarCode.encode("sys_pageid") +"="+ piPAGEID	           
		+"&"+CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
		+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
        +"&"+CesarCode.encode("TenVT")+"="+psTenVT
		+"&"+CesarCode.encode("TenTT")+"="+psTenTT	
		;		
    } 
	
	public String layDSNguoi_tthi_giadinhvp(String piPAGEID,String piREC_PER_PAGE,String psTenVT
						   ,String psTenTT
						   ) {
    
	return "/main?"+CesarCode.encode("configFile")+"=mtv/family/nguoivandong/frmNguoi_vd_ajax"
		+"&"+CesarCode.encode("sys_pageid") +"="+ piPAGEID	           
		+"&"+CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
		+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
        +"&"+CesarCode.encode("TenVT")+"="+psTenVT
		+"&"+CesarCode.encode("TenTT")+"="+psTenTT	
		;		
    } 

 //create by Lan Anh: 18/07/2010
//=================================================================
	
 public String lay_nguoith( String func_schema,
 			     String psNguoi_th,
    			     String psQuyen_cn
			) 
 {
   String s= "begin ?:= ccs_admin.pttb_dulieu.lay_nguoith('"+getUserVar("sys_dataschema")
	               +"','"+psNguoi_th+"'"
		           +",'"+psQuyen_cn
		           +"');end;";
	System.out.println("test1 :"+s);
	return s;
  }  	
  
  public String capnhat_quyen( String func_schema,
  				String psNguoi_th,
    				String psQuyen_cn,    							 	    																  
    				String pspage_num,
				String pspage_rec
			) 
 {
   String s= "begin ?:= ccs_admin.pttb_dulieu.capnhat_quyen('"+getUserVar("userID")
		+"','"+getUserVar("sys_dataschema")+"'"
		+",'"+psNguoi_th+"'"
		+",'"+psQuyen_cn+"'"
		+",'"+pspage_num+"'"
		+",'"+pspage_rec
		+"');end;";
	System.out.println("test2:"+s);
	return s;
  }  
		
  public String huy_quyen( String func_schema,
  			   String psNguoi_th,
    			   String psQuyen_cn,    							 	    																  
    			   String pspage_num,
			   String pspage_rec
									) 
 {
   String s= "begin ?:=ccs_admin.pttb_dulieu.huy_quyen('"+getUserVar("userID")
		+"','"+getUserVar("sys_dataschema")+"'"
		+",'"+psNguoi_th+"'"
		+",'"+psQuyen_cn+"'"
		+",'"+pspage_num+"'"
		+",'"+pspage_rec
		+"');end;";
	System.out.println("test 3:"+s);
	return s;
  } 
  
  //  	check user
  public String check_user( String psNguoi_th) 
 {
   String s= "begin ?:= ccs_admin.pttb_dulieu.check_nguoith('"+psNguoi_th+"');end;";
   System.out.println("test 3:"+s);
   return s;
  }  	
  
  
  public String layds_capnhat(String psNguoi_th,
    			      String pspage_num,
			      String pspage_rec
									) {
    return "/main?"+CesarCode.encode("configFile")+"=PTTB/chucnang/cn_gan_nvtc/ajax_capnhat_gannvtc"
		+"&"+CesarCode.encode("nguoi_th") + "=" + psNguoi_th
		+"&"+CesarCode.encode("page_num") + "=" + pspage_num
		+"&"+CesarCode.encode("page_rec") + "=" + pspage_rec
        ;
	}
	// Create by LUATTD : 30/08/3010
  
    public String layDS_mbvnn(String piPAGEID,String piREC_PER_PAGE,String pssotb
						   ,String psgoiid
						   ) {
    
	return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld/goi_mbvnn/frmGoi_mbvnn_ajax"
		+"&"+CesarCode.encode("sys_pageid") +"="+ piPAGEID	           
		+"&"+CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
		+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
        +"&"+CesarCode.encode("sotb")+"="+pssotb
		+"&"+CesarCode.encode("goiid")+"="+psgoiid	
		;		
    } 
}
