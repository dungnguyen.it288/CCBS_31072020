package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class pttb_laphd_vsms
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb_laphd was called");
  }
  public String InPhieu() {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/inphieu_yc";
  }
  public String Inbd_numstore() {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/inphieu_yc_numstore";
  }
  public String getUrlKieuLD(String value) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/hdld_ajax_kieuld"
        +"&"+CesarCode.encode("dichvuvt_id")+"="+value
        ;
  }
  public String layds_hopdong_bm(String value) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdbm/hdbm_ajax_thietbi_daban"
        +"&"+CesarCode.encode("ma_hd")+"="+value
        ;
  }

  public String layds_thietbi(String value) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdbm/hdbm_ajax_thietbi"
        +"&"+CesarCode.encode("loaitb_id")+"='"+value+"'"
        ;
  }

  public String getUrlDsThueBao(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/hdld_ajax_dstb"
        +"&"+CesarCode.encode("ma_hd")+"="+userInput
        ;
  }
  public String getUrlDsThueBaoDC(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hddc/hddc_ajax_dstb"
        +"&"+CesarCode.encode("ma_hd")+"="+userInput
        ;
  }
  public String layds_thuebao_cq(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdcqsd/hdcqsd_ajax_dstb"
        +"&"+CesarCode.encode("ma_hd")+"="+userInput
        ;
  }
  public String layds_dichvu_cothe_dk(String psMA_TB,String piLOAITB_ID, String psMA_HD) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hddv/hddv_ajax_cothe_dk"
        +"&"+CesarCode.encode("ma_tb")+"="+psMA_TB
		+"&"+CesarCode.encode("loaitb_id")+"="+piLOAITB_ID
		+"&"+CesarCode.encode("ma_hd")+"="+psMA_HD
        ;
  }
  public String layds_dichvu_da_dk(String psMA_TB,String piLOAITB_ID) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hddv/hddv_ajax_da_dk"
        +"&"+CesarCode.encode("ma_tb")+"="+psMA_TB
		+"&"+CesarCode.encode("loaitb_id")+"="+piLOAITB_ID
        ;
  }
  public String layds_dichvu_da_sd(String psMA_TB,String piLOAITB_ID) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hddv/hddv_ajax_da_sd"
        +"&"+CesarCode.encode("ma_tb")+"="+psMA_TB
		+"&"+CesarCode.encode("loaitb_id")+"="+piLOAITB_ID
        ;
  }
  public String layds_dichvu_dang_sd(String psMA_TB,String piLOAITB_ID) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hddv/hddv_ajax_dang_sd"
        +"&"+CesarCode.encode("ma_tb")+"="+psMA_TB
		+"&"+CesarCode.encode("loaitb_id")+"="+piLOAITB_ID
        ;
  }

  public String getUrlDanhSachDVLD(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/tbld_cd_ajax_dsdv"
        +"&"+CesarCode.encode("thuebao_id")+"="+userInput
        ;
  }
  public String getUrlDsDoiTuong(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/tbld_ajax_doituong"
        +"&"+CesarCode.encode("loaikh_id")+"="+userInput
        ;
  }
  public String layds_nhanvien_dd(String userInput,String piDICHVUVT_ID) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/tbld_ajax_nvdd"
        +"&"+CesarCode.encode("donvild_id")+"="+userInput
		+"&"+CesarCode.encode("dichvuvt_id")+"="+piDICHVUVT_ID
        ;
  }
  public String layds_tramvts(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/tbld_ajax_tramvts"
        +"&"+CesarCode.encode("donviql_id")+"="+userInput
        ;
  }
  
  public String layds_cbodvql(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/tbld_ajax_donviql"
        +"&"+CesarCode.encode("donviql_id")+"="+userInput
        ;
  }
  public String layds_cbodvtc(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/tbld_ajax_donvitc"
        +"&"+CesarCode.encode("donviql_id")+"="+userInput
        ;
  }

  public String getUrlDanhSachSIMLD(
		String piPAGEID                         
		,String piREC_PER_PAGE                     
		,String psSOMAY                         
		,String psTRANGTHAI                     
		,String psSOSIM                         
		,String psSOPIN                         
		,String psSOPUK                         
  	) {
    return "/main?"+CesarCode.encode("configFile")+"=common/tracuu_sim_ajax"
		+"&"+CesarCode.encode("sys_pageid")+"="+piPAGEID                                                  
		+"&"+CesarCode.encode("sys_rec_per_page")+"="+piREC_PER_PAGE                                          
		+"&"+CesarCode.encode("SOMAY")+"="+psSOMAY                                                  
		+"&"+CesarCode.encode("TRANGTHAI")+"="+psTRANGTHAI                                          
		+"&"+CesarCode.encode("SOSIM")+"="+psSOSIM                                                  
		+"&"+CesarCode.encode("SOPIN")+"="+psSOPIN                                                  
		+"&"+CesarCode.encode("SOPUK")+"="+psSOPUK                                                  
        ;
	}
  public String getUrlDanhSachACCLD(
		String piPAGEID                         
		,String piREC_PER_PAGE                     
		,String psACCOUNT                         
		,String psCALLING                     
		,String psTRANGTHAI                         
		,String psLOAI_ACC                         
  	) {
    return "/main?"+CesarCode.encode("configFile")+"=common/tracuu_acc_ajax"
		+"&"+CesarCode.encode("sys_pageid")+"="+piPAGEID                                                  
		+"&"+CesarCode.encode("sys_rec_per_page")+"="+piREC_PER_PAGE                                          
		+"&"+CesarCode.encode("account")+"="+psACCOUNT                                                  
		+"&"+CesarCode.encode("calling")+"="+psCALLING                                          
		+"&"+CesarCode.encode("trangthai")+"="+psTRANGTHAI                                                  
		+"&"+CesarCode.encode("loai_acc")+"="+psLOAI_ACC                                                  
		
        ;
	}
  public String laytt_hopdong_ld(String schema,String userInput, String userid, String column, String piTRACUU_DB) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_hdld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"
		+userInput+"','"+CesarCode.decode(column)+"',"+piTRACUU_DB+"); end;";
  }
  public String laytt_hopdong_ld_theo_matb(String schema,String matb, String userid, String piTRACUU_DB) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_hdld_theo_matb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+matb+"',"+piTRACUU_DB+"); end;";
  }

  public String laytt_hopdong_dv(String schema,String psMA_TB, String psMA_HD) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_hopdong_dv('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_TB+"','"+psMA_HD+"'); end;";
  }
  public String laytt_mabc_theo_dvql(String schema,String piDONVIQL_ID) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_laphd.laytt_mabc_theo_dvql('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
		+piDONVIQL_ID+"); end;";
  }
  
  
  
  public String laytt_hopdong_bm(String schema,String psMA_HD, String psUSERID) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_hopdong_bm('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'); end;";
  }

  public String laytt_hopdong_dc(String schema,String userInput, String userid, String column) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_hddc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"','"+CesarCode.decode(column)+"'); end;";
  }
  public String laytt_somay_dc(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_somay_dc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }
  public String laytt_somay_dv(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_somay_dv('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }

  public String laytt_somay_cqsd(String schema,String userInput, String userid, String piLOAIHD_ID,String piTCDB, String piMaTTCu) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_somay_cqsd1('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"',"+piLOAIHD_ID+","+piTCDB+","+piMaTTCu+"); end;";
  }
  
    public String laytt_somay_cqsd1(String schema,String userInput, String userid, String piLOAIHD_ID, String tcdb) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_somay_cqsd('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"',"+piLOAIHD_ID+","+tcdb+"); end;";
  }

  public String laytt_tienmay(String schema,String piCHUNGLOAI_TB) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_tienmay('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"+piCHUNGLOAI_TB+"); end;";
  }
	/*public String laytt_cuoc_ld(String schema,String piMAHD_ID,String piLOAITB_ID,String piKIEULD_ID, String ma_tb) {
	String s ="begin ?:="+CesarCode.decode(schema)+"pttb_temp.laytt_cuoc_ld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"+piMAHD_ID+","+piLOAITB_ID+","+piKIEULD_ID+",'"+ma_tb+"'); end;";

    return s;
  }*/
  
  	public String laytt_cuoc_ld(String schema,String piMAHD_ID,String piLOAITB_ID,String piKIEULD_ID, String ma_tb , String sDSLK) {
	String sTempDSLK ="";	
	
	/*if ((sDSLK.indexOf("45") !=-1) ||(sDSLK.indexOf("46") !=-1) || (sDSLK.indexOf("52") !=-1) ||(sDSLK.indexOf("53") !=-1)) {
		sTempDSLK = "45,";
	}*/
	
	if ((sDSLK.indexOf("60") !=-1) ||(sDSLK.indexOf("61") !=-1) || (sDSLK.indexOf("62") !=-1) ||(sDSLK.indexOf("63") !=-1)||(sDSLK.indexOf("64") !=-1)||(sDSLK.indexOf("65") !=-1)) {
		sTempDSLK = "45,";
		}
	if ((sDSLK.indexOf("83") !=-1) ||(sDSLK.indexOf("84") !=-1) || (sDSLK.indexOf("85") !=-1) ||(sDSLK.indexOf("86") !=-1)||(sDSLK.indexOf("87") !=-1)||(sDSLK.indexOf("88") !=-1)) {
		sTempDSLK = "46,";
		}
	//Iphone 5C
	if ((sDSLK.indexOf("106") !=-1) ||(sDSLK.indexOf("107") !=-1) || (sDSLK.indexOf("108") !=-1) ||(sDSLK.indexOf("109") !=-1)||(sDSLK.indexOf("110") !=-1)||(sDSLK.indexOf("101") !=-1)||(sDSLK.indexOf("102") !=-1)||(sDSLK.indexOf("103") !=-1)||(sDSLK.indexOf("104") !=-1)||(sDSLK.indexOf("105") !=-1)) {
		sTempDSLK = "47,";
		}	
	//Iphone 5S
	if ((sDSLK.indexOf("116") !=-1) ||(sDSLK.indexOf("117") !=-1) || (sDSLK.indexOf("118") !=-1) ||(sDSLK.indexOf("119") !=-1)||(sDSLK.indexOf("120") !=-1)||(sDSLK.indexOf("121") !=-1)||(sDSLK.indexOf("122") !=-1)||(sDSLK.indexOf("123") !=-1)||(sDSLK.indexOf("124") !=-1)) {
		sTempDSLK = "48,";
		}	
		
	String s ="begin ?:= admin_v2.pttb_chucnang.laytt_cuoc_ld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"+piMAHD_ID+","+piLOAITB_ID+","+piKIEULD_ID+",'"+ma_tb+"','"+sTempDSLK+"'); end;";
    return s;
  }


  public String laytt_thuebao_ld(String schema,String userInput, String userid, String column) {
    return "begin ?:= admin_v2.pttb_tracuu.laytt_tbld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"','"+CesarCode.decode(column)+"'); end;";
  }
  public String laytt_visa(String thuebao_id) {
    return "begin ?:="+getSysConst("FuncSchema")+"pttb_tracuu.laytt_visa('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"+thuebao_id+"); end;";
  }

  public String laytt_thuebao_ld_in(String schema,String userInput, String userid, String column) {
	  // kiennt cap nhat ngay 11/05/2007
	  return "begin ?:=" + CesarCode.decode(schema) + "pttb_tracuu.laytt_tbld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + userInput  + "','" + CesarCode.decode(column) + "'); end;";
//    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_tbld_in('"+userInput+"','"+CesarCode.decode(userid)+"','"+CesarCode.decode(column)+"'); end;";
  }

  public String laytt_thuebao_dc(String schema,String userInput, String userid, String column) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_tbdc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"','"+CesarCode.decode(column)+"'); end;";
  }
  public String laytt_thuebao_cqsd(String schema,String userInput, String userid, String column) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_tbcqsd('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"','"+CesarCode.decode(column)+"'); end;";
  }
  public String layds_phieukshoanthanh(String schema, String piYEUCAUKS_ID)
  {
	  return "begin ?:=" + CesarCode.decode(schema) + "PTTB_LAPHD.laytt_phieuks_ld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"'," + piYEUCAUKS_ID + "); end;";
  }

  
  
  public String themmoiHDLD(String schema
									, String psMA_HD, String psMA_KH
                                     ,String psMA_CQ,String psCOQUAN,String  psNGUOI_DD
                                     ,String psCHUCDANH,String psDIENTHOAI_LH,String piQUAN_ID
                                     ,String piPHUONG_ID,String piPHO_ID
                                     ,String psSO_NHA,String psDIACHI_CT,String psSO_GT
                                     ,String pdNGAYCAP_GT,String psNOICAP_GT,String piLOAIGT_ID
                                     ,String pdNGAYSINH,String piPHAI,String piDICHVUVT_ID
                                     ,String piKIEU_LD,String psSODAIDIEN,String piLOAIKH_ID
                                     ,String piKHLON_ID,String piNGANHNGHE_ID,String piUUTIEN_ID
                                     ,String piDANGKY_DB,String piDANGKY_TV,String piDIADIEMTT_ID
                                     ,String psMA_BC,String psTEN_TT, String psDIACHI_TT,String piQUANTT_ID
                                     ,String piPHUONGTT_ID,String piPHOTT_ID,String psSOTT_NHA
                                     ,String psMS_THUE,String piNGANHANG_ID
                                     ,String psTAIKHOAN,String pdNGAY_LAPHD,String psGHICHU
                                     ,String psnguoi_cn,String psmay_cn, String piDONVIQL_ID
									 ,String psMA_NV, String piYEUCAUKS_ID,String psEMAIL,String piKHRR
									 ,String psso_gt1  ,String psngaycap_gt1 ,String psnoicap_gt1  ,int piLOAIGT_ID1,String piCHUYENKHOAN_ID 
									 ,String psKyTen , String psNguoiGT, String psMaT, String psTTTuyen, String psMa_NS
									 ) {

    String s=    "begin ? := admin_v2.pttb_laphd.themmoi_hopdong_ld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+psMA_KH+"'"
        +",'"+psMA_CQ+"'"
        +",'"+psCOQUAN+"'"
        +",'"+psNGUOI_DD+"'"
        +",'"+psCHUCDANH+"'"
        +",'"+psDIENTHOAI_LH+"'"
        +","+piQUAN_ID
        +","+piPHUONG_ID
        +","+piPHO_ID
        +",'"+psSO_NHA+"'"
        +",'"+psDIACHI_CT+"'"
		+",'"+psSO_GT+"'"
		+",'"+pdNGAYCAP_GT+"'"
		+",'"+psNOICAP_GT+"'"
		+","+piLOAIGT_ID
		+","+pdNGAYSINH
		+","+piPHAI
		
        +","+piDICHVUVT_ID
		+","+piKIEU_LD
		
        +",'"+psSODAIDIEN+"'"
        +","+piLOAIKH_ID
		+","+piKHLON_ID
		+","+piNGANHNGHE_ID
		+","+piUUTIEN_ID
		+","+piDANGKY_DB
		+","+piDANGKY_TV
		+","+piDIADIEMTT_ID
		
		+",'"+psMA_BC+"'"
		+",'"+psTEN_TT+"'"
		+",'"+psDIACHI_TT+"'"
		
        +",'"+piQUANTT_ID+"'"
        +",'"+piPHUONGTT_ID+"'"
        +",'"+piPHOTT_ID+"'"
        +",'"+psSOTT_NHA+"'"
        +",'"+psMS_THUE+"'"
        
        +","+piNGANHANG_ID
        +",'"+psTAIKHOAN+"'"
		+","+pdNGAY_LAPHD
        +","+psGHICHU
        
        +",'"+psnguoi_cn+"'"
        +",'"+psmay_cn+"'"
		+",'"+piDONVIQL_ID+"'"
		+",'"+psMA_NV+"'"
		+","+piYEUCAUKS_ID
		+",'"+psEMAIL+"'"
		+","+piKHRR
		+",'"+psso_gt1+"'"
		+",'"+psngaycap_gt1+"'"
		+",'"+psnoicap_gt1+"'"
		+","+piLOAIGT_ID1
		+","+piCHUYENKHOAN_ID
		+",'"+psKyTen+"'"
		+",'"+psNguoiGT+"'"
		+",'"+psMaT+"'"
		+",'"+psTTTuyen+"'"
		+",'"+psMa_NS+"'"		
        +");"
        +"end;"
        ;
		System.out.println(s);
    return s;

  }
public String capnhatHDLD(String schema
	,String psMA_HD                         
	,String psMA_KH                         
	,String psTEN_TT                        
	,String psNGUOI_DD                      
	,String psDIACHI_CT                     
	,String psDIACHI_TT                     
	,String psSO_GT                           
	,String psCOQUAN                        
	,String psDIENTHOAI_LH                  
	,String psMS_THUE                       
	,String psTAIKHOAN                      
	,String pdNGAY_LAPHD                    
	,String psGHICHU                        
	,String piDIADIEMTT_ID                  
	,String piDICHVUVT_ID                   
	,String piNGANHANG_ID                   
	,String piTONGTIEN_HD                   
	,String piLOAIKH_ID                     
	,String piQUAN_ID                       
	,String piPHUONG_ID                     
	,String piPHO_ID                        
	,String psNGUOI_CN                      
	,String piLOAIGT_ID                        
	,String piKIEU_LD                       
	,String psSO_NHA                        
	,String psNGAYCAP_GT                   
	,String psNOICAP_GT                    
	,String pdNGAYSINH                
	,String piPHAI                 
	,String piMA_CQ                         
	,String psMA_BC                         
	,String piKHLON_ID                      
	,String psMAY_CN                        
	
	,String piNGANHNGHE_ID                  
	,String piUUTIEN_ID                     
	,String piDANGKY_TV                     
	,String piDANGKY_DB                     
	,String psCHUCDANH                      
	,String psSODAIDIEN                     
	,String piQUANTT_ID                     
	,String piPHUONGTT_ID                   
	,String piPHOTT_ID                      
	,String psSOTT_NHA   
	,String piDONVIQL_ID   	
	,String psMA_NV
	,String psEMAIL
	,String piKH_RR 
	,String psso_gt1  ,String psngaycap_gt1 ,String psnoicap_gt1  ,int piLOAIGT_ID1,String piCHUYENKHOAN_ID      
	,String psKyTen , String psNguoiGT, String psMa_T, String psTTTuyen,String psMa_ns
	) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.capnhat_hopdong_ld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"',"                         
		+"'"+psMA_KH+"',"                         
		+"'"+psTEN_TT+"',"                        
		+"'"+psNGUOI_DD+"',"                      
		+"'"+psDIACHI_CT+"',"                     
		+"'"+psDIACHI_TT+"',"                     
		+"'"+psSO_GT+"',"                           
		+"'"+psCOQUAN+"',"                        
		+"'"+psDIENTHOAI_LH+"',"                  
		+"'"+psMS_THUE+"',"                       
		+"'"+psTAIKHOAN+"',"                      
		+pdNGAY_LAPHD+","                         
		+psGHICHU+","                        
		+piDIADIEMTT_ID+","                       
		+piDICHVUVT_ID+","                        
		+piNGANHANG_ID+","                        
		+piTONGTIEN_HD+","                        
		+piLOAIKH_ID+","                          
		+piQUAN_ID+","                            
		+piPHUONG_ID+","                          
		+piPHO_ID+","                             
		+"'"+psNGUOI_CN+"',"                      
		+piLOAIGT_ID+","                        
		+piKIEU_LD+","                            
		+"'"+psSO_NHA+"',"                        
		+"'"+psNGAYCAP_GT+"',"                   
		+"'"+psNOICAP_GT+"',"                    
		+pdNGAYSINH+","                
		+piPHAI+","                 
		+piMA_CQ+","                              
		+"'"+psMA_BC+"',"                         
		+piKHLON_ID+","                           
		+"'"+psMAY_CN+"',"                        
		+piNGANHNGHE_ID+","                       
		+piUUTIEN_ID+","                          
		+piDANGKY_TV+","                          
		+piDANGKY_DB+","                          
		+"'"+psCHUCDANH+"',"                      
		+"'"+psSODAIDIEN+"',"                     
		+"'"+piQUANTT_ID+"',"                          
		+"'"+piPHUONGTT_ID+"',"                        
		+"'"+piPHOTT_ID+"',"                           
		+"'"+psSOTT_NHA+"',"                      
		+piDONVIQL_ID
		+",'"+psMA_NV+"'"
		+",'"+psEMAIL+"'"
		+","+piKH_RR 
		+",'"+psso_gt1+"'"
		+",'"+psngaycap_gt1+"'"
		+",'"+psnoicap_gt1+"'"
		+","+piLOAIGT_ID1
		+","+piCHUYENKHOAN_ID
		+",'"+psKyTen+"'"
		+",'"+psNguoiGT+"'"
		+",'"+psMa_T+"'"
		+",'"+psTTTuyen+"'"
		+",'"+psMa_ns+"'"		
	+");"
        +"end;"
        ;
    //System.out.println(s);
    return s;

  }  
  
  
/*
   public String themmoiTBLD(String schema ,String psNGUOI_CN ,String piDONVIQL_ID ,String psSO_NHA ,String piTHUTU_IN                      
						,String psSOMAY_TN ,String psMA_TB ,String psMAY_CN ,String psMA_BC ,String piDONVITC_ID                    
						,String piTRACUU_DB ,String psGIAPDANH                      
						,String piCUOCNH_ID                     
						,String piKHUYENMAI                     
						,String piDANGKY_DB                     
						,String psTEN_DB                        
						,String piTRAMVT_ID                     
						,String piVISA_ID                       
						,String piTHUEBAO_ID                    
						,String psMA_HD                         
						,String psTEN_TB                        
						,String psDIACHI                        
						,String piLOAITB_ID                     
						,String psDOITUONG_ID                   
						,String piDANGKY_DV                       
						,String piTRAGOP                          
						,String piCUOC_LD                       
						,String piCUOC_DV                       
						,String piTRANGTHAI                     
						,String piVAT_LD                        
						,String piVAT_DV                        
						,String piLOAIDB_ID                     
						,String piINCHITIET                     
						,String piCHUNGLOAI_ID                  
						,String piTIENMAY                       
						,String piVAT_MAY                       
						,String psGHICHU                        
						,String piQUAN_ID                       
						,String piPHUONG_ID                     
						,String piPHO_ID                        
						,String piKIEULD_ID   
						,String psDANHSACH_DV
						,String psDANHSACH_DVNOIDUNG						
						//,String piDICHVUVT_ID                        
						,String piLOAIKH_ID   
						
						,String piLOAIDAY_ID    
						,String piSLDAY   
						,String piDONVILD_ID
						,String pdNGAY_PDV 
						,String piMANV_ID
						,String piKHUYENMAI_ID
						,String piTIEN_KM
						
						,String piloaidcbc,String  pikieubc,String pimabaocuoc
						,String pingaysinh
						
						,String pshidDSLK
						,String pshidDSTenLK
						,String pshidDSSLLK
						,String psTBNhanTK
						,String psThangcamket
						,String piKH_DoanhNghiep 
						) {
    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_vsms.themmoi_thuebao_ld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psNGUOI_CN+"',"                      
			+piDONVIQL_ID+","                         
			+"'"+psSO_NHA+"',"                        
			+piTHUTU_IN+","                           
			+"'"+psSOMAY_TN+"',"                      
			+"'"+psMA_TB+"',"                         
			+"'"+psMAY_CN+"',"                        
			+"'"+psMA_BC+"',"                         
			+piDONVITC_ID+","                         
			+piTRACUU_DB+","                          
			+"'"+psGIAPDANH+"',"                      
			+piCUOCNH_ID+","                          
			+piKHUYENMAI+","   
			+piDANGKY_DB+","                          
			+"'"+psTEN_DB+"',"                        
			+piTRAMVT_ID+","                          
			+piVISA_ID+","                            
			+piTHUEBAO_ID+","                         
			+"'"+psMA_HD+"',"                         
			+"'"+psTEN_TB+"',"                        
			+"'"+psDIACHI+"',"                        
			+piLOAITB_ID+","                          
			+"'"+psDOITUONG_ID+"',"                   
			+piDANGKY_DV+","     
			
			+piTRAGOP+","                               
			+piCUOC_LD+","                            
			+piCUOC_DV+","                            
			+piTRANGTHAI+","                          
			+piVAT_LD+","                             
			+piVAT_DV+","                             
			+piLOAIDB_ID+","                          
			+piINCHITIET+","                          
			+piCHUNGLOAI_ID+","                       
			+piTIENMAY+","                            
			+piVAT_MAY+","                            
			+psGHICHU+","                        
			+piQUAN_ID+","                            
			+piPHUONG_ID+","                          
			+piPHO_ID+","                             
			+piKIEULD_ID+","     
			+"'"+psDANHSACH_DV+"',"      
			+"'"+psDANHSACH_DVNOIDUNG+"',"			
			//+piDICHVUVT_ID+","                             
			+piLOAIKH_ID+"," 

			+piLOAIDAY_ID+","    
			+piSLDAY+","   
			+piDONVILD_ID+","
			+pdNGAY_PDV+"," 
			+piMANV_ID+ ","
			+"'"+piKHUYENMAI_ID + "',"
			+piTIEN_KM
			+","+piloaidcbc
			+","+pikieubc
			+","+pimabaocuoc
			+","+pingaysinh
						
			+",'"+pshidDSLK + "'"
			+",'"+pshidDSTenLK + "'"
			+",'"+pshidDSSLLK + "'"
			+",'"+psTBNhanTK+ "'"
			+",'"+psThangcamket+ "'"	
			+",'"+piKH_DoanhNghiep+ "'"				
        +");"
        +"end;";
    return s;

  }  
 
 public String themmoiTBLD(String schema ,String psNGUOI_CN ,String piDONVIQL_ID ,String psSO_NHA ,String piTHUTU_IN                      
						,String psSOMAY_TN ,String psMA_TB ,String psMAY_CN ,String psMA_BC ,String piDONVITC_ID                    
						,String piTRACUU_DB ,String psGIAPDANH                      
						,String piCUOCNH_ID                     
						,String piKHUYENMAI                     
						,String piDANGKY_DB                     
						,String psTEN_DB                        
						,String piTRAMVT_ID                     
						,String piVISA_ID                       
						,String piTHUEBAO_ID                    
						,String psMA_HD                         
						,String psTEN_TB                        
						,String psDIACHI                        
						,String piLOAITB_ID                     
						,String psDOITUONG_ID                   
						,String piDANGKY_DV                       
						,String piTRAGOP                          
						,String piCUOC_LD                       
						,String piCUOC_DV                       
						,String piTRANGTHAI                     
						,String piVAT_LD                        
						,String piVAT_DV                        
						,String piLOAIDB_ID                     
						,String piINCHITIET                     
						,String piCHUNGLOAI_ID                  
						,String piTIENMAY                       
						,String piVAT_MAY                       
						,String psGHICHU                        
						,String piQUAN_ID                       
						,String piPHUONG_ID                     
						,String piPHO_ID                        
						,String piKIEULD_ID   
						,String psDANHSACH_DV
						,String psDANHSACH_DVNOIDUNG						
						//,String piDICHVUVT_ID                        
						,String piLOAIKH_ID   
						
						,String piLOAIDAY_ID    
						,String piSLDAY   
						,String piDONVILD_ID
						,String pdNGAY_PDV 
						,String piMANV_ID
						,String piKHUYENMAI_ID
						,String piTIEN_KM
						
						,String piloaidcbc,String  pikieubc,String pimabaocuoc
						,String pingaysinh
						
						,String pshidDSLK
						,String pshidDSTenLK
						,String pshidDSSLLK
						,String psTBNhanTK
						,String psThangcamket
						,String piKH_DoanhNghiep 
						,String psNhomMBVNN
						,String piSerial
						,String piImei
						,String piGoi  
						) {
	String s=    "begin ? := admin_v2.pttb_tracuu.themmoi_thuebao_ld('"+getUserVar("userID")	  
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psNGUOI_CN+"',"                      
			+piDONVIQL_ID+","                         
			+"'"+psSO_NHA+"',"                        
			+piTHUTU_IN+","                           
			+"'"+psSOMAY_TN+"',"                      
			+"'"+psMA_TB+"',"                         
			+"'"+psMAY_CN+"',"                        
			+"'"+psMA_BC+"',"                         
			+piDONVITC_ID+","                         
			+piTRACUU_DB+","                          
			+"'"+psGIAPDANH+"',"                      
			+piCUOCNH_ID+","                          
			+piKHUYENMAI+","   
			+piDANGKY_DB+","                          
			+"'"+psTEN_DB+"',"                        
			+piTRAMVT_ID+","                          
			+piVISA_ID+","                            
			+piTHUEBAO_ID+","                         
			+"'"+psMA_HD+"',"                         
			+"'"+psTEN_TB+"',"                        
			+"'"+psDIACHI+"',"                        
			+piLOAITB_ID+","                          
			+"'"+psDOITUONG_ID+"',"                   
			+piDANGKY_DV+","     
			
			+piTRAGOP+","                               
			+piCUOC_LD+","                            
			+piCUOC_DV+","                            
			+piTRANGTHAI+","                          
			+piVAT_LD+","                             
			+piVAT_DV+","                             
			+piLOAIDB_ID+","                          
			+piINCHITIET+","                          
			+piCHUNGLOAI_ID+","                       
			+piTIENMAY+","                            
			+piVAT_MAY+","                            
			+psGHICHU+","                        
			+piQUAN_ID+","                            
			+piPHUONG_ID+","                          
			+piPHO_ID+","                             
			+piKIEULD_ID+","     
			+"'"+psDANHSACH_DV+"',"      
			+"'"+psDANHSACH_DVNOIDUNG+"',"			
			//+piDICHVUVT_ID+","                             
			+piLOAIKH_ID+"," 

			+piLOAIDAY_ID+","    
			+piSLDAY+","   
			+piDONVILD_ID+","
			+pdNGAY_PDV+"," 
			+piMANV_ID+ ","
			+"'"+piKHUYENMAI_ID + "',"
			+piTIEN_KM
			+","+piloaidcbc
			+","+pikieubc
			+","+pimabaocuoc
			+","+pingaysinh
						
			+",'"+pshidDSLK + "'"
			+",'"+pshidDSTenLK + "'"
			+",'"+pshidDSSLLK + "'"
			+",'"+psTBNhanTK+ "'"
			+",'"+psThangcamket+ "'"	
			+",'"+piKH_DoanhNghiep+ "'"			
			+",'"+psNhomMBVNN+ "'"			
			+",'"+piSerial+ "'"			
			+",'"+piImei+ "'"	
			+",'"+piGoi+"'"
        +");"
        +"end;";
    return s;

  }  
 */ 

/*  
public String capnhatTBLD(String schema
						,String psNGUOI_CN                      
						,String piDONVIQL_ID                    
						,String psSO_NHA                        
						,String piTHUTU_IN                      
						,String psSOMAY_TN                      
						,String psMA_TB                         
						,String psMAY_CN                        
						,String psMA_BC                         
						,String piDONVITC_ID                    
						,String piTRACUU_DB                     
						,String psGIAPDANH                      
						,String piCUOCNH_ID                     
						,String piKHUYENMAI                     
						,String piDANGKY_DB                     
						,String psTEN_DB                        
						,String piTRAMVT_ID                     
						,String piVISA_ID                       
						,String piTHUEBAO_ID                    
						,String psMA_HD                         
						,String psTEN_TB                        
						,String psDIACHI                        
						,String piLOAITB_ID                     
						,String psDOITUONG_ID                   
						,String piDANGKY_DV                       
						,String piTRAGOP                          
						,String piCUOC_LD                       
						,String piCUOC_DV                       
						,String piTRANGTHAI                     
						,String piVAT_LD                        
						,String piVAT_DV                        
						,String piLOAIDB_ID                     
						,String piINCHITIET                     
						,String piCHUNGLOAI_ID                  
						,String piTIENMAY                       
						,String piVAT_MAY                       
						,String psGHICHU                        
						,String piQUAN_ID                       
						,String piPHUONG_ID                     
						,String piPHO_ID                        
						,String piKIEULD_ID   
						,String psDANHSACH_DV                     
						//,String piDICHVUVT_ID                        
						,String piLOAIKH_ID   
						,String psMA_TB_CU   
						
						,String piLOAIDAY_ID    
						,String piSLDAY   
						,String piDONVILD_ID
						,String pdNGAY_PDV 
						,String piMANV_ID
						, String piKHUYENMAI_ID
						, String piTIEN_KM
						,String piloaidcbc,String  pikieubc,String pimabaocuoc
						
						,String pshidDSLK
						,String pshidDSTenLK
						,String pshidDSSLLK
						,String psTBNhanTK 
						,String psThangcamket
						,String piKH_DoanhNghiep
						,String pinhommb_vnn
						,String piSerial
						,String piImei
						,String piGoi
						
									 ) {
    String s=    "begin ? := admin_v2.pttb_tracuu.capnhat_thuebao_ld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psNGUOI_CN+"',"                      
			+piDONVIQL_ID+","                         
			+"'"+psSO_NHA+"',"                        
			+piTHUTU_IN+","                           
			+"'"+psSOMAY_TN+"',"                      
			+"'"+psMA_TB+"',"                         
			+"'"+psMAY_CN+"',"                        
			+"'"+psMA_BC+"',"                         
			+piDONVITC_ID+","                         
			+piTRACUU_DB+","                          
			+"'"+psGIAPDANH+"',"                      
			+piCUOCNH_ID+","                          
			+piKHUYENMAI+","                          
			+piDANGKY_DB+","                          
			+"'"+psTEN_DB+"',"                        
			+piTRAMVT_ID+","                          
			+piVISA_ID+","                            
			+piTHUEBAO_ID+","                         
			+"'"+psMA_HD+"',"                         
			+"'"+psTEN_TB+"',"                        
			+"'"+psDIACHI+"',"                        
			+piLOAITB_ID+","                          
			+"'"+psDOITUONG_ID+"',"                   
			+piDANGKY_DV+","                            
			+piTRAGOP+","                               
			+piCUOC_LD+","                            
			+piCUOC_DV+","                            
			+piTRANGTHAI+","                          
			+piVAT_LD+","                             
			+piVAT_DV+","                             
			+piLOAIDB_ID+","                          
			+piINCHITIET+","                          
			+piCHUNGLOAI_ID+","                       
			+piTIENMAY+","                            
			+piVAT_MAY+","                            
			+psGHICHU+","                        
			+piQUAN_ID+","                            
			+piPHUONG_ID+","                          
			+piPHO_ID+","                             
			+piKIEULD_ID+","     
			+"'"+psDANHSACH_DV+"',"               
			//+piDICHVUVT_ID+","                             
			+piLOAIKH_ID+","                             
			+"'"+psMA_TB_CU+"',"     

			+piLOAIDAY_ID+","    
			+piSLDAY+","   
			+piDONVILD_ID+","
			+pdNGAY_PDV+","
			+ piMANV_ID + "," 
			+ piKHUYENMAI_ID + ","
			+ piTIEN_KM
			+","+piloaidcbc
			+","+pikieubc
			+","+pimabaocuoc
			
			+",'"+pshidDSLK + "'"
			+",'"+pshidDSTenLK + "'"
			+",'"+pshidDSSLLK + "'"
			+",'"+psTBNhanTK+ "'"
			+",'"+psThangcamket+ "'"			
			+",'"+piKH_DoanhNghiep+ "'"
			+",'"+pinhommb_vnn+ "'"			
			+",'"+piSerial+ "'"			
			+",'"+piImei+ "'"	
			+",'"+piGoi+"'"
        +");"
        +"end;"
        ;
		System.out.println(s);
	
    return s;

  }*/
  
public String capnhatTBLD_in(String schema
						,String psNGUOI_CN                      
						,String piDONVIQL_ID                    
						,String psSO_NHA                        
						,String piTHUTU_IN                      
						,String psSOMAY_TN                      
						,String psMA_TB                         
						,String psMAY_CN                        
						,String psMA_BC                         
						,String piDONVITC_ID                    
						,String piTRACUU_DB                     
						,String psGIAPDANH                      
						,String piCUOCNH_ID                     
						,String piKHUYENMAI                     
						,String piDANGKY_DB                     
						,String psTEN_DB                        
						,String piTRAMVT_ID                     
						,String piVISA_ID                       
						,String piTHUEBAO_ID                    
						,String psMA_HD                         
						,String psTEN_TB                        
						,String psDIACHI                        
						,String piLOAITB_ID                     
						,String psDOITUONG_ID                   
						,String piDANGKY_DV                       
						,String piTRAGOP                          
						,String piCUOC_LD                       
						,String piCUOC_DV                       
						,String piTRANGTHAI                     
						,String piVAT_LD                        
						,String piVAT_DV                        
						,String piLOAIDB_ID                     
						,String piINCHITIET                     
						,String piCHUNGLOAI_ID                  
						,String piTIENMAY                       
						,String piVAT_MAY                       
						,String psGHICHU                        
						,String piQUAN_ID                       
						,String piPHUONG_ID                     
						,String piPHO_ID                        
						,String piKIEULD_ID   
						,String psDANHSACH_DV                     
						//,String piDICHVUVT_ID                        
						,String piLOAIKH_ID   
						,String psMA_TB_CU   
						
						,String piLOAIDAY_ID    
						,String piSLDAY   
						,String piDONVILD_ID
						,String pdNGAY_PDV 
						,String piMANV_ID 

						,String piSLOT_BRAS
						,String psPORT_BRAS
						,String piVPI_NNI
						,String piVCI_NNI
						,String piCARD_ADSL
						,String piPORT_ADSL
						,String pdNGAY_KHAI
						,String psPASSWORD
						,String piKHUYENMAI_ID
						,String piTIEN_KM

						,String picuoc_tron_goi
						,String psip_tinh
						,String psemail
						
									 ) {
    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.capnhat_thuebao_ld_in('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psNGUOI_CN+"',"                      
			+piDONVIQL_ID+","                         
			+"'"+psSO_NHA+"',"                        
			+piTHUTU_IN+","                           
			+"'"+psSOMAY_TN+"',"                      
			+"'"+psMA_TB+"',"                         
			+"'"+psMAY_CN+"',"                        
			+"'"+psMA_BC+"',"                         
			+piDONVITC_ID+","                         
			+piTRACUU_DB+","                          
			+"'"+psGIAPDANH+"',"                      
			+piCUOCNH_ID+","                          
			+piKHUYENMAI +"," //                         
			+piDANGKY_DB+","                          
			+"'"+psTEN_DB+"',"                        
			+piTRAMVT_ID+","                          
			+piVISA_ID+","                            
			+piTHUEBAO_ID+","                         
			+"'"+psMA_HD+"',"                         
			+"'"+psTEN_TB+"',"                        
			+"'"+psDIACHI+"',"                        
			+piLOAITB_ID+","                          
			+"'"+psDOITUONG_ID+"',"                   
			+piDANGKY_DV+","                            
			+piTRAGOP+","                               
			+piCUOC_LD+","                            
			+piCUOC_DV+","                            
			+piTRANGTHAI+","                          
			+piVAT_LD+","                             
			+piVAT_DV+","                             
			+piLOAIDB_ID+","                          
			+piINCHITIET+","                          
			+piCHUNGLOAI_ID+","                       
			+piTIENMAY+","                            
			+piVAT_MAY+","                            
			+psGHICHU+","                        
			+piQUAN_ID+","                            
			+piPHUONG_ID+","                          
			+piPHO_ID+","                             
			+piKIEULD_ID+","     
			+"'"+psDANHSACH_DV+"',"               
			//+piDICHVUVT_ID+","                             
			+piLOAIKH_ID+","                             
			+"'"+psMA_TB_CU+"',"     

			+piLOAIDAY_ID+","    
			+piSLDAY+","   
			+piDONVILD_ID+","
			+pdNGAY_PDV+"," 
			+piMANV_ID +","

			+piSLOT_BRAS+","   
			+"'"+psPORT_BRAS+"',"
			+piVPI_NNI+","    
			+piVCI_NNI+","   
			+piCARD_ADSL+","    
			+piPORT_ADSL+","   

			+pdNGAY_KHAI+"," 
			+"'"+psPASSWORD+"',"
			+ piKHUYENMAI_ID + ","
			+ piTIEN_KM+ ","
			
			+picuoc_tron_goi+","
			+"'"+psip_tinh+"',"
			+"'"+psemail+"'"
			
        +");"
        +"end;"
        ;
    return s;

  }
  public String themmoiTBLD_in(String schema
						,String psNGUOI_CN                      
						,String piDONVIQL_ID                    
						,String psSO_NHA                        
						,String piTHUTU_IN                      
						,String psSOMAY_TN                      
						,String psMA_TB                         
						,String psMAY_CN                        
						,String psMA_BC                         
						,String piDONVITC_ID                    
						,String piTRACUU_DB                     
						,String psGIAPDANH                      
						,String piCUOCNH_ID                     
						,String piKHUYENMAI                     
						,String piDANGKY_DB                     
						,String psTEN_DB                        
						,String piTRAMVT_ID                     
						,String piVISA_ID                       
						,String piTHUEBAO_ID                    
						,String psMA_HD                         
						,String psTEN_TB                        
						,String psDIACHI                        
						,String piLOAITB_ID                     
						,String psDOITUONG_ID                   
						,String piDANGKY_DV                       
						,String piTRAGOP                          
						,String piCUOC_LD                       
						,String piCUOC_DV                       
						,String piTRANGTHAI                     
						,String piVAT_LD                        
						,String piVAT_DV                        
						,String piLOAIDB_ID                     
						,String piINCHITIET                     
						,String piCHUNGLOAI_ID                  
						,String piTIENMAY                       
						,String piVAT_MAY                       
						,String psGHICHU                        
						,String piQUAN_ID                       
						,String piPHUONG_ID                     
						,String piPHO_ID                        
						,String piKIEULD_ID   
						,String psDANHSACH_DV                     
						//,String piDICHVUVT_ID                        
						,String piLOAIKH_ID   
						
						,String piLOAIDAY_ID    
						,String piSLDAY   
						,String piDONVILD_ID
						,String pdNGAY_PDV 
						,String piMANV_ID 

						,String piSLOT_BRAS
						,String psPORT_BRAS
						,String piVPI_NNI
						,String piVCI_NNI
						,String piCARD_ADSL
						,String piPORT_ADSL
						,String pdNGAY_KHAI
						,String psPASSWORD
						, String piKHUYENMAI_ID
						, String piTIEN_KM
						
						,String picuoc_tron_goi
						,String psip_tinh
						,String psemail
						
									 ) {
    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.themmoi_thuebao_ld_in('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psNGUOI_CN+"',"                      
			+piDONVIQL_ID+","                         
			+"'"+psSO_NHA+"',"                        
			+piTHUTU_IN+","                           
			+"'"+psSOMAY_TN+"',"                      
			+"'"+psMA_TB+"',"                         
			+"'"+psMAY_CN+"',"                        
			+"'"+psMA_BC+"',"                         
			+piDONVITC_ID+","                         
			+piTRACUU_DB+","                          
			+"'"+psGIAPDANH+"',"                      
			+piCUOCNH_ID+","                          
			+piKHUYENMAI +"," 
			+piDANGKY_DB+","                          
			+"'"+psTEN_DB+"',"                        
			+piTRAMVT_ID+","                          
			+piVISA_ID+","                            
			+piTHUEBAO_ID+","                         
			+"'"+psMA_HD+"',"                         
			+"'"+psTEN_TB+"',"                        
			+"'"+psDIACHI+"',"                        
			+piLOAITB_ID+","                          
			+"'"+psDOITUONG_ID+"',"                   
			+piDANGKY_DV+","                            
			+piTRAGOP+","                               
			+piCUOC_LD+","                            
			+piCUOC_DV+","                            
			+piTRANGTHAI+","                          
			+piVAT_LD+","                             
			+piVAT_DV+","                             
			+piLOAIDB_ID+","                          
			+piINCHITIET+","                          
			+piCHUNGLOAI_ID+","                       
			+piTIENMAY+","                            
			+piVAT_MAY+","                            
			+psGHICHU+","                        
			+piQUAN_ID+","                            
			+piPHUONG_ID+","                          
			+piPHO_ID+","                             
			+piKIEULD_ID+","     
			+"'"+psDANHSACH_DV+"',"               
			//+piDICHVUVT_ID+","                             
			+piLOAIKH_ID+"," 

			+piLOAIDAY_ID+","    
			+piSLDAY+","   
			+piDONVILD_ID+","
			+pdNGAY_PDV+"," 
			+piMANV_ID +"," 
			
			+piSLOT_BRAS+","   
			+"'"+psPORT_BRAS+"',"
			+piVPI_NNI+","    
			+piVCI_NNI+","   
			+piCARD_ADSL+","    
			+piPORT_ADSL+","   

			+pdNGAY_KHAI+"," 
			+"'"+psPASSWORD+"',"
			+ piKHUYENMAI_ID + ","
			+ piTIEN_KM+ ","

			+picuoc_tron_goi+","
			+"'"+psip_tinh+"',"
			+"'"+psemail+"'"
			
        +");"
        +"end;"
        ;
    return s;

  }

  public String xoaTBLD(String schema
						,String piTHUEBAO_ID                    
						,String psMA_HD                         						                      
									 ) {
    String s=    "begin ? := admin_v2.pttb_hmm.xoa_thuebao_ld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
			+piTHUEBAO_ID+","                         
			+"'"+psMA_HD+"'"                         		                
        +");"
        +"end;"
        ;
    return s;

  }
  public String themmoiHDDC(String schema
									,String psMA_HD                         
									,String psMA_KH                         
									,String psTEN_TT_MOI                    
									,String psDIACHITT_MOI                  
									,String psNGUOI_DD                      
									,String psDIENTHOAI_LH                  
									,String pdNGAY_LAPHD                    
									,String pdNGAY_TT                       
									,String piTRANGTHAIHD_ID                
									,String psGHICHU                        
									,String psNGUOI_CN                      
									,String piQUAN_ID                       
									,String piPHUONG_ID                     
									,String piPHO_ID                        
									,String psMA_KH_MOI                     
									,String piLOAIKH_ID                     
									,String psSO_NHA                        
									,String psMA_BC                         
									,String psCOQUAN_MOI                    
									,String psDIACHICT_MOI                  
									,String psTEN_TT_CU                     
									,String piQUANTT_ID                     
									,String piPHUONGTT_ID                   
									,String piPHOTT_ID                      
									,String psSOTT_NHA                      
									
									,String piLAY_MAKH_MOI
									) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.themmoi_hopdong_dc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"',"                         
			+"'"+psMA_KH+"',"                         
			+"'"+psTEN_TT_MOI+"',"                    
			+"'"+psDIACHITT_MOI+"',"                  
			+"'"+psNGUOI_DD+"',"                      
			+"'"+psDIENTHOAI_LH+"',"                  
			+pdNGAY_LAPHD+","                         
			+pdNGAY_TT+","                            
			+piTRANGTHAIHD_ID+","                     
			+psGHICHU+","                        
			+"'"+psNGUOI_CN+"',"                      
			+piQUAN_ID+","                            
			+piPHUONG_ID+","                          
			+piPHO_ID+","                             
			+"'"+psMA_KH_MOI+"',"                     
			+piLOAIKH_ID+","                          
			+"'"+psSO_NHA+"',"                        
			+"'"+psMA_BC+"',"                         
			+"'"+psCOQUAN_MOI+"',"                    
			+"'"+psDIACHICT_MOI+"',"                  
			+"'"+psTEN_TT_CU+"',"                     
			+piQUANTT_ID+","                          
			+piPHUONGTT_ID+","                        
			+piPHOTT_ID+","                           
			+"'"+psSOTT_NHA+"',"                      
			+piLAY_MAKH_MOI
        +");"
        +"end;"
        ;
    return s;

  }
  public String capnhatHDDC(String schema
									,String psMA_HD                         
									,String psMA_KH                         
									,String psTEN_TT_MOI                    
									,String psDIACHITT_MOI                  
									,String psNGUOI_DD                      
									,String psDIENTHOAI_LH                  
									,String pdNGAY_LAPHD                    
									,String pdNGAY_TT                       
									,String piTRANGTHAIHD_ID                
									,String psGHICHU                        
									,String psNGUOI_CN                      
									,String piQUAN_ID                       
									,String piPHUONG_ID                     
									,String piPHO_ID                        
									,String psMA_KH_MOI                     
									,String piLOAIKH_ID                     
									,String psSO_NHA                        
									,String psMA_BC                         
									,String psCOQUAN_MOI                    
									,String psDIACHICT_MOI                  
									,String psTEN_TT_CU                     
									,String piQUANTT_ID                     
									,String piPHUONGTT_ID                   
									,String piPHOTT_ID                      
									,String psSOTT_NHA                      
									
									,String piLAY_MAKH_MOI
									) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.capnhat_hopdong_dc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"',"                         
			+"'"+psMA_KH+"',"                         
			+"'"+psTEN_TT_MOI+"',"                    
			+"'"+psDIACHITT_MOI+"',"                  
			+"'"+psNGUOI_DD+"',"                      
			+"'"+psDIENTHOAI_LH+"',"                  
			+pdNGAY_LAPHD+","                         
			+pdNGAY_TT+","                            
			+piTRANGTHAIHD_ID+","                     
			+psGHICHU+","                        
			+"'"+psNGUOI_CN+"',"                      
			+piQUAN_ID+","                            
			+piPHUONG_ID+","                          
			+piPHO_ID+","                             
			+"'"+psMA_KH_MOI+"',"                     
			+piLOAIKH_ID+","                          
			+"'"+psSO_NHA+"',"                        
			+"'"+psMA_BC+"',"                         
			+"'"+psCOQUAN_MOI+"',"                    
			+"'"+psDIACHICT_MOI+"',"                  
			+"'"+psTEN_TT_CU+"',"                     
			+piQUANTT_ID+","                          
			+piPHUONGTT_ID+","                        
			+piPHOTT_ID+","                           
			+"'"+psSOTT_NHA+"',"                      
			+piLAY_MAKH_MOI
        +");"
        +"end;"
        ;
    return s;

  }
public String xoaHDDC(String schema
						,String psMA_HD                         
						,String psROWID                        
									 ) {
    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.xoa_hopdong_dc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"                         
			+","+psROWID
        +");"
        +"end;"
        ;
    return s;

  }
  public String xoaHDBM(String schema
						,String psMA_HD                         
						,String psROWID                        
									 ) {
    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.xoa_hopdong_bm('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"                         
			+","+psROWID
        +");"
        +"end;"
        ;
    return s;

  }
public String xoaHDCQSD(String schema
						,String psMA_HD                         
						,String psROWID                        
									 ) {
    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.xoa_hopdong_cqsd('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"                         
			+","+psROWID
        +");"
        +"end;"
        ;
    return s;

  }

  public String themmoiTBDC(
  						String schema
						,String psTENTB_CU                      
						,String psSO_NHA                        
						,String psSOMAY_TN                      
							
						,String psTEN_DB                        
						,String psMA_HD                         
						,String psSOMAY                         
						,String psSOMOI                         
							
						,String piLOAITB_ID                     
						,String psTEN_TB                        
						,String psDIACHI_MOI                    
						,String piCUOC_DV                       
							
						,String piVAT                           
						,String piTRANGTHAI                     
						,String psNGUOI_CN                      
						,String piQUAN_ID                       
							
						,String piPHUONG_ID                     
						,String piPHO_ID                        
						,String psGHICHU                        
						,String psDIACHI_CU                     
						
						,String piLOAIDAY_ID    
						,String piSLDAY   
						,String piDONVILD_ID
						,String pdNGAY_PDV 
							
						,String piMANV_ID 
						,String piDOISO
						,String psCUOC_DS
						,String psVAT_DS
							
						,String psCUOC_DT
						,String psVAT_DT
						
									 ) {
    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.themmoi_thuebao_dc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psTENTB_CU+"',"                      
			+"'"+psSO_NHA+"',"                        
			+"'"+psSOMAY_TN+"',"                      
			+"'"+psTEN_DB+"',"                        
			+"'"+psMA_HD+"',"                         
			+"'"+psSOMAY+"',"                         
			+"'"+psSOMOI+"',"                         
			+piLOAITB_ID+","                          
			+"'"+psTEN_TB+"',"                        
			+"'"+psDIACHI_MOI+"',"                    
			+piCUOC_DV+","                            
			+piVAT+","                                
			+piTRANGTHAI+","                          
			+"'"+psNGUOI_CN+"',"                      
			+piQUAN_ID+","                            
			+piPHUONG_ID+","                          
			+piPHO_ID+","                             
			+psGHICHU+","                        
			+"'"+psDIACHI_CU+"',"       
			
			+piLOAIDAY_ID+","    
			+piSLDAY+","   
			+piDONVILD_ID+","
			+pdNGAY_PDV+"," 
			+piMANV_ID +","
			+piDOISO+","
			+"'"+psCUOC_DS+"',"  
			+"'"+psVAT_DS+"',"  
			+"'"+psCUOC_DT+"',"  
			+"'"+psVAT_DT+"'"  
			
        +");"
        +"end;"
        ;
    return s;

  }
  public String capnhatTBDC(
  						String schema
						,int piDICHVU_VT
						,String psTENTB_CU                      
						,String psSO_NHA                        
							
						,String psSOMAY_TN                      
						,String psTEN_DB                        
						,String psMA_HD                         
						,String psSOMAY                         
							
						,String psSOMOI                         
						,String piLOAITB_ID                     
						,String psTEN_TB                        
						,String psDIACHI_MOI                    
							
						,String piCUOC_DV                       
						,String piVAT                           
						,String piTRANGTHAI                     
						,String psNGUOI_CN                      
							
						,String piQUAN_ID                       
						,String piPHUONG_ID                     
						,String piPHO_ID                        
						,String psGHICHU                        
							
						,String psDIACHI_CU                     
						,String psSOMOI_CU                     						
						,String piLOAIDAY_ID    
						,String piSLDAY   
							
						,String piDONVILD_ID
						,String pdNGAY_PDV 
						,String piMANV_ID 
						,String piDOISO
							
						,String psCUOC_DS
						,String psVAT_DS
						,String psCUOC_DT
						,String psVAT_DT	
						
									 ) {
    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.capnhat_thuebao_dc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
			+piDICHVU_VT+","
			+"'"+psTENTB_CU+"',"                      
			+"'"+psSO_NHA+"',"                        
			+"'"+psSOMAY_TN+"',"                      
			+"'"+psTEN_DB+"',"                        
			+"'"+psMA_HD+"',"                         
			+"'"+psSOMAY+"',"                         
			+"'"+psSOMOI+"',"                         
			+piLOAITB_ID+","                          
			+"'"+psTEN_TB+"',"                        
			+"'"+psDIACHI_MOI+"',"                    
			+piCUOC_DV+","                            
			+piVAT+","                                
			+piTRANGTHAI+","                          
			+"'"+psNGUOI_CN+"',"                      
			+piQUAN_ID+","                            
			+piPHUONG_ID+","                          
			+piPHO_ID+","                             
			+psGHICHU+","                        
			+"'"+psDIACHI_CU+"',"                     
			+"'"+psSOMOI_CU+"',"                 

			+piLOAIDAY_ID+","    
			+piSLDAY+","   
			+piDONVILD_ID+","
			+pdNGAY_PDV+"," 
			+piMANV_ID+","  
			+piDOISO+","
			+"'"+psCUOC_DS+"',"  
			+"'"+psVAT_DS+"',"  
			+"'"+psCUOC_DT+"',"  
			+"'"+psVAT_DT+"'"  
			
        +");"
        +"end;"
        ;
    return s;

  }
  
  
public String xoaTBDC(String schema
						,String psSOMAY                    
						,String psMA_HD                         
						//,String piDICHVUVT_ID                        
									 ) {
    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.xoa_thuebao_dc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psSOMAY+"',"                         
			+"'"+psMA_HD+"'"                         
		//	+piDICHVUVT_ID                  
        +");"
        +"end;"
        ;
    return s;

  }
  public String laytt_hopdong_cq(String schema,String userInput, String userid, String column,String piLOAIHD_ID) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_hdcq('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"','"+CesarCode.decode(column)+"',"+piLOAIHD_ID+"); end;";
  }
  
  public String themmoiHDCQSD(String schema
					,String psMA_HD                         
					,String psMAKH_CU                       
					,String psMAKH_MOI                      
					,String psDIACHI_CT                     
					,String psTEN_TT                        
					,String psDIACHI_TT                     
					,String psSO_GT                           
					,String psQUOCTICH                      
					,String psDIENTHOAI_LH                  
					,String psNGUOI_DD                      
					,String psMS_THUE                       
					,String psTAIKHOAN                      
					,String piCUOC_DV                       
					,String piVAT                           
					,String pdNGAY_TT                       
					,String psGHICHU                        
					,String pdNGAY_LAPHD                    
					,String piTRANGTHAIHD_ID                
					,String piDIADIEMTT_ID                  
					,String piNGANHANG_ID                   
					,String piLOAIKH_ID                     
					,String psNGUOI_CN                      
					,String piQUAN_ID                       
					,String piPHUONG_ID                     
					,String piPHO_ID                        
					,String piLOAGT_ID                        
					,String psCOQUAN                        
					,String psSODAIDIEN                     
					,String psSO_NHA                        
					,String piMA_CQ                         
					,String psMA_BC                         
					,String piKHLON_ID                      
					,String piNGANHNGHE_ID                  
					,String piUUTIEN_ID                     
					,String piDANGKY_TV                     
					,String piDANGKY_DB                     
					,String psCHUCDANH                      
					,String psNGAYCAP_GT                   
					,String psNOICAP_GT                    
					,String pdNGAYSINH
					,String piPHAI                 
					,String psTEN_TT_CU                     
					,String psDIACHI_TT_CU                  
					,String piLOAIHD_ID                     
					,String piQUANTT_ID                     
					,String piPHUONGTT_ID                   
					,String piPHOTT_ID                      
					,String psSOTT_NHA                      

					//,String piDONVIQL_ID                      
					,String piDICHVUVT_ID 
					,String psMA_NV
					,String psEMAIL
					,String pdNgaySN
					
					,String piKHRR
					,String psso_gt1  ,String psngaycap_gt1 ,String psnoicap_gt1  ,int    piLOAIGT_ID1 , int  piHINHTTHUC_CQ , String pi_CK 
  ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.themmoi_hopdong_cqsd1('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"',"                         
				+"'"+psMAKH_CU+"',"                       
				+"'"+psMAKH_MOI+"',"                      
				+"'"+psDIACHI_CT+"',"                     
				+"'"+psTEN_TT+"',"                        
				+"'"+psDIACHI_TT+"',"                     
				+"'"+psSO_GT+"',"                           
				+"'"+psQUOCTICH+"',"                      
				+"'"+psDIENTHOAI_LH+"',"                  
				+"'"+psNGUOI_DD+"',"                      
				+"'"+psMS_THUE+"',"                       
				+"'"+psTAIKHOAN+"',"                      
				+piCUOC_DV+","                            
				+piVAT+","                                
				+pdNGAY_TT+","                            
				+psGHICHU+","                        
				+pdNGAY_LAPHD+","                         
				+piTRANGTHAIHD_ID+","                     
				+piDIADIEMTT_ID+","                       
				+piNGANHANG_ID+","                        
				+piLOAIKH_ID+","                          
				+"'"+psNGUOI_CN+"',"                      
				+"'"+piQUAN_ID+"',"                            
				+"'"+piPHUONG_ID+"',"                          
				+"'"+piPHO_ID+"',"                             
				+piLOAGT_ID+","                        
				+"'"+psCOQUAN+"',"                        
				+"'"+psSODAIDIEN+"',"                     
				+"'"+psSO_NHA+"',"                        
				+piMA_CQ+","                              
				+"'"+psMA_BC+"',"                         
				+piKHLON_ID+","                           
				+piNGANHNGHE_ID+","                       
				+piUUTIEN_ID+","                          
				+piDANGKY_TV+","                          
				+piDANGKY_DB+","                          
				+"'"+psCHUCDANH+"',"                      
				+"'"+psNGAYCAP_GT+"',"                   
				+"'"+psNOICAP_GT+"',"                    
				+pdNGAYSINH+","                
				+piPHAI+","                 
				+"'"+psTEN_TT_CU+"',"                     
				+"'"+psDIACHI_TT_CU+"',"                  
				+piLOAIHD_ID+","                          
				+"'"+piQUANTT_ID+"',"                          
				+"'"+piPHUONGTT_ID+"',"                          
				+"'"+piPHOTT_ID+"',"                          
				+"'"+psSOTT_NHA+"',"                      
				//+piDONVIQL_ID+","
				+piDICHVUVT_ID+","
				+"'"+psMA_NV+"',"                      
				+"'"+psEMAIL+"'" 	
				+",'"+pdNgaySN+"'"		

				+","+piKHRR
				+",'"+psso_gt1+"'"
				+",'"+psngaycap_gt1+"'"
				+",'"+psnoicap_gt1+"'"
				+","+piLOAIGT_ID1	
				+","+piHINHTTHUC_CQ
				+","+pi_CK	
        +");"
        +"end;"
        ;
    return s;

  }
 public String capnhatHDCQSD(String schema
					,String psMA_HD                         
					,String psMAKH_CU                       
					,String psMAKH_MOI                      
					,String psDIACHI_CT                     
					,String psTEN_TT                        
					,String psDIACHI_TT                     
					,String psSO_GT                           
					,String psQUOCTICH                      
					,String psDIENTHOAI_LH                  
					,String psNGUOI_DD                      
					,String psMS_THUE                       
					,String psTAIKHOAN                      
					,String piCUOC_DV                       
					,String piVAT                           
					,String pdNGAY_TT                       
					,String psGHICHU                        
					,String pdNGAY_LAPHD                    
					,String piTRANGTHAIHD_ID                
					,String piDIADIEMTT_ID                  
					,String piNGANHANG_ID                   
					,String piLOAIKH_ID                     
					,String psNGUOI_CN                      
					,String piQUAN_ID                       
					,String piPHUONG_ID                     
					,String piPHO_ID                        
					,String piLOAGT_ID                        
					,String psCOQUAN                        
					,String psSODAIDIEN                     
					,String psSO_NHA                        
					,String piMA_CQ                         
					,String psMA_BC                         
					,String piKHLON_ID                      
					,String piNGANHNGHE_ID                  
					,String piUUTIEN_ID                     
					,String piDANGKY_TV                     
					,String piDANGKY_DB                     
					,String psCHUCDANH                      
					,String psNGAYCAP_GT                   
					,String psNOICAP_GT                    
					,String pdNGAYSINH                
					,String piPHAI                 
					,String psTEN_TT_CU                     
					,String psDIACHI_TT_CU                  
					,String piLOAIHD_ID                     
					,String piQUANTT_ID                     
					,String piPHUONGTT_ID                   
					,String piPHOTT_ID                      
					,String psSOTT_NHA                      

					//,String piDONVIQL_ID                      
					,String piDICHVUVT_ID 
					,String psMA_NV 
					,String ps_Email
					,String pdNgaySN
					
					,String piKH_RR 
					,String psso_gt1  ,String psngaycap_gt1 ,String psnoicap_gt1  ,int    piLOAIGT_ID1 , int  piHINHTTHUC_CQ, String pi_CK     
  ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.capnhat_hopdong_cqsd('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"',"                         
				+"'"+psMAKH_CU+"',"                       
				+"'"+psMAKH_MOI+"',"                      
				+"'"+psDIACHI_CT+"',"                     
				+"'"+psTEN_TT+"',"                        
				+"'"+psDIACHI_TT+"',"                     
				+"'"+psSO_GT+"',"                           
				+"'"+psQUOCTICH+"',"                      
				+"'"+psDIENTHOAI_LH+"',"                  
				+"'"+psNGUOI_DD+"',"                      
				+"'"+psMS_THUE+"',"                       
				+"'"+psTAIKHOAN+"',"                      
				+piCUOC_DV+","                            
				+piVAT+","                                
				+pdNGAY_TT+","                            
				+psGHICHU+","                        
				+pdNGAY_LAPHD+","                         
				+piTRANGTHAIHD_ID+","                     
				+piDIADIEMTT_ID+","                       
				+piNGANHANG_ID+","                        
				+piLOAIKH_ID+","                          
				+"'"+psNGUOI_CN+"',"                      
				+piQUAN_ID+","                            
				+piPHUONG_ID+","                          
				+piPHO_ID+","                             
				+piLOAGT_ID+","                        
				+"'"+psCOQUAN+"',"                        
				+"'"+psSODAIDIEN+"',"                     
				+"'"+psSO_NHA+"',"                        
				+piMA_CQ+","                              
				+"'"+psMA_BC+"',"                         
				+piKHLON_ID+","                           
				+piNGANHNGHE_ID+","                       
				+piUUTIEN_ID+","                          
				+piDANGKY_TV+","                          
				+piDANGKY_DB+","                          
				+"'"+psCHUCDANH+"',"                      
				+"'"+psNGAYCAP_GT+"',"                   
				+"'"+psNOICAP_GT+"',"                    
				+pdNGAYSINH+","                
				+piPHAI+","                 
				+"'"+psTEN_TT_CU+"',"                     
				+"'"+psDIACHI_TT_CU+"',"                  
				+piLOAIHD_ID+","                          
				+piQUANTT_ID+","                          
				+piPHUONGTT_ID+","                        
				+piPHOTT_ID+","                           
				+"'"+psSOTT_NHA+"',"                      

				//+piDONVIQL_ID+","
				+piDICHVUVT_ID+","
				+"'"+psMA_NV+"',"
				+"'"+ps_Email+"'"
				+",'"+pdNgaySN+"'"
				
				+","+piKH_RR 
				+",'"+psso_gt1+"'"
				+",'"+psngaycap_gt1+"'"
				+",'"+psnoicap_gt1+"'"
				+","+piLOAIGT_ID1
				+","+piHINHTTHUC_CQ
				+","+pi_CK
        +");"
        +"end;"
        ;
	System.out.println(s);
    return s;

  }
  public String themmoiTBCQSD(String schema
								,String piCUOC_LD                       
								,String piVAT_LD                        
								,String piQUAN_ID                       
								,String piPHUONG_ID                     
								,String piPHO_ID                        
								,String psDIACHI                        
								,String psSO_NHA                        
								,String piTHUTU_IN                      
								,String psSOMAY_TN                      
								,String piTRACUU_DB                     
								,String piGIAPDANH                      
								,String piCUOCNH_ID                     
								,String piDONVITC_ID                    
								,String piDONVIQL_ID                    
								,String psTEN_DB                        
								,String piDICHUYEN                      
								,String piCUOC_DC                       
								,String piVAT_DC                        
								,String psMA_HD                         
								,String psMA_TB                         
								,String piLOAITB_ID                     
								,String psTENTB_CU                      
								,String psTENTB_MOI                     
								,String piDOITUONG_ID                   
								,String piDANGKY_DB                     
								,String piLOAIDB_ID                     
								,String piINCHITIET          
								
								,String piLOAIDAY_ID    
								,String piSLDAY   
								,String piDONVILD_ID
								,String pdNGAY_PDV 
								,String piMANV_ID 
								
								,String piloaidcbc,String  pikieubc,String pimabaocuoc,String pingaysinh
  									 ) {
    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.themmoi_thuebao_cqsd('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
								+piCUOC_LD+","                            
								+piVAT_LD+","                             
								+"'"+piQUAN_ID+"',"                            
								+"'"+piPHUONG_ID+"',"                          
								+"'"+piPHO_ID+"',"                             
								+"'"+psDIACHI+"',"                        
								+"'"+psSO_NHA+"',"                        
								+piTHUTU_IN+","                           
								+"'"+psSOMAY_TN+"',"                      
								+piTRACUU_DB+","                          
								+piGIAPDANH+","                           
								+piCUOCNH_ID+","                          
								+piDONVITC_ID+","                         
								+piDONVIQL_ID+","                         
								+"'"+psTEN_DB+"',"                        
								+piDICHUYEN+","                           
								+piCUOC_DC+","                            
								+piVAT_DC+","                             
								+"'"+psMA_HD+"',"                         
								+"'"+psMA_TB+"',"                         
								+piLOAITB_ID+","                          
								+"'"+psTENTB_CU+"',"                      
								+"'"+psTENTB_MOI+"',"                     
								+piDOITUONG_ID+","                        
								+piDANGKY_DB+","                          
								+piLOAIDB_ID+","                          
								+piINCHITIET+","
								
								+piLOAIDAY_ID+","    
								+piSLDAY+","   
								+piDONVILD_ID+","
								+pdNGAY_PDV+"," 
								+piMANV_ID 
								+","+piloaidcbc
								+","+pikieubc
								+","+pimabaocuoc
								+","+pingaysinh

	+");"
        +"end;"
        ;
    return s;

  }
public String capnhatTBCQSD(String schema
								,String piCUOC_LD                       
								,String piVAT_LD                        
								,String piQUAN_ID                       
								,String piPHUONG_ID                     
								,String piPHO_ID                        
								,String psDIACHI                        
								,String psSO_NHA                        
								,String piTHUTU_IN                      
								,String psSOMAY_TN                      
								,String piTRACUU_DB                     
								,String piGIAPDANH                      
								,String piCUOCNH_ID                     
								,String piDONVITC_ID                    
								,String piDONVIQL_ID                    
								,String psTEN_DB                        
								,String piDICHUYEN                      
								,String piCUOC_DC                       
								,String piVAT_DC                        
								,String psMA_HD                         
								,String psMA_TB                         
								,String piLOAITB_ID                     
								,String psTENTB_CU                      
								,String psTENTB_MOI                     
								,String piDOITUONG_ID                   
								,String piDANGKY_DB                     
								,String piLOAIDB_ID                     
								,String piINCHITIET           

								,String piLOAIDAY_ID    
								,String piSLDAY   
								,String piDONVILD_ID
								,String pdNGAY_PDV 
								,String piMANV_ID 
								
								,String piloaidcbc,String  pikieubc,String pimabaocuoc,String pingaysinh
								
  									 ) {
    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.capnhat_thuebao_cqsd('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
								+piCUOC_LD+","                            
								+piVAT_LD+","                             
								+"'"+piQUAN_ID+"',"                            
								+"'"+piPHUONG_ID+"',"                          
								+"'"+piPHO_ID+"',"                             
								+"'"+psDIACHI+"',"                        
								+"'"+psSO_NHA+"',"                        
								+piTHUTU_IN+","                           
								+"'"+psSOMAY_TN+"',"                      
								+piTRACUU_DB+","                          
								+piGIAPDANH+","                           
								+piCUOCNH_ID+","                          
								+piDONVITC_ID+","                         
								+piDONVIQL_ID+","                         
								+"'"+psTEN_DB+"',"                        
								+piDICHUYEN+","                           
								+piCUOC_DC+","                            
								+piVAT_DC+","                             
								+"'"+psMA_HD+"',"                         
								+"'"+psMA_TB+"',"                         
								+piLOAITB_ID+","                          
								+"'"+psTENTB_CU+"',"                      
								+"'"+psTENTB_MOI+"',"                     
								+piDOITUONG_ID+","                        
								+piDANGKY_DB+","                          
								+piLOAIDB_ID+","                          
								+piINCHITIET+","

								+piLOAIDAY_ID+","    
								+piSLDAY+","   
								+piDONVILD_ID+","
								+pdNGAY_PDV+"," 
								+piMANV_ID 
								
								+","+piloaidcbc
								+","+pikieubc
								+","+pimabaocuoc
								+","+pingaysinh

	+");"
        +"end;"
        ;
    return s;

  }
public String xoaTBCQSD(String schema
						,String psMA_TB                    
						,String psMA_HD                         
						//,String piDICHVUVT_ID                        
									 ) {
    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.xoa_thuebao_cqsd('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
			+"'"+psMA_TB+"',"                         
			+"'"+psMA_HD+"'"                         
		//	+piDICHVUVT_ID                  
        +");"
        +"end;"
        ;
    return s;

  }
  
  public String themmoiHDDV(String schema
					,String psMA_HD                         
					,String psMA_TB                       
					,String piLOAITB_ID                      
					,String psTEN_TB                     
					,String psDIACHI                        
					,String psDsDichvu                     
					,String psDsCuoc_dk                           
					,String psDsVat_dk                      
					,String psDsNoidung                  
					,String psUSERID                      
					,String pnCUOC_DV                       
					,String pnVAT                      
					,String pdNGAY_LAPHD                       
					,String psGHICHU                        
  ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.themmoi_hopdong_dv('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"',"                         
				+"'"+psMA_TB+"',"                       
				+piLOAITB_ID+","                      
				+"'"+psTEN_TB+"',"                        
				+"'"+psDIACHI+"',"                     
				+"'"+psDsDichvu+"',"                           
				+"'"+psDsCuoc_dk+"',"                      
				+"'"+psDsVat_dk+"',"                  
				+"'"+psDsNoidung+"',"                      
				+pnCUOC_DV+","                       
				+pnVAT+","                      
				+pdNGAY_LAPHD+","                            
				+psGHICHU
        +");"
        +"end;"
        ;
    return s;

  }
  public String xoaHDDV(String schema
						,String psMA_HD                         
						,String psROWID                        
									 ) {
    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.xoa_hopdong_dv('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"                         
			+",'"+psROWID+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String xoaHDLD(String schema
						,String psMA_HD                         
						,String psROWID                        
									 ) {
    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.xoa_hopdong_ld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"                         
			+",'"+psROWID+"'"
        +");"
        +"end;"
        ;
    return s;

  }

  public String capnhatHDDV(String schema
					,String psMA_HD                         
					,String psMA_TB                       
					,String piLOAITB_ID                      
					,String psTEN_TB                     
					,String psDIACHI                        
					,String psDsDichvu                     
					,String psDsCuoc_dk                           
					,String psDsVat_dk                      
					,String psDsNoidung                  
					,String psUSERID                      
					,String pnCUOC_DV                       
					,String pnVAT                      
					,String pdNGAY_LAPHD                       
					,String psGHICHU       
					,String piTRANGTHAIHD_ID                       
					,String pdNGAY_TT       
  ) {

    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.capnhat_hopdong_dv('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"',"                         
				+"'"+psMA_TB+"',"                       
				+piLOAITB_ID+","                      
				+"'"+psTEN_TB+"',"                        
				+"'"+psDIACHI+"',"                     
				+"'"+psDsDichvu+"',"                           
				+"'"+psDsCuoc_dk+"',"                      
				+"'"+psDsVat_dk+"',"                  
				+"'"+psDsNoidung+"',"                      
				+pnCUOC_DV+","                       
				+pnVAT+","                      
				+pdNGAY_LAPHD+","                            
				+psGHICHU +","
				+piTRANGTHAIHD_ID+","                      
				+pdNGAY_TT
				
        +");"
        +"end;"
        ;
    return s;

  }
  
 public String themmoiHDBM(String schema
					,String psTEN_TB                       
					,String psDIACHI                      
					,String psDsChungloai                     
					,String psDsTienMay   					
					,String psDsVat       
					,String psDsSoLuong
					,String psDsChietKhau
					,String psUSERID                           
					,String pdNGAY_LAPHD 
					,String psMa_tb
					,String psSo_sim
					,String psdv_id
		            ,String psMst
					//,String psMhdon
					,String psdichvu
					,String psdauso				
					,String psh_dv
					,String psloai_tb	
  ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.themmoi_hopdong_bm('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")
		+"','"+psTEN_TB+"',"                       
				+"'"+psDIACHI+"',"                     
				+"'"+psDsChungloai+"',"                           
				+"'"+psDsTienMay+"',"                      
				+"'"+psDsVat+"',"   
				+"'"+psDsSoLuong+"'," 
				+"'"+psDsChietKhau+"',"  				
				+pdNGAY_LAPHD
				+",'"+psMa_tb+"'"
				+",'"+psSo_sim+"'"
				+",'"+psdv_id+"'"
				+",'"+psMst+"'"
				//+",'"+psMhdon+"'"			
				+",'"+psh_dv+"'"
				+",'"+psdichvu+"'"
				+",'"+psdauso+"'"
				+",'"+psloai_tb+"'"
        +");"
        +"end;"
        ;
    return s;

  }
/*
  public String themmoiHDBM(String schema
					,String psTEN_TB                       
					,String psDIACHI                      
					,String psDsChungloai                     
					,String psDsTienMay                        
					,String psDsVat                     
					,String psUSERID                           
					,String pdNGAY_LAPHD                      
  ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.themmoi_hopdong_bm('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psTEN_TB+"',"                       
				+"'"+psDIACHI+"',"                     
				+"'"+psDsChungloai+"',"                           
				+"'"+psDsTienMay+"',"                      
				+"'"+psDsVat+"',"                  
				+pdNGAY_LAPHD
        +");"
        +"end;"
        ;
    return s;

  }
*/
  public String capnhatHDBM(String schema
					,String psMA_HD                         
					,String psTEN_TB                       
					,String psDIACHI                      
					,String psDsChungloai                     
					,String psDsTienMay                        
					,String psDsVat                     
					,String psUSERID                           
					,String pdNGAY_LAPHD                      
  ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.capnhat_hopdong_bm('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"',"                         
				+"'"+psTEN_TB+"',"                       
				+"'"+psDIACHI+"',"                     
				+"'"+psDsChungloai+"',"                           
				+"'"+psDsTienMay+"',"                      
				+"'"+psDsVat+"',"                  
				+pdNGAY_LAPHD
        +");"
        +"end;"
        ;
    return s;

  }
  
  public String lay_maTT_HDDC(
					 String schema
					,String psMA_TB                         
					,String psMA_HD                       
					,String psUSERID                   
  ) {
  String s=    "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.lay_maTT_HDDC('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_TB+"',"                         
				+"'"+psMA_HD+"'"                       
		+");"
        +"end;"
        ;
    return s;

  }
  //Start hiepdh
  public String loaibo_dichvu_dadangky(
					 String schema
					,String psMA_TB                         
					,String psDsDichvu                       
  ) {
  String s=    "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_HIEP.loaibo_dichvu_dadangky('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_TB+"',"                         
				+"'"+psDsDichvu+"'"
		+");"
        +"end;"
        ;
    return s;

  }
  //end hiepdh
  public NEOCommonData lay_url_visa(String acc) {
    
	String url_ = "http://user.vnn.vn/cgi-bin/passwd.cgi?option=2&username=" + acc + "&old=ptic123&new=ptic123&confirm=ptic123"
        ;
	String result_ = null;
	try {
		String doc_ = reqPage(url_);
		if (doc_.indexOf("Ten tai khoan khong dung!") ==-1) { 
			result_= "1";
		} else {
			result_= "0";
		}
	} catch (Exception ex) {
		ex.printStackTrace();
	}
	return new NEOCommonData(result_);
  }

   public String layTinhCu(
					 String psFuncSchema
					,String psSchema
					,String psMA_TB
  ) {
  String s=    "begin ? := "+psFuncSchema+"PTTB_LAPHD.layTinhCu("		
		+"'"+psSchema+"','"+psMA_TB+"'"                                  
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;

  }
    /*public String layTTKhuyenMai(
					 String psFuncSchema
					,String psDataSchema
					,String psMA_TB
					,String piKieu_ld
					,String piLoai_tb
  ) {
  String s=    "begin ? := "+psFuncSchema+"PTTB_LAPHD.laytt_khuyenmai("		
		+"'"+psDataSchema+"'"
		+",'"+psMA_TB+"'"                                  
		+"," +piKieu_ld
		+"," +piLoai_tb
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;

  }*/
  public String layTTKhuyenMai(
					 String psFuncSchema
					,String psDataSchema
					,String psMA_TB
					,String piKieu_ld
					,String piLoai_tb
  ) {
   return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/ajax_khuyenmai"
				  +"&"+CesarCode.encode("so_tb")+"="+psMA_TB
				  +"&"+CesarCode.encode("kieu_ld")+"="+piKieu_ld
				  +"&"+CesarCode.encode("loai_tb")+"="+piLoai_tb;
  }
 
     public String laySoSim(
		 String psgFuncSchema 
		,String psDataSchema                                         
		,String psSOMAY                         
		
  	) {
     String s=    "begin ? := "+psgFuncSchema+"PTTB_LAPHD.laytt_sosim("		
		+"'"+psgFuncSchema+"'"
		+",'"+psDataSchema+"'"                                  
		+",'"+psSOMAY+"'"                                  
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
    public String InPhieu_dv() {
    return "/main?"+CesarCode.encode("configFile")+"=vinacore/Baocao_dichvu/inphieu_ycdv";
  }
    public String InPhieu_cm() {
    return "/main?"+CesarCode.encode("configFile")+"=vinacore/Baocao_dichvu/inphieu_yccm";
  }
  public String layds_manv(	     
		 String schema 
		,String psDataSchema   	
		,String psDiadiemId
		,String piQuanId 
		,String piphuongId                                         		                      	
		,String piPhoId
		,String piHinhthuc_ck	
  	) {
     String s=    "begin ? := "+schema+"PTTB_LAPHD.layds_manv_theophuong("		
		+"'"+psDataSchema+"'"		                               
		+","+psDiadiemId
		+","+piQuanId
		+","+piphuongId		
		+","+piPhoId
		+","+piHinhthuc_ck
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
	public String CN_HDBM(String schema
					,String psTEN_TB                       
					,String psDIACHI                                          
					,String psUSERID                           
					,String pdNGAY_LAPHD 
					,String psMa_tb
					,String psSo_sim
					,String psdv_id
		            ,String psMst
					,String psMhdon
					,String psdichvu
					,String psdauso				
					,String psh_dv
					,String psloai_tb
					,String psmahd					
  ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_laphd.capnhat_hdbm('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")
		+"','"+psTEN_TB+"',"                       
				+"'"+psDIACHI+"',"                                      
				+pdNGAY_LAPHD
				+",'"+psMa_tb+"'"
				+",'"+psSo_sim+"'"
				+",'"+psdv_id+"'"
				+",'"+psMst+"'"
				+",'"+psMhdon+"'"			
				+",'"+psh_dv+"'"
				+",'"+psdichvu+"'"
				+",'"+psdauso+"'"
				+",'"+psloai_tb+"'"
				+",'"+psmahd+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  	public  String bc_thu_hni(String phuongid)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/bc_thu_hn"
   				+"&"+CesarCode.encode("phuongid") + "=" + phuongid;
   			}
	public  String bc_thu_t()
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/bc_thu_s";
   			}
	public String layTTKhuyenMai_hni(
					 String psFuncSchema
					,String psDataSchema
					,String psMA_TB
					,String piKieu_ld
					,String piLoai_tb
  ) {
   String agentCode = getUserVar("sys_agentcode");
  
   //if (agentCode.equals("HCM") || agentCode.equals("DNI") || agentCode.equals("QNH") || agentCode.equals("TNH")|| agentCode.equals("HNI")) {		
		return  "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/ajax_khuyenmai"
				  +"&"+CesarCode.encode("so_tb")+"="+psMA_TB
				  +"&"+CesarCode.encode("agentcode")+"="+agentCode
				  +"&"+CesarCode.encode("kieu_ld")+"="+piKieu_ld
				  +"&"+CesarCode.encode("loai_tb")+"="+piLoai_tb;  
			
   // }
   /*
   return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/ajax_khuyenmai_s"
				  +"&"+CesarCode.encode("so_tb")+"="+psMA_TB
				  +"&"+CesarCode.encode("kieu_ld")+"="+piKieu_ld
				  +"&"+CesarCode.encode("loai_tb")+"="+piLoai_tb;*/
  }		
	public String layds_loaitb_id(String userInput) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/ajax_loaitb"
			+"&"+CesarCode.encode("psma_tb")+"="+userInput
        ;
  }  
  

	public String layds_kieuld(String userInput) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld_vsms/ajax_kieuld"
			+"&"+CesarCode.encode("psma_tb")+"="+userInput
        ;
  }    
    public String laytt_loaitb(
		 String psgFuncSchema 
		,String psDataSchema                                         
		,String psSOMAY                         
		
  	) {
     String s=    "begin ? := "+psgFuncSchema+"PTTB_LAPHD.laytt_loaitb("		
		+"'"+psgFuncSchema+"'"
		+",'"+psDataSchema+"'"                                  
		+",'"+psSOMAY+"'"                                  
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
	
	public String laytt_tienkm_hm(
		 String psgFuncSchema 
		,String psDataSchema                                         
		,String pskhuyenmai_id
		,String pstien
		,String pskieu_ld
		
  	) {
     String s=    "begin ? := admin_v2.pttb_tracuu.laytt_tienkm("		
		+"'"+psDataSchema+"'"                            
		+",'"+pskhuyenmai_id+"'"    
		+",'"+pstien+"'"
		+",'"+pskieu_ld+"'"
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
	public String laytt_txtIpad(
		 String psgFuncSchema 
		,String psDataSchema                                         
		,String pskhuyenmai_id
		
  	) {
     String s=    "begin ? := "+psgFuncSchema+"PTTB_PROMOTION_CONFIG.laytt_ipad_km("		
		+"'"+psDataSchema+"'"                            
		+",'"+pskhuyenmai_id+"'"    
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
	public String lay_tien_ipad(
		 String psgFuncSchema 
		,String psDataSchema                                         
		
  	) {
     String s=    "begin ? := "+psgFuncSchema+"PTTB_KM.laytt_tienhm_ipad("		
		+"'"+psDataSchema+"'"                            
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}
	
	/* bo sung lay loai cho hop dong thanh ly 
	 update: 07/12/2010 - natuan
	*/
	public String laytt_loaitb_tl(String userInput) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/HDTL/ajax_loaitb"
			+"&"+CesarCode.encode("psma_tb")+"="+userInput
        ;
  }  
  
  public String laytt_cuoc_chonso(String schema,String ma_tb) {	
	String s ="begin ?:="+CesarCode.decode(schema)+"pttb_temp.laytt_tien_chonso('"+getUserVar("sys_dataschema")		
		+"','"+ma_tb+"'); end;";
    return s;
  }

  public String themmoiTBLD(String schema ,String psNGUOI_CN ,String piDONVIQL_ID ,String psSO_NHA ,String piTHUTU_IN                      
						,String psSOMAY_TN ,String psMA_TB ,String psMAY_CN ,String psMA_BC ,String piDONVITC_ID                    
						,String piTRACUU_DB ,String psGIAPDANH                      
						,String piCUOCNH_ID                     
						,String piKHUYENMAI                     
						,String piDANGKY_DB                     
						,String psTEN_DB                        
						,String piTRAMVT_ID                     
						,String piVISA_ID                       
						,String piTHUEBAO_ID                    
						,String psMA_HD                         
						,String psTEN_TB                        
						,String psDIACHI                        
						,String piLOAITB_ID                     
						,String psDOITUONG_ID                   
						,String piDANGKY_DV                       
						,String piTRAGOP                          
						,String piCUOC_LD                       
						,String piCUOC_DV                       
						,String piTRANGTHAI                     
						,String piVAT_LD                        
						,String piVAT_DV                        
						,String piLOAIDB_ID                     
						,String piINCHITIET                     
						,String piCHUNGLOAI_ID                  
						,String piTIENMAY                       
						,String piVAT_MAY                       
						,String psGHICHU                        
						,String piQUAN_ID                       
						,String piPHUONG_ID                     
						,String piPHO_ID                        
						,String piKIEULD_ID   
						,String psDANHSACH_DV
						,String psDANHSACH_DVNOIDUNG						
						//,String piDICHVUVT_ID                        
						,String piLOAIKH_ID   
						
						,String piLOAIDAY_ID    
						,String piSLDAY   
						,String piDONVILD_ID
						,String pdNGAY_PDV 
						,String piMANV_ID
						,String piKHUYENMAI_ID
						,String piTIEN_KM
						
						,String piloaidcbc,String  pikieubc,String pimabaocuoc
						,String pingaysinh
						
						,String pshidDSLK
						,String pshidDSTenLK
						,String pshidDSSLLK
						,String psTBNhanTK
						,String psThangcamket
						,String piKH_DoanhNghiep 
						,String psNhomMBVNN
						,String piSerial
						,String piImei
						,String piGoi
						,String piBillLimit
						,String piMonthLive	
						,String piEmail							
						) {
	String s=    "begin ? := admin_v2.pttb_hmm.themmoi_thuebao_ld('"+getUserVar("userID")	  
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psNGUOI_CN+"',"                      
			+piDONVIQL_ID+","                         
			+"'"+psSO_NHA+"',"                        
			+piTHUTU_IN+","                           
			+"'"+psSOMAY_TN+"',"                      
			+"'"+psMA_TB+"',"                         
			+"'"+psMAY_CN+"',"                        
			+"'"+psMA_BC+"',"                         
			+piDONVITC_ID+","                         
			+piTRACUU_DB+","                          
			+"'"+psGIAPDANH+"',"                      
			+piCUOCNH_ID+","                          
			+piKHUYENMAI+","   
			+piDANGKY_DB+","                          
			+"'"+psTEN_DB+"',"                        
			+piTRAMVT_ID+","                          
			+piVISA_ID+","                            
			+piTHUEBAO_ID+","                         
			+"'"+psMA_HD+"',"                         
			+"'"+psTEN_TB+"',"                        
			+"'"+psDIACHI+"',"                        
			+piLOAITB_ID+","                          
			+"'"+psDOITUONG_ID+"',"                   
			+piDANGKY_DV+","     
			
			+piTRAGOP+","                               
			+piCUOC_LD+","                            
			+piCUOC_DV+","                            
			+piTRANGTHAI+","                          
			+piVAT_LD+","                             
			+piVAT_DV+","                             
			+piLOAIDB_ID+","                          
			+piINCHITIET+","                          
			+piCHUNGLOAI_ID+","                       
			+piTIENMAY+","                            
			+piVAT_MAY+","                            
			+psGHICHU+","                        
			+piQUAN_ID+","                            
			+piPHUONG_ID+","                          
			+piPHO_ID+","                             
			+piKIEULD_ID+","     
			+"'"+psDANHSACH_DV+"',"      
			+"'"+psDANHSACH_DVNOIDUNG+"',"			
			//+piDICHVUVT_ID+","                             
			+piLOAIKH_ID+"," 

			+piLOAIDAY_ID+","    
			+piSLDAY+","   
			+piDONVILD_ID+","
			+pdNGAY_PDV+"," 
			+piMANV_ID+ ","
			+"'"+piKHUYENMAI_ID + "',"
			+piTIEN_KM
			+","+piloaidcbc
			+","+pikieubc
			+","+pimabaocuoc
			+","+pingaysinh
						
			+",'"+pshidDSLK + "'"
			+",'"+pshidDSTenLK + "'"
			+",'"+pshidDSSLLK + "'"
			+",'"+psTBNhanTK+ "'"
			+",'"+psThangcamket+ "'"	
			+",'"+piKH_DoanhNghiep+ "'"			
			+",'"+psNhomMBVNN+ "'"			
			+",'"+piSerial+ "'"			
			+",'"+piImei+ "'"	
			+",'"+piGoi+"'"
			+",'"+piBillLimit+"'"
			+",'"+piMonthLive+"'"
			+",'"+piEmail+"'"
        +");"
        +"end;";
    return s;

  }
  
  public String capnhatTBLD(String schema
						,String psNGUOI_CN                      
						,String piDONVIQL_ID                    
						,String psSO_NHA                        
						,String piTHUTU_IN                      
						,String psSOMAY_TN                      
						,String psMA_TB                         
						,String psMAY_CN                        
						,String psMA_BC                         
						,String piDONVITC_ID                    
						,String piTRACUU_DB                     
						,String psGIAPDANH                      
						,String piCUOCNH_ID                     
						,String piKHUYENMAI                     
						,String piDANGKY_DB                     
						,String psTEN_DB                        
						,String piTRAMVT_ID                     
						,String piVISA_ID                       
						,String piTHUEBAO_ID                    
						,String psMA_HD                         
						,String psTEN_TB                        
						,String psDIACHI                        
						,String piLOAITB_ID                     
						,String psDOITUONG_ID                   
						,String piDANGKY_DV                       
						,String piTRAGOP                          
						,String piCUOC_LD                       
						,String piCUOC_DV                       
						,String piTRANGTHAI                     
						,String piVAT_LD                        
						,String piVAT_DV                        
						,String piLOAIDB_ID                     
						,String piINCHITIET                     
						,String piCHUNGLOAI_ID                  
						,String piTIENMAY                       
						,String piVAT_MAY                       
						,String psGHICHU                        
						,String piQUAN_ID                       
						,String piPHUONG_ID                     
						,String piPHO_ID                        
						,String piKIEULD_ID   
						,String psDANHSACH_DV                     
						//,String piDICHVUVT_ID                        
						,String piLOAIKH_ID   
						,String psMA_TB_CU   
						
						,String piLOAIDAY_ID    
						,String piSLDAY   
						,String piDONVILD_ID
						,String pdNGAY_PDV 
						,String piMANV_ID
						, String piKHUYENMAI_ID
						, String piTIEN_KM
						,String piloaidcbc,String  pikieubc,String pimabaocuoc
						
						,String pshidDSLK
						,String pshidDSTenLK
						,String pshidDSSLLK
						,String psTBNhanTK 
						,String psThangcamket
						,String piKH_DoanhNghiep
						,String pinhommb_vnn
						,String piSerial
						,String piImei
						,String piGoi
						,String piBillLimit
						,String piMonthLive
						,String piEmail
									 ) {
    String s=    "begin ? := admin_v2.pttb_tracuu.capnhat_thuebao_ld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psNGUOI_CN+"',"                      
			+piDONVIQL_ID+","                         
			+"'"+psSO_NHA+"',"                        
			+piTHUTU_IN+","                           
			+"'"+psSOMAY_TN+"',"                      
			+"'"+psMA_TB+"',"                         
			+"'"+psMAY_CN+"',"                        
			+"'"+psMA_BC+"',"                         
			+piDONVITC_ID+","                         
			+piTRACUU_DB+","                          
			+"'"+psGIAPDANH+"',"                      
			+piCUOCNH_ID+","                          
			+piKHUYENMAI+","                          
			+piDANGKY_DB+","                          
			+"'"+psTEN_DB+"',"                        
			+piTRAMVT_ID+","                          
			+piVISA_ID+","                            
			+piTHUEBAO_ID+","                         
			+"'"+psMA_HD+"',"                         
			+"'"+psTEN_TB+"',"                        
			+"'"+psDIACHI+"',"                        
			+piLOAITB_ID+","                          
			+"'"+psDOITUONG_ID+"',"                   
			+piDANGKY_DV+","                            
			+piTRAGOP+","                               
			+piCUOC_LD+","                            
			+piCUOC_DV+","                            
			+piTRANGTHAI+","                          
			+piVAT_LD+","                             
			+piVAT_DV+","                             
			+piLOAIDB_ID+","                          
			+piINCHITIET+","                          
			+piCHUNGLOAI_ID+","                       
			+piTIENMAY+","                            
			+piVAT_MAY+","                            
			+psGHICHU+","                        
			+piQUAN_ID+","                            
			+piPHUONG_ID+","                          
			+piPHO_ID+","                             
			+piKIEULD_ID+","     
			+"'"+psDANHSACH_DV+"',"               
			//+piDICHVUVT_ID+","                             
			+piLOAIKH_ID+","                             
			+"'"+psMA_TB_CU+"',"     

			+piLOAIDAY_ID+","    
			+piSLDAY+","   
			+piDONVILD_ID+","
			+pdNGAY_PDV+","
			+ piMANV_ID + "," 
			+ piKHUYENMAI_ID + ","
			+ piTIEN_KM
			+","+piloaidcbc
			+","+pikieubc
			+","+pimabaocuoc
			
			+",'"+pshidDSLK + "'"
			+",'"+pshidDSTenLK + "'"
			+",'"+pshidDSSLLK + "'"
			+",'"+psTBNhanTK+ "'"
			+",'"+psThangcamket+ "'"			
			+",'"+piKH_DoanhNghiep+ "'"
			+",'"+pinhommb_vnn+ "'"			
			+",'"+piSerial+ "'"			
			+",'"+piImei+ "'"	
			+",'"+piGoi+"'"
			+",'"+piBillLimit+"'"
			+",'"+piMonthLive+"'"
			+",'"+piEmail+"'"
        +");"
        +"end;"
        ;
		System.out.println(s);
	
    return s;

  }
  
}
