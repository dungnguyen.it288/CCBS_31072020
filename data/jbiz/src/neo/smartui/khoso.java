package neo.smartui;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import java.util.*;
import javax.sql.*;
import javax.sql.*;
import java.util.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.io.*;
import java.util.*;
import java.util.regex.*;

import neo.smartui.common.CesarCode;

public class khoso extends NEOProcessEx 
{	
	public String capnhat_kho(		
		String id,
		String scope,
		String rule_name,
		String can_view,
		String can_grant,
		String can_active,
		String active_types,
		String status,
		String userip,
		String agentcode) throws Exception {
			String s ="begin ? :=neo.kho_sim.capnhat_role("
				+"'"+id+"',"
				+"'"+scope+"',"
				+"'"+rule_name+"',"
				+"'"+can_view+"',"
				+"'"+can_grant+"',"
				+"'"+can_active+"',"
				+"'"+active_types+"',"
				+"'"+status+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+userip+"',"				
				+"'"+agentcode+"'"
				+"); end;";
		return this.reqValue("CMDVOracleDS", s);
	}
	public String xoa_kho(		
		String id,String userip, String agentcode) throws Exception {
			String s ="begin ? :=neo.kho_sim.xoa_role("
				+"'"+id+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+userip+"',"
				+"'"+agentcode+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("CMDVOracleDS", s);
	}	
	public String chuyen_kho(		
		String tbs, String khoid,String userip, String agentcode) throws Exception {
			String s ="begin ? :=neo.kho_sim.chuyen_kho("
				+"'"+tbs+"',"
				+"'"+khoid+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+userip+"',"
				+"'"+agentcode+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("CMDVOracleDS", s);
	}
	public String phanquyen_kho(
		String users, String khos,String userip, String agentcode
	) throws Exception {
			String s ="begin ? :=neo.kho_sim.phanquyen_kho("
				+"'"+users+"',"
				+"'"+khos+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+userip+"',"
				+"'"+agentcode+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("CMDVOracleDS", s);
	}
}
	