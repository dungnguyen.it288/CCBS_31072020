package neo.smartui;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import java.util.*;
import javax.sql.*;
import javax.sql.*;
import java.util.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.io.*;
import java.util.*;
import java.util.regex.*;

import neo.smartui.common.CesarCode;

public class doanvien extends NEOProcessEx 
{	
	public String dangky_mxt(String sotb,String userid) throws Exception {
		String s = "begin ?:=prepaid.pps.dangky_xacthuc('"+        	
			userid+"','"+
			"1414"+"','"+
			"MXT "+sotb+"'); end;";
		System.out.println(s);
		return this.reqValue("CMDVOracleDS", s);
	}
	
	public String dangky(		
		String so_tb,
		String loaikh,
		String ten_kh,
		String nghenghiep,
		String ngaysinh,
		
		String congty,
		String loaigt,
		String diachicty,
		String sochungminh,
		String gioitinh,
		
		String nuoc,
		String ngaycap,
		String noicap,
		String email,
		String diachi,
		
		String sub_type,
		String unit_id,
		String ref_type,
		String ref_number,
		String mxt,
		
		String note,
		String userip,
		String agentcode) throws Exception {
			String s ="begin ? :=neo.goicuoc.youth_union_register("
				+"'"+so_tb+"',"
				+"'"+loaikh+"',"
				+"'"+ConvertFont.UtoD(ten_kh)+"',"
				+"'"+nghenghiep+"',"
				+"'"+ngaysinh+"',"
				
				+"'"+ConvertFont.UtoD(congty)+"',"
				+"'"+loaigt+"',"
				+"'"+ConvertFont.UtoD(diachicty)+"',"
				+"'"+sochungminh+"',"
				+"'"+gioitinh+"',"
				
				+"'"+nuoc+"',"
				+"'"+ngaycap+"',"
				+"'"+noicap+"',"
				+"'"+email+"',"
				+"'"+ConvertFont.UtoD(diachi)+"',"
				
				+"'"+sub_type+"',"
				+"'"+unit_id+"',"
				+"'"+ref_type+"',"
				+"'"+ref_number+"',"
				+"'"+mxt+"',"
				
				+"'"+note+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+userip+"',"				
				+"'"+agentcode+"'"
				+"); end;";
		return this.reqValue("CMDVOracleDS", s);
	}
	
	public String huy(		
		String sotb,String userip, String agentcode) throws Exception {
			String s ="begin ? :=neo.goicuoc.youth_union_unregister("
				+"'"+sotb+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+userip+"',"
				+"'"+agentcode+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("CMDVOracleDS", s);
	}
	
	public String capnhat_donvi(		
		String id,		
		String cap,
		String name,
		String tinh,
		String quan,
		String phuong,
		String pho,
		String so_nha,
		String note,
		String userip) throws Exception {
			String s ="begin ? :=neo.goicuoc.youth_union_update_unit("
				+"'"+id+"',"				
				+"'"+cap+"',"
				+"'"+ConvertFont.UtoD(name)+"',"
				+"'"+tinh+"',"
				+"'"+quan+"',"
				+"'"+phuong+"',"
				+"'"+pho+"',"
				+"'"+so_nha+"',"
				+"'"+note+"',"
				+"'"+getUserVar("userID")+"',"
				+"'"+userip+"'"				
				+"); end;";
		return this.reqValue("CMDVOracleDS", s);
	}
	
	public String get_hethan(String ngaysinh) throws Exception {
		String s="declare a date:=to_date('"+ngaysinh+"','dd/mm/yyyy'); b varchar2(1000);"+
				"begin "+
				"	if to_char(a,'yyyy')+42 < to_char(sysdate,'yyyy') then "+
				"		b:='Ng�y sinh kh�ng h&#7907;p l&#7879;'; "+
				"	end if;"+
				"	b := 'Ng�y h&#7871;t h&#7841;n g�i c&#432;&#7899;c l�: 31/12/'|| (to_char(a,'yyyy')+42); "+
				"	?:=b;end;";				
		return this.reqValue("CMDVOracleDS", s);
	}
}
	