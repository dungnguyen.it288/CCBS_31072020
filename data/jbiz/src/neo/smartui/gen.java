package neo.smartui;

import neo.smartui.process.*;

import java.util.*;
import javax.sql.*;
import javax.sql.RowSet.*;
import javax.sql.RowSetInternal.*;
import javax.sql.rowset.JdbcRowSet;  
import java.lang.Object.*;
import javax.sql.ResultSetMetaData.*;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.io.*;
//import neo.smartui.common.ConvertFont;


import neo.smartui.common.CesarCode;
//Code Gen file htmt, sql va file jbiz
//TrinhHV Update 26/09/2011
public class gen extends NEOProcessEx 
{		
    public static String getDateTime(String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        Date date = new Date();
		
        return dateFormat.format(date);
    }
	
	public void writerFile(String file, String content) throws IOException
	{
		File f = new File(file);
		if (f.exists()){			
			f.renameTo(new File(f.getParent()+"/bak/"+ f.getName()+"."+getDateTime("yyyy-mm-dd.HH.mm.ss")));
		}
		f = new File(file);
  		BufferedWriter output = new BufferedWriter(new FileWriter(f));
		output.write(content);
   		output.flush();
   		output.close();	    
  	}
	public String getPrimaryKey(String psOwner,String psTable) throws Exception
	{
		String s ="begin ? := "+getSysConst("FuncSchema")+"gen_code.get_primary_key('"+psOwner+"','"+psTable+"'); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	// tao form
	public String autogenReport(String content, String root_dir, String report_title)
	 {
	
		String ngaygio=getDateTime("yyyy.mm.dd.HH.mm.ss");
		String report_name="auto_"+ngaygio;
		String report_index ="auto_index_"+ngaygio;
	    int colcount=0;
	    String str_inform="";
		String inform_detail="";
		String script="";
		String sub_script="";
		String arr_content[]=content.split("@");
		colcount = arr_content.length;
		
	     System.out.println(content);	
		int j=0;
		int jj=1;   
		inform_detail+="<tr>";
		script+="<script>$(document).ready(function(){";
	 try 
	    {
	     do 
		      {
			    String arr_[]= arr_content[j].split(",");
								
				if((jj%3)==0)
				   {
				    
					inform_detail+="<tr>";
				   }
				  	 System.out.println("cac bien:"+arr_.length);
				inform_detail+="<td width=\"20%\" class=label>"+arr_[0]+"</td><td width=30% class=label1>";
			
				String type = arr_[1];
				 System.out.println("cac bien:");
				
				if(type.equals("textarea"))
				            {
							 inform_detail+="<textarea id="+arr_[2]+" style=\"width:90%\"></textarea>";
							 sub_script+="+\"&{encode:"+arr_[2]+"}=\"+$(\"#"+arr_[2]+"\").val()";
							}
				if(type.equals("select"))
							{
							
							inform_detail+="";
							   if(arr_[4].equals("sql"))
				     		     {
								    inform_detail+="<select id="+arr_[2]+" name=\"--"+arr_[2]+"\" style=\"width:90%\" sql=";
									sub_script+="+\"&{encode:"+arr_[2]+"}=\"+$(\"#"+arr_[2]+"\").val()";
									int k=5;
									
									do 
									 { 
										
										if(k!=arr_.length-1)
										{
											inform_detail+=arr_[k]+",";
										}
										else 
										{  
										   inform_detail+=arr_[k];
										}
										k++;
									 }									 
									 while(k<arr_.length);
									 inform_detail+=" emptyOption=\"\"></select>";
								 }
								else 
								  {
									inform_detail+="<select id="+arr_[2].replace("'","")+"  name=\"--"+arr_[2].replace("'","")+
									" style=\"width:90%\" class ="+arr_[3]+"\">\n";
									sub_script+="+\"&{encode:"+arr_[2]+"}=\"+$(\"#"+arr_[2]+"\").val()";
									int l=5;
									
									do 
									 { 
										
										inform_detail+="<option value="+arr_[l+1].replace("'","")+"> "+arr_[l].replace("'","")+"</option>";
										
										l=l+2;
									 }									 
									 while(l<arr_.length);
									 inform_detail+="</select>";
								 }
							}
				if(type.equals("input"))
				   {
				    
				        
						if(arr_[3].equals("text"))
							{
					           inform_detail+="<input type=text style=\"width:90%\" id="+arr_[2]+" class="+arr_[4]+">"; 	
										sub_script+="+\"&{encode:"+arr_[2]+"}=\"+$(\"#"+arr_[2]+"\").val()";							   
								if(arr_[4].equals("cdate"))
								   {
								     script+="$(\"#"+arr_[2]+"\").datepicker({dateFormat:\"dd/mm/yy\"});\n";
								   }
								if(arr_[4].equals("numbersOnly"))
								   {
								     script+="$(\"."+arr_[4]+"\").keydown(function(event){\n"+
									           "if(event.shiftKey) event.preventDefault();\n"+
											   " if (event.keyCode == 46 || event.keyCode == 8) {\n"+
													"}\n"+
	   											"else {\n"+
	        											"if (event.keyCode < 95) {\n"+
														  "  if (event.keyCode < 48 || event.keyCode > 57) {\n"+
															"event.preventDefault();\n"+
															  "}\n"+
															"}\n"+
														"else {\n"+
																"if (event.keyCode < 96 || event.keyCode > 105) {\n"+
																	"event.preventDefault();\n"+
																	" }\n"+
															"}\n"+
												"}})\n;";
								   }
							}
						if(arr_[3].equals("checkbox"))
						    {
							     inform_detail+="<input type=checkbox  id="+arr_[2]+" name="+arr_[4]+">";
								sub_script+="+\"&{encode:"+arr_[2]+"}=\"+$(\"#"+arr_[2]+"\").val()";
							}
						if(arr_[3].equals("radio"))
						    {
							     inform_detail+="<input type=radio  id="+arr_[2]+" name="+arr_[4]+">";
								 sub_script+="+\"&{encode:"+arr_[2]+"}=\"+$(\"#"+arr_[2]+"\").val()";
							}
						
				   }
				   
				   if((jj%2)==0)
				   {
				   
				   inform_detail+="</td></tr>";
				   }
				   if(jj==colcount)
				   {
						if((jj%2)!=0)
						{
						   inform_detail+="</td><td class=label>&nbsp;</td><td class=label>&nbsp;</td></tr>";
						}
				   }
				j++;
				jj++;
			  }			  
			while(j<colcount);
		
				
			script+=" $(\"#btnBaoCao\").click(function(){\n"+
			        "var type_export =0;\n"+
						"if($(\"#radhtm\").attr(\"checked\"))\n"+
						"{ type_export=3;}\n"+
						"if($(\"#radpdf\").attr(\"checked\"))\n"+
						"{ type_export=1;}\n"+
						"if($(\"#radxls\").attr(\"checked\"))\n"+
						"{ type_export=2;}\n"+
						 "s_='main?{encode:+configFile}={encode:report/design/report_file/"+report_name+".jrxml}'\n"+
						 sub_script+"+\"&{encode:jrReportType}=\"+type_export"+";\n"+
						 "window.open(s_,'mywindow');\n"+
						 "});\n"+
						 "$(\"#btnketthuc\").click(function(){\n"+
						 " window.close();"+
						 +"});\n"+
			        "}); </script>";
	   str_inform="{includep:layout/ccbs/style_migrate}"+
			"<HEAD>\n"+
				"<TITLE>"+report_title+"</TITLE>\n"+
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">	\n"+
				"<meta sql=\"select to_char(sysdate,'dd/mm/yyyy') ngay1, to_char(sysdate,'dd/mm/yyyy') ngay2 from dual\" rec-per-page=\"100\"\n"+
				"name=\"DATE\" dbms-font=\"VN8VN3\" run=\"1\" >\n"+
			"<style>\n"+
			".label1 {\n"+
				"color: #000;\n"+
				"text-align: left;\n"+
				"background-color: #f0f0f0;\n"+
				"padding: 4;\n"+
			"}\n"+
			".label2 {\n"+
			"	color: #000;\n"+
			"	text-align: center;\n"+
			"	background-color: #f0f0f0;\n"+
			 "  	padding: 4;\n"+
			"}\n"+
			"</style>\n"+
			"</HEAD>\n"+
			"<BODY>\n"+
			"<table width=100% class=\"secContent\"><tr><td class=secHead>\n"+
				report_title+
			"</td></tr>\n"+
			"<tr><td align=center>\n"+
			 "      <table width=100% align=center class=\"secContent\" cellpadding=0 cellspacing=1>     \n"+
			 inform_detail+
				   " <tr>    \n"+
							"  <td align=center class=label2 colspan=4> \n"+
							"  <input type=radio name=\"xuatdulieu\" id=\"radhtm\">HTML\n"+
							"  <input type=radio name=\"xuatdulieu\" id=\"radpdf\">PDF\n"+
							"  <input type=radio name=\"xuatdulieu\" id=\"radxls\" >Exel\n"+            
							"  <input type=button value=\"B�o c�o\" class=\"button\" id=\"btnBaoCao\">&nbsp;&nbsp;\n"+
							"  <input type=button value=\"K&#7871;t th�c\" class=button id=\"btnketthuc\">\n"+
							"  </td>\n"+

						"  </tr>\n"+
						"    </table>\n"+
				" </td></tr>\n"+
			"</table>\n"  +
			"</BODY>\n"+
			script+
			"</HTML>\n";
			
				
			writerFile(this.getSysConst("ConfigDir")+root_dir+"/"+report_index+".htm",str_inform);	
		
			System.out.println("Tao file thanh cong");
		}
		catch(Exception ex){
		
		 
			return ex.getMessage();
		}
       return report_name;		
	 }
	 
/*	public String autogenReport_bk(String psfunction, String columns, String root_dir, String report_title,String title)
	 {
	
	  String ngaygio=getDateTime("yyyy.mm.dd.HH.mm.ss");
	   String report_name="auto_"+ngaygio;
	   String report_index ="auto_index_"+ngaygio;
	    int colcount=0;
	 try 
	 {
	   
	    String ajaxFunc = psfunction.replace(",","}','{p0").replace("(","('{p0").replace(")","}')").replace(" ","");
		
		String cols[]=columns.split(",");
		int count_col = cols.length;
		int j =0;
		 
		
		 String str_inform ="";
		 
		 int position1 = psfunction.indexOf("(");
		 int position2= psfunction.indexOf(")");
		 String inform =psfunction.substring(position1+1,position2);	
			inform=inform.replace(" ","");				
		String inform_title = this.reqValue("","begin ?:=admin_v2.pkg_report_auto.get_name_single_column('"+inform+"'); end;");		
		 String informs[]=inform_title.split(",");
		int count_inform=informs.length;
	     String str_script="";
      int j1=0;
		String inform_detail="";
		do 
		  {			
		   
			
			    if((j1%2)==0)
				{inform_detail+="<tr>\n";
			      inform_detail+= "<td class = label width=50%>"+informs[j1]+"</td>\n";
				  
				 }
				 else 
				 {
				  inform_detail+= "<td class =label1><input type=text id="+informs[j1]+"></td>\n";
				  inform_detail+= "</tr>\n";	
				  str_script+="'&{encode:"+informs[j1]+"}='+$(\"#"+informs[j1]+"\").val()+\n";
				  }
				  		
			
		         j1++;
		  }
		 while(j1<count_inform);
		 str_script+="'&{encode:jrReportType}='+type_export;\n";
		 //System.out.println(inform_detail);
		 //tao script
		 String script="<script>\n"+
		 "$(document).ready(function(){\n"+
				" $(\"#btnBaoCao\").click(function(){\n"+
				        "var type_export =0;\n"+
						"if($(\"#radhtm\").attr(\"checked\"))\n"+
						"{ type_export=3;}\n"+
						"if($(\"#radpdf\").attr(\"checked\"))\n"+
						"{ type_export=1;}\n"+
						"if($(\"#radxls\").attr(\"checked\"))\n"+
						"{ type_export=2;}\n"+
		                "s_='main?{encode:+configFile}={encode:report/design/report_file/"+report_name+".jrxml}'+\n"+
						 str_script+
						"window.open(s_,'mywindow');\n"+
				"});\n"+
		 "});\n"+
		 "</script>";
		 
			str_inform="{includep:layout/ccbs/style_migrate}"+
			"<HEAD>\n"+
				"<TITLE>"+report_title+"</TITLE>\n"+
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">	\n"+
				"<meta sql=\"select to_char(sysdate,'dd/mm/yyyy') ngay1, to_char(sysdate,'dd/mm/yyyy') ngay2 from dual\" rec-per-page=\"100\"\n"+
				"name=\"DATE\" dbms-font=\"VN8VN3\" run=\"1\" >\n"+
			"<style>\n"+
			".label1 {\n"+
				"color: #000;\n"+
				"text-align: left;\n"+
				"background-color: #f0f0f0;\n"+
				"padding: 4;\n"+
			"}\n"+
			".label2 {\n"+
			"	color: #000;\n"+
			"	text-align: center;\n"+
			"	background-color: #f0f0f0;\n"+
			 "  	padding: 4;\n"+
			"}\n"+
			"</style>\n"+
			"</HEAD>\n"+
			"<BODY>\n"+
			"<table width=100% class=\"secContent\"><tr><td class=secHead>\n"+
				report_title+
			"</td></tr>\n"+
			"<tr><td align=center>\n"+
			 "      <table width=100% align=center class=\"secContent\" cellpadding=0 cellspacing=1>     \n"+
			 inform_detail+
				   " <tr>    \n"+
							"  <td align=center class=label2 colspan=2> \n"+
							"  <input type=radio name=\"xuatdulieu\" id=\"radhtm\">HTML\n"+
							"  <input type=radio name=\"xuatdulieu\" id=\"radpdf\">PDF\n"+
							"  <input type=radio name=\"xuatdulieu\" id=\"radxls\" >Exel\n"+            
							"  <input type=button value=\"B�o c�o\" class=\"button\" id=\"btnBaoCao\">&nbsp;&nbsp;\n"+
							"  <input type=button value=\"K&#7871;t th�c\" class=button id=\"btnketthuc\">\n"+
							"  </td>\n"+

						"  </tr>\n"+
						"    </table>\n"+
				" </td></tr>\n"+
			"</table>\n"  +
			"</BODY>\n"+
			script+
			"</HTML>\n";
		
		
	
		// tao file jasper
	System.out.println("decode_1252");
	System.out.println(W1252toUTF8(report_title));
	 genReportJrxml( ajaxFunc,columns, report_title, report_name, root_dir);
		writerFile(this.getSysConst("ConfigDir")+root_dir+"/"+report_index+".htm",str_inform);	
		
		System.out.println("Tao file thanh cong");
		}
		catch(Exception ex){
		//System.out.println("du lieu file"+cmd);
		 
			return ex.getMessage();
		}
       return root_dir+"/"+report_name;		
	 }
	 */
	 // tao file jrxml
	 public String genReportJrxml(String ajaxFunc,String columns, String report_title, String report_name, String root_dir, String id)
	 {
	 String psfunction = ajaxFunc.replace(",","}','{p0").replace("(","('{p0").replace(")","}')").replace(" ","");
	 
	 String result_="";
	 	try
	 	{
	 		 StringBuffer localStringBuffer = new StringBuffer();
	 	//	String newString = new String(report_title.getBytes("UTF-8"));
		//jasperReport jasperReport;
	 	String str_cols ="";
		String str_detail="";
		
		String cols[]=columns.replace(" ","").split(",");
		int count_col = cols.length;
	 	int x =50;
	 	int j =0;
		int x1 =50;
		int y = 118;
		int width=100;
		int height=15;
	 		do 
				{
				    str_cols+="<field name=\""+cols[j]+"\" class=\"java.lang.String\"/>\n";

					j++;
					  
				}
				while (j<count_col);
		
		String cols_title=this.reqValue("","begin ?:=admin_v2.pkg_report_auto.get_name_single_column('"+columns+"'); end;");
	     String titles[] = cols_title.split(",");
		 int count_title= titles.length;
		 int jj =1;
		 String str_tiles="";		
		 int k=0;
		
		do {
		   System.out.println("do dai cua title"+titles[jj]);
		str_detail+="<textField isStretchWithOverflow=\"true\">\n"+
				"<reportElement stretchType=\"RelativeToBandHeight\" x=\""+x1+"\" y=\"0\" width=\""+width+"\" height=\""+height+
				"\" isPrintWhenDetailOverflows=\"true\"/>\n"+
					"<box>\n"+
					"<pen lineWidth=\"0.25\"/>\n"+
					"<topPen lineWidth=\"0.25\"/>\n"+
					"<leftPen lineWidth=\"0.25\"/>\n"+
					"<bottomPen lineWidth=\"0.25\"/>\n"+
					"<rightPen lineWidth=\"0.25\"/>\n"+
				"</box>\n"+
				"<textElement>\n"+
					"<font fontName=\"Times New Roman\" size=\"12\"/>\n"+
				"</textElement>\n"+
				"<textFieldExpression><![CDATA[$F{"+titles[jj]+"}]]></textFieldExpression>\n"+
			"</textField>\n";
			x1=x1+width;	 
			jj=jj+2;
			
		}
		while(jj<count_title);
		 do
		   {
		   
            //if(jj%2)		
			//{			
		
			str_tiles+="<staticText>\n"+
				    "<reportElement x=\""+x+"\" y=\""+y+"\" width=\""+width+"\" height=\""+height+"\"/>\n"+
				"<box>\n"+
				"	<pen lineWidth=\"0.25\"/>\n"+
				"	<topPen lineWidth=\"0.25\"/>\n"+
				"	<leftPen lineWidth=\"0.25\"/>\n"+
				"	<bottomPen lineWidth=\"0.25\"/>\n"+
				"	<rightPen lineWidth=\"0.25\"/>\n"+
				"</box>\n"+
				"<textElement textAlignment=\"Center\" verticalAlignment=\"Middle\">\n"+					
				"	<font fontName=\"Times New Roman\" size=\"12\"  pdfFontName=\"Times-Roman\" isBold=\"true\"  pdfEncoding=\"Identity-H\" isPdfEmbedded=\"true\" />\n"+
				"</textElement>\n"+
				"<text><![CDATA["+titles[k]+"]]></text>\n"+
			"</staticText>\n";
			x=x+width;
			//}
			k=k+2;
				   
		   }
		  while (k<count_title);
	 		 int page_width=width*(count_title/2) + 100;
		  int x_element =page_width/2;
		  if(x_element>=277)
		  { x_element = x_element + (x_element-277)/2;
		  	}
		  	else 
		  	{x_element= page_width-277;
		  		
		  	}
		int width_sum = page_width-276;
		  	
          String jasper="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            +"<jasperReport xmlns=\"http://jasperreports.sourceforge.net/jasperreports\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" 
			+"xsi:schemaLocation=\"http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd\"\n"
			+"name=\"tt_tb_tratruoc\" language=\"groovy\" pageWidth=\""+page_width+"\" pageHeight=\"842\""
			+" whenNoDataType=\"AllSectionsNoDetail\" columnWidth=\""+page_width+"\" leftMargin=\"0\" rightMargin=\"0\" topMargin=\"0\" bottomMargin=\"0\" isIgnorePagination=\"true\">\n"
				+"<property name=\"ireport.zoom\" value=\"1.0\"/>\n"
				+"<property name=\"ireport.x\" value=\"0\"/>\n"
				+"<property name=\"ireport.y\" value=\"0\"/>\n"
				+"<property name=\"ireport.scriptlethandling\" value=\"0\" />\n"				
				+"<subDataset name=\"f01\">\n"
		+"<queryString>\n"
			+"<![CDATA[select to_char(sysdate,'dd') ngay1, to_char(sysdate,'mm') thang1, to_char(sysdate,'yyyy') nam1, "
			+"to_char(sysdate,'dd/mm/yyyy') ngay_tt1 from dual]]>\n"
		+"</queryString>\n"
	+"</subDataset>\n"
	+"<parameter name=\"NEO_SQL_META_MAP\" class=\"java.lang.Object\"/>\n"
	+"<queryString>\n"
	+"	<![CDATA["+psfunction.replace("?:="," ?:= ")+"]]>\n"
	+"</queryString>\n"
	+str_cols
	+"<background>\n"
	+"		<band splitType=\"Stretch\"/>\n"
	+"</background>\n"
	+"<title>\n"
	+"	<band height=\"133\" splitType=\"Stretch\">\n"
			+"<image>\n"
				+"<reportElement x=\"78\" y=\"3\" width=\"83\" height=\"28\"/>\n"
				+"<imageExpression><![CDATA[\"images\\\\vinaphone_logo.png\"]]></imageExpression>\n"
			+"</image>\n"
			+"<staticText>\n"
				+"<reportElement x=\""+x_element+"\" y=\"3\" width=\"277\" height=\"14\"/>\n"
				+"<box>\n"
					+"<pen lineWidth=\"0.0\"/>\n"
					+"<topPen lineWidth=\"0.0\"/>\n"
					+"<leftPen lineWidth=\"0.0\"/>\n"
					+"<bottomPen lineWidth=\"0.0\"/>\n"
					+"<rightPen lineWidth=\"0.0\"/>\n"
				+"</box>\n"
				+"<textElement textAlignment=\"Center\" verticalAlignment=\"Middle\">\n"
				+"	<font fontName=\"Times New Roman\" size=\"12\" isBold=\"false\" pdfEncoding=\"Identity-H\" isPdfEmbedded=\"true\" />\n"
				+"</textElement>\n"
				+"<text><![CDATA[Cộng hòa xã hội chủ nghĩa việt Nam]]></text>\n"
			+"</staticText>\n"
			+"<staticText>\n"
				+"<reportElement x=\""+x_element+"\" y=\"17\" width=\"277\" height=\"14\"/>\n"
				+"<box>\n"
					+"<pen lineWidth=\"0.0\"/>\n"
					+"<topPen lineWidth=\"0.0\"/>\n"
					+"<leftPen lineWidth=\"0.0\"/>\n"
					+"<bottomPen lineWidth=\"0.0\"/>\n"
					+"<rightPen lineWidth=\"0.0\"/>\n"
				+"</box>\n"
				+"<textElement textAlignment=\"Center\" verticalAlignment=\"Middle\">\n"
					+"<font fontName=\"Times New Roman\" size=\"12\" isBold=\"false\" pdfEncoding=\"Identity-H\" isPdfEmbedded=\"false\" />\n"
				+"</textElement>\n"
				+"<text><![CDATA["+"đ"+"ộc lập tự do hạnh phúc]]></text>\n"
			+"</staticText>\n"
			+"<staticText>\n"
				+"<reportElement x=\""+x_element+"\" y=\"31\" width=\"277\" height=\"14\"/>\n"
				+"<box>\n"
					+"<pen lineWidth=\"0.0\"/>\n"
					+"<topPen lineWidth=\"0.0\"/>\n"
					+"<leftPen lineWidth=\"0.0\"/>\n"
					+"<bottomPen lineWidth=\"0.0\"/>\n"
					+"<rightPen lineWidth=\"0.0\"/>\n"
				+"</box>\n"
				+"<textElement textAlignment=\"Center\" verticalAlignment=\"Middle\">\n"
					+"<font fontName=\"Times New Roman\" size=\"12\" isBold=\"false\"/>\n"
				+"</textElement>\n"
				+"<text><![CDATA[-------------o0o-------------]]></text>\n"
			+"</staticText>\n"
			+"<staticText>\n"
				+"<reportElement x=\"0\" y=\"64\" width=\""+page_width+"\" height=\"30\"/>"
				+"<box>"
					+"<pen lineWidth=\"0.0\"/>\n"
					+"<topPen lineWidth=\"0.0\"/>\n"
					+"<leftPen lineWidth=\"0.0\"/>\n"
					+"<bottomPen lineWidth=\"0.0\"/>\n"
					+"<rightPen lineWidth=\"0.0\"/>\n"
				+"</box>\n"
				+"<textElement textAlignment=\"Center\" verticalAlignment=\"Middle\">\n"
					+"<font fontName=\"Times New Roman\" size=\"12\" isBold=\"true\"/>\n"
				+"</textElement>\n"
				+"<text><![CDATA["+W1252toUTF8(report_title)+"]]></text>\n"
			+"</staticText>\n"
			+str_tiles
		+"</band>\n"
	+"</title>\n"
	+"<pageHeader>\n"
		+"<band splitType=\"Stretch\"/>\n"
	+"</pageHeader>\n"
	+"<columnHeader>\n"
		+"<band splitType=\"Stretch\"/>\n"
	+"</columnHeader>\n"
	+"<detail>\n"
		+"<band height=\"15\" splitType=\"Stretch\">\n"
		+str_detail
		+"</band>"
	+"</detail>\n"
	+"<columnFooter>\n"
		+"<band splitType=\"Stretch\"/>\n"
	+"</columnFooter>\n"
	+"<pageFooter>\n"
		+"<band splitType=\"Stretch\"/>\n"
	+"</pageFooter>\n"
	+"<summary>"
		+"<band height=\"90\">"
			+"<textField>"
				+"<reportElement x=\""+width_sum+"\" y=\"19\" width=\"226\" height=\"50\"/>"
				+"<textElement>"
					+"<font fontName=\"Times\" size=\"12\" pdfFontName=\"Times-Roman\" pdfEncoding=\"Identity-H\"/>"
				+"</textElement>"
				+"<textFieldExpression><![CDATA[\"Ngày \"+((HashMap)$P{NEO_SQL_META_MAP}).get(\"f01ngay1\").toString()+\" tháng \"+((HashMap)$P{NEO_SQL_META_MAP}).get(\"f01thang1\").toString()+\" năm \"+((HashMap)$P{NEO_SQL_META_MAP}).get(\"f01nam1\").toString()]]></textFieldExpression>"
			+"</textField>"
		+"</band>"
	+"</summary>"
+"</jasperReport>\n";
System.out.println("ASCII");

	writerFile(this.getSysConst("ConfigDir")+root_dir+"/"+report_name+".jrxml",jasper);	
	// them moi bao cao
	String rs_ = null;
	
	if(id =="")
	    {
		   String s= "declare n number; s varchar2(2000); out_ varchar2(200):='OK'; begin "+
			" begin "+
				"  select max(report_id)+1 into n from ccs_admin.report_file;"+			
			" s:='insert into ccs_admin.report_file (file_name,report_id,report_title,reportgroup_id,disable)"+
			" values(''"+root_dir+"/"+report_name.replace("auto","auto_index")+"'','||n||',''"+report_title+"'',12,0)' ; execute immediate s; commit;"+	
			" exception when others then "+
            " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "+			
			" end; ? :=n;  end;";		
		
		
		try{
			rs_ =this.reqValue("", s);	
			result_="ID cua bao cao:"+rs_;
			if(rs_!="")
			 {
			     s= "declare n number; s varchar2(2000); out_ varchar2(200):='OK'; begin "+
			" begin "+		
			" s:='insert into ccs_admin.report_file_access (role_id,report_id)"+
			" values(''admin'',''"+rs_+"'')' ; execute immediate s; commit;"+	
			" exception when others then "+
            " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "+			
			" end; ? :=out_;  end;";	
			 }
				rs_ =this.reqValue("", s);		
            				
		} catch (Exception ex) {			
			ex.printStackTrace();
			
		}	
		}
		
		// cap nhat lai bao cao
	else 
	    {
		 String s= "declare n number; s varchar2(2000); out_ varchar2(200):='OK'; begin "+
			" begin "+
				
			" s:='update ccs_admin.report_file set file_name="+
			" ''"+root_dir+"/"+report_name.replace("auto","auto_index")+"'' where report_id="+id+"' ; execute immediate s; commit;"+	
			" exception when others then "+
            " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "+			
			" end; ? :=out_;  end;";		
		
		  rs_ = null;
		try{
			rs_ = this.reqValue("", s);
			result_="Tao bao cao va cap nhat bao cao thanh cong";
		} catch (Exception ex) {			
			ex.printStackTrace();
		}	
		}
	
			}
			catch(Exception ex){
		//System.out.println("du lieu file"+cmd);
		 
			return ex.getMessage();
		}
		   //System.out.println(result_);
			return result_;
	 }
	  public static String W1252toUTF8(String s)
     { 
         if(s == null || s.equals(""))
             return "";
         int i = 0;
         String name = "";
         String s1 = "";
         int c = 0;
		 int j=0;
		  String font_1252[]={"225","224","7843","227","7841","259","7855","7857","7859","7861","7863","226","7845","7847","7849","7851","7853","233" ,"232" ,"7867","7869","7865","234","7871","7873","7875","7877","7879","237","236","7881","297","7883","243","242","7887","245","7885","244","7889","7891","7893","7895","7897","417","7899","7901","7903","7905","7907","250","249","7911","361","7909","432","7913","7915","7917","7919","7921","253","7923","7927","7929","7925","273","193","192","7842","195","7840","258","7854","7856","7858","7860","7862","194","7844","7846","7848","7850","7852","201","200","7866","7868","7864","202","7870","7872","7874","7876","7878","205","204","7880","296","7882","211","210","7886","213","7884","212","7888","7890","7892","7894","7896","416","7898","7900","7902","7904","7906","218","217","7910","360","7908","431","7912","7914","7916","7918","7920","221","7922","7926","7928","7924","272"};		 
		  String font_utf_8[]={"á","à" ,"ả","ã"  ,"ạ" ,"ă" ,"ắ" ,"ằ" ,"ẳ" ,"ẵ" ,"ặ" ,"â" ,"ấ" ,"ầ" ,"ẩ" ,"ẫ" ,"ậ" ,"é"  ,"è"  ,"ẻ" ,"ẽ" ,"ẹ" ,"ê" ,"ế" ,"ề" ,"ể" ,"ễ" ,"ệ"  ,"í","ì" ,"ỉ" ,"ĩ" ,"ị" ,"ó" ,"ò" ,"ỏ" ,"õ" ,"ọ" ,"ô" ,"ố" ,"ồ" ,"ổ" ,"ỗ" ,"ộ" ,"ơ" ,"ớ" ,"ờ" ,"ở" ,"ỡ"  ,"ợ"  ,"ú","ù" ,"ủ" ,"ũ" ,"ụ" ,"ư","ứ" ,"ừ" ,"ử" ,"ữ"  ,"ự" ,"ý","ỳ" ,"ỷ" ,"ỹ" ,"ỵ" ,"đ" ,"Á" ,"À" ,"Ả" ,"Ã" ,"Ạ" ,"Ă" ,"Ắ" ,"Ằ" ,"Ẳ" ,"Ẵ" ,"Ặ" ,"Â" ,"Ấ" ,"Ầ" ,"Ẫ" ,"Ẩ" ,"Ậ" ,"É" ,"È","Ẻ"  ,"Ẽ"  ,"Ẹ" ,"Ê","Ế" ,"Ề" ,"Ể" ,"Ễ" ,"Ệ" ,"Í" ,"Ì" ,"Ỉ" ,"Ĩ" ,"Ị" ,"Ó" ,"Ò" ,"Ỏ" ,"Õ" ,"Ọ" ,"Ô" ,"Ố" ,"Ồ" ,"Ổ�","Ỗ" ,"Ộ" ,"Ơ" ,"Ớ" ,"Ờ" ,"Ở" ,"Ỡ" ,"Ợ" ,"Ú" ,"Ù" ,"Ủ" ,"Ũ" ,"Ụ" ,"Ư" ,"Ứ" ,"Ừ" ,"Ử" ,"Ữ" ,"Ự" ,"Ý" ,"Ỳ" ,"Ỷ" ,"Ỹ" ,"Ỵ" ,"Đ"};
       
		   System.out.println("mang 1"+font_1252.length);  
		   System.out.println("mang 2"+font_utf_8.length);
         boolean startFound = false;
		  System.out.println(s);
         for(; i < s.length(); i++)
         {
             if(s.charAt(i) == '&')
             {
                 if(name.length() > 0)
                     s1 = s1 + name;
                 if(i < s.length() - 1)
                 {
                     if(s.charAt(i + 1) == '#')
                     {
                         startFound = true;
                         name = "";
                         i++;
                     } else
                     {
                         s1 = s1 + s.charAt(i);
                     }
                 } else
                 {
                     s1 = s1 + s.charAt(i);
                 }
                 continue;
             }
             if(s.charAt(i) == ';')
             {
                 if(name.length() > 0)
                 {
                     if(name == "nbsp")
                     {
                         s1 = s1 + " ";
                         continue;
                     }
                     c = Integer.parseInt(name);
					 
                     if(c < 128)
                         s1 = s1 + (char)c;
					 else 
					 {
					     int j=0;
					     do 
						{
							  
						  if(font_1252[j].equals(name))
						   {
						     if(c==7901)
							  {
						      s1 = s1;
							  }
							  else 
							  {
						      s1 = s1 +font_utf_8[j];
							  }
							  
						   }
						   j++;
						}   while (j<font_1252.length);
					  
							
							
					 }	 
                    
                     startFound = false;
                     name = "";
                 } else
                 {
                     s1 = s1 + s.charAt(i);
                 }
                 continue;
             }
			  
			  
             if(!startFound)
                 s1 = s1 + s.charAt(i);
             else
                 name = name + s.charAt(i);
         }

         if(name.length() > 0)
             s1 = s1 + name;			
			 //s1=s1.replace("đ","");
			 System.out.println(s1);
         return s1;
     }

	public String autoReportOneTable(String tables, String rootdir,String dataSrc, 
							String dk1,String dk2,String dk3,
							String dk4,
							String content,String title
						  ) 
	{
	String report_name="";
	String result_="";
		String rs_ = null;
	  try 
	  {   
	    String ngaygio=getDateTime("yyyy.mm.dd.HH.mm.ss");
		System.out.println(ngaygio);
		String table = tables;
		 
		 report_name="auto_"+ngaygio;
		String report_name_detail="auto_"+ngaygio+"_rpt";
		String report_ajax_data = "auto_"+ngaygio+"_rpt_data";
		String report_sql = "auto_"+ngaygio+"_rpt_sql";
		String root_dir = rootdir;
		String dataSrouce=dataSrc;
		String con_dk1="";				
		String con_dk3="";
		String con_dk2="";
		String aj_content="";
		String aj_data="";
		String sql_="";
		String str_form3="";
		String sql_pra1 =" ";
		String sql_pra2 =" ";
		
		String columns=this.reqValue("","begin ?:=admin_v2.pkg_report_auto.get_name_column('"+dk3+"',1); end;");
		String columns_short=this.reqValue("","begin ?:=admin_v2.pkg_report_auto.get_name_column('"+dk3+"',0); end;");
	       System.out.println(columns);
		String[] arr_column=columns.split(",");
		// report  
	sql_+="select "+dk4+" from {p0sys_dataschema}"+table.replace("MMYYYY","{p0chuky}").replace("$SCHEMA$","")+" a where 1=1 "+
				dk1.replace("<BR>"," ")+" \n{p0parameters}\n";
		
		  aj_content+="<meta sql='{includep:"+root_dir+"/"+report_sql+"}' dbms-font='VN8VN3' run='1'  name='DULIEU' rec-per-page='100'>\n"+
		             "<meta sql=\"select to_char(sysdate,'dd') ngay, to_char(sysdate,'mm') thang,to_char(sysdate,'yyyy')nam,"+
					 " to_char(sysdate,'dd/mm/yyyy') ngaythang from dual\" dbms-font='VN8VN3' run='1' name='ngay' rec-per-page='100' >\n"+
		             "{includep:layout/ccbs/style_migrate}\n"+
					 "{includep:ccbs_qltn/qltn_tienmat/qltn_style}\n"+
					 "<table width=100% >\n"+
					 " <tr><td align=left width=50%><img src='images/vinaphone_logo.png' /> &nbsp;</td>"+
					 "<td width=50%><span align=right> <i>Ng&#224;y b&#225;o c&#225;o {F02ngaythang}</i></span></td></tr>\n"+
					 "<tr><td colspan=2>&nbsp;</td></tr>\n"+
					 "<tr  ><td align=center colspan=2><b> <font size=4px>"+title+"</font></b></td></tr>\n"+
					  "<tr><td colspan=2>&nbsp;</td></tr>\n"+
					 "<tr><td colspan=2><table width=100% id='dsMenuTab'   cellpadding=1 cellspacing=1  class='secContent'>"+
					 "<tr class='tableHeadBg' class='qlabell qheadrow'>\n"+
					 "<td class='title'>STT</td>";
					 int size_column=arr_column.length;
					 int count_column=0;
        		do 
				{
				    aj_content+="<td class='title'>"+arr_column[count_column]+"</td>\n";
					count_column++;
				}
				while (count_column<size_column);
				 aj_content+="</tr>\n<tr  "+
				 " sql-index=1  td-value=\"{includep:report/design/report_file/"+report_ajax_data+"}\">";
				 str_form3="<tr><td >{seq}</td>";
				 String[] arr_subcol=columns_short.split(",");
			   count_column=0;
			    int size_  = arr_subcol.length;
			   do 
				{
				    str_form3+="<td >{f01"+arr_subcol[count_column]+"}</td>\n";
					count_column++;
				}
				while (count_column<size_);
				 str_form3+="</tr>\n";
				 size_++;
		  aj_content+="</tr>\n"+
		  "</table></td></tr>"+		
		  "<tr><td colspan="+size_ +">&nbsp;</td></tr>"+
		  "<tr><td colspan="+size_ +">&nbsp;</td></tr>"+
		  "<tr><td colspan="+size_ +" align=right>{u0user_tinh_thanhpho}, Ng&#224;y {f02ngay} th&#225;ng {f02thang}"+
		  " n&#259;m {f02nam}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>"+"</table>\n"+
		  " <script type='text/javascript'>\n"+
				" $(document).ready(function()\n"+
				"{\n"+ 
				"$('#dsMenuTab tr').each(function (index) {\n"+
				"if (index!=0) \n"+
					"{\n"+
					"if (index %2 == 0)\n"+
					 "{\n"+				
					 "$(this).css('background-color', '#F8F8F8');\n"+
					 "}\n"+
					"if (index %2 == 1) \n"+
					"{$(this).css('background-color', '#ffffff');\n"+
					"}\n"+
				"}\n"+
			"}\n"+
			")\n"+

			"});\n"+
		  " function setColorOfRow(id){"+
				" $('#dsMenuTab tr:even').css('background-color', '#F8F8F8');\n"+
				" $('#dsMenuTab tr:odd').css('background-color', '#ffffff');\n"+
				" $('#tr'+id).css('background-color', '#FBE0AA');} </script>\n";
		  
		// System.out.println(aj_content);
		
			 if(tables.indexOf("MMYYYY")!=-1)
			{
			 con_dk2+="'&{encode:chuky}='+$('#chuky').val()";
			}
			con_dk2+=";";
			  aj_data="{includep:layout/ccbs/style_migrate}\n"+
		 "<title>"+title+"</title>"+
		 "<table width=100% class=secContent><tr class=secHead> <td colspan=2>"+title+"</td></tr>\n";
		 if(tables.indexOf("MMYYYY")!=-1)
		 {
			aj_data+="<tr><td class=label>Chu k&#7923;</td><td>"+
			" <select id='chuky' name='--chuky'  sql=\"  select replace(chukyno,'/','') chukyno,"+
			" replace(chukyno,'/','') chuky from ("+
			" SELECT ccs_admin.qltn.chukyno_dangchu(chukyno,'{p0sys_dataschema}','{p0sys_userid}') chukyno "+
			"from {p0sys_dataschema}chukynos where trangthai=1)"+
			" order by to_date(chukyno,'mm/yyyy') desc\"></select>"+
			"</td></tr>\n";	
		 }
		// dieu kien sau menh de where
		System.out.println("dieu kien 2"+dk2);
		
		RowSet r = this.reqRSet("","begin ?:=admin_v2.pkg_report_auto.get_auto_column('"+dk2.replace("a.","").replace("b.","")+"'); end;");
		while (r.next())
		{
		 aj_data+="<tr>";
		  String r_rowset = r.getString("COLUMNS");
		  System.out.println("dieu kien 2"+r_rowset);
		   String[] input =r_rowset.split(",");
		 //int size = input.length;
		 //int count=0;
		 sql_pra1+="";
		 String[] arr_dk2 = dk2.split(",");
		 int size_dk2=arr_dk2.length;
			 int count_dk2=0;
			  int pos=0;
		 if(input[0].equals("CHUKY")||input[0].equals("CHUKYNO"))
				{
				 if(tables.indexOf("MMYYYY")!=1)				
				  aj_data+="<td width=40% class=label>"+input[1]+"</td>";
				}
		    else 
		   {
		    if(input[2].equals("DATE"))
					{
					pos =dk2.indexOf(input[0]); 
					 if(pos!=0)
					 {
					    pos=pos-2;
					 }
					 System.out.println("Hien thi truong date");
				sql_pra1+=" if($('#TUNGAY_"+input[0]+"').val()!='') "+
					"  { str=str+ 'and "+dk2.substring(pos,pos+2)+input[0]+" >= to_date(\\{p0SingleQuote}'+$('#TUNGAY_"+input[0]+"').val()+'\\{p0SingleQuote},\\{p0SingleQuote}dd/mm/yyyy\\{p0SingleQuote})';}\n"+
					" if($('#DENNGAY_"+input[0]+"').val()!='') "+
					"  { str=str+ 'and "+dk2.substring(pos,pos+2)+input[0]+" <= to_date(\\{p0SingleQuote}'+$('#DENNGAY_"+input[0]+"').val()+'\\{p0SingleQuote},\\{p0SingleQuote}dd/mm/yyyy\\{p0SingleQuote})';}\n";
					/*	*/
					aj_data+="<td width=40% class=label>T&#7915; "+input[1]+"</td>\n";
					aj_data+="<td>";
					aj_data+="<input type=text style='width:80%' id='TUNGAY_"+input[0]+"' name='"+input[0]+"'>";
					aj_data+="</td></tr>\n";
					aj_data+="<tr><td width=40% class=label>&#272;&#7871;n "+input[1]+"</td>\n";
					aj_data+="<td>";
					aj_data+="<input type=text style='width:80%' id='DENNGAY_"+input[0]+"' name='"+input[0]+"'>\n";
					aj_data+="</td>";
					aj_data+="<script>\n$(document).ready(function() {"+
						"var goto_page=25;\n"+
						"$('#TUNGAY_"+input[0]+"').datepicker({dateFormat:'dd/mm/yy'});\n"+
						"$('#DENNGAY_"+input[0]+"').datepicker({dateFormat:'dd/mm/yy'});\n"+
						"});</script>\n";
			
					}
			   else
					{aj_data+="<td width=40% class=label>"+input[1]+"</td>";
					}
			}
		    if(input[2].equals("VARCHAR2"))  
			 {
			  if(input[0].equals("CHUKY")||input[0].equals("CHUKYNO"))
			  {
				if(tables.indexOf("MMYYYY")!=-1)
				 
					aj_data+="<td>";
					aj_data+="<select id='"+input[0]+"' name='--"+input[0]+"' "+
					"  sql='  select replace(chukyno,{p0singleQuote}/{p0singleQuote},{p0singleQuote}{p0singleQuote}) chukyno,"+
					" replace(chukyno,{p0singleQuote}/{p0singleQuote},{p0singleQuote}{p0singleQuote}) chuky from ("+
					" SELECT ccs_admin.qltn.chukyno_dangchu(chukyno,{p0singleQuote}{p0sys_dataschema}{p0singleQuote}"+",{p0singleQuote}{p0sys_userid}{p0singleQuote}) chukyno from {p0sys_dataschema}chukynos where trangthai=1)"+
					" order by to_date(chukyno,\"mm/yyyy\") desc'></select>\n";
						aj_data+="</td>";
				 
			  }
			  else 
			  {
			  aj_data+="<td>";
			   aj_data+="<input type=text style='width:80%' class='input_textText' id='"+input[0]+"'>\n";
			   aj_data+="</td>";
			   }
			 pos =dk2.indexOf(input[0]); 
					 if(pos!=0)
					 {
					    pos=pos-2;
					 }
			 sql_pra1+=" if($('#"+input[0]+"').val()!='') "+
					"  { str=str+ 'and "+dk2.substring(pos,pos+2)+input[0]+" = trim(\\{p0SingleQuote}'+$('#"+input[0]+"').val()+'\\{p0SingleQuote})';}\n";
				 /* */
			 }
			if(input[2].equals("NUMBER"))  
			 {
		pos =dk2.indexOf(input[0]); 
					 if(pos!=0)
					 {
					    pos=pos-2;
					 }
			 sql_pra1+=" if($('#"+input[0]+"').val()!='') "+
					"  {str=str+ 'and "+dk2.substring(pos,pos+2)+input[0]+" = trim(\\{p0SingleQuote}'+$('#"+input[0]+"').val()+'\\{p0SingleQuote})';}\n";
				/**/
			 aj_data+="<td>";
			   aj_data+="<input type=text style='width:80%' class='input_textNumber' id='"+input[0]+"'>\n";
			   aj_data+="</td>";
			 }
			 
			aj_data+="</tr>\n";
		}
		
			if(sql_pra1!="")
			  {			  
			   sql_pra2="'&{encode:parameters}='+str+";
			  }
						
		  aj_data+="<tr><td colspan=2 align=center><input class=button type=button value='b�o c�o' id='cmd_rpt'>&nbsp;&nbsp;"+
		  "<input type=button class=button value='K&#7871;t th&#250;c' id='cmd_cancel'></td></tr>";
		 aj_data+="</table>";
		 aj_data+="<script>";
		 aj_data+="$(document).ready(function(){});\n"+
		          "$('#cmd_cancel').click(function(){window.close();});\n"+
                   "$('#cmd_rpt').click(function(){\n"+
				   "var s_ ='';\n"+
				   "var str ='';\n"+
				   sql_pra1+
				   " s_='main?{encode:+configFile}={encode:"+root_dir+"/"+report_name_detail+"}'+\n"+	
				   sql_pra2+
                   con_dk2+				   
				   "window.open(s_,'mywindow');"+
				   "});";				  
		 aj_data+="</script>";
		
		
		writerFile(this.getSysConst("ConfigDir")+root_dir+"/"+report_name+".htm",aj_data);			
		writerFile(this.getSysConst("ConfigDir")+root_dir+"/"+report_sql+".htm",sql_);	
		writerFile(this.getSysConst("ConfigDir")+root_dir+"/"+report_name_detail+".htm",aj_content);
		writerFile(this.getSysConst("ConfigDir")+root_dir+"/"+report_ajax_data+".htm",str_form3);
		System.out.println("Tao file thanh cong");
		
		  String s= "declare n number; s varchar2(2000); out_ varchar2(200):='OK'; begin "+
			" begin "+
				"  select max(report_id)+1 into n from ccs_admin.report_file;"+			
			" s:='insert into ccs_admin.report_file (file_name,report_id,report_title,reportgroup_id,disable)"+
			" values(''"+root_dir+"/"+report_name+"'','||n||',''"+title+"'',12,0)' ; execute immediate s; commit;"+	
			" exception when others then "+
            " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "+			
			" end; ? :=n;  end;";		
		
		
		try{
			rs_ =this.reqValue("", s);	
			result_="ID cua bao cao:"+rs_;
			if(rs_!="")
			 {
			     s= "declare n number; s varchar2(2000); out_ varchar2(200):='OK'; begin "+
			" begin "+		
			" s:='insert into ccs_admin.report_file_access (role_id,report_id)"+
			" values(''admin'',''"+rs_+"'')' ; execute immediate s; commit;"+	
			" exception when others then "+
            " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "+			
			" end; ? :=out_;  end;";	
			 }
				rs_ =this.reqValue("", s);		
            				
		} catch (Exception ex) {			
			ex.printStackTrace();
			
		}	
		}
		catch(Exception ex){
			return ex.getMessage();
		}
       return result_;		
		
  	}
	public String autoReport3Table(String tables, String rootdir,String dataSrc, 
							String dk1,String dk2,String dk3,
							String dk4,
							String content,String title
						  ) 
	{
	String report_name="";
		String result_="";
		String rs_="";
	  try 
	  {   
	    String ngaygio=getDateTime("yyyy.mm.dd.HH.mm.ss");
		System.out.println(ngaygio);
		String[] table = tables.split(",");
		 //report_name="auto_"+table[0].replace("_MMYYYY","").replace("$SCHEMA$","");
		 report_name="auto_"+ngaygio;
		String report_name_detail="auto_"+ngaygio+"_rpt";
		String report_ajax_data = "auto_"+ngaygio+"_rpt_data";
		String report_sql = "auto_"+ngaygio+"_rpt_sql";
		String root_dir = rootdir;
		String dataSrouce=dataSrc;
		String con_dk1="";				
		String con_dk3="";
		String con_dk2="";
		String aj_content="";
		String aj_data="";
		String sql_="";
		String str_form3="";
		String sql_pra1 =" ";
		String sql_pra2 =" ";
	
		String columns=this.reqValue("","begin ?:=admin_v2.pkg_report_auto.get_name_column('"+dk3+"',1); end;");
		String columns_short=this.reqValue("","begin ?:=admin_v2.pkg_report_auto.get_name_column('"+dk3+"',0); end;");
	       System.out.println(columns);
		String[] arr_column=columns.split(",");
		// report  
	sql_+="select "+dk4+" from {p0sys_dataschema}"+table[0].replace("MMYYYY","{p0chuky}").replace("$SCHEMA$","")+" a, {p0sys_dataschema}"+
				table[1].replace("MMYYYY","{p0chuky}").replace("$SCHEMA$","")+" b,{p0sys_dataschema}"+
				table[2].replace("MMYYYY","{p0chuky}").replace("$SCHEMA$","")+" c where 1=1 "+
				dk1.replace("<BR>"," ")+" \n{p0parameters}\n";
		
		  aj_content+="<meta sql='{includep:"+root_dir+"/"+report_sql+"}' dbms-font='VN8VN3' run='1'  name='DULIEU' rec-per-page='100'>\n"+
		             "<meta sql=\"select to_char(sysdate,'dd') ngay, to_char(sysdate,'mm') thang,to_char(sysdate,'yyyy')nam,"+
					 " to_char(sysdate,'dd/mm/yyyy') ngaythang from dual\" dbms-font='VN8VN3' run='1' name='ngay' rec-per-page='100' >\n"+
		             "{includep:layout/ccbs/style_migrate}\n"+
					 "{includep:ccbs_qltn/qltn_tienmat/qltn_style}\n"+
					 "<table width=100% >\n"+
					 " <tr><td align=left width=50%><img src='images/vinaphone_logo.png' /> &nbsp;</td>"+
					 "<td width=50%><span align=right> <i>Ng&#224;y b&#225;o c&#225;o {F02ngaythang}</i></span></td></tr>\n"+
					 "<tr><td colspan=2>&nbsp;</td></tr>\n"+
					 "<tr  ><td align=center colspan=2><b> <font size=4px>"+title+"</font></b></td></tr>\n"+
					  "<tr><td colspan=2>&nbsp;</td></tr>\n"+
					 "<tr><td colspan=2><table width=100% id='dsMenuTab'   cellpadding=1 cellspacing=1  class='secContent'>"+
					 "<tr class='tableHeadBg' class='qlabell qheadrow'>\n"+
					 "<td class='title'>STT</td>";
					 int size_column=arr_column.length;
					 int count_column=0;
        		do 
				{
				    aj_content+="<td class='title'>"+arr_column[count_column]+"</td>\n";
					count_column++;
				}
				while (count_column<size_column);
				 aj_content+="</tr>\n<tr  "+
				 " sql-index=1  td-value=\"{includep:report/design/report_file/"+report_ajax_data+"}\">";
				 str_form3="<tr><td >{seq}</td>";
				 String[] arr_subcol=columns_short.split(",");
			   count_column=0;
			    int size_  = arr_subcol.length;
			   do 
				{
				    str_form3+="<td >{f01"+arr_subcol[count_column]+"}</td>\n";
					count_column++;
				}
				while (count_column<size_);
				 str_form3+="</tr>\n";
				 size_++;
		  aj_content+="</tr>\n"+
		  "</table></td></tr>"+		
		  "<tr><td colspan="+size_ +">&nbsp;</td></tr>"+
		  "<tr><td colspan="+size_ +">&nbsp;</td></tr>"+
		  "<tr><td colspan="+size_ +" align=right>{u0user_tinh_thanhpho}, Ng&#224;y {f02ngay} th&#225;ng {f02thang}"+
		  " n&#259;m {f02nam}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>"+"</table>\n"+
		  " <script type='text/javascript'>\n"+
				" $(document).ready(function()\n"+
				"{\n"+ 
				"$('#dsMenuTab tr').each(function (index) {\n"+
				"if (index!=0) \n"+
					"{\n"+
					"if (index %2 == 0)\n"+
					 "{\n"+				
					 "$(this).css('background-color', '#F8F8F8');\n"+
					 "}\n"+
					"if (index %2 == 1) \n"+
					"{$(this).css('background-color', '#ffffff');\n"+
					"}\n"+
				"}\n"+
			"}\n"+
			")\n"+

			"});\n"+
		  " function setColorOfRow(id){"+
				" $('#dsMenuTab tr:even').css('background-color', '#F8F8F8');\n"+
				" $('#dsMenuTab tr:odd').css('background-color', '#ffffff');\n"+
				" $('#tr'+id).css('background-color', '#FBE0AA');} </script>\n";
		  
		// System.out.println(aj_content);
		
			 if(tables.indexOf("MMYYYY")!=-1)
			{
			 con_dk2+="'&{encode:chuky}='+$('#chuky').val()";
			}
			con_dk2+=";";
			  aj_data="{includep:layout/ccbs/style_migrate}\n"+
		 "<title>"+title+"</title>"+
		 "<table width=100% class=secContent><tr class=secHead> <td colspan=2>"+title+"</td></tr>\n";
		 if(tables.indexOf("MMYYYY")!=-1)
		 {
			aj_data+="<tr><td class=label>Chu k&#7923;</td><td>"+
			" <select id='chuky' name='--chuky'  sql=\"  select replace(chukyno,'/','') chukyno,"+
			" replace(chukyno,'/','') chuky from ("+
			" SELECT ccs_admin.qltn.chukyno_dangchu(chukyno,'{p0sys_dataschema}','{p0sys_userid}') chukyno "+
			"from {p0sys_dataschema}chukynos where trangthai=1)"+
			" order by to_date(chukyno,'mm/yyyy') desc\"></select>"+
			"</td></tr>\n";	
		 }
		// dieu kien sau menh de where
		
		
		RowSet r = this.reqRSet("","begin ?:=admin_v2.pkg_report_auto.get_auto_column('"+dk2.replace("a.","").replace("b.","")+"'); end;");
		while (r.next())
		{
		 aj_data+="<tr>";
		  String r_rowset = r.getString("COLUMNS");
		  System.out.println(r_rowset);
		   String[] input =r_rowset.split(",");
		 //int size = input.length;
		 //int count=0;
		 sql_pra1+="";
		 String[] arr_dk2 = dk2.split(",");
		 int size_dk2=arr_dk2.length;
			 int count_dk2=0;
			  int pos=0;
		 if(input[0].equals("CHUKY")||input[0].equals("CHUKYNO"))
				{
				 if(tables.indexOf("MMYYYY")!=1)				
				  aj_data+="<td width=40% class=label>"+input[1]+"</td>";
				}
		    else 
		   {
		    if(input[2].equals("DATE"))
					{
					pos =dk2.indexOf(input[0]); 
					 if(pos!=0)
					 {
					    pos=pos-2;
					 }
					 System.out.println("hien thi truong date");
				sql_pra1+=" if($('#TUNGAY_"+input[0]+"').val()!='') "+
					"  { str=str+ 'and "+dk2.substring(pos,pos+2)+input[0]+" >= to_date(\\{p0SingleQuote}'+$('#TUNGAY_"+input[0]+"').val()+'\\{p0SingleQuote},\\{p0SingleQuote}dd/mm/yyyy\\{p0SingleQuote})';}\n"+
					" if($('#DENNGAY_"+input[0]+"').val()!='') "+
					"  { str=str+ 'and "+dk2.substring(pos,pos+2)+input[0]+" <= to_date(\\{p0SingleQuote}'+$('#DENNGAY_"+input[0]+"').val()+'\\{p0SingleQuote},\\{p0SingleQuote}dd/mm/yyyy\\{p0SingleQuote})';}\n";
					/*	*/
					aj_data+="<td width=40% class=label>T&#7915; "+input[1]+"</td>\n";
					aj_data+="<td>";
					aj_data+="<input type=text style='width:80%' id='TUNGAY_"+input[0]+"' name='"+input[0]+"'>";
					aj_data+="</td></tr>\n";
					aj_data+="<tr><td width=40% class=label>&#272;&#7871;n "+input[1]+"</td>\n";
					aj_data+="<td>";
					aj_data+="<input type=text style='width:80%' id='DENNGAY_"+input[0]+"' name='"+input[0]+"'>\n";
					aj_data+="</td>";
					aj_data+="<script>\n$(document).ready(function() {"+
						"var goto_page=25;\n"+
						"$('#TUNGAY_"+input[0]+"').datepicker({dateFormat:'dd/mm/yy'});\n"+
						"$('#DENNGAY_"+input[0]+"').datepicker({dateFormat:'dd/mm/yy'});\n"+
						"});</script>\n";
			
					}
			   else
					{aj_data+="<td width=40% class=label>"+input[1]+"</td>";
					}
			}
		    if(input[2].equals("VARCHAR2"))  
			 {
			  if(input[0].equals("CHUKY")||input[0].equals("CHUKYNO"))
			  {
				if(tables.indexOf("MMYYYY")!=-1)
				 
					aj_data+="<td>";
					aj_data+="<select id='"+input[0]+"' name='--"+input[0]+"' "+
					"  sql='  select replace(chukyno,{p0singleQuote}/{p0singleQuote},{p0singleQuote}{p0singleQuote}) chukyno,"+
					" replace(chukyno,{p0singleQuote}/{p0singleQuote},{p0singleQuote}{p0singleQuote}) chuky from ("+
					" SELECT ccs_admin.qltn.chukyno_dangchu(chukyno,{p0singleQuote}{p0sys_dataschema}{p0singleQuote}"+",{p0singleQuote}{p0sys_userid}{p0singleQuote}) chukyno from {p0sys_dataschema}chukynos where trangthai=1)"+
					" order by to_date(chukyno,\"mm/yyyy\") desc'></select>\n";
						aj_data+="</td>";
				 
			  }
			  else 
			  {
			  aj_data+="<td>";
			   aj_data+="<input type=text style='width:80%' class='input_textText' id='"+input[0]+"'>\n";
			   aj_data+="</td>";
			   }
			 pos =dk2.indexOf(input[0]); 
					 if(pos!=0)
					 {
					    pos=pos-2;
					 }
			 sql_pra1+=" if($('#"+input[0]+"').val()!='') "+
					"  { str=str+ 'and "+dk2.substring(pos,pos+2)+input[0]+" = trim(\\{p0SingleQuote}'+$('#"+input[0]+"').val()+'\\{p0SingleQuote})';}\n";
				 /* */
			 }
			if(input[2].equals("NUMBER"))  
			 {
		pos =dk2.indexOf(input[0]); 
					 if(pos!=0)
					 {
					    pos=pos-2;
					 }
			 sql_pra1+=" if($('#"+input[0]+"').val()!='') "+
					"  {str=str+ 'and "+dk2.substring(pos,pos+2)+input[0]+" = trim(\\{p0SingleQuote}'+$('#"+input[0]+"').val()+'\\{p0SingleQuote})';}\n";
				/**/
			 aj_data+="<td>";
			   aj_data+="<input type=text style='width:80%' class='input_textNumber' id='"+input[0]+"'>\n";
			   aj_data+="</td>";
			 }
			 
			aj_data+="</tr>\n";
		}
		
			if(sql_pra1!="")
			  {			  
			   sql_pra2="'&{encode:parameters}='+str+";
			  }
						
		  aj_data+="<tr><td colspan=2 align=center><input class=button type=button value='b�o c�o' id='cmd_rpt'>&nbsp;&nbsp;"+
		  "<input type=button class=button value='K&#7871;t th&#250;c' id='cmd_cancel'></td></tr>";
		 aj_data+="</table>";
		 aj_data+="<script>";
		 aj_data+="$(document).ready(function(){});\n"+
		          "$('#cmd_cancel').click(function(){window.close();});\n"+
                   "$('#cmd_rpt').click(function(){\n"+
				   "var s_ ='';\n"+
				   "var str ='';\n"+
				   sql_pra1+
				   " s_='main?{encode:+configFile}={encode:"+root_dir+"/"+report_name_detail+"}'+\n"+	
				   sql_pra2+
                   con_dk2+				   
				   "window.open(s_,'mywindow');"+
				   "});";				  
		 aj_data+="</script>";
		
		
		writerFile(this.getSysConst("ConfigDir")+root_dir+"/"+report_name+".htm",aj_data);			
		writerFile(this.getSysConst("ConfigDir")+root_dir+"/"+report_sql+".htm",sql_);	
		writerFile(this.getSysConst("ConfigDir")+root_dir+"/"+report_name_detail+".htm",aj_content);
		writerFile(this.getSysConst("ConfigDir")+root_dir+"/"+report_ajax_data+".htm",str_form3);
		System.out.println("Tao file thanh cong");
		 String s= "declare n number; s varchar2(2000); out_ varchar2(200):='OK'; begin "+
			" begin "+
				"  select max(report_id)+1 into n from ccs_admin.report_file;"+			
			" s:='insert into ccs_admin.report_file (file_name,report_id,report_title,reportgroup_id,disable)"+
			" values(''"+root_dir+"/"+report_name+"'','||n||',''"+title+"'',12,0)' ; execute immediate s; commit;"+	
			" exception when others then "+
            " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "+			
			" end; ? :=n;  end;";		
		
		
		try{
			rs_ =this.reqValue("", s);	
			result_="ID cua bao cao:"+rs_;
			if(rs_!="")
			 {
			     s= "declare n number; s varchar2(2000); out_ varchar2(200):='OK'; begin "+
			" begin "+		
			" s:='insert into ccs_admin.report_file_access (role_id,report_id)"+
			" values(''admin'',''"+rs_+"'')' ; execute immediate s; commit;"+	
			" exception when others then "+
            " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "+			
			" end; ? :=out_;  end;";	
			 }
				rs_ =this.reqValue("", s);		
            				
		} catch (Exception ex) {			
			ex.printStackTrace();
			
		}	
		}
		catch(Exception ex){
			return ex.getMessage();
		}
       return result_;
		
  	}
  // tao file bao cao 2 table 
   public String autoReport(String tables, String rootdir,String dataSrc, 
							String dk1,String dk2,String dk3,
							String dk4,
							String content,String title
						  ) 
	{
	String report_name="";
	  String result_="a";
	    String rs_="";
	  try 
	  {   
	    String ngaygio=getDateTime("yyyy.mm.dd.HH.mm.ss");
		System.out.println(ngaygio);
		String[] table = tables.split(",");
		 //report_name="auto_"+table[0].replace("_MMYYYY","").replace("$SCHEMA$","");
		 report_name="auto_"+ngaygio;
		String report_name_detail="auto_"+ngaygio+"_rpt";
		String report_ajax_data = "auto_"+ngaygio+"_rpt_data";
		String report_sql = "auto_"+ngaygio+"_rpt_sql";
		String root_dir = rootdir;
		String dataSrouce=dataSrc;
		String con_dk1="";				
		String con_dk3="";
		String con_dk2="";
		String aj_content="";
		String aj_data="";
		String sql_="";
		String str_form3="";
		String sql_pra1 =" ";
		String sql_pra2 =" ";
		
		String columns=this.reqValue("","begin ?:=admin_v2.pkg_report_auto.get_name_column('"+dk3+"',2); end;");
		String columns_short=this.reqValue("","begin ?:=admin_v2.pkg_report_auto.get_name_column('"+dk3+"',0); end;");
	     
		String[] arr_column=columns.split(",");
		// report  
	
	sql_+="select "+dk4+" from {p0sys_dataschema}"+table[0].replace("MMYYYY","{p0chuky}").replace("$SCHEMA$","")+" a, {p0sys_dataschema}"+
				table[1].replace("MMYYYY","{p0chuky}").replace("$SCHEMA$","")+" b where 1=1 "+
				dk1.replace("<BR>"," ")+" \n{p0parameters}\n";
	
		  aj_content+="<meta sql='{includep:"+root_dir+"/"+report_sql+"}' dbms-font='VN8VN3' run='1'  name='DULIEU' rec-per-page='100000'>\n"+
		             "<meta sql=\"select to_char(sysdate,'dd') ngay, to_char(sysdate,'mm') thang,to_char(sysdate,'yyyy')nam,"+
					 " to_char(sysdate,'dd/mm/yyyy') ngaythang from dual\" dbms-font='VN8VN3' run='1' name='ngay' rec-per-page='100' >\n"+
		             "{includep:layout/ccbs/style_migrate}\n"+
					 "{includep:ccbs_qltn/qltn_tienmat/qltn_style}\n"+
					 "<table width=100% >\n"+
					 " <tr><td align=left width=50%><img src='images/vinaphone_logo.png' /> &nbsp;</td>"+
					 "<td width=50%><span align=right> <i>Ng&#224;y b&#225;o c&#225;o {F02ngaythang}</i></span></td></tr>\n"+
					 "<tr><td colspan=2>&nbsp;</td></tr>\n"+
					 "<tr  ><td align=center colspan=2><b> <font size=4px>"+title+"</font></b></td></tr>\n"+
					  "<tr><td colspan=2>&nbsp;</td></tr>\n"+
					 "<tr><td colspan=2><table width=100% id='dsMenuTab'   cellpadding=1 cellspacing=1  class='secContent'>"+
					 "<tr class='tableHeadBg' class='qlabell qheadrow'>\n"+
					 "<td class='title'>STT</td>";
					 int size_column=arr_column.length;
					 int count_column=0;
        		do 
				{
				    aj_content+="<td class='title'>"+arr_column[count_column]+"</td>\n";
					count_column++;
				}
				while (count_column<size_column);
				 aj_content+="</tr>\n<tr  "+
				 " sql-index=1  td-value=\"{includep:report/design/report_file/"+report_ajax_data+"}\">";
				 str_form3="<tr><td >{seq}</td>";
				 String[] arr_subcol=columns_short.split(",");
			   count_column=0;
			    int size_  = arr_subcol.length;
			   do 
				{
				    str_form3+="<td >{f01"+arr_subcol[count_column]+"}</td>\n";
					count_column++;
				}
				while (count_column<size_);
				 str_form3+="</tr>\n";
				 size_++;
		  aj_content+="</tr>\n"+
		  "</table></td></tr>"+		
		  "<tr><td colspan="+size_ +">&nbsp;</td></tr>"+
		  "<tr><td colspan="+size_ +">&nbsp;</td></tr>"+
		  "<tr><td colspan="+size_ +" align=right>{u0user_tinh_thanhpho}, Ng&#224;y {f02ngay} th&#225;ng {f02thang}"+
		  " n&#259;m {f02nam}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>"+"</table>\n"+
		  " <script type='text/javascript'>\n"+
				" $(document).ready(function()\n"+
				"{\n"+ 
				"$('#dsMenuTab tr').each(function (index) {\n"+
				"if (index!=0) \n"+
					"{\n"+
					"if (index %2 == 0)\n"+
					 "{\n"+				
					 "$(this).css('background-color', '#F8F8F8');\n"+
					 "}\n"+
					"if (index %2 == 1) \n"+
					"{$(this).css('background-color', '#ffffff');\n"+
					"}\n"+
				"}\n"+
			"}\n"+
			")\n"+

			"});\n"+
		  " function setColorOfRow(id){"+
				" $('#dsMenuTab tr:even').css('background-color', '#F8F8F8');\n"+
				" $('#dsMenuTab tr:odd').css('background-color', '#ffffff');\n"+
				" $('#tr'+id).css('background-color', '#FBE0AA');} </script>\n";
		  
		// System.out.println(aj_content);
		
			 if(tables.indexOf("MMYYYY")!=-1)
			{
			 con_dk2+="'&{encode:chuky}='+$('#chuky').val()";
			}
			con_dk2+=";";
			  aj_data="{includep:layout/ccbs/style_migrate}\n"+
		 "<title>"+title+"</title>"+
		 "<table width=100% class=secContent><tr class=secHead> <td colspan=2>"+title+"</td></tr>\n";
		 if(tables.indexOf("MMYYYY")!=-1)
		 {
			aj_data+="<tr><td class=label>Chu k&#7923;</td><td>"+
			" <select id='chuky' name='--chuky'  sql=\"  select replace(chukyno,'/','') chukyno,"+
			" replace(chukyno,'/','') chuky from ("+
			" SELECT ccs_admin.qltn.chukyno_dangchu(chukyno,'{p0sys_dataschema}','{p0sys_userid}') chukyno "+
			"from {p0sys_dataschema}chukynos where trangthai=1)"+
			" order by to_date(chukyno,'mm/yyyy') desc\"></select>"+
			"</td></tr>\n";	
		 }
		// dieu kien sau menh de where
		
		
		RowSet r = this.reqRSet("","begin ?:=admin_v2.pkg_report_auto.get_auto_column('"+dk2.replace("a.","").replace("b.","")+"'); end;");
		while (r.next())
		{
		 aj_data+="<tr>";
		  String r_rowset = r.getString("COLUMNS");
		  
		   String[] input =r_rowset.split(",");
		 //int size = input.length;
		 //int count=0;
		 sql_pra1+="";
		 String[] arr_dk2 = dk2.split(",");
		 int size_dk2=arr_dk2.length;
			 int count_dk2=0;
			  int pos=0;
		 if(input[0].equals("CHUKY")||input[0].equals("CHUKYNO"))
				{
				 if(tables.indexOf("MMYYYY")<=0)				
				  aj_data+="<td width=40% class=label>"+input[1]+"</td>";
				}
		    else 
		   {
		    if(input[2].equals("DATE"))
					{
					pos =dk2.indexOf(input[0]); 
					 if(pos!=0)
					 {
					    pos=pos-2;
					 }
					 
				sql_pra1+=" if($('#TUNGAY_"+input[0]+"').val()!='') "+
					"  { str=str+ 'and "+dk2.substring(pos,pos+2)+input[0]+" >= to_date(\\{p0SingleQuote}'+$('#TUNGAY_"+input[0]+"').val()+'\\{p0SingleQuote},\\{p0SingleQuote}dd/mm/yyyy\\{p0SingleQuote})';}\n"+
					" if($('#DENNGAY_"+input[0]+"').val()!='') "+
					"  { str=str+ 'and "+dk2.substring(pos,pos+2)+input[0]+" <= to_date(\\{p0SingleQuote}'+$('#DENNGAY_"+input[0]+"').val()+'\\{p0SingleQuote},\\{p0SingleQuote}dd/mm/yyyy\\{p0SingleQuote})';}\n";
					/*	*/
					aj_data+="<td width=40% class=label>T&#7915; "+input[1]+"</td>\n";
					aj_data+="<td>";
					aj_data+="<input type=text style='width:80%' id='TUNGAY_"+input[0]+"' name='"+input[0]+"'>";
					aj_data+="</td></tr>\n";
					aj_data+="<tr><td width=40% class=label>&#272;&#7871;n "+input[1]+"</td>\n";
					aj_data+="<td>";
					aj_data+="<input type=text style='width:80%' id='DENNGAY_"+input[0]+"' name='"+input[0]+"'>\n";
					aj_data+="</td>";
					aj_data+="<script>\n$(document).ready(function() {"+
						"var goto_page=25;\n"+
						"$('#TUNGAY_"+input[0]+"').datepicker({dateFormat:'dd/mm/yy'});\n"+
						"$('#DENNGAY_"+input[0]+"').datepicker({dateFormat:'dd/mm/yy'});\n"+
						"});</script>\n";
			
					}
			   else
					{aj_data+="<td width=40% class=label>"+input[1]+"</td>";
					}
			}
		    if(input[2].equals("VARCHAR2"))  
			 {
			  if(input[0].equals("CHUKY")||input[0].equals("CHUKYNO"))
			  {
				if(tables.indexOf("MMYYYY")==-1)
				 
					aj_data+="<td>";
					aj_data+="<select id='"+input[0]+"' name='--"+input[0]+"' "+
					"  sql='  select replace(chukyno,{p0singleQuote}/{p0singleQuote},{p0singleQuote}{p0singleQuote}) chukyno,"+
					" replace(chukyno,{p0singleQuote}/{p0singleQuote},{p0singleQuote}{p0singleQuote}) chuky from ("+
					" SELECT ccs_admin.qltn.chukyno_dangchu(chukyno,{p0singleQuote}{p0sys_dataschema}{p0singleQuote}"+",{p0singleQuote}{p0sys_userid}{p0singleQuote}) chukyno from {p0sys_dataschema}chukynos where trangthai=1)"+
					" order by to_date(chukyno,{p0singleQuote}mm/yyyy{p0singleQuote}) desc'></select>\n";
						aj_data+="</td>";
				 
			  }
			  else 
			  {
			  aj_data+="<td>";
			   aj_data+="<input type=text style='width:80%' class='input_textText' id='"+input[0]+"'>\n";
			   aj_data+="</td>";
			   }
			 pos =dk2.indexOf(input[0]); 
					 if(pos!=0)
					 {
					    pos=pos-2;
					 }
			 sql_pra1+=" if($('#"+input[0]+"').val()!='') "+
					"  { str=str+ 'and "+dk2.substring(pos,pos+2)+input[0]+" = trim(\\{p0SingleQuote}'+$('#"+input[0]+"').val()+'\\{p0SingleQuote})';}\n";
				 /* */
			 }
			if(input[2].equals("NUMBER"))  
			 {
		pos =dk2.indexOf(input[0]); 
					 if(pos!=0)
					 {
					    pos=pos-2;
					 }
			 sql_pra1+=" if($('#"+input[0]+"').val()!='') "+
					"  {str=str+ 'and "+dk2.substring(pos,pos+2)+input[0]+" = trim(\\{p0SingleQuote}'+$('#"+input[0]+"').val()+'\\{p0SingleQuote})';}\n";
				/**/
			 aj_data+="<td>";
			   aj_data+="<input type=text style='width:80%' class='input_textNumber' id='"+input[0]+"'>\n";
			   aj_data+="</td>";
			 }
			 
			aj_data+="</tr>\n";
		}
		
			if(sql_pra1!="")
			  {			  
			   sql_pra2="'&{encode:parameters}='+str+";
			  }
						
		  aj_data+="<tr><td colspan=2 align=center><input class=button type=button value='b�o c�o' id='cmd_rpt'>&nbsp;&nbsp;"+
		  "<input type=button class=button value='K&#7871;t th&#250;c' id='cmd_cancel'></td></tr>";
		 aj_data+="</table>";
		 aj_data+="<script>";
		 aj_data+="$(document).ready(function(){});\n"+
		          "$('#cmd_cancel').click(function(){window.close();});\n"+
                   "$('#cmd_rpt').click(function(){\n"+
				   "var s_ ='';\n"+
				   "var str ='';\n"+
				   sql_pra1+
				   " s_='main?{encode:+configFile}={encode:"+root_dir+"/"+report_name_detail+"}'+\n"+	
				   sql_pra2+
                   con_dk2+				   
				   "window.open(s_,'mywindow');"+
				   "});";				  
		 aj_data+="</script>";
		
		
		writerFile(this.getSysConst("ConfigDir")+root_dir+"/"+report_name+".htm",aj_data);			
		writerFile(this.getSysConst("ConfigDir")+root_dir+"/"+report_sql+".htm",sql_);	
		writerFile(this.getSysConst("ConfigDir")+root_dir+"/"+report_name_detail+".htm",aj_content);
		writerFile(this.getSysConst("ConfigDir")+root_dir+"/"+report_ajax_data+".htm",str_form3);
		System.out.println("Tao file thanh cong");
		 String s= "declare n number; s varchar2(2000); out_ varchar2(200):='OK'; begin "+
			" begin "+
				"  select max(report_id)+1 into n from ccs_admin.report_file;"+			
			" s:='insert into ccs_admin.report_file (file_name,report_id,report_title,reportgroup_id,disable)"+
			" values(''"+root_dir+"/"+report_name+"'','||n||',''"+title+"'',12,0)' ; execute immediate s; commit;"+	
			" exception when others then "+
            " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "+			
			" end; ? :=n;  end;";		
		
		result_="";
		try{
			rs_ =this.reqValue("", s);	
			result_="ID cua bao cao:"+rs_;
			if(rs_!="")
			 {
			     s= "declare n number; s varchar2(2000); out_ varchar2(200):='OK'; begin "+
			" begin "+		
			" s:='insert into ccs_admin.report_file_access (role_id,report_id)"+
			" values(''admin'',''"+rs_+"'')' ; execute immediate s; commit;"+	
			" exception when others then "+
            " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "+			
			" end; ? :=out_;  end;";	
			 }
				rs_ =this.reqValue("", s);	
             				
            				
		} catch (Exception ex) {			
			ex.printStackTrace();
			result_="ERr:";
			
		}	
		}
		catch(Exception ex){
			return ex.getMessage();
		}
       return result_;		
		
  	}
  	/*--Tim kiem -----------------------------------------------------*/		
  	//Ham tao code html, jbiz va oracle
  	//TrinhHV 26/09/2011
 	public String autoGenericCode(String owner, String table1,String rootdir,String funcShema,String dataShema,
 			String hPackageOra,String file_jbiz,String smartcss,String sDataSrc){		
 		String table = table1.toLowerCase();				
		String[] smart_css = smartcss.split(",");
		String c_table=smart_css[0],c_grid=smart_css[1],c_button=smart_css[2],c_date=smart_css[3],c_text=smart_css[4];
		System.out.println("AutGen:Directory Exists");
		try{		
			String s = "begin ?:="+getSysConst("FuncSchema")+"gen_code.get_table_struct('"+owner+"','"+table.toUpperCase()+"'); end;";
			String s_pk = "begin ?:="+getSysConst("FuncSchema")+"gen_code.get_primary_key('"+owner+"','"+table.toUpperCase()+"'); end;";
			RowSet r = this.reqRSet("",s);	
			String id = this.reqValue("",s_pk).toLowerCase();
			String jbiz="",jbiz_new="",jbiz_s_new="",jbiz_edit="",jbiz_s_edit="",jbiz_del="",jbiz_s_del="";
			String sql="",sql_select="",sql_var="",sql_col="",sql_del="",sql_log_del="";
			String sql_edit="",sql_col_edit="",sql_par_edit="",sql_log="";
			String sql_new="",sql_par_new="",sql_bind_new="",sql_col_new="";
			String h="",h_var="",h_exec="",h_var_del="",h_exec_del="",h_var_edit="",h_exec_edit="",h_var_new="",h_exec_new="";
			String title_="",notnull="";
			String flabel="",aj_fill_data="";
			flabel+="vi="+id+";"+id+"\r\n";
			aj_fill_data += "		$(\"#"+table.toLowerCase()+" *[id="+id+"]\").val(\"{f01"+id+"}\");\n";
			String validate="";
			String css =	"{includep:layout/ccbs/style_migrate}\n";
			String form=	"<div style=\"width:99%\">\n"+
					"	<div style=\"width:100%;float:left\" id=\""+table.toLowerCase()+"\">\n"+
					"		<table border=0 cellpadding=2 cellspacing=0 style=\"width:100%;float:left\" align=left id=menuDetailTab class="+c_table+">\n"+
					"			<tr class="+c_grid+">\n" +
					"				<th colspan=4 align=center>CHI TI&#7870;T TH&Ocirc;NG TIN "+table.toUpperCase()+" </th>\n"+
					"			</tr>\n"+
					"		<tr>\n" +
					"			<td width=\"20%\" align=right>\n"+
					"			<label title=\""+title_+"\" id=\""+id+"_label\">{l0"+id+"} <font color=\"#FF0000\">*</font></label></td>\n"+
					"			<td width=\"30%\">	<input type=\"text\" style=\"width:80%\" name=\""+id+"\" class=\""+c_text+"\" id=\""+id+"\"  readonly=\"true\" disabled=\"true\">"+
					"			<span id=\""+id+"_msg\"></span>\n"+
					"			</td>\n";
			sql_var+="function search_"+table.toLowerCase()+"(\n"+"	ps_schema varchar2,\n";
			sql_edit="function edit_"+table.toLowerCase()+"(\n"+"	ps_schema varchar2,\n";
			sql_edit+="	ps_"+id+" varchar2,\n";
			sql_new="function new_"+table.toLowerCase()+"(\n"+"	ps_schema varchar2,\n";
			sql_del="function del_"+table.toLowerCase()+"(\n"+"	ps_schema varchar2,\n";
			sql_log_del="--logger.access('"+dataShema+table.toLowerCase()+"|DEL',";
			sql_col=" ";
			sql_col_edit="";
			sql_col_new="("+id+",";
			sql_par_edit=" using ";
			sql_par_new=" using vn_id,";
			sql_bind_new=" values (:"+id+",";
			sql_log="--logger.access('"+dataShema+table.toLowerCase()+"|SEARCH',";
			
			jbiz_new+=	"	public String new_"+table.toLowerCase()+"(\n"+"		String pschema,\n";
			jbiz_s_new+=	"			String s =\"begin ? :="+funcShema+hPackageOra+".new_"+table.toLowerCase()+"(\"\n";
			jbiz_s_new+=	"				+\"'\"+pschema+\"',\"\n";
			jbiz_edit+=	"	public String edit_"+table.toLowerCase()+"(\n"+"	String pschema,\n";
			jbiz_edit+=	"		String p"+id+",\n";
			jbiz_s_edit+=	"			String s =\"begin ? :="+funcShema+hPackageOra+".edit_"+table.toLowerCase()+"(\"\n";
			jbiz_s_edit+=	"				+\"'\"+pschema+\"',\"\n";
			jbiz_s_edit+=	"				+\"'\"+p"+id+"+\"',\"\n";
			jbiz_del+=	"	public String del_"+table.toLowerCase()+"(\n"+"		String pschema,\n";
			jbiz_s_del+=	"			String s =\"begin ? :="+funcShema+hPackageOra+".del_"+table.toLowerCase()+"(\"\n";
			jbiz_s_del+=	"				+\"'\"+pschema+\"',\"\n";
			h_var=	"	$(\"#"+table.toLowerCase()+" *[id='doSearch']\").click(function(){\n";
			h_exec=	"'main?{encode:+configFile}={encode:"+rootdir+"/ajax_"+table+"}&{encode:record_per_page}='+pageRec+'&{encode:page_index}='+pageNum";
			h_var_del=	"	$(\"#"+table.toLowerCase()+" *[id='doDelete']\").click(function(){\n";
			h_exec_del=	"		var exec='"+file_jbiz+".del_"+table.toLowerCase()+"(\""+dataShema+"\",";
			h_var_edit=	"	$(\"#"+table.toLowerCase()+" *[id='doUpdate']\").click(function(){\n";
			h_exec_edit=	"		var exec='"+file_jbiz+".edit_"+table.toLowerCase()+"(\""+dataShema+"\",";
			h_exec_edit+=	"\"'+Url.encode_1252($(\"#"+table.toLowerCase()+" *[id='"+id+"']\").val())+'\",";
			h_var_new=	"	$(\"#"+table.toLowerCase()+" *[id='doNew']\").click(function(){\n";
			h_exec_new=	"		var exec='"+file_jbiz+".new_"+table.toLowerCase()+"(\""+dataShema+"\",";
			
			String link_edit =	"'{encode:+configFile}={encode:"+rootdir+"/edit}'";
			String link_delete=  "'{encode:+configFile}={encode:"+rootdir+"/delete}'";
			
			String aj_data=	"<tr id=\"Data{seq}\" title=\"Click &#273;&#7875; hi&#7875;n th&#7883; th&ocirc;ng tin\">\n" +
							"	<td align=center name=seq>{seq}</td>\n" +
							"	<td align=center name='"+id+"'>&nbsp;{f01"+id+"}</td>\n";
			String aj_para="";
			String aj_header="		<th align=center>STT</th>\n";
					aj_header+="		<th align=center>{l0"+id+"}</th>\n";
			String aj="";
				
			String calendar="";
			String valid="";
			
			int count=0;
			
			while (r.next()){
				/*Form*/				
				if (r.getString("DATA_TYPE").equals("NUMBER") || r.getString("DATA_TYPE").equals("VARCHAR2") || r.getString("DATA_TYPE").equals("DATE") ||  r.getString("DATA_TYPE").equals("TEXT") ){
					title_="Nh&#7853;p nhi&#7873;u nh&#7845;t "+r.getString("DATA_LENGTH")+" k&yacute; t&#7921;";
				}else{
					title_="";
				}
				if (r.getString("CHECK_NULLABLE").equals("1")){
					notnull=" <font color=\"#FF0000\">*</font>";
				}else{
					notnull="";
				}
				/*Title cho cac phan plugins*/
				String plugin_hint="";
				if (r.getString("COLUMN_NAME").toLowerCase().indexOf("image")>=0){
					plugin_hint="title='Double Click for File Manager Plugin.'";
				}
				if(count%2==1)   form+= 	"		<tr>\n";
				if (r.getString("DATA_TYPE").equals("TEXT") && count%2==0) {
						form+="		</tr>\n		<tr>\n";
					}
				if (r.getString("DATA_TYPE").equals("TEXT") && count%2==1) {
					count++;
				}
				form+=	"			<td width=\"20%\" align=right>\n"+
						"			<label title=\""+title_+"\" id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_label\">{l0"+r.getString("COLUMN_NAME").toLowerCase()+"}"+notnull+"</label></td>\n";
				if (r.getString("DATA_TYPE").equals("NUMBER") || r.getString("DATA_TYPE").equals("VARCHAR2")){
				form+=	"			<td width=\"30%\">	<input "+plugin_hint+" type=\"text\" style=\"width:80%\" name=\""+r.getString("COLUMN_NAME").toLowerCase()+"\" class=\""+c_text+"\" id=\""+r.getString("COLUMN_NAME").toLowerCase()+"\">";
					sql_select += r.getString("COLUMN_NAME")+",";
				}else if (r.getString("DATA_TYPE").equals("DATE")){
				form+=	"			<td width=\"30%\">	<input "+plugin_hint+" type=\"text\" style=\"width:80%\" name=\""+r.getString("COLUMN_NAME").toLowerCase()+"\" class=\""+c_date+"\" id=\""+r.getString("COLUMN_NAME").toLowerCase()+"\">";
					sql_select += "to_char("+r.getString("COLUMN_NAME")+",''dd/mm/yyyy'') "+r.getString("COLUMN_NAME")+",";
				}else if (r.getString("DATA_TYPE").equals("TEXT")){
				form+=	"			<td width=\"30%\" colspan=3>	<textarea title='Double Click for Editor Plugin.' rows=2 class=\"ctext\" name=\""+r.getString("COLUMN_NAME").toLowerCase()+"\" id=\""+r.getString("COLUMN_NAME").toLowerCase()+"\" style='vertical-align:top;width:92%'></textarea>";
					sql_select += r.getString("COLUMN_NAME")+",";
				}else if (r.getString("DATA_TYPE").equals("SELECT")){
					sql_select += r.getString("COLUMN_NAME")+",";
					form+=	"		<td width=\"30%\" colspan=3>	<select name=\"--"+r.getString("COLUMN_NAME").toLowerCase()+"\" style=\"width:80%\" class=\""+c_text+"\" id=\""+r.getString("COLUMN_NAME").toLowerCase()+"\" sql=\""+r.getString("RELATED")+"\" data-src=\""+sDataSrc+"\"  emptyOption=\"\" dbms-font=\"VN8VN3\"></select>";
				}
				form+=	"			<span id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"></span>\n"+
						"			</td>\n";
				if(count%2==0)
					form+= 	"		</tr>\n";
											
				if (r.getString("DATA_TYPE").equals("DATE")){
					calendar+=	"		$(\"#"+table.toLowerCase()+" *[id='"+r.getString("COLUMN_NAME").toLowerCase()+"']\").datepicker({dateFormat:\"dd/mm/yy\"});\n";
					valid+=	"		$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"\"]').blur(function(){\n";
					valid+=	"			if ($(this).val()=='' || patternValidate($(this),'dd/mm/yyyy')){\n"+
							"				$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"]').html('');\n"+
							"				$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"]').attr('style','display:none');\n"+								
							"			}else{\n"+
							"				$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"]').html('{l0"+r.getString("COLUMN_NAME").toLowerCase()+"} d&#7841;ng dd/mm/yyyy');\n"+
							"				$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"]').attr('style','display:block;color:#FF0000');\n"+								
							"				$(this).focus();\n"+
							"				return false;\n"+								
							"			}\n";					
					valid+=		"		});\n";	
				}else if (r.getString("DATA_TYPE").equals("NUMBER")){
					valid+=	"		$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"\"]').blur(function(){\n";
					valid+=	"			if ($(this).val()*1==$(this).val()){\n"+
							"				$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"]').html('');\n"+
							"				$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"]').attr('style','display:none');\n"+
							"			}else{\n"+
							"				$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"]').html('{l0"+r.getString("COLUMN_NAME").toLowerCase()+"} c&oacute; d&#7841;ng s&#7889;');\n"+
							"				$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"]').attr('style','display:block;color:#FF0000');\n"+								
							"				$(this).focus();\n"+
							"				return false;\n"+
							"			}\n";					
					valid+=	"		});\n";
				}else if(r.getString("DATA_TYPE").equals("VARCHAR2")){
					valid+=	"		$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"\"]').change(function(){\n";
					valid+=	"			if ($(this).val().length<="+r.getString("DATA_LENGTH")+"){\n"+
							"				$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"]').html('');\n"+
							"				$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"]').attr('style','display:none');\n"+
							"			}else{\n"+
							"				$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"]').html('{l0"+r.getString("COLUMN_NAME").toLowerCase()+"} kh&ocirc;ng &#273;&#432;&#7907;c ph&eacute;p nh&#7853;p qu&aacute; "+r.getString("DATA_LENGTH")+" k&iacute; t&#7921;');\n"+
							"				$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"]').attr('style','display:block;color:#FF0000');\n"+								
							"				$(this).focus();\n"+
							"				return false;\n"+
							"			}\n";					
					valid+=	"		});\n";
				}
				if (r.getString("CHECK_NULLABLE").equals("1")){
					validate += "		if($('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"\"]').val()!=''){\n"+
								"			$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"]').html('');\n"+
								"			$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"]').attr('style','display:none');\n"+	
								"		}else{\n"+
								"			$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"]').html('{l0"+r.getString("COLUMN_NAME").toLowerCase()+"} kh&ocirc;ng &#273;&#432;&#7907;c b&#7887; tr&#7889;ng');\n"+
								"			$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"_msg\"]').attr('style','display:block;color:#FF0000');\n"+								
								"			$(this).focus();\n"+
								"			return false;\n"+
								"		}\n";
				}
				/*plugins editor, file*/
				if (r.getString("DATA_TYPE").equals("TEXT")){
					valid+=	"		$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"\"]').dblclick(function(){\n"+
							"			_popup(\"main?{encode:+configFile}={encode:plugins/editor/index}&{encode:+modal}={encode:1}&{encode:+source}={encode:#"+table.toLowerCase()+" [id="+r.getString("COLUMN_NAME").toLowerCase()+"]}&{encode:content}=\"+encodeW1252($('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"\"]').val()),'selectFile',700,280);\n"+
							"		});\n"+
							"		//$.ctrl('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"\"]','1', function(){$(this).dblclick();});\n";
				}else if (r.getString("COLUMN_NAME").toLowerCase().indexOf("image")>=0){
					valid+=	"		$('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"\"]').dblclick(function(){\n"+
							"			_popup(\"main?{encode:+configFile}={encode:file_manager/index}&{encode:+modal}={encode:1}&{encode:+source}={encode:#"+table.toLowerCase()+" [id="+r.getString("COLUMN_NAME").toLowerCase()+"]}\",'selectFile',760,420);\n"+
							"		});\n"+
							"		//$.ctrl('#"+table.toLowerCase()+" *[id=\""+r.getString("COLUMN_NAME").toLowerCase()+"\"]','1', function(){$(this).dblclick();});\n";
				}				
				
				/*Label*/
				flabel+="vi="+r.getString("COLUMN_NAME").toLowerCase()+";"+r.getString("COLUMN_NAME").toLowerCase()+"\r\n";
				
				/*sql*/				
				sql_var+="	ps_"+r.getString("COLUMN_NAME").toLowerCase()+" varchar2,\n";
				sql_edit+="	ps_"+r.getString("COLUMN_NAME").toLowerCase()+" varchar2,\n";
				sql_new+="	ps_"+r.getString("COLUMN_NAME").toLowerCase()+" varchar2,\n";
				sql_col_new+=r.getString("COLUMN_NAME").toLowerCase()+",";
				sql_col_edit+=r.getString("COLUMN_NAME").toLowerCase()+"=:"+r.getString("COLUMN_NAME").toLowerCase()+",";
				sql_bind_new+=":"+r.getString("COLUMN_NAME").toLowerCase()+",";
				if (r.getString("DATA_TYPE").equals("DATE")){
					sql_col+=	"	if ps_"+r.getString("COLUMN_NAME").toLowerCase()+" is not null then s:=s||' and "+r.getString("COLUMN_NAME").toLowerCase()+"=to_date('''||ps_"+r.getString("COLUMN_NAME").toLowerCase()+"||''',''dd/mm/yyyy'')'; end if;\n";				
					sql_par_edit+="to_date(ps_"+r.getString("COLUMN_NAME").toLowerCase()+",'dd/mm/yyyy'),";
					sql_par_new+="to_date(ps_"+r.getString("COLUMN_NAME").toLowerCase()+",'dd/mm/yyyy'),";
				}else{
					sql_col+=	"	if ps_"+r.getString("COLUMN_NAME").toLowerCase()+" is not null then s:=s||' and "+r.getString("COLUMN_NAME").toLowerCase()+" like ''%'||ps_"+r.getString("COLUMN_NAME").toLowerCase()+"||'%'''; end if;\n";				
					sql_par_edit+="ps_"+r.getString("COLUMN_NAME").toLowerCase()+",";
					sql_par_new+="ps_"+r.getString("COLUMN_NAME").toLowerCase()+",";
				}
				sql_log+="ps_"+r.getString("COLUMN_NAME").toLowerCase()+"||'|'||";
				
				/*jbiz loop*/
				jbiz_new+=	"		String p"+r.getString("COLUMN_NAME").toLowerCase()+",\n";
				jbiz_edit+=	"		String p"+r.getString("COLUMN_NAME").toLowerCase()+",\n";
				if (r.getString("DATA_TYPE").equals("TEXT")||r.getString("DATA_TYPE").equals("VARCHAR2")){
					jbiz_s_new+=	"				+\"'\"+ConvertFont.UtoD(p"+r.getString("COLUMN_NAME").toLowerCase()+")+\"',\"\n";				
					jbiz_s_edit+=	"				+\"'\"+ConvertFont.UtoD(p"+r.getString("COLUMN_NAME").toLowerCase()+")+\"',\"\n";				
				}else{
					jbiz_s_new+=	"				+\"'\"+p"+r.getString("COLUMN_NAME").toLowerCase()+"+\"',\"\n";				
					jbiz_s_edit+=	"				+\"'\"+p"+r.getString("COLUMN_NAME").toLowerCase()+"+\"',\"\n";				
				}
					
				/*html loop*/
				if (r.getString("DATA_TYPE").equals("TEXT")){
					h_exec+=	"+'&{encode:"+r.getString("COLUMN_NAME").toLowerCase()+"}='+encodeW1252($(\"#"+table.toLowerCase()+" *[id='"+r.getString("COLUMN_NAME").toLowerCase()+"']\").val())";
					h_exec_edit+=	"\"'+encodeW1252($(\"#"+table.toLowerCase()+" *[id='"+r.getString("COLUMN_NAME").toLowerCase()+"']\").val())+'\",";
					h_exec_new+=	"\"'+encodeW1252($(\"#"+table.toLowerCase()+" *[id='"+r.getString("COLUMN_NAME").toLowerCase()+"']\").val())+'\",";	
					aj_fill_data += "		$(\"#"+table.toLowerCase()+" *[id="+r.getString("COLUMN_NAME").toLowerCase()+"]\").val(Url.decode_1252(\"{f01"+r.getString("COLUMN_NAME").toLowerCase()+"}\"));\n";
				}
				else if (r.getString("DATA_TYPE").equals("VARCHAR2")){
					h_exec+=	"+'&{encode:"+r.getString("COLUMN_NAME").toLowerCase()+"}='+Url.encode_1252($(\"#"+table.toLowerCase()+" *[id='"+r.getString("COLUMN_NAME").toLowerCase()+"']\").val())";					
					h_exec_edit+=	"\"'+Url.encode_1252($(\"#"+table.toLowerCase()+" *[id='"+r.getString("COLUMN_NAME").toLowerCase()+"']\").val())+'\",";
					h_exec_new+=	"\"'+Url.encode_1252($(\"#"+table.toLowerCase()+" *[id='"+r.getString("COLUMN_NAME").toLowerCase()+"']\").val())+'\",";
					aj_fill_data += "		$(\"#"+table.toLowerCase()+" *[id="+r.getString("COLUMN_NAME").toLowerCase()+"]\").val(Url.decode_1252(\"{f01"+r.getString("COLUMN_NAME").toLowerCase()+"}\"));\n";
				}else{				
					h_exec+=	"+'&{encode:"+r.getString("COLUMN_NAME").toLowerCase()+"}='+$(\"#"+table.toLowerCase()+" *[id='"+r.getString("COLUMN_NAME").toLowerCase()+"']\").val()";					
					h_exec_edit+=	"\"'+$(\"#"+table.toLowerCase()+" *[id='"+r.getString("COLUMN_NAME").toLowerCase()+"']\").val()+'\",";
					h_exec_new+=	"\"'+$(\"#"+table.toLowerCase()+" *[id='"+r.getString("COLUMN_NAME").toLowerCase()+"']\").val()+'\",";
					aj_fill_data += "		$(\"#"+table.toLowerCase()+" *[id="+r.getString("COLUMN_NAME").toLowerCase()+"]\").val(\"{f01"+r.getString("COLUMN_NAME").toLowerCase()+"}\");\n";
				}
				if (r.getString("COLUMN_NAME").toLowerCase().equals("id")){
					link_edit+=	"+'&{encode:"+r.getString("COLUMN_NAME").toLowerCase()+"}='+$('#"+table.toLowerCase()+" :radio:checked').parent().parent().children(\"[name='id']\").text()";
					link_delete+=	"+'&{encode:"+r.getString("COLUMN_NAME").toLowerCase()+"}='+$('#"+table.toLowerCase()+" :radio:checked').parent().parent().children(\"[name='id']\").text()";
				}
				
				/*Ajax data*/
				if (r.getString("DATA_TYPE").equals("NUMBER")||r.getString("DATA_TYPE").equals("DATE")){
					aj_data+=	"	<td align=center name='"+r.getString("COLUMN_NAME").toLowerCase()+"'>&nbsp;{f01"+r.getString("COLUMN_NAME").toLowerCase()+"}</td>\n";	
				}else{
					aj_data+=	"	<td align=center name='"+r.getString("COLUMN_NAME").toLowerCase()+"'>&nbsp;{f01"+r.getString("COLUMN_NAME").toLowerCase()+"}</td>\n";
				}
				aj_header+=	"		<th align=center>{l0"+r.getString("COLUMN_NAME").toLowerCase()+"}</th>\n";
				aj_para+=	"'{p0"+r.getString("COLUMN_NAME").toLowerCase()+"}',";
				count++;
			}
			aj_data+=	"	<td align=center><span id=\"Delete{seq}\"  title=\"Delete\" style=\"display:inline-block;width:18px;height:18px\" class=\"cicon ui-icon-trash\"></span></td>\n</tr>\n";	
			aj_data+="<script>\n" +
			"	$(document).ready(function(){\n" +
			"		$(\"#data tr:even\").css(\"background-color\", \"#EEEEEE\");\n" +
			"		$(\"#data tr:odd\").css(\"background-color\", \"#ffffff\");\n" +
			"	$(\"#Data{seq}\").click(function(){\n" +
			"		$(\"#data tr:even\").css(\"background-color\", \"#EEEEEE\");\n" +
			"		$(\"#data tr:odd\").css(\"background-color\", \"#ffffff\");\n" +
			aj_fill_data+
			"		//$(\"#"+table.toLowerCase()+" *[id='doNew']\").attr(\"disabled\",\"disabled\");\n"+
			"		$(\"#"+table.toLowerCase()+" *[id='doSearch']\").attr(\"disabled\",\"disabled\");\n"+
			"		$(\"#"+table.toLowerCase()+" *[id='doUpdate']\").removeAttr(\"disabled\");\n"+
			"		$(\"#"+table.toLowerCase()+" *[id='doDelete']\").removeAttr(\"disabled\");\n"+
			"		$(this).css(\"background-color\", \"#FBE0AA\");\n" +
			"	});\n" +
			"	$(\"#Delete{seq}\").click(function(){\n"+
			"			if( !confirm('Tiep tuc thuc hien?') ){return;}\n"+
			"	"+h_exec_del+"\"{f01"+id+"}\",\"{p0sys_userid}\",\"{p0sys_userip}\")';\n"+
			"			$.ajax({\n"+
			"				url: 'main?{encode:+configFile}={encode:admin/jfunction}&{encode:jfunction}='+exec,\n"+
			"				success: function(data){ \n"+
			"					if (data==\"1\"){\n"+
			"						_inform(\"inform\",\"X�a d&#7919; li&#7879;u th�nh c�ng!\"); \n"+
			"						$(\"#"+table.toLowerCase()+" *[id='doNew']\").removeAttr(\"disabled\");\n"+
			"						$(\"#"+table.toLowerCase()+" *[id='doSearch']\").removeAttr(\"disabled\");\n"+
			"						$(\"#"+table.toLowerCase()+" *[id='doUpdate']\").attr(\"disabled\",\"disabled\");\n"+
			"						$(\"#"+table.toLowerCase()+" *[id='doDelete']\").attr(\"disabled\",\"disabled\");\n"+
			"						$(\"#"+table.toLowerCase()+" input[type='text'],textarea\").val('');\n"+
			"						getData(1,$(\"#cboSetPageRec\").val());\n"+
			"					}else{\n"+
			"						_alert(\"inform\",data);\n"+
			"					}\n"+
			"				}\n"+
			"			});\n"+
			"		});\n"+
			"});\n" +
			"</script>";
			form+=	"		<tr>\n" +
					"			<td colspan=4 align=center>\n"+
					"				<INPUT type=button style='width:100px' value=\"T&igrave;m ki&#7871;m\" class=\""+c_button+"\" name=\"doSearch\" id=\"doSearch\"/>&nbsp;\n"+					
					"				<INPUT type=button style='width:100px' value=\"Th&ecirc;m m&#7899;i &gt;\" class=\""+c_button+"\" name=\"doNew\" id=\"doNew\"/>&nbsp;\n"+	
					"				<INPUT type=button style='width:100px' value=\"C&#7853;p nh&#7853;t &gt;\" class=\""+c_button+"\" name=\"doUpdate\" id=\"doUpdate\"/>&nbsp;\n"+
					"				<INPUT type=button style='width:100px' value=\"X&oacute;a d&#7919; li&#7879;u &gt;\" class=\""+c_button+"\" name=\"doDelete\" id=\"doDelete\"/>&nbsp;\n"+				
					"			</td>\n" +
					"		</tr>\n"+
					"	</table>\n" +
					"	<div id=\"inform\" style=\"width:95%;float:left\">\n" +
					"		</div>\n";	
			form+= "<table style=\"width:100%;float:left\" align=center cellpadding='0' cellspacing='1' bgcolor=\"#FFFFFF\" class='"+c_table+"' >\n"+
				   "	<tr width=\"100%\" class='"+c_grid+"'>\n"+
				   "		<th align=center colspan=7>DANH S&Aacute;CH "+table.toUpperCase()+"</th>\n"+
				   "	</tr>\n" +
				   "</table>\n" +
				   "</div>\n";
			form+=	"	<div id=\"data\" style=\"width:100%;float:left;height:255;overflow:auto\">\n" +
					"		</div>\n" +
					"{includef:admin/PageRec}\n" +
					"</div>\n";
			/*join sql*/
			sql_var+=	"	ps_Type varchar2,\n"+
						"	ps_RecordPerPage varchar2,\n"+
						"	ps_PageIndex varchar2,\n"+
						"	ps_UserId varchar2,\n"+
						"	ps_UserIp varchar2)\n";
			sql_edit +=	"	ps_UserId varchar2,\n"+
						"	ps_UserIp varchar2)\n";
			sql_new	+=	"	ps_UserId varchar2,\n"+
						"	ps_UserIp varchar2)\n";
			sql_del	+=	"	ps_"+id+" varchar2,\n"+
						"	ps_UserId varchar2,\n"+
						"	ps_UserIp varchar2)\n";
			sql="PACKAGE BODY "+hPackageOra.toUpperCase()+"\nIS\n"+sql_var+"return sys_refcursor\n" +
			"is\n" +
			"	s varchar2(1000);\n" +
			"	ref_ sys_refcursor;\n" +
			"begin\n"+
			"	"+sql_log.substring(0,sql_log.length()-7)+");\n"+
			"	s:='select "+id.toUpperCase()+","+sql_select.substring(0, sql_select.length()-1)+" from '||ps_schema||'"+table.toLowerCase() +" where 1=1';\n"+ sql_col+ 
			"	s:=s||' order by "+id.toUpperCase()+" asc';\n"+
			"	if ps_Type='1' then\n"+
			"		open ref_ for util.xuly_phantrang(s,ps_PageIndex,ps_RecordPerPage);\n"+
			"		return ref_;\n"+
			"	end if;\n"+
			"	if ps_Type='2' then\n"+
			"		return util.GetTotalRecord(s,ps_PageIndex,ps_RecordPerPage);\n"+
			"	end if;\n"+
			"	exception when others then\n"+
			"		declare	err varchar2(500);\n" +
			" 		begin	\n" +
			"			err:='Loi thuc hien, ma loi:'||to_char(sqlerrm);\n" +
			"			open ref_ for 'select :1 err from dual' using err;\n" +
			"		return ref_;\n" +
			"	end;\n"+
			"end;\n";
			sql+=sql_edit+"return varchar2\n" +
			"is\n" +
			"	s varchar2(1000);\n" +
			"begin\n"+
			"	"+sql_log.substring(0,sql_log.length()-7)+");\n"+
			"	s:='update '||ps_schema||'"+table.toLowerCase()+ " set " + sql_col_edit.substring(0,sql_col_edit.length()-1)+" where "+id+"=:"+id+"';\n" +
			"	execute immediate s"+sql_par_edit.substring(0,sql_par_edit.length()-1)+",ps_"+id+";\n"+
			"	commit;\n" +
			"	return '1';\n"+
			"	exception when others then\n"+
			"		declare	err varchar2(500);\n" +
			"		begin	\n" +
			"				err:='Loi thuc hien, ma loi:'||to_char(sqlerrm); \n" +
			"				return err;\n" +
			"		end;\n"+
			"end;\n";
			sql += sql_new + "return varchar2\n" +
			"is\n " +
			"	s varchar2(1000);\n " +
			"	vn_id number;\n" +
			"begin\n " +
			"	vn_id := admin_v2.GETSEQ('"+table.toUpperCase()+"_SEQ');\n"+
			"	"+sql_log.substring(0,sql_log.length()-7)+");\n"+
			"	s:='insert into '||ps_schema||'"+table.toLowerCase()+ sql_col_new.substring(0,sql_col_new.length()-1)+")\n"+
			"	"+sql_bind_new.substring(0,sql_bind_new.length()-1)+")';\n"+
			"	execute immediate s"+sql_par_new.substring(0,sql_par_new.length()-1)+";\n"+
			"	commit;\n" +
			"	return '1';\n"+
			"	exception when others then\n"+
			"		declare	err varchar2(500);\n" +
			"		begin\n" +
			"			err:='Loi thuc hien, ma loi:'||to_char(sqlerrm);\n" +
			"			return err;\n" +
			"		end;\n"+
			"end;\n";
			sql+=sql_del+"return varchar2\n" +
			"is\n" +
			"	s varchar2(1000);\n" +
			"begin\n"+
			"	"+sql_log_del+"pId);\n"+
			"	s:='delete from '||ps_schema||'"+table.toLowerCase()+ " where "+id+"=:"+id+"';\n" +
			"	execute immediate s using ps_"+id+";\n"+
			"	commit;\n" +
			"	return '1';\n"+
			"	exception when others then\n"+
			"		declare	err varchar2(500);\n" +
			"	begin	\n" +
			"		err:='Loi thuc hien, ma loi:'||to_char(sqlerrm);\n" +
			"		return err;\n" +
			"	end;\n"+
			"end;\n";
			sql+="END;\n";
			/*Jbiz join*/
			jbiz="package "+file_jbiz.substring(0,file_jbiz.lastIndexOf('.'))+";\n\n" +
					"import neo.smartui.process.*;\n" +
					"import neo.smartui.common.*;\n\n" +
					"public class "+file_jbiz.substring(file_jbiz.lastIndexOf('.')+1)+" extends NEOProcessEx {\n";
			
			jbiz+=jbiz_new+
					"		String puserid,\n"+
					"		String puserip) throws Exception {\n"+
				jbiz_s_new+
					"				+\"'\"+puserid+\"',\"\n"+
					"				+\"'\"+puserid+\"'\"\n"+
					"				+\"); end;\";\n"+
					"		System.out.println(s);\n"+
					"       return this.reqValue(\""+sDataSrc+"\", s);\n"+					
					"	}\n";
			jbiz+=	jbiz_del+
					"		String pid,\n"+
					"		String puserid,\n"+
					"		String puserip) throws Exception {\n"+
					jbiz_s_del+
					"				+\"'\"+pid+\"',\"\n"+
					"				+\"'\"+puserid+\"',\"\n"+
					"				+\"'\"+puserid+\"'\"\n"+
					"				+\"); end;\";\n"+
					"		System.out.println(s);\n"+
					"		return this.reqValue(\""+sDataSrc+"\", s);\n"+
					"	}\n";
			jbiz+=jbiz_edit+
					"		String puserid,\n"+
					"		String puserip) throws Exception {\n"+
					jbiz_s_edit+
					"				+\"'\"+puserid+\"',\"\n"+
					"				+\"'\"+puserid+\"'\"\n"+
					"				+\"); end;\";\n"+
					"		System.out.println(s);\n"+
					"		return this.reqValue(\""+sDataSrc+"\", s);\n"+
					"	}\n";
			jbiz+="}";
			/*html join*/
			h=	"<script type=\"text/javascript\">\n"+
				"	$(function(){\n"+				
				"	"+h_var+
				"			getData(1,$(\"#cboSetPageRec\").val());\n"+
				"		});\n"+
				"	"+h_var_del+
				"			if( !confirm('Tiep tuc thuc hien?') ){return;}\n"+
				"			$(\"#inform\").html('');\n" +
				"	"+h_exec_del+"\"'+$(\"#"+table.toLowerCase()+" *[id='"+id+"']\").val()+'\",\"{p0sys_userid}\",\"{p0sys_userip}\")';\n"+
				"			$.ajax({\n"+
				"				url: 'main?{encode:+configFile}={encode:admin/jfunction}&{encode:jfunction}='+exec,\n"+
				"				cache: false,\n"+
				"				success: function(data){ \n"+
				"					if (data==\"1\"){\n"+
				"						_inform(\"inform\",\"X�a d&#7919; li&#7879;u th�nh c�ng!\"); \n"+
				"						$(\"#"+table.toLowerCase()+" *[id='doNew']\").removeAttr(\"disabled\");\n"+
				"						$(\"#"+table.toLowerCase()+" *[id='doSearch']\").removeAttr(\"disabled\");\n"+
				"						$(\"#"+table.toLowerCase()+" *[id='doUpdate']\").attr(\"disabled\",\"disabled\");\n"+
				"						$(\"#"+table.toLowerCase()+" *[id='doDelete']\").attr(\"disabled\",\"disabled\");\n"+
				"						$(\"#"+table.toLowerCase()+" input[type='text'],textarea\").val('');\n"+
				"						getData(1,$(\"#cboSetPageRec\").val());\n"+
				"					}else{\n"+
				"						_alert(\"inform\",data);\n"+
				"					}\n"+
				"				}\n"+
				"			});\n"+
				"		});\n"+
				"	"+h_var_edit+
				"			if(checkValue()!=''){\n" +
				"			_alert(\"inform\",\"Y&#234;u c&#7847;u nh&#7853;p &#273;&#250;ng &#273;&#7883;nh d&#7841;ng c&#225;c tr&#432;&#7901;ng th&#244;ng tin!\");return;\n" +
				"			}\n" +
				"			if( !confirm('Tiep tuc thuc hien?') ){return;}\n"+
				"			$(\"#inform\").html('');\n" 
				+validate+
				"	"+h_exec_edit+"\"{p0sys_userid}\",\"{p0sys_userip}\")';\n"+
				"			$.ajax({\n"+
				"				url: 'main?{encode:+configFile}={encode:admin/jfunction}&{encode:jfunction}='+exec,\n"+
				"				cache: false,\n"+
				"				success: function(data){ \n"+
				"					if (data==\"1\"){\n"+
				"						_inform(\"inform\",\"C&#7853;p nh&#7853;t d&#7919; li&#7879;u th�nh c�ng!\"); \n"+
				"						$(\"#"+table.toLowerCase()+" *[id='doNew']\").removeAttr(\"disabled\");\n"+
				"						$(\"#"+table.toLowerCase()+" *[id='doSearch']\").removeAttr(\"disabled\");\n"+
				"						$(\"#"+table.toLowerCase()+" *[id='doUpdate']\").attr(\"disabled\",\"disabled\");\n"+
				"						$(\"#"+table.toLowerCase()+" *[id='doDelete']\").attr(\"disabled\",\"disabled\");\n"+
				"						$(\"#"+table.toLowerCase()+" input[type='text'],textarea\").val('');\n"+
				"						getData(1,$(\"#cboSetPageRec\").val());\n"+
				"					}else{\n"+
				"						_alert(\"inform\",data);\n"+
				"					}\n"+
				"				}\n"+
				"			});\n"+
				"		});\n"+
				"	"+h_var_new+
				"			if(checkValue()!=''){\n" +
				"			_alert(\"inform\",\"Y&#234;u c&#7847;u nh&#7853;p &#273;&#250;ng &#273;&#7883;nh d&#7841;ng c&#225;c tr&#432;&#7901;ng th&#244;ng tin!\");return;\n" +
				"			}\n" +
				"			if( !confirm('Tiep tuc thuc hien?') ){return;}\n"+validate+
				"			$(\"#inform\").html('');\n" +
				"	"+h_exec_new+"\"{p0sys_userid}\",\"{p0sys_userip}\")';\n"+
				"			$.ajax({\n"+
				"				url: 'main?{encode:+configFile}={encode:admin/jfunction}&{encode:jfunction}='+exec,\n"+
				"				cache: false,\n"+
				"				success: function(data){ \n"+
				"					if (data==\"1\"){\n"+
				"						_inform(\"inform\",\"Th&ecirc;m m&#7899;i d&#7919; li&#7879;u th�nh c�ng!\"); \n"+
				"						$(\"#"+table.toLowerCase()+" input[type='text'],textarea\").val('');\n"+
				"						getData(1,$(\"#cboSetPageRec\").val());\n"+
				"					}else{\n"+
				"						_alert(\"inform\",data);\n"+
				"					}\n"+
				"				}\n"+
				"			});\n"+
				"		});\n"+valid+
				"	});\n"+
				"	$(document).ready(function(){\n"+calendar+
				"		$(\"#"+table.toLowerCase()+" *[id='doDelete']\").attr(\"disabled\",\"disabled\");\n"+
				"		$(\"#"+table.toLowerCase()+" *[id='doUpdate']\").attr(\"disabled\",\"disabled\");\n"+
				"		getData(1,$(\"#cboSetPageRec\").val());\n"+
				"	});\n"+	
				"	function checkValue(){\n"+
				"			var s='';\n"+
				"			$(\"#"+table.toLowerCase()+" span\").each(function(index, element) {\n"+
				"				s+=$(\"#"+table.toLowerCase()+" span\")[index].innerHTML;\n"+
				"				}); \n"+
				"				return s;\n"+								
				"	}\n"+
				"	function getData(pageNum,pageRec){\n"+
				"			$.ajax({\n"+
				"				url: "+h_exec+",\n"+
				"				cache: false,\n"+
				"				success: function(data){ \n"+
				"					$(\"#data\").html(data);\n"+
				"					setPage(pageNum,$(\"#page\").html());\n"+
				"				}\n"+
				"			});\n"+
				"		}\n"+
				"</script>";
				
			/*Ajax files*/
			aj_header += "		<th align=center>Action</th>\n";
			aj =	"<div>\n<meta sql=\"begin ?:= "+funcShema+hPackageOra+".search_"+table+"('"+dataShema+"',"+aj_para+"'1','{p0record_per_page}','{p0page_index}','{p0sys_userid}','{p0sys_userip}') ;end;\" name=\"ajax_"+table+"\" data-src=\""+sDataSrc+"\" rec-per-page=\"500\" dbms-font=\"VN8VN3\" run=\"1\">\n"+
					"<meta sql=\"begin ?:= "+funcShema+hPackageOra+".search_"+table+"('"+dataShema+"',"+aj_para+"'2','{p0record_per_page}','{p0page_index}','{p0sys_userid}','{p0sys_userip}') ;end;\" name=\"ajax_"+table+"\" data-src=\""+sDataSrc+"\" rec-per-page=\"500\" dbms-font=\"VN8VN3\" run=\"1\">\n"+
					"<table width=\"99%\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" class=\""+c_table+"\" id=\"ajax_"+table+"\">\n"+
					"	<thead>\n	<tr>\n"+aj_header+
					"	</tr>\n"+
					"	</thead><tr sql-index=\"1\" td-value=\"{includep:"+rootdir+"/ajax_"+table+"_data}\">\n"+
					"</table>\n"+
					"</div>\n"+
					"<script>\n"+
					"	$(document).ready(function(){\n"+
					"		$(\"#total\").html('{F02record}');\n"+
					"		$(\"#page\").html(parseInt($(\"#total\").html()/$(\"#cboSetPageRec\").val())+1);\n"+
					"	});\n"+
					"</script>\n";
			
			/*Write all file*/
			try{
				boolean dir = new File(this.getSysConst("ConfigDir")+"/"+rootdir+"/bak/").mkdirs();				
			}catch(Exception ex){
				System.out.println("AutGen:Directory Exists");
			}
			writerFile(this.getSysConst("ConfigDir")+rootdir+"/index_label.txt",flabel);
			writerFile(this.getSysConst("ConfigDir")+rootdir+"/ajax_"+table+"_label.txt",flabel);
			writerFile(this.getSysConst("ConfigDir")+rootdir+"/"+hPackageOra+".sql",sql);			
			writerFile(this.getSysConst("ConfigDir")+rootdir+"/index.htm",css+form+"\n"+h);
			writerFile(this.getSysConst("ConfigDir")+rootdir+"/"+file_jbiz.substring(file_jbiz.lastIndexOf('.')+1)+".java",jbiz);	
			writerFile(this.getSysConst("ConfigDir")+rootdir+"/ajax_"+table+".htm",aj);
			writerFile(this.getSysConst("ConfigDir")+rootdir+"/ajax_"+table+"_data.htm",aj_data);
		}catch(Exception ex){
			return ex.getMessage();
		}
		return "1";
	}
}