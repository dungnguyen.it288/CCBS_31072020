package neo.smartui;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import java.util.*;
import javax.sql.*;
import javax.sql.*;
import java.util.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.io.*;
import java.util.*;
import java.util.regex.*;

import neo.smartui.common.CesarCode;

public class block extends NEOProcessEx 
{	
	public String capnhat_blockip(
		String userip,
		String id,
		String rulename,
		String isblock,
		String agentcode,
		String donvi_id,
		String mabc,
		String fromip,
		String toip,
		String user,
		String priority,
		String status,
		String act) throws Exception {									
			
			String s ="begin ? :="+getSysConst("FuncSchema")+"pkg_fwrules.capnhat_blockip("
				+"'"+getUserVar("userID")+"',"
				+"'"+userip+"',"
				+"'"+getUserVar("sys_dataschema")+"',"
				+"'"+getUserVar("sys_agentcode")+"',"
				+"'"+id+"',"
				+"'"+rulename+"',"
				+"'"+isblock+"',"
				+"'"+agentcode+"',"
				+"'"+donvi_id+"',"
				+"'"+mabc+"',"
				+"'"+fromip+"',"
				+"'"+toip+"',"
				+"'"+user+"',"
				+"'"+priority+"',"
				+"'"+status+"',"
				+"'"+act+"'"
				+"); end;";
		return this.reqValue("", s);
	}
	public String xoa_blockip(
		String userip,
		String id) throws Exception {
			String s ="begin ? :="+getSysConst("FuncSchema")+"pkg_fwrules.xoa_blockip("
				+"'"+getUserVar("userID")+"',"
				+"'"+userip+"',"
				+"'"+getUserVar("sys_dataschema")+"',"
				+id
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String check_ip_isblock(
		String userip,
		String agent,
		String donvi_id,
		String ma_bc,
		String user,
		String ipcheck) throws Exception {
			String s ="begin ? :="+getSysConst("FuncSchema")+"pkg_fwrules.check_ip_isblock("
				+"'"+getUserVar("userID")+"',"
				+"'"+userip+"',"
				+"'"+getUserVar("sys_dataschema")+"',"
				+"'"+agent+"',"
				+"'"+donvi_id+"',"
				+"'"+ma_bc+"',"
				+"'"+user+"',"
				+"'"+ipcheck+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
}
	