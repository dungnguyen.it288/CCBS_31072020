package neo.smartui;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import javax.sql.*;
import java.util.*;
import java.io.*;
import java.text.*;

public class customer extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("Portal");	
		//funcschema=getSysConst("FuncSchema");
    	//dataschema=getUserVar("sys_dataschema");
    	//userid=getUserVar("userID");	
	}
	
	public String register(
		String pCustomerId,
		String pFistName,
		String pMiddleName,
		String pLastName,
		String pEmail,
		String pMobile,
		String pPassword,
		String pUserIp) throws Exception {
			String s ="begin ? :="+getSysConst("FuncSchema")+"customer.register("
				+"'"+pCustomerId+"',"
				+"'"+ConvertFont.UtoD(pFistName)+"',"
				+"'"+ConvertFont.UtoD(pMiddleName)+"',"
				+"'"+ConvertFont.UtoD(pLastName)+"',"
				+"'"+ConvertFont.UtoD(pEmail)+"',"
				+"'"+pMobile+"',"
				+"'"+  CesarCode.getMd5(CesarCode.getMd5(pPassword))+"',"
				+"'"+pUserIp+"'"				
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String edit_customer_account(
		String pCustomerId,		
		String pEmail,
		String pMobile,
		String pOldpassword,
		String pPassword,
		String pUserIp) throws Exception {
			String s ="begin ? :="+getSysConst("FuncSchema")+"customer.edit_customer_account("
				+"'"+pCustomerId+"',"				
				+"'"+ConvertFont.UtoD(pEmail)+"',"
				+"'"+pMobile+"',"
				+"'"+  CesarCode.getMd5(CesarCode.getMd5(pOldpassword))+"',"
				+"'"+  CesarCode.getMd5(CesarCode.getMd5(pPassword))+"',"
				+"'"+pUserIp+"'"				
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String edit_customer_information(
		String pid,
		String pfirst_name,
		String pmiddle_name,
		String plast_name,
		String pdate_of_birth,
		String ptax_number,
		String ptax_name,
		String ptax_address,		
		String pgender,
		String pcmt,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+getSysConst("FuncSchema")+"customer.edit_customer_information("
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pfirst_name)+"',"
				+"'"+ConvertFont.UtoD(pmiddle_name)+"',"
				+"'"+ConvertFont.UtoD(plast_name)+"',"
				+"'"+pdate_of_birth+"',"
				+"'"+ConvertFont.UtoD(ptax_number)+"',"
				+"'"+ConvertFont.UtoD(ptax_name)+"',"					
				+"'"+ConvertFont.UtoD(ptax_address)+"',"					
				+"'"+pgender+"',"
				+"'"+pcmt+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String logoutProcess(	
	){
		setUserVar("user_customer_signed","0");		
		setUserVar("user_customer_email","");
		setUserVar("user_customer_mobile","");
		setUserVar("user_customer_password","");
		setUserVar("user_customer_password","");
		return "";
	}
	
	public String loginProcess(	
		String pCustomerId,
		String pEmail,
		String pVinaphone,
		String pPassword,
		String pUserIp
	) throws Exception {
		String s ="begin ? :="+getSysConst("FuncSchema")+"customer.check_login("
				+"'"+pCustomerId+"',"
				+"'"+pEmail+"',"
				+"'"+pVinaphone+"',"
				+"'"+  CesarCode.getMd5(CesarCode.getMd5(pPassword))+"',"
				+"'"+pUserIp+"'"				
				+"); end;";
		System.out.println(s);
		
		RowSet rs_ = this.reqRSet("", s);
		while (rs_.next()){
			
			setUserVar("user_customer_signed","0");		
			setUserVar("user_customer_email","");
			setUserVar("user_customer_vinaphone","");
			setUserVar("user_customer_id","");
				
			String out_=rs_.getString("code");
			if (out_.equals("-1")){
				return "S&#7889; thu� bao ph&#7843;i l� thu� bao Vinaphone";
			}else if (out_.equals("-2")){
				return "Email kh�ng ch�nh x�c";
			}else if (out_.equals("-3")){
				return "S&#7889; thu� bao kh�ng ch�nh x�c";
			}else if (out_.equals("-4")){
				return "M&#7853;t kh&#7849;u kh�ng ch�nh x�c";
			}else if (out_.equals("-5")){
				return "T�i kho&#7843;n c&#7911;a b&#7841;n hi&#7879;n &#273;ang b&#7883; kh�a";
			}else if (out_.equals("0")){
				return "Vui l�ng ki&#7875;m tra l&#7841;i th�ng tin &#273;&#259;ng nh&#7853;p";
			}else if (out_.equals("1")){
				//Da dang nhap
				//System.out.println("EZO: dang nhap thanh cong|"+rs_.getString("id")+"|"+rs_.getString("email"));
				setUserVar("user_customer_signed","1");
				setUserVar("user_customer_id",rs_.getString("id"));
				setUserVar("user_customer_email",rs_.getString("email"));
				setUserVar("user_customer_vinaphone",rs_.getString("vinaphone"));				
				setUserVar("user_customer_password",rs_.getString("password"));
				System.out.println("EZO: dang nhap thanh cong|"+getUserVar("user_customer_signed")+"|"+rs_.getString("email"));
				return "1";
			}
		}	
		return "0";	
	}
	
	public String addToCart(
		String pProductId,	
		String pQty,	
		String pCustomerId,
		String pUserIp
	) throws Exception {
			String s ="begin ? :="+getSysConst("FuncSchema")+"customer.add_to_cart("
				+"'"+CesarCode.decode(pProductId)+"',"	
				+"'"+pQty+"',"			
				+"'"+pCustomerId+"',"
				+"'"+pUserIp+"'"				
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String editToCart(
		String pProductId,	
		String pQty,	
		String pCustomerId,
		String pUserIp
	) throws Exception {		
		String s ="begin ? :="+getSysConst("FuncSchema")+"customer.edit_to_cart("
				+"'"+CesarCode.decode(pProductId)+"',"	
				+"'"+pQty+"',"			
				+"'"+pCustomerId+"',"
				+"'"+pUserIp+"'"				
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String removeFromCart(
		String pProductId,			
		String pCustomerId,
		String pUserIp
	) throws Exception {
			String s ="begin ? :="+getSysConst("FuncSchema")+"customer.remove_from_cart("
				+"'"+CesarCode.decode(pProductId)+"',"					
				+"'"+pCustomerId+"',"
				+"'"+pUserIp+"'"				
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String customerOrder(		
        String pCustomerId,
        String pEmail,
        String pVinaphone,
        String pProductTotal,
        String pSubtotal,
        String pIsGift,
        String pSenderFullName,
        String pGenderId,
       	String pYearOld,
        String pMessage,
        String pAddressId,
        String pProvinceId,
        String pDistrictId,
        String pTownId,
        String pTownOther,
        String pHouseNumber,
        String pCompany,
        String pTelephone,
        String pFax,
        String isAddtoAddressBook,
        String pAdditionRequirement,
        String pUserId,
        String pUserIp
    ) throws Exception {
    	String s ="begin ? :="+getSysConst("FuncSchema")+"customer.customer_order("
    		+"'"+pCustomerId+"',"	
    		+"'"+pEmail+"',"	
    		+"'"+pVinaphone+"',"	
			+"'"+CesarCode.decode(pProductTotal)+"',"
			+"'"+CesarCode.decode(pSubtotal)+"',"	
    		+"'"+pIsGift+"',"	
			+"'"+ConvertFont.UtoD(pSenderFullName)+"',"	    			
			+"'"+pGenderId+"',"	
    		+"'"+pYearOld+"',"	
			+"'"+ConvertFont.UtoD(pMessage)+"',"	    			
			+"'"+pAddressId+"',"	
    		+"'"+pProvinceId+"',"	
			+"'"+pDistrictId+"',"	
			+"'"+pTownId+"',"	
			+"'"+ConvertFont.UtoD(pTownOther)+"',"	
			+"'"+ConvertFont.UtoD(pHouseNumber)+"',"	
			+"'"+ConvertFont.UtoD(pCompany)+"',"	
			+"'"+pTelephone+"',"	
			+"'"+pFax+"',"	
			+"'"+isAddtoAddressBook+"',"
			+"'"+ConvertFont.UtoD(pAdditionRequirement)+"',"	
    		+"'"+pUserId+"',"				
			+"'"+pUserIp+"'"			
			+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String customerVote(
		String pPollId,			
		String pAnswerId,
		String pCustomerId,
		String pEmail,
		String pVinaphone,
		String pUserIp						
	) throws Exception {
			String s ="begin ? :="+getSysConst("FuncSchema")+"customer.voting("
				+"'"+pPollId+"',"					
				+"'"+pAnswerId+"',"
				+"'"+pCustomerId+"',"					
				+"'"+pEmail+"',"
				+"'"+pVinaphone+"',"
				+"'"+pUserIp+"'"				
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public static void main(String[] args){
		String map="PACKING_LIST_NO;MID_CASE_BARCODE;PRODUCT_BARCODE;BOARD_BARCODE;SPU_BARCODE;ROHS;SOFTWARE_VERSION;COLOR;SIM_LOCK;IMEI";
		String val="9139704670001P;005690270274C1242449003368;XVA4CA1122209672;020XAN4C12093031;00B69207027091318894;Y2;V100R001B126D85SP00C00;WHITE;64566814;356576047163448";
		String[] m = map.split(";",-1);
		String[] v = val.split(";",-1);
		StringBuffer x = new StringBuffer();
		for (int i=0;i<v.length;i++){
			x.append("<"+m[i]+">").append(v[i]).append("</"+m[i]+">");
		}
		System.out.println(x.toString());
	}
	public String moving_order(
		String pIds,
		String pMaquay,
		String pUserid,
		String pUserip			
	) throws Exception {	
		String s ="begin ? :="+getSysConst("FuncSchema")+"customer.moving_order_to_dealer("
				+"'"+pIds+"',"					
				+"'"+pMaquay+"',"
				+"'"+pUserid+"',"
				+"'"+pUserip+"'"				
				+"); end;";
		return this.reqValue("EZO", s);
	}
}