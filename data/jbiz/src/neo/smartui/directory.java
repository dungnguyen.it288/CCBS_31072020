package neo.smartui;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import java.util.*;
import javax.sql.*;
import javax.sql.*;
import java.util.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.io.*;
import java.util.*;
import java.util.regex.*;

import neo.smartui.common.CesarCode;

public class directory extends NEOProcessEx 
{	
	public String new_key_value(
		String pid,
		String pkv_type,
		String pkv_comment,
		String ptype_id,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_key_value("
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pkv_type)+"',"
				+"'"+ConvertFont.UtoD(pkv_comment)+"',"
				+"'"+ConvertFont.UtoD(ptype_id)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_key_value(
		String pid,
		String pkv_type,
		String pkv_comment,
		String ptype_id,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_key_value("
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pkv_type)+"',"
				+"'"+ConvertFont.UtoD(pkv_comment)+"',"
				+"'"+ConvertFont.UtoD(ptype_id)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String del_key_value(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_key_value("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	
	public String del_catalog_category(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_catalog_category("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_catalog_category(
		String pid,
		String pname,
		String pis_active,
		String pthumbnail_image,
		String pdescription,
		String pimage,
		String pmeta_keywords,
		String pmeta_description,
		String pparent_id,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_catalog_category("
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pname)+"',"
				+"'"+pis_active+"',"
				+"'"+ConvertFont.UtoD(pthumbnail_image)+"',"
				+"'"+ConvertFont.UtoD(pdescription)+"',"
				+"'"+ConvertFont.UtoD(pimage)+"',"
				+"'"+ConvertFont.UtoD(pmeta_keywords)+"',"
				+"'"+ConvertFont.UtoD(pmeta_description)+"',"
				+"'"+pparent_id+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}	
	public String new_catalog_category(
		String pid,
		String pname,
		String pis_active,
		String pthumbnail_image,
		String pdescription,
		String pimage,
		String pmeta_keywords,
		String pmeta_description,
		String pparent_id,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_catalog_category("
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pname)+"',"
				+"'"+pis_active+"',"
				+"'"+ConvertFont.UtoD(pthumbnail_image)+"',"
				+"'"+ConvertFont.UtoD(pdescription)+"',"
				+"'"+ConvertFont.UtoD(pimage)+"',"
				+"'"+ConvertFont.UtoD(pmeta_keywords)+"',"
				+"'"+ConvertFont.UtoD(pmeta_description)+"',"
				+"'"+pparent_id+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}	
	public String new_catalog_product(
		String pid,
		String pdescription,
		String pshort_description,
		String psku,
		String pweight,
		String pas_new_date_from,
		String pstatus_id,
		String pvisibility_id,
		String pproduct_name,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_catalog_product("
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pdescription)+"',"
				+"'"+ConvertFont.UtoD(pshort_description)+"',"
				+"'"+ConvertFont.UtoD(psku)+"',"
				+"'"+ConvertFont.UtoD(pweight)+"',"
				+"'"+pas_new_date_from+"',"
				+"'"+pstatus_id+"',"
				+"'"+pvisibility_id+"',"
				+"'"+ConvertFont.UtoD(pproduct_name)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}	
	public String edit_catalog_product(
		String pid,
		String pdescription,
		String pshort_description,
		String psku,
		String pweight,
		String pas_new_date_from,
		String pstatus_id,
		String pvisibility_id,
		String pproduct_name,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_catalog_product("
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pdescription)+"',"
				+"'"+ConvertFont.UtoD(pshort_description)+"',"
				+"'"+ConvertFont.UtoD(psku)+"',"
				+"'"+ConvertFont.UtoD(pweight)+"',"
				+"'"+pas_new_date_from+"',"
				+"'"+pstatus_id+"',"
				+"'"+pvisibility_id+"',"
				+"'"+ConvertFont.UtoD(pproduct_name)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String del_catalog_product(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_catalog_product("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}	
	public String new_catalog_product_price(
		String pid,
		String pproduct_id,
		String pprice,
		String pspecial_price,
		String pspecial_price_from_date,
		String pspecial_price_to_date,
		String pcost,
		String ptax_class_id,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_catalog_product_price("
				+"'"+pid+"',"
				+"'"+pproduct_id+"',"
				+"'"+pprice+"',"
				+"'"+pspecial_price+"',"
				+"'"+pspecial_price_from_date+"',"
				+"'"+pspecial_price_to_date+"',"
				+"'"+pcost+"',"
				+"'"+ptax_class_id+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_catalog_product_price(
		String pid,
		String pproduct_id,
		String pprice,
		String pspecial_price,
		String pspecial_price_from_date,
		String pspecial_price_to_date,
		String pcost,
		String ptax_class_id,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_catalog_product_price("
				+"'"+pid+"',"
				+"'"+pproduct_id+"',"
				+"'"+pprice+"',"
				+"'"+pspecial_price+"',"
				+"'"+pspecial_price_from_date+"',"
				+"'"+pspecial_price_to_date+"',"
				+"'"+pcost+"',"
				+"'"+ptax_class_id+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String del_catalog_product_price(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_catalog_product_price("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}					
	public String new_catalog_product_price_tier(
		String pid,
		String pprice_id,
		String pcustomer_group_id,
		String pqty,
		String pprice,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_catalog_product_price_tier("
				+"'"+pid+"',"
				+"'"+pprice_id+"',"
				+"'"+pcustomer_group_id+"',"
				+"'"+pqty+"',"
				+"'"+pprice+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_catalog_product_price_tier(
		String pid,
		String pprice_id,
		String pcustomer_group_id,
		String pqty,
		String pprice,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_catalog_prod_price_tier("
				+"'"+pid+"',"
				+"'"+pprice_id+"',"
				+"'"+pcustomer_group_id+"',"
				+"'"+pqty+"',"
				+"'"+pprice+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String del_catalog_product_price_tier(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_catalog_product_price_tier("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}		
	public String del_customer_group(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_customer_group("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_customer_group(
		String pid,
		String pgroup_name,
		String pdescription,
		String pgroup_comment,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_customer_group("
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pgroup_name)+"',"
				+"'"+ConvertFont.UtoD(pdescription)+"',"
				+"'"+ConvertFont.UtoD(pgroup_comment)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}	
	public String new_customer_group(
		String pid,
		String pgroup_name,
		String pdescription,
		String pgroup_comment,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_customer_group("
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pgroup_name)+"',"
				+"'"+ConvertFont.UtoD(pdescription)+"',"
				+"'"+ConvertFont.UtoD(pgroup_comment)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}		
	public String new_catalog_product_image(
		String pid,
		String pproduct_id,
		String pimage_file,
		String plabel,
		String pis_base_image,
		String pis_small_image,
		String pis_thumbnail_image,
		String pexclude,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_catalog_product_image("
				+"'"+pid+"',"
				+"'"+pproduct_id+"',"
				+"'"+ConvertFont.UtoD(pimage_file)+"',"
				+"'"+ConvertFont.UtoD(plabel)+"',"
				+"'"+pis_base_image+"',"
				+"'"+pis_small_image+"',"
				+"'"+pis_thumbnail_image+"',"
				+"'"+pexclude+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_catalog_product_image(
		String pid,
		String pproduct_id,
		String pimage_file,
		String plabel,
		String pis_base_image,
		String pis_small_image,
		String pis_thumbnail_image,
		String pexclude,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_catalog_product_image("
				+"'"+pid+"',"
				+"'"+pproduct_id+"',"
				+"'"+ConvertFont.UtoD(pimage_file)+"',"
				+"'"+ConvertFont.UtoD(plabel)+"',"
				+"'"+pis_base_image+"',"
				+"'"+pis_small_image+"',"
				+"'"+pis_thumbnail_image+"',"
				+"'"+pexclude+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}	
	public String del_catalog_product_image(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_catalog_product_image("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}			
	public String new_catalog_product_category(
		String pid,
		String pproduct_id,
		String pcategory_id,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_catalog_product_category("
				+"'"+pid+"',"
				+"'"+pproduct_id+"',"
				+"'"+pcategory_id+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_catalog_product_category(
		String pid,
		String pproduct_id,
		String pcategory_id,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_catalog_product_category("
				+"'"+pid+"',"
				+"'"+pproduct_id+"',"
				+"'"+pcategory_id+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String del_catalog_product_category(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_catalog_product_category("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String new_catalog_product_related(
		String pid,
		String pproduct_id,
		String prelated_product_id,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_catalog_product_related("
				+"'"+pid+"',"
				+"'"+pproduct_id+"',"
				+"'"+prelated_product_id+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_catalog_product_related(
		String pid,
		String pproduct_id,
		String prelated_product_id,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_catalog_product_related("
				+"'"+pid+"',"
				+"'"+pproduct_id+"',"
				+"'"+prelated_product_id+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String del_catalog_product_related(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_catalog_product_related("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_customer(
		String pid,
		String playout_id,
		String pcustomer_group_id,
		String pregister_method_id,
		String pfirst_name,
		String pmiddle_name,
		String plast_name,
		String pemail,
		String pdate_of_birth,
		String ptax_number,
		String pgender,
		String pparent_id,
		String pmobile,
		String ppassword,
		String pstatus,
		String ptax_address,
		String pcmtnd,
		String ptax_name,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_customer("
				+"'"+pid+"',"
				+"'"+playout_id+"',"
				+"'"+pcustomer_group_id+"',"
				+"'"+pregister_method_id+"',"
				+"'"+ConvertFont.UtoD(pfirst_name)+"',"
				+"'"+ConvertFont.UtoD(pmiddle_name)+"',"
				+"'"+ConvertFont.UtoD(plast_name)+"',"
				+"'"+ConvertFont.UtoD(pemail)+"',"
				+"'"+pdate_of_birth+"',"
				+"'"+ConvertFont.UtoD(ptax_number)+"',"
				+"'"+pgender+"',"
				+"'"+pparent_id+"',"
				+"'"+ConvertFont.UtoD(pmobile)+"',"
				+"'"+ConvertFont.UtoD(ppassword)+"',"
				+"'"+pstatus+"',"
				+"'"+ConvertFont.UtoD(ptax_address)+"',"
				+"'"+ConvertFont.UtoD(pcmtnd)+"',"
				+"'"+ConvertFont.UtoD(ptax_name)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}							
	public String new_customer(
		String pid,
		String playout_id,
		String pcustomer_group_id,
		String pregister_method_id,
		String pfirst_name,
		String pmiddle_name,
		String plast_name,
		String pemail,
		String pdate_of_birth,
		String ptax_number,
		String pgender,
		String pparent_id,
		String pmobile,
		String ppassword,
		String pstatus,
		String ptax_address,
		String pcmtnd,
		String ptax_name,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_customer("
				+"'"+pid+"',"
				+"'"+playout_id+"',"
				+"'"+pcustomer_group_id+"',"
				+"'"+pregister_method_id+"',"
				+"'"+ConvertFont.UtoD(pfirst_name)+"',"
				+"'"+ConvertFont.UtoD(pmiddle_name)+"',"
				+"'"+ConvertFont.UtoD(plast_name)+"',"
				+"'"+ConvertFont.UtoD(pemail)+"',"
				+"'"+pdate_of_birth+"',"
				+"'"+ConvertFont.UtoD(ptax_number)+"',"
				+"'"+pgender+"',"
				+"'"+pparent_id+"',"
				+"'"+ConvertFont.UtoD(pmobile)+"',"
				+"'"+ConvertFont.UtoD(ppassword)+"',"
				+"'"+pstatus+"',"
				+"'"+ConvertFont.UtoD(ptax_address)+"',"
				+"'"+ConvertFont.UtoD(pcmtnd)+"',"
				+"'"+ConvertFont.UtoD(ptax_name)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String del_customer(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_customer("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}			
	public String new_customer_address(
		String pid,
		String pcustomer_id,
		String paddress_type_id,
		String pfirt_name,
		String pmiddle_name,
		String plast_name,
		String pcompany,
		String phouse_number,
		String ptown_id,
		String pdistrict_id,
		String pprovince_id,
		String pcountry_id,
		String ptelephone,
		String pfax,
		String paddress_comment,
		String ptown_other,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_customer_address("
				+"'"+pid+"',"
				+"'"+pcustomer_id+"',"
				+"'"+paddress_type_id+"',"
				+"'"+ConvertFont.UtoD(pfirt_name)+"',"
				+"'"+ConvertFont.UtoD(pmiddle_name)+"',"
				+"'"+ConvertFont.UtoD(plast_name)+"',"
				+"'"+ConvertFont.UtoD(pcompany)+"',"
				+"'"+ConvertFont.UtoD(phouse_number)+"',"
				+"'"+ptown_id+"',"
				+"'"+pdistrict_id+"',"
				+"'"+pprovince_id+"',"
				+"'"+pcountry_id+"',"
				+"'"+ConvertFont.UtoD(ptelephone)+"',"
				+"'"+ConvertFont.UtoD(pfax)+"',"
				+"'"+ConvertFont.UtoD(paddress_comment)+"',"
				+"'"+ConvertFont.UtoD(ptown_other)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_customer_address(
		String pid,
		String pcustomer_id,
		String paddress_type_id,
		String pfirt_name,
		String pmiddle_name,
		String plast_name,
		String pcompany,
		String phouse_number,
		String ptown_id,
		String pdistrict_id,
		String pprovince_id,
		String pcountry_id,
		String ptelephone,
		String pfax,
		String paddress_comment,
		String town_other,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_customer_address("
				+"'"+pid+"',"
				+"'"+pcustomer_id+"',"
				+"'"+paddress_type_id+"',"
				+"'"+ConvertFont.UtoD(pfirt_name)+"',"
				+"'"+ConvertFont.UtoD(pmiddle_name)+"',"
				+"'"+ConvertFont.UtoD(plast_name)+"',"
				+"'"+ConvertFont.UtoD(pcompany)+"',"
				+"'"+ConvertFont.UtoD(phouse_number)+"',"
				+"'"+ptown_id+"',"
				+"'"+pdistrict_id+"',"
				+"'"+pprovince_id+"',"
				+"'"+pcountry_id+"',"
				+"'"+ConvertFont.UtoD(ptelephone)+"',"
				+"'"+ConvertFont.UtoD(pfax)+"',"
				+"'"+ConvertFont.UtoD(paddress_comment)+"',"
				+"'"+ConvertFont.UtoD(town_other)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String del_customer_address(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_customer_address("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}				
	public String new_region(
		String pid,
		String pregion_name,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_region("
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pregion_name)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_region(
		String pid,
		String pregion_name,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_region("
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pregion_name)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String del_region(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_region("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}				
	public String new_eav_attribute(
		String pid,
		String pattr_name,
		String pattr_type_id,
		String pattr_class_id,
		String pattr_comment,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_eav_attribute("
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pattr_name)+"',"
				+"'"+pattr_type_id+"',"
				+"'"+pattr_class_id+"',"
				+"'"+ConvertFont.UtoD(pattr_comment)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_eav_attribute(
		String pid,
		String pattr_name,
		String pattr_type_id,
		String pattr_class_id,
		String pattr_comment,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_eav_attribute("
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pattr_name)+"',"
				+"'"+pattr_type_id+"',"
				+"'"+pattr_class_id+"',"
				+"'"+ConvertFont.UtoD(pattr_comment)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String del_eav_attribute(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_eav_attribute("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}		
	public String new_catalog_product_attr(
		String pid,
		String pproduct_id,
		String pattr_id,
		String pattr_value,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_catalog_product_attr("
				+"'"+pid+"',"
				+"'"+pproduct_id+"',"
				+"'"+pattr_id+"',"
				+"'"+ConvertFont.UtoD(pattr_value)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_catalog_product_attr(
		String pid,
		String pproduct_id,
		String pattr_id,
		String pattr_value,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_catalog_product_attr("
				+"'"+pid+"',"
				+"'"+pproduct_id+"',"
				+"'"+pattr_id+"',"
				+"'"+ConvertFont.UtoD(pattr_value)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String del_catalog_product_attr(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_catalog_product_attr("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}				
	public String new_header_image(
		String pid,
		String pscheduler_type_id,
		String pstart_at_date,
		String pend_at_date,
		String pimage_type_id,
		String pstatus_id,
		String ppriority,
		String pimage_file,
		String pfrom_hour,
		String pto_hour,
		String pfrom_day,
		String pto_day,
		String pimage_logo_file,
		String pccs,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_header_image("
				+"'"+pid+"',"
				+"'"+pscheduler_type_id+"',"
				+"'"+pstart_at_date+"',"
				+"'"+pend_at_date+"',"
				+"'"+pimage_type_id+"',"
				+"'"+pstatus_id+"',"
				+"'"+ppriority+"',"
				+"'"+ConvertFont.UtoD(pimage_file)+"',"
				+"'"+pfrom_hour+"',"
				+"'"+pto_hour+"',"
				+"'"+pfrom_day+"',"
				+"'"+pto_day+"',"
				+"'"+ConvertFont.UtoD(pimage_logo_file)+"',"
				+"'"+ConvertFont.UtoD(pccs)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String del_header_image(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_header_image("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_header_image(
		String pid,
		String pscheduler_type_id,
		String pstart_at_date,
		String pend_at_date,
		String pimage_type_id,
		String pstatus_id,
		String ppriority,
		String pimage_file,
		String pfrom_hour,
		String pto_hour,
		String pfrom_day,
		String pto_day,
		String pimage_logo_file,
		String pccs,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_header_image("
				+"'"+pid+"',"
				+"'"+pscheduler_type_id+"',"
				+"'"+pstart_at_date+"',"
				+"'"+pend_at_date+"',"
				+"'"+pimage_type_id+"',"
				+"'"+pstatus_id+"',"
				+"'"+ppriority+"',"
				+"'"+ConvertFont.UtoD(pimage_file)+"',"
				+"'"+pfrom_hour+"',"
				+"'"+pto_hour+"',"
				+"'"+pfrom_day+"',"
				+"'"+pto_day+"',"
				+"'"+ConvertFont.UtoD(pimage_logo_file)+"',"
				+"'"+ConvertFont.UtoD(pccs)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}	
	public String del_address(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_address("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_address(
		String pid,
		String paddress_type_id,
		String phouse_number,
		String ptown_id,
		String pdistrict_id,
		String pprovince_id,
		String pcountry_id,
		String ptelephone,
		String pfax,
		String paddress_comment,
		String ptown_other,
		String ploguser,
		String plogip,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_address("
				+"'"+pid+"',"
				+"'"+paddress_type_id+"',"
				+"'"+ConvertFont.UtoD(phouse_number)+"',"
				+"'"+ptown_id+"',"
				+"'"+pdistrict_id+"',"
				+"'"+pprovince_id+"',"
				+"'"+pcountry_id+"',"
				+"'"+ConvertFont.UtoD(ptelephone)+"',"
				+"'"+ConvertFont.UtoD(pfax)+"',"
				+"'"+ConvertFont.UtoD(paddress_comment)+"',"
				+"'"+ConvertFont.UtoD(ptown_other)+"',"
				+"'"+ConvertFont.UtoD(ploguser)+"',"
				+"'"+ConvertFont.UtoD(plogip)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String new_address(
		String pid,
		String paddress_type_id,
		String phouse_number,
		String ptown_id,
		String pdistrict_id,
		String pprovince_id,
		String pcountry_id,
		String ptelephone,
		String pfax,
		String paddress_comment,
		String ptown_other,
		String ploguser,
		String plogip,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_address("
				+"'"+pid+"',"
				+"'"+paddress_type_id+"',"
				+"'"+ConvertFont.UtoD(phouse_number)+"',"
				+"'"+ptown_id+"',"
				+"'"+pdistrict_id+"',"
				+"'"+pprovince_id+"',"
				+"'"+pcountry_id+"',"
				+"'"+ConvertFont.UtoD(ptelephone)+"',"
				+"'"+ConvertFont.UtoD(pfax)+"',"
				+"'"+ConvertFont.UtoD(paddress_comment)+"',"
				+"'"+ConvertFont.UtoD(ptown_other)+"',"
				+"'"+ConvertFont.UtoD(ploguser)+"',"
				+"'"+ConvertFont.UtoD(plogip)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String new_warehouse_location(
		String pid,
		String plocaton_type_id,
		String plocation_name,
		String prepresentative,
		String pparent_id,
		String plocation_code,
		String paddress_id,
		String plocation_comment,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_warehouse_location("
				+"'"+pid+"',"
				+"'"+plocaton_type_id+"',"
				+"'"+ConvertFont.UtoD(plocation_name)+"',"
				+"'"+ConvertFont.UtoD(prepresentative)+"',"
				+"'"+pparent_id+"',"
				+"'"+ConvertFont.UtoD(plocation_code)+"',"
				+"'"+paddress_id+"',"
				+"'"+ConvertFont.UtoD(plocation_comment)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_warehouse_location(
		String pid,
		String plocaton_type_id,
		String plocation_name,
		String prepresentative,
		String pparent_id,
		String plocation_code,
		String paddress_id,
		String plocation_comment,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_warehouse_location("
				+"'"+pid+"',"
				+"'"+plocaton_type_id+"',"
				+"'"+ConvertFont.UtoD(plocation_name)+"',"
				+"'"+ConvertFont.UtoD(prepresentative)+"',"
				+"'"+pparent_id+"',"
				+"'"+ConvertFont.UtoD(plocation_code)+"',"
				+"'"+paddress_id+"',"
				+"'"+ConvertFont.UtoD(plocation_comment)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String del_warehouse_location(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_warehouse_location("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}							
	public String new_warehouse(
		String pid,
		String pwarehouse_name,
		String plocation_input_id,
		String plocation_stock_id,
		String plocation_output_id,
		String paddress_id,
		String pwarehouse_comment,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.new_warehouse("
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pwarehouse_name)+"',"
				+"'"+plocation_input_id+"',"
				+"'"+plocation_stock_id+"',"
				+"'"+plocation_output_id+"',"
				+"'"+paddress_id+"',"
				+"'"+ConvertFont.UtoD(pwarehouse_comment)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String edit_warehouse(
		String pid,
		String pwarehouse_name,
		String plocation_input_id,
		String plocation_stock_id,
		String plocation_output_id,
		String paddress_id,
		String pwarehouse_comment,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.edit_warehouse("
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pwarehouse_name)+"',"
				+"'"+plocation_input_id+"',"
				+"'"+plocation_stock_id+"',"
				+"'"+plocation_output_id+"',"
				+"'"+paddress_id+"',"
				+"'"+ConvertFont.UtoD(pwarehouse_comment)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String del_warehouse(
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"directory.del_warehouse("
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}				
	public String stockOutTicket(
		String pBuffer,
		String pTicket,
		String pIn,
		String pOut,
		String pStockType,
		String pComment,
		String pProducts,
		String pQtys,
		String pUnits,
		String pImeis,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"catalog.new_stock_out_ticket("
				+"'"+pBuffer+"',"
				+"'"+pTicket+"',"
				+"'"+pIn+"',"
				+"'"+pOut+"',"
				+"'"+pStockType+"',"
				+"'"+pComment+"',"
				+"'"+pProducts+"',"
				+"'"+pQtys+"',"
				+"'"+pUnits+"',"
				+"'"+pImeis+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String stockInTicket(
		String pBuffer,
		String pTicket,
		String pIn,
		String pOut,
		String pStockType,
		String pComment,
		String pProducts,
		String pQtys,
		String pUnits,
		String pImeis,
		String pStockOut,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :="+"admin."+"catalog.new_stock_in_ticket("
				+"'"+pBuffer+"',"
				+"'"+pTicket+"',"
				+"'"+pIn+"',"
				+"'"+pOut+"',"
				+"'"+pStockType+"',"
				+"'"+pComment+"',"
				+"'"+pProducts+"',"
				+"'"+pQtys+"',"
				+"'"+pUnits+"',"
				+"'"+pImeis+"',"
				+"'"+pStockOut+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("EZO", s);
	}
	public String moving_order(
		String pIds,
		String pMaquay,
		String pUserid,
		String pUserip			
	) throws Exception {	
		String s ="begin ? :=admin.customer.moving_order_to_dealer("
				+"'"+pIds+"',"					
				+"'"+pMaquay+"',"
				+"'"+pUserid+"',"
				+"'"+pUserip+"'"				
				+"); end;";
		return this.reqValue("EZO", s);
	}
	public String sms_to_vina(
		String pIds,
		String pVina,
		String pLuuSo,
		String pUserid,
		String pUserip			
	) throws Exception {	
		String s ="begin ? :=admin.customer.sms_to_vina("
				+"'"+pIds+"',"					
				+"'"+pVina+"',"
				+"'"+pLuuSo+"',"
				+"'"+pUserid+"',"
				+"'"+pUserip+"'"				
				+"); end;";
		return this.reqValue("EZO", s);
	}
	public String sms(		
		String pVina,
		String pContent,
		String pUserid,
		String pUserip			
	) throws Exception {	
		String s ="begin ? :=admin.customer.sms("
				+"'"+pVina+"',"
				+"'"+pContent+"',"
				+"'"+pUserid+"',"
				+"'"+pUserip+"'"				
				+"); end;";
		return this.reqValue("EZO", s);
	}
	public String new_customer_order_trace(		
		String pOrderId,
        String pOrderStatus,
        String pcontent,
        String pMaquay,
        String pSchema,
        String pAgentCode,
        String pUserId,
        String pUserIp		
	) throws Exception {	
		String s ="begin ? :=admin.customer.new_customer_order_trace("
				+"'"+pOrderId+"',"
				+"'"+pOrderStatus+"',"
				+"'"+ConvertFont.UtoD(pcontent)+"',"
				+"'"+pMaquay+"',"
				+"'"+pSchema+"',"
				+"'"+pAgentCode+"',"
				+"'"+pUserId+"',"
				+"'"+pUserIp+"'"				
				+"); end;";
		return this.reqValue("EZO", s);
	}
	public String edit_customer_order_trace(		
		String pId,
        String pOrderStatus,
        String pcontent,
        String pMaquay,
        String pSchema,
        String pAgentCode,
        String pUserId,
        String pUserIp		
	) throws Exception {	
		String s ="begin ? :=admin.customer.edit_customer_order_trace("
				+"'"+pId+"',"
				+"'"+pOrderStatus+"',"
				+"'"+ConvertFont.UtoD(pcontent)+"',"
				+"'"+pMaquay+"',"
				+"'"+pSchema+"',"
				+"'"+pAgentCode+"',"
				+"'"+pUserId+"',"
				+"'"+pUserIp+"'"				
				+"); end;";
		return this.reqValue("EZO", s);
	}
}