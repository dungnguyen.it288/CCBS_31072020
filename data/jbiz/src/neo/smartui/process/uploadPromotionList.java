package neo.smartui.process;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.RowSet;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.jspsmart.file.Fso;
import com.jspsmart.upload.SmartUpload;

import neo.smartui.report.DataParameter;
import neo.smartui.report.EnvInfo;
import neo.smartui.report.ReportLayoutParser;

/**
 * Doc danh sach thue bao duoc khuyen mai tu file txt/excel va luu vao DB ds_khuyenmais.
 */
public class uploadPromotionList implements ProcessInterface {
	EnvInfo _ei = null;
	String strNumber = "0123456789";
	ArrayList arrErr = new ArrayList();
	private final String retErr = "&#272;&#7883;nh d&#7841;ng file kh&#244;ng &#273;&#250;ng. B&#7841;n h&#227;y ki&#7875;m tra file v&#224; upload l&#7841;i.";
	
	private static final String IS_DELETE = "2";
	
    public uploadPromotionList() {}
    public String process(ReportLayoutParser parser) {
    	
    	System.out.println("+++++++++++++++++++++++uploadPromotionList+++++++++++++++++++");
        this._ei = parser.getEnvInfo();
        String ret = "";
        String sSQL = "";
        String uploadId = "";
        String fileType = "";
        int fileSize = 0;

        SmartUpload mySmartUpload = parser.getSmartupload();
        String cmdv_datasrc_ = "VNPBilling";
        Map parameters = parser.getParameters();        
        
        String loaiUploadId = ((String) parameters.get("loai_upload_id")).trim();
        String congvanId = ((String) parameters.get("cbokmcv_id")).trim();
        String hinhthucId = ((String) parameters.get("cbokmht_id")).trim();
        //String isOverride = ((String)parameters.get("overridematb"));
        String uploadFileType = (String)parameters.get("uploadfiletype");
        String uploadType = (String)parameters.get("htupload");
        
        System.out.println("+++++++++++radFileType : " + uploadFileType);
        System.out.println("+++++++++++uploadType : " + uploadType);
        
        
        //String funcSchema = ((String) parameters.get("sys_funcschema")).trim();
        String agentCode = ((String) parameters.get("sys_agentcode")).trim();
        String userId = ((String) parameters.get("sys_userid")).trim();
        String userIp = ((String) parameters.get("sys_userip")).trim();
        String usingLoader =((String) parameters.get("using_loader")).trim();
        //String data =((String) parameters.get("datashema_")).trim();
        
        String pTableName = "";
        String pSchema = "";
        String pColumns = "";
        String pSeparator = "";
        String pProc = "";
        String pDelFile = "";
        String pColIns = "";
        String pColIdx = "";        
        String pDir = "";
        String condValid = "";
        
        System.out.println("----------Start Upload : " + new Date());
        try {
            for (int i = 0; i < mySmartUpload.getFiles().getCount(); i++) {
                String fileName = null;
                com.jspsmart.upload.File myFile = mySmartUpload.getFiles().
                                                  getFile(i);
                fileType = myFile.getFileExt();
                fileSize = myFile.getSize();                
                
                //Lay cac tham so cua cau hinh Upload
                sSQL = "SELECT * FROM ccs_common.data_uploads WHERE id="+loaiUploadId;
                RowSet para_ = DataParameter.getRSet(_ei,cmdv_datasrc_,sSQL);
                
                while (para_.next()){
	                pTableName = para_.getString("TABLE_NAME");
	                pSchema = para_.getString("SCHEMA");
	                pColumns = para_.getString("COLUMNS");
	                pSeparator = para_.getString("SEPARATOR");
	                pProc = para_.getString("PROCEDURE");
	                pDelFile = para_.getString("DELETE_FILE");
	                pColIns = para_.getString("COL_INSERTS");
	                pColIdx = para_.getString("COL_INDEXES");
	                pDir = para_.getString("DIR");
	                condValid = para_.getString("SQLLDR_CMD");	                
                }
                
                 sSQL = "begin \n" +
                           "  ? := BILLING_DOISOAT.THAMSO.lay_giatri_sequence('SEQ_KHUYENMAI'); \n  end;\n";
                  
                uploadId = DataParameter.getValue(_ei,cmdv_datasrc_, sSQL);
                
                if (pDir != null){
                	pDir = pDir+"/";
                }else{
                	pDir = "";
                }
                
                SimpleDateFormat simpledateformat = new SimpleDateFormat("ss-mm-HH_dd-MM-yyyy");
                Date date = new Date();
                String cur_=simpledateformat.format(date);
                
                if (!myFile.isMissing()) {
                    fileName = parser.getServlet().getConfigDir() +
                                "upload/" + pDir + pSchema +
                                pTableName + uploadId + "_" + cur_ + "." + fileType;
                        if (Fso.fileExists(fileName)) {
                            try {
                            	Fso.deleteFile(fileName);
                            }catch(IOException ioe){
                            	ioe.printStackTrace();
                            }
                        }
                }
                myFile.saveAs(fileName);
                
                //create table if not exist
                sSQL="declare kq_ varchar2(1000):='1'; begin\n begin\n"+
                	  "		execute immediate 'create table "+pSchema+pTableName+"("+pColumns.replace("#",",").replace(":"," ")+")';\n"+
                	  "exception when others then kq_:='2';\n"+
                	  "end; ?:=kq_; end;";
                DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);
               	
               	//create index
               	String[] indexes = pColIdx.split("#");
               	for (int j=0;j<indexes.length;j++){
					
					String[] col_idxs=indexes[j].split(":");
	               	
	               	sSQL="declare kq_ varchar2(1000):='1'; begin\n begin\n"+
	                	  "		execute immediate 'create index "+pSchema+col_idxs[1]+" on "+pSchema+pTableName+"("+col_idxs[0]+")';\n"+
	                	  "exception when others then kq_:='2';\n"+
	                	  "end; ?:=kq_; end;";
	                
	                DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);	               	
               	}
                
                File inputFile = new File(fileName);
                if (usingLoader == null) usingLoader = "0";
                if (Integer.parseInt(usingLoader) == 0){
	                int cnt = 0;
	                if (fileType.toUpperCase().contains("TXT")){
	                	
	                	System.out.println("----------Start check format file : " + new Date());
	                	BufferedReader inTmp = new BufferedReader(new FileReader(fileName));
		                String line;
		                String lineTmp;		                
		                
		                //check format file input
		                int lineIdx = 0;
		                while ((lineTmp = inTmp.readLine()) != null) {
		                	lineTmp = lineTmp.trim();
		                	lineTmp = lineTmp.replace(" ", "");		                	
		                	lineIdx += 1;
		                	validateLine(lineTmp,condValid,lineIdx);
		                }
		                inTmp.close();
		                
		                if(!arrErr.isEmpty()) {
		                	ret = "{file:\"" + pSchema + pTableName + uploadId + "." + fileType +
	                     	  "\",type:\"" + fileType + 
	                        "\",size:" + String.valueOf(fileSize) +
	                        ",upload_id:"+uploadId +
	                        ",errMsg:\"" + _toString(arrErr) 
	                        + "\"}";
		                	System.out.println("----------End check format file err : " + new Date());
		                	return ret;
		                }
		                System.out.println("----------End check format file : " + new Date());
		                //end check format
		                
		                
		                BufferedReader in = new BufferedReader(new FileReader(fileName));
		                sSQL = "declare psUploadID varchar2(30000);\n"+
		                	   "begin \n"+
		                	   "	begin \n";
		                
		                try {
		                	System.out.println("----------Start Insert : " + new Date());
			            	while ((line = in.readLine()) != null) {
			                	line = line.trim();
			                	line = line.replace(" ", "");
			                	line = line.replace("'","''");
			                	line = line.replace(pSeparator,"','");
			                	
			                	sSQL += "insert into " + pSchema+pTableName+"("+pColIns+") values ('" + line +"','"+uploadId+"','"+agentCode+"',"+congvanId+","+hinhthucId+","+loaiUploadId+",'"+userId+"','"+userIp+"');\n";
			                	if (cnt % 100 == 0 && cnt != 0) {
			                		//Thuc thi 100 lan 1
			                    	sSQL += "	psUploadID:=" + uploadId + "; \n" +
			                            	"	exception when others then " +
			                            	"   rollback;\n"+
			                            	"	psUploadID := to_char(SQLCODE)||': '||SQLERRM; end; \n" +
			                            	"   ? := psUploadID; end;";
			                        
			                    	ret = DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);
				                    
			                    	try {
				                        if (Integer.parseInt(ret) != Integer.parseInt(uploadId)) {
				                            return ret;
				                        }
				                    } catch (NumberFormatException ex) {
				                    	in.close();
				                    	System.out.println("NumberFormatException: " + ex.getMessage());
				                        return retErr;
				                    }
				
				                    sSQL = "declare psUploadID varchar2(30000);\n"+
				                	   	   "begin \n"+
				                	   	   "	begin \n";
			                	}
			                	cnt += 1;
			            	}
		            		in.close();
		            		System.out.println("----------End Insert : " + new Date());
		                } catch(Exception fe){
		                	in.close();
		                	System.out.println("Exception : " + fe.getMessage());
		                	return retErr;
		                }
	                } else if (fileType.toUpperCase().contains("XLS")){  //excel file
	                	
	                	InputStream myxls = new FileInputStream(fileName);
	                	HSSFWorkbook wb = new HSSFWorkbook(myxls);
	                	HSSFRow row;
	                	HSSFCell cell;
	                	String line = "";
		                String lineTmp = "";
		                
	                	StringTokenizer stTmp = new StringTokenizer(condValid);
	                	int cntCondValid = stTmp.countTokens();
	                	int nRow = 1;
	                	
	                	while (true) {
	                		row = wb.getSheetAt(0).getRow(nRow);
	                		if(row == null) {
	                			break;
	                		}
	                		lineTmp = "";
	                		for(int j = 1; j <= cntCondValid; j++) {
		                		cell = row.getCell(j);
		                		if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
		                			lineTmp += cell.getNumericCellValue() + ",";
		                		} else {
		                			lineTmp += cell.getStringCellValue() + ",";
		                		}
		                	}
	                		if(lineTmp.length() > 0 ) {
	                			validateLine(lineTmp.substring(0,lineTmp.length() - 1),condValid,nRow);
	                		}
	                		nRow += 1;
	                	}
	                	
		                System.out.println("++++arrErr : " + _toString(arrErr));
		                System.out.println("+++++condValid : " + condValid);
		                
		                if(!arrErr.isEmpty()) {		                	
		                	//return "{err:\"" + _toString(arrErr) +"\"}";
		                	ret = "{file:\"" + pSchema + pTableName + uploadId + "." + fileType +
	                     	  "\",type:\"" + fileType + 
	                        "\",size:" + String.valueOf(fileSize) +
	                        ",upload_id:"+uploadId +
	                        ",errMsg:\"" + _toString(arrErr) 
	                        + "\"}";
		                	return ret;
		                }
		                //end check format
		                
		                BufferedReader in = new BufferedReader(new FileReader(fileName));
		                sSQL = "declare psUploadID varchar2(30000);\n"+
		                	   "begin \n"+
		                	   "	begin \n";
		                
		                try {
		                	nRow = 1;
		                	while (true) {	                	    
		                		row = wb.getSheetAt(0).getRow(nRow);
		                		if(row == null) {
		                			break;
		                		}
		                		line = "";
		                		for(int j = 1; j <= cntCondValid; j++) {
			                		cell = row.getCell(j);
			                		if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
			                			line += cell.getNumericCellValue() + ",";
			                		} else {
			                			line += cell.getStringCellValue() + ",";
			                		}
			                	}
			                	
		                		if(line.length() > 0 ) {
		                			line = line.substring(0,line.length() - 1);
		                		}
		                		
			                	sSQL += "insert into " + pSchema+pTableName+"("+pColIns+") values ('" + line +"','"+uploadId+"','"+agentCode+"',"+congvanId+","+hinhthucId+","+loaiUploadId+",'"+userId+"','"+userIp+"');\n";
			                	if (cnt % 50 == 0 && cnt != 0) {
			                		//Thuc thi 50 lan 1
			                    	sSQL += "	psUploadID:=" + uploadId + "; \n" +
			                            	"	exception when others then " +
			                            	"   rollback;\n"+
			                            	"	psUploadID := to_char(SQLCODE)||': '||SQLERRM; end; \n" +
			                            	"   ? := psUploadID; end;";
			                        
			                    	ret = DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);
				                    
			                    	try {
				                        if (Integer.parseInt(ret) != Integer.parseInt(uploadId)) {
				                            return ret;
				                        }
				                    } catch (NumberFormatException ex) {				                    	
				                    	System.out.println("NumberFormatException: " + ex.getMessage());
				                        return retErr;
				                    }
				
				                    sSQL = "declare psUploadID varchar2(30000);\n"+
				                	   	   "begin \n"+
				                	   	   "	begin \n";
			                	}
			                	cnt += 1;
			                	nRow += 1;
			            	}		            		
		                } catch(Exception fe){		                	
		                	System.out.println("Exception : " + fe.getMessage());
		                	return retErr;
		                }
	                } //end excel file
	            	
	            	sSQL += "	psUploadID:=" + uploadId + "; \n" +
	                	"	exception when others then " +
	                	"   rollback;\n"+
	                	"	psUploadID := to_char(SQLCODE)||': '||SQLERRM; end; \n" +
	                	"   ? := psUploadID; end;";                                    
	                
		            ret = DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);
		            
		            try {
	                    if (Integer.parseInt(ret) != Integer.parseInt(uploadId)) {
	                        return ret;
	                    }
	                } catch (NumberFormatException ne) {
	                	System.out.println("NumberFormatException 2 : " + ne.getMessage());
	                	//ex.printStackTrace();
	                	return retErr;
	                }
            	} else {
            		//Xu ly file de SQL Loader
		            BufferedReader in = new BufferedReader(new FileReader(fileName));
		            String line_;
		            String data_loader_file="";
		            try{
		            	data_loader_file = parser.getServlet().getConfigDir() +
                            "upload\\sqlldr\\" + cur_ + ".data";
                        if (Fso.fileExists(data_loader_file)) {
                            try {
                            	Fso.deleteFile(data_loader_file);
                            }catch(IOException ioe){
                            	ioe.printStackTrace();
                            }
                        }
                        BufferedWriter wFile = new BufferedWriter(new FileWriter(data_loader_file));
                        wFile.write("LOAD DATA\n");
                        wFile.write("INFILE *\n");
                        wFile.write("APPEND INTO TABLE "+pSchema+pTableName+"\n");
                        wFile.write("FIELDS TERMINATED BY \""+pSeparator+"\"\n");
                        wFile.write("("+pColIns+")\n");
                        wFile.write("BEGINDATA\n");	                        
                        
		            	while ((line_ = in.readLine()) != null) {
		                	wFile.append(line_+pSeparator+uploadId+"\n");
		            	}
	            		in.close();
	            		wFile.close();
	            		
                    	String cmd = parser.getServlet().getConfigDir()+"upload\\sqlldr\\"+_ei.sysConst("SqlLoader");
                    	cmd+=" '"+data_loader_file+"'";
                    	                    		
                    	Process pr = Runtime.getRuntime().exec(cmd);
                    	
                    	int exitVal = pr.waitFor();
                    	
                    	if (exitVal!=0){
                            BufferedReader err_ = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
                            for(String l_ = null; (l_ = err_.readLine()) != null;)
                                System.out.println(l_);
                    	}
						
	                }catch(Exception fe){
	                	in.close();
	                	return fe.getMessage();
	                }
            	}                
	            
                if ("1".equals(pDelFile)){
	            	inputFile.delete();
	            }
	            
	            sSQL="declare out_ varchar2(100):=1; begin "+
	            	 "begin insert into ccs_common.log_uploads(upload_id,content,nguoith,mayth,ngayth) values ('"+
	            		uploadId+"','"+
	            		pTableName+"#"+pSchema+"#"+pProc.replace("'","''")+"#"+uploadType
	            		+"','"+
	            		userId+"','"+
	            		userIp+"',sysdate); "+
	            	 "exception when others then out_:=2; end; "+
	            	 " ?:=out_; end;";
	            DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);
                
                //incase user select option delete then don't need to execute function kiemtra_upload_KM
                if(!uploadType.equals(IS_DELETE)) {
		            pProc = pProc.replace("$upload_id$",uploadId);
		            pProc = pProc.replace("$userid$",userId);
		            pProc = pProc.replace("$ma_tinh$",agentCode);
		            
		            sSQL = "begin commit; ?:="+pProc+"; end;";
		          	ret = DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);
		          	System.out.println("Ket qua kiem tra : " +  ret);
		          	
		          	if (!"1".equals(ret)){
		          		return ret;
		          	}
                }
	            
	            ret = "{file:\"" + pSchema + pTableName + uploadId + "." + fileType +
                       	  "\",type:\"" + fileType + 
                          "\",size:" + String.valueOf(fileSize) +
                          ",upload_id:"+uploadId +                          
                          ",uploadtype:" + uploadType +
                          ",errMsg:\"\""
                          + "}";
	            System.out.println("Return value : " + ret);
            }
            System.out.println("----------End Upload : " + new Date());
            return ret;
        } catch (Exception e) {
        	System.out.println("Exception 2 : " + e.getMessage());
        	e.printStackTrace();
        	return retErr;
        }
    }
    
    //check format line
    private String validateLine(String line, String condValid, int lineIdx) {
    	
    	try {
    		/*
    		String[] arrLine = line.split(",");
        	String[] arrCond = condValid.split("#");
        	*/
        	StringTokenizer stLine =  new StringTokenizer(line, ",");
        	StringTokenizer stCond = new StringTokenizer(condValid, "#");
        	
        	int cntLine = stLine.countTokens();
        	int cntCond = stCond.countTokens();
        	
        	if(cntLine < cntCond) {
        		arrErr.add("Line " + lineIdx + ":(" + line + ") Kh&#244;ng &#273;&#7911; d&#7919; li&#7879;u");
        		return "";
        	} else if (cntLine > cntCond) {
        		arrErr.add("Line " + lineIdx + ":(" + line + ") Th&#7915;a d&#7919; li&#7879;u");
        		return "";
        	}
        		
        	String errMsg = "";
        	String len = "";        	
        	String eleCondTmp = "";
        	String lineTmp = "";
        	
        	while (stCond.hasMoreElements()) {
        		eleCondTmp = stCond.nextToken();
        		lineTmp = stLine.nextToken();

        		if(eleCondTmp.toUpperCase().contains("NUMBER")) {
        			if(!isNumber(lineTmp)) {        				
        				errMsg +=  eleCondTmp.substring(0,eleCondTmp.indexOf(":")) + " kh&#244;ng ph&#7843;i ki&#7875;u s&#7889;;";
        			}
        		}
        		
        		if (eleCondTmp.toUpperCase().contains("VARCHAR")){
        			len = eleCondTmp.substring(eleCondTmp.indexOf("(") + 1,eleCondTmp.indexOf(")"));
        			if(lineTmp.length() > Integer.parseInt(len)) {
        				errMsg += eleCondTmp.substring(0,eleCondTmp.indexOf(":")) + " &#272;&#7885; d&#224;i v&#432;&#7907;t qu&#225; gi&#7899;i h&#7841;n cho ph&#233;p(" + len + " character);";
        			}
        		}
        		
        		if(eleCondTmp.toUpperCase().contains("DATE")) {
        			if(!isDate(lineTmp, "dd/MM/yyyy", false)) {
        				errMsg += eleCondTmp.substring(0,eleCondTmp.indexOf(":")) + " kh&#244;ng ph&#7843;i ki&#7875;u date; ";
        			}
        		}
        	}
        	/*
        	for(int i = 0; i < arrCond.length; i++) {
        		if(arrCond[i].toUpperCase().contains("NUMBER")) {
        			if(!isNumber(arrLine[i])) {        				
        				errMsg += " | Field(" + arrCond[i] + " khong phai kieu so";
        			}
        		}
        		if (arrCond[i].toUpperCase().contains("VARCHAR")){
        			len = arrCond[i].substring(arrCond[i].indexOf("(") + 1,arrCond[i].indexOf(""));
        			if(arrLine[i].length() > Integer.parseInt(len)) {
        				errMsg += " | Field(" + arrCond[i] + " do dai vuot qua cho phep";
        			}
        		}
        	}
        	*/
        	if(errMsg.length() > 0 ) {
        		arrErr.add("Line " + lineIdx + ":(" + line + ")-" + errMsg );
        	}
    	} catch (Exception ex) {
    		ex.printStackTrace();
    		System.out.println("Exception: " + ex.getMessage());
    		return null;
    	}
    	return null;    	
    }
    
    private boolean isNumber(String strValue) {
    	for(int i = 0; i < strValue.length() - 1; i++) {
			if(!strNumber.contains(strValue.subSequence(i,i+1))) {
				return false;
			}
		}
    	return true;
    }
    
    private boolean isDate(String maybeDate, String format, boolean lenient) {
		boolean ret = false;

		// test date string matches format structure using regex
		// - weed out illegal characters and enforce 4-digit year
		// - create the regex based on the local format string
		String reFormat = Pattern.compile("d+|M+").matcher(Matcher.quoteReplacement(format)).replaceAll("\\\\d{1,2}");
		reFormat = Pattern.compile("y+").matcher(reFormat).replaceAll("\\\\d{4}");
		
		if (Pattern.compile(reFormat).matcher(maybeDate).matches()) {
			// date string matches format structure,
			// - now test it can be converted to a valid date
			SimpleDateFormat sdf = (SimpleDateFormat) DateFormat.getDateInstance();
			sdf.applyPattern(format);
			sdf.setLenient(lenient);
			try {
				sdf.parse(maybeDate);
				ret = true;
			} catch (ParseException e) {
				ret = false;
			}
		}
		return ret;
	}
    
    private String _toString(ArrayList arr) {
    	String ret = "";
    	for(int i = 0; i < arr.size(); i++) {
    		ret += arr.get(i); 
    	}
    	return ret;
    }
}
