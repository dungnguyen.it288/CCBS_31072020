package neo.smartui.process;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.sql.RowSet;

import neo.smartui.report.DataParameter;
import neo.smartui.report.EnvInfo;
import neo.smartui.report.ReportLayoutParser;

import com.jspsmart.file.Fso;
import com.jspsmart.upload.SmartUpload;
import com.svcon.jdbf.DBFReader;
import com.svcon.jdbf.JDBFException;
import com.svcon.jdbf.JDBField;

/*Ham xu ly file gach no, tra ve thong tin duoi dang json:
 file:Ten file tren may chu
 type:Loai file tren may chu
 size:Kich thuoc file tren may chu
 lantra:ID cua lan tra, lan gach upload file len may chu
 */
 
public class CopyOfFileUploader implements ProcessInterface {
    EnvInfo _ei = null;
    public CopyOfFileUploader() {
    }
    public String process(ReportLayoutParser parser) {
        this._ei = parser.getEnvInfo();
        Fso f = null;
        String return_ = "";
        String sSQL = "";

        String UploadID = "";
        String FileType = "";
        int FileSize = 0;
        String ten ="";
        String temp ="";
        SmartUpload mySmartUpload = parser.getSmartupload();
        String cmdv_datasrc_ = "VNPBilling";
        Map parameters = parser.getParameters();
        
        System.out.println("Aloooooooooooooooooooooooooooooaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!  ");
        
        String LoaiUploadID = ((String) parameters.get("loai_upload_id")).trim();
        String congvan_ = ((String) parameters.get("cbokmcv_id")).trim();
        String hinhthuckm_ = ((String) parameters.get("cbokmht_id")).trim();
        
        /*Bo qua do data la gphone_data - Neu dung cho ccbs thi dung ham commment
        String DataSchema = ((String) parameters.get("sys_dataschema")).trim();
        */
        String DataSchema = ((String) _ei.sysConst("DataSchema")).trim();
        
        String FuncSchema = ((String) parameters.get("sys_funcschema")).trim();
        String MaTinh = ((String) parameters.get("sys_agentcode")).trim();
        String UserId = ((String) parameters.get("sys_userid")).trim();
        String UserIp = ((String) parameters.get("sys_userip")).trim();
        String UsingLoader=((String) parameters.get("using_loader")).trim();
        String data_=((String) parameters.get("datashema_")).trim();
        
        System.out.println("UF-Upload Type: "+LoaiUploadID);
        System.out.println("UF-User IP: "+UserIp);
        System.out.println("UF-Data Schema: "+data_);
        System.out.println("UF-Data MaTinh: "+MaTinh);
        
        String pTableName= "";
        String pSchema= "";
        String pColumns="";
        String pSeparator="";
        String pProc="";
        String pDelFile="";
        String pColIns="";
        String pColIdx="";        
        String pDir="";

        try {
            for (int i = 0; i < mySmartUpload.getFiles().getCount(); i++) {
                String file_name = null;
                com.jspsmart.upload.File myFile = mySmartUpload.getFiles().
                                                  getFile(i);
                FileType = myFile.getFileExt();
                FileSize = myFile.getSize();                
                
                //Lay cac tham so cua cau hinh Upload
                sSQL="select * from -- where id="+LoaiUploadID;
                RowSet para_ = DataParameter.getRSet(_ei,cmdv_datasrc_,sSQL);

                
                while (para_.next()){                
	                pTableName= para_.getString("TABLE_NAME");
	                pSchema= para_.getString("SCHEMA");
	                pColumns=para_.getString("COLUMNS");
	                pSeparator=para_.getString("SEPARATOR");
	                pProc=para_.getString("PROCEDURE");
	                pDelFile=para_.getString("DELETE_FILE");
	                pColIns=para_.getString("COL_INSERTS");
	                pColIdx=para_.getString("COL_INDEXES");
	                pDir=para_.getString("DIR");
                }
                
                
                System.out.println("Thien dai ca day  heeeeeeeeeeeeeeeeeee: "+para_);
                System.out.println("UF-Table Name: "+pTableName);
                System.out.println("UF-Schema Upload: "+data_);
                System.out.println("UF-Column Parameters: "+pColumns);
                System.out.println("UF-Oracle Procedure: "+pProc);
                System.out.println("UF-List Insert Data Columns: "+pColIns);
                System.out.println("UF-List Index Columns: "+pColIdx);
                System.out.println("UF-Temp Directory: "+pDir);
                
                 sSQL = "begin \n" +
                           "  ? := BILLING_DOISOAT.THAMSO.lay_giatri_sequence('SEQ_KHUYENMAI'); \n  end;\n";
                  
                /*sSQL = "begin \n" +
                       "  ? :=" + FuncSchema + "qtht.lay_giatri_sequence('" +
                       UserId + "','"+MaTinh+"','" + data_ +
                       "', 'SEQ_KHUYENMAI',1); \n  end;\n";*/
                
                System.out.println("UF-Cau lenh lay gia tri SEQ: "+sSQL);
                
                UploadID = DataParameter.getValue(_ei,cmdv_datasrc_, sSQL);
                
                System.out.println("UF-Upload ID: "+UploadID);
                
                if (pDir!=null){
                	pDir=pDir+"/";
                }else{
                	pDir="";
                }
                
                SimpleDateFormat simpledateformat = new SimpleDateFormat("ss-mm-HH_dd-MM-yyyy");
                Date date = new Date();
                String cur_=simpledateformat.format(date);
                
                if (!myFile.isMissing()) {
                    file_name = parser.getServlet().getConfigDir() +
                                "upload/" + pDir + pSchema +
                                pTableName + UploadID +"_" + cur_ + "." + FileType;
                        if (f.fileExists(file_name)) {
                            try {
                            	f.deleteFile(file_name);
                            }catch(IOException ioe){
                            	ioe.printStackTrace();
                            }
                        }
                }
                myFile.saveAs(file_name);
                
                System.out.println("UF-File Name: "+file_name);
                
                //Tao bang neu khong ton tai
                sSQL="declare kq_ varchar2(1000):='1'; begin\n begin\n"+
                	  "		execute immediate 'create table "+pSchema+pTableName+"("+pColumns.replace("#",",").replace(":"," ")+")';\n"+
                	  "exception when others then kq_:='2';\n"+
                	  "end; ?:=kq_; end;";
                temp=DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);
                
                System.out.println("Tao bao neeeeeeeeeeeeeeeeeeeeeeeeeeeeee "+sSQL);
               	
               	System.out.println("UF-Ket qua tao bang: "+temp);
               	
               	//Tao index cho bang
               	String[] indexes=pColIdx.split("#");
               	for (int j=0;j<indexes.length;j++){
					
					String[] col_idxs=indexes[j].split(":");
	               	
	               	sSQL="declare kq_ varchar2(1000):='1'; begin\n begin\n"+
	                	  "		execute immediate 'create index "+pSchema+col_idxs[1]+" on "+pSchema+pTableName+"("+col_idxs[0]+")';\n"+
	                	  "exception when others then kq_:='2';\n"+
	                	  "end; ?:=kq_; end;";
	                
	                temp=DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);	               	
	               	System.out.println("UF-Ket qua tao index "+col_idxs[1]+": "+temp);
               	}
                
                File inputFile = new File(file_name);
                if (UsingLoader==null) UsingLoader="0";
                if (Integer.parseInt(UsingLoader)==0){
            		//Xu ly file TEXT                
	                int count_ = 0;
	                if ( FileType.toUpperCase().contains("TXT")){
	                	
	                	System.out.println("UF-Dinh dang FILE TXT");
		                BufferedReader in = new BufferedReader(new FileReader(file_name));
		                String line_;	            	
		                
		                sSQL = "declare psUploadID varchar2(30000);\n"+
		                	   "begin \n"+
		                	   "	begin \n";
		                try{
			            	while ((line_ = in.readLine()) != null) {
			                	line_ = line_.trim();
			                	line_=line_.replace(" ", "");
			                	line_=line_.replace("'","''");
			                	line_=line_.replace(pSeparator,"','");
			                	
			                	sSQL += "insert into " + pSchema+pTableName+"("+pColIns+") values ('" + line_ +"','"+UploadID+"','"+MaTinh+"',"+congvan_+","+hinhthuckm_+","+LoaiUploadID+",'"+UserId+"','"+UserIp+"');\n";
			                	if (count_ % 50 == 0 && count_ != 0) {
			                		//Thuc thi 50 lan 1
			                    	sSQL += "	psUploadID:=" + UploadID + "; \n" +
			                            	"	exception when others then " +
			                            	"   rollback;\n"+
			                            	"	psUploadID := to_char(SQLCODE)||': '||SQLERRM; end; \n" +
			                            	"   ? := psUploadID; end;";
			                        
			                        System.out.println("UF-Load 50 ban ghi: "+sSQL);                            	
			                    	return_ = DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);
			                    	
			                    	System.out.println("UF-So dong thuc hien: "+count_+", ket qua: "+return_);
				                    try {
				                        if (Integer.parseInt(return_) != Integer.parseInt(UploadID)) {
				                            return return_;
				                        }
				                    } catch (Exception ex) {
				                    	in.close();
				                        return return_;
				                    }
				
				                    sSQL = "declare psUploadID varchar2(30000);\n"+
				                	   	   "begin \n"+
				                	   	   "	begin \n";
			                	}
			                	count_ += 1;
			            	}
		            		in.close();
		                }catch(Exception fe){
		                	in.close();
		                	return fe.getMessage();
		                }
	                }else{
	                	
	                	//Thuc hien xu ly file DBF
	                	System.out.println("UF-Dinh dang FILE DBF");
	                	DBFReader dbfRead = new DBFReader(file_name);
	                	System.out.println(file_name);
	                	System.out.println(dbfRead.toString());
	                	                	
				        JDBField[] fields = new JDBField[dbfRead.getFieldCount()];			        
		                
		                sSQL = "declare psUploadID varchar2(30000);\n"+
		                	   "begin \n"+
		                	   "	begin \n";
				        try {
				            while (dbfRead.hasNextRecord()) {
				            	
				            	String line_="";
				            	String cell_="";
				            	
				            	Object	[] record = dbfRead.nextRecord();
				                for (int k = 0; k < fields.length; k++) {
				                	cell_ = cell_.trim();
		                			cell_=cell_.replace(" ", "");
		                			cell_=cell_.replace("'","''");
		                				                			
				                	line_ += "'" + record[k].toString() + "',";
				                }
				                line_=line_.substring(1,line_.length()-1);
				            	sSQL += "insert into " + pSchema+pTableName+"("+pColIns+") values ('" + line_ +"','"+UploadID+"');\n";
				            	
				            	if (count_ % 50 == 0 && count_ != 0) {
			                		//Thuc thi 50 lan 1
			                    	sSQL += "	psUploadID:=" + UploadID + "; \n" +
			                            	"	exception when others then " +
			                            	"   rollback;\n"+
			                            	"	psUploadID := to_char(SQLCODE)||': '||SQLERRM; end; \n" +
			                            	"   ? := psUploadID; end;";
			                        
			                        System.out.println("UF-Load 50 ban ghi: "+sSQL);                            	
			                    	return_ = DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);
			                    	
			                    	System.out.println("UF-So dong thuc hien: "+count_+", ket qua: "+return_);
				                    try {
				                        if (Integer.parseInt(return_) != Integer.parseInt(UploadID)) {
				                            return return_;
				                        }
				                    } catch (Exception ex) {
				                        return return_;
				                    }
				
				                    sSQL = "declare psUploadID varchar2(30000);\n"+
				                	   	   "begin \n"+
				                	   	   "	begin \n";
			                	}
			                	count_ += 1;
				            }
				            dbfRead.close();
				        } catch (Exception e) {
				            dbfRead.close();			            
				            return e.getMessage();
				        }
	                }
	            	
	            	sSQL += "	psUploadID:=" + UploadID + "; \n" +
	                	"	exception when others then " +
	                	"   rollback;\n"+
	                	"	psUploadID := to_char(SQLCODE)||': '||SQLERRM; end; \n" +
	                	"   ? := psUploadID; end;";                                    
	                
	                System.out.println("UF-Load lan cuoi: "+sSQL);            
		            return_ = DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);
		            
		            System.out.println("UF-So dong thuc hien: "+count_+", ket qua: "+return_);
		            try {
	                    if (Integer.parseInt(return_) != Integer.parseInt(UploadID)) {
	                        return return_;
	                    }
	                } catch (Exception ex) {
	                    return return_;
	                }
            		//Ket thuc xu ly TXT, DBF
            	}else{
            		System.out.println("UF-Su dung SQL Data Loader");
            		//Xu ly file de SQL Loader
            		System.out.println("UF-Dinh dang FILE TXT");
		            BufferedReader in = new BufferedReader(new FileReader(file_name));
		            String line_;
		            String data_loader_file="";
		            try{
		            	data_loader_file = parser.getServlet().getConfigDir() +
                            "upload\\sqlldr\\" + cur_ + ".data";
                        if (f.fileExists(data_loader_file)) {
                            try {
                            	f.deleteFile(data_loader_file);
                            }catch(IOException ioe){
                            	ioe.printStackTrace();
                            }
                        }
                        BufferedWriter wFile = new BufferedWriter(new FileWriter(data_loader_file));
                        wFile.write("LOAD DATA\n");
                        wFile.write("INFILE *\n");
                        wFile.write("APPEND INTO TABLE "+pSchema+pTableName+"\n");
                        wFile.write("FIELDS TERMINATED BY \""+pSeparator+"\"\n");
                        wFile.write("("+pColIns+")\n");
                        wFile.write("BEGINDATA\n");	                        
                        
		            	while ((line_ = in.readLine()) != null) {
		                	wFile.append(line_+pSeparator+UploadID+"\n");
		            	}
	            		in.close();
	            		wFile.close();
	            		
                    	String cmd = parser.getServlet().getConfigDir()+"upload\\sqlldr\\"+_ei.sysConst("SqlLoader");
                    	cmd+=" '"+data_loader_file+"'";
                    	                    		
                    	Process pr = Runtime.getRuntime().exec(cmd);
                    	
                    	int exitVal = pr.waitFor();
                    	
                    	if (exitVal!=0){
                    		System.out.println("UF-Loi Loader File "+cur_);
                            BufferedReader err_ = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
                            for(String l_ = null; (l_ = err_.readLine()) != null;)
                                System.out.println(l_);
                    	}
						
	                }catch(Exception fe){
	                	in.close();
	                	return fe.getMessage();
	                }
            		//ket thuc xu ly Sql Loader
            	}                
	            
	            //Thay cac tham so muc dinh
	            pProc=pProc.replace("$upload_id$",UploadID);
	            pProc=pProc.replace("$data_schema$",data_);
	            pProc=pProc.replace("$func_schema$",FuncSchema);
	            pProc=pProc.replace("$userid$",UserId);
	            pProc=pProc.replace("$userip$",UserIp);
	            pProc=pProc.replace("$ma_tinh$",MaTinh);
	            
	            //THuc hien lenh cuoi cung sau khi upload song
	            System.out.println("Cau lenh em day cac bac  aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa : "+pProc);
	            
	            sSQL="begin commit; ?:="+pProc+"; end;";
	          	return_ = DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);
	          	System.out.println("UF-Ket qua thuc hien lenh cuoi cung: "+return_);
	          	
	          	if (Integer.parseInt(return_)!=1){
	          		return return_;
	          	}
	            	            
	            if (Integer.parseInt(pDelFile)==1){
	            	inputFile.delete();
	            }
	            
	            sSQL="declare out_ varchar2(100):=1; begin "+
	            	 "begin insert into ccs_common.log_uploads(upload_id,content,nguoith,mayth) values ('"+
	            		UploadID+"','"+
	            		pTableName+"#"+pSchema+"#"+pProc.replace("'","''")+"','"+
	            		UserId+"','"+
	            		UserIp+"'); "+
	            	 "exception when others then out_:=2; end; "+
	            	 " ?:=out_; end;";
	            System.out.println("UF-Cau lenh Log Upload: "+sSQL);
	            temp=DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);
	            System.out.println("UF-Ghi log thuc hien upload: "+UploadID);
	            
	            return_ = "{file:\"" + pSchema + pTableName + UploadID + "." + FileType +
                       	  "\",type:\"" + FileType + 
                          "\",size:" + String.valueOf(FileSize) +
                          ",upload_id:"+UploadID+ "}";
            }
            return return_;
        } catch (Exception e) {
            return (e.getMessage());
        }
    }
    
    public String Text(EnvInfo Env,String FileName, String DataSchema,
                       String FuncSchema, String UserID, String Kyhoadon,
                       String Lantra, String FileType, int FileSize) throws
            JDBFException,
            Exception {
        String sSQL = "";
        String return_ = "";
        File inputFile = new File(FileName);
        try {
            BufferedReader in = new BufferedReader(new FileReader(
                    inputFile));
            String line_;
            int count_ = 0;
            sSQL =
                    "declare psLantra varchar2(30000); begin \n begin \n";
            while ((line_ = in.readLine()) != null) {
                line_ = line_.trim().replace(" ", "").replace(",",
                        "','");
                sSQL += "insert into " + DataSchema +
                        "CT_TRA_FILE_" + Kyhoadon +
                        " values (" + Lantra + ",'" + line_ +
                        "',null,null); \n";
                if (count_ % 50 == 0 && count_ != 0) {
                    sSQL += "     psLantra:=" + Lantra + "; \n" +
                            "exception when others then " +
                            "     rollback; psLantra := to_char(SQLCODE)||': '||SQLERRM; end; \n" +
                            "  ? := psLantra; end;";
                    return_ = DataParameter.getValue(_ei, sSQL);
                    try {
                        if (Integer.parseInt(return_) !=
                            Integer.parseInt(Lantra)) {
                            return return_;
                        }
                    } catch (Exception e) {
                        return return_;
                    }

                    sSQL =
                            "declare psLantra varchar2(30000); begin \n begin \n";

                }
                count_ += 1;
            }
            sSQL += "  psLantra:="+FuncSchema+"qltn_tracuu.kiemtra_gachnofile("+
                    "            '"+Kyhoadon+"',"+
                    "            '"+Lantra+"',"+
                    "            '"+DataSchema+"',"+
                    "            '"+UserID+"'); \n"+
                    " commit; \n" +
                    " exception when others then " +
                    "   rollback; psLantra := to_char(SQLCODE)||': '||SQLERRM; end; \n" +
                    "  ? := psLantra; end;";
            return_ = DataParameter.getValue(_ei,sSQL);
            in.close();
            inputFile.delete();


        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            int count_ = Integer.parseInt(return_);
            return_ = "{file:\"" + DataSchema + "CT_TRA_FILE_" +
                      Kyhoadon + "\",type:\"" +
                      FileType + "\",size:" +
                      String.valueOf(FileSize) +
                      ",lantra:"+Lantra+ "}";
            return return_;
        } catch (Exception ex2) {
            return return_;
        }

    }
    
}
