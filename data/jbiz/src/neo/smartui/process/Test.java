package neo.smartui.process;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {
	public static void main(String[] args) {
		/*
		 * String str = "0123456789";
		 * 
		 * String strValue = "-322aa";
		 * 
		 * for(int i = 0; i < strValue.length(); i++) { if(!str.contains("" +
		 * strValue.charAt(i))) { System.out.println("Character : " +
		 * strValue.charAt(i) + " not match"); } }
		 * System.out.println("Success!!");
		 */
		// System.out.println(strValue.matches(str));
		/*
		 * String str = "varchar2(20)";
		 * 
		 * System.out.println(str.substring(str.indexOf("(") +
		 * 1,str.indexOf(")")));
		 * 
		 * ArrayList arr = new ArrayList(); arr.add("aaa"); arr.add("bbb");
		 * arr.add("ccc"); System.out.println(arr.toString());
		 * 
		 * String strTmp = "SO_TB:varchar2(20)#NGAY_LD:VARCHAR(50)"; String[]
		 * arrTmp = strTmp.split("#");
		 * 
		 * StringTokenizer st = new StringTokenizer(strTmp, "#");
		 * 
		 * System.out.println(st.countTokens());
		 */
		/*
		 * String strDate = "12/31/2010"; System.out.println(isDate(strDate));
		 */
		// used like this:
		Date date = parseDate("31/3/20", "dd/MM/yyyy", false);

		System.out.println("Date : " + date);
		String test = "Test:111";
		System.out.println(test.substring(0,test.indexOf(":")));
	}

	private static boolean isDate(String strDate) {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			System.out.println("date : " + dateFormat.parse(strDate));
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	private static Date parseDate(String maybeDate, String format,boolean lenient) {
		boolean ret = false;
		Date date = null;
		// test date string matches format structure using regex
		// - weed out illegal characters and enforce 4-digit year
		// - create the regex based on the local format string
		String reFormat = Pattern.compile("d+|M+").matcher(Matcher.quoteReplacement(format)).replaceAll("\\\\d{1,2}");
		reFormat = Pattern.compile("y+").matcher(reFormat).replaceAll("\\\\d{4}");
		
		if (Pattern.compile(reFormat).matcher(maybeDate).matches()) {
			// date string matches format structure,
			// - now test it can be converted to a valid date
			SimpleDateFormat sdf = (SimpleDateFormat) DateFormat.getDateInstance();
			sdf.applyPattern(format);
			sdf.setLenient(lenient);
			try {
				date = sdf.parse(maybeDate);
				ret = true;
			} catch (ParseException e) {
				ret = false;
			}
		}
		return date;
	}

}
