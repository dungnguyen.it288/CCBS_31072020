package neo.smartui.process;

import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class writeToText {
	//read excel file
	InputStream myxls = new FileInputStream("E:/Source_Ref/Java_Word_Excel/ExportToExcel/Stock.xls");
	HSSFWorkbook wb     = new HSSFWorkbook(myxls);
	
	HSSFSheet sheet = wb.getSheetAt(0);       // first sheet
	HSSFRow row     = sheet.getRow(3);        // row 3
	HSSFCell cell   = row.getCell((short)0);  // cell 0
	
	if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
		System.out.println("Cell String value :  " + cell.getStringCellValue());
	} else if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
		System.out.println("Cell Number value :  " + cell.getNumericCellValue());
	} else {
		System.out.println("Other value");
	}
	
	/*
	Date d = new Date();
	Calendar cl = Calendar.getInstance();
	System.out.println(cl.get(Calendar.DATE));
	//System.out.println("Milisecond : " + cl.getTimeInMillis());
	*/
	
	/*
	Date now = new Date(1900,1,1,0,0);
     long nowLong = now.getTime();
    Date test = new Date(0);
    System.out.println("Today is " + now);
    System.out.println("nowLong " + nowLong);
    System.out.println("Date " + test);
	*/
}
