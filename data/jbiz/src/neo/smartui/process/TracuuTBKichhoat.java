package neo.smartui.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import neo.smartui.report.DataParameter;
import neo.smartui.report.EnvInfo;
import neo.smartui.report.ReportLayoutParser;

import com.jspsmart.file.Fso;
import com.jspsmart.upload.SmartUpload;

public class TracuuTBKichhoat implements ProcessInterface {
	EnvInfo _ei = null;
	
	public String process(ReportLayoutParser parser) {
		this._ei = parser.getEnvInfo();
        String ret = "";
        String sSQL = "";

        SmartUpload mySmartUpload = parser.getSmartupload();
        String cmdv_datasrc_ = "VNPBilling";
        Map parameters = parser.getParameters();
        
        String userId = ((String) parameters.get("sys_userid")).trim();
        
        String tracuuId = "";
        String fileType = "";
        String retErr = "0";
        
        try {
        	
        	String fileDir = parser.getServlet().getConfigDir() +
	        	"upload\\" + "Doisoat\\Tracuu";
	    	
	    	if(!Fso.folderExists(fileDir)) {
	    		Runtime.getRuntime().exec("cmd /C mkdir " + fileDir);
	    	}
        	
            for (int i = 0; i < mySmartUpload.getFiles().getCount(); i++) {
                String fileName = null;
                com.jspsmart.upload.File myFile = mySmartUpload.getFiles().
                                                  getFile(i);
                fileType = myFile.getFileExt();
                
                 sSQL = "begin \n" +
                           "  ? := BILLING_DOISOAT.THAMSO.lay_giatri_sequence('SEQ_TRACUU_TB_ACTIVE'); \n  end;\n";
                  
                tracuuId = DataParameter.getValue(_ei,cmdv_datasrc_, sSQL);
                
                SimpleDateFormat simpledateformat = new SimpleDateFormat("ss-mm-HH_dd-MM-yyyy");
                Date date = new Date();
                String cur_ = simpledateformat.format(date);
                
                if (!myFile.isMissing()) {
                	
                    fileName = fileDir + "/" + tracuuId + "_" + cur_ + "." + fileType;
                		
                    if (Fso.fileExists(fileName)) {
                        try {
                        	Fso.deleteFile(fileName);
                        }catch(IOException ioe){
                        	ioe.printStackTrace();
                        }
                    }
                }
                myFile.saveAs(fileName);
                
                //create table if not exist
                sSQL="declare kq_ varchar2(1000):='1'; begin\n begin\n"+
                	  "		execute immediate 'create table ccs_common.tracuu_tb_kichhoat(id number,so_tb varchar2(20),nguoi_dung varchar2(50),ngay_thang date)';\n"+
                	  "exception when others then kq_:='2';\n"+
                	  "end; ?:=kq_; end;";
                DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);
               	
                File inputFile = new File(fileName);
                int cnt = 0;
                if (fileType.toUpperCase().contains("TXT")) {
	                String line;
	                BufferedReader in = new BufferedReader(new FileReader(fileName));
	                sSQL = "declare psUploadID varchar2(30000);\n"+
	                	   "begin \n"+
	                	   "	begin \n";
	                
	                try {
		            	while ((line = in.readLine()) != null) {
		                	line = line.trim();
		                	line = line.replace(" ", "");		                	
		                	if(line.length() == 0) {
		                		continue;
		                	}
		                	sSQL += "insert into ccs_common.tracuu_tb_kichhoat(id,so_tb,nguoi_dung,ngay_thang) values(" + tracuuId + ",'"+line+"','"+userId+"',sysdate);\n";
		                	
		                	if (cnt % 200 == 0 && cnt != 0) {
		                		//Thuc thi 200 lan 1
		                    	sSQL += "	psUploadID:=" + tracuuId + "; \n" +
		                            	"	exception when others then " +
		                            	"   rollback;\n"+
		                            	"	psUploadID := to_char(SQLCODE)||': '||SQLERRM; end; \n" +
		                            	"   ? := psUploadID; end;";
		                    	ret = DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);
			                    
		                    	try {
			                        if (Integer.parseInt(ret) != Integer.parseInt(tracuuId)) {
			                            return ret;
			                        }
			                    } catch (NumberFormatException ex) {
			                    	in.close();
			                    	System.out.println("NumberFormatException: " + ex.getMessage());
			                        return retErr;
			                    }
			
			                    sSQL = "declare psUploadID varchar2(30000);\n"+
			                	   	   "begin \n"+
			                	   	   "	begin \n";
		                	}
		                	cnt += 1;
		            	}
	            		in.close();
	                } catch(Exception fe){
	                	in.close();
	                	System.out.println("Exception : " + fe.getMessage());
	                	return retErr;
	                }
                }
            	
            	sSQL += "	psUploadID:=" + tracuuId + "; \n" +
                	"	exception when others then " +
                	"   rollback;\n"+
                	"	psUploadID := to_char(SQLCODE)||': '||SQLERRM; end; \n" +
                	"   ? := psUploadID; end;";                                    
            	
	            ret = DataParameter.getValue(_ei,cmdv_datasrc_,sSQL);
	            
	            try {
                    if (Integer.parseInt(ret) != Integer.parseInt(tracuuId)) {
                        return ret;
                    }
                } catch (NumberFormatException ne) {
                	System.out.println("NumberFormatException 2 : " + ne.getMessage());
                	return retErr;
                }
            
	            inputFile.delete();
	            
	            ret = tracuuId;
            }
            return ret;
        
        } catch(Exception ex) {
        	ex.printStackTrace();
        	return retErr;
        }
	}	
}
