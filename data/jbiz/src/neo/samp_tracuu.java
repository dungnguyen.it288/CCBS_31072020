package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class samp_tracuu
extends NEOProcessEx {
{
	public void run()
	{
		System.out.println("neo.pttb_tracuu was called");
	}
  
  public String get_cbophongban(String psMacq) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/ajax_phongban"
		+"&"+CesarCode.encode("macq") + "=" + psMacq
        ;
	}
	
  public String load_cbophuong(String pmahuyen) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/TRACUU/ajax_cbophuong"
        +"&"+CesarCode.encode("mahuyen")+"="+pmahuyen
        ;
   }  
   
   public String load_DSpho(String piphuong_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/Danhmuc/ajax_DSpho"
        +"&"+CesarCode.encode("phuong_id")+"="+piphuong_id
        ;
   }
	public String tracuuhd(
		String psTEN_CQ
		, String psDIACHICQ
		, String psCMT
		, String psHOKHAU
		, String psTENTB
		, String psDIACHITB
		, String psMATB
		, String psTENTT
		, String psSOMAY_TN
		, String piHDORTB
		)
	{
		return "/main?" + CesarCode.encode("configFile") + "=pttb/tracuu/tracuu_hd/ajax_DStracuu"
			+ "&" + CesarCode.encode("tencq") + "=" + psTEN_CQ
			+ "&" + CesarCode.encode("diachicq") + "=" + psDIACHICQ
			+ "&" + CesarCode.encode("cmt") + "=" + psCMT
			+ "&" + CesarCode.encode("hokhau") + "=" + psHOKHAU
			+ "&" + CesarCode.encode("tentb") + "=" + psTENTB
			+ "&" + CesarCode.encode("diachitb") + "=" + psDIACHITB
			+ "&" + CesarCode.encode("matb") + "=" + psMATB
			+ "&" + CesarCode.encode("tentt") + "=" + psTENTT
			+ "&" + CesarCode.encode("somay_tn") + "=" + psSOMAY_TN
			+ "&" + CesarCode.encode("hdortb") + "=" + piHDORTB
			;
	}
	public String samp_tracuu(
		String psTHANG
		, String piDONVIQL_ID
		, String piDICHVUVT_ID
		)
	{
		return "/main?" + CesarCode.encode("configFile") + "=samp/ajax_DStracuu"
			+ "&" + CesarCode.encode("thang") + "=" + psTHANG
			+ "&" + CesarCode.encode("dvql_id") + "=" + piDONVIQL_ID
			+ "&" + CesarCode.encode("dichvuvt_id") + "=" + piDICHVUVT_ID
			;
	}
	
 public String update_frmphos(String schema
									,String piPHUONG_ID
									,String psDSpho_id
									
									) {
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.danhmuc_update_frmPhos("
       							+piPHUONG_ID+","
								+"'"+psDSpho_id+"'"
								
        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }  
  public String laytt_tbcd(String schema,String ma_tb, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_tbcd('"+ma_tb+"','"+CesarCode.decode(userid)+"'); end;";
  }
  public String laytt_tbdd(String schema, String ma_tb, String userid)
  {
	  return "begin ?:=" + CesarCode.decode(schema) + "pttb_tracuu.laytt_tbdd('" + ma_tb + "','" + CesarCode.decode(userid) + "'); end;";
  }
  public String laytt_tbins(String schema, String ma_tb, String userid)
  {
	  return "begin ?:=" + CesarCode.decode(schema) + "pttb_tracuu.laytt_tbins('" + ma_tb + "','" + CesarCode.decode(userid) + "'); end;";
  }
  public String layds_hopdong_theo_tb(String psMA_TB) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tracuu/tttb/ajax_layds_hopdong"
        +"&"+CesarCode.encode("ma_tb")+"="+psMA_TB
        ;
  }
  public String layds_notonghop(String psMA_TB) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tracuu/tttb/ajax_lay_no_tonghop"
        +"&"+CesarCode.encode("ma_tb")+"="+psMA_TB
        ;
  }
  public String layds_nochitiet(String psMA_TB,String piLOAITIEN_ID) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tracuu/tttb/ajax_lay_no_chukyno"
        +"&"+CesarCode.encode("ma_tb")+"="+psMA_TB
		+"&"+CesarCode.encode("loaitien_id")+"="+piLOAITIEN_ID
        ;
  }

}