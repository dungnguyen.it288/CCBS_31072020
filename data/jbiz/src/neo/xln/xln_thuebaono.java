package neo.xln;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class xln_thuebaono extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("QLTN Gach no");
		//funcschema=getSysConst("FuncSchema");
    	//dataschema=getUserVar("sys_dataschema");
    	//userid=getUserVar("userID");		
	}
	public String doc_thuebaono()
	{
		return "/main?" + CesarCode.encode("configFile")+"=xln/xln_thuebaono_km/ajax_thuebaono";
	}
	public String doc_thuebao_km()
	{
		return "/main?" + CesarCode.encode("configFile")+"=xln/xln_thuebaono_km/ajax_thuebao_km";
	}
	public String doc_thongtin_tb_no(
		String psSoMay,
		String psTenTB,
		String psDiaChiCT,
		String psMaKH,		
		String psCoQuan,
		String psDiaChiKH,
		String psMaTT,
		String psTenTT,
		String psDiaChiTT,
		String psDonvi_ID,
		String psMaNV,
		String psLoaiTien_ID,
		String psMaxOfTien
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=xln/xln_thuebaono_km/ajax_ds_thongtin_no_th"
			+ "&" + CesarCode.encode("SoMay") + "=" + psSoMay
			+ "&" + CesarCode.encode("TenTB") + "=" + psTenTB
			+ "&" + CesarCode.encode("DiaChiCT") + "=" + psDiaChiCT
			+ "&" + CesarCode.encode("MaKH") + "=" + psMaKH
			+ "&" + CesarCode.encode("CoQuan") + "=" + psCoQuan
			+ "&" + CesarCode.encode("DiaChiKH") + "=" + psDiaChiKH
			+ "&" + CesarCode.encode("MaTT") + "=" + psMaTT
			+ "&" + CesarCode.encode("TenTT") + "=" + psTenTT
			+ "&" + CesarCode.encode("DiaChiTT") + "=" + psDiaChiTT
			+ "&" + CesarCode.encode("Donvi_ID") + "=" + psDonvi_ID
			+ "&" + CesarCode.encode("MaNV") + "=" + psMaNV
			+ "&" + CesarCode.encode("LoaiTien_ID") + "=" + psLoaiTien_ID
			+ "&" + CesarCode.encode("MaxOfTien") + "=" + psMaxOfTien;
	}
	public String doc_thuebaokhoamay(	
		String psMaTB,
		String psKhoaTu,
		String psKhoaDen,
		String psKyHoaDon,
		String psDonvi_ID
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=xln/xln_thuebaono_km/ajax_ds_thongtin_km"
			+ "&" + CesarCode.encode("MaTB") + "=" + psMaTB
			+ "&" + CesarCode.encode("KhoaTu") + "=" + psKhoaTu
			+ "&" + CesarCode.encode("KhoaDen") + "=" + psKhoaDen
			+ "&" + CesarCode.encode("KyHoaDon") + "=" + psKyHoaDon
			+ "&" + CesarCode.encode("Donvi_ID") + "=" + psDonvi_ID;
	}
	public String value_themhoso_no(
		String psToXLN,
		String psMaKH,
		String psMaTB,
		String psGhiChu
	)
	{
		return "begin ?:="+getSysConst("FuncSchema")+"xulyno.themHosoMoi("			
			+"'"+psMaKH+"',"
			+"'"+psMaTB+"',"
			+"'"+psToXLN+"',"
			+"'"+psGhiChu+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
	}
}