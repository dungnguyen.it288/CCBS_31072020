package neo.xln;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;

public class xulyno
    extends NEOProcessEx {
  public void run() {
    System.out.println("neo.xulyno was called");
  }
  
  public String ds_gt(String xuly_id)
  {
	  return "/main?" + CesarCode.encode("configFile") + "=xln/ajax_ds_gt_xl"
	  +"&"+CesarCode.encode("xulyid") + "=" + xuly_id
	  ;
  }
  
public String ds_nv_tt(String to)
{
	if(to=="1=1")
	{
		return "/main?"+CesarCode.encode("configFile")+"=xln/dsnhanvien/DSQuanTheoTo_ajax_quan"
		
		;
	}
	else 
	{	
		return "/main?"+CesarCode.encode("configFile")+"=xln/dsnhanvien/DSQuanNv_ajax_tt"
		+"&"+CesarCode.encode("toxuly") + "=" + to
		;
	}
}
  public String layds_quan() {
    return "/main?"+CesarCode.encode("configFile")+"=xln/DSQuanTheoTo/DSQuanTheoTo_ajax_quan"
        ;
  }
  public String layds_tinh() {
    return "/main?"+CesarCode.encode("configFile")+"=xln/DSQuanTheoTo/DSQuanTheoTo_ajax_tinh"
        ;
  }
  public String layds_quan_tt(String psto_id) {
    return "/main?"+CesarCode.encode("configFile")+"=xln/DSQuanTheoTo/DSQuanTheoTo_ajax_quan_tt"
		+"&"+CesarCode.encode("to_id") + "=" + psto_id
        ;
  }
  public String layds_tinh_tt(String psto_id) {
    return "/main?"+CesarCode.encode("configFile")+"=xln/DSQuanTheoTo/DSQuanTheoTo_ajax_tinh_tt"
		+"&"+CesarCode.encode("to_id") + "=" + psto_id
        ;
  }
  
   //Da sua
  public String update_frmquan(String schema,String sSchema,String sMaTinhNgDung,String sUserId
									,String piTO_ID
									,String psDSquan_id)
	{
	 String s= "begin ? := "+schema+"XULYNO.update_frmquans("
								 +"'"+sSchema+"',"
							     +"'"+sMaTinhNgDung+"',"
							     +"'"+sUserId+"',"
       							 +piTO_ID+","
								 +"'"+psDSquan_id+"'"
								
        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }
  //Da sua
  public String update_frmtinh(String schema,String sSchema,String sMaTinhNgDung,String sUserId
									,String piTO_ID
									,String psDStinh_id
									
									) {
	
    String s= "begin ? :=  "+schema+"XULYNO.update_frmtinhs("
								 +"'"+sSchema+"',"
							     +"'"+sMaTinhNgDung+"',"
							     +"'"+sUserId+"',"
       							 +piTO_ID+","
								 +"'"+psDStinh_id+"'"
								
								+");"
								+"end;"
        ;
    System.out.println(s);
    return s;

  }
  
   public String layds_lxl() {
    return "/main?"+CesarCode.encode("configFile")+"=xln/KieuXuLy/DSLoaixuly_ajax"
		;
  }
  
 //Da sua
 public String add_lxl(String schema,String psSchema,String sMaTinhNgDung,String sUserId,String psten_id) 
  {	
    String s= "begin ? := "+schema+"XULYNO.them_kieuxuly("
							   +"'"+psSchema+"',"
							   +"'"+sMaTinhNgDung+"',"
							   +"'"+sUserId+"',"
       						   +"'"+psten_id+"'"						
							   +");"
							   +"end;"
                           ;
    return s;

  }
  //Da sua
  public String update_lxl(String schema,String psSchema,String sMaTinhNgDung,String sUserId,String piid_id,String psten_id) 
  {
    String s= "begin ? := "+schema+"XULYNO.capnhat_kieuxuly("
							   +"'"+psSchema+"',"
							   +"'"+sMaTinhNgDung+"',"
							   +"'"+sUserId+"',"
       						   +piid_id+","
							   +"'"+psten_id+"'"				
							   +");"
							   +"end;"
								;

    return s;

  }
  
  //Da sua
  public String del_lxl(String schema,String psSchema,String sMaTinhNgDung,String sUserId,String piid_id) 
	{ String s= "begin ? := "+schema+"XULYNO.xoa_kieuxuly("
							   +"'"+psSchema+"',"
							   +"'"+sMaTinhNgDung+"',"
							   +"'"+sUserId+"',"	
       						   +piid_id+									
							   +");"
							   +"end;"
								;
    return s;

  }
//Da sua
 public String layds_tra() {
    return "/main?"+CesarCode.encode("configFile")+"=xln/dsnhanvien/DSQuanTheoTo_ajax_quan"
        ;
    }

//Da sua
public String xoaNV_XLNf(String schema,String psSchema,String sMaTinhNgDung,String sUserId,String psnv_id) 
	{String s= "begin ? := "+schema+"XULYNO.xoaNV_XLN("
								+"'"+psSchema+"',"
							    +"'"+sMaTinhNgDung+"',"
							    +"'"+sUserId+"',"
       							+"'"+psnv_id+"'"								
								+");"
								+"end;"
							        ;
 
    return s;
  }	

//Da sua  
public String THEMNV_XLNf(String schema,String sSchema,String sMaTinhNgDung,String sUserId,String hoten,String ma,String ngaycap,String login,String to)
 {	
    String s= "begin ? := "+schema+"XULYNO.THEMNV_XLN("
       							+"'"+sSchema+"',"
							    +"'"+sMaTinhNgDung+"',"
							    +"'"+sUserId+"',"
								+"'"+hoten+"',"
								+"'"+ma+"',"
								+"'"+ngaycap+"',"
								+"'"+login+"',"
								+to+
								+");"
								+"end;"
								;
     return s;

  }
  
//Da sua
public String updateNV_XLNf(String schema,String sSchema,String sMaTinhNgDung,String sUserId,String id,String hoten,String ma,String ngaycap,String login ,String to) 
{	
    String s= "begin ? := "+schema+"XULYNO.updateNV_XLN("
       							+"'"+sSchema+"',"
							    +"'"+sMaTinhNgDung+"',"
							    +"'"+sUserId+"',"
								+id
								+",'"+hoten+"'"
								+",'"+ma+"'"
								+",'"+ngaycap+"'"
								+",'"+login+"'"
								+","+to
								+");"
								+"end;"
        ;
 
    return s;
}
 
 //Cac loai giay to
 public String capnhatdulieu()
	{
		return "/main?" + CesarCode.encode("configFile") + "=xln/ajax_danhsachgiayto";
					
	}   

public String lay_tong_loaigt(String schema,String sSchema,String sMaTinhNgDung,String sUserId) 
 {  
	String  s=  "begin ?:= "+schema+"XULYNO.lay_tong_loaigt("
					+"'"+sSchema+"',"
					+"'"+sMaTinhNgDung+"',"
					+"'"+sUserId+"'"
					+");"
					+"end;"
					;
    return s;
  }
  
public String themmoi_giayto(String schema,
							String sSchema,
							String sMaTinhNgDung,
							String sUserId,
							String pstxt_ten,
							String buocxuly,
							String pstxt_ketqua,
							String pstxt_thongtin,								
							String pstxt_lenhxuat,
							String pstxt_viettat,
							String pstxt_tenfile	 
					  ) 
 {String s=  "begin ?:= "+schema+"XULYNO.them_giayto("
						+"'"+sSchema+"',"
						+"'"+sMaTinhNgDung+"',"
						+"'"+sUserId+"',"
						+"'"+pstxt_ten+"',"
						+buocxuly+","						
						+"'"+pstxt_ketqua+"',"
						+"'"+pstxt_thongtin+"',"
						+"'"+pstxt_lenhxuat+"',"
						+"'"+pstxt_viettat+"',"
						+"'"+pstxt_tenfile+"'"
						+");"
						+"end;"
						;					
    System.out.println(s);	
	return s;	
 }  
 
public String capnhat_giayto( String schema,
								String sSchema,
								String sMaTinhNgDung,
								String sUserId,
    							String pstxt_id,
								String pstxt_ten,
								String buocxuly,
								String pstxt_ketqua,
                                String pstxt_thongtin,								
                                String pstxt_lenhxuat,
                                String pstxt_viettat,
								String pstxt_tenfile                                  
                               ) 
 {String  s=  "begin ?:= "+schema+"XULYNO.capnhat_giayto("
						+"'"+sSchema+"',"
						+"'"+sMaTinhNgDung+"',"
						+"'"+sUserId+"',"
						+pstxt_id
						+",'"+pstxt_ten+"'"
						+","+buocxuly
						+",'"+pstxt_ketqua+"'"
						+",'"+pstxt_thongtin+"'"
						+",'"+pstxt_lenhxuat+"'"
						+",'"+pstxt_viettat+"'"
						+",'"+pstxt_tenfile+"'"
						+");"
						+"end;"
						;
			return s;
 }

public String xoa_giayto(String schema,
						  String sSchema,
						  String sMaTinhNgDung,
						  String sUserId,
    					  String pstxt_id						                                           
						) 
 {  String  s="begin ?:= "+schema+"XULYNO.xoaloaigiayto("
				+"'"+sSchema+"',"
				+"'"+sMaTinhNgDung+"',"
				+"'"+sUserId+"',"
				+"'"+pstxt_id+"'"
				+");"
				+"end;"
				;
    return s;
  }
  
 //Danh sach tra 
 public String getDanhSachTraTien_TT_Ttienf(String sSchema,
										String sMaTinhNgDung,
										String sUserId,
										String tu_n,
										String den_n,
										String to,
										String tinhtrang)
		{			
			return "/main?"+CesarCode.encode("configFile")+"=xln/ds_ajax_dstra"
				+"&"+CesarCode.encode("tu_n")+"="+tu_n
				+"&"+CesarCode.encode("den_n")+"="+den_n
				+"&"+CesarCode.encode("to")+"="+to
				+"&"+CesarCode.encode("tinhtrang")+"="+tinhtrang
				; 	
		}

// Xem ho so
	public String capnhat_ttToaAn(	   String func_schema
									  ,String data_schema
									  ,String agent_schema
									  ,String user_id

									,String pdNgayxetxu
									,String psBanan ,String pdNgaybanan ,String psSotien_tamung_anphi
									,String psBienlai_tamung_anphi ,String pdNgaytamunganphi
									,String psBienlai_hoan_anphi 
									,String psSotien_tamthu_phithihanhan 
									,String psBienlai_tamthu_phithihanhan ,String pdNgay_tamthu_phithihanhan
									,String psBienlai_hoan_phithihanhan ,String psHosoID

				 ) {

   String s=    "begin ? := "+func_schema+"XULYNO.capnhat_ttToaAn("
   			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"'," 

		+"'"+pdNgayxetxu+"',"
		+"'"+psBanan+"',"
		+"'"+pdNgaybanan+"',"
		+psSotien_tamung_anphi+","
		+"'"+psBienlai_tamung_anphi+"',"
		+"'"+pdNgaytamunganphi+"',"
		+"'"+psBienlai_hoan_anphi+"',"
		+psSotien_tamthu_phithihanhan+","
		+"'"+psBienlai_tamthu_phithihanhan+"',"
    	+"'"+pdNgay_tamthu_phithihanhan+"',"
		+"'"+psBienlai_hoan_phithihanhan+"',"
    	+psHosoID
        +");"
        +"end;"
        ;
    return s;

  }

   	public String Xoacongvan(String func_schema
									  ,String data_schema
									  ,String agent_schema
									  ,String user_id
									,String psGiayto_id
				 ) {

   String s=    "begin ? := "+func_schema+"XULYNO.Xoacongvan("
   			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"',"

		+"'"+psGiayto_id+"'"
        +");"
        +"end;"
        ;
    return s;

  }

 	public String Xoaghichu(String func_schema
									  ,String data_schema
									  ,String agent_schema
									  ,String user_id
									,String pdNgay
									,String psGhiChu ,String psGiaiquyet_id
				 ) {

   String s=    "begin ? := "+func_schema+"XULYNO.Xoaghichu("
   			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"',"

		+"'"+pdNgay+"',"
		+"'"+psGhiChu+"',"
		+psGiaiquyet_id
        +");"
        +"end;"
        ;
    return s;

  }

	public String lay_so_hs_no(String func_schema
									  ,String data_schema
									  ,String agent_schema
									  ,String user_id
										, String ma_tb)
	{
		String s = "begin ? := "+func_schema+"XULYNO.SoHoSoNo("		

   			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"',"

		+ "'" + ma_tb + "'"
		+ ",0"
		+ ",0"
		+ ",0"
		+ ",0"
		+ ",0"
		+ ",null"
		+ ",null"
		+ ",''"
		+ ",''"
		+ ",null"
		+ ",0"
		+ ",0"
		+ ",''"
		+ ",0"
		+");"
		+ " end;";
		return s;
	}
	public String laytt_IdHoso(String func_schema
									  ,String data_schema
									  ,String agent_schema
									  ,String user_id, String ma_tb)
	{
		String s = "begin ? := "+func_schema+"XULYNO.HoSoNo_ID("	
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"',"

		+ "'" + ma_tb + "'"
		+ ",0"
		+ ",0"
		+ ",0"
		+ ",0"
		+ ",0"
		+ ",null"
		+ ",null"
		+ ",''"
		+ ",''"
		+ ",null"
		+ ",0"
		+ ",0"
		+ ",''"
		+ ",0"
		+");"
		+ " end;";
		return s;
	}
	public String laytt_quatrinh_xuly(String schema, String id)
	{
		return "/main?"+CesarCode.encode("configFile")+"=xln/xln_xemhoso/ds_no_ajax"
				+"&"+CesarCode.encode("id")+"="+id; 	
	}
	public String laytt_gt(String schema, String id)
	{
		return "/main?"+CesarCode.encode("configFile")+"=xln/xln_xemhoso/ds_gt_ajax"
				+"&"+CesarCode.encode("id")+"="+id; 	
	}
	public String laytt_hentra(String schema, String id)
	{
		return "/main?"+CesarCode.encode("configFile")+"=xln/ds_hentra_ajax"
				+"&"+CesarCode.encode("id")+"="+id; 	
	}
	public String laytt_hoso(String func_schema
									  ,String data_schema
									  ,String agent_schema
									  ,String user_id, String id)
	{
		String s = "begin ? := "+func_schema+"XULYNO.getHosoXLN_hs("	
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"',"
		+ "'" + id + "'"
		+");"
		+ " end;";
		return s;
	}
	public String laytt_toaan(String func_schema
									  ,String data_schema
									  ,String agent_schema
									  ,String user_id, String id)
	{
		String s = "begin ? := "+func_schema+"XULYNO.getTT_ToaAn("		
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"',"
		+ "'" + id + "'"
		+");"
		+ " end;";
		return s;
	}
//Thue bao no
	public String getRecKhachHang(String func_schema
									  ,String data_schema
									  ,String agent_schema
									  ,String user_id
									  ,String makh
		)			
	{
		return "begin ?:= "+func_schema+"xulyno.fgetkhachhang("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"','"
		+makh+"'); end; ";
	}
	public String getRecTBDungTen(
		String makh
		)
	{
		return "/main?"+CesarCode.encode("configFile")+"=xln/ajax_thuebaono_khachhang"
        +"&"+CesarCode.encode("makh")+"="+makh;
	}

	public String getRecThueBao(String func_schema
									  ,String data_schema
									  ,String agent_schema
									  ,String user_id
										,String matb
		)			
	{
		return "begin ?:= "+func_schema+"xulyno.ftimKhachHangcungMaTB("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"','"		
		+matb+"'); end; ";
	}
	public String getRecThongTinThueBao_common(String func_schema
									  ,String data_schema
									  ,String agent_schema
									  ,String user_id
										,String makh
										,String matb
		)			
	{
		return "begin ?:= "+func_schema+"xulyno.fgetThuebao_common("
			+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"','"	
			+makh+"','"+matb+"'); end; ";
	}
		public String getDocTB_ThoiGianCatMoTT(
		String makh,
		String matb,		
		String schema
		)			
	{
		return "/main?"+CesarCode.encode("configFile")+"=xln/ajax_thuebao_thoigiancatmott"
        +"&"+CesarCode.encode("makh")+"="+makh
        +"&"+CesarCode.encode("matb")+"="+matb;
	}
	public String getDocTB_TinhHinhNoCuoc(
		String makh,
		String matb,		
		String schema
		)			
	{
		return "/main?"+CesarCode.encode("configFile")+"=xln/ajax_thuebao_tinhhinhnocuoc"
        +"&"+CesarCode.encode("makh")+"="+makh
        +"&"+CesarCode.encode("matb")+"="+matb;
	}
	public String getDocTB_TinhHinhThanhToan(
		String makh,
		String matb,		
		String schema
		)			
	{
		return "/main?"+CesarCode.encode("configFile")+"=xln/ajax_thuebao_tinhhinhthantoan"
        +"&"+CesarCode.encode("makh")+"="+makh
        +"&"+CesarCode.encode("matb")+"="+matb;
	}
	public String getDocDiaChiDCCT(
		String makh
		)
	{
		return "/main?"+CesarCode.encode("configFile")+"=xln/ajax_lichsu_dc_ct"
        +"&"+CesarCode.encode("makh")+"="+makh;
	}
//Xu ly ho so
	public String  getURL_cboGiayTo(String value) 
  	{
    	return "/main?"+CesarCode.encode("configFile")+"=xln/ajax_xulyhoso_cbogiayto"
        +"&"+CesarCode.encode("cboGiaiQuyet")+"="+value;
  	} 
  	public String load_ThongTinGiayTo(String value) 
  	{
    	return "/main?"+CesarCode.encode("configFile")+"=xln/ajax_xulyhoso_thongtingiayto"
        +"&"+CesarCode.encode("loaigiayto_id")+"="+value;
  	}
  	public String load_NhanVien() 
  	{
    	return "/main?"+CesarCode.encode("configFile")+"=xln/ajax_xulyhoso_cbonhanvien";
  	}
	public String timXuLyHoSo(
  		String ps_matb, 
  		String ps_quanid, 
  		String ps_phuongid,
  		String ps_quanid_hokhau,
  		String ps_loaigiaiquyetid,
  		String ps_toxuly,
  		String ps_ngay_hentra, 
  		String ps_denthang,
  		String ps_tinh_tvt,
  		String ps_ghichu,
  		String ps_thangnodau,  		
  		String ps_namno,
  		String ps_loaikhid,
  		String ps_mabill
		//String ps_loaikt3
  		
  		)
	{
		return "/main?" + CesarCode.encode("configFile") + "=xln/ajax_xulyhoso_ds_hoso"
			+ "&" + CesarCode.encode("matb") + "=" + ps_matb
			+ "&" + CesarCode.encode("quanid") + "=" + ps_quanid
			+ "&" + CesarCode.encode("phuongid") + "=" + ps_phuongid
			+ "&" + CesarCode.encode("quanid_hokhau") + "=" + ps_quanid_hokhau
			+ "&" + CesarCode.encode("loaigiaiquyetid") + "=" + ps_loaigiaiquyetid
			+ "&" + CesarCode.encode("toxuly") + "=" + ps_toxuly
			+ "&" + CesarCode.encode("ngay_hentra") + "=" + ps_ngay_hentra
			+ "&" + CesarCode.encode("denthang") + "=" + ps_denthang			
			+ "&" + CesarCode.encode("tinh_tvt") + "=" + ps_tinh_tvt
			+ "&" + CesarCode.encode("ghichu") + "=" + ps_ghichu
			+ "&" + CesarCode.encode("thangnodau") + "=" + ps_thangnodau			
			+ "&" + CesarCode.encode("namno") + "=" + ps_namno
			+ "&" + CesarCode.encode("loaikhid") + "=" + ps_loaikhid
			+ "&" + CesarCode.encode("mabill") + "=" + ps_mabill
			//+ "&" + CesarCode.encode("loaikt3") + "=" + ps_loaikt3
			;		
	}	
	public String sql_langiaiquyet_id_seq(String data_schema)
	{
		return "SELECT (max(id)+1) id FROM  "+data_schema+"xln_danhsach_xuly";
	}
	public String sql_upd_danhsachxulys(String func_schema
									  ,String data_schema
									  ,String agent_schema
									  ,String user_id
									,String statement)
	{				
		return "begin ? :8/27/2007= "+func_schema+"xulyno.UDP_DANHSACHXULYS('"+statement+"');"+
			"end;";
	}	
	public String update_giaiquyethosono(
		String func_schema
									  ,String data_schema
									  ,String agent_schema
									  ,String user_id,
		String in_lan_id,
		String in_loaigiaiquyet_id,
		String in_ghichu,
		String in_ngay,
		String in_nguoith
	)
	{
		return "begin ?:= "+func_schema+"xulyno.fgiaiquyethosono("
					+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"','"	

		+in_lan_id+"',"+in_loaigiaiquyet_id+",'"+in_ghichu+"','"+in_ngay+
			"','"+in_nguoith+"');"+
			"end; ";										
	}
	public String update_taogiayto(		String func_schema
									  ,String data_schema
									  ,String agent_schema
									  ,String user_id,
		String in_xlid,
		String in_ngaygq,
		String in_thongtin,
		String in_loaigiayto,
		String schema
		)
	{		
		return "begin ?:=  "+func_schema+"xulyno.fTaoGiayTo("
							+"'"+data_schema+"'" 
			+",'"+agent_schema+"'" 
			+",'"+user_id+"',"	
		
		+in_xlid+",'"+in_ngaygq+"','"+in_thongtin+"',"+in_loaigiayto+");"+
			"end; ";
	}

	public String getValue_ToID(	String func_schema
									  ,String data_schema
									  ,String agent_schema
									  ,String user_id)
	{
		return "declare out_value number(3); "+
		"begin begin select toxln into out_value from "+data_schema+"xln_nhanvien where username='"+user_id+"'; "+
		"exception "+
		"when others then "+
    	"out_value:=0; end; ?:=out_value; end;";
	}
	public String getDoc_ThemHoSoNo(
		String makh,
		String matb,
		String toxln
		)
	{
			return "/main?"+CesarCode.encode("configFile")+"=xln/ajax_xulyhoso_ds_hoso_themhs"
        	+"&"+CesarCode.encode("matb")+"="+matb
        	+"&"+CesarCode.encode("makh")+"="+makh
        	+"&"+CesarCode.encode("toxln")+"="+toxln;
	}
	public String getDocThueBaoCungDC(
		String matb
		)
	{
		return "/main?"+CesarCode.encode("configFile")+"=xln/ajax_thuebaocung_dc"
        +"&"+CesarCode.encode("matb")+"="+matb;
	}
	public String yeucau
	( String func_schema
									  ,String data_schema
									  ,String agent_schema
									  ,String user_id
    ,String psma_tb
    ,String psma_kh
	,String psten_kh
	,String pstinhtrang
	,String psuser_id
    ,String yeucau
    ,String ngay
    ,String pslydo
	                       
                                
	) 
 {    String s=  "begin ?:= "+func_schema+"XULYNO.them_yeucau("
						+"'"+data_schema+"'" 
			
			+",'"+user_id+"',"	

        +"'"+psma_tb+"'"
		+",'"+psma_kh+"'"
		+",'"+pstinhtrang+"'"
	    +",'"+psten_kh+"'"
		
        +",'"+yeucau+"'"
        
        +",'"+pslydo+"'"
        +",'"+ngay+"'"
        +");"
        +"end;"
        ;
    return s;

 }
// Tim ho so
   	public String getURL_cboPhuongXa(String value) 
  	{
    	return "/main?"+CesarCode.encode("configFile")+"=xln/ajax_timhoso_cbophuongxa"
        +"&"+CesarCode.encode("cboQuanHuyen")+"="+value;
  	} 
  	public String getURL_cboQuanHuyen(String value) 
  	{
    	return "/main?"+CesarCode.encode("configFile")+"=xln/ajax_timhoso_cboquanhuyen"
        +"&"+CesarCode.encode("cboTinh")+"="+value;
  	} 
public String Themmoighichu(
String func_schema,
String schema,
String userid,
String ngay,
String ghichu,
String giaiquyet_id
)

{
return "begin ?:= "+func_schema+"xulyno.TMoiGhichu('"+schema+"','"+userid+"','"+ngay+"','"+ghichu+"','"+giaiquyet_id+"'); end;";		

}	
public String Suaghichu(
	String func_schema,
	String schema,
	String userid,
	String ngay_cu,
	String ghichu_cu,
	String giaiquyet_id, 
	String ngay, 
	String ghichu
)

{
return "begin ?:= "+func_schema+"xulyno.TMoiGhichu('"+schema+"','"+userid+"','"+ngay+"','"+ghichu+"','"+giaiquyet_id+"'); end;";		

}	
	
public String timHoSoNo(
		String sSchema,
		String sMaTinhNgDung,
		String sUserId,
  		String ps_matb, 
  		String ps_quanid, 
  		String ps_phuongid,
  		String ps_quanid_hokhau,
  		String ps_loaigiaiquyetid,
  		String ps_toxuly,
  		String ps_ngay_hentra, 
  		String ps_denthang,
  		String ps_tinh_tvt,
  		String ps_ghichu,
  		String ps_thangnodau,  		
  		String ps_namno,
  		String ps_loaikhid,
  		String ps_mabill
  		
  		)
	{
		return "/main?" + CesarCode.encode("configFile") + "=xln/ajax_timhoso_ds_hoso"
			+ "&" + CesarCode.encode("matb") + "=" + ps_matb
			+ "&" + CesarCode.encode("quanid") + "=" + ps_quanid
			+ "&" + CesarCode.encode("phuongid") + "=" + ps_phuongid
			+ "&" + CesarCode.encode("quanid_hokhau") + "=" + ps_quanid_hokhau
			+ "&" + CesarCode.encode("loaigiaiquyetid") + "=" + ps_loaigiaiquyetid
			+ "&" + CesarCode.encode("toxuly") + "=" + ps_toxuly
			+ "&" + CesarCode.encode("ngay_hentra") + "=" + ps_ngay_hentra
			+ "&" + CesarCode.encode("denthang") + "=" + ps_denthang			
			+ "&" + CesarCode.encode("tinh_tvt") + "=" + ps_tinh_tvt
			+ "&" + CesarCode.encode("ghichu") + "=" + ps_ghichu
			+ "&" + CesarCode.encode("thangnodau") + "=" + ps_thangnodau			
			+ "&" + CesarCode.encode("namno") + "=" + ps_namno
			+ "&" + CesarCode.encode("loaikhid") + "=" + ps_loaikhid
			+ "&" + CesarCode.encode("mabill") + "=" + ps_mabill
			;		
	}	
	public String value_capnhat_qtxl
	(		
		String giaiquyet_id, 
		String ngay, 
		String ghichu
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"xulyno.capnhat_qtxl('"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"','"+ngay+"','"+ghichu+"','"+giaiquyet_id+"'); end;";		
	}	
 }