package neo.xln;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;
public class xln_xemhoso
extends NEOProcessEx
{

	public String lay_tenreport(String dataschema, String id)
	{
		return  "begin ?:="+getSysConst("FuncSchema")+"xulyno.lay_report('"+dataschema+"','"+id+"'); end;";
	}
	public String update_quan(	
		String schema
        ,String userid
		,String toid
		, String dsquan
	)
    {
	   return  "begin ?:="+getSysConst("FuncSchema")+"xulyno.update_quan_tt('"+schema+"','"+userid+"','"+toid+"','"+dsquan+"'); end;";
	}
	public String laytt_to(String schema)
	{
		return "/main?"+CesarCode.encode("configFile")+"=xln/DSQuanTheoTo/ds_to_data";
	}
	
	public String themmoi_to(String tento
                        , String psschema
						, String psUserId
	)
    {
	   return  "begin ?:="+getSysConst("FuncSchema")+"xulyno.themmoito('"+tento+"','"+psschema+"','"+psUserId+"'); end;";
	}

	public String capnhat_to(String id
	                        , String tento
							, String schema
							, String userid )
	{
	 return  "begin ?:="+getSysConst("FuncSchema")+"xulyno.capnhat_to('"+id+"','"+tento+"','"+schema+"','"+userid+"'); end;";
	}
	//dsquan theo to
	public String layds_quan() {
    return "/main?"+CesarCode.encode("configFile")+"=xln/DSQuanTheoTo/DSQuanTheoTo_ajax_quan"
        ;
  }
	
	public String danhsach_no()
	{
		return "/main?"+CesarCode.encode("configFile")+"=xln/DSQuanTheoTo/danhsach_no_ajax";
	}
	public String layds_quan_tt(String psto_id) {
    return "/main?"+CesarCode.encode("configFile")+"=xln/DSQuanTheoTo/DSQuanTheoTo_ajax_quan_tt"
		+"&"+CesarCode.encode("to_id") + "=" + psto_id
        ;
	}
 
	public String laytt_loaigiayto()
	{
		return "/main?" + CesarCode.encode("configFile") + "=xln/ajax_danhsachgiayto";
					
	}   
	public String upd_dsxuly(
				String dataschema, 
				String agent, 
				String userid, 
				String matb,
				String makh,
				String id, 
				String loaigiaiquyet,
			    String ghichu,
				String ngay,
				String nguoinhap,
				String ngayxuly, 
				String thongtin,
				String giayto
	)
	{						
		String a=  "begin ?:="+getSysConst("FuncSchema")+"xulyno.UDP_DANHSACHXULY('"+dataschema+"','"+agent+"','"+userid+"','"+matb+"','"+makh+"','"+id+"','"+ loaigiaiquyet+"','"+ghichu+"','"+ngay+"','"+nguoinhap+"','"+ngayxuly+"','"+thongtin+"','"+giayto+"'); end;";		
		return a;
	}
	public String sql_upd_dsxulys(String func_schema, String statement)
	{						
		return "begin "+statement+" ?:=1; end;";		
	}
	public String sql_langiaiquyet_id_seq(String data_schema)
	{
		return "SELECT (max(id)+1) id FROM  "+data_schema+"xln_danhsach_xuly";
	}	
	public String lay_to(String schema, String userid, String quanid)
	{
		return  "begin ?:="+getSysConst("FuncSchema")+"xulyno.lay_to('"+schema+"','"+userid+"','"+quanid+"'); end;";
	}
	//DucTT bo xung
	public String rec_hoso_xln(String hoso_id)
	{
		return "begin ?:="+getSysConst("FuncSchema")+"xulyno.laytt_hoso_xln("
			+"'"+hoso_id+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
	}
}