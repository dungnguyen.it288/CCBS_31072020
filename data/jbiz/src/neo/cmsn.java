package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import com.svcon.jdbf.*;
import javax.sql.RowSet;
import java.util.zip.*;
import java.io.*;
import java.net.*;
import org.apache.commons.net.*;
import neo.qltn.ftp;
import neo.qltn.FTPupload;
import sun.misc.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.*;
import java.text.*;
import java.nio.charset.StandardCharsets;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;

public class cmsn extends NEOProcessEx{	
	//Xuat ra excel
    public String export_csv(String groupid, String agent,String area, String loaikh){
        String fileName = new String("");
		String file = "";
		try{	
			int rowIndex;
			String sql = "";
			String fileUploadDir = getSysConst("FileUploadDir");
			//Lay ten file
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//ddMMyyyyHH
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//ddMMyyyyHH
			
			java.util.Date now = cal.getTime();
			String time = dateFormat.format(now).toString().replace("/","");
			fileName = "bcm2_"+ time+ ".csv";			
			// creat folder to save excel file
			String folderPath = fileUploadDir +"report_cmsn/";
			File f = new File(folderPath);
			if(!f.exists())
				f.mkdir();				
			file = fileUploadDir +"report_cmsn/" +fileName;				
			File file1 = new File(file);
			if (file1.exists()) {
				file1.delete();
			}
			sql = "begin ? := admin_v2.PKG_BIRTHDAY.get_data_careplus_cmsn("
					+ "'" + area + "'" 
					+ ",'" + agent + "'"			
					+ ",'" + groupid + "'"			
					+ ",''"			
					+ ",'" + loaikh + "'"			
					+ ");" 
					+ "end;";	
			System.out.println(sql);
			System.out.println(sql);
			System.out.println(sql);
			RowSet rs = this.reqRSet(sql);
			int line = 4;   //Luu so dong
            int i = 1;
			FileWriter fileWriter = new FileWriter(file);
			while(rs.next()){
				if (i==1){
					fileWriter.append("\"STT\"");
					fileWriter.append(",\"STB\"");
					fileWriter.append(",\"Ma tinh\"");
					fileWriter.append(",\"Loai nhom\"");
					fileWriter.append(",\"Send SMS/OB\"");
					fileWriter.append(",\"Cuoc BQ\"");
					fileWriter.append(",\"Thang\"");
					fileWriter.append(",\"Ngay sinh\"");
					fileWriter.append(",\"Loai DL\"");
					fileWriter.append("\n");
					
					
				}
				fileWriter.append("\""+i+"\"");
				fileWriter.append(",\""+String.valueOf(rs.getString("MSISDN"))+"\"");
				fileWriter.append(",\""+String.valueOf(rs.getString("AGENT"))+"\"");
				fileWriter.append(",\""+String.valueOf(rs.getString("GROUP_ID"))+"\"");
				fileWriter.append(",\""+String.valueOf(rs.getString("SEND_SMS_OB"))+"\"");
				fileWriter.append(",\""+String.valueOf(rs.getString("CUOC_BQ"))+"\"");
				fileWriter.append(",\""+String.valueOf(rs.getString("MONTH_ACTIVE"))+"\"");
				fileWriter.append(",\""+String.valueOf(rs.getString("BIRTHDAY_DATE"))+"\"");
				fileWriter.append(",\""+String.valueOf(rs.getString("DATA_TYPE"))+"\"");
				fileWriter.append("\n");
				i++;
			}
			//Ghi xuong file
			fileWriter.flush();
			fileWriter.close();
			return file;
		}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}
    }
	
	public String exportExcel_map(String ma_tinh, String date_all_start, String date_all_end){
        String fileName = new String("");
		String file = "";
		try{	
			int rowIndex;
			String sql = "";
			String fileUploadDir = getSysConst("FileUploadDir");
			//Lay ten file
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//ddMMyyyyHH
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//ddMMyyyyHH
			
			java.util.Date now = cal.getTime();
			String time = dateFormat.format(now).toString().replace("/","");
			fileName = "Export_KHDN_MAP_"+ time+ ".xls";			
			// creat folder to save excel file
			String folderPath = fileUploadDir +"report/";
			File f = new File(folderPath);
			if(!f.exists())
				f.mkdir();				
			file = fileUploadDir +"report/" +fileName;				
			File file1 = new File(file);
			if (file1.exists()) {
				file1.delete();
			}
			//Duong dan file template
			String temp = fileUploadDir+"template/Export_KHDN_MAP.xls";
			temp=temp.replace("\\","/");
			//TODO
			FileInputStream fs =new FileInputStream(temp);
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			HSSFSheet sheet = wb.getSheetAt(0);			
            //Style
            HSSFFont font = wb.createFont();
            font.setFontHeight((short)240);
            font.setFontName("Times New Roman");
            HSSFCellStyle style = wb.createCellStyle();
            style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setFont(font);
			
			HSSFCellStyle style1 = wb.createCellStyle();
            style1.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style1.setFont(font);
            
            HSSFCellStyle style2 = wb.createCellStyle();
            style2.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            style2.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style2.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style2.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style2.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style2.setFont(font);
			
			HSSFCellStyle style3 = wb.createCellStyle();
            style3.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            style3.setFont(font);
            //Lay du lieu tu database
			sql = "begin ? := admin_v2.pkg_export_khdn.khdn_map("
					+ "'" + date_all_start + "'" 
					+ ",'" + date_all_end + "'"			
					+ ",'"+ma_tinh+"'"		
					+ ");" 
					+ "end;";	
			System.out.println(sql);				
			RowSet rs = this.reqRSet(sql);
			int line = 4;   //Luu so dong
            int i = 1;
			
			// Tu ngay
			sheet.createRow((short)1);
			sheet.getRow(1).createCell(3).setCellValue(ConvertFont.decodeW1252("T&#7915; ng&#224;y"));	
            sheet.getRow(1).getCell(3).setCellStyle(style3);
			
			sheet.getRow(1).createCell(4).setCellValue(date_all_start);	
            sheet.getRow(1).getCell(4).setCellStyle(style3);
			// Den ngay
			sheet.getRow(1).createCell(5).setCellValue(ConvertFont.decodeW1252("&#272;&#7871;n ng&#224;y"));	
            sheet.getRow(1).getCell(5).setCellStyle(style3);
			sheet.getRow(1).createCell(6).setCellValue(date_all_end);	
            sheet.getRow(1).getCell(6).setCellStyle(style3);
			
            //Ghi du lieu vao file excel
			while(rs.next()){
				sheet.createRow((short)line);
                sheet.getRow(line).createCell(0).setCellValue(i);		
                sheet.getRow(line).getCell(0).setCellStyle(style1);
				sheet.getRow(line).createCell(1).setCellValue(rs.getString("MA_DN"));	
                sheet.getRow(line).getCell(1).setCellStyle(style1);                
                sheet.getRow(line).createCell(2).setCellValue(rs.getString("NAME"));		
                sheet.getRow(line).getCell(2).setCellStyle(style1);				
				sheet.getRow(line).createCell(3).setCellValue(rs.getString("MA_TINH"));		
                sheet.getRow(line).getCell(3).setCellStyle(style1);
				sheet.getRow(line).createCell(4).setCellValue(rs.getString("MST"));		
                sheet.getRow(line).getCell(4).setCellStyle(style1);
				sheet.getRow(line).createCell(5).setCellValue(rs.getString("ADDR"));		
                sheet.getRow(line).getCell(5).setCellStyle(style1);
				sheet.getRow(line).createCell(6).setCellValue(rs.getString("ADDR_DKKD"));	
                sheet.getRow(line).getCell(6).setCellStyle(style1);                
                sheet.getRow(line).createCell(7).setCellValue(rs.getString("NGAY_THANH_LAP"));		
                sheet.getRow(line).getCell(7).setCellStyle(style1);				
				sheet.getRow(line).createCell(8).setCellValue(rs.getString("SO_TAI_KHOAN"));		
                sheet.getRow(line).getCell(8).setCellStyle(style1);
				sheet.getRow(line).createCell(9).setCellValue(rs.getString("NGAN_HANG"));		
                sheet.getRow(line).getCell(9).setCellStyle(style1);
				sheet.getRow(line).createCell(10).setCellValue(rs.getString("DD_HO_TEN"));		
                sheet.getRow(line).getCell(10).setCellStyle(style1);				
				sheet.getRow(line).createCell(11).setCellValue(rs.getString("DD_NGAY_SINH"));	
                sheet.getRow(line).getCell(11).setCellStyle(style1);  
				sheet.getRow(line).createCell(12).setCellValue(rs.getString("DD_CMT"));	
                sheet.getRow(line).getCell(12).setCellStyle(style1);                
                sheet.getRow(line).createCell(13).setCellValue(rs.getString("DD_NGAYCAP_CMT"));		
                sheet.getRow(line).getCell(13).setCellStyle(style1);				
				sheet.getRow(line).createCell(14).setCellValue(rs.getString("DD_MOBILE"));		
                sheet.getRow(line).getCell(14).setCellStyle(style1);
				sheet.getRow(line).createCell(15).setCellValue(rs.getString("NGANH_NGHE"));		
                sheet.getRow(line).getCell(15).setCellStyle(style1);
				sheet.getRow(line).createCell(16).setCellValue(rs.getString("PHAN_LOAI"));		
                sheet.getRow(line).getCell(16).setCellStyle(style1);				
				sheet.getRow(line).createCell(17).setCellValue(rs.getString("LOAI_KH"));	
                sheet.getRow(line).getCell(17).setCellStyle(style1);                
                sheet.getRow(line).createCell(18).setCellValue(rs.getString("HANG_KH"));		
                sheet.getRow(line).getCell(18).setCellStyle(style1);
				sheet.getRow(line).createCell(19).setCellValue(rs.getString("CREATE_DATE"));	
                sheet.getRow(line).getCell(19).setCellStyle(style1);                
                sheet.getRow(line).createCell(20).setCellValue(rs.getString("CREATE_USER"));		
                sheet.getRow(line).getCell(20).setCellStyle(style1);				
				
                line++;i++;
			}
			// Ngay xuat bao cao
			sheet.createRow((short)line+1);
			sheet.getRow(line+1).createCell(3).setCellValue(ConvertFont.decodeW1252("Ng&#224;y b&#225;o c&#225;o: ")+"  "+dateFormat1.format(now).toString());	
            sheet.getRow(line+1).getCell(3).setCellStyle(style3);
			// Nguoi xuat bao cao
			sheet.createRow((short)line+2);
			sheet.getRow(line+2).createCell(3).setCellValue(ConvertFont.decodeW1252("Ng&#432;&#7901;i b&#225;o c&#225;o: ")+"  "+getUserVar("userID"));	
            sheet.getRow(line+2).getCell(3).setCellStyle(style3);
			//Ghi xuong file
			FileOutputStream fos = new FileOutputStream(file, true);
			wb.write(fos);
			fos.flush();
			fos.close();
			return file;
		}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}
    }
	public String exportCSV_not_map(String ma_tinh){
		String fileName = new String("");
		String file = "";
		try{
			String sql = "";
			String fileUploadDir = getSysConst("FileUploadDir");
			//Lay ten file
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//ddMMyyyyHH
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//ddMMyyyyHH
				
			java.util.Date now = cal.getTime();
			String time = dateFormat.format(now).toString().replace("/","");
			fileName = "Export_KHDN_NOT_MAP_"+ time+ ".csv";			
			// creat folder to save csv file
			String folderPath = fileUploadDir +"report/";
			File f = new File(folderPath);
			if(!f.exists())
				f.mkdir();				
			file = fileUploadDir +"report/" +fileName;				
			File file1 = new File(file);
			if (file1.exists()) {
				file1.delete();
			}

		//FileWriter writer = new FileWriter(file1);
		
		//Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file1), StandardCharsets.UTF_8));
		OutputStream os = new FileOutputStream(file1);
		os.write(239);
		os.write(187);
		os.write(191);
		PrintWriter out = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
		try{
			int counter = 0;
			out.print(ConvertFont.decodeW1252("STT,M&#227; t&#7881;nh,M&#227 KH,S&#7889; thu&#234; bao,T&#234;n DN,MST,Ph&#226;n lo&#7841;i KH,Ph&#226;n h&#7841;ng,Ng&#224;nh ngh&#7873;,Lo&#7841;i KH,Ng&#224;y s&#7917;a &#273;&#7893;i,User,S&#7889; H&#272;,Ng&#224;y k&#253; H&#272;,Ng&#224;y h&#7871;t h&#7841;n H&#272;,M&#227; DN c&#361;,M&#227; DN m&#7899;i,&#272;&#7889;i t&#432;&#7907;ng DN,Kh&#7889;i DN,&#272;&#7883;a ch&#7881; giao d&#7883;ch,&#272;&#7883;a ch&#7881; tr&#234;n GPKD,Ng&#224;y th&#224;nh l&#7853;p,S&#7889; T&#224;i kho&#7843;n,T&#7841;i Ng&#226;n h&#224;ng,NDD.H&#7885; t&#234;n,NDD.Ng&#224;y sinh,NDD.S&#7889; CMND,NDD.Ng&#224;y c&#7845;p,L&#297;nh v&#7921;c KD\n"));
			sql = "begin ? := admin_v2.pkg_export_khdn.khdn_not_map("
					+ "'"+ma_tinh+"'"	
					+ ");" 
					+ "end;";	
			System.out.println(sql);				
			RowSet rs = this.reqRSet(sql);	
			while (rs.next()) {				
				counter++;
				out.print(String.valueOf(counter)+"," 
						+ rs.getString("MA_TINH") + ","
						+ rs.getString("MA_KH") + ","
						+ rs.getString("SOMAY") + ","
						+ rs.getString("TEN_DN") + ","
						+ rs.getString("MST") + ","
						+ rs.getString("PHAN_LOAI") + ","
						+ rs.getString("HANG_KH") + ","
						+ rs.getString("NGANH_NGHE") + ","
						+ rs.getString("LOAI_KH") + ","
						+ rs.getString("NGAY_SUA_DOI") + ","
						+ rs.getString("LOG_USER") + ","
						+ rs.getString("MA_HD") + ","
						+ rs.getString("NGAY_LAPHD") + ","
						+ rs.getString("NGAY_HET_HAN") + ","
						+ rs.getString("MA_DN_CU") + ","
						+ rs.getString("MA_DN") + ","
						+ rs.getString("DOITUONG_DN") + ","
						+ rs.getString("KHOI_DN") + ","
						+ rs.getString("ADDR") + ","
						+ rs.getString("ADDR_DKKD") + ","
						+ rs.getString("NGAY_THANH_LAP") + ","
						+ rs.getString("SO_TAI_KHOAN") + ","
						+ rs.getString("NGAN_HANG") + ","
						+ rs.getString("DD_HO_TEN") + ","
						+ rs.getString("DD_NGAY_SINH") + ","
						+ rs.getString("DD_CMT") + ","
						+ rs.getString("DD_NGAYCAP_CMT") + ","
						+ rs.getString("LINH_VUC") + "\n"
				);
			}			
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}finally {
			out.flush();
			out.close();
		}
		return file;
	}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}
	}
	
	public String exportCSV_map(String ma_tinh){
		String fileName = new String("");
		String file = "";
		try{
			String sql = "";
			String fileUploadDir = getSysConst("FileUploadDir");
			//Lay ten file
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//ddMMyyyyHH
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//ddMMyyyyHH
				
			java.util.Date now = cal.getTime();
			String time = dateFormat.format(now).toString().replace("/","");
			fileName = "Export_KHDN_MAP_"+ time+ ".csv";			
			// creat folder to save csv file
			String folderPath = fileUploadDir +"report/";
			File f = new File(folderPath);
			if(!f.exists())
				f.mkdir();				
			file = fileUploadDir +"report/" +fileName;				
			File file1 = new File(file);
			if (file1.exists()) {
				file1.delete();
			}
			
		//FileWriter writer = new FileWriter(file1);
		
		OutputStream os = new FileOutputStream(file1);
		os.write(239);
		os.write(187);
		os.write(191);
		PrintWriter out = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
		
		try{
			int counter = 0;
			out.print(ConvertFont.decodeW1252("STT,M&#227; t&#7881;nh,S&#7889; thu&#234; bao,T&#234;n DN,MST,Ph&#226;n lo&#7841;i KH,Ph&#226;n h&#7841;ng,Ng&#224;nh ngh&#7873;,Lo&#7841;i KH,Ng&#224;y s&#7917;a &#273;&#7893;i,User,S&#7889; H&#272;,Ng&#224;y k&#253; H&#272;,Ng&#224;y h&#7871;t h&#7841;n H&#272;,M&#227; DN c&#361;,M&#227; DN m&#7899;i,&#272;&#7889;i t&#432;&#7907;ng DN,Kh&#7889;i DN,&#272;&#7883;a ch&#7881; giao d&#7883;ch,&#272;&#7883;a ch&#7881; tr&#234;n GPKD,Ng&#224;y th&#224;nh l&#7853;p,S&#7889; T&#224;i kho&#7843;n,T&#7841;i Ng&#226;n h&#224;ng,NDD.H&#7885; t&#234;n,NDD.Ng&#224;y sinh,NDD.S&#7889; CMND,NDD.Ng&#224;y c&#7845;p,L&#297;nh v&#7921;c KD\n"));
			sql = "begin ? := admin_v2.pkg_export_khdn.khdn_map("
					+ "'"+ma_tinh+"'"		
					+ ");" 
					+ "end;";	
			System.out.println(sql);				
			RowSet rs = this.reqRSet(sql);	
			while (rs.next()) {				
				counter++;
				out.print(String.valueOf(counter)+"," 
						+ rs.getString("MA_TINH") + ","
						+ rs.getString("SOMAY") + ","
						+ rs.getString("TEN_DN") + ","
						+ rs.getString("MST") + ","
						+ rs.getString("PHAN_LOAI") + ","
						+ rs.getString("HANG_KH") + ","
						+ rs.getString("NGANH_NGHE") + ","
						+ rs.getString("LOAI_KH") + ","
						+ rs.getString("NGAY_SUA_DOI") + ","
						+ rs.getString("LOG_USER") + ","
						+ rs.getString("MA_HD") + ","
						+ rs.getString("NGAY_LAPHD") + ","
						+ rs.getString("NGAY_HET_HAN") + ","
						+ rs.getString("MA_DN_CU") + ","
						+ rs.getString("MA_DN") + ","
						+ rs.getString("DOITUONG_DN") + ","
						+ rs.getString("KHOI_DN") + ","
						+ rs.getString("ADDR") + ","
						+ rs.getString("ADDR_DKKD") + ","
						+ rs.getString("NGAY_THANH_LAP") + ","
						+ rs.getString("SO_TAI_KHOAN") + ","
						+ rs.getString("NGAN_HANG") + ","
						+ rs.getString("DD_HO_TEN") + ","
						+ rs.getString("DD_NGAY_SINH") + ","
						+ rs.getString("DD_CMT") + ","
						+ rs.getString("DD_NGAYCAP_CMT") + ","
						+ rs.getString("LINH_VUC") + "\n"
				);
			}			
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}finally {
			out.flush();
			out.close();
		}
		return file;
	}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}
	}
	
	
	public void doZip(String filename,String zipfilename) throws IOException {
    	
        try {
            FileInputStream fis = new FileInputStream(filename);
            File file=new File(filename);
            byte[] buf = new byte[fis.available()];
            fis.read(buf,0,buf.length);
            
            CRC32 crc = new CRC32();
            ZipOutputStream s = new ZipOutputStream(
                    (OutputStream)new FileOutputStream(zipfilename));
            
            s.setLevel(6);
            
            ZipEntry entry = new ZipEntry(file.getName());
            entry.setSize((long)buf.length);
            crc.reset();
            crc.update(buf);
            entry.setCrc( crc.getValue());
            s.putNextEntry(entry);
            s.write(buf, 0, buf.length);
            s.finish();
            s.close();
            fis.close();
            //file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	public String update_cmsn(String groupid, String agent,String area){
		String rs ="";
		try{
			int counter = 0;
			String sql = "begin ? := admin_v2.PKG_BIRTHDAY_2.update_data_careplus_cmsn("
					+ "'" + area + "'" 
					+ ",'" + agent + "'"			
					+ ",'" + groupid + "'"			
					+ ",'"+getUserVar("userID")+"'"			
					+ ");" 
					+ "end;";	
			System.out.println(sql);				
			 rs = this.reqValue(sql);
			
		} catch (Exception e) {
            e.printStackTrace();
        }
		return rs;
	}
	
}