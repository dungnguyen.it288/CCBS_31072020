package neo;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import sun.misc.BASE64Encoder;
import java.util.*;
import javax.sql.*;

import java.text.SimpleDateFormat;
import java.net.*;
import java.io.*;
import neo.qlsp.*;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import javax.xml.soap.*;
//import java.util.concurrent.TimeUnit;



public class thuphi extends NEOProcessEx 
{
	public String add_phi (String p_name,String p_mucphi,String p_ghichu, String p_ngay_apdung, String p_matinh, String p_userid)
  	{		
		String result="";
		String s="begin ?:=  admin_v2.pkg_thuphi_tainha.add_mucphi('"+p_name
							+"','"+p_mucphi
							+"','"+p_ghichu
							+"','"+p_ngay_apdung
							+"','"+p_matinh
							+"','"+p_userid
							+"');"
							+" end;"
							;       
		try
			{		    
				 result=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	}  
	
	public String edit_phi (String p_id,String p_name,String p_mucphi,String p_ghichu, String p_ngay_apdung, String p_matinh, String p_userid)
  	{		
		String result="";
		String s="begin ?:=  admin_v2.pkg_thuphi_tainha.edit_mucphi('"+p_id
							+"','"+p_name
							+"','"+p_mucphi
							+"','"+p_ghichu
							+"','"+p_ngay_apdung
							+"','"+p_matinh
							+"','"+p_userid
							+"');"
							+" end;"
							;         
		try
			{		    
				 result=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	} 


	public String del_phi (String p_id, String p_matinh, String p_userid)
  	{		
		String result="";
		String s="begin ?:=  admin_v2.pkg_thuphi_tainha.del_mucphi('"+p_id
							+"','"+p_matinh
							+"','"+p_userid
							+"');"
							+" end;"
							;         
		try
			{		    
				 result=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	} 
	
	public String rec_laytt_kiemtra(String psUploadID){
		String out_ ="begin ?:= ADMIN_V2.pkg_thuphi_tainha.laytt_kiemtra_file('"+psUploadID+"'); end;";
		return out_;
	}
	
	public String rec_layds_ctxl(String psUploadID)
	{
		return "/main?"+CesarCode.encode("configFile")+"=thuphi_tainha/thuphi_file/ajax_layds_chitiet"
		+"&"+CesarCode.encode("upload_id")+"="+psUploadID
        ;
	}
	
	public NEOCommonData mucphi_file(String psUploadID, String userid, String matinh)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := ADMIN_V2.pkg_thuphi_tainha.mucphi_file('"+psUploadID+"','"+userid+"','"+matinh+"'); ";
		xs_ = xs_ + " end; ";		
		
		String rs_ = null;
		try{
			rs_ = reqValue("", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}

}