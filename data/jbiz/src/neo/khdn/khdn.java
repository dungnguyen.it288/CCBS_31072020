package neo.khdn;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import sun.misc.BASE64Encoder;
import java.util.*;
import javax.sql.*;

import java.text.SimpleDateFormat;
import java.net.*;
import java.io.*;
import neo.qlsp.*;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import javax.xml.soap.*;
//import java.util.concurrent.TimeUnit;



public class khdn extends NEOProcessEx 
{
	
	public String reset_4g(String msisdn, String ip,String idcv)
	{
		String _userid = getUserVar("userID");
		String _userip = getUserVar("userIP");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		 String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_khdn.reset4g_func('"+ip+"','"+msisdn+"','"+idcv+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
		
	}
	public String dangky_ld(String psip,String so_tb, String cuocld, String ghichu )
	{
		String _userid = getUserVar("userID");
		String _userip = getUserVar("userIP");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		 String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_khdn.dangky_ld('"+psip+"','"+so_tb+"','"+ cuocld+"','"+_userid+"','"+_userip+"','"+matinh_ngdung_+"','"+ghichu+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	}
	public String dangky_goicuoc(String psip,String so_tb,String idcv,String pkgid, String ghichu ,String userip)
	{
		String _userid = getUserVar("userID");
		String _userip = getUserVar("userIP");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		 String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_khdn.register_goiip('"+so_tb+"','"+psip+"','"+ idcv+"','"+pkgid+"','"+matinh_ngdung_+"','"+_userid+"','"+userip+"','"+ghichu+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	}
	
	public String huy_goicuoc(String psip,String so_tb,String idcv,String pkgid, String ghichu ,String userip)
	{
		String _userid = getUserVar("userID");
		String _userip = getUserVar("userIP");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		 String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_khdn.unregister_goiip('"+so_tb+"','"+psip+"','"+ idcv+"','"+pkgid+"','"+matinh_ngdung_+"','"+_userid+"','"+userip+"','"+ghichu+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	}
	public String xuat_tttgc (String ip,String userid,String userip, String action, String id_hddt)
  	{		
		String result="";
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		String dataschema="CCS_"+matinh_ngdung_+".";
		String s="begin ?:=  admin_v2.pkg_khdn.xuat_tttgc('"
							+ dataschema		
							+"','"+matinh_ngdung_
							+"','"+userid
							+"','"+userip
							+"','"+ip
							+"','"+action
							+"','"+id_hddt
							+"');"
							+" end;"
							;       
		try
			{		    
				 result=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	}
	
	public String huy_ld(String psip,String so_tb, String cuocld, String ghichu )
	{
		String _userid = getUserVar("userID");
		String _userip = getUserVar("userIP");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		 String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_khdn.huy_ld('"+psip+"','"+so_tb+"','"+ cuocld+"','"+_userid+"','"+_userip+"','"+matinh_ngdung_+"','"+ghichu+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	}
	public String ckeck_tbdk_ld(String so_tb )
	{
		
		 String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_khdn.check_tb_dk_lds('"+so_tb+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	}
	
	public String ckeck_tbdkgoicuoc(String so_tb,String idcv )
	{
		
		 String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_khdn.check_tbdkgoicuoc('"+so_tb+"','"+idcv+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	}
	 public String check_thuchien_numstore(String msin,String so_tb,String xacthuc,String psDescription, String loai,String pkho,String pMeg)
	{		
		String _userid = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
	   String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";
		
	xs_ = xs_ + " 	?:= neo.pkg_chonso_capdoi.thuc_hien_numstore_new_qlsp('"+msin+"','"+so_tb+"','"+xacthuc+"','"+_userid+"','"+matinh_ngdung_+"','"+psDescription+"','"+loai+"','"+pkho+"','"+pMeg+"'); "; // code cho QLSP
		
		
		
		xs_ = xs_ + " end; ";	

 try
			{		    
				 result=	this.reqValue("CMDVOracleDS",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;			
	}
	public String capnhat_db_capdoi(String msin,String so_tb)
	{		
		String _userid = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
	   String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";
		
	xs_ = xs_ + " 	?:= admin_V2.capnhat_khoitao_capdoi('"+msin+"','"+so_tb+"','"+_userid+"','CCBS','"+matinh_ngdung_+"'); "; // code cho QLSP
		
		
		
		xs_ = xs_ + " end; ";	

 try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;			
	}
	
	public String capnhat_vnp_capdoi(String msin,String so_tb)
	{		
		String _userid = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
	   String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";
		
	xs_ = xs_ + " 	?:= neo.update_vnp_couple@db_2_nsa('"+msin+"','"+so_tb+"','"+_userid+"','CCBS'); "; // code cho QLSP
		
		
		
		xs_ = xs_ + " end; ";	

 try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;			
	}
	 public String get_info_sub(String so_tb)
	{		
		String Host="10.156.4.86";
		String Port="8080";
		String Uri="application/g/get_isdn_detail";
		String Request = "{\"p_isdn\": \""+so_tb+"\"}";
		//String Request = "{\"p_isdn\": \"*135\",\"p_prefix\": \"8481\",\"p_page_rec\": \"30\",\"p_page_num\": \"1\",\"p_stock_id\": \"34117\",\"p_numbertype_id\": \"\"}";
      

		String token="eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzUxMiJ9.eyJhdWQiOiJjY2JzIiwidHBzIjoxMDAsInJvbGVzIjpbImxpbWl0ZWQtdXNlciJdLCJpc3MiOiJ0dW5ncGh1b25nIiwicmVzb3VyY2VzIjpbImcvKiJdLCJjYXRlZ29yaWVzIjpbIioiXSwiZXhwIjowLCJpcHMiOlsiMTAuMTQ5LjM0LjE2MiIsIjEwLjE0OS4zNC4xNjQiLCIxMC4xNDkuMzQuMTY1IiwiMTAuMTQ5LjM0LjE2NiIsIjEwLjE0OS45MS4xNDEiLCIxOTAuMTAuMTAuODkiLCIxMC4xNDkuMzQuMTE5IiwiMTAuMTQ5LjkxLjEyMCJdLCJqdGkiOiJjY2JzIn0=.3T7Zbsa/JLg2uB+QTxNA1TP/51zzwU9VsZWdPawW9BaVL7KML9ho+PDZ3pUXz9hIkXM2MPe6DNxAeDNTZxKp9A==";
		StringBuffer buf = new StringBuffer();	    
	    try {
	    	URL url = new URL("http://"+Host+":"+Port+"/"+Uri);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(1000);
			conn.setReadTimeout(1000);
			conn.setRequestMethod("POST");	        
			conn.setRequestProperty("Authorization",token);			
	        conn.setRequestProperty("Content-Type","application/json;charset=utf-8");	        	        	   
			conn.setRequestProperty("Content-Length", String.valueOf(Request.length()));
			conn.setDoOutput(true);
	        conn.setDoInput(true);
	        OutputStreamWriter writer = new OutputStreamWriter( conn.getOutputStream() );
	        writer.write(Request);
	        writer.flush();
			writer.close();
	        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
	        String res_line;
	        while ((res_line = in.readLine()) != null)
	            buf.append(res_line);
	        in.close();
			System.out.println(buf.toString());
	        return buf.toString();
	    }catch (Exception e) {
	        e.printStackTrace();
	        return "error:"+e.getMessage();
	    }		
	}
	
public String check_thuchien_numstore_pps(String msin,String so_tb,String xacthuc,String psDescription, String loai)
	{	
		String result="";
		String _userid = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
	
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= subadmin.PKG_NUMBERS_STORE_PPS_test.thuc_hien_numstore_pps('"+msin+"','"+so_tb+"','"+xacthuc+"','"+_userid+"','"+matinh_ngdung_+"','"+psDescription+"','"+loai+"'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);
		
		try
			{		    
				 result=	this.reqValue("CMDVOracleDS",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;			
	}	
		
		
	/* ------------------------------------------------------------------------------------- */
		

	
	
	 			//lay ds loai thue bao khoi tao
	public String layds_loaitb_capdoi(String psloaikho)
	{
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumber_numstore_capdoi/ds_loaitb_sql";
		url_ = url_ + "&"+CesarCode.encode("psloaikho")+"="+psloaikho;								
		url_ = url_ +"&sid="+Math.random();
		return url_;
	} 
	
	public String timkiem_tk_numstore_next(String pPrefix, String so_tb,String xac_thuc,String loai_kho, String pageNum)
	{
		String rs_ = "";
		try {
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"timkiem_tk_numstore", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return rs_;
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage()) ;
		}
		
		
		String userid_ = getUserVar("userID");
		String url_ ="/main";
		
		//url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumber_numstore/mod_simtonumber";
		
		//if ((loai_kho =="3")||(loai_kho =="4"))
		//{
			url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumber_numstore_capdoi/mod_simtonumber_qlsp_seq";
		//}
		//else
		//{
		//	url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumber_numstore/mod_simtonumber";
		//}
		
		url_ = url_ + "&"+CesarCode.encode("so_tb_")+"="+so_tb;
		url_ = url_ + "&"+CesarCode.encode("xac_thuc_")+"="+xac_thuc;
		url_ = url_ + "&"+CesarCode.encode("userid_")+"="+userid_;
		url_ = url_ + "&"+CesarCode.encode("loai_kho")+"="+loai_kho;
		url_ = url_ + "&"+CesarCode.encode("pPrefix")+"="+pPrefix;
		url_ = url_ + "&"+CesarCode.encode("pageNum")+"="+pageNum;
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	
		//tim kiem so cap doi 
	public String searchmsisdn_api(String msisdn, String p_isdn, String prefix,String stockid, String pagenum, String pagerec, String clientip, String otp){
		String Host="10.156.4.86";
		String Port="8080";
		String Uri="application/g/search_msisdn";
		String Request = "{\"msisdn\": \""+msisdn+"\",\"p_isdn\": \""+p_isdn+"\",\"p_prefix\": \""+prefix+"\",\"p_page_rec\": \""+pagerec+"\",\"p_page_num\": \""+pagenum+"\",\"p_stock_id\": \""+stockid+"\",\"p_numbertype_id\": \"-1\",\"client_key\":\""+clientip+"\",\"p_otp\":\""+otp+"\"}";
		//String Request = "{\"p_isdn\": \"*135\",\"p_prefix\": \"8481\",\"p_page_rec\": \"30\",\"p_page_num\": \"1\",\"p_stock_id\": \"34117\",\"p_numbertype_id\": \"\"}";
      

		String token="eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzUxMiJ9.eyJhdWQiOiJjY2JzIiwidHBzIjoxMDAsInJvbGVzIjpbImxpbWl0ZWQtdXNlciJdLCJpc3MiOiJ0dW5ncGh1b25nIiwicmVzb3VyY2VzIjpbImcvKiJdLCJjYXRlZ29yaWVzIjpbIioiXSwiZXhwIjowLCJpcHMiOlsiMTAuMTQ5LjM0LjE2MiIsIjEwLjE0OS4zNC4xNjQiLCIxMC4xNDkuMzQuMTY1IiwiMTAuMTQ5LjM0LjE2NiIsIjEwLjE0OS45MS4xNDEiLCIxOTAuMTAuMTAuODkiLCIxMC4xNDkuMzQuMTE5IiwiMTAuMTQ5LjkxLjEyMCJdLCJqdGkiOiJjY2JzIn0=.3T7Zbsa/JLg2uB+QTxNA1TP/51zzwU9VsZWdPawW9BaVL7KML9ho+PDZ3pUXz9hIkXM2MPe6DNxAeDNTZxKp9A==";
		StringBuffer buf = new StringBuffer();	    
	    try {
	    	URL url = new URL("http://"+Host+":"+Port+"/"+Uri);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(1000);
			conn.setReadTimeout(1000);
			conn.setRequestMethod("POST");	        
			conn.setRequestProperty("Authorization",token);			
	        conn.setRequestProperty("Content-Type","application/json;charset=utf-8");	        	        	   
			conn.setRequestProperty("Content-Length", String.valueOf(Request.length()));
			conn.setDoOutput(true);
	        conn.setDoInput(true);
	        OutputStreamWriter writer = new OutputStreamWriter( conn.getOutputStream() );
	        writer.write(Request);
	        writer.flush();
			writer.close();
	        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
	        String res_line;
	        while ((res_line = in.readLine()) != null)
	            buf.append(res_line);
	        in.close();
			System.out.println(buf.toString());
	        return buf.toString();
	    }catch (Exception e) {
	        e.printStackTrace();
	        return "error:"+e.getMessage();
	    }
		
	  }
	  //lay danh sach cac so dang giu
	public String get_keepmsisdn_api(String msisdn){
		String Host="10.156.4.86";
		String Port="8080";
		String Uri="application/g/list_keep_msisdn";
		String Request = "{\"msisdn\": \""+msisdn+"\"}";
		//String Request = "{\"p_isdn\": \"*135\",\"p_prefix\": \"8481\",\"p_page_rec\": \"30\",\"p_page_num\": \"1\",\"p_stock_id\": \"34117\",\"p_numbertype_id\": \"\"}";
      

		String token="eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzUxMiJ9.eyJhdWQiOiJjY2JzIiwidHBzIjoxMDAsInJvbGVzIjpbImxpbWl0ZWQtdXNlciJdLCJpc3MiOiJ0dW5ncGh1b25nIiwicmVzb3VyY2VzIjpbImcvKiJdLCJjYXRlZ29yaWVzIjpbIioiXSwiZXhwIjowLCJpcHMiOlsiMTAuMTQ5LjM0LjE2MiIsIjEwLjE0OS4zNC4xNjQiLCIxMC4xNDkuMzQuMTY1IiwiMTAuMTQ5LjM0LjE2NiIsIjEwLjE0OS45MS4xNDEiLCIxOTAuMTAuMTAuODkiLCIxMC4xNDkuMzQuMTE5IiwiMTAuMTQ5LjkxLjEyMCJdLCJqdGkiOiJjY2JzIn0=.3T7Zbsa/JLg2uB+QTxNA1TP/51zzwU9VsZWdPawW9BaVL7KML9ho+PDZ3pUXz9hIkXM2MPe6DNxAeDNTZxKp9A==";
		StringBuffer buf = new StringBuffer();	    
	    try {
	    	URL url = new URL("http://"+Host+":"+Port+"/"+Uri);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(1000);
			conn.setReadTimeout(1000);
			conn.setRequestMethod("POST");	        
			conn.setRequestProperty("Authorization",token);			
	        conn.setRequestProperty("Content-Type","application/json;charset=utf-8");	        	        	   
			conn.setRequestProperty("Content-Length", String.valueOf(Request.length()));
			conn.setDoOutput(true);
	        conn.setDoInput(true);
	        OutputStreamWriter writer = new OutputStreamWriter( conn.getOutputStream() );
	        writer.write(Request);
	        writer.flush();
			writer.close();
	        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
	        String res_line;
	        while ((res_line = in.readLine()) != null)
	            buf.append(res_line);
	        in.close();
			System.out.println(buf.toString());
	        return buf.toString();
	    }catch (Exception e) {
	        e.printStackTrace();
	        return "error:"+e.getMessage();
	    }
		
	  }
	  
	  public static String regis_capdoi_spi(String msisdn, String submsisdn){
		String Host="10.156.4.125";
		String Port="8080";
		String Uri="/axis/services/spi";
		String username="neo";
		String password="neo#1";
		String Request="";
		String method="registerPairSubscribers";
		StringBuffer buf = new StringBuffer();	    
	    try {
			
	    	URL url = new URL("http://"+Host+":"+Port+"/"+Uri);
			System.out.println("URL: " + url);			
	        URLConnection urlc = url.openConnection();
	        urlc.setConnectTimeout(8000);
			BASE64Encoder enc = new sun.misc.BASE64Encoder();
			String userpassword = username + ":" + password;
			String encodedAuthorization = enc.encode(userpassword.getBytes());
			urlc.setRequestProperty("Authorization", "Basic " + encodedAuthorization);
	        urlc.setRequestProperty("Content-Type","text/xml");
	        urlc.setRequestProperty("SOAPAction", Uri +"/"+method);	        
	        urlc.setDoOutput(true);
	        urlc.setDoInput(true);
	        OutputStreamWriter writer = new OutputStreamWriter( urlc.getOutputStream() );
	        writer.write( Request );
	        writer.flush();
	        BufferedReader in = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
	        String res_line;
	        while ((res_line = in.readLine()) != null)
	            buf.append(res_line);
	        in.close();
	        return buf.toString();
	    }catch (Exception e) {
	        e.printStackTrace();
	        return e.getMessage();
	    }
	  }
	 


	
	  //http://10.156.4.100:2222/ws/in/RegService?wsdl
	 public String regis_capdoi_kmcb(String msisdn, String submsisdn){
		 String l_msisdn = msisdn;
		 String l_submsisdn = submsisdn;
		 
		 if(msisdn.length()>9 && msisdn.substring(0,2).equals("84"))
		 {
			 l_msisdn = msisdn.substring(2,msisdn.length());			 			
		 }
		 
		  if(submsisdn.length()>9 && submsisdn.substring(0,2).equals("84"))
		 {
			 l_submsisdn = submsisdn.substring(2,submsisdn.length());			 			
		 }
		 //http://10.156.4.100:3131/ws/in/RegService?wsdl
		String Host="10.156.4.100";
		String Port="3131";
		String Uri="ws/in/RegService";
		String method="addCouplePreValidate";
	String Request="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws.seconds.vnp.com/\">"+
				  " <soapenv:Header/>"+
				  " <soapenv:Body>"+
				   "   <ws:addCouplePreValidate>"+
				    "     <servicecode>CDTHOAI</servicecode>"+
				    "     <host>"+l_msisdn+"</host>"+
				     "    <member>"+l_submsisdn+"</member>"+
				     " </ws:addCouplePreValidate>"+
				 "  </soapenv:Body>"+
			"	</soapenv:Envelope>";
		
		StringBuffer buf = new StringBuffer();	    
	    try {
	    	URL url = new URL("http://10.156.4.100:3131/ws/in/RegService");
				
	        URLConnection urlc = url.openConnection();
	       // urlc.setConnectTimeout(8000);			
		   urlc.setRequestProperty("Content-Length", String.valueOf(Request.length()));
	        urlc.setRequestProperty("Content-Type","text/xml;");
	        urlc.setRequestProperty("SOAPAction", "/ws/in/RegService/addCouplePreValidate");	  
			//urlc.setRequestMethod("POST");			
	        urlc.setDoOutput(true);
	        urlc.setDoInput(true);
	        OutputStreamWriter writer = new OutputStreamWriter( urlc.getOutputStream() );
	        writer.write( Request );
	        writer.flush();
	        BufferedReader in = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
	        String res_line;
	        while ((res_line = in.readLine()) != null)
	            buf.append(res_line);
	        in.close();
	        return buf.toString();
	    }catch (Exception e) {
	        e.printStackTrace();
	        return e.getMessage();
	    }
		
	  }
	// prepaid.util.enqueue_array@db_2_nsa('vnp-1414','1414',phonenum,content)
	  
	  //send OTP
	   public String send_OTP(String msisdn, String opt){
		   String  result="";
			String s = "begin ? := " 
                + "prepaid.util.enqueue_array@db_2_nsa('vnp-1414','1414','"+msisdn+"','Ma giao dich de tim kiem so cap doi 081xx cua Quy khach la "+opt+"');"
                + "end;";
		
       try
			{		    
				 result=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	   }
	   //get otp
	  public String get_otp_api(String msisdn, String method){
		String Host="10.156.4.86";
		String Port="8080";
		String Uri="application/g/get_otp";
		String Request = "{\"msisdn\": \""+msisdn+"\",\"p_service\":\""+method+"\"}";
		
		String token="eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzUxMiJ9.eyJhdWQiOiJjY2JzIiwidHBzIjoxMDAsInJvbGVzIjpbImxpbWl0ZWQtdXNlciJdLCJpc3MiOiJ0dW5ncGh1b25nIiwicmVzb3VyY2VzIjpbImcvKiJdLCJjYXRlZ29yaWVzIjpbIioiXSwiZXhwIjowLCJpcHMiOlsiMTAuMTQ5LjM0LjE2MiIsIjEwLjE0OS4zNC4xNjQiLCIxMC4xNDkuMzQuMTY1IiwiMTAuMTQ5LjM0LjE2NiIsIjEwLjE0OS45MS4xNDEiLCIxOTAuMTAuMTAuODkiLCIxMC4xNDkuMzQuMTE5IiwiMTAuMTQ5LjkxLjEyMCJdLCJqdGkiOiJjY2JzIn0=.3T7Zbsa/JLg2uB+QTxNA1TP/51zzwU9VsZWdPawW9BaVL7KML9ho+PDZ3pUXz9hIkXM2MPe6DNxAeDNTZxKp9A==";
		StringBuffer buf = new StringBuffer();	    
	    try {
	    	URL url = new URL("http://"+Host+":"+Port+"/"+Uri);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(1000);
			conn.setReadTimeout(1000);
			conn.setRequestMethod("POST");	        
			conn.setRequestProperty("Authorization",token);			
	        conn.setRequestProperty("Content-Type","application/json;charset=utf-8");	        	        	   
			conn.setRequestProperty("Content-Length", String.valueOf(Request.length()));
			conn.setDoOutput(true);
	        conn.setDoInput(true);
	        OutputStreamWriter writer = new OutputStreamWriter( conn.getOutputStream() );
	        writer.write(Request);
	        writer.flush();
			writer.close();
	        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
	        String res_line;
	        while ((res_line = in.readLine()) != null)
	            buf.append(res_line);
	        in.close();
			System.out.println(buf.toString());
	        return buf.toString();
	    }catch (Exception e) {
	        e.printStackTrace();
	        return "error:"+e.getMessage();
	    }
		
	  }
	  
	    //dang ky/huy so cap doi 
	public String re_ungister_capdoi_api(String msisdn, String p_msin, String stockid, String ptime, String status, String comment,String ipclient, String opt){
		String Host="10.156.4.86";
		String Port="8080";
		String Uri="application/g/qlsp_update_isdn_date";
		//String strdate = "03/10/2018 23:59:59";
		long HOUR = 3600*1000;
			String strdate = "28/09/2018 00:00:00";
			String strdate1= "03/10/2018 23:59:59";
			try {
			SimpleDateFormat fmtDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = fmtDate.parse(strdate);
	
		      Date date1 = new Date();
				System.out.println(date1);
		     long timeInMillis = System.currentTimeMillis();
		     timeInMillis += 72*HOUR;
		     Date date2 = new Date(timeInMillis);
		     if(date1.getTime()<date.getTime())
		     {
		    	 		    	
		    	strdate = strdate1; 
		    	 
		     }
		     else {
		    	 strdate = fmtDate.format(date2);
		    	 
		    	 
		     }
			} catch(Exception ee)
			{
				System.out.println(ee.getMessage());
				
			}
			

		String Request = "{\"msisdn\": \""+msisdn+"\",\"p_isdn\": \""+p_msin+"\",\"p_stock_id\": \""+stockid+"\",\"p_date\": \""+strdate+"\",\"p_status\": \""+status+"\",\"p_comment\": \""+comment+"\",\"p_type\":\"COUPLE\",\"p_otp\":\""+opt+"\",\"client_key\":\""+ipclient+"\"}";		

		String token="eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzUxMiJ9.eyJhdWQiOiJjY2JzIiwidHBzIjoxMDAsInJvbGVzIjpbImxpbWl0ZWQtdXNlciJdLCJpc3MiOiJ0dW5ncGh1b25nIiwicmVzb3VyY2VzIjpbImcvKiJdLCJjYXRlZ29yaWVzIjpbIioiXSwiZXhwIjowLCJpcHMiOlsiMTAuMTQ5LjM0LjE2MiIsIjEwLjE0OS4zNC4xNjQiLCIxMC4xNDkuMzQuMTY1IiwiMTAuMTQ5LjM0LjE2NiIsIjEwLjE0OS45MS4xNDEiLCIxOTAuMTAuMTAuODkiLCIxMC4xNDkuMzQuMTE5IiwiMTAuMTQ5LjkxLjEyMCJdLCJqdGkiOiJjY2JzIn0=.3T7Zbsa/JLg2uB+QTxNA1TP/51zzwU9VsZWdPawW9BaVL7KML9ho+PDZ3pUXz9hIkXM2MPe6DNxAeDNTZxKp9A==";
		StringBuffer buf = new StringBuffer();	    
	    try {
	    	URL url = new URL("http://"+Host+":"+Port+"/"+Uri);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(1000);
			conn.setReadTimeout(1000);
			conn.setRequestMethod("POST");	        
			conn.setRequestProperty("Authorization",token);			
	        conn.setRequestProperty("Content-Type","application/json;charset=utf-8");	        	        	   
			conn.setRequestProperty("Content-Length", String.valueOf(Request.length()));
			conn.setRequestProperty("charset","utf-8");	  
			conn.setDoOutput(true);
	        conn.setDoInput(true);
	        OutputStreamWriter writer = new OutputStreamWriter( conn.getOutputStream() );
	        writer.write(Request);
	        writer.flush();
			writer.close();
	        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
	        String res_line;
	        while ((res_line = in.readLine()) != null)
	            buf.append(res_line);
	        in.close();
			System.out.println(buf.toString());
	        return buf.toString();
	    }catch (Exception e) {
	        e.printStackTrace();
	        return "error:"+e.getMessage();
	    }
		
	  }
	  	public String update_created_subs_ok(String msisdn, String p_isdn){
		String Host="10.156.4.86";
		String Port="8080";
		String Uri="application/g/update_created_subs_ok";
		//String strdate = "03/10/2018 23:59:59";
		

		String Request = "{\"msisdn\": \""+msisdn+"\",\"p_isdn\": \""+p_isdn+"\"}";		

		String token="eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzUxMiJ9.eyJhdWQiOiJjY2JzIiwidHBzIjoxMDAsInJvbGVzIjpbImxpbWl0ZWQtdXNlciJdLCJpc3MiOiJ0dW5ncGh1b25nIiwicmVzb3VyY2VzIjpbImcvKiJdLCJjYXRlZ29yaWVzIjpbIioiXSwiZXhwIjowLCJpcHMiOlsiMTAuMTQ5LjM0LjE2MiIsIjEwLjE0OS4zNC4xNjQiLCIxMC4xNDkuMzQuMTY1IiwiMTAuMTQ5LjM0LjE2NiIsIjEwLjE0OS45MS4xNDEiLCIxOTAuMTAuMTAuODkiLCIxMC4xNDkuMzQuMTE5IiwiMTAuMTQ5LjkxLjEyMCJdLCJqdGkiOiJjY2JzIn0=.3T7Zbsa/JLg2uB+QTxNA1TP/51zzwU9VsZWdPawW9BaVL7KML9ho+PDZ3pUXz9hIkXM2MPe6DNxAeDNTZxKp9A==";
		StringBuffer buf = new StringBuffer();	    
	    try {
	    	URL url = new URL("http://"+Host+":"+Port+"/"+Uri);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(1000);
			conn.setReadTimeout(1000);
			conn.setRequestMethod("POST");	        
			conn.setRequestProperty("Authorization",token);			
	        conn.setRequestProperty("Content-Type","application/json;charset=utf-8");	        	        	   
			conn.setRequestProperty("Content-Length", String.valueOf(Request.length()));
			conn.setRequestProperty("charset","utf-8");	  
			conn.setDoOutput(true);
	        conn.setDoInput(true);
	        OutputStreamWriter writer = new OutputStreamWriter( conn.getOutputStream() );
	        writer.write(Request);
	        writer.flush();
			writer.close();
	        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
	        String res_line;
	        while ((res_line = in.readLine()) != null)
	            buf.append(res_line);
	        in.close();
			System.out.println(buf.toString());
	        return buf.toString();
	    }catch (Exception e) {
	        e.printStackTrace();
	        return "error:"+e.getMessage();
	    }
		
	  }
	  
	  //5359--HIENTT
	  
	public String add_khloaikh(String makh,String loaikh,String plkh,String agent,String userid)
	{
		 String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_khdn.add_loaikh_func('"+makh+"','"+plkh+"','"+loaikh+"','"+agent+"','"+userid+"'); "; 			
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	}
	
	public String update_khloaikh(String makh,String loaikh,String plkh,String agent,String userid)
	{
		 String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_khdn.update_loaikh_func('"+makh+"','"+plkh+"','"+loaikh+"','"+agent+"','"+userid+"'); "; 			
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	}
	
	public String delete_khloaikh(String makh,String agent,String userid)
	{
		 String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
	xs_ = xs_ + " 	?:= admin_v2.pkg_khdn.delete_loaikh_func('"+makh+"','"+agent+"','"+userid+"');";  			
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	}
	
	public String accept_upcode_phanloaikh (								
									String upload_id,
									String upload_type,
									String funcschema,
									String funcCommon,
									String psuserip
	)throws Exception
	{ 
		String s = "begin ? :=admin_v2.upload_khdn.accept_upcode_phanloaikh("
			+"'"+upload_id+"',"
			+"'"+upload_type+"',"
			+"'"+getUserVar("sys_agentcode")+"',"
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";	
		System.out.println(s);
		return this.reqValue("",s);
	}
	
	

	   //dang ky/huy so cap doi 
	public String check_maxacthuc(String msisdn){
		String Host="10.156.4.86";
		String Port="8080";
		String Uri="application/g/get_secret_code";
		String Request = "{\"msisdn\": \""+msisdn+"\"}";		

		String token="eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzUxMiJ9.eyJhdWQiOiJjY2JzIiwidHBzIjoxMDAsInJvbGVzIjpbImxpbWl0ZWQtdXNlciJdLCJpc3MiOiJ0dW5ncGh1b25nIiwicmVzb3VyY2VzIjpbImcvKiJdLCJjYXRlZ29yaWVzIjpbIioiXSwiZXhwIjowLCJpcHMiOlsiMTAuMTQ5LjM0LjE2MiIsIjEwLjE0OS4zNC4xNjQiLCIxMC4xNDkuMzQuMTY1IiwiMTAuMTQ5LjM0LjE2NiIsIjEwLjE0OS45MS4xNDEiLCIxOTAuMTAuMTAuODkiLCIxMC4xNDkuMzQuMTE5IiwiMTAuMTQ5LjkxLjEyMCJdLCJqdGkiOiJjY2JzIn0=.3T7Zbsa/JLg2uB+QTxNA1TP/51zzwU9VsZWdPawW9BaVL7KML9ho+PDZ3pUXz9hIkXM2MPe6DNxAeDNTZxKp9A==";
		StringBuffer buf = new StringBuffer();	    
	    try {
	    	URL url = new URL("http://"+Host+":"+Port+"/"+Uri);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(1000);
			conn.setReadTimeout(1000);
			conn.setRequestMethod("POST");	        
			conn.setRequestProperty("Authorization",token);			
	        conn.setRequestProperty("Content-Type","application/json;charset=utf-8");	        	        	   
			conn.setRequestProperty("Content-Length", String.valueOf(Request.length()));
			conn.setRequestProperty("charset","utf-8");	  
			conn.setDoOutput(true);
	        conn.setDoInput(true);
	        OutputStreamWriter writer = new OutputStreamWriter( conn.getOutputStream() );
	        writer.write(Request);
	        writer.flush();
			writer.close();
	        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
	        String res_line;
	        while ((res_line = in.readLine()) != null)
	            buf.append(res_line);
	        in.close();
			System.out.println(buf.toString());
	        return buf.toString();
	    }catch (Exception e) {
	        e.printStackTrace();
	        return "error:"+e.getMessage();
	    }
		
	  }
	  public String add_bl_khdn(String msisdn,String agent,String userid, String psghichu)
	{
		 String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
	xs_ = xs_ + " 	?:= admin_v2.pkg_khdn.themmoi_blkhdn_func('"+msisdn+"','"+agent+"','"+userid+"','"+psghichu+"');";  			
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	
		
	}
		public String add_kenh_3GVPN (String p_name,String p_pdpcp,String p_apnid, String p_epsprofileid , String p_note, String p_userid)
  	{		
		String result="";
		String s="begin ?:=  admin_v2.pkg_khdn.add_kenh_3GVPN('"+p_name
							+"','"+p_pdpcp
							+"','"+p_apnid
							+"','"+p_epsprofileid
							+"','"+p_note
							+"','"+p_userid
							+"');"
							+" end;"
							;       
		try
			{		    
				 result=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	}  
	
	public String edit_kenh_3GVPN (String p_id,String p_name, String p_pdpcp,String p_apnid, String p_epsprofileid , String p_note, String p_userid)
  	{		
		String result="";
		String s="begin ?:=  admin_v2.pkg_khdn.edit_kenh_3GVPN('"+p_id
							+"','"+p_name
							+"','"+p_pdpcp
							+"','"+p_apnid
							+"','"+p_epsprofileid
							+"','"+p_note
							+"','"+p_userid
							+"');"
							+" end;"
							;         
		try
			{		    
				 result=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	} 
	
	public String reg_IP_3GVPN(String psip,String psmsisdn,String psid, String psstatic, String psuserid, String psmatinh)
	{
		 String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_khdn.reg_ip_vpn_func('"+psip+"','"+psmsisdn+"','"+ psid+"','"+psstatic+"','"+psuserid+"','"+psmatinh+"'); ";		
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	}
	
	public String unreg_IP_3GVPN(String psmsisdn, String psuserid, String psmatinh)
	{
		String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_khdn.unreg_ip_vpn_func('"+psmsisdn+"','"+psuserid+"','"+psmatinh+"'); ";		
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	}
	public String delete_bl_khdn(String msisdn,String agent,String userid)
	{
		 String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
	xs_ = xs_ + " 	?:= admin_v2.pkg_khdn.delete_blkhdn_func('"+msisdn+"','"+agent+"','"+userid+"');";  			
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	}
	 public String accept_upload_bl_khdn (								
									String upload_id,
									String upload_type,
									String funcschema,
									String funcCommon,
									String psuserip
	)throws Exception
	{ 
		String s = "begin ? :=admin_v2.pkg_upload_khdn.accept_upload_bl_khdn("
			+"'"+upload_id+"',"
			+"'"+upload_type+"',"
			+"'"+getUserVar("sys_agentcode")+"',"
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";	
		System.out.println(s);
		return this.reqValue("",s);
	}
}
