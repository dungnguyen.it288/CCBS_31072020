package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_hddm
extends NEOProcessEx {
	public void run()
	{
		System.out.println("neo.pttb_hddm was called");
	}
	
	public String getUrlDSHD(int trangthaihd_id)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hddm/ajax_layds_hddm"
		+"&"+CesarCode.encode("trangthaihd_id")+"="+trangthaihd_id
        ;
	}
	
	public String getUrlttHopdongDM(String schema, String ma_hd, String ma_tb, String user_id)
	{
		String s = "begin ? := " + CesarCode.decode(schema) + "PTTB_PPS_VMSM.laytt_tb_hddm('"
		+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")
		+"','" + ma_hd + "'"
		+ ",'" + ma_tb + "'"
		+");"
		+ " end;";
		return s;
	}
	
	public String capnhat_hddm(String schema   
        ,String psMA_HD
        ,String pdNGAY_LAPHD
        ,String psMA_TB
        ,String psTEN_KH
        ,String psDIACHI
        ,String piLOAI_GT
        ,String psSO_GT
        ,String pdNGAY_CAP_GT
		,String psNOI_CAP_GT
        ,String pdNGAY_SINH
        ,String psGHI_CHU
        ,String psMAY_CN
		,String psMASP_CU
		,String psLOAISP_MOI	
		,String piTIEN_MAY
		,String piVAT_MAY
		,String piTIEN_DADC
		,String piVAT_DADC
		,String piTIEN_DC
		,String piVAT_DC
		)
	{
		String s = "begin ? := admin_v2.pttb_chucnang.capnhat_hddm_vsms_giahan('"+getUserVar("userID")
					+"','"+getUserVar("sys_agentcode")
					+"','"+getUserVar("sys_dataschema")+"','"
					+psMA_HD + "'"
					+ ",'" + pdNGAY_LAPHD + "'"
					+ ",'" + psMA_TB + "'"
					+ ",'" + psTEN_KH + "'"
					+ ",'" + psDIACHI + "'"
					+ ",'" + piLOAI_GT + "'"
					+ ",'" + psSO_GT + "'"
					+ ",'" + pdNGAY_CAP_GT + "'"
					+ ",'" + psNOI_CAP_GT + "'"					
					+ ",'" + pdNGAY_SINH + "'"
					+ ",'" + psGHI_CHU + "'"
					+ ",'" + psMAY_CN + "'"
					+ ",'" + psMASP_CU + "'"
					+ ",'" + psLOAISP_MOI + "'"
					+ ",'" + piTIEN_MAY + "'"
					+ ",'" + piVAT_MAY + "'"
					+ ",'" + piTIEN_DADC + "'"
					+ ",'" + piVAT_DADC + "'"
					+ ",'" + piTIEN_DC + "'"
					+ ",'" + piVAT_DC + "'"
					+ ");"
					+ "end;";			
		System.out.println("naTuan"+s);
		return s;		
	}
	
	public String xoa_HDDM(String schema, String ma_hd)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "PTTB_PPS_VMSM.xoa_hddm('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_hd + "'" + ");"
		+ " end;";
		System.out.println(s);
		return s;
	}
	

	
}