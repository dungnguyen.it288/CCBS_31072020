package neo;

//import java.io.File;
//import java.io.IOException;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.RowSet;

import org.apache.commons.io.FileUtils;

import neo.smartui.SessionProcess;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

import neo.qlsp.*;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import javax.xml.parsers.DocumentBuilder; 
import javax.xml.parsers.DocumentBuilderFactory; 
import javax.xml.parsers.ParserConfigurationException; 
import org.w3c.dom.Document; 
import org.w3c.dom.Element; 
import org.w3c.dom.Node; 
import org.w3c.dom.NodeList; 
import org.xml.sax.InputSource;

public class tracuu_tb extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("Search");		
	}
	
	public String getSession(){
		String session = "";
		File file = new File (this.getSysConst("ConfigCompiledDir")+"pttb/laphd/chonso/session_api.neo");
		if (!file.exists() || file.lastModified()<System.currentTimeMillis()-50000){
			boolean del = true;
			if (file.exists()){
				del=file.delete();
			}			
			session = Get_API_Search.login();			
			if (del){
				try {
					FileUtils.writeStringToFile(file,session);
				} catch (IOException e) {
					session = e.getMessage();
				}
			}
		}else{
			try {
				session = FileUtils.readFileToString(file);
			} catch (IOException e) {}
		}
		return session;
	}
	

	public String doc_lichsu_3g(String psSoTB)
	{
		try{
			boolean blnCheck = SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"ajax_lichsu_3gs", getUserVar("userID"),
					getUserVar("sys_agentcode"),"10|1|30");
			if(!blnCheck)
			return  "/main?"+CesarCode.encode("configFile")+"=admin/not-access";
			reqValue("begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
						+psSoTB+"','ajax_lichsu_3gs'," 
						+"'"+psSoTB+";',"
						+"'','"
						+getUserVar("userID")+"','"
						+getEnvInfo().getParserParam("sys_userip")+"','"
						+getUserVar("sys_agentcode")+"'); "
						+ "   commit; "
						+ " end; ");
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		
		try{
			String session = getSession();
			StringBuffer ret = new StringBuffer();
			String jsonString = Get_API_Search.getDataCCBS(session,psSoTB);	
			String tpl = FileUtils.readFileToString(new File(this.getSysConst("ConfigDir")+"vinacore/ajax_danhsach_lichsu_3gs.htm"));
			JSONParser jsonParser = new JSONParser();	            
			JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonString);	                           
		            
			JSONArray jsonArray = (JSONArray) jsonObject.get("result");
			int stt;
			int i;
			for (i = 0; i < jsonArray.size(); i++) {
				JSONObject jsonObject_ = (JSONObject) jsonArray.get(i);				
				stt				= i+1;
				String id 				= (String) jsonObject_.get("ID");
				String msisdn 			= (String) jsonObject_.get("MSISDN");
				String service_id		= (String) jsonObject_.get("SERVICE_ID");
				String service_code		= (String) jsonObject_.get("SERVICE_CODE");
				String time_start		= (String) jsonObject_.get("TIME_START");	
				String time_end			= (String) jsonObject_.get("TIME_END");	
				String active			= (String) jsonObject_.get("ACTIVE");
				String last_modify_time	= (String) jsonObject_.get("LAST_MODIFY_TIME");				
				String content			= (String) jsonObject_.get("CONTENT");
				String direction_name 	= (String) jsonObject_.get("DIRECTION_NAME");					
				String thaotac="";
				if (active.equals("Y")){
					thaotac="Mo";					
				}else thaotac="Dong";				
										
				ret.append(tpl.replace("{f01stt}",stt+"").replace("{f01service_id}",service_id).replace("{f01service_code}",service_code).replace("{f01active}",thaotac).replace("{f01time_start}",time_start).replace("{f01time_end}",time_end).replace("{f01direction_name}",direction_name));			
			}	  
				
			getEnvInfo().setUserVar("timkiem_api",ret.toString());	
			
		} catch (Exception e) {
		    e.printStackTrace();
		}
				
		return "/main?" + CesarCode.encode("configFile")+"=vinacore/ajax_lichsu_3gs_api";
	}
	
	
	public static String getName(String pstrTagName, Element peNode) {
         NodeList nodeList = peNode.getElementsByTagName(pstrTagName);
         Element element1 = (Element) nodeList.item(0);

         if (element1 != null) {
             return element1.getChildNodes().item(0).getNodeValue();
         }
         return null;
     }
	 
	public String doc_layds_vascloud(
		String psMsisdn,
		String psMonth,
		String psYear,
		String psUserId,
		String psIP
	) 
	{
		try{
			boolean blnCheck = SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"doc_layds_vascloud", getUserVar("userID"),
					getUserVar("sys_agentcode"),"10|1|30");
			if(!blnCheck)
			return  "/main?"+CesarCode.encode("configFile")+"=admin/not-access";
			reqValue("begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
						+psMsisdn+"','doc_layds_vascloud'," 
						+"'"+psMsisdn+";',"
						+"'','"
						+getUserVar("userID")+"','"
						+getEnvInfo().getParserParam("sys_userip")+"','"
						+getUserVar("sys_agentcode")+"'); "
						+ "   commit; "
						+ " end; ");
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		
		try{
			String xml = Get_VasCloud.getusehist(psMsisdn, psMonth, psYear, psUserId, psIP);
			StringBuffer ret = new StringBuffer();
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(new InputSource(new StringReader(xml)));			
	        NodeList entrieErrors = document.getElementsByTagName("RPLY");			
			String tpl = FileUtils.readFileToString(new File(this.getSysConst("ConfigDir")+"ccbs_inhoadon/tracuu_vascloud/ajax_danhsach_data.htm"));
			int stt;
	        for (int j = 0; j < entrieErrors.getLength(); j++) 
	        {
	           Element nodeError = (Element) entrieErrors.item(j);	           
	            if (nodeError.getNodeType() == Node.ELEMENT_NODE) {
	                String error = getName("error", nodeError);
	                    if ("0".equals(error)) {
	                        NodeList entries = document.getElementsByTagName("use_hist_item");
	                        for (int i = 0; i < entries.getLength(); i++) 
							{
	                            Element node = (Element) entries.item(i);									                            
	                            if (node.getNodeType() == Node.ELEMENT_NODE) 
								{	                                
									stt					= i+1;
	                                String msisdn 		= getName("msisdn", node);
									String ten_dvhd 	= "VAS CLOUD";
									String service_name = getName("service_name", node);
									String channel 		= getName("channel", node);
									String action_date 	= getName("action_date", node);
									String price 		= getName("price", node);									
									ret.append(tpl.replace("{f01stt}",stt+"").replace("{f01msisdn}",msisdn).replace("{f01ten_dvhd}",ten_dvhd).replace("{f01service_name}",service_name).replace("{f01channel}",channel).replace("{f01action_date}",action_date).replace("{f01price}",price));
	                            }
	                        }   
	                    }                   
	             }
	        }  
			
			getEnvInfo().setUserVar("print_api",ret.toString());
			//System.out.println(xml);
		} catch (Exception e) {
		    e.printStackTrace();
		}
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_inhoadon/tracuu_vascloud/ajax_danhsach";			
	}
	
}