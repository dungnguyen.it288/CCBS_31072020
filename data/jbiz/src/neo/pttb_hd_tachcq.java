package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_hd_tachcq
    extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb_hd_tachcq was called");
  }

  public String getUrlBuuCucThu(String value) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/capnhat/khachhang/ajax_buucucthu"
        +"&"+CesarCode.encode("donviql_id")+"="+value
        ;
  }
  public String getUrlNganhNghe(String value) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/capnhat/khachhang/ajax_nganhnghe"
        +"&"+CesarCode.encode("+loainn_id")+"="+value
        ;
  }
  public String layDsKhachHang(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.layds_kh_trong_cq('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }
  public String getUrlDsKhachHang(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/capnhat/khachhang/ajax_layds_kh_trong_cq"
        +"&"+CesarCode.encode("userinput")+"="+userInput
        ;
  }
  public String getUrlDsThueBaoCD(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/capnhat/danhba/ajax_layds_tbcd_trong_cq"
        +"&"+CesarCode.encode("userinput")+"="+userInput
        ;
  }
  public String getUrlDsThueBaoDD(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/capnhat/danhba/ajax_layds_tbdd_trong_cq"
        +"&"+CesarCode.encode("userinput")+"="+userInput
        ;
  }
  public String getUrlDsThueBaoIN(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/capnhat/danhba/ajax_layds_tbin_trong_cq"
        +"&"+CesarCode.encode("userinput")+"="+userInput
        ;
  }
  public String getUrlDsThueBaoEM(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/capnhat/danhba/ajax_layds_tbem_trong_cq"
        +"&"+CesarCode.encode("userinput")+"="+userInput
        ;
  }

  public String capnhatKhachHangPTTB(String schema, String pima_cq, String pscoquan
                                     ,String pssodaidien,String psdienthoai_lh,String  pscmt
                                     ,String psngaycap_cmt,String psnoicap_cmt,String pshokhau
                                     ,String psngaycap_hokhau,String psnoicap_hokhau
                                     ,String piquan_id,String piphuong_id,String pipho_id
                                     ,String psso_nha,String psdiachi_ct,String pidichvuvt_id
                                     ,String psma_kh,String psten_tt,String piquantt_id
                                     ,String piphuongtt_id,String piphott_id,String pssott_nha
                                     ,String psdiachi_tt,String psma_bc,String pidiadiemtt_id
                                     ,String psma_nv,String psma_t,String pinganhang_id
                                     ,String pstaikhoan,String psms_thue,String pidonviql_id
                                     ,String piloaikh_id,String pikhlon_id,String pichuky_id
                                     ,String pidangky_tv,String pitrangthai_id
                                     ,String piuutien_id,String pinganhnghe_id,String pidangky_db
                                     ,String psghichu,String pdngay_cn,String psnguoi_cn,String psmay_cn) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_capnhat.capnhat_kh_pttb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
        +pima_cq
        +",'"+pscoquan+"'"
        +",'"+pssodaidien+"'"
        +",'"+psdienthoai_lh+"'"
        +",'"+pscmt+"'"
        +",'"+psngaycap_cmt+"'"
        +",'"+psnoicap_cmt+"'"
        +",'"+pshokhau+"'"
        +",'"+psngaycap_hokhau+"'"
        +",'"+psnoicap_hokhau+"'"
        +","+piquan_id
        +","+piphuong_id
        +","+pipho_id
        +",'"+psso_nha+"'"
        +",'"+psdiachi_ct+"'"
        +","+pidichvuvt_id
        +",'"+psma_kh+"'"
        +",'"+psten_tt+"'"
        +","+piquantt_id
        +","+piphuongtt_id
        +","+piphott_id
        +",'"+pssott_nha+"'"
        +",'"+psdiachi_tt+"'"
        +",'"+psma_bc+"'"
        +","+pidiadiemtt_id
        +",'"+psma_nv+"'"
        +",'"+psma_t+"'"
        +","+pinganhang_id
        +",'"+pstaikhoan+"'"
        +",'"+psms_thue+"'"
        +","+pidonviql_id
        +","+piloaikh_id
        +","+pikhlon_id
        +","+pichuky_id
        +","+pidangky_tv
        +","+pitrangthai_id
        +","+piuutien_id
        +","+pinganhnghe_id
        +","+pidangky_db
        +","+psghichu+""
        +","+pdngay_cn
        +",'"+psnguoi_cn+"'"
        +",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;

  }
  public String capnhatDanhBaCDPTTB(String schema
				,String psSOMAY
				,String piTYLE_VAT
				,String piCUOCNH_ID
				,String piQUAN_ID
				,String piPHUONG_ID
				,String piPHO_ID
				,String piTHUTU_IN
				,String psSO_NHA
				,String piGIAPDANH
				,String piDANGKY_DB
				,String psTEN_DB
				,String piTRAMVT_ID
				,String psTEN_TB
				,String psDIACHI
				,String piCUOC_TB
				,String piCUOC_DV
				,String piLOAITB_ID
				,String pdNGAY_LD
				,String pdNGAY_TD
				,String pdNGAY_TTHOAI
				,String piINCHITIET
				,String psGHICHU
				,String piTRANGTHAI_ID
				,String piDONVIQL_ID
				,String piDOITUONG_ID
				,String piDONVITC_ID
				,String psNGUOI_CN
	) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_capnhat.capnhat_tbcd_pttb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psSOMAY+"',"
		+piTYLE_VAT+","
		+piCUOCNH_ID+","
		+piQUAN_ID+","
		+piPHUONG_ID+","
		+piPHO_ID+","
		+piTHUTU_IN+","
		+"'"+psSO_NHA+"',"
		+piGIAPDANH+","
		+piDANGKY_DB+","
		+"'"+psTEN_DB+"',"
		+piTRAMVT_ID+","
		+"'"+psTEN_TB+"',"
		+"'"+psDIACHI+"',"
		+piCUOC_TB+","
		+piCUOC_DV+","
		+piLOAITB_ID+","
		+pdNGAY_LD+","
		+pdNGAY_TD+","
		+pdNGAY_TTHOAI+","
		+piINCHITIET+","
		+psGHICHU+","
		+piTRANGTHAI_ID+","
		+piDONVIQL_ID+","
		+piDOITUONG_ID+","
		+piDONVITC_ID+","
                +"'"+psNGUOI_CN+"'"
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;

  }
  public String capnhatDanhBaDDPTTB(String schema
                                ,String psSOMAY
                                ,String piTYLE_VAT
                                ,String piQUAN_ID
                                ,String piPHUONG_ID
                                ,String piPHO_ID
                                ,String piTHUTU_IN
                                ,String psSO_NHA
                                ,String piDANGKY_DB
                                ,String psTEN_DB
                                ,String psTEN_TB
                                ,String psDIACHI
                                ,String piCUOC_TB
                                ,String piCUOC_DV
                                ,String piLOAITB_ID
                                ,String pdNGAY_LD
                                ,String pdNGAY_TD
                                ,String pdNGAY_TTHOAI
                                ,String piINCHITIET
                                ,String psGHICHU
                                ,String piTRANGTHAI_ID
                                ,String piDONVIQL_ID
                                ,String piDOITUONG_ID
                                ,String psNGUOI_CN
        ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_capnhat.capnhat_tbdd_pttb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psSOMAY+"',"
                +piTYLE_VAT+","
                +piQUAN_ID+","
                +piPHUONG_ID+","
                +piPHO_ID+","
                +piTHUTU_IN+","
                +"'"+psSO_NHA+"',"
                +piDANGKY_DB+","
                +"'"+psTEN_DB+"',"
                +"'"+psTEN_TB+"',"
                +"'"+psDIACHI+"',"
                +piCUOC_TB+","
                +piCUOC_DV+","
                +piLOAITB_ID+","
                +pdNGAY_LD+","
                +pdNGAY_TD+","
                +pdNGAY_TTHOAI+","
                +piINCHITIET+","
                +psGHICHU+","
                +piTRANGTHAI_ID+","
                +piDONVIQL_ID+","
                +piDOITUONG_ID+","
                +"'"+psNGUOI_CN+"'"
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;

  }
  public String capnhatDanhBaINPTTB(String schema
                                ,String psCALLING
                                ,String piTYLE_VAT
                                ,String piQUAN_ID
                                ,String piPHUONG_ID
                                ,String piPHO_ID
                                ,String piTHUTU_IN
                                ,String psSO_NHA
                                ,String piDANGKY_DB
                                ,String psTEN_DB
                                ,String piTRAMVT_ID
                                ,String psTEN_TB
                                ,String psDIACHI
                                ,String piCUOC_TB
                                ,String piCUOC_DV
                                ,String piLOAITB_ID
                                ,String pdNGAY_LD
                                ,String pdNGAY_TD
                                ,String pdNGAY_TTHOAI
                                ,String piINCHITIET
                                ,String psGHICHU
                                ,String piTRANGTHAI_ID
                                ,String piDONVIQL_ID
                                ,String piDOITUONG_ID
                                ,String psSOMAY_TN
                                ,String psACCOUNT

                                ,String psNGUOI_CN
        ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_capnhat.capnhat_tbin_pttb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psCALLING+"',"
                +piTYLE_VAT+","
                +piQUAN_ID+","
                +piPHUONG_ID+","
                +piPHO_ID+","
                +piTHUTU_IN+","
                +"'"+psSO_NHA+"',"
                +piDANGKY_DB+","
                +"'"+psTEN_DB+"',"
                +piTRAMVT_ID+","
                +"'"+psTEN_TB+"',"
                +"'"+psDIACHI+"',"
                +piCUOC_TB+","
                +piCUOC_DV+","
                +piLOAITB_ID+","
                +pdNGAY_LD+","
                +pdNGAY_TD+","
                +pdNGAY_TTHOAI+","
                +piINCHITIET+","
                +psGHICHU+","
                +piTRANGTHAI_ID+","
                +piDONVIQL_ID+","
                +piDOITUONG_ID+","
                +"'"+psSOMAY_TN+"',"
                +"'"+psACCOUNT+"',"
                +"'"+psNGUOI_CN+"'"
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;

  }
  public String capnhatDanhBaEMPTTB(String schema
                                ,String psACCOUNT
                                ,String piTYLE_VAT
                                ,String piQUAN_ID
                                ,String piPHUONG_ID
                                ,String piPHO_ID
                                ,String piTHUTU_IN
                                ,String psSO_NHA
                                ,String piDANGKY_DB
                                ,String psTEN_DB
                                ,String psTEN_TB
                                ,String psDIACHI
                                ,String piCUOC_TB
                                ,String piCUOC_DV
                                ,String piLOAITB_ID
                                ,String pdNGAY_LD
                                ,String pdNGAY_TD
                                ,String pdNGAY_TTHOAI
                                ,String piINCHITIET
                                ,String psGHICHU
                                ,String piTRANGTHAI_ID
                                ,String piDONVIQL_ID
                                ,String piDOITUONG_ID
                                ,String psSOMAY_TN
                                ,String psNGUOI_CN
        ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_capnhat.capnhat_tbem_pttb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psACCOUNT+"',"
                +piTYLE_VAT+","
                +piQUAN_ID+","
                +piPHUONG_ID+","
                +piPHO_ID+","
                +piTHUTU_IN+","
                +"'"+psSO_NHA+"',"
                +piDANGKY_DB+","
                +"'"+psTEN_DB+"',"
                +"'"+psTEN_TB+"',"
                +"'"+psDIACHI+"',"
                +piCUOC_TB+","
                +piCUOC_DV+","
                +piLOAITB_ID+","
                +pdNGAY_LD+","
                +pdNGAY_TD+","
                +pdNGAY_TTHOAI+","
                +piINCHITIET+","
                +psGHICHU+","
                +piTRANGTHAI_ID+","
                +piDONVIQL_ID+","
                +piDOITUONG_ID+","
                +"'"+psSOMAY_TN+"',"
                +"'"+psNGUOI_CN+"'"
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;

  }

}
