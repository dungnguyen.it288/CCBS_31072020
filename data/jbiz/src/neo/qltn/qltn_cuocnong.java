package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import java.util.Random;

public class qltn_cuocnong extends NEOProcessEx
{
	public void run() 
	{
    	System.out.println("Cuoc nong was called-Updated 16/07/2007");    	
    	//	getSysConst("FuncSchema");
    	//	getUserVar("sys_dataschema");
    	//	getUserVar("userID");
  	}
  	
	public String doc_phieutra(
		String pstuKyHD,
		String psdsphieu_id,
		String psma_kh,
		String psma_tb,
		String psten_tt,
		String psdiachi_tt,
		String pstuNgayTT,
		String psdenNgayTT,
		String psMaTN,
		String schema,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_cuocnong/phieutras/ajax_phieutra"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("phieus")+"="+psdsphieu_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("ten_tt")+"="+psten_tt
			+"&"+CesarCode.encode("diachi_tt")+"="+psdiachi_tt
			+"&"+CesarCode.encode("tungay")+"="+pstuNgayTT
			+"&"+CesarCode.encode("denngay")+"="+psdenNgayTT
			+"&"+CesarCode.encode("matn")+"="+psMaTN
			+"&"+CesarCode.encode("schema")+"="+schema
			+"&"+CesarCode.encode("page_num")+"="+psPageNum
			+"&"+CesarCode.encode("page_rec")+"="+psPageRec;
	}
	public String doc_phieuhuy(
		String pstuKyHD,
		String psdsphieu_id,
		String psma_kh,
		String psma_tb,
		String psten_tt,
		String psdiachi_tt,
		String pstuNgayTT,
		String psdenNgayTT,
		String psMaTN,
		String schema,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_cuocnong/phieutras/ajax_phieuhuy"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("phieus")+"="+psdsphieu_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("ten_tt")+"="+psten_tt
			+"&"+CesarCode.encode("diachi_tt")+"="+psdiachi_tt
			+"&"+CesarCode.encode("tungay")+"="+pstuNgayTT
			+"&"+CesarCode.encode("denngay")+"="+psdenNgayTT
			+"&"+CesarCode.encode("matn")+"="+psMaTN
			+"&"+CesarCode.encode("schema")+"="+schema
			+"&"+CesarCode.encode("page_num")+"="+psPageNum
			+"&"+CesarCode.encode("page_rec")+"="+psPageRec;
	}
	
	public String rec_lay_tt_khachhang(
		String psUserInput,
		String psChukyno,
		String psDonviQL
	)
	{
		return "begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.laytt_khachhang_donvi('"+
			psUserInput+"','"+
			psChukyno+"','"+
			psDonviQL+"','"+
			getUserVar("sys_dataschema")+"','"+
			getUserVar("userID")+"'); end;";
	}
	public String doc_lay_ds_cuocnong(
		String psUserInput,
		String psTheoKM
	)
	{
		String theo_km="";
		//if (psTheoKM=="1") theo_km="_theokm";
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_cuocnong/ajax_lay_ds_cuocnong"+theo_km
  			+ "&" + CesarCode.encode("userinput") + "=" + psUserInput
  			+ "&" + CesarCode.encode("theokm") + "=" + psTheoKM;
	}
	public String rec_lay_tongno(
		String psMaKH,
		String psMaTB,		
		String psChukyno,
		String psLoaiTien
	)
	{
		return "begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.laytt_kh_cuocnong('"
			+psChukyno+"','"
			+psMaKH+"','"
			+psMaTB+"','"
			+psLoaiTien+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
	}
	public String rec_laytt_cuocnong(
		String psMaKH,
		String psMaTB,
		String psChukyno
	)
	{
		return "begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.laytt_cuocnong('"
			+psMaKH+"','"
			+psMaTB+"','"
			+psChukyno+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("sys_agentcode")+"','"
			+getUserVar("userID")+"'); end;";
	}
	public String rec_notonghop(
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psLoaiTienID	
	)
	{
		String s= "begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.priv_laytt_notonghop_new('"
			+psChukyno+"','"
			+psMaKH+"','"
			+psMaTB+"','"
			+psLoaiTienID+"','"
			+"0','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"',1); end;";
		System.out.println(s);
		return s;
  	}
}