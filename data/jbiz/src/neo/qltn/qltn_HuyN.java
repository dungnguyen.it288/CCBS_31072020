package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class qltn_HuyN
extends NEOProcessEx {
  private String ThePackage  = "qltn_tracuu.";
  public void run() {
    System.out.println("neo.qltn was called");
  }

  public String test(String psSchema)
  {
    return "begin ?:=" + psSchema+ ThePackage + "test;"
      + " end;"
      ;

  }

  public String getUrl(String manv,String chukyno,String loaituyen,String donviql_id)
  {
    if (loaituyen=="1")
    {
      return "/main?" + CesarCode.encode("configFile") +
          "=ccbs_qltn/qltn_ChuyenTuyenTheoThang/ajax_ds_KH_TuyenCu"
          + "&" + CesarCode.encode("ma_nv") + "=" + manv
          + "&" + CesarCode.encode("chukyno") + "=" + chukyno
          + "&" + CesarCode.encode("loaituyen") + "=" + loaituyen
          + "&" + CesarCode.encode("donviql_id") + "=" + donviql_id
          ;
    }
    else if (loaituyen=="0")
    {
      return "/main?" + CesarCode.encode("configFile") +
          "=ccbs_qltn/qltn_ChuyenTuyenTheoThang/ajax_ds_KH_TuyenMoi"
          + "&" + CesarCode.encode("ma_nv") + "=" + manv
          + "&" + CesarCode.encode("chukyno") + "=" + chukyno
          + "&" + CesarCode.encode("loaituyen") + "=" + loaituyen
          + "&" + CesarCode.encode("donviql_id") + "=" + donviql_id
          ;
    }
    else
    {
      return "";
    }
  }

  public String ChuyenTuyen_Thang(
          String ma_nv_cu
        , String chukyno
        , String ma_nv_moi
        , String psThang
       , String dsMaKH
       ,String psFuncSchema
        , String psSchema
       , String psUserID

        )

  {
    String result;
    result = "begin ?:=" + psFuncSchema+ ThePackage + "chuyentuyen_thang("
        + "'" + ma_nv_cu + "',"
        + "'" + chukyno + "',"
        + "'" + ma_nv_moi + "',"
        + "'" + psThang + "',"
        + dsMaKH + ","
        + "'" + psSchema + "',"
        + "'" + psUserID
        + "'" + ");"
        + " end;"
        ;	
	return result;
  }
}
