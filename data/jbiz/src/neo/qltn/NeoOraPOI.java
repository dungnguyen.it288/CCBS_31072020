package neo.qltn;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.*;
import java.io.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

public class NeoOraPOI 
{		
	public void CreateExcelFile(String Output_File_Path, String SQL_Statement) 
		throws SQLException, IOException
	{
		//Truy cap Database
		DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());		
		String url="jdbc:oracle:thin:@//10.252.15.53:1521/neo";
		Connection conn = DriverManager.getConnection(url,"qbh_admin_backup","a");
		conn.setAutoCommit(false);
		Statement sql=conn.createStatement();
		ResultSet rSet=sql.executeQuery(SQL_Statement);		
		ResultSetMetaData rSetMeta = rSet.getMetaData();

		FileOutputStream fos = new FileOutputStream(new File(Output_File_Path), true);
		HSSFWorkbook wb = new HSSFWorkbook();		
		HSSFSheet sheet = wb.createSheet("Page 1");		
		//createSheetHeader(wb, sheet, rSetMeta, wb.getCellStyleAt((short)(0)));		
		int rowCount = 0, sheetCount=1;		
		while (rSet.next())
		{
			if (rowCount==65535)
			{				
				rowCount=0;sheetCount++;
				sheet=wb.createSheet("Page "+sheetCount);
				createSheetHeader(wb, sheet, rSetMeta, wb.getCellStyleAt((short)(0)));				
			}			
			rowCount++;
			HSSFRow dataRow = sheet.createRow(rowCount);			
			for (int i = 1; i < rSetMeta.getColumnCount() + 1; i++) 
			{				
				dataRow.createCell((short) (i - 1)).setCellValue(rSet.getString(i));
			}			
		}
		wb.write(fos);
		fos.close();
		sql.close();
	}
	
	public void createSheetHeader(HSSFWorkbook WorkBook, HSSFSheet Sheet, ResultSetMetaData SQL_MetaData, HSSFCellStyle Style)
		throws SQLException
	{
		HSSFCellStyle style=WorkBook.createCellStyle();
		style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
	    style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);	    
	    style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
	    style.setBottomBorderColor(HSSFColor.BLACK.index);
	    style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
	    style.setLeftBorderColor(HSSFColor.BLACK.index);
	    style.setBorderRight(HSSFCellStyle.BORDER_THIN);
	    style.setRightBorderColor(HSSFColor.BLACK.index);
	    style.setBorderTop(HSSFCellStyle.BORDER_THIN);
	    style.setTopBorderColor(HSSFColor.BLACK.index);	    
		HSSFRow dataRow = Sheet.createRow(0);		
		for (int i = 1; i < SQL_MetaData.getColumnCount() + 1; i++) 
		{			
			HSSFCell cell = dataRow.createCell((short) (i - 1));			
			cell.setCellValue(SQL_MetaData.getColumnLabel(i));			
		}
	}
}