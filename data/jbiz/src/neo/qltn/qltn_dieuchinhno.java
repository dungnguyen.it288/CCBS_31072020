package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class qltn_dieuchinhno extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("QLTN Gach no");
		//funcschema=getSysConst("FuncSchema");
    	//dataschema=getUserVar("sys_dataschema");
    	//userid=getUserVar("userID");		
	}
	public String value_tong_phieu_gach(
		String psChukyno,
		String psNgayTT
	)
	{
		return "begin SELECT COUNT(PHIEU_ID) into ? from "+getUserVar("sys_dataschema")+"BANGPHIEUTRA_"+psChukyno+" PH where PH.nguoigach='"+getUserVar("userID")+"' AND PH.ngay_tt=TO_DATE('"+psNgayTT+"','DD/MM/YYYY'); end;";		
	}
	public String value_tong_tien_gach(
		String psChukyno,
		String psNgayTT
	)
	{
		return "begin select nvl(to_char( SUM(TRAGOC)+SUM(TRATHUE),'999,999,999,999' ),0) into ? from "+getUserVar("sys_dataschema")+"CT_TRA_"+psChukyno+" CT, "+getUserVar("sys_dataschema")+"BANGPHIEUTRA_"+psChukyno+" PH WHERE CT.phieu_id =PH.phieu_id AND PH.nguoigach='"+getUserVar("userID")+"' AND PH.ngay_tt=TO_DATE('"+psNgayTT+"','DD/MM/YYYY'); end;";
	}
	public String lay_tt_khachhang(
  		String psUserInput,
  		String psDonviQL,
  		String psChukyno
  		)
  	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_laytt_khachhang"
  			+ "&" + CesarCode.encode("userinput") + "=" + psUserInput
  			+ "&" + CesarCode.encode("don_vi_ql") + "=" + psDonviQL
  			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno;
  	}
  	public String lay_diadiem_tt(
  		String psMaKH
  	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_lay_diadiem_tt_dc"
  			+ "&" + CesarCode.encode("ma_kh") + "=" + psMaKH
  			;	
	}
	public String lay_no_tong_hop(
		String psMaKH,
		String psMaTB,
		String psLoaiTienID,
		String psChukyno
	)
	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_lay_no_tonghop"
  			+ "&" + CesarCode.encode("ma_kh") + "=" + psMaKH
  			+ "&" + CesarCode.encode("ma_tb") + "=" + psMaTB
  			+ "&" + CesarCode.encode("id_tien") + "=" + psLoaiTienID
  			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno;
  	}
  	public String doc_no_chukyno(
		String psMaKH,
		String psMaTB,		
		String psChukyno,
		String psLoaiTienID
	)
	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_lay_no_chukyno"
  			+ "&" + CesarCode.encode("ma_kh") + "=" + psMaKH
  			+ "&" + CesarCode.encode("ma_tb") + "=" + psMaTB
  			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
  			+ "&" + CesarCode.encode("loaitien_id") + "=" + psLoaiTienID;
  	}  	
  	public String lay_no_khoanmuc(
		String psMaKH,
		String psMaTB,		
		String psDsChukyno,
		String psChukyno,
		String psLoaiTienID
	)
	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_lay_dcno_theo_km"
  			+ "&" + CesarCode.encode("ma_kh") + "=" + psMaKH
  			+ "&" + CesarCode.encode("ma_tb") + "=" + psMaTB
  			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
  			+ "&" + CesarCode.encode("dschukyno") + "=" + psDsChukyno
  			+ "&" + CesarCode.encode("loaitien_id") + "=" + psLoaiTienID;
  	}  	
  	public String lay_mathanhtoan(
  		String psMaTB,
  		String psDonViQL,
  		String psChukyno
  	)
	{
		return "begin ? := "+getSysConst("FuncSchema")+"qltn_tracuu.laytt_khachhang_donvi('"+psMaTB+"','"+psChukyno+"',"+psDonViQL+",'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
	}
	
	public String dieuchinhno
	(	
		String pschukyno,
		String psma_kh,
		String psma_tb,
		String psdskmdcdk,
		String psdstiendcdk,
		String psdskmdcps,
		String psdstiendcps,
		String psngay_tt,
		String psloaitien_id,
		String psluottt,
		String psthangdc,
		String pspsghichu
	)
	{
		String s= "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.dieuchinhno("
			+"'"+pschukyno+"'"
			+",'"+psma_kh+"'"
			+",'"+psma_tb+"'"
			+",'"+psdskmdcdk+"'"
			+",'"+psdstiendcdk+"'"
			+",'"+psdskmdcps+"'"
			+",'"+psdstiendcps+"'"
			+",to_date('"+psngay_tt+"','DD/MM/YYYY')"
			+","+psloaitien_id
			+","+psluottt
			+",'"+psthangdc+"'"
			+",'"+pspsghichu+"'"
			+",'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	
	
	public String value_gachno_khoanmuc(	
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psDsKhoanmuc,
		String psDsTienTraDK,
		String psDsTienTraPS,
		String psChucNangGachNo,
		String psNgay_TT,
		String psHTTT_ID,
		String psLoaiTien_ID,
		String psNganHang_ID,
		String psChungTu,
		String psNgayNH,
		String psLuotTT
	)
	{
		return "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.gachno_khoanmuc('"
			+psChukyno+"'"
			+",'"+psMaKH+"'"
			+",'"+psMaTB+"'"
			+",'"+psDsKhoanmuc+"'"
			+",'"+psDsTienTraDK+"'"
			+",'"+psDsTienTraPS+"'"
			+","+psChucNangGachNo
			+",to_date('"+psNgay_TT+"','DD/MM/YYYY')"
			+","+psHTTT_ID
			+","+psLoaiTien_ID
			+","+psNganHang_ID
			+",'"+psChungTu+"'"
			+",to_date('"+psNgayNH+"','DD/MM/YYYY')"
			+","+psLuotTT
			+",'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
	}
	public String doc_luotthanhtoan(
		String psChukyno,
		String psNgaythu,		
		String psLuotthu_ID,
		String psDieukien
	)
	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/frmTraCuuLuotThanhToan_ajax"
  			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
  			+ "&" + CesarCode.encode("luotthu_id") + "=" + psLuotthu_ID
  			+ "&" + CesarCode.encode("ngaythu") + "=" + psNgaythu
  			+ "&" + CesarCode.encode("dieukien") + "=" + psDieukien;
  	}
  	public String value_xoaphieu_tt(
  		String psChukyno,
  		String psDsRid,
  		String psKieuhuy,
  		String psSys_UserIp
  	)
  	{
  		return "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.xoaphieu_thanhtoan('"+psChukyno+"','"+psDsRid+"'"
			+",'"+psKieuhuy+"','"+psSys_UserIp+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
  	}
  	public String rec_tracuu_phieu_tt(
  		String psChukyno,
  		String psLuot_ID,
  		String psNgayThu,
  		String psDieuKien,
  		String psMa_KH,
  		String psMa_KH1,
  		String psMa_KH2
  	)
  	{
  		return "SELECT count(*) tongso_phieu,trim(to_char(sum(sotien),'999,999,999,999')) tongso_tien"
  			+" FROM "+getUserVar("sys_dataschema")+"chitietphieu_"+psChukyno+" a"
 			+" WHERE TO_CHAR (ngaythu, 'DD/MM/YYYY') = '"+psNgayThu+"'"
   			+" AND sotien <> 0 AND luotthu_id = "+psLuot_ID+" AND "+psDieuKien+" "+psMa_KH+" "+psMa_KH1+" "+psMa_KH2;
  	}
  	public String value_apply_khuyenmai(
  		String psChukyno,
  		String psDsMaKH,
  		String psChucNangGachNo,
  		String psNgayTT,  		
  		String psHTTT_ID,  		
  		String psNgayNH,
  		String psLoaiTienID,  		
  		String psHinhThucKM_ID
  	)
  	{
  		return "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.gachno_khuyenmai('"
			+psChukyno+"'"			
			+",'"+psDsMaKH+"'"
			+","+psChucNangGachNo
			+",to_date('"+psNgayTT+"','DD/MM/YYYY')"
			+","+psHTTT_ID
			+",to_date('"+psNgayNH+"','DD/MM/YYYY')"
			+","+psLoaiTienID
			+","+psHinhThucKM_ID				
			+",'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";		
  	}
	public String doc_ds_ckn_kh(
		String psMaKH,
		String psChukyno
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_lay_dschukyno_theo_kh"
			+ "&" + CesarCode.encode("ma_kh") + "=" + psMaKH
			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno;
	}
}