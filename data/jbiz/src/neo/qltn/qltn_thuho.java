package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class qltn_thuho extends NEOProcessEx 

{
  public void run() 
  {
    System.out.println("neo.qltn.qltn_tienmat was called");
  }
  
    public String kiemtra_thuho
							( String funcschema
							 ,String  ma_tinh
    						 ,String  ma_tb			        
                             ,String psschema
							 ,String psuserID							   
							  ) 
	{  
 	
 	  String  s=  "begin ?:= "+funcschema+"qltn_tracuu.kiemtra_thuho("
 	   
		+"'"+psschema+"'"
		+",'"+psuserID+"'"
		+",'"+ma_tinh+"'"
		+",'"+ma_tb+"'"
		+");"
        +"end;"
        ;
		   return s;	
	}
	
   
   
    public String laytt_thuebao(String func_schema, String data_schema  , String user_id,
							String somay,String ma_tinh) {
    return "begin ?:="+func_schema+"qltn_tracuu.laytt_thuebao_thuho("
			+"'"+data_schema+"'" 
			+",'"+user_id+"','" 
			+somay+"','"+ma_tinh+"'); end;";
  }
   
  public String tracuu_thuho1(
		String schema,
		String chukyno,
		String ma_khachhang,
		String ma_thuebao,
		String ma_sophieu,
		String thutungay,
		String thudenngay,
		String ma_tinh
		
		)
		{	
		System.out.println("neo.xln was called-Updated 16/07/2007");    
		
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_thuho/ajax_tracuuthuho1"
				+"&"+CesarCode.encode("chukyno")+"="+chukyno
				+"&"+CesarCode.encode("ma_kh")+"="+ma_khachhang
				+"&"+CesarCode.encode("ma_tb")+"="+ma_thuebao
				+"&"+CesarCode.encode("ma_phieu")+"="+ma_sophieu
				+"&"+CesarCode.encode("tungay")+"="+thutungay
				+"&"+CesarCode.encode("denngay")+"="+thudenngay
				+"&"+CesarCode.encode("ma_tinh")+"="+ma_tinh
				
		; 	
		}

 
 
 	public String tracuu_thuho(
		String schema,
		String chukyno,
		String ma_khachhang,
		String ma_thuebao,
		String ma_sophieu,
		String thutungay,
		String thudenngay,
		String ma_tinh,
		
		String buucuc,
		String ma_quay,
		String ma_thungan
		)
		{	
		System.out.println("neo.xln was called-Updated 16/07/2007");    
		
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_thuho/ajax_tracuuthuho"
				+"&"+CesarCode.encode("chukyno")+"="+chukyno
				+"&"+CesarCode.encode("ma_kh")+"="+ma_khachhang
				+"&"+CesarCode.encode("ma_tb")+"="+ma_thuebao
				+"&"+CesarCode.encode("ma_phieu")+"="+ma_sophieu
				+"&"+CesarCode.encode("tungay")+"="+thutungay
				+"&"+CesarCode.encode("denngay")+"="+thudenngay
				+"&"+CesarCode.encode("ma_tinh")+"="+ma_tinh
				
				+"&"+CesarCode.encode("buucuc")+"="+buucuc
				+"&"+CesarCode.encode("ma_quay")+"="+ma_quay
				+"&"+CesarCode.encode("ma_thungan")+"="+ma_thungan
		; 	
		}

 
 
  	public String danhsach_thuhotinhkhac2(
		String schema,
		String chukyno,
		String ma_tinh
		
		)
		{	
			System.out.println("thu ngniem: "+getSysConst("FuncSchema")+getSysConst("DataSchema"));
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_thuho/ajax_thuhotinhkhac"
				+"&"+CesarCode.encode("chukyno")+"="+chukyno
				+"&"+CesarCode.encode("ma_tinh")+"="+ma_tinh
				; 	
		}

		
		public String danhsach_thuhotinhkhac(
		String schema,
		String chukyno,
		String ma_tb
		)
		{	
			System.out.println("thu ngniem: "+getSysConst("FuncSchema")+getSysConst("DataSchema"));
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_thuho/ajax_thuhotinhkhac"
				+"&"+CesarCode.encode("chukyno")+"="+chukyno+"&"+CesarCode.encode("ma_tb")+"="+ma_tb
				; 	
		}
		
		
	public String danhsach_phieuhuy(
		String schema,
		String chukyno
		)
		{	
			
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_thuho/ajax_phieuhuy"
				+"&"+CesarCode.encode("chukyno")+"="+chukyno
				; 	
		}	
	
public String themmoi_thuhotinh(     String funcSchema 
								    ,String pschukyno 
								    ,String psht_thanhtoan_id 
								    ,String psloaitien_id 
								    ,String psthungan_id 
								    ,String psten_khachhang 
									,String psma_thuebao
								    ,String psdiachi 
								    ,String pitinh_id 
								    ,String sery	
								    ,String psmaso_thue
								    ,String pisotien 
								    ,String psma_quay 
								    ,String psghichu 
									,String psSchema
								    ,String psUserID 
								                    
							  ) 
							  
							  
							  
							  
							  
 { 
 String s =  "begin ?:= "+funcSchema+"qltn_tracuu.themmoi_phieuthuho_tinhkhac("
        +"'"+pschukyno+"'"
		+","+psht_thanhtoan_id 
		+","+psloaitien_id
        +",'"+psthungan_id+"'"
        +",'"+psten_khachhang+"'"
		+",'"+psma_thuebao+"'"
        +",'"+psdiachi+"'"
        +","+pitinh_id
        +","+sery
		+",'"+psmaso_thue+"'"
		+","+pisotien
		+",'"+psma_quay+"'"
        +",'"+psghichu+"'"
        +",'"+psSchema+"'"
        +",'"+psUserID+"'"
	    +"); "  
        +" end;" ;
		
    return s;

 }	
	
	
	
public String suadoi_phieuthuhotinh( String funcSchema 
									,String pschukyno 
									,String psht_thanhtoan_id 
								    ,String psloaitien_id 
								    ,String psthungan_id 
								    ,String psten_khachhang 
									,String psma_thuebao
								    ,String psdiachi 
								    ,String pitinh_id 
								    ,String psmaso_thue
								    ,String pisotien 
								    ,String psphieu_id
								    ,String psghichu 
									,String psSchema
								    ,String psUserID 
								                    
							  ) 
										  
							  
							  
							  
 { 
 String s =  "begin ?:= "+funcSchema+"qltn_tracuu.capnhat_phieuthuho_tinhkhac("
        +"'"+pschukyno+"'"
		+","+psht_thanhtoan_id 
		+","+psloaitien_id
		+","+pisotien
        +",'"+psthungan_id+"'"
		+",'"+psma_thuebao+"'"
        +",'"+psten_khachhang+"'"
		+",'"+psdiachi+"'"
        +","+pitinh_id
		+",'"+psmaso_thue+"'"
		+",'"+psphieu_id+"'"
        +",'"+psghichu+"'"
        +",'"+psSchema+"'"
        +",'"+psUserID+"'"
	    +"); "  
        +" end;" ;
		
    return s;

 }	
/*	
  public String xoaphieu_thuho
							( String  pschukyno ,
							  String  psphieu_id  ,
							  String  psht_thanhtoan_id ,
							  String  psloaitien_id ,
							  String  psthungan_id ,
							  String  psten_khachhang ,
							  String  psma_thuebao ,
							  String  psdiachi ,
							  String  pitinh_id ,
							  String  psmaso_thue ,
							  String  pisotien ,
							  String  psma_quay ,
							  String  pilanin ,
							  String  psnguoigach ,                   
							  String  psgachtumay ,                   
							  String  psngay_tt ,                    
							  String  psngay_thuc ,
							  String  psghichu  ,                      
							  String  pskyhoadon  ,                     
							  String  psnguoixoa  ,                    
							  String  psmayxoa   ,                    
							  String psngayxoa ,
							  String psSchema ,
							  String  psUserID 						   
														  ) 
	{  
 	
 	  String  s=  "begin ?:= "+funcschema+"qltn_tracuu.capnhat_phieuhuy("
							 +"'"+pschukyno+"'"
							  +","+psphieu_id
							  +","+psht_thanhtoan_id
							  +","+psloaitien_id
							  +",'"+psthungan_id+"'"
							  +",'"+psten_khachhang+"'"
							  +",'"+psma_thuebao+"'"
							  +",'"+psdiachi+"'"
							  +","+pitinh_id
							  +",'"+psmaso_thue+"'"
							  +","+pisotien
							  +",'"+psma_quay+"'"
							  +","+pilanin
							  +",'"+psnguoigach+"'"
							  +",'"+psgachtumay +"'"                  
							  +",'"+psngay_tt +"'"                   
							  +",'"+psngay_thuc +"'"
							  +",'"+psghichu  +"'"                   
							  +",'"+pskyhoadon  +"'"                    
							  +",'"+psnguoixoa  +"'"                   
							  +",'"+psmayxoa   +"'"                    
							  +",'"+psngayxoa +"'"
							  +",'"+psSchema +"'"
							  +",'"+psUserID +"'"	
							  +");"
						      +"end;"  ;
		   return s;	
	}
	*/
	
	 public String xoaphieu_thuho
							( String funcschema
							 ,String  pschukyno
    						 ,String psphieu_id						        
                              ,String psschema
							,String psuserID							   
							  ) 
	{  
 	
 	  String  s=  "begin ?:= "+funcschema+"qltn_tracuu.xoaphieu_thuhotinh("
 	    +"'"+pschukyno+"'"
		+",'"+psphieu_id+"'"
		+",'"+psschema+"'"
		+",'"+psuserID+"'"
		+");"
        +"end;"
        ;
		   return s;	
	}
	
	
	
	public String dsphieuhuy(
		String schema,
		String ma_khachhang,
		String ma_thuebao,
		String ma_sophieu,
		String thutungay,
		String thudenngay,
		String ma_tinh,
		String buucuc,
		String ma_quay,
		String ma_thungan,
		String trangthai_id
		)
		{	
		System.out.println("neo.xln was called-Updated 16/07/2007");    
		
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_thuho/ajax_phieuhuy"
				+"&"+CesarCode.encode("ma_kh")+"="+ma_khachhang
				+"&"+CesarCode.encode("ma_tb")+"="+ma_thuebao
				+"&"+CesarCode.encode("ma_phieu")+"="+ma_sophieu
				+"&"+CesarCode.encode("tungay")+"="+thutungay
				+"&"+CesarCode.encode("denngay")+"="+thudenngay
				+"&"+CesarCode.encode("ma_tinh")+"="+ma_tinh
				+"&"+CesarCode.encode("buucuc")+"="+buucuc
				+"&"+CesarCode.encode("ma_quay")+"="+ma_quay
				+"&"+CesarCode.encode("ma_thungan")+"="+ma_thungan
				+"&"+CesarCode.encode("trangthai_id")+"="+trangthai_id
				; 	
		}
		
		
	
	
	
	
	
}
