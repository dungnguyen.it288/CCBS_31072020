package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import java.sql.*;

public class qltn_bctk extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("QLTN Gach no");
		//funcschema=getSysConst("FuncSchema");
    	//dataschema=getUserVar("sys_dataschema");
    	//userid=getUserVar("userID");		
	}
	public String doc_tentruong(
		String psTenBang
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/bctk/ajax_ds_tentruong"
  			+ "&" + CesarCode.encode("ten_bang") + "=" + psTenBang;
	}	
	public String rec_metadata(String sql)
	{
		String s="";		
		try
		{
			ResultSetMetaData rset=this.reqRSet(sql).getMetaData();
			s="select ";
			for (int i=1;i<=rset.getColumnCount();i++)
			{
				s+="'"+rset.getColumnName(i)+"' "+rset.getColumnName(i)+",";
			}
			s=s.substring(0,s.length()-1)+" from dual";
		} catch (Exception sqlex)
		{
			System.out.println(sqlex.getMessage());
		}
		System.out.println(s);
		return s;
	}
	public NEOCommonData doc_metadata(String sql)
	{		
		NEOExecInfo nei_= new NEOExecInfo(sql);
		NEOCommonData ncd_ = new NEOCommonData(nei_);
		return ncd_;
	}
}