package neo.qltn;

import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class qltn_cauhinh extends NEOProcessEx {

    public void run() {
        System.out.println("neo.qltn.qltn_cauhinh was called");
    }	
	public String taocauhinh(
        String psngaydauthang,
        String pstienmomay,	
        String psthoigianquet,
        String psthoigianquetlai,
        String psngaykhoacuoc,
        String psthuebao3thang,
		String psmomay
	)
	{
		String return_="begin ?:=admin_v2.cauhinh_kmm.cauhinh_kmm("
			 + "'" + psngaydauthang + "'" 
			 + ",'" + pstienmomay+ "'" 
    		 + ",'" + psthoigianquet + "'"
    		 + ",'" + psthoigianquetlai + "'"
			 + ",'" + psngaykhoacuoc + "'"
			 + ",'" + psthuebao3thang + "'"
			 + ",'" + psmomay + "'"			 
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	System.out.println(return_);
    	return return_;
	}
	
	public String ls_cauhinh_kmm(
		String matinh
	)
	{   return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/cauhinh/ajax_tracuu" 
        	+ "&" + CesarCode.encode("matinh") + "=" + matinh;
    }
	
	public String doc_khoamay_nocuoc_all
	(
		String ckn_so,
		String donvi,
		String tientu,
		String tienden,
		String loaikh,
		String soluong,
		String chieukhoa,
		String khdb,
		String khunc,
		String khmotam
	)
	{
	return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/nocuoc/ajax_thuebao_all" 
                    + "&" + CesarCode.encode("ckn_so") + "=" +ckn_so 
	 				+ "&" + CesarCode.encode("donvi") + "=" + donvi
                    + "&" + CesarCode.encode("tientu") + "=" + tientu
                    + "&" + CesarCode.encode("tienden") + "=" + tienden
                    + "&" + CesarCode.encode("loaikh_id") + "=" + loaikh
                    + "&" + CesarCode.encode("sobanghi") + "=" + soluong
	    			+ "&" + CesarCode.encode("chieukhoa") + "=" + chieukhoa
	    			+ "&" + CesarCode.encode("khdb") + "=" + khdb
	    			+ "&" + CesarCode.encode("khunc") + "=" + khunc
	    			+ "&" + CesarCode.encode("khmotam") + "=" + khmotam;
	}
	///NEW_SPS
	public String value_khoamay_nocuoc_all
	(
		String ckn_so,
		String donvi,	
		String tientu,
		String tienden,
		String loaikh,
		String soluong,
		String chieukhoa,
		String khdb,
		String khunc,
		String khmotam
	)
	{
		String return_="begin ?:=admin_v2.pkg_api_hlr.khoamay_nocuoc_all("
    		 + "'" + ckn_so + "'" 
			 + ",'" + donvi+ "'" 
    		 + ",'" + tientu + "'"
    		 + ",'" + tienden + "'"
    		 + ",'" + loaikh + "'"
    		 + ",'" + khdb + "'"
    		 + ",'" + khunc + "'"
    		 + ",'" + khmotam + "'"
	         + ",'" + soluong + "'"
		     + ",'" + chieukhoa + "'"
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	System.out.println(return_);
    	return return_;
	}
	
	/*public String value_khoamay_nocuoc_all
	(
		String ckn_so,
		String donvi,	
		String tientu,
		String tienden,
		String loaikh,
		String soluong,
		String chieukhoa,
		String khdb,
		String khunc,
		String khmotam
	)
	{
		String return_="begin ?:=admin_v2.cauhinh_kmm.khoamay_nocuoc_all("
    		 + "'" + ckn_so + "'" 
			 + ",'" + donvi+ "'" 
    		 + ",'" + tientu + "'"
    		 + ",'" + tienden + "'"
    		 + ",'" + loaikh + "'"
    		 + ",'" + khdb + "'"
    		 + ",'" + khunc + "'"
    		 + ",'" + khmotam + "'"
	         + ",'" + soluong + "'"
		     + ",'" + chieukhoa + "'"
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	System.out.println(return_);
    	return return_;
	}*/
	
	public String doc_layds_ls_khoamay_all
	(
		String ngaykhoa,
		String donvi,	
		String chieukhoa
	)
	{
		return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/nocuoc/tinhtrang/ajax_ls_km_all" 
            + "&" + CesarCode.encode("ngaykhoa") + "=" +ngaykhoa 
            + "&" + CesarCode.encode("donvi") + "=" +donvi 
            + "&" + CesarCode.encode("chieukhoa") + "=" + chieukhoa;
	}
	

}