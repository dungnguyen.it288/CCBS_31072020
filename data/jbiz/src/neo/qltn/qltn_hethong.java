package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;
import javax.sql.RowSet;

public class qltn_hethong extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("QLTN He thong");
	}
	public String doc_layds_thuho(
		String pskyhoadon,
		String psphieu_id,
		String psloaitien_id,
		String pshttt_id,
		String psma_kh,
		String psma_tb,
		String pstenkh,
		String psdiachi,
		String psms_thue,
		String pssotien,
		String psngaythu,
		String pstrangthai_id,
		String pstinh_id,
		String psghichu,
		String psLoai,
		String pspage_num,
		String pspage_rec		
	)		
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_thuho/ajax_dsphieu"
			+"&"+CesarCode.encode("kyhoadon")+"="+pskyhoadon
			+"&"+CesarCode.encode("phieu_id")+"="+psphieu_id
			+"&"+CesarCode.encode("loaitien_id")+"="+psloaitien_id
			+"&"+CesarCode.encode("httt_id")+"="+pshttt_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("tenkh")+"="+pstenkh
			+"&"+CesarCode.encode("diachi")+"="+psdiachi
			+"&"+CesarCode.encode("ms_thue")+"="+psms_thue
			+"&"+CesarCode.encode("sotien")+"="+pssotien
			+"&"+CesarCode.encode("ngaythu")+"="+psngaythu
			+"&"+CesarCode.encode("trangthai_id")+"="+pstrangthai_id
			+"&"+CesarCode.encode("tinh_id")+"="+pstinh_id
			+"&"+CesarCode.encode("ghichu")+"="+psghichu
			+"&"+CesarCode.encode("loai")+"="+psLoai
			+"&"+CesarCode.encode("page_num")+"="+pspage_num
			+"&"+CesarCode.encode("page_rec")+"="+pspage_rec;				
	}
	public String value_capnhat_phieuthuho(
		String pskyhoadon,
		String psPhieu_id,
		String psTientra
	){		
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.capnhat_trangthai_thuho('"
			+pskyhoadon+"','"
			+psPhieu_id+"','"
			+psTientra+"','"			
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_capnhatquay(
		String psquaythu,
		String pstc_gachno,
		String pstuychon
	){		
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn.capnhat_quaythu('"
			+psquaythu+"','"
			+pstc_gachno+"','"
			+pstuychon+"','"			
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String doc_nhanvien(String psma_bc)		
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_hethong/phanquyen/ajax_nhanvien"
			+"&"+CesarCode.encode("ma_bc")+"="+psma_bc;
	}
	public String doc_quaythu(String psma_bc)		
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_hethong/phanquyen/ajax_quaythu"
			+"&"+CesarCode.encode("ma_bc")+"="+psma_bc;
	}
	public String doc_chukyno()		
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_hethong/phanquyen/ajax_chukyno";
	}
	public String capnhat_hinhthuctt(
    	String psDsHTTT,
    	String psDsUser){
    		String users=psDsUser.replace(",","','");
    		String out_ = "declare out_ varchar2(1000); begin begin "
    			+ "	update "+getUserVar("sys_dataschema")+"nguoigachnos set "
    			+ "	HINHTHUCTT_IDS='"+psDsHTTT+",0' where nguoigach in ('"
    			+ users +"'); out_ := qltn.log_quyengn('"+psDsHTTT+"','"+psDsUser+"','','','"+						
    				getUserVar("sys_dataschema")+"'); "
    			+ " exception when others then "
    			+ " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "
    			+ " end; ?:=out_; end;";
    		return out_;    		
    }
    public String capnhat_quyengnxp(
    	String psgachno_pre,
    	String psxoaphieu_pre,
    	String pscapquanly,
    	String psgn_luingay,
    	String psxp_luingay,
    	String psDsUser){
    		String users=psDsUser.replace(",","','");
    		String out_ = "declare out_ varchar2(1000); begin begin "
    			+ "	update "+getUserVar("sys_dataschema")+"nguoigachnos set "
    			+ "	gn_luingay='"+psgn_luingay+"',capquanly='"+pscapquanly+"',"
    			+ " xp_luingay='"+psxp_luingay+"' where nguoigach in ('"
    			+ users +"'); "
    			+ " update "+getUserVar("sys_dataschema")+"users set "
    			+ " gachno_pre="+psgachno_pre+",xoaphieu_pre="+psxoaphieu_pre+" where name in ('"
    			+ users + "'); out_ := qltn.log_quyengn('','"+psDsUser+"','"+psxoaphieu_pre+"','"+psgachno_pre+"','"+
    			  getUserVar("sys_dataschema")+"-"+getUserVar("userID")+"'); "
    			+ " exception when others then "
    			+ " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "
    			+ " end; ?:=out_; end;";
    		return out_;
    }
    public String capnhat_thanggn(
		String psdsthang,
		String psdsuser,
		String psuser_ip
	){		
		String s = "begin ?:="+getSysConst("FuncSchema")+"qtht.capnhat_quyengachno_theothang('"
			+psdsthang+"','"
			+psdsuser+"','"
			+"0"+"','"
			+"00"+"','"
			+"0"+"','"
			+"00"+"','"
			+getUserVar("userID")+"','"
			+psuser_ip+"','"
			+getUserVar("sys_dataschema")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String capnhat_quays(
		String psdsuser,
		String psdsquay
	){		
		String s = "declare out_ varchar2(1000); "
				  +"begin "
				  +"	begin"
				  +"	update "+getUserVar("sys_dataschema")+"nguoigachnos set quaythus='"+psdsquay+"' where nguoigach in ('"+psdsuser.replaceAll(",","','")+"');"				  
				  +"	out_:='1';commit;"
				  +"	exception when others then out_:='0'; end;"
				  +" ?:=out_;"
				  +"end;";
		System.out.println(s);
		return s;
	}
	public String capnhat_mauhoadon(
		String psdsuser,
		String psvat,
		String psprinter,
		String psvt01path
	){		
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_baocao.capnhat_mauhoadon("
				+"'"+psdsuser+"',"
				+"'"+psvat+"',"
				+"'"+psprinter+"',"
				+"'"+psvt01path+"',"
				+"'"+getUserVar("sys_dataschema")+"',"
				+"'"+getUserVar("userID")+"'); end;";
		/*s = "begin ? := ccs_admin.qltn_baocao.capnhat_mauhoadon("
				+"'ductt_neo',"
				+"'"+psvat+"',"
				+"'"+psprinter+"',"
				+"'"+psvt01path+"',"
				+"'CCS_HCM.',"
				+"'ductt_neo'); end;";
		String s="begin ?:= 'aaaa'; end;";*/
		System.out.println(s);
		return s;
	}
	public String laytt_mauhoadon(
		String psuserid
	){
		String s = "select vt01_printer,vt01_path, vt01 from "
			  +			getUserVar("sys_dataschema")+"nguoigachnos "
			  +"		where nguoigach = '"+psuserid+"'";
		return s;
	}
	public RowSet getOc4jInstanceName(){		
		String oc4j_name	=	System.getProperty("oracle.oc4j.instancename");
		RowSet rows_ = null;
		try{		
			rows_	=	reqRSet("select '"+oc4j_name+"' oc4j_name from dual");
		}catch (Exception ex){}
		return rows_;
	}
	
	public String capnhat_ngaycuoc(
		String psngaycocuoc,
		String psngaykhoacuoc,
		String psuserip
	){		
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_baocao.capnhat_ngaycuoc("
				+"'"+psngaycocuoc+"',"
				+"'"+psngaykhoacuoc+"',"
				+"'"+psuserip+"',"				
				+"'"+getUserVar("sys_dataschema")+"',"
				+"'"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
}