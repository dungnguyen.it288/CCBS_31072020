package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class qltn_thanhtoan extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("QLTN Gach no");
		//funcschema=getSysConst("FuncSchema");
    	//dataschema=getUserVar("sys_dataschema");
    	//userid=getUserVar("userID");		
	}
	public String value_thanhtoan4(
		String pskyhoadon,
		String psdsma_kh,
		String psdsma_tb,
		String psdschukyno,
		String psdstientra,
		String psdskieutra,
		String psdskhoanmuc,
		String psdskmck,
		String pichucnang_gachno,
		String pdngay_tt,
		String pihttt_id,
		String piloaitien_id,
		String pinganhang_id,
		String pschungtu,
		String pdngaynganhang,
		String psgomhd,
		String psluotthanhtoan,
		String pstheokhoanmuc
	)
	{
		String s = "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.thanhtoan4("+
			"'"+pskyhoadon+"',"+
			"'"+psdsma_kh+"',"+
			"'"+psdsma_tb+"',"+
			"'"+psdschukyno+"',"+
			"'"+psdstientra+"',"+
			"'"+psdskieutra+"',"+
			"'"+psdskhoanmuc+"',"+
			"'"+psdskmck+"',"+
			"'"+pichucnang_gachno+"',"+
			"to_date('"+pdngay_tt+"','DD/MM/YYYY'),"+
			"'"+pihttt_id+"',"+
			"'"+piloaitien_id+"',"+
			""+pinganhang_id+","+
			"'"+pschungtu+"',"+
			"to_date('"+pdngaynganhang+"','DD/MM/YYYY'),"+
			"'"+psgomhd+"',"+
			"'"+psluotthanhtoan+"',"+
			"'"+pstheokhoanmuc+"',"+
			"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}




	public String doc_print_baono(String dsphieu)
	{
  		String out_="/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_baono/baono"
				+ "&" + CesarCode.encode("dsphieu") + "=" +  dsphieu;
  		System.out.println(out_);
  		return out_;
  	}


	public String doc_nochitiet1(
		String psMaKH,
		String psMaTB,		
		String psKyhoadon,
		String psLoaiTienID,
		String psTheokhoanmuc
	)
	{
  		String out_="/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_taiquay/ajax_laytt_nochitiet"
  			+ "&" + CesarCode.encode("ma_khs") + "=" + psMaKH
  			+ "&" + CesarCode.encode("ma_tbs") + "=" + psMaTB
  			+ "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
  			+ "&" + CesarCode.encode("loaitien_ids") + "=" + psLoaiTienID
  			+ "&" + CesarCode.encode("theokhoanmuc") + "=" + psTheokhoanmuc;
  		System.out.println(out_);
  		return out_;
  	}
	

	public String value_thanhtoan7(
		String pskyhoadon,
		String psdsphieu,
		String pdngay_tt,
		String pinganhang_id,
		String pschungtu,
		String pdngaynganhang,
		String psgomhd,
		String psluotthanhtoan
	)
	{
		String s = "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.thanhtoan7("+
			"'"+pskyhoadon+"',"+
			"'"+psdsphieu+"',"+
			"'"+pdngay_tt+"',"+
			"'"+pinganhang_id+"',"+
			"'"+pschungtu+"',"+
			"'"+pdngaynganhang+"',"+
			"'"+psgomhd+"',"+
			"'"+psluotthanhtoan+"',"+
			"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String doc_nochitiet(
		String psMaKH,
		String psMaTB,		
		String psKyhoadon,
		String psLoaiTienID,
		String psTheokhoanmuc
	)
	{
  		String out_="/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tienmat/ajax_laytt_nochitiet"
  			+ "&" + CesarCode.encode("ma_khs") + "=" + psMaKH
  			+ "&" + CesarCode.encode("ma_tbs") + "=" + psMaTB
  			+ "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
  			+ "&" + CesarCode.encode("loaitien_ids") + "=" + psLoaiTienID
  			+ "&" + CesarCode.encode("theokhoanmuc") + "=" + psTheokhoanmuc;
  		System.out.println(out_);
  		return out_;
  	}
	public String doc_nodieuchinh(
		String psMaKH,
		String psMaTB,		
		String psKyhoadon,
		String psLoaiTienID
	)
	{
  		String out_="/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_dieuchinh/ajax_laytt_nochitiet"
  			+ "&" + CesarCode.encode("ma_khs") + "=" + psMaKH
  			+ "&" + CesarCode.encode("ma_tbs") + "=" + psMaTB
  			+ "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
  			+ "&" + CesarCode.encode("loaitien_ids") + "=" + psLoaiTienID;
  		System.out.println(out_);
  		return out_;
  	}
  	public String doc_layds_giaophieu(  		
		String psChukyno,
		String psMaNV,
		String psMaT,
		String psDsTrangthai,
		String psKyhoadon,
		String psPageNum,
		String psPageRec
  	)
  	{  		
  		String config_="ccbs_qltn/qltn_giaohoadon/ajax_layds_giaophieu";  				
		return priv_doc_layds_giaophieu("0",psChukyno,psMaNV,psMaT,"",psDsTrangthai,psKyhoadon,psPageNum,psPageRec,config_);
  	}
  	public String doc_layds_giaophieu_gachno(  		
		String psLuotgiao,
		String psChukyno,
		String psMaNV,
		String psMaT,
		String psNgayGiao,
		String psDsTrangthai,
		String psKyhoadon,
		String psPageNum,
		String psPageRec
  	)
  	{  		
  		String config_="ccbs_qltn/qltn_thudaily/ajax_layds_giaophieu_theo_lg_de_gno";  				
		return priv_doc_layds_giaophieu(psLuotgiao,psChukyno,psMaNV,psMaT,psNgayGiao,psDsTrangthai,psKyhoadon,psPageNum,psPageRec,config_);
  	}
  	public String priv_doc_layds_giaophieu(
		String psLuotgiao,
		String psChukyno,
		String psMaNV,
		String psMaT,
		String psNgayGiao,
		String psDsTrangthai,
		String psKyhoadon,
		String psPageNum,
		String psPageRec,
		String psConfigFile
	)		
	{
		
		return "/main?" + CesarCode.encode("configFile")+"="+psConfigFile
  			+ "&" + CesarCode.encode("luotgiao_id") + "=" + psLuotgiao
  			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
  			+ "&" + CesarCode.encode("ma_nv") + "=" + psMaNV
  			+ "&" + CesarCode.encode("ma_t") + "=" + psMaT
  			+ "&" + CesarCode.encode("ngaygiao") + "=" + psNgayGiao
  			+ "&" + CesarCode.encode("trangthai") + "=" + psDsTrangthai
  			+ "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	/*public String rec_laytt_giaophieu(
		String psma_nv,
   		String psma_t,
   		String psluotgiao_id,
   		String psngaygiao
	){
		String out_="begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.laytt_giaophieu("
			+"'"+psma_nv+"',"
			+"'"+psma_t+"',"
			+"'"+psluotgiao_id+"',"
			+"'"+psngaygiao+"',"			
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		System.out.println(out_);
		return out_;
	}*/
	public String rec_laytt_giaophieu(
		String psma_nv,
   		String psma_t,
   		String psluotgiao_id,
   		String psngaygiao,
		String pschukyno
	){
		String out_="begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.laytt_giaophieu("
			+"'"+psma_nv+"',"
			+"'"+psma_t+"',"
			+"'"+psluotgiao_id+"',"
			+"'"+psngaygiao+"',"
			+"'"+pschukyno+"',"
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		System.out.println(out_);
		return out_;
	}
	public String gachno_thutainha(
  		String pskyhoadon,
		String psdsma_kh,		
		String psdstientra,
		String psdskmck,
		
		String psdshttt,
		String psdssoct,
		String psdsckn,
		
		String psngay_tt,
		String psloaitien,
		String psma_nv,
		String pschucnang_gachno,
		String piluotthanhtoan	
	)
	{
		String a= "begin ? := "+getSysConst("FuncSchema")+"ninhbd_thtoan.thanhtoan3("
			+"'"+pskyhoadon+"',"
			+"'"+psdsma_kh+"',"
			+"'"+psdstientra+"',"	
			+"'"+psdskmck+"',"	
			
			+"'"+psdshttt+"',"	
			+"'"+psdssoct+"',"	
			+"'"+psdsckn+"',"	
				
			+"'"+psngay_tt+"',"
			+"'"+psloaitien+"',"
			+"'"+psma_nv+"',"
			+"'"+piluotthanhtoan+"',"
			+"'"+pschucnang_gachno+"',"			
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
	}






	public String gachno_chuyenkhoan(
  		String pskyhoadon,
		String psdsma_kh,		
		String psdstientra,
		String psdskmck,
		
		String psdshttt,
		String psdssoct,
		String psdsckn,
		
		String psngay_tt,
		String psloaitien,
		String psma_nv,
		String pschucnang_gachno,
		String piluotthanhtoan	
	)
	{
		String a= "begin ? := "+getSysConst("FuncSchema")+"qltn_hni.thanhtoan3("
			+"'"+pskyhoadon+"',"
			+"'"+psdsma_kh+"',"
			+"'"+psdstientra+"',"	
			+"'"+psdskmck+"',"	
			
			+"'"+psdshttt+"',"	
			+"'"+psdssoct+"',"	
			+"'"+psdsckn+"',"	
				
			+"'"+psngay_tt+"',"
			+"'"+psloaitien+"',"
			+"'"+psma_nv+"',"
			+"'"+piluotthanhtoan+"',"
			+"'"+pschucnang_gachno+"',"			
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
	}






	public String gachno_chuyenkhoan_file(
  		String pskyhoadon,
		String psdsma_kh,		
		String psdstientra,
		String psdskmck,
		
		String psdshttt,
		String psdssoct,
		String psdsckn,
		
		String psngay_tt,
		String psloaitien,
		String psma_nv,
		String pschucnang_gachno,
		String piluotthanhtoan	
	)
	{
		String a= "begin ? := "+getSysConst("FuncSchema")+"qltn_hni.thanhtoan6("
			+"'"+pskyhoadon+"',"
			+"'"+psdsma_kh+"',"
			+"'"+psdstientra+"',"	
			+"'"+psdskmck+"',"	
			
			+"'"+psdshttt+"',"	
			+"'"+psdssoct+"',"	
			+"'"+psdsckn+"',"	
			+"'"+psngay_tt+"',"
			+"'"+psloaitien+"',"
			+"'"+psma_nv+"',"
			+"'"+piluotthanhtoan+"',"
			+"'"+pschucnang_gachno+"',"			
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
	}






	public String value_xoaphieu(
		String pskyhoadon,
		String psdsphieu_id,
		String psdschukyno,
		String pskieuhuy
	)
	{
		String a= "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.xoaphieu("
			+"'"+pskyhoadon+"',"
			+"'"+psdsphieu_id+"',"
			+"'"+psdschukyno+"',"	
			+"'"+pskieuhuy+"',"			
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
	}
	public String value_dieuchinh(
		String pskyhoadon,
		String psdsma_kh,
		String psdsma_tb,
		String psdschukyno,
		String psdstragoc,
		String psdstrathue,
		String psdskieutra,
		String psdskhoanmuc,
		String psdsdonviql_id,
		String pdngay_tt,
		String pihttt_id,
		String piloaitien_id,
		String pslydo
	)
	{
		String a= "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.thanhtoan5("
			+"'"+pskyhoadon+"',"
			+"'"+psdsma_kh+"',"
			+"'"+psdsma_tb+"',"	
			+"'"+psdschukyno+"',"		
			+"'"+psdstragoc+"',"
			+"'"+psdstrathue+"',"
			+"'"+psdskieutra+"',"	
			+"'"+psdskhoanmuc+"',"
			+"'1',"
			+"'"+psdsdonviql_id+"',"
			+"to_date('"+pdngay_tt+"','DD/MM/YYYY'),"
			+"'"+pihttt_id+"',"	
			+"'"+piloaitien_id+"',"	
			+"'"+pslydo+"',"
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
	}
	public String value_xoaphieu_dc(
		String pskyhoadon,
		String psdsphieu_id,
		String psdschukyno,
		String pskieuhuy
	)
	{
		
		//begin ?:= admin_v2.qltn_donno.Chuyenno_Ihd_Tapchung
		String a= "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.xoaphieu_dc("
		//String a= "begin ? := admin_v2.qltn_thtoan.xoaphieu_dc("
			+"'"+pskyhoadon+"',"
			+"'"+psdsphieu_id+"',"
			+"'"+psdschukyno+"',"	
			+"'"+pskieuhuy+"',"			
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
	}
	public String doc_printvat(
		String pschukyno,
		String psphieu_id,		
		String psgomhd
	)
	{
  		String out_="/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tienmat/invat/hoadon"
  			+ "&" + CesarCode.encode("chukyno") + "=" + pschukyno
  			+ "&" + CesarCode.encode("phieu_id") + "=" + psphieu_id
  			+ "&" + CesarCode.encode("gomhd") + "=" + psgomhd;
  		System.out.println(out_);
  		return out_;
  	}
	
	
	
	
		public String doc_printvat_unc(
		String pschukyno,
		String psphieu_id,		
		String psgomhd
	)
	{
  		String out_="/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tienmat/invat/hoadon_unc"
  			+ "&" + CesarCode.encode("chukyno") + "=" + pschukyno
  			+ "&" + CesarCode.encode("phieu_id") + "=" + psphieu_id
  			+ "&" + CesarCode.encode("gomhd") + "=" + psgomhd;
  		System.out.println(out_);
  		return out_;
  	}
	
	
		public String doc_printvat_unc1(
		String pschukyno,
		String psphieu_id,		
		String psgomhd
	)
	{
  		String out_="/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tienmat/invat/hoadon_unc1"
  			+ "&" + CesarCode.encode("chukyno") + "=" + pschukyno
  			+ "&" + CesarCode.encode("phieu_id") + "=" + psphieu_id
  			+ "&" + CesarCode.encode("gomhd") + "=" + psgomhd;
  		System.out.println(out_);
  		return out_;
  	}
	
	
  	public String doc_printvat1(
		String pschukyno,
		String psphieu_id,		
		String psgomhd
	) 
	{
  		String out_="/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tienmat/invat/hoadon"
  			+ "&" + CesarCode.encode("chukyno") + "=" + pschukyno
  			+ "&" + CesarCode.encode("phieu_id") + "=" + CesarCode.decode(psphieu_id)
  			+ "&" + CesarCode.encode("gomhd") + "=" + psgomhd;
  		System.out.println(out_);
  		return out_;
  	}
	public String doc_print_phieuthu(
  		String psConfigFile,
		String pschukyno,
		String psphieu_id,
		String psloai
	)
	{
  		String out_="/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tienmat/invat/"+CesarCode.decode(psConfigFile)
  			+ "&" + CesarCode.encode("chukyno") + "=" + pschukyno
  			+ "&" + CesarCode.encode("phieu_id") + "=" + psphieu_id
  			+ "&" + CesarCode.encode("loai") + "=" + psloai;
  		System.out.println(out_);
  		return out_;
  	}
  	public String doc_printvat_cn(
		String pschukyno,
		String psphieu_id
	)
	{
  		String out_="/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tienmat/invat/hoadon_cn"
  			+ "&" + CesarCode.encode("chukyno") + "=" + pschukyno
  			+ "&" + CesarCode.encode("phieu_id") + "=" + psphieu_id;
  		System.out.println(out_);
  		return out_;
  	}
	
	public String value_check_hddt(
		String psdsma_kh,
		String pskyhoadon,
		String psdsphieu_id
	)
	{
		String s = "begin ? :=admin_v2.qltn_tracuu_new.check_hddt_hni("+
			"'"+psdsma_kh+"',"+
			"'"+pskyhoadon+"',"+
			"'"+psdsphieu_id+"',"+
			"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}

	public String value_check_hddt1(
		String psdsma_kh,
		String pskyhoadon,
		String psdsphieu_id
	)
	{
		String s = "begin ? :=admin_v2.qltn_tracuu_new.check_hddt_hni("+
			"'"+psdsma_kh+"',"+
			"'"+pskyhoadon+"',"+
			"'"+CesarCode.decode(psdsphieu_id)+"',"+
			"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}		
}