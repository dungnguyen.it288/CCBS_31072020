package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class qltn_chuyentuyen extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("QLTN Chuyen tuyen");
	}
	public String layds_kh_chuyentuyen(
		String psChukyno,
		String psMaNV,
		String psMaT,
		String psPhia,
		String pspage_num,
		String pspage_rec
	)
	{
  		return "/main?" + CesarCode.encode("configFile") +
	          "=ccbs_qltn/qltn_chuyentuyen/ajax_laydskh_tuyen"
	          + "&" + CesarCode.encode("chukyno") + "=" + psChukyno
	          + "&" + CesarCode.encode("ma_nv") + "=" + psMaNV
	          + "&" + CesarCode.encode("ma_t") + "=" + psMaT
			  + "&" + CesarCode.encode("phia") + "=" + psPhia
			  + "&" + CesarCode.encode("page_num") + "=" + pspage_num
			  + "&" + CesarCode.encode("page_rec") + "=" + pspage_rec
	          ;
	}
	
	
	public String layds_tuyenthu_chuyentuyen(
		String psMaNV,
		String psPhia
	)
	{
  		String str="/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_chuyentuyen/ajax_tthu_chuyentuyen"
  			+ "&" + CesarCode.encode("manv") + "=" + psMaNV
			+ "&" + CesarCode.encode("phia") + "=" + psPhia;
		System.out.println(str);
  		return str;
	}
	
	public String value_thuchien_chuyentuyen(
		String psChukyno,
		String psdsmakh,
		String psmanv_moi,
		String psmat_moi,
		String psmanv_cu,
		String psmat_cu,
		String pikieuchuyen,
		String pimanvall,
		String pichuyentuyenall,
		String pdngaychuyen
	)
	{
		String s = "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.chuyentuyen('"+psdsmakh+"','"+psmanv_cu+"','"
																			+psmat_cu+"','"+psmanv_moi+"','"
																			+psmat_moi+"',"+pikieuchuyen+","+pimanvall+","
																			+pichuyentuyenall+",'"+psChukyno+"','"
																			+pdngaychuyen+"','"
																			+getUserVar("sys_dataschema")+"','"
																			+getUserVar("userID")+"'); end;";
		return s;
  	}
		public String tracuu_chuyentuyen
	(
		String 	pstungay,
		String 	psdenngay,
		String 	psKyhoadon,
		String	psma_kh,
		String	psma_tb,
		String	psma_nv_cu,
		String	psma_nv_moi,
		String	psma_t_cu,
		String	psma_t_moi
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_chuyentuyen/tracuu/ajax_ds_khachhangs"
			+"&"+CesarCode.encode("kyhoadon")+"="+psKyhoadon
			+"&"+CesarCode.encode("tungay")+"="+pstungay
			+"&"+CesarCode.encode("denngay")+"="+psdenngay
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("ma_nv_cu")+"="+psma_nv_cu
			+"&"+CesarCode.encode("ma_nv_moi")+"="+psma_nv_moi
			+"&"+CesarCode.encode("ma_t_cu")+"="+psma_tb
			+"&"+CesarCode.encode("ma_t_moi")+"="+psma_tb
			+"&"+CesarCode.encode("page_num")+"="+1
			+"&"+CesarCode.encode("page_rec")+"="+1000;
	}

	
}