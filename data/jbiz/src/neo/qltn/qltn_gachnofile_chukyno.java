package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class qltn_gachnofile_chukyno extends NEOProcessEx
{
	public void run()
	{
		System.out.println("QLTN Gach no");
		//funcschema=getSysConst("FuncSchema");
    	//dataschema=getUserVar("sys_dataschema");
    	//userid=getUserVar("userID");
	}
	public String doc_tienno0(
                   String psKyhoadon,
                   String psLantra
        )
        {
                return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_gachnofile_new/ajax_tienno0"
                        + "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
                        + "&" + CesarCode.encode("lantra") + "=" + psLantra;
        }
	public String value_retiendu(
        String psKyhoadon,
        String psLantra,
        String psMa_khs,
        String psMa_tbs,
        String psDonvis,
        String psTientra,
        String psNgayTTs,
        String psHttts,
		String psChukynos,
		String psMa_nvs,
		String psMa_ts,
		String psChungtus,
		String nganhangid,
		String gomhoadon,
        String psNguoigachs
	)
	{
		String return_="begin ? := admin_V2.qltn_tracuu.REUPLOAD_GACHNOFILE_CHUKYNO_du("
			+"'"+psKyhoadon+"',"
			+"'"+psLantra+"',"
			+"'"+psMa_khs+"',"
			+"'"+psMa_tbs+"',"
			+"'"+psTientra+"',"
			+"'"+psHttts+"',"
			+"'"+psChukynos+"',"
			+"'"+psDonvis+"',"
			+"'"+psNgayTTs+"',"
			+"'"+psMa_nvs+"',"
			+"'"+psMa_ts+"',"
			+"'"+psChungtus+"',"
			+"'"+nganhangid+"',"
			+"'"+gomhoadon+"',"
			+"'"+psNguoigachs+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(return_);
		return return_;
	}
	public String doc_hople(
                   String psKyhoadon,
                   String psLantra
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_gachnofile_new/ajax_hople"
  			+ "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
                        + "&" + CesarCode.encode("lantra") + "=" + psLantra;
	}
    public String doc_khonghople(
                   String psKyhoadon,
                   String psLantra
        )
        {
                return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_gachnofile_new/ajax_khonghople"
                        + "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
                        + "&" + CesarCode.encode("lantra") + "=" + psLantra;
        }
    public String doc_nhaplai(
                   String psKyhoadon,
                   String psLantra
        )
        {
                return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_gachnofile_new/ajax_nhaplai"
                        + "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
                        + "&" + CesarCode.encode("lantra") + "=" + psLantra;
        }
	public String rec_tonghop(
                   String psKyhoadon,
                   String psLantra
        )
	{
	 	String a= "select to_char(sum(decode(invalid,null,sotien,0)),"+getSysConst("FuncSchema")+
                         "qltn.lay_fm(1)) tongtien,count(*)-count(invalid) hople,count(invalid) khonghople,"+
                         "count(decode(invalid,1,invalid,null)) c1,"+
                         "count(decode(invalid,2,invalid,null)) c2,"+
                         "count(decode(invalid,3,invalid,null)) c3,"+
                         "count(decode(invalid,4,invalid,null)) c4,"+
                         "count(decode(invalid,5,invalid,null)) c5,"+
                         "count(decode(invalid,6,invalid,null)) c6,"+
                         "count(decode(invalid,7,invalid,null)) c7,"+
                         "count(decode(invalid,8,invalid,null)) c8, "+	
						 "count(decode(invalid,9,invalid,null)) c9, "+	
						 "count(decode(invalid,10,invalid,null)) c10, "+						 
						 "count(decode(invalid,11,invalid,null)) c11,"+
						 "count(decode(invalid,12,invalid,null)) c12,"+
						 "count(decode(invalid,13,invalid,null)) c13,"+
						 "count(decode(invalid,14,invalid,null)) c14"+
                         " from "+ getUserVar("sys_dataschema")+"ct_tra_file_chukyno_"+psKyhoadon+" where lantra_id="+psLantra;
        System.out.println(a);
               return a;
	}
	public String value_huyUpload(
		String psKyhoadon,
		String psLantra)
	{
		String return_= "declare re_ varchar2(30000); n number; begin begin \n"
					   +"   select count(*) into n from "+getUserVar("sys_dataschema")+"ct_tra_file_chukyno_"+psKyhoadon
					   +"         where phieu_id is not null and lantra_id="+psLantra+"; \n"
					   +"   if n>0 then re_:='Lan Upload da co phieu gach no vao he thong'; \n "
					   +"   else "
					   +"	 delete from "+getUserVar("sys_dataschema")+"ct_tra_file_chukyno_"+psKyhoadon
					   +"        where lantra_id="+psLantra+";"
					   +"   re_:=1; end if; "
					   +" exception when others then "
					   +"   re_:='huyUpload|Error: '||TO_CHAR(SQLCODE)||': '||SQLERRM;"
					   +" end; ? := re_; end;";
	    System.out.println(return_);
		return return_;
	}
	public String value_reUpload(
        String psKyhoadon,
        String psLantra,
        String psMa_khs,
        String psMa_tbs,
        String psDonvis,
        String psTientra,
        String psNgayTTs,
        String psHttts,
		String psChukynos,
		String psMa_nvs,
		String psMa_ts,
		String psChungtus,
        String psNguoigachs
	)
	{
		String return_="begin ? := admin_v2.qltn_tracuu.reupload_gachnofile_chukyno("
			+"'"+psKyhoadon+"',"
			+"'"+psLantra+"',"
			+"'"+psMa_khs+"',"
			+"'"+psMa_tbs+"',"
			+"'"+psTientra+"',"
			+"'"+psHttts+"',"
			+"'"+psChukynos+"',"
			+"'"+psDonvis+"',"
			+"'"+psNgayTTs+"',"
			+"'"+psMa_nvs+"',"
			+"'"+psMa_ts+"',"
			+"'"+psChungtus+"',"
			+"'"+psNguoigachs+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(return_);
		return return_;
	}
	public String value_gachnofile(
		String psKyhoadon,
		String psLantra,
		String psMomay
	)
	{
		String return_="begin ? := admin_v2.qltn_thtoan_new.thanhtoan8("
			+"'"+psKyhoadon+"',"
			+"'"+psLantra+"',"
			+"'"+psMomay+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(return_);
		return return_;
	}
	public String value_status(
		String psKyhoadon,
		String psLantra
	)
	{
		String return_="begin ? := "+getSysConst("FuncSchema")+"qltn_thtoan.laytt_gachnofile_chukyno("
			+"'"+psKyhoadon+"',"
			+"'"+psLantra+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(return_);
		return return_;
	}

	public String value_capnhatseri(String psKyhoadon,String lantra)
	{
			return "begin ?:= admin_v2.qltn_thtoan_new.capnhatseri_file_chukyno("
			+"'"+psKyhoadon+"'"
			+",'"+lantra+"'"
	 	  	+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	
  	}		
}
