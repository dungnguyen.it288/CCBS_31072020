package neo.qltn;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class subscriber_lock_open extends NEOProcessEx {
	public String new_customer_special_kind(
		String pschema,
		String pcustomer_specail_name,
		String pstatus,
		String pDateLock,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_subscriber_lock_open.new_customer_special_kind("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(pcustomer_specail_name)+"',"
				+"'"+pstatus+"',"
				+"'"+pDateLock+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_customer_special_kind(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_subscriber_lock_open.del_customer_special_kind("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_customer_special_kind(
	String pschema,
		String pcustomer_specail_id,
		String pcustomer_specail_name,
		String pstatus,
		String pDateLock,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_subscriber_lock_open.edit_customer_special_kind("
				+"'"+pschema+"',"
				+"'"+pcustomer_specail_id+"',"
				+"'"+ConvertFont.UtoD(pcustomer_specail_name)+"',"
				+"'"+pstatus+"',"
				+"'"+pDateLock+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String rec_upload_customer_special(String psUploadID){
	String s  ="begin ?:= admin_v2.pkg_subscriber_lock_open.upload_cus_spe_info('"+psUploadID+"'); end;";
	 System.out.println(s); 	
	 return s;
	}
	public String value_accept_customer_special(String psUploadID, String use_schema, String com_schema, String psuserip, int isOverride){				
		String s=  "begin ?:= admin_v2.pkg_subscriber_lock_open.accept_upload_cus_spe("
			+"'"+ psUploadID+"',"
			+isOverride+","
			+"'"+use_schema+ "'," 
			+"'"+com_schema+ "'," 			
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";
			
	     return  s;
	}
	public String upload_customer_special_error(String psLanUpload, String psFuncSchema) {
    return "/main?"+CesarCode.encode("configFile")+"=ccbs_qltn/qltn_kmm/khdb/upload_cus_spe/ajax_khonghople"
        +"&"+CesarCode.encode("upload_id")+"="+psLanUpload
        ;
  }
  public String check_lock_time(String pschieu)
  {
			String s=  "begin ?:= admin_v2.pkg_subscriber_lock_open.fcheck_lock_hand_time("
			+"'"+ pschieu+"'," 			
			+"'"+getUserVar("sys_agentcode")+"'); end;";
			
	     return  s;
  }
	public String get_list_auto_lock(String psDirec) throws Exception{				
		String s=  "begin ?:= admin_v2.pkg_subscriber_lock_open.khoamay_nocuoc_taods("
			+"'"+ psDirec+"'); end;";			
	     System.out.println(s);
		return this.reqValue("", s);
	}
}