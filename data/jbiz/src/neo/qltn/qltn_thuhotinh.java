package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class qltn_thuhotinh extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("QLTN Gach no");
	}	
	public String rec_notonghop_thuho(
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psLoaiTienID,
		String psSum
	)
	{
		String s = "declare schema_ varchar2(100); "
			+"begin schema_:="+getSysConst("FuncSchema")+"qltn_tracuu.lay_agent_parameter('"+psMaTB+"','schema'); "
			+"	"+getSysConst("FuncSchema")+"qltn_tamthu.lichsu_tracuu_thuho('"+psMaTB+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"','"+getSysConst("FuncSchema")+"'); "
			+"?:= "+getSysConst("FuncSchema")+"qltn_tracuu.laytt_notonghop('"
			+psChukyno+"','"
			+psMaKH+"','"
			+psMaTB+"','"
			+psLoaiTienID+"','"
			+psSum+"',"
			+"schema_,'"
			+getUserVar("userID")+"'); end;";			
		System.out.println(s);
		return s;
  	}
	public String doc_notonghop_thuho(
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psLoaiTienID,
		String psSum
	)
	{
		String schema_="";
		try{
			schema_ = reqValue("begin ? :="+getSysConst("FuncSchema")+"qltn_tracuu.lay_agent_parameter('"+psMaTB+"','schema'); end;");
		} catch (Exception ex){
			System.out.println(ex.getMessage());
		}
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tienmat/ajax_laytt_notonghop_thuho"
			+"&"+CesarCode.encode("chukyno")+"="+psChukyno
			+"&"+CesarCode.encode("userinput")+"="+psMaKH
			+"&"+CesarCode.encode("ma_tb")+"="+psMaTB
			+"&"+CesarCode.encode("loaitien")+"="+psLoaiTienID
			+"&"+CesarCode.encode("schema")+"="+schema_;
  	}
  	public String rec_laytt_cuocnong_thuho(
		String psMaKH,
		String psMaTB,
		String psChukyno
	)
	{		
		String s = "declare schema_ varchar2(100);"+
						" smatinh_ varchar2(200); "
			+"begin smatinh_:="+getSysConst("FuncSchema")+"qltn_tracuu.lay_agent_parameter('"+psMaTB+"','short_name'); "
			+"		schema_:="+getSysConst("FuncSchema")+"qltn_tracuu.lay_agent_parameter('"+psMaTB+"','schema'); "
			+" ?:="+getSysConst("FuncSchema")+"qltn_tracuu.laytt_cuocnong('"
			+psMaKH+"','"
			+psMaTB+"','"
			+psChukyno+"',schema_,smatinh_,'"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;		
	}
	public String value_themmoi_thuho(
		String pskyhoadon,
		String psloaitien_id,
		String pshttt_id,
		String psma_kh,
		String psma_tb,
		String pstenkh,
		String psdiachi,
		String psms_thue,
		String pssotien,
		String pschukyno,
		String psma_tn,
		String psmaquay,
		String pstinh_id,
		String pstinhthu_id,
		String psngaythu,
		String psghichu
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_tamthu.themmoi_thuho('"
			+pskyhoadon+"','"
			+psloaitien_id+"','"
			+pshttt_id+"','"
			+psma_kh+"','"
			+psma_tb+"','"
			+pstenkh+"','"
			+psdiachi+"','"
			+psms_thue+"','"
			+pssotien+"','"
			+pschukyno+"','"
			+psma_tn+"','"
			+psmaquay+"','"
			+pstinh_id+"','"
			+pstinhthu_id+"',to_date('"
			+psngaythu+"','DD/MM/YYYY'),'"
			+psghichu+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}	
	public String doc_layds_thuho(
		String pskyhoadon,
		String psphieu_id,
		String psloaitien_id,
		String pshttt_id,
		String psma_kh,
		String psma_tb,
		String pstenkh,
		String psdiachi,
		String psms_thue,
		String pssotien,
		String psngaythu,
		String pstrangthai_id,
		String pstinh_id,
		String psghichu,
		String psLoai,
		String pspage_num,
		String pspage_rec		
	)		
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_thuho/ajax_dsphieu"
			+"&"+CesarCode.encode("kyhoadon")+"="+pskyhoadon
			+"&"+CesarCode.encode("phieu_id")+"="+psphieu_id
			+"&"+CesarCode.encode("loaitien_id")+"="+psloaitien_id
			+"&"+CesarCode.encode("httt_id")+"="+pshttt_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("tenkh")+"="+pstenkh
			+"&"+CesarCode.encode("diachi")+"="+psdiachi
			+"&"+CesarCode.encode("ms_thue")+"="+psms_thue
			+"&"+CesarCode.encode("sotien")+"="+pssotien
			+"&"+CesarCode.encode("ngaythu")+"="+psngaythu
			+"&"+CesarCode.encode("trangthai_id")+"="+pstrangthai_id
			+"&"+CesarCode.encode("tinh_id")+"="+pstinh_id
			+"&"+CesarCode.encode("ghichu")+"="+psghichu
			+"&"+CesarCode.encode("loai")+"="+psLoai
			+"&"+CesarCode.encode("page_num")+"="+pspage_num
			+"&"+CesarCode.encode("page_rec")+"="+pspage_rec;				
	}
	public String value_capnhat_phieuthuho(
		String pskyhoadon,
		String psPhieu_id,
		String psTientra
	){		
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.capnhat_trangthai_thuho('"
			+pskyhoadon+"','"
			+psPhieu_id+"','"
			+psTientra+"','"			
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_capnhat_phieuchuyenkhoan(
		String pskyhoadon,
		String psPhieu_id,
		String psTientra
	){		
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.capnhat_trangthai_chuyenkhoan('"
			+pskyhoadon+"','"
			+psPhieu_id+"','"
			+psTientra+"','"			
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_agent_id(String psSomay)
	{
		return "begin ? := "+getSysConst("FuncSchema")+
			"qltn_tracuu.lay_agent_parameter('"+psSomay+"','agent_id'); end;";
	}
	public String value_xoa_thuho(
		String pskyhoadon,
		String psdsphieu_id		
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_tamthu.xoa_thuho('"
			+pskyhoadon+"','"			
			+psdsphieu_id+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		//System.out.println(s);
		return s;
	}
	public String value_laytt_thangno(
		String pskyhoadon,
		String psma_kh,
		String psma_tb
	){		
		String s = "declare schema_ varchar2(100); "
			+"begin schema_:="+getSysConst("FuncSchema")+"qltn_tracuu.lay_agent_parameter('"+psma_tb+"','schema'); "
			+"	"+getSysConst("FuncSchema")+"qltn_tamthu.lichsu_tracuu_thuho('"+psma_tb+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"','"+getSysConst("FuncSchema")+"'); "
			+"?:= "+getSysConst("FuncSchema")+"qltn_xln.laytt_thangno('"
			+pskyhoadon+"','"
			+psma_kh+"','"
			+psma_tb+"',"			
			+"schema_,'"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String rec_laytt_thangno(
		String pskyhoadon,
		String psma_kh,
		String psma_tb
	){		
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_tamthu.laytt_thuho('"
			+pskyhoadon+"','"			
			+psma_kh+"','"
			+psma_tb+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
}