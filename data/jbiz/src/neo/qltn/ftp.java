package neo.qltn;
// FTPupload.java by Rowland http://www.home.comcast.net/~rowland3/
// Upload a file via FTP, using the JDK.

import java.io.*;
import java.net.*;


class ftp {
    public final String host;
    public final String user;
    protected final String password;
    protected URLConnection urlc;

    public ftp(String _host, String _user, String _password) {
  		host= _host;  user= _user;  password= _password;
  		urlc = null;
    }
    protected URL makeURL(String targetfile) throws MalformedURLException  {
  if (user== null)
      return new URL("ftp://"+ host+ "/"+ targetfile+ ";type=i");
  else
      return new URL("ftp://"+ user+ ":"+ password+ "@"+ host+ "/"+ targetfile+ ";type=i");
    }

    protected InputStream openDownloadStream(String targetfile) throws Exception {
  URL url= makeURL(targetfile);
  urlc = url.openConnection();
  InputStream is = urlc.getInputStream();
  return is;
    }

    protected OutputStream openUploadStream(String targetfile) throws Exception {
  URL url= makeURL(targetfile);
  urlc = url.openConnection();
  OutputStream os = urlc.getOutputStream();
  return os;
    }

    protected void close() {
  urlc= null;
    }
}