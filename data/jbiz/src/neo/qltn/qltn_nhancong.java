package neo.qltn;

import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class qltn_nhancong extends NEOProcessEx {

    public void run() {
        System.out.println("neo.qltn.qltn_nhancong was called");
    }

	public String value_momay_thuebao(
			String psChukyno,
			String psDsTB,
			String psChieumo,
			String psKM,
			String psGhichu) {
        String s = "begin ?:= admin_v2.khoamo_nhancong('"+
        	psChukyno+"','"+
			psDsTB+"','"+
			psChieumo+"','"+
			psKM+"','"+
			psGhichu+"','"+
        	getUserVar("sys_dataschema")+"','"+
        	getUserVar("userID") + "'); end;";
        return s;
    }
}