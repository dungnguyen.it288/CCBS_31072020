package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class qltn_gachnofile_nganhang extends NEOProcessEx
{
	public void run()
	{
		System.out.println("QLTN Gach no");
		//funcschema=getSysConst("FuncSchema");
    	//dataschema=getUserVar("sys_dataschema");
    	//userid=getUserVar("userID");
	}
	public String doc_tienno0(
                   String psKyhoadon,
                   String psLantra
        )
        {
                return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_gachnofile1/ajax_tienno0"
                        + "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
                        + "&" + CesarCode.encode("lantra") + "=" + psLantra;
        }
	public String value_retiendu(
        String psKyhoadon,
        String psLantra,
        String psMa_khs,
        String psMa_tbs,       
        String psTientra,
		String psthangnos,
		String socts,
        String psNgayTTs,
		String pshinthuctt,
		String psnganhangs,
		String psnguoigachnos
	)
	{
		String return_="begin ? := "+getSysConst("FuncSchema")+"qltn_tracuu.REUPLOAD_GACHNOFILE_NH_DU("
			+"'"+psKyhoadon+"',"
			+"'"+psLantra+"',"
			+"'"+psMa_khs+"',"
			+"'"+psMa_tbs+"',"			
			+"'"+psTientra+"',"
			+"'"+psthangnos+"',"
			+"'"+socts+"',"
			+"'"+psNgayTTs+"',"
			+"'"+pshinthuctt+"',"
			+"'"+psnganhangs+"',"		
			+"'"+psnguoigachnos+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(return_);
		return return_;
	}
	public String doc_hople(
                   String psKyhoadon,
                   String psLantra
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_gachnofile1/ajax_hople"
  			+ "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
                        + "&" + CesarCode.encode("lantra") + "=" + psLantra;
	}
    public String doc_khonghople(
                   String psKyhoadon,
                   String psLantra
        )
        {
                return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_gachnofile1/ajax_khonghople"
                        + "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
                        + "&" + CesarCode.encode("lantra") + "=" + psLantra;
        }
    public String doc_nhaplai(
                   String psKyhoadon,
                   String psLantra
        )
        {
                return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_gachnofile1/ajax_nhaplai"
                        + "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
                        + "&" + CesarCode.encode("lantra") + "=" + psLantra;
        }
	public String rec_tonghop(
                   String psKyhoadon,
                   String psLantra
        )
	{
	          String a= "select to_char(sum(decode(invalid,null,nogoc,0)),"+getSysConst("FuncSchema")+
                         "qltn.lay_fm(1)) tongtien,count(*)-count(invalid) hople,count(invalid) khonghople,"+
                         "count(decode(invalid,1,invalid,null)) c1,"+
                         "count(decode(invalid,2,invalid,null)) c2,"+
                         "count(decode(invalid,3,invalid,null)) c3,"+
                         "count(decode(invalid,4,invalid,null)) c4,"+
                         "count(decode(invalid,5,invalid,null)) c5,"+
                         "count(decode(invalid,6,invalid,null)) c6,"+
                         "count(decode(invalid,7,invalid,null)) c7,"+
                         "count(decode(invalid,8,invalid,null)) c8 "+
                         " from "+ getUserVar("sys_dataschema")+"chuyenkhoans_file_"+psKyhoadon+" where lanupload="+psLantra;
        System.out.println(a);
               return a;
	}


	public String rec_tonghop_kqphat(
                   String psKyhoadon,
                   String psLantra
        )
	{
	          String a= "select to_char(sum(decode(invalid,null,nogoc,0)),"+getSysConst("FuncSchema")+
                         "qltn.lay_fm(1)) tongtien,count(*)-count(invalid) hople,count(invalid) khonghople,"+
                         "count(decode(invalid,1,invalid,null)) c1,"+
                         "count(decode(invalid,2,invalid,null)) c2,"+
                         "count(decode(invalid,3,invalid,null)) c3,"+
                         "count(decode(invalid,4,invalid,null)) c4,"+
                         "count(decode(invalid,5,invalid,null)) c5,"+
                         "count(decode(invalid,6,invalid,null)) c6,"+
                         "count(decode(invalid,7,invalid,null)) c7,"+
                         "count(decode(invalid,8,invalid,null)) c8 "+
                         " from "+ getUserVar("sys_dataschema")+"chuyenkhoans_file_"+psKyhoadon+" where lanupload="+psLantra;
        System.out.println(a);
               return a;
	}




	public String value_huyUpload(
		String psKyhoadon,
		String psLantra)
	{
		String return_= "declare re_ varchar2(30000); n number; begin begin \n"
					   +"   select count(*) into n from "+getUserVar("sys_dataschema")+"chuyenkhoans_file_"+psKyhoadon
					   +"         where phieu_id is not null and lanupload="+psLantra+"; \n"
					   +"   if n>0 then re_:='Lan Upload da co phieu gach no vao he thong'; \n "
					   +"   else "
					   +"	 delete from "+getUserVar("sys_dataschema")+"chuyenkhoans_file_"+psKyhoadon
					   +"        where lanupload="+psLantra+";"
					   +"   re_:=1; end if; "
					   +" exception when others then "
					   +"   re_:='huyUpload|Error: '||TO_CHAR(SQLCODE)||': '||SQLERRM;"
					   +" end; ? := re_; end;";
	    System.out.println(return_);
		return return_;
	}
	public String value_reUpload(
        String psKyhoadon,
        String psLantra,
        String psMa_khs,
        String psMa_tbs,
        String psDonvis,
        String psTientra,
        String psNgayTTs,
        String psHttts,
        String psNguoigachs
	)
	{
		String return_="begin ? := "+getSysConst("FuncSchema")+"qltn_tracuu.reupload_gachnofile_chuyenkhoan("
			+"'"+psKyhoadon+"',"
			+"'"+psLantra+"',"
			+"'"+psMa_khs+"',"
			+"'"+psMa_tbs+"',"
			+"'"+psDonvis+"',"
			+"'"+psTientra+"',"
			+"'"+psNgayTTs+"',"
			+"'"+psHttts+"',"
			+"'"+psNguoigachs+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(return_);
		return return_;
	}


	
	public String Value_xacnhanDL(
		String psKyhoadon,
		String psLantra

	)
	{
	String return_="begin ? := "+getSysConst("FuncSchema")+"qltn_tracuu.xacnhan_gachnofile_nganhang("
			+"'"+psKyhoadon+"',"
			+"'"+psLantra+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(return_);
	return return_;

	}
	public String value_gachnofile_chuyenkhoan(
		String psKyhoadon,
		String psLantra,
		String psMomay,
		String psNgay_tt
	)
	{
		String return_="begin ? := ccs_admin.gachno_chuyenkhoan_theofile("
			+"'"+psKyhoadon+"',"
			+"'"+psLantra+"',"
			+"'"+psMomay+"',"
			+"'"+psNgay_tt+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(return_);
		return return_;
	}
	public String value_status(
		String psKyhoadon,
		String psLantra
	)
	{
		String return_="begin ? := "+getSysConst("FuncSchema")+"qltn_thtoan.laytt_gachnofile_chuyenkhoan("
			+"'"+psKyhoadon+"',"
			+"'"+psLantra+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(return_);
		return return_;
	}
}
