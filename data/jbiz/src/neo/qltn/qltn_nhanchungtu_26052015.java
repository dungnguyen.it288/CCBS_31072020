package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class qltn_nhanchungtu extends NEOProcessEx 

{
  	public void run() 
  	{
    	System.out.println("neo.qltn.qltn_tienmat was called");
    	//	getSysConst("FuncSchema");
    	//	getUserVar("sys_dataschema");
    	//	getUserVar("userID");
  	}
	public String doc_ds_chungtu(
	   String psKyhoadon,
       String psma_tb,
       String psnoidung,
       String psso_ct,
       String pssobienlai,
       String pssotk_dvh,
       String pssotk_dvph,
       String pstrangthai,
       String psngaynhan,
       String psloaict_id,
       String psma_kh,
       String pspagenum,
       String pspagerec
    )
	{		
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_chuyenkhoan/ajax_ds_chungtu"
			+"&"+CesarCode.encode("kyhoadon")+"="+psKyhoadon
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("noidung")+"="+psnoidung
			+"&"+CesarCode.encode("so_ct")+"="+psso_ct
			+"&"+CesarCode.encode("sobienlai")+"="+pssobienlai
			+"&"+CesarCode.encode("sotk_dvh")+"="+pssotk_dvh
			+"&"+CesarCode.encode("sotk_dvp")+"="+pssotk_dvph
			+"&"+CesarCode.encode("trangthai")+"="+CesarCode.decode(pstrangthai)
			+"&"+CesarCode.encode("ngaynhan")+"="+psngaynhan
			+"&"+CesarCode.encode("loaict_id")+"="+psloaict_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("page_num")+"="+pspagenum
			+"&"+CesarCode.encode("page_rec")+"="+pspagerec; 	
	}
	public String value_themmoi_chuyenkhoan(  
		String 	  pskyhoadon,
		String	  pschukyno,
		String    psso_ct,
		String    pstientruocthue,
		String    pstienthue,
		String	  psma_bc,
		String	  psmatb,
		String	  psghichu,
		String    psloaitien_id,
		String    pstennh_phat,
		String    psdonvi_huong,
		String    psdiachi_dvh,
		String    pssotk_dvh,
		String    psnganhang_dvh,
		String    psdonvi_phat,
		String    psdiachi_dvp,
		String    pssotk_dvp,
		String    pitrangthai,
		String    psngaygui,
		String    psma_kh,
		String 	  psloai_chungtu
	) 						  
 	{ 
 		String s = "begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.themmoi_chuyenkhoan("
	        +"'"+pskyhoadon+"'"
	        +",'"+pschukyno+"'"
			+",'"+psso_ct+"'"
	        +",'"+pstientruocthue.replace(",","")+"'"
			+",'"+pstienthue.replace(",","")+"'"
			+",'"+psma_bc+"'"
			+",'"+psmatb+"'"
			+",'"+psghichu+"'"
	        +","+psloaitien_id
			+","+pstennh_phat
	        +",'"+psdonvi_huong+"'"
	        +",'"+psdiachi_dvh+"'"
	        +",'"+pssotk_dvh+"'"
			+","+psnganhang_dvh
			+",'"+psdonvi_phat+"'"
			+",'"+psdiachi_dvp+"'"
	        +",'"+pssotk_dvp+"'"
	       	+",'"+pitrangthai+"'"
			+",'"+psngaygui+"'"
			+",'"+psma_kh+"'"
		    +","+psloai_chungtu
		    +",'"+getUserVar("sys_dataschema")+"'"
	        +",'"+getUserVar("userID")+"'"
		    +"); end;";
		System.out.println(s);
		return s;
 	}
	public String ReportLog_time
	( 
		
		String psreport_title,
		String psreport_file
	)
	{
		String s = "begin ?:= admin_v2.chart_report.reportlog_time("
	 	    +"'"+psreport_title+"'"
	 	    +",'"+psreport_file+"'"
			+",'"+getUserVar("userID")+"'"
	 		+",'"+getUserVar("sys_dataschema")+"'"
			
			+");"
	        +"end;";
			System.out.println(s);
		return s;
	}
	
	public String value_thongbaoinbiennhan(String psKyhoadon,String phieu_id)
	{
			return "begin ?:= "+getSysConst("FuncSchema")+"qltn_thtoan.kt_biennhan("
			+"'"+psKyhoadon+"'"
			+",'"+phieu_id+"'"
	 	  	+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	
  	}
	public String ReportLog
	( 
		String psgroup_id,
		String psreport_title,
		String psreport_file
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_baocao.ReportLog("
			+"'"+psgroup_id+"',"
	 	    +"'"+psreport_title+"'"
	 	    +",'"+psreport_file+"'"
	 		+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
	
	
	
	public String value_xoa_chuyenkhoan
	( 
		String psKyhoadon,
		String psDsPhieu_ID,
		String psMa_BC,
		String psHinhthucTT
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.xoa_chuyenkhoan("
			+"'"+psKyhoadon+"',"
	 	    +"'"+psDsPhieu_ID+"'"
	 	    +",'"+psMa_BC+"'"
	 	    +",'"+psHinhthucTT+"'"		
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
	public String value_capnhat_chungtu(  
		String 	  pskyhoadon,
		String	  pschukyno,
		String    psso_ct,		
		String    pstientruocthue,
		String    pstienthue,
		String	  psma_bc,
		String	  psmatb,
		String	  psghichu,
		String    psloaitien_id,
		String    pstennh_phat,
		String    psdonvi_huong,
		String    psdiachi_dvh,
		String    pssotk_dvh,
		String    psnganhang_dvh,
		String    psdonvi_phat,
		String    psdiachi_dvp,
		String    pssotk_dvp,
		String    pitrangthai,
		String    psngaygui,
		String    psma_kh,
		String 	  psloai_chungtu,
		String 	  psphieu_id
	) 						  
 	{ 
 		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.capnhat_chungtu("
	        +"'"+pskyhoadon+"'"
	        +",'"+pschukyno+"'"
			+",'"+psso_ct+"'" 			
	        +",'"+pstientruocthue.replace(",","")+"'"
			+",'"+pstienthue.replace(",","")+"'"
			+",'"+psma_bc+"'"
			+",'"+psmatb+"'"
			+",'"+psghichu+"'"
	        +","+psloaitien_id
			+","+pstennh_phat
	        +",'"+psdonvi_huong+"'"
	        +",'"+psdiachi_dvh+"'"
	        +",'"+pssotk_dvh+"'"
			+","+psnganhang_dvh
			+",'"+psdonvi_phat+"'"
			+",'"+psdiachi_dvp+"'"
	        +",'"+pssotk_dvp+"'"
	       	+",'"+pitrangthai+"'"
			+",'"+psngaygui+"'"
			+",'"+psma_kh+"'"
		    +","+psloai_chungtu
		    +","+psphieu_id
		    +",'"+getUserVar("sys_dataschema")+"'"
	        +",'"+getUserVar("userID")+"'"
		    +"); end;";
 	}
  	public String RecTTKhachHang_ChuyenKhoan(String InMaKh)	
	{
		return "begin ? := "+getSysConst("FuncSchema")+"qltn_tracuu.tracuu_thongtin_KH_TB(null,null,null,'"+InMaKh+"',null,null,null,null,null,null,null,'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
	}
	

		public String doc_notonghop1(
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psLoaiTienID,
		String psSum
	)

	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_chuyenkhoan/ajax_laytt_notonghop"
			+"&"+CesarCode.encode("chukyno")+"="+psChukyno
			+"&"+CesarCode.encode("userinput")+"="+psMaKH
			+"&"+CesarCode.encode("ma_tb")+"="+psMaTB
			+"&"+CesarCode.encode("loaitien")+"="+psLoaiTienID;
  	}
	//thiennt 05-07-10
	public String doc_notonghop_tracuu(
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psLoaiTienID,
		String psSum
	)

	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tracuuttkh/notra_new/ajax_laytt_notonghop"
			+"&"+CesarCode.encode("chukyno")+"="+psChukyno
			+"&"+CesarCode.encode("userinput")+"="+psMaKH
			+"&"+CesarCode.encode("ma_tb")+"="+psMaTB
			+"&"+CesarCode.encode("loaitien")+"="+psLoaiTienID;
  	}

  	public String rec_notonghop(
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psLoaiTienID,
		String psSum	
	)
	{
		String s="begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.laytt_notonghop('"
			+psChukyno+"','"
			+psMaKH+"','"
			+psMaTB+"','"
			+psLoaiTienID+"','"
			+psSum+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
  	}
	public String rec_notonghop3(
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psLoaiTienID,
		String psSum
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.priv_laytt_notonghop('"
			+psChukyno+"','"
			+psMaKH+"','"
			+psMaTB+"','"
			+psLoaiTienID+"','"
			+psSum+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"',1); end;";
  	}
	public String doc_notonghop(
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psLoaiTienID,
		String psSum
	)
	{
		try{
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"ajax_laytt_notonghop", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return  "/main?"+CesarCode.encode("configFile")+"=admin/not-access";
			reqValue("begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
						+psMaTB+"','ajax_laytt_notonghop'," 
						+"'"+psMaTB+";',"
						+"'','"
						+getUserVar("userID")+"','"
						+getEnvInfo().getParserParam("sys_userip")+"','"
						+getUserVar("sys_agentcode")+"'); "
						+ "   commit; "
						+ " end; ");
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tienmat/ajax_laytt_notonghop"
			+"&"+CesarCode.encode("chukyno")+"="+psChukyno
			+"&"+CesarCode.encode("userinput")+"="+psMaKH
			+"&"+CesarCode.encode("ma_tb")+"="+psMaTB
			+"&"+CesarCode.encode("loaitien")+"="+psLoaiTienID;
  	}
	
	public String doc_thongbaoinbiennhan_unc(String psKyhoadon,String phieu_ids)

	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_thtoan.kt_biennhan_unc("
			+"'"+psKyhoadon+"'"
			+",'"+phieu_ids+"'"
	 	  	+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
  	}
  	public String doc_khuyenmai(
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psLoaiTienID,
		String psNgayTT,
		String psHTTT	
	)
	{
  		
  		try{
  			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"ajax_khuyenmai", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return  "/main?"+CesarCode.encode("configFile")+"=admin/not-access";
			reqValue("begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
						+psMaTB+"','ajax_khuyenmai'," 
						+"'"+psMaTB+";',"
						+"'','"
						+getUserVar("userID")+"','"
						+getEnvInfo().getParserParam("sys_userip")+"','"
						+getUserVar("sys_agentcode")+"'); "
						+ "   commit; "
						+ " end; ");
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
  		
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tienmat/ajax_khuyenmai"
			+"&"+CesarCode.encode("kyhoadon")+"="+psChukyno
			+"&"+CesarCode.encode("user_input")+"="+psMaKH
			+"&"+CesarCode.encode("ma_tb")+"="+psMaTB
			+"&"+CesarCode.encode("loaitien")+"="+psLoaiTienID			
			+"&"+CesarCode.encode("ngay_tt")+"="+psNgayTT
			+"&"+CesarCode.encode("httt")+"="+psHTTT;
  	}
	
	

  	public String doc_cuocnong()
  	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_nhanchungtu/ajax_tamthu_cuocnong";
  	}
  	public String doc_chuyenkhoan()
  	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_nhanchungtu/ajax_chuyenkhoan";
  	}
	public String doc_tamthu()
  	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_nhanchungtu/ajax_kyquy";
  	}
  	public String value_themmoi_phieuthu(
		String psma_kh           ,
		String psma_tb           ,
		String pstien            ,				
		String psnoidung         ,
		String psso_ct           ,
		String pschukyno         ,
		String psloaitien_id     ,
		String psngaynhap        ,
		String pshinhthuctt		 ,
		String psma_bc           ,
		String psma_tn           ,
		String psmaquay          ,
		String psloai            
	)
	{
		String s="begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.themmoi_phieuthu("
			+"'"+ psma_kh         +"',"
			+"'"+ psma_tb         +"',"			
			+"'"+ pstien          +"',"
			+"'"+ psnoidung       +"',"
			+"'"+ psso_ct         +"',"
			+"'"+ pschukyno       +"',"
			+"'"+ psloaitien_id   +"',"
			+"to_date('"+ psngaynhap      +"','DD/MM/YYYY'),"
			+"'"+ pshinhthuctt    +"',"
			+"'"+ psma_bc         +"',"
			+"'"+ psma_tn         +"',"
			+"'"+ psmaquay        +"',"
			+"'"+ psloai          +"',"  
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String doc_layds_phieuthu(
		String psphieu_id   ,
	    String psma_kh      ,
		String psma_tb      ,
		String psso_ct      ,
		String psngaynhap   ,
		String psloai       ,	
		String pspagenum    ,
		String pspagerec    
	)
	{
		String file="ajax_ds_cuocnong";
		if (psloai=="1") file="ajax_ds_kyquy";
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_nhanchungtu/"+file
			+"&"+CesarCode.encode("phieu_id")+"="+psphieu_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("so_ct")+"="+psso_ct			
			+"&"+CesarCode.encode("ngaynhan")+"="+psngaynhap
			+"&"+CesarCode.encode("loai")+"="+psloai			
			+"&"+CesarCode.encode("page_num")+"="+pspagenum
			+"&"+CesarCode.encode("page_rec")+"="+pspagerec;
	}
	public String value_capnhat_phieuthu(
		String psphieu_id		 ,
		String psma_kh           ,
		String psma_tb           ,
		String pstien            ,				
		String psnoidung         ,
		String psso_ct           ,
		String pschukyno         ,
		String psloaitien_id     ,
		String psngaynhap        ,
		String pshinhthuctt      ,
		String psma_bc           ,
		String psma_tn           ,
		String psmaquay          ,
		String psloai            
	)
	{
		String s="begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.capnhat_phieuthu("
			+"'"+ psphieu_id      +"',"
			+"'"+ psma_kh         +"',"
			+"'"+ psma_tb         +"',"			
			+"'"+ pstien          +"',"
			+"'"+ psnoidung       +"',"
			+"'"+ psso_ct         +"',"
			+"'"+ pschukyno       +"',"
			+"'"+ psloaitien_id   +"',"
			+"to_date('"+ psngaynhap      +"','DD/MM/YYYY'),"
			+"'"+ pshinhthuctt    +"',"
			+"'"+ psma_bc         +"',"
			+"'"+ psma_tn         +"',"
			+"'"+ psmaquay        +"',"
			+"'"+ psloai          +"',"  
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}

	public String value_kthinhthuc
	(
		String hinhthuc,
		String chukyno
	)
	{
		return "begin ?:="+getSysConst("FuncSchema")+"qltn_tamthu.kiemtra_hinhthuc('"+hinhthuc+"','"+chukyno+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";	
	}
	


	public String value_kiemtra_tienam
	(
		String hinhthuc,
		String tongtien
	)
	{
		return "begin ?:="+getSysConst("FuncSchema")+"qltn_tamthu.kiemtra_tienam('"+hinhthuc+"','"+tongtien+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";	
	}
	


	public String value_xoa_cuocnong
	( 
		String psKyhoadon,
		String psDsPhieu_ID,
		String psMa_BC
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.xoa_cuocnong("			
		    +"'"+psKyhoadon+"'"
	 	    +",'"+psDsPhieu_ID+"'"
	 	    +",'"+psMa_BC+"'"
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
	public String value_xoa_phieuchi
	( 
		String psKyhoadon,
		String psDsPhieu_ID,
		String psMa_BC,
		String psLoai
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.xoa_phieuchi("			
		    +"'"+psKyhoadon+"'"
	 	    +",'"+psDsPhieu_ID+"'"
	 	    +",'"+psMa_BC+"'"
	 	    +",'"+psLoai+"'"		
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
	public String value_muctien_kyquy(String psDichvuKQ_ID)		
	{
		return "begin select sotien into ? from "
			+getUserVar("sys_dataschema")
			+"dichvu_kqs where dichvukq_id="+psDichvuKQ_ID
			+"; end;";
	}
	public String value_themmoi_phieuchi(
		String psma_kh           ,
		String psma_tb           ,
		String psphieuchi		 ,
		String pstien            ,				
		String psnoidung         ,
		String psso_ct           ,
		String pschukyno         ,
		String psloaitien_id     ,
		String psngaynhap        ,
		String pshinhthuctt		 ,
		String psma_bc           ,
		String psma_tn           ,
		String psmaquay          ,
		String psloai            
	)
	{
		String s="begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.themmoi_phieuchi("
			+"'"+ psma_kh         +"',"
			+"'"+ psma_tb         +"',"			
			+"'"+ psphieuchi      +"',"
			+"'"+ pstien          +"',"
			+"'"+ psnoidung       +"',"
			+"'"+ psso_ct         +"',"
			+"'"+ pschukyno       +"',"
			+"'"+ psloaitien_id   +"',"
			+"to_date('"+ psngaynhap      +"','DD/MM/YYYY'),"
			+"'"+ pshinhthuctt    +"',"
			+"'"+ psma_bc         +"',"
			+"'"+ psma_tn         +"',"
			+"'"+ psmaquay        +"',"
			+"'"+ psloai          +"',"  
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}	
	public String doc_layds_kyquy(
		String psphieu_id   ,
	    String psma_kh      ,
		String psma_tb      ,
		String psso_ct      ,
		String psngaynhap   ,
		String pspagenum    ,
		String pspagerec    
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_kyquy/ajax_ds_kyquy"
			+"&"+CesarCode.encode("phieu_id")+"="+psphieu_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("so_ct")+"="+psso_ct			
			+"&"+CesarCode.encode("ngaynhan")+"="+psngaynhap			
			+"&"+CesarCode.encode("page_num")+"="+pspagenum
			+"&"+CesarCode.encode("page_rec")+"="+pspagerec;
	}
	public String value_themmoi_kyquy(
		String psma_kh           ,
		String psma_tb           ,
		String pstien            ,				
		String psnoidung         ,
		String pschukyno         ,
		String psloaitien_id     ,
		String psngaynhap        ,
		String psdichvukq_id	 ,
		String psma_bc           ,
		String psma_tn           ,
		String psmaquay          
	)
	{
		String s="begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.themmoi_kyquy("
			+"'"+ psma_kh         +"',"
			+"'"+ psma_tb         +"',"			
			+"'"+ pstien          +"',"
			+"'"+ psnoidung       +"',"
			+"'"+ pschukyno       +"',"
			+"'"+ psloaitien_id   +"',"
			+"to_date('"+ psngaynhap      +"','DD/MM/YYYY'),"
			+"'"+ psdichvukq_id    +"',"
			+"'"+ psma_bc         +"',"
			+"'"+ psma_tn         +"',"
			+"'"+ psmaquay        +"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_themmoi_chikyquy(
		String psma_kh           ,
		String psma_tb           ,
		String psphieuchi		 ,
		String pstien            ,				
		String psnoidung         ,
		String pschukyno         ,
		String psloaitien_id     ,
		String psngaynhap        ,
		String psdichvukq_id     ,
		String psma_bc           ,
		String psma_tn           ,
		String psmaquay                      
	)
	{
		String s="begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.themmoi_chikyquy("
			+"'"+ psma_kh         +"',"
			+"'"+ psma_tb         +"',"			
			+"'"+ psphieuchi      +"',"
			+"'"+ pstien          +"',"
			+"'"+ psnoidung       +"',"
			+"'"+ pschukyno       +"',"
			+"'"+ psloaitien_id   +"',"
			+"to_date('"+ psngaynhap      +"','DD/MM/YYYY'),"
			+"'"+ psdichvukq_id   +"',"
			+"'"+ psma_bc         +"',"
			+"'"+ psma_tn         +"',"
			+"'"+ psmaquay        +"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_xoa_kyquy
	( 
		String psKyhoadon,
		String psDsPhieu_ID,
		String psMa_BC
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.xoa_kyquy("			
		    +"'"+psKyhoadon+"'"
	 	    +",'"+psDsPhieu_ID+"'"
	 	    +",'"+psMa_BC+"'"		
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
	public String value_xoa_chikyquy
	( 
		String psKyhoadon,
		String psDsPhieu_ID,
		String psMa_BC
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.xoa_chikyquy("			
		    +"'"+psKyhoadon+"'"
	 	    +",'"+psDsPhieu_ID+"'"
	 	    +",'"+psMa_BC+"'"	
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
	public String doc_layds_ctcuocnong(
	    String psma_kh,
		String psma_tb      
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tamthu/ajax_chitietcuoc"
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb;
	}
	//Da xoa ham nay ductt - 21/02/2009
	public String value_tamthu1(
		String pskyhoadon,
		String psdsma_kh,
		String psdsma_tb,
		String pstientra,
		String pdngay_tt,
		String pihttt_id,
		String piloaitien_id,
		String psgomhd,
		String psnoidung
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.tamthu1("			
		    +"'"+pskyhoadon+"'"
	 	    +",'"+psdsma_kh+"'"
	 	    +",'"+psdsma_tb+"'"
	 	    +",'"+pstientra+"'"
	 	    +",'"+pdngay_tt+"'"
	 	    +",'"+pihttt_id+"'"
	 	    +",'"+piloaitien_id+"'"
	 	    +",'"+psgomhd+"'"
	 	    +",'"+psnoidung+"'"
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
	public String value_chicuocnong1(
		String pskyhoadon,
		String psphieuthu_id,
		String psdsma_kh,
		String psdsma_tb,
		String pstientra,
		String pdngay_tt,
		String pihttt_id,
		String piloaitien_id,
		String psgomhd,
		String psnoidung
	)
	{
		String sql="begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.chicuocnong1("			
		    +"'"+pskyhoadon+"'"
		    +",'"+psphieuthu_id+"'"
	 	    +",'"+psdsma_kh+"'"
	 	    +",'"+psdsma_tb+"'"
	 	    +",'"+pstientra+"'"
	 	    +",'"+pdngay_tt+"'"
	 	    +",'"+pihttt_id+"'"
	 	    +",'"+piloaitien_id+"'"
	 	    +",'"+psgomhd+"'"
	 	    +",'"+psnoidung+"'"
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	    System.out.println(sql);
	    return sql;
	}
	public String value_xoa_chicuocnong
	( 
		String psKyhoadon,
		String psDsPhieu_ID,
		String psMa_BC
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.xoa_chicuocnong("			
		    +"'"+psKyhoadon+"'"
	 	    +",'"+psDsPhieu_ID+"'"
	 	    +",'"+psMa_BC+"'"	
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
	
	public String value_xoa_tamthu
	( 
		String psKyhoadon,
		String psDsPhieu_ID,
		String psMa_BC
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.xoa_tamthu("			
		    +"'"+psKyhoadon+"'"
	 	    +",'"+psDsPhieu_ID+"'"
	 	    +",'"+psMa_BC+"'"		
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}


	public String doc_layds_tamthu_bk(
		String psphieu_id   ,
	     	String psma_kh      ,
		String psma_tb      ,
		String psma_kh1      ,
		String psma_tb1     ,
		String pschukyno	  ,	
		String psso_ct      ,
		String psngaynhap   ,
		String pspagenum    ,
		String pspagerec    
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tratruoc/ajax_ds_kyquy"
			+"&"+CesarCode.encode("phieu_id")+"="+psphieu_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("ma_kh1")+"="+psma_kh1
			+"&"+CesarCode.encode("ma_tb1")+"="+psma_tb1
			+"&"+CesarCode.encode("ck_no")+"="+pschukyno
			+"&"+CesarCode.encode("so_ct")+"="+psso_ct			
			+"&"+CesarCode.encode("ngaynhan")+"="+psngaynhap			
			+"&"+CesarCode.encode("page_num")+"="+pspagenum
			+"&"+CesarCode.encode("page_rec")+"="+pspagerec;
	}
	public String doc_layds_tamthu(
		String psphieu_id   ,
	    String psma_kh      ,
		String psma_tb      ,
		String psma_kh1      ,
		String psma_tb1     ,
		String pschukyno	  ,		
		String psngaynhap   ,
		String psnguoinhap   ,
		String pshttt   ,
		String pspagenum    ,
		String pspagerec    
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tratruoc/ajax_ds_kyquy"
			+"&"+CesarCode.encode("phieu_id")+"="+psphieu_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("ma_kh1")+"="+psma_kh1
			+"&"+CesarCode.encode("ma_tb1")+"="+psma_tb1
			+"&"+CesarCode.encode("ck_no")+"="+pschukyno
			+"&"+CesarCode.encode("ngaynhan")+"="+psngaynhap			
			+"&"+CesarCode.encode("nguoinhap")+"="+psnguoinhap
			+"&"+CesarCode.encode("httt_id")+"="+pshttt			
			+"&"+CesarCode.encode("page_num")+"="+pspagenum
			+"&"+CesarCode.encode("page_rec")+"="+pspagerec;
	}

	public String value_themmoi_tamthu(
		String psma_kh           ,
		String psma_tb           ,
		String psma_kh1           ,
		String psma_tb1           ,
		String pschukyno         ,
		String pschukyno1         ,
		String pstien            ,
		String psvat             ,	
		String pstongtien	,			
		String psghichu          ,
		String pskyhoadon	,
		String pshttt        	 ,
		String pskieunhap	,
		String pskieugiam,
		String psngaynhap        ,
		String psma_bc           ,
		String psma_tn           
		
	)
	{
		String s="begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.themmoi_tamthu("
			+"'"+ psma_kh         +"',"
			+"'"+ psma_tb         +"',"	
			+"'"+ psma_kh1         +"',"
			+"'"+ psma_tb1         +"',"		
			+"'"+ pstien          +"',"
			+"'"+ psvat          +"',"
			+"'"+ pstongtien          +"',"
			+"'"+ psghichu        +"',"
			+"'"+ pschukyno       +"',"
			+"'"+ pschukyno1       +"',"
			+"'"+ pskyhoadon +"',"
			+"'"+ pshttt   	    +"',"
			+"'"+ pskieunhap    +"',"
			+"'"+ pskieugiam    +"',"
			+"'"+ psngaynhap    +"',"
			//+"to_date('"+ psngaynhap      +"','DD/MM/YYYY'),"
			+"'"+ psma_bc          +"',"
			+"'"+ psma_tn          +"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}

	public String doc_layds_tamthu_12(
		String psphieu_id   ,
	     String psma_kh      ,
		String psma_tb      ,
		String pschukyno	  ,	
		String psso_ct      ,
		String psngaynhap   ,
		String pspagenum    ,
		String pspagerec    
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tratruoc/ajax_ds_kyquy"
			+"&"+CesarCode.encode("phieu_id")+"="+psphieu_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("ck_no")+"="+pschukyno
			+"&"+CesarCode.encode("so_ct")+"="+psso_ct			
			+"&"+CesarCode.encode("ngaynhan")+"="+psngaynhap			
			+"&"+CesarCode.encode("page_num")+"="+pspagenum
			+"&"+CesarCode.encode("page_rec")+"="+pspagerec;
	}
	
	public String rec_notonghop_tratruoc(
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psLoaiTienID		
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.laytt_notonghop('"
			+psChukyno+"','"
			+psMaKH+"','"
			+psMaTB+"','"
			+""+psLoaiTienID+"','"
				+""+psLoaiTienID+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
  	}
	
	//Bo xung de tach tam thu va cuoc nong ductt:21/02/2009
	public String value_tamthu1(
		String pskyhoadon,
		String psdsma_kh,
		String psdsma_tb,
		String pstientra,
		String pdngay_tt,
		String pihttt_id,
		String piloaitien_id,
		String psgomhd,
		String psnoidung,
		String pschitamthu
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.tamthu1("			
		    +"'"+pskyhoadon+"'"
	 	    +",'"+psdsma_kh+"'"
	 	    +",'"+psdsma_tb+"'"
	 	    +",'"+pstientra+"'"
	 	    +",'"+pdngay_tt+"'"
	 	    +",'"+pihttt_id+"'"
	 	    +",'"+piloaitien_id+"'"
	 	    +",'"+psgomhd+"'"
	 	    +",'"+psnoidung+"'"
			+",'"+pschitamthu+"'"
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
	public String doc_notonghop2(
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psLoaiTienID
	)
	{
		/*String s="begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.laytt_notonghop2('"
			+psChukyno+"','"
			+psMaKH+"','"
			+psMaTB+"','"
			+psLoaiTienID+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;*/
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tienmat/ajax_laytt_notonghop_kyquy"
			+"&"+CesarCode.encode("chukyno")+"="+psChukyno
			+"&"+CesarCode.encode("userinput")+"="+psMaKH
			+"&"+CesarCode.encode("ma_tb")+"="+psMaTB
			+"&"+CesarCode.encode("loaitien")+"="+psLoaiTienID;
  	}
		public String value_themmoi_chitra(
		String psma_kh,
		String psma_tb,	
		String phieu_id,	
		String pschukyno,		
		String pssotien ,
		String pshtctt,
		String psngaynhap     
		
	)
	{
		String s="begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.themmoi_chitra("
			+"'"+ psma_kh +"',"
			+"'"+ psma_tb  +"',"	
			+"'"+ phieu_id +"',"	
		
			+"'"+ pssotien +"',"		
			+"'"+ pschukyno +"',"			
			+"'"+ pshtctt +"',"		
			+"'"+ psngaynhap +"',"			
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_thuhoi(
		String psma_kh,
		String psma_tb,			
		String pschukyno		   
		
	)
	{
		String s="begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.thuhoi_thuong("
			+"'"+ psma_kh +"',"
			+"'"+ psma_tb  +"',"			
			+"'"+ pschukyno +"',"			
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
		public String rec_no_ckn(
		String pschukyno,
		String psma_tb	,
		String psma_kh
			
	)
	{
		//return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.laytt_no_theockn('"
		return "begin ?:= "+getSysConst("FuncSchema")+"ninhbd_qltn.laytt_no_theockn('"
			+pschukyno+"','"
			+psma_tb+"','"	
			+psma_kh+"','"					
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
  	}
 public String doc_tracuu_tamthu(
	  String	psphieu_id   ,
	  String   psma_kh      ,
      String psma_tb      ,
      String psdonvi      ,
      String psbuucuc      ,
      String psnguoith     ,
      String pschukyno    ,
      String pstungay     ,
      String psdenngay    ,
      String pstuky       ,
      String psdenky      ,
	  String pshttt      ,
      String pspagenum    ,
      String pspagerec    
	   
	
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tratruoc/tracuu/ajax_ds_tamthu"
			+"&"+CesarCode.encode("phieu_id")+"="+psphieu_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("donvi")+"="+psdonvi
			+"&"+CesarCode.encode("buucuc")+"="+psbuucuc
			+"&"+CesarCode.encode("nguoinhan")+"="+psnguoith
			+"&"+CesarCode.encode("ck_no")+"="+pschukyno
			+"&"+CesarCode.encode("tungay")+"="+pstungay
			+"&"+CesarCode.encode("denngay")+"="+psdenngay
			+"&"+CesarCode.encode("tuky")+"="+pstuky			
			+"&"+CesarCode.encode("denky")+"="+psdenky	
			+"&"+CesarCode.encode("httt")+"="+pshttt				
			+"&"+CesarCode.encode("page_num")+"="+pspagenum
			+"&"+CesarCode.encode("page_rec")+"="+pspagerec;
	}
public String doc_tracuu_thuong(
	  String	psphieu_id   ,
	  String   psma_kh      ,
      String psma_tb      ,
      String psdonvi      ,
      String psbuucuc      ,
      String psnguoith     ,
      String pschukyno    ,
      String pstungay     ,
      String psdenngay    ,
      String pstuky       ,
      String psdenky      ,
      String pspagenum    ,
      String pspagerec    
	   
	
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tratruoc/tracuu/ajax_ds_trichthuong"
			+"&"+CesarCode.encode("phieu_id")+"="+psphieu_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("donvi")+"="+psdonvi
			+"&"+CesarCode.encode("buucuc")+"="+psbuucuc
			+"&"+CesarCode.encode("nguoinhan")+"="+psnguoith
			+"&"+CesarCode.encode("ck_no")+"="+pschukyno
			+"&"+CesarCode.encode("tungay")+"="+pstungay
			+"&"+CesarCode.encode("denngay")+"="+psdenngay
			+"&"+CesarCode.encode("tuky")+"="+pstuky			
			+"&"+CesarCode.encode("denky")+"="+psdenky			
			+"&"+CesarCode.encode("page_num")+"="+pspagenum
			+"&"+CesarCode.encode("page_rec")+"="+pspagerec;
	}
	public String value_tt_chitra(
		String psma_kh,
		String psma_tb,	
		String pschukyno		
	)
	{
		String s="begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.laytt_tamthu_trichthuong("
			+"'"+ psma_kh +"',"
			+"'"+ psma_tb  +"',"	
			+"'"+ pschukyno +"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_xoa_tt_tratruoc
	( 
		String psKyhoadon,
		String psDsPhieu_ID,
		String psMa_BC
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"ninhbd_qltn.xoa_tt_tratruoc("			
		    +"'"+psKyhoadon+"'"
	 	    +",'"+psDsPhieu_ID+"'"
	 	    +",'"+psMa_BC+"'"		
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
	public String doc_layds_trichthuong(
		String psphieu_id   ,
	    String psma_kh      ,
		String psma_tb      ,
		String psma_kh1      ,
		String psma_tb1     ,
		String pschukyno	  ,		
		String psngaynhap   ,
		String psnguoinhap   ,
		String pshttt   ,
		String pspagenum    ,
		String pspagerec    
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tratruoc/ajax_ds_trichthuong"
			+"&"+CesarCode.encode("phieu_id")+"="+psphieu_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("ma_kh1")+"="+psma_kh1
			+"&"+CesarCode.encode("ma_tb1")+"="+psma_tb1
			+"&"+CesarCode.encode("ck_no")+"="+pschukyno
			+"&"+CesarCode.encode("ngaynhan")+"="+psngaynhap			
			+"&"+CesarCode.encode("nguoinhap")+"="+psnguoinhap
			+"&"+CesarCode.encode("httt_id")+"="+pshttt			
			+"&"+CesarCode.encode("page_num")+"="+pspagenum
			+"&"+CesarCode.encode("page_rec")+"="+pspagerec;
	}
	public String value_capnhatseri(String psKyhoadon,String luotthanhtoan)
	{
			return "begin ?:= admin_v2.qltn_thtoan_new.capnhatseri("
			+"'"+psKyhoadon+"'"
			+",'"+luotthanhtoan+"'"
	 	  	+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	
  	}		
}