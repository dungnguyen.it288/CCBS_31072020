package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class qltn_gachno extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("QLTN Gach no");
		//funcschema=getSysConst("FuncSchema");
    	//dataschema=getUserVar("sys_dataschema");
    	//userid=getUserVar("userID");		
	}
	public String value_tong_phieu_gach(
		String psChukyno,
		String psNgayTT
	)
	{
		return "begin ?:="+getSysConst("FuncSchema")+"qltn.laytt_tongphieu_gachno('"+
			psChukyno+"','"+psNgayTT+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
	}
	public String value_tong_tien_gach(
		String psChukyno,
		String psNgayTT
	)
	{
		return "begin ?:="+getSysConst("FuncSchema")+"qltn.laytt_tongtien_gachno('"+
			psChukyno+"','"+psNgayTT+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
	}
	public String rec_tienphieu_gachno(
		String psChukyno,
		String psNgayTT
	)
	{
		return "begin ?:="+getSysConst("FuncSchema")+"qltn.laytt_tienphieu_gachno('"+psChukyno+"','"+psNgayTT+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
	}
	public String doc_tt_khachhang(
  		String psUserInput,
  		String psDonviQL,
  		String psChukyno
  		)
  	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_laytt_khachhang"
  			+ "&" + CesarCode.encode("userinput") + "=" + psUserInput
  			+ "&" + CesarCode.encode("don_vi_ql") + "=" + psDonviQL
  			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno;
  	}
	public String rec_tt_khachhang(
  		String psUserInput,
  		String psDonviQL,
  		String psChukyno
  		)
  	{
  		return "begin ? := "+getSysConst("FuncSchema")
  			+"qltn_tracuu.laytt_khachhang_donvi('"
  			+psUserInput+"','"
  			+psChukyno+"',"
  			+psDonviQL+",'"
  			+getUserVar("sys_dataschema")+"','"
  			+getUserVar("userID")+"'); end;";
  	}
  	public NEOCommonData rset_tt_khachhang(
  		String psUserInput,
  		String psDonviQL,
  		String psChukyno
  	)
	{
		String sql = "begin ? := "+getSysConst("FuncSchema")
  			+"qltn_tracuu.laytt_khachhang_donvi('"
  			+psUserInput+"','"
  			+psChukyno+"',"
  			+psDonviQL+",'"
  			+getUserVar("sys_dataschema")+"','"
  			+getUserVar("userID")+"'); end;";
		NEOExecInfo nei_= new NEOExecInfo(sql);
		NEOCommonData ncd_ = new NEOCommonData(nei_);
		return ncd_;
	}
  	public String value_diadiem_tt(
  		String psMaKH
  	)
	{
		return "begin ? := "+getSysConst("FuncSchema")+"qltn_tracuu.laytt_diadiem_tt('"+psMaKH+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
	}
	public String doc_no_tong_hop(
		String psMaKH,
		String psMaTB,
		String psLoaiTienID,
		String psChukyno
	)
	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_lay_no_tonghop"
  			+ "&" + CesarCode.encode("ma_kh") + "=" + psMaKH
  			+ "&" + CesarCode.encode("ma_tb") + "=" + psMaTB
  			+ "&" + CesarCode.encode("id_tien") + "=" + psLoaiTienID
  			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno;
  	}
  	public String rec_no_tong_hop(
		String psMaKH,
		String psMaTB,
		String psLoaiTienID,
		String psChukyno
	)
	{
  		return "begin ? := "+getSysConst("FuncSchema")
  			+"qltn_tracuu.laytt_no_tonghop('"
  			+psMaKH+"','"
  			+psMaTB+"','"
  			+psChukyno+"',"
  			+psLoaiTienID+",'"
  			+getUserVar("sys_dataschema")+"','"
  			+getUserVar("userID")+"'); end;";
  	}
  	public String doc_nochitiet(
		String psMaKH,
		String psMaTB,		
		String psChukyno,
		String psLoaiTienID
	)
	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_lay_no_chukyno"
  			+ "&" + CesarCode.encode("ma_kh") + "=" + psMaKH
  			+ "&" + CesarCode.encode("ma_tb") + "=" + psMaTB
  			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
  			+ "&" + CesarCode.encode("loaitien_id") + "=" + psLoaiTienID;
  	}  	
  	public String doc_no_khoanmuc(
		String psMaKH,
		String psMaTB,		
		String psDsChukyno,
		String psChukyno,
		String psLoaiTienID
	)
	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_lay_no_khoanmuc"
  			+ "&" + CesarCode.encode("ma_kh") + "=" + psMaKH
  			+ "&" + CesarCode.encode("ma_tb") + "=" + psMaTB
  			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
  			+ "&" + CesarCode.encode("dschukyno") + "=" + psDsChukyno
  			+ "&" + CesarCode.encode("loaitien_id") + "=" + psLoaiTienID;
  	}  	
  	public String rec_mathanhtoan(
  		String psMaTB,
  		String psDonViQL,
  		String psChukyno
  	)
	{
		return "begin ? := "+getSysConst("FuncSchema")+"qltn_tracuu.laytt_khachhang_donvi('"+psMaTB+"','"+psChukyno+"',"+psDonViQL+",'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
	}
	public String value_gachno_chukyno(	
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psDsChukyno,
		String psDsTienTraDK,
		String psDsTienTraPS,
		String psChucNangGachNo,
		String psNgay_TT,
		String psHTTT_ID,
		String psLoaiTien_ID,
		String psNganHang_ID,
		String psChungTu,
		String psNgayNH,
		String psLuotTT
	)
	{
		return "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.gachno('"
			+psChukyno+"'"
			+",'"+psMaKH+"'"
			+",'"+psMaTB+"'"
			+",'"+psDsChukyno+"'"
			+",'"+psDsTienTraDK+"'"
			+",'"+psDsTienTraPS+"'"
			+","+psChucNangGachNo
			+",to_date('"+psNgay_TT+"','DD/MM/YYYY')"
			+","+psHTTT_ID
			+","+psLoaiTien_ID
			+","+psNganHang_ID
			+",'"+psChungTu+"'"
			+",to_date('"+psNgayNH+"','DD/MM/YYYY')"
			+","+psLuotTT
			+",'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";		
	}
	public String value_gachno_khoanmuc(	
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psDsKhoanmuc,
		String psDsTienTraDK,
		String psDsTienTraPS,
		String psChucNangGachNo,
		String psNgay_TT,
		String psHTTT_ID,
		String psLoaiTien_ID,
		String psNganHang_ID,
		String psChungTu,
		String psNgayNH,
		String psLuotTT
	)
	{
		return "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.gachno_khoanmuc('"
			+psChukyno+"'"
			+",'"+psMaKH+"'"
			+",'"+psMaTB+"'"
			+",'"+psDsKhoanmuc+"'"
			+",'"+psDsTienTraDK+"'"
			+",'"+psDsTienTraPS+"'"
			+","+psChucNangGachNo
			+",to_date('"+psNgay_TT+"','DD/MM/YYYY')"
			+","+psHTTT_ID
			+","+psLoaiTien_ID
			+","+psNganHang_ID
			+",'"+psChungTu+"'"
			+",to_date('"+psNgayNH+"','DD/MM/YYYY')"
			+","+psLuotTT
			+",'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
	}
	public String doc_luotthanhtoan(
		String psChukyno,
		String psNgaythu,		
		String psLuotthu_ID,
		String psDieukien,
		String psMaNV
	)
	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_thudaily/TraCuuLuotThanhToan_ajax"
  			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
  			+ "&" + CesarCode.encode("luotthu_id") + "=" + psLuotthu_ID
  			+ "&" + CesarCode.encode("ma_nv") + "=" + psMaNV
  			+ "&" + CesarCode.encode("ngaythu") + "=" + psNgaythu
  			+ "&" + CesarCode.encode("dieukien") + "=" + psDieukien;
  	}
  	public String value_xoaphieu_tt(
  		String psChukyno,
  		String psDsRid,
  		String psKieuhuy,
  		String psSys_UserIp
  	)
  	{
  		return "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.xoaphieu_thanhtoan('"+psChukyno+"','"+psDsRid+"'"
			+",'"+psKieuhuy+"','"+psSys_UserIp+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
  	}
  	public String rec_tracuu_phieu_tt(
  		String psChukyno,
  		String psLuot_ID,
  		String psNgayThu,
  		String psDieuKien,
  		String psMa_KH,
  		String psMa_KH1,
  		String psMa_KH2
  	)
  	{
  		return "SELECT count(*) tongso_phieu,trim(to_char(sum(sotien),'999,999,999,999')) tongso_tien"
  			+" FROM "+getUserVar("sys_dataschema")+"chitietphieu_"+psChukyno+" a"
 			+" WHERE TO_CHAR (ngaythu, 'DD/MM/YYYY') = '"+psNgayThu+"'"
   			+" AND sotien <> 0 AND luotthu_id = "+psLuot_ID+" AND "+psDieuKien+" "+psMa_KH+" "+psMa_KH1+" "+psMa_KH2;
  	}
  	public String value_apply_khuyenmai(
  		String psChukyno,
  		String psDsMaKH,
  		String psChucNangGachNo,
  		String psNgayTT,  		
  		String psHTTT_ID,  		
  		String psNgayNH,
  		String psLoaiTienID,  		
  		String psHinhThucKM_ID
  	)
  	{
  		return "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.gachno_khuyenmai('"
			+psChukyno+"'"			
			+",'"+psDsMaKH+"'"
			+","+psChucNangGachNo
			+",to_date('"+psNgayTT+"','DD/MM/YYYY')"
			+","+psHTTT_ID
			+",to_date('"+psNgayNH+"','DD/MM/YYYY')"
			+","+psLoaiTienID
			+","+psHinhThucKM_ID				
			+",'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";		
  	}
  	public String doc_cuocnong()
  	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_nhanchungtu/ajax_tamthu_cuocnong";
  	}
  	public String doc_chuyenkhoan()
  	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_nhanchungtu/ajax_chuyenkhoan";
  	}
	public String doc_tamthu()
  	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_nhanchungtu/ajax_tamthu";
  	}
	public String doc_gachno_tienmat()
  	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/frmThanhtoan"
			+ "&" + CesarCode.encode("httt_id") + "=6&" + CesarCode.encode("debtype") + "=4";
  	}
	public String ds_chungtu(
		String chukyno,
		String ma_kh,
		String tungay,
		String denngay,
		String trangthai_id,
		String loai_chuyenkhoan,
		String psLoaiTienId
	)
	{		
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_nhanchungtu/ajax_ds_sec"
			+"&"+CesarCode.encode("chukyno")+"="+chukyno
			+"&"+CesarCode.encode("ma_kh")+"="+ma_kh
			+"&"+CesarCode.encode("tungay")+"="+tungay
			+"&"+CesarCode.encode("denngay")+"="+denngay
			+"&"+CesarCode.encode("trangthai_id")+"="+trangthai_id
			+"&"+CesarCode.encode("loai_chuyenkhoan")+"="+loai_chuyenkhoan
			+"&"+CesarCode.encode("loaitien_id")+"="+psLoaiTienId; 	
	}
	public String ds_chungtu_cn(
		String chukyno,
		String ma_kh,
		String tungay,
		String denngay,
		String trangthai_id,
		String loai_chuyenkhoan,
		String psLoaiTienId
	)
	{		
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_nhanchungtu/ajax_ds_sec_cn"
			+"&"+CesarCode.encode("chukyno")+"="+chukyno
			+"&"+CesarCode.encode("ma_kh")+"="+ma_kh
			+"&"+CesarCode.encode("tungay")+"="+tungay
			+"&"+CesarCode.encode("denngay")+"="+denngay
			+"&"+CesarCode.encode("trangthai_id")+"="+trangthai_id
			+"&"+CesarCode.encode("loai_chuyenkhoan")+"="+loai_chuyenkhoan
			+"&"+CesarCode.encode("loaitien_id")+"="+psLoaiTienId; 	
	}
	public String doc_cac_tb_theo_makh(
		String psMaKH,
		String psChukyno
	)
  	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/qltn/tracuu/ajax_layma_tb"
			+"&"+CesarCode.encode("chukyno")+"="+psChukyno
			+"&"+CesarCode.encode("ma_kh")+"="+psMaKH;
  	}
	public String doc_tracuu_chitiet_thanhtoan(
		String psCheckAll,
		String psMaKH,
		String psMaTB,
		String psChukyno
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/common/qltn/tracuu/frmtracuu_chitiet_thanhtoan_data1"
			+"&"+CesarCode.encode("checkall")+"="+psCheckAll
			+"&"+CesarCode.encode("txtMAKH")+"="+psMaKH
			+"&"+CesarCode.encode("somay")+"="+psMaTB
			+"&"+CesarCode.encode("CHUKYNO")+"="+psChukyno; 	
	}
	public String rec_no_tonghop(
		String psMaKH,
		String psMaTB,
		String psChukyno
	)
	{
		return "begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.laytt_no_tonghop('"
			+psMaKH+"','"
			+psMaTB+"','"
			+psChukyno+"','"
			+"1"+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
	}
	public String doc_trangthai_thuebao(
		String psMaKH					
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/ajax_ds_trangthai_tb"
			+"&"+CesarCode.encode("ma_kh")+"="+psMaKH;
	}
	public String value_xoa_phieu(
		String psChukyno,
		String psPhieu_ID,
		String psKieuhuy
	)
	{
		return "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.xoaphieu_bangphieutra('"
			+psChukyno+"',"
			+psPhieu_ID+",'"
			+psKieuhuy+"','"
			+getUserVar("sys_userip")+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
	}
}