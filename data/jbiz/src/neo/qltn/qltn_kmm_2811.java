package neo.qltn;

import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class qltn_kmm extends NEOProcessEx {

    public void run() {
        System.out.println("neo.qltn.qltn_tienmat was called");
    }


	public String doc_layds_ls_khoamay_qnh(
      		 String ngaykhoa,
		String donvi,	
		String chieukhoa
	){
		return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/nocuoc/tinhtrang/ajax_ls_km_qnh" 
            + "&" + CesarCode.encode("ngaykhoa") + "=" +ngaykhoa 
            + "&" + CesarCode.encode("donvi") + "=" +donvi 
            + "&" + CesarCode.encode("chieukhoa") + "=" + chieukhoa;
	}
	

public String doc_layds_ls_khoamay_vtu
(
	String ngaykhoa,
	String donvi,	
	String chieukhoa
)
{
	return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/nocuoc/tinhtrang/ajax_ls_km_vtu" 
            + "&" + CesarCode.encode("ngaykhoa") + "=" +ngaykhoa 
            + "&" + CesarCode.encode("donvi") + "=" +donvi 
            + "&" + CesarCode.encode("chieukhoa") + "=" + chieukhoa;
}

public String doc_layds_ls_khoamay_qni
(
	String ngaykhoa,
	String donvi,	
	String chieukhoa
)
{
	return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/nocuoc/tinhtrang/ajax_ls_km_qni" 
            + "&" + CesarCode.encode("ngaykhoa") + "=" +ngaykhoa 
            + "&" + CesarCode.encode("donvi") + "=" +donvi 
            + "&" + CesarCode.encode("chieukhoa") + "=" + chieukhoa;
}

public String doc_layds_ls_khoamay_pto
(
	String ngaykhoa,
	String donvi,	
	String chieukhoa
)
{
	return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/nocuoc/tinhtrang/ajax_ls_km_pto" 
            + "&" + CesarCode.encode("ngaykhoa") + "=" +ngaykhoa 
            + "&" + CesarCode.encode("donvi") + "=" +donvi 
            + "&" + CesarCode.encode("chieukhoa") + "=" + chieukhoa;
}


    public String value_taods_kmtd(
            String dvvt,
            String kyhoadon,
            String thang,
            String kieukhoa,
            String kieunhac,
            String ngaygh,
            String tienMin,
            String tienMax,
            String thangMin,
            String thangMax,
            String manv,
            String loaikh,
            String kieutien,
            String mabc,
            String khdb,
            String hinhthuckhoa,
            String psDonvi) {
        String s = "begin ?:= " + getSysConst("FuncSchema") + "QLN_KMM.tao_ds_kmtd(" + "'" + kyhoadon + "'," + "'" + thang + "'," + "'danhba_dds_pttb'," + dvvt + "," + kieukhoa + "," + kieunhac + "," + ngaygh + "," + tienMin + "," + tienMax + "," + thangMin + "," + thangMax + "," + "'" + manv + "'," + "'" + loaikh + "'," + kieutien + "," + "'somay'," + "'" + mabc + "'," + khdb + "," + hinhthuckhoa + "," + "'" + psDonvi + "'," + "'" + getUserVar("sys_dataschema") + "'," + "'" + getUserVar("userID") + "'" + "); end;";
        return s;
    }

    public String ref_layds_kmtd(
            String psKieu,
            String psMaBC,
            String loai_tb,
            String ht_khoa,
            String psPageNum,
            String psPageRec) {
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/khoamomay/khoamaytudong/ajax_ds_chuakhoa" + "&" + CesarCode.encode("kieu") + "=" + psKieu + "&" + CesarCode.encode("ma_bc") + "=" + psMaBC + "&" + CesarCode.encode("dvvt") + "=" + loai_tb + "&" + CesarCode.encode("hinhthuckhoa") + "=" + ht_khoa + "&" + CesarCode.encode("page_num") + "=" + psPageNum + "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
    }

    public String value_xoa_tbs_kmtd(
            String psds_thuebao, String pihinhthuckhoa, String loai_tb, String ma_bc) {
        String s = "begin ?:=" + getSysConst("FuncSchema") + "qln_kmm.xoa_ds_kmtd(" + "'" + psds_thuebao + "'" + ",'" + pihinhthuckhoa + "'" + ",'" + loai_tb + "'" + ",'" + ma_bc + "'" + ",'" + getUserVar("sys_dataschema") + "'," + "'" + getUserVar("userID") + "'" + ");" + " end;";
        return s;
    }

    public String value_khoamay_tudong(
            String pi_DVVT,
            String ps_MA_BC,
            String pn_Hinh_Thuc_Khoa) {
        String s = "begin ?:=" + getSysConst("FuncSchema") + "qln_kmm.khoamay_tudong(" + "'" + pi_DVVT + "'," + "'" + ps_MA_BC + "'," + "'" + pn_Hinh_Thuc_Khoa + "'," + "'" + getUserVar("sys_dataschema") + "'," + "'" + getUserVar("userID") + "'); end;";
        return s;
    }

    public String ds_theodoi_kmm(
            String pikieuds,
            String piloaitb,
            String pidieukienloc,
            String pstungay,
            String psdenngay,
            String pidvql_id,
            String psPageNum,
            String psPageRec) {
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/khoamomay/theodoichemmay/ajax_lay_ds" + "&" + CesarCode.encode("kieuds") + "=" + pikieuds + "&" + CesarCode.encode("loaitb") + "=" + piloaitb + "&" + CesarCode.encode("dkloc") + "=" + pidieukienloc + "&" + CesarCode.encode("tungay") + "=" + pstungay + "&" + CesarCode.encode("denngay") + "=" + psdenngay + "&" + CesarCode.encode("dvql_id") + "=" + pidvql_id + "&" + CesarCode.encode("page_num") + "=" + psPageNum + "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
    }

    public String value_capnhat_kmm(
            String ps_DanhSach_TB, String psTrangThai, String psKhoaMay, String pi_LoaiDB) {
        String s = "begin ?:= " + getSysConst("FuncSchema") + "QLN_KMM.capnhat_theodoi_kmm(" + "'" + ps_DanhSach_TB + "'" + ",'" + psTrangThai + "'" + ",'" + psKhoaMay + "'" + ",'" + pi_LoaiDB + "'" + ",'" + getUserVar("sys_dataschema") + "'" + ",'" + getUserVar("userID") + "'" + "); end;";
        return s;
    }

    public String value_luulichsu_kmm(
            String ps_DanhSach_TB,
            String psKieuDs) {
        String s = "begin ?:= " + getSysConst("FuncSchema") + "QLN_KMM.luulichsu_kmm(" + "'" + ps_DanhSach_TB + "'," + psKieuDs + ",'" + getUserVar("sys_dataschema") + "'" + ",'" + getUserVar("userID") + "'" + "); end;";
        return s;
    }

    public String value_khoamay_tb(
            String psDsMaTB,
            String pi_DVVT,
            String ps_MA_BC,
            String pn_Hinh_Thuc_Khoa) {
        String s = "begin ?:=" + getSysConst("FuncSchema") + "qln_kmm.khoamay_theo_tb(" + "'" + psDsMaTB + "'," + "'" + pi_DVVT + "'," + "'" + ps_MA_BC + "'," + "'" + pn_Hinh_Thuc_Khoa + "'," + "'" + getUserVar("sys_dataschema") + "'," + "'" + getUserVar("userID") + "'); end;";
        return s;
    }

    public String value_them_tbs_kmm(
            String psDVVT,
            String psMaBC,
            String psDsMaTB,
            String psHinhThucKM,
            String psDonviQL) {
        String s = "begin ?:=" + getSysConst("FuncSchema") + "qln_kmm.them_tb_kmm(" + "'" + psDVVT + "'," + "'" + psMaBC + "'," + "'" + psDsMaTB + "'," + "'" + psHinhThucKM + "'," + "'" + psDonviQL + "'," + "'" + getUserVar("sys_dataschema") + "'," + "'" + getUserVar("userID") + "'); end;";
        return s;
    }
    /*Cac ham cua Mo may*/

    public String value_tao_ds_mmtd(
            String kyhoadon,
            String loai_tb,
            String kieunhac,
            String tien,
            String ma_bc) {
        String s = "begin ?:= " + getSysConst("FuncSchema") + "QLN_KMM.tao_ds_mmtd(" + "'" + kyhoadon + "'" + "," + loai_tb + "," + kieunhac + "," + tien + ",'" + ma_bc + "'," + "'" + getUserVar("sys_dataschema") + "'," + "'" + getUserVar("userID") + "'); end;";
        return s;
    }

    public String doc_ds_momay(
            String loai_tb,
            String ma_bc,
            String psPageNum,
            String psPageRec) {
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/khoamomay/momaytudong/ajax_tao_ds" + "&" + CesarCode.encode("loai_tb") + "=" + loai_tb + "&" + CesarCode.encode("ma_bc") + "=" + ma_bc + "&" + CesarCode.encode("page_num") + "=" + psPageNum + "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
    }

    public String value_momay_tudong(
            String psDsMaTB,
            String psLoai_TB,
            String ma_bc) {
        String s = "begin ?:= " + getSysConst("FuncSchema") + "QLN_KMM.momay_tudong(" + "'" + psDsMaTB + "'" + ",'" + psLoai_TB + "'" + ",'" + ma_bc + "'" + ",'" + getUserVar("sys_dataschema") + "'" + ",'" + getUserVar("userID") + "'); end;";
        return s;

    }

    public String value_xoa_tbs_mmtd(
            String psDsMaTB,
            String ma_bc,
            String loai_tb) {
        String s = "begin ?:= " + getSysConst("FuncSchema") + "QLN_KMM.xoa_ds_mmtd(" + "'" + psDsMaTB + "'" + ",'" + ma_bc + "'" + ",'" + getUserVar("sys_dataschema") + "'," + "'" + getUserVar("userID") + "'); end;";
        return s;
    }

    public String value_them_tbs_mmtd(
            String psDVVT,
            String psMaBC,
            String psDsMaTB,
            String psHinhThucKM,
            String psDonviQL,
            String psChukyno) {
        String s = "begin ?:=" + getSysConst("FuncSchema") + "qln_kmm.them_tb_mmtd(" + "'" + psDVVT + "'," + "'" + psMaBC + "'," + "'" + psDsMaTB + "'," + "'" + psHinhThucKM + "'," + "'" + psDonviQL + "'," + "'" + psChukyno + "'," + "'" + getUserVar("sys_dataschema") + "'," + "'" + getUserVar("userID") + "'); end;";
        return s;
    }
    




    public String doc_layds_baono(
        String psKyhoadon,
        String psKieuno,
        String psMa_nv,
        String psMaT,
        String psNgayphat,
        String psngaybao,
       String pshantra,

        String psKhDB,
        String psKhDBT,
        String psKhUN,
        String psKhKMYC,
        String psLoaiKH,
        String psThangnotu,
        String psThangnoden,
        String psTiennotu,
        String psTiennoden,
          
        String psMaCQ,
        String psMa_kh,
        String psMa_tb,
        String psTenTT,
        String psDiachiTT,
        
        String psDonviql_id,
        String psMa_bc,
        String psxx,	
        
        String psPageNum,
        String psPageRec
    ){
    	return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_baono/ajax_thuebao" 
                + "&" + CesarCode.encode("Kyhoadon") + "=" +psKyhoadon 
                + "&" + CesarCode.encode("Kieuno") + "=" + psKieuno
                + "&" + CesarCode.encode("Ma_nv") + "=" + psMa_nv
                + "&" + CesarCode.encode("Ma_t") + "=" + psMaT
                + "&" + CesarCode.encode("Ngayphat") + "=" + psNgayphat
  	 + "&" + CesarCode.encode("ngaybao") + "=" + psngaybao
  	 + "&" + CesarCode.encode("hantra") + "=" + pshantra
                + "&" + CesarCode.encode("KhDB") + "=" + psKhDB
                + "&" + CesarCode.encode("KhDBT") + "=" + psKhDBT
                + "&" + CesarCode.encode("KhUN") + "=" + psKhUN
                + "&" + CesarCode.encode("KhKMYC") + "=" + psKhKMYC
                + "&" + CesarCode.encode("LoaiKH") + "=" + psLoaiKH
                + "&" + CesarCode.encode("Thangnotu") + "=" + psThangnotu
                + "&" + CesarCode.encode("Thangnoden") + "=" + psThangnoden
                + "&" + CesarCode.encode("Tiennotu") + "=" + psTiennotu
                + "&" + CesarCode.encode("Tiennoden") + "=" + psTiennoden
                
                 + "&" + CesarCode.encode("MaCQ") + "=" + psMaCQ
                + "&" + CesarCode.encode("Ma_kh") + "=" + psMa_kh
                + "&" + CesarCode.encode("Ma_tb") + "=" + psMa_tb
                + "&" + CesarCode.encode("TenTT") + "=" + psTenTT
                + "&" + CesarCode.encode("DiachiTT") + "=" + psDiachiTT
                
                + "&" + CesarCode.encode("donviql_id") + "=" + psDonviql_id
                + "&" + CesarCode.encode("ma_bc") + "=" + psMa_bc
	  + "&" + CesarCode.encode("xx") + "=" + psxx
                                
                + "&" + CesarCode.encode("PageNum") + "=" + psPageNum
                + "&" + CesarCode.encode("PageRec") + "=" + psPageRec;
    }












    public String doc_layds_kmm(
        String psKyhoadon,
        String psKieuno,
        String psMa_nv,
        String psMaT,
        String psNgayphat,
        String psKhDB,
        String psKhDBT,
        String psKhUN,
        String psKhKMYC,
        String psLoaiKH,
        String psThangnotu,
        String psThangnoden,
        String psTiennotu,
        String psTiennoden,
        
        String psDkTu,
        String psDkDen,
        String psPsTu,
        String psPsDen,
        String psTraTu,
        String psTraDen,
        
        String psMaCQ,
        String psMa_kh,
        String psMa_tb,
        String psTenTT,
        String psDiachiTT,
        
        String psDonviql_id,
        String psMa_bc,
        
        String psPageNum,
        String psPageRec
    ){
    	return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/ajax_thuebao" 
                + "&" + CesarCode.encode("Kyhoadon") + "=" +psKyhoadon 
                + "&" + CesarCode.encode("Kieuno") + "=" + psKieuno
                + "&" + CesarCode.encode("Ma_nv") + "=" + psMa_nv
                + "&" + CesarCode.encode("Ma_t") + "=" + psMaT
                + "&" + CesarCode.encode("Ngayphat") + "=" + psNgayphat
                + "&" + CesarCode.encode("KhDB") + "=" + psKhDB
                + "&" + CesarCode.encode("KhDBT") + "=" + psKhDBT
                + "&" + CesarCode.encode("KhUN") + "=" + psKhUN
                + "&" + CesarCode.encode("KhKMYC") + "=" + psKhKMYC
                + "&" + CesarCode.encode("LoaiKH") + "=" + psLoaiKH
                + "&" + CesarCode.encode("Thangnotu") + "=" + psThangnotu
                + "&" + CesarCode.encode("Thangnoden") + "=" + psThangnoden
                + "&" + CesarCode.encode("Tiennotu") + "=" + psTiennotu
                + "&" + CesarCode.encode("Tiennoden") + "=" + psTiennoden
                
                + "&" + CesarCode.encode("dk_tu") + "=" + psDkTu
                + "&" + CesarCode.encode("dk_den") + "=" + psDkDen
                + "&" + CesarCode.encode("ps_tu") + "=" + psPsTu
                + "&" + CesarCode.encode("ps_den") + "=" + psPsDen
                + "&" + CesarCode.encode("tra_tu") + "=" + psTraTu
                + "&" + CesarCode.encode("tra_den") + "=" + psTraDen
                
                + "&" + CesarCode.encode("MaCQ") + "=" + psMaCQ
                + "&" + CesarCode.encode("Ma_kh") + "=" + psMa_kh
                + "&" + CesarCode.encode("Ma_tb") + "=" + psMa_tb
                + "&" + CesarCode.encode("TenTT") + "=" + psTenTT
                + "&" + CesarCode.encode("DiachiTT") + "=" + psDiachiTT
                
                + "&" + CesarCode.encode("donviql_id") + "=" + psDonviql_id
                + "&" + CesarCode.encode("ma_bc") + "=" + psMa_bc
                                
                + "&" + CesarCode.encode("PageNum") + "=" + psPageNum
                + "&" + CesarCode.encode("PageRec") + "=" + psPageRec;
    }
    public String doc_layds_kmm(
            String psKmmID,
            String psKyhoadon,
            String psPageNum,
            String psPageRec
          ){
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/caclankhoa/ajax_dakhoa" 
                    + "&" + CesarCode.encode("Kyhoadon") + "=" +psKyhoadon 
                    + "&" + CesarCode.encode("kmm_id") + "=" + psKmmID
                    + "&" + CesarCode.encode("page_num") + "=" + psPageNum
                    + "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
    }
	public String them_khdb(
    	String psDsMaKH,
    	String psDsSomay,
    	String psKyhoadon,
    	String psLoai
    )	{
    	String return_="begin ?:=" + getSysConst("FuncSchema") + "QLN_KMM1.them_khdb("
    		 + "'" + psDsMaKH + "'" 
    		 + ",'" + psDsSomay + "'"
    		 + ",'" + psKyhoadon + "'"			 
    		 + ",'" + psLoai + "'"
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	System.out.println(return_);
    	return return_;
    }
    public String them_khdb(
    	String psLoaiKHDB,
    	String psKyhoadon,
    	String psMaKH,
    	String psMaTB,
    	String psTrangthai,
    	String psNguoiYC,
    	String psGhichu,
		String psLoai
    )
    {
    	String return_="begin ?:=" + getSysConst("FuncSchema") + "QLN_KMM1.them_khdb("
    		 + "'" + psLoaiKHDB + "'" 
    		 + ",'" + psKyhoadon + "'"
    		 + ",'" + psMaKH + "'"
    		 + ",'" + psMaTB + "'"
    		 + ",'" + psTrangthai + "'"
    		 + ",'" + psNguoiYC + "'"
    		 + ",'" + psGhichu + "'"
			 + ",'" + psLoai + "'"			 
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	System.out.println(return_);
    	return return_;
    }
	public String them_khdb(
    	String psLoaiKHDB,
    	String psKyhoadon,
    	String psMaKH,
    	String psMaTB,
    	String psTrangthai,
    	String psNguoiYC,
		String doituong,
		String Loaikh,
    	String psGhichu,
		String psLoai
    )
    {
    	String return_="begin ?:=" + getSysConst("FuncSchema") + "QLN_KMM1.them_khdb_2("
    		 + "'" + psLoaiKHDB + "'" 
    		 + ",'" + psKyhoadon + "'"
    		 + ",'" + psMaKH + "'"
    		 + ",'" + psMaTB + "'"
    		 + ",'" + psTrangthai + "'"
    		 + ",'" + psNguoiYC + "'"
			 + ",'" + doituong + "'"
			 + ",'" + Loaikh + "'"
    		 + ",'" + psGhichu + "'"
			 + ",'" + psLoai + "'"			 
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	System.out.println(return_);
    	return return_;
    }
    public String doc_layds_khdb(
            String psLoaiKHDB,
            String psKyhoadon,
            String psMa_kh,
            String psMa_tb,
			String psdonvi,
            String psTenTT,
            String psTiennotu,
            String psLoai,
			
			String psMaCQ,
			String psDoituong,
			String psLoaiKH,
			
            String psPageNum,
            String psPageRec
          ){
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/khdb/ajax_dskhdb" 
            + "&" + CesarCode.encode("loaikhdb") + "=" +psLoaiKHDB 
            + "&" + CesarCode.encode("kyhoadon") + "=" +psKyhoadon 
            + "&" + CesarCode.encode("ma_kh") + "=" +psMa_kh 
            + "&" + CesarCode.encode("ma_tb") + "=" +psMa_tb
			 + "&" + CesarCode.encode("donvi") + "=" +psdonvi
            + "&" + CesarCode.encode("ten_tt") + "=" +psTenTT 
            + "&" + CesarCode.encode("tiennotu") + "=" +psTiennotu
            + "&" + CesarCode.encode("loai") + "=" +psLoai 
			
			+ "&" + CesarCode.encode("ma_cq") + "=" +psMaCQ 
			+ "&" + CesarCode.encode("doituong") + "=" +psDoituong 
			+ "&" + CesarCode.encode("loaikh") + "=" +psLoaiKH 
			
            + "&" + CesarCode.encode("page_num") + "=" +psPageNum
            + "&" + CesarCode.encode("page_rec") + "=" +psPageRec;
	}
	public String xoa_khdb(
		String psMakh,
		String psDsSomay,
		String psschema,
		String userid
	)
	{
	String return_="begin ?:=" + getSysConst("FuncSchema") + "QLN_KMM1.xoa_khdb("
    		 + "'" + psMakh + "'"
    		 + ",'" + psDsSomay + "'" 
    		 + ",'" +psschema + "'" 
    		 + ",'" + userid + "'); end;";
    	System.out.println(return_);
    	return return_;
		
	}
	
	
	public String xoa_khdbb(
		String psMakh,
		String psDsSomay,
		String psschema,
		String userip,
		String userid
	)
	{

	String return_ = "begin ?:= admin_v2.QLTN_KMM.xoa_khdb("
    		 + "'" + psMakh + "'"
    		 + ",'" + psDsSomay + "'" 
    		 + ",'" +psschema + "'" 
			 + ",'" +userip + "'" 
    		 + ",'" + userid + "'); end;";
    	System.out.println(return_);
    	return return_;
		
	}
	
	public String rec_ttkhachhang(
		String psKyhoadon,
		String psMaKH,
		String psMaTB
	){
		String s="select kh.ma_kh,db.somay,kh.ten_tt,kh.diachi_tt from "
			+getUserVar("sys_dataschema")+"khachhangs_"+psKyhoadon+" kh, "
			+"(select max(somay) somay,ma_kh from "+getUserVar("sys_dataschema")+
				"danhba_dds_"+psKyhoadon;
		if (psMaTB!="") s+=" where '"+psMaTB+"'=somay";
		s+=" group by ma_kh) db where kh.ma_kh=db.ma_kh";
		if (psMaKH!="") s+=" and kh.ma_kh='"+psMaKH+"'";
		return s;
	}
	public String value_kmm_theodanhsach(
        String psKyhoadon,
        String psDsSomay,
        String psDsMaKH,
        String psDsSotien,
        String psGoiDen,
        String psGoiDi,
        String pskieukhoa
	){
		String return_="begin ?:=" + getSysConst("FuncSchema") + "QLN_KMM.kmm_theodanhsach("
    		 + "'" + psKyhoadon + "'" 
    		 + ",'" + psDsSomay + "'"
    		 + ",'" + psDsMaKH + "'"
    		 + ",'" + psDsSotien + "'"
    		 + ",'" + psGoiDen + "'"
    		 + ",'" + psGoiDi + "'"
    		 + ",'" + pskieukhoa + "'"
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	System.out.println(return_);
    	return return_;
	}
	


public String doc_khoamay_nocuoc_vtu
(
	String ckn_so,
  	String donvi,
   	String tientu,
  	String tienden,
   	String loaikh,
 	String soluong,
    String chieukhoa,
	String khdb,
	String khunc,
	String khmotam
)
{
	return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/nocuoc/ajax_thuebao_vtu" 
                    + "&" + CesarCode.encode("ckn_so") + "=" +ckn_so 
	 				+ "&" + CesarCode.encode("donvi") + "=" + donvi
                    + "&" + CesarCode.encode("tientu") + "=" + tientu
                    + "&" + CesarCode.encode("tienden") + "=" + tienden
                    + "&" + CesarCode.encode("loaikh_id") + "=" + loaikh
                    + "&" + CesarCode.encode("sobanghi") + "=" + soluong
	    			+ "&" + CesarCode.encode("chieukhoa") + "=" + chieukhoa
	    			+ "&" + CesarCode.encode("khdb") + "=" + khdb
	    			+ "&" + CesarCode.encode("khunc") + "=" + khunc
	    			+ "&" + CesarCode.encode("khmotam") + "=" + khmotam;
}
    

public String doc_khoamay_nocuoc_qni
(
	String ckn_so,
  	String donvi,
   	String tientu,
  	String tienden,
   	String loaikh,
 	String soluong,
    String chieukhoa,
	String khdb,
	String khunc,
	String khmotam
)
{
	return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/nocuoc/ajax_thuebao_qni" 
                    + "&" + CesarCode.encode("ckn_so") + "=" +ckn_so 
	 				+ "&" + CesarCode.encode("donvi") + "=" + donvi
                    + "&" + CesarCode.encode("tientu") + "=" + tientu
                    + "&" + CesarCode.encode("tienden") + "=" + tienden
                    + "&" + CesarCode.encode("loaikh_id") + "=" + loaikh
                    + "&" + CesarCode.encode("sobanghi") + "=" + soluong
	    			+ "&" + CesarCode.encode("chieukhoa") + "=" + chieukhoa
	    			+ "&" + CesarCode.encode("khdb") + "=" + khdb
	    			+ "&" + CesarCode.encode("khunc") + "=" + khunc
	    			+ "&" + CesarCode.encode("khmotam") + "=" + khmotam;
}	




public String doc_khoamay_nocuoc_pto
(
	String ckn_so,
  	String donvi,
   	String tientu,
  	String tienden,
   	String loaikh,
 	String soluong,
    String chieukhoa,
	String khdb,
	String khunc,
	String khmotam
)
{
	return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/nocuoc/ajax_thuebao_pto" 
                    + "&" + CesarCode.encode("ckn_so") + "=" +ckn_so 
	 				+ "&" + CesarCode.encode("donvi") + "=" + donvi
                    + "&" + CesarCode.encode("tientu") + "=" + tientu
                    + "&" + CesarCode.encode("tienden") + "=" + tienden
                    + "&" + CesarCode.encode("loaikh_id") + "=" + loaikh
                    + "&" + CesarCode.encode("sobanghi") + "=" + soluong
	    			+ "&" + CesarCode.encode("chieukhoa") + "=" + chieukhoa
	    			+ "&" + CesarCode.encode("khdb") + "=" + khdb
	    			+ "&" + CesarCode.encode("khunc") + "=" + khunc
	    			+ "&" + CesarCode.encode("khmotam") + "=" + khmotam;
}	



public String doc_khoamay_nocuoc_qnh(
            String ckn_so,
            String donvi,
            String tientu,
            String tienden,
            String loaikh,
            String soluong,
            String chieukhoa,
			String khdb,
			String khunc,
			String khmotam
          ){
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/nocuoc/ajax_thuebao_qnh" 
                    + "&" + CesarCode.encode("ckn_so") + "=" +ckn_so 
	 				+ "&" + CesarCode.encode("donvi") + "=" + donvi
                    + "&" + CesarCode.encode("tientu") + "=" + tientu
                    + "&" + CesarCode.encode("tienden") + "=" + tienden
                    + "&" + CesarCode.encode("loaikh_id") + "=" + loaikh
                    + "&" + CesarCode.encode("sobanghi") + "=" + soluong
	    			+ "&" + CesarCode.encode("chieukhoa") + "=" + chieukhoa
	    			+ "&" + CesarCode.encode("khdb") + "=" + khdb
	    			+ "&" + CesarCode.encode("khunc") + "=" + khunc
	    			+ "&" + CesarCode.encode("khmotam") + "=" + khmotam;
    }




	 public String doc_tracuu_kmm(
            String psKyhoadon,
            String psKMM,
            String psChieu,
            String psLankhoa,
            String psNgaykhoa,
            String psNguoikhoa,
            String psMaKH,
            String psMaTB,
            String psTenTT,
            String psPageNum,
            String psPageRec
          ){
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/tracuu/ajax_tracuu" 
                    + "&" + CesarCode.encode("chukyno") + "=" +psKyhoadon 
                    + "&" + CesarCode.encode("kmm") + "=" + psKMM
                    + "&" + CesarCode.encode("chieu") + "=" + psChieu
                    + "&" + CesarCode.encode("lankhoa") + "=" + psLankhoa
                    + "&" + CesarCode.encode("ngaykhoa") + "=" + psNgaykhoa
                    + "&" + CesarCode.encode("nguoi_th") + "=" + psNguoikhoa
                    + "&" + CesarCode.encode("ma_kh") + "=" + psMaKH
                    + "&" + CesarCode.encode("ma_tb") + "=" + psMaTB
                    + "&" + CesarCode.encode("ten_tt") + "=" + psTenTT
                    + "&" + CesarCode.encode("PageNum") + "=" + psPageNum
                    + "&" + CesarCode.encode("PageRec") + "=" + psPageRec;
    }

public String value_khoamay_nocuoc_qnh
(
	String ckn_so,
	String donvi,	
	String tientu,
	String tienden,
	String loaikh,
	String soluong,
	String chieukhoa,
	String khdb,
	String khunc,
	String khmotam
)
{	
	String return_="begin ?:=" + getSysConst("FuncSchema") + "QLN_KMM1.khoamay_nocuoc_qnh("
		 + "'" + ckn_so + "'" 
		 + ",'" + donvi+ "'" 
		 + ",'" + tientu + "'"
		 + ",'" + tienden + "'"
		 + ",'" + loaikh + "'"
		 + ",'" + khdb + "'"
		 + ",'" + khunc + "'"
		 + ",'" + khmotam + "'"
		 + ",'" + soluong + "'"
		 + ",'" + chieukhoa + "'"
		 + ",'" + getUserVar("sys_dataschema") + "'" 
		 + ",'" + getUserVar("userID") + "'); end;";
	System.out.println(return_);
	return return_;
}
public String value_khoamay_nocuoc_vtu
(
	String ckn_so,
    String donvi,	
   	String tientu,
   	String tienden,
  	String loaikh,
   	String soluong,
	String chieukhoa,
	String khdb,
	String khunc,
	String khmotam
)
{
	String return_="begin ?:=" + getSysConst("FuncSchema") + "QLN_KMM1.khoamay_nocuoc_vtu("
    		 + "'" + ckn_so + "'" 
			 + ",'" + donvi+ "'" 
    		 + ",'" + tientu + "'"
    		 + ",'" + tienden + "'"
    		 + ",'" + loaikh + "'"
    		 + ",'" + khdb + "'"
    		 + ",'" + khunc + "'"
    		 + ",'" + khmotam + "'"
	         + ",'" + soluong + "'"
		     + ",'" + chieukhoa + "'"
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	System.out.println(return_);
    	return return_;
}


public String value_khoamay_nocuoc_qni
(
	String ckn_so,
    String donvi,	
   	String tientu,
   	String tienden,
  	String loaikh,
   	String soluong,
	String chieukhoa,
	String khdb,
	String khunc,
	String khmotam
)
{
	String return_="begin ?:=" + getSysConst("FuncSchema") + "QLN_KMM1.khoamay_nocuoc_qni("
    		 + "'" + ckn_so + "'" 
			 + ",'" + donvi+ "'" 
    		 + ",'" + tientu + "'"
    		 + ",'" + tienden + "'"
    		 + ",'" + loaikh + "'"
    		 + ",'" + khdb + "'"
    		 + ",'" + khunc + "'"
    		 + ",'" + khmotam + "'"
	         + ",'" + soluong + "'"
		     + ",'" + chieukhoa + "'"
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	System.out.println(return_);
    	return return_;
}


public String value_khoamay_nocuoc_pto
(
	String ckn_so,
    String donvi,	
   	String tientu,
   	String tienden,
  	String loaikh,
   	String soluong,
	String chieukhoa,
	String khdb,
	String khunc,
	String khmotam
)
{
	String return_="begin ?:=" + getSysConst("FuncSchema") + "QLN_KMM1.khoamay_nocuoc_pto("
    		 + "'" + ckn_so + "'" 
			 + ",'" + donvi+ "'" 
    		 + ",'" + tientu + "'"
    		 + ",'" + tienden + "'"
    		 + ",'" + loaikh + "'"
    		 + ",'" + khdb + "'"
    		 + ",'" + khunc + "'"
    		 + ",'" + khmotam + "'"
	         + ",'" + soluong + "'"
		     + ",'" + chieukhoa + "'"
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	System.out.println(return_);
    	return return_;
}

	public String doc_khoamay_nocuoc(
            String ckn_so,
            String tientu,
            String tienden,
            String loaikh,
            String soluong,
			String chieukhoa,
			String khdb,
			String khunc,
			String khmotam
          ){
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/nocuoc/ajax_thuebao" 
                    + "&" + CesarCode.encode("ckn_so") + "=" +ckn_so 
                    + "&" + CesarCode.encode("tientu") + "=" + tientu
                    + "&" + CesarCode.encode("tienden") + "=" + tienden
                    + "&" + CesarCode.encode("loaikh_id") + "=" + loaikh
                    + "&" + CesarCode.encode("sobanghi") + "=" + soluong
					+ "&" + CesarCode.encode("chieukhoa") + "=" + chieukhoa
					+ "&" + CesarCode.encode("khdb") + "=" + khdb
					+ "&" + CesarCode.encode("khunc") + "=" + khunc
					+ "&" + CesarCode.encode("khmotam") + "=" + khmotam;
    }
	public String value_khoamay_nocuoc(
        String ckn_so,
        String tientu,
        String tienden,
        String loaikh,
        String soluong,
		String chieukhoa,
		String khdb,
		String khunc,
		String khmotam
	){
		String return_="begin ?:=" + getSysConst("FuncSchema") + "QLN_KMM1.khoamay_nocuoc("
		
    		 + "'" + ckn_so + "'" 
    		 + ",'" + tientu + "'"
    		 + ",'" + tienden + "'"
    		 + ",'" + loaikh + "'"
    		 + ",'" + khdb + "'"
    		 + ",'" + khunc + "'"
    		 + ",'" + khmotam + "'"
			 + ",'" + soluong + "'"
			 + ",'" + chieukhoa + "'"
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	System.out.println(return_);
    	return return_;
	}
	public String value_capnhat_tttb(
        String ngaykhoa,
		String chieukhoa
	){
		String return_="begin ?:=" + getSysConst("FuncSchema") + "QLN_KMM.capnhat_tttb_hientai("
    		 + "'" + ngaykhoa + "'" 
    		 + ",'" + chieukhoa + "'"
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	System.out.println(return_);
    	return return_;
	}
	public String doc_layds_ls_khoamay(
        String ngaykhoa,
		String chieukhoa
	){
		return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/nocuoc/tinhtrang/ajax_ls_km" 
            + "&" + CesarCode.encode("ngaykhoa") + "=" +ngaykhoa 
            + "&" + CesarCode.encode("chieukhoa") + "=" + chieukhoa;
	}
	public String value_quet_khoatudong(
			String psChukyno,
            String psDsTb) {
        String s = "begin ?:=" + getSysConst("FuncSchema") + "momay_gachno2(sysdate,'"+
        	psDsTb+"','"+
        	psChukyno+"','"+
        	getUserVar("sys_dataschema")+"','"+
        	getUserVar("userID") + "'); end;";
        return s;
    }
	public String value_quet_khoatudong1(
			String psChukyno) {
        String s = "begin ?:=" + getSysConst("FuncSchema") + "momay_gachno1('"+
        	psChukyno+"','"+
        	getUserVar("sys_dataschema")+"','"+
        	getUserVar("userID") + "'); end;";
        return s;
    }	
    //Khoa may HNI
    public String doc_khoamay_hni(
		String psthangno,
		
		String pstientu,
		String pstienden,
		
		String psdktu,
		String psdkden,
		
		String psThangnotu,
		String psThangnoden,
		
		String psloaikh_id,
		String pskhdb,
		String pskhunc,
		
		String psMa_nv,
		String psMaT,
		String pssongayphat,
		
		String psdonvi,
		String psma_bc,
		
		String pssobanghi,
		String pstaobang,
		
		String pslankhoamo,
		String pschieukhoa
	){
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/theotuyen/ajax_thuebao" 
            + "&" + CesarCode.encode("thangno") + "=" +psthangno 
            	
            + "&" + CesarCode.encode("tientu") + "=" + pstientu
            + "&" + CesarCode.encode("tienden") + "=" + pstienden
            
			+ "&" + CesarCode.encode("dktu") + "=" + psdktu
            + "&" + CesarCode.encode("dkden") + "=" + psdkden
			
			+ "&" + CesarCode.encode("thangnotu") + "=" + psThangnotu
            + "&" + CesarCode.encode("thangnoden") + "=" + psThangnoden
			
			+ "&" + CesarCode.encode("loaikh_id") + "=" + psloaikh_id
			+ "&" + CesarCode.encode("khdb") + "=" + pskhdb
			+ "&" + CesarCode.encode("khunc") + "=" + pskhunc
				
			+ "&" + CesarCode.encode("ma_nv") + "=" + psMa_nv
			+ "&" + CesarCode.encode("mat") + "=" + psMaT
			+ "&" + CesarCode.encode("songayphat") + "=" + pssongayphat
			
			+ "&" + CesarCode.encode("donvi") + "=" + psdonvi
			+ "&" + CesarCode.encode("ma_bc") + "=" + psma_bc
			
			+ "&" + CesarCode.encode("sobanghi") + "=" + pssobanghi
			+ "&" + CesarCode.encode("taobang") + "=" + pstaobang
				
			+ "&" + CesarCode.encode("lankhoamo") + "=" + pslankhoamo	
			+ "&" + CesarCode.encode("chieukhoa") + "=" + pschieukhoa;
    }
	public String value_khoamay_hni(
        String psthangno,
		
		String pstientu,
		String pstienden,
		
		String psdktu,
		String psdkden,
		
		String psThangnotu,
		String psThangnoden,
		
		String psloaikh_id,
		String pskhdb,
		String pskhunc,
		
		String psMa_nv,
		String psMaT,
		String pssongayphat,
		
		String psdonvi,
		String psma_bc,
		
		String pssobanghi,
		String pstaobang,
		
		String pslankhoamo,
		String pschieukhoa,
		String psdsma_tb
	){
		String return_="begin ?:=" + getSysConst("FuncSchema") + "khoamay_nocuoc_hni("
    		 + "'" + psthangno + "'" 
    		 	
    		 + ",'" + pstientu + "'"
    		 + ",'" + pstienden + "'"
			 
			 + ",'" + psdktu + "'"
    		 + ",'" + psdkden + "'"
			 
    		 + ",'" + psThangnotu + "'"
    		 + ",'" + psThangnoden + "'"
    		 	
    		 + ",'" + psloaikh_id + "'"
    		 + ",'" + pskhdb + "'"
			 + ",'" + pskhunc + "'"
			 	
			 + ",'" + psMa_nv + "'"
    		 + ",'" + psMaT + "'"
			 + ",'" + pssongayphat + "'"
			 	
			 + ",'" + psdonvi + "'"
    		 + ",'" + psma_bc + "'"
			 
			 + ",'" + pssobanghi + "'"
    		 + ",'" + pstaobang + "'"
			 
    		 + ",'" + pslankhoamo + "'"
			 + ",'" + pschieukhoa + "'"
			 + ",'" + psdsma_tb + "'"
			 
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	
    	return return_;
	}
	
	
	//Ham moi co them ma khach hang
	public String value_khoamay_hni(
        String psthangno,
		
		String pstientu,
		String pstienden,
		
		String psdktu,
		String psdkden,
		
		String psThangnotu,
		String psThangnoden,
		
		String psloaikh_id,
		String pskhdb,
		String pskhunc,
		
		String psMa_nv,
		String psMaT,
		String pssongayphat,
		
		String psdonvi,
		String psma_bc,
		
		String pssobanghi,
		String pstaobang,
		
		String pslankhoamo,
		String pschieukhoa,
		String psdsma_tb,
		String psma_kh
	){
		String return_="begin ?:=" + getSysConst("FuncSchema") + "khoamay_nocuoc_hni1("
    		 + "'" + psthangno + "'" 
    		 	
    		 + ",'" + pstientu + "'"
    		 + ",'" + pstienden + "'"
			 
			 + ",'" + psdktu + "'"
    		 + ",'" + psdkden + "'"
			 
    		 + ",'" + psThangnotu + "'"
    		 + ",'" + psThangnoden + "'"
    		 	
    		 + ",'" + psloaikh_id + "'"
    		 + ",'" + pskhdb + "'"
			 + ",'" + pskhunc + "'"
			 	
			 + ",'" + psMa_nv + "'"
    		 + ",'" + psMaT + "'"
			 + ",'" + pssongayphat + "'"
			 	
			 + ",'" + psdonvi + "'"
    		 + ",'" + psma_bc + "'"
			 
			 + ",'" + pssobanghi + "'"
    		 + ",'" + pstaobang + "'"
			 
    		 + ",'" + pslankhoamo + "'"
			 + ",'" + pschieukhoa + "'"
			 + ",'" + psdsma_tb + "'"
			 + ",'" + psma_kh + "'"
			 
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	
    	return return_;
	}
	public String doc_khoamay_hni(
		String psthangno,
		
		String pstientu,
		String pstienden,
		
		String psdktu,
		String psdkden,
		
		String psThangnotu,
		String psThangnoden,
		
		String psloaikh_id,
		String pskhdb,
		String pskhunc,
		
		String psMa_nv,
		String psMaT,
		String pssongayphat,
		
		String psdonvi,
		String psma_bc,
		
		String pssobanghi,
		String pstaobang,
		
		String pslankhoamo,
		String pschieukhoa,
		String psma_kh
	){
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/theotuyen/ajax_thuebao" 
            + "&" + CesarCode.encode("thangno") + "=" +psthangno 
            	
            + "&" + CesarCode.encode("tientu") + "=" + pstientu
            + "&" + CesarCode.encode("tienden") + "=" + pstienden
            
			+ "&" + CesarCode.encode("dktu") + "=" + psdktu
            + "&" + CesarCode.encode("dkden") + "=" + psdkden
			
			+ "&" + CesarCode.encode("thangnotu") + "=" + psThangnotu
            + "&" + CesarCode.encode("thangnoden") + "=" + psThangnoden
			
			+ "&" + CesarCode.encode("loaikh_id") + "=" + psloaikh_id
			+ "&" + CesarCode.encode("khdb") + "=" + pskhdb
			+ "&" + CesarCode.encode("khunc") + "=" + pskhunc
				
			+ "&" + CesarCode.encode("ma_nv") + "=" + psMa_nv
			+ "&" + CesarCode.encode("mat") + "=" + psMaT
			+ "&" + CesarCode.encode("songayphat") + "=" + pssongayphat
			
			+ "&" + CesarCode.encode("donvi") + "=" + psdonvi
			+ "&" + CesarCode.encode("ma_bc") + "=" + psma_bc
			
			+ "&" + CesarCode.encode("sobanghi") + "=" + pssobanghi
			+ "&" + CesarCode.encode("taobang") + "=" + pstaobang
				
			+ "&" + CesarCode.encode("lankhoamo") + "=" + pslankhoamo	
			+ "&" + CesarCode.encode("chieukhoa") + "=" + pschieukhoa
			+ "&" + CesarCode.encode("ma_kh") + "=" + psma_kh;
    }
	
	public String value_momay_thuebao(
			String psChukyno,
			String psDsTB,
			String psChieumo,
			String psKM,
			String psGhichu) {
        String s = "begin ?:= admin_v2.khoamo_nhancong('"+
        	psChukyno+"','"+
			psDsTB+"','"+
			psChieumo+"','"+
			psKM+"','"+
			psGhichu+"','"+
        	getUserVar("sys_dataschema")+"','"+
        	getUserVar("userID") + "'); end;";
        return s;
    }
	public String doc_ds_huy(
		String psthangno,		
		String pstientu,		
		String pssongayphat,		
		String psdonvi,
		String psma_bc,
		String psTrangthai,
		String pssobanghi,
		String pstaobang,		
		String pslankhoamo,
		String psdsma_tb
	){
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn_hni/qltn_huythuebao/ajax_thuebao" 
            + "&" + CesarCode.encode("thangno") + "=" +psthangno             	
            + "&" + CesarCode.encode("tientu") + "=" + pstientu           
			+ "&" + CesarCode.encode("songayphat") + "=" + pssongayphat			
			+ "&" + CesarCode.encode("donvi") + "=" + psdonvi
			+ "&" + CesarCode.encode("ma_bc") + "=" + psma_bc			
			+ "&" + CesarCode.encode("trangthai") + "=" + psTrangthai
			+ "&" + CesarCode.encode("taobang") + "=" + pstaobang				
			+ "&" + CesarCode.encode("lankhoamo") + "=" + pslankhoamo
			+ "&" + CesarCode.encode("ma_tb") + "=" +  psdsma_tb;
    }
	public String value_huytb_hni(
       	String psthangno,		
		String pstientu,		
		String pssongayphat,		
		String psdonvi,
		String psma_bc,
		String psTrangthai,
		String pssobanghi,
		String pstaobang,		
		String pslankhoamo,
		String psdsma_tb
	){
		String return_="begin ?:=" + getSysConst("FuncSchema") + "HUY_TB_TRASAU_HNI("
    		 + "'" + psthangno + "'"     		 	
    		 + ",'" + pstientu + "'"    		
			 + ",'" + pssongayphat + "'"			 	
			 + ",'" + psdonvi + "'"
    		 + ",'" + psma_bc + "'"
			 + ",'" + psTrangthai + "'"
			 + ",'" + pssobanghi + "'"
    		 + ",'" + pstaobang + "'"			 
    		 + ",'" + pslankhoamo + "'"	
				+ ",'" + psdsma_tb + "'"				 
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	
    	return return_;
	}
	
public String doc_layds_log(
            String psTuNgay,
            String psDenNgay
          ){
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/khdb/log/ajax_log_dskhdb" 
            + "&" + CesarCode.encode("tungay") + "=" +psTuNgay 
            + "&" + CesarCode.encode("denngay") + "=" +psDenNgay;
	}
	
}