package neo.qltn;
// FTPupload.java by Rowland http://www.home.comcast.net/~rowland3/
// Upload a file via FTP, using the JDK.

import java.io.*;
import java.net.*;

public class FTPupload {
    protected ftp cconn;
    public final String localfile;
    public final String targetfile;

    public FTPupload(String _host, String _user, String _password,
         String _localfile, String _targetfile) {
  cconn= new ftp(_host, _user, _password);
  localfile= _localfile;
  targetfile= _targetfile;
  doit();
    }
    public FTPupload(String _host, String _user, String _password, String _file) {
  cconn= new ftp(_host, _user, _password);
  localfile= _file;
  targetfile= _file;
  doit();
    }

    protected void doit() {
  try {		
      OutputStream os= cconn.openUploadStream(targetfile);
      FileInputStream is=  new FileInputStream(localfile);
      byte[] buf= new byte[16384];
      int c;
      while (true) {
    //System.out.print(".");
    c= is.read(buf);
    if (c<= 0)  break;
    //System.out.print("[");
    os.write(buf, 0, c);
    //System.out.print("]");
      }
      os.close();
      is.close();
      cconn.close(); // section 3.2.5 of RFC1738
  } catch (Exception E) {
      System.err.println(E.getMessage());
      E.printStackTrace();
  }
    }

    public static void main(String args[]) {
  // Usage: FTPupload host, user, password, file
  //	new FTPupload("190.10.10.86:8181", "luattd", "123456", "c:\\test.zip", "VPC/test.zip");  
    }

}