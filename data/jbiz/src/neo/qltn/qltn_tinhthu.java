package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class qltn_tinhthu extends NEOProcessEx 

{
  public void run() 
  {
    System.out.println("neo.qltn.qltn_tienmat was called");
  }
  
  public String laytt_thuebao(String func_schema, String data_schema  , String user_id,
							String somay) {
    return "begin ?:="+func_schema+"qltn_tracuu.laytt_thuebao_tinhthu("
			+"'"+data_schema+"'" 
			+",'"+user_id+"','" 
			+somay+"'); end;";
  }
   
  
   public String kiemtra_tb
							( String funcschema
							 ,String  ma_tb			        
                             ,String psschema
							 ,String psuserID							   
							  ) 
	{  
 	
 	  String  s=  "begin ?:= "+funcschema+"qltn_tracuu.kiemtra_tb("
 	   
		+"'"+psschema+"'"
		+",'"+psuserID+"'"
		+",'"+ma_tb+"'"
		+");"
        +"end;"
        ;
		   return s;	
	}
	
  
  
 public String tracuutinhthu(
		String schema,
		String ma_khachhang,
		String ma_thuebao,
		String ma_sophieu,
		String thutungay,
		String thudenngay,
		String ma_tinh,
		String buucuc,
		String ma_quay,
		String ma_thungan,
		String trangthai_id
		)
		{	
		System.out.println("neo.xln was called-Updated 16/07/2007");    
		
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tinhthu/ajax_tracuuthuho"
				+"&"+CesarCode.encode("ma_kh")+"="+ma_khachhang
				+"&"+CesarCode.encode("ma_tb")+"="+ma_thuebao
				+"&"+CesarCode.encode("ma_phieu")+"="+ma_sophieu
				+"&"+CesarCode.encode("tungay")+"="+thutungay
				+"&"+CesarCode.encode("denngay")+"="+thudenngay
				+"&"+CesarCode.encode("ma_tinh")+"="+ma_tinh
				+"&"+CesarCode.encode("buucuc")+"="+buucuc
				+"&"+CesarCode.encode("ma_quay")+"="+ma_quay
				+"&"+CesarCode.encode("ma_thungan")+"="+ma_thungan
				+"&"+CesarCode.encode("trangthai_id")+"="+trangthai_id
				; 	
		}
		
public String nhanphieutinhthu(      String funcSchema 
									,String pschukyno 
									,String psloaitien_id 
								    ,String psthungan_id 
									,String chungtu
								    ,String psma_khachhang 
									,String psma_thuebao
									,String ngaythu
								    ,String pitinh_id 
								    ,String pisotien 
								    ,String psma_quay 
								    ,String psghichu 
									,String psSchema
								    ,String psUserID 
								                    
							  ) 
							  
		  
							  
 { 
 
 String s =  "begin ?:= "+funcSchema+"qltn_tracuu.nhanphieuthuho("
        +"'"+pschukyno+"'"
		+","+psloaitien_id
        +",'"+psthungan_id+"'"
		+",'"+chungtu+"'"
        +",'"+psma_khachhang+"'"
		+",'"+psma_thuebao+"'"
		+",'"+ngaythu+"'"
        +","+pitinh_id
		+","+pisotien
		+",'"+psma_quay+"'"
        +",'"+psghichu+"'"
        +",'"+psSchema+"'"
        +",'"+psUserID+"'"
	    +"); "  
        +" end;" ;
		
    return s;

 }	

 
 
 
public String suaphieutinhthu(      String funcSchema 
									,String pschukyno 
									,String psloaitien_id 
								    ,String psthungan_id 
									,String chungtu
								    ,String psma_khachhang 
									,String psma_thuebao
									,String ngaythu
								    ,String pitinh_id 
								    ,String pisotien 
								    ,String psma_quay 
								    ,String psghichu 
									,String phieu_id
									,String psSchema
								    ,String psUserID 
								                    
							  ) 
							  
		  
							  
 { 
 
 String s =  "begin ?:= "+funcSchema+"qltn_tracuu.suaphieuthuho("
        +"'"+pschukyno+"'"
		+","+psloaitien_id
        +",'"+psthungan_id+"'"
		+",'"+chungtu+"'"
        +",'"+psma_khachhang+"'"
		+",'"+psma_thuebao+"'"
		+",'"+ngaythu+"'"
        +","+pitinh_id
		+","+pisotien
		+",'"+psma_quay+"'"
        +",'"+psghichu+"'"
		+","+phieu_id
        +",'"+psSchema+"'"
        +",'"+psUserID+"'"
	    +"); "  
        +" end;" ;
		
    return s;

 }	

 
	 public String capnhat_trangthai_phieuthu
	( 
	  String funcschema
	 ,String dsphieuthu 
	 ,String psschema
	 ,String psuserID							   
	  ) 
	{  
 	
 	  String  s=  "begin ?:= "+funcschema+"qltn_tracuu.capnhat_phieuthu("
 	  	+"'"+dsphieuthu+"'"
		+",'"+psschema+"'"
		+",'"+psuserID+"'"
		+");"
        +" end; " ;
		   return s;	
	}
	

  	public String dsphieuhuy(
		String schema,
		String ma_khachhang,
		String ma_thuebao,
		String ma_sophieu,
		String thutungay,
		String thudenngay,
		String ma_tinh,
		String buucuc,
		String ma_quay,
		String ma_thungan,
		String trangthai_id
		)
		{	
		System.out.println("neo.xln was called-Updated 16/07/2007");    
		
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tinhthu/ajax_phieuhuy"
				+"&"+CesarCode.encode("ma_kh")+"="+ma_khachhang
				+"&"+CesarCode.encode("ma_tb")+"="+ma_thuebao
				+"&"+CesarCode.encode("ma_phieu")+"="+ma_sophieu
				+"&"+CesarCode.encode("tungay")+"="+thutungay
				+"&"+CesarCode.encode("denngay")+"="+thudenngay
				+"&"+CesarCode.encode("ma_tinh")+"="+ma_tinh
				+"&"+CesarCode.encode("buucuc")+"="+buucuc
				+"&"+CesarCode.encode("ma_quay")+"="+ma_quay
				+"&"+CesarCode.encode("ma_thungan")+"="+ma_thungan
				+"&"+CesarCode.encode("trangthai_id")+"="+trangthai_id
				; 	
		}
		
//DucTT bo sung tu qltn_tienmat
   public String getdanhsach_thuhotinh(
		String schema,
		String ma_khachhang,
		String ma_thuebao,
		String ma_sophieu,
		String thutungay,
		String thudenngay,
		String ma_tinh,
		String buucuc,
		String trangthai_id,
		String ma_quay,
		String ma_thungan
	)
	{	
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tinhthu/ajax_tinhthuho"
				+"&"+CesarCode.encode("ma_kh")+"="+ma_khachhang
				+"&"+CesarCode.encode("ma_tb")+"="+ma_thuebao
				+"&"+CesarCode.encode("ma_phieu")+"="+ma_sophieu
				+"&"+CesarCode.encode("tungay")+"="+thutungay
				+"&"+CesarCode.encode("denngay")+"="+thudenngay
				+"&"+CesarCode.encode("ma_tinh")+"="+ma_tinh
				+"&"+CesarCode.encode("buucuc")+"="+buucuc
				+"&"+CesarCode.encode("trangthai_id")+"="+trangthai_id
				+"&"+CesarCode.encode("ma_quay")+"="+ma_quay
				+"&"+CesarCode.encode("ma_thungan")+"="+ma_thungan
				; 	
	}		
	public String xoaphieu_tinhthuho(String funcschema
    						 ,String psphieu_id
							 ,String psschema
							 ,String psuserID
    ) 
	{  
 	
		String  s=  "begin ?:= "+funcschema+"qltn_tracuu.xoaphieu_tinhthuho("
		+"'"+psphieu_id+"'"
		+",'"+psschema+"'"
	   	+",'"+psuserID+"'"
		+");"
        +"end;"
        ;
		return s;

	}
	public String getdanhsach_tracuuthuhotinh(
		String schema,
		String ma_khachhang,
		String ma_thuebao,
		String ma_sophieu,
		String thutungay,
		String thudenngay,
		String ma_tinh,
		String buucuc,
		String ma_quay,
		String ma_thungan,
		String trangthai_id
		)
		{	
		System.out.println("neo.xln was called-Updated 16/07/2007");    
		
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tinhthu/ajax_tracuutinhthuho"
				+"&"+CesarCode.encode("ma_kh")+"="+ma_khachhang
				+"&"+CesarCode.encode("ma_tb")+"="+ma_thuebao
				+"&"+CesarCode.encode("ma_phieu")+"="+ma_sophieu
				+"&"+CesarCode.encode("tungay")+"="+thutungay
				+"&"+CesarCode.encode("denngay")+"="+thudenngay
				+"&"+CesarCode.encode("ma_tinh")+"="+ma_tinh
				+"&"+CesarCode.encode("buucuc")+"="+buucuc
				+"&"+CesarCode.encode("ma_quay")+"="+ma_quay
				+"&"+CesarCode.encode("ma_thungan")+"="+ma_thungan
				+"&"+CesarCode.encode("trangthai_id")+"="+trangthai_id
				; 	
		}
}
