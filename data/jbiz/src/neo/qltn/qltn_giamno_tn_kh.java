package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;
public class qltn_giamno_tn_kh
extends NEOProcessEx
{
	public String laytt_khachhang(String schema)
	{
		return "/main?"+CesarCode.encode("configFile")+"=xln/DSQuanTheoTo/khachhangs_data";
	}
	public String laytt_seri
	       (String seri
           ,String quyen
           ,String soseri)
	{		
	 	return  "begin ?:="+getSysConst("FuncSchema")+"qltn.capnhat_seri('"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"','"+seri+"','"+quyen+"','"+soseri+"'); end;";	 
	}	
}
