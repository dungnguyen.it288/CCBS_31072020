package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class qltn_tracuu_new extends NEOProcessEx
{
	public void run() {
		System.out.println("neo.qltn.qltn_tienmat was called");
	}
	public NEOCommonData delInform_114( String userId, String userIp, String id) {
	
		String s= "declare n number; s varchar2(2000); out_ varchar2(200):='OK'; begin "+
			" begin "+
				
			" s:='delete from neo.informs "+
			"  where id="+id+"' ; execute immediate s; commit;"+	
			" exception when others then "+
            " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "+			
			" end; ? :=out_;  end;";		
		
		 String rs_ = null;
		 System.out.println("do do hoi "+s);
		try{
		
			rs_ = reqValue("CMDVOracleDS", s);		
		  
		} catch (Exception ex) {			
			ex.printStackTrace();
		}			
		return new NEOCommonData(rs_);
	}
	public NEOCommonData updInform_114(String userId, String userIp, String id, String status, 
			String title, String popupLink,String content) {
	
		String s= "declare n number; s varchar2(2000); out_ varchar2(200):='OK'; begin "+
			" begin "+				
			" s:='update neo.informs set title=''"+title+"'', content=''"+content+"'', status=''"+status+"'' "+
			 ", popup_link=''"+popupLink+"'', ngayth=sysdate, nguoith =''"+userId+"'', mayth=''"+userIp+"''"+		
			"  where id="+id+"'; execute immediate s ;  commit;"+	
			" exception when others then "+
            " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "+			
			" end; ? :=out_;  end;";		
		System.out.println(s);
		 String rs_ = null;
		try{
			rs_ = reqValue("CMDVOracleDS", s);		
		} catch (Exception ex) {			
			ex.printStackTrace();
		}			
		return new NEOCommonData(rs_);
	}
	public NEOCommonData addInform_114( String userId, String userIp, String dispOrder, String status, String typeid, String title, String icon, String popupLink, String content, String userids)  {
		
		String s= "declare n number; s varchar2(2000); out_ varchar2(200):='OK'; begin "+
			" begin "+
				"  select max(id)+1 into n from neo.informs;"+			
			" s:='insert into neo.informs (id, title,content,nguoith,mayth) "+
			" values('||n||',''"+title+"'',''"+content+"'',''"+userId+"'',''"+userIp+"'')' ; execute immediate s; commit;"+	
			" exception when others then "+
            " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "+			
			" end; ? :=out_;  end;";		
		
		 String rs_ = null;
		try{
			rs_ = reqValue("CMDVOracleDS", s);		
		} catch (Exception ex) {			
			ex.printStackTrace();
		}			
		return new NEOCommonData(rs_);
	}
	public NEOCommonData getUrlDsInform114(String status, String matinh, String typeId) {
		String s = "/main?"+CesarCode.encode("configFile")+"=qltn/dulieu/inform/ajax_ds_inform_114"
			+"&"+CesarCode.encode("status") + "=" + status
			+"&"+CesarCode.encode("matinh") + "=" + matinh
			+"&"+CesarCode.encode("typeId") + "=" + typeId;
		
	    NEOExecInfo nei_ = new NEOExecInfo(s);
	    return new NEOCommonData(nei_);
	} 
 	public String luotthanhtoan_ds(
		String schema,
		String chukyno,
		String ma_nv,
		String dvql_id,
		String capquanly,
		String ma_bc)
	{
		System.out.println("neo.xln was called-Updated 16/07/2007");
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/common/qltn/tracuu/frmLuotThanhToan_ajax"
			+"&"+CesarCode.encode("chukyno")+"="+chukyno
			+"&"+CesarCode.encode("ma_nv")+"="+ma_nv
			+"&"+CesarCode.encode("donviql_id")+"="+dvql_id
			+"&"+CesarCode.encode("capquanly")+"="+capquanly
			+"&"+CesarCode.encode("ma_bc")+"="+ma_bc;
	}
	
	
	
   	public String luotthanhtoan_luottra(
		String schema,
		String chukyno,
		String ma_nv,
		String tungay,
		String denngay,
		String luotthu,
		String dvql_id,
		String capquanly,
		String ma_bc)
	{
		System.out.println("neo.xln was called-Updated 16/07/2007");
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/common/qltn/tracuu/frmLuotThanhToan_ajax1"
			+"&"+CesarCode.encode("chukyno")+"="+chukyno
			+"&"+CesarCode.encode("ma_nv")+"="+ma_nv
			+"&"+CesarCode.encode("ngay_tt")+"="+tungay
			+"&"+CesarCode.encode("DEN_NGAY_TT")+"="+denngay
			+"&"+CesarCode.encode("donviql_id")+"="+dvql_id
			+"&"+CesarCode.encode("capquanly")+"="+capquanly
			+"&"+CesarCode.encode("ma_bc")+"="+ma_bc
			+"&"+CesarCode.encode("luotthanhtoan")+"="+luotthu;
	}
 	public String value_capnhat_thuho(
		String pskyhoadon,
		String psphieu_id,
		String psloaitien_id,
		String pshttt_id,
		String psma_kh,
		String psma_tb,
		String pstenkh,
		String psdiachi,
		String psms_thue,
		String pssotien,
		String pschukyno,
		String psma_tn,
		String psmaquay,
		String pstinh_id,
		String psngaythu,
		String psghichu
	)
	{
		return "begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.capnhat_thuho('"
			+pskyhoadon+"','"
			+psphieu_id+"','"
			+psloaitien_id+"','"
			+pshttt_id+"','"
			+psma_kh+"','"
			+psma_tb+"','"
			+pstenkh+"','"
			+psdiachi+"','"
			+psms_thue+"','"
			+pssotien+"','"
			+pschukyno+"','"
			+psma_tn+"','"
			+psmaquay+"','"
			+pstinh_id+"',to_date('"
			+psngaythu+"','DD/MM/YYYY'),'"
			+psghichu+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
	}
   	public String notonghop_dssomay(
		String schema,
		String chukyno,
		String tukhoa,
		String ma_khachhang,
		String loaitien)
	{
		System.out.println("neo.xln was called-Updated 16/07/2007");
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/common/ajax_tra_somay"
			+"&"+CesarCode.encode("chukyno")+"="+chukyno
			+"&"+CesarCode.encode("makh")+"="+ma_khachhang
			+"&"+CesarCode.encode("matb")+"="+tukhoa
			+"&"+CesarCode.encode("loaitien")+"="+loaitien;
	}
	public String notonghop_ttnokhoanmuc(
		String schema,
		String chukyno,
		String tukhoa,
		String ma_khachhang,
		String loaitien)
	{
		System.out.println("neo.xln was called-Updated 16/07/2007");
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/common/ajax_noth_khoanmuc"
			+"&"+CesarCode.encode("chukyno")+"="+chukyno
			+"&"+CesarCode.encode("makh")+"="+ma_khachhang
			+"&"+CesarCode.encode("matb")+"="+tukhoa
			+"&"+CesarCode.encode("loaitien")+"="+loaitien;
	}
	public String notonghop_layttkhach(
		String schema,
		String userinput,
		String chukyno)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/common/ajax_laytt_kh_tracuu_no_tonghop"
			+"&"+CesarCode.encode("chukyno")+"="+chukyno
			+"&"+CesarCode.encode("userinput")+"="+userinput;
	}
	public String notonghop_notheotb(
		String schema,
		String chukyno,
		String tukhoa,
		String ma_khachhang,
		String thieu_db)
	{
		System.out.println("neo.xln was called-Updated 16/07/2007");
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/common/ajax_no_somay"
			+"&"+CesarCode.encode("chukyno")+"="+chukyno
			+"&"+CesarCode.encode("makh")+"="+ma_khachhang
			+"&"+CesarCode.encode("matb")+"="+tukhoa
			+"&"+CesarCode.encode("thieu_db")+"="+thieu_db;
	}
	public String notonghop_dieuchinh(
		String schema,
		String chukyno,
		String tukhoa,
		String ma_khachhang)
	{
		System.out.println("neo.xln was called-Updated 16/07/2007");
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/common/ajax_noth_dieuchinh"
			+"&"+CesarCode.encode("chukyno")+"="+chukyno
			+"&"+CesarCode.encode("makh")+"="+ma_khachhang
			+"&"+CesarCode.encode("matb")+"="+tukhoa;
	}
	public String value_xoa_thuho(
		String pskyhoadon,
		String psdsphieu_id
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_tamthu.xoa_thuho('"
			+pskyhoadon+"','"
			+psdsphieu_id+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String doc_phieutra(
		String pstuKyHD,
		String psdenKyHD,
		String psdsphieu_id,
		String psma_kh,
		String psma_tb,
		String psten_tt,
		String psdiachi_tt,
		String pstuNgayTT,
		String psdenNgayTT,
		String psLuotTT,
		String psMaNV,
		String psMaTN,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat_test/phieutras/ajax_phieutra"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("denkyhd")+"="+psdenKyHD
			+"&"+CesarCode.encode("phieus")+"="+psdsphieu_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("ten_tt")+"="+psten_tt
			+"&"+CesarCode.encode("diachi_tt")+"="+psdiachi_tt
			+"&"+CesarCode.encode("tungay")+"="+pstuNgayTT
			+"&"+CesarCode.encode("denngay")+"="+psdenNgayTT
			+"&"+CesarCode.encode("luottt")+"="+psLuotTT
			+"&"+CesarCode.encode("ma_nv")+"="+psMaNV
			+"&"+CesarCode.encode("matn")+"="+psMaTN
			+"&"+CesarCode.encode("page_num")+"="+psPageNum
			+"&"+CesarCode.encode("page_rec")+"="+psPageRec;
	}

public String doc_phieutra_nganhang(
		String pstuKyHD,
		String psdenKyHD,
		String psdsphieu_id,

		String psma_kh,
		String psma_tb,

		String pshttt,
		String psinhd,
		String psngayct,
		String psten_tt,



		String psdiachi_tt,
		String pstuNgayTT,
		String psdenNgayTT,
		String psLuotTT,

		String psdonviql,
		String psmabc,
		String pschonngay,

		String psMaNV,
		String psMaTN,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn_hni/qltn_chuyenkhoan/phieutras/ajax_phieutra"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("denkyhd")+"="+psdenKyHD
			+"&"+CesarCode.encode("phieus")+"="+psdsphieu_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("httt")+"="+pshttt
			+"&"+CesarCode.encode("inhd")+"="+psinhd
			+"&"+CesarCode.encode("ngayct")+"="+psngayct
			+"&"+CesarCode.encode("ten_tt")+"="+psten_tt
			+"&"+CesarCode.encode("diachi_tt")+"="+psdiachi_tt
			+"&"+CesarCode.encode("tungay")+"="+pstuNgayTT
			+"&"+CesarCode.encode("denngay")+"="+psdenNgayTT
			+"&"+CesarCode.encode("luottt")+"="+psLuotTT

			+"&"+CesarCode.encode("donviql")+"="+psdonviql
			+"&"+CesarCode.encode("ma_bc")+"="+psmabc
			+"&"+CesarCode.encode("chonngay")+"="+pschonngay

			+"&"+CesarCode.encode("ma_nv")+"="+psMaNV
			+"&"+CesarCode.encode("matn")+"="+psMaTN
			+"&"+CesarCode.encode("page_num")+"="+psPageNum
			+"&"+CesarCode.encode("page_rec")+"="+psPageRec;
	}


	public String doc_lichsuno(
		String pstuKyHD,
		String psdenKyHD,
		String psma_kh,
		String psma_tb
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/lichsu_notras/ls_no"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("denkyhd")+"="+psdenKyHD
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb;
	}
	public String doc_lichsutra(
		String pstuKyHD,
		String psdenKyHD,
		String psma_kh,
		String psma_tb
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/lichsu_notras/ls_tra"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("denkyhd")+"="+psdenKyHD
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb;
	}
	public String doc_lichsuno1(
		String pstuKyHD,
		String psdenKyHD,
		String psma_kh,
		String psma_tb
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/lichsu_notras/hcm/ls_no"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("denkyhd")+"="+psdenKyHD
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb;
	}
	public String doc_lichsutra1(
		String pstuKyHD,
		String psdenKyHD,
		String psma_kh,
		String psma_tb
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/lichsu_notras/hcm/ls_tra"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("denkyhd")+"="+psdenKyHD
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb;
	}
	public String doc_phieutra2(
		String psKyhoadon,
		String psma_kh,
		String psma_tb,
		String pstuNgayTT,
		String psdenNgayTT,
		String psten_tt,
		String loaiphieu,

		String psDonvi,
		String psMaBC,
		String psMaquay,
		String psMaTN,
		String psHTTT,
		String psChungtu,

		String psTientu,
		String psTienden,
		String psPhieuIDTu,
		String psPhieuIDDen,
		String psSophieuTu,
		String psSophieuDen,
		String psSoseriTu,
		String pssoseriDen,
		String psGomHDTu,
		String psGomHDDen,
		String psSeri,
		String psChuain,
		String psOrder,

		String psPageNum,
		String psPageRec
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/phieuthus/ajax_phieutra_unc"
			+"&"+CesarCode.encode("kyhoadon")+"="+psKyhoadon
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("tungay")+"="+pstuNgayTT
			+"&"+CesarCode.encode("denngay")+"="+psdenNgayTT
			+"&"+CesarCode.encode("ten_tt")+"="+psten_tt
			+"&"+CesarCode.encode("loaiphieu")+"="+loaiphieu

			+"&"+CesarCode.encode("donvi")+"="+psDonvi
			+"&"+CesarCode.encode("ma_bc")+"="+psMaBC
			+"&"+CesarCode.encode("maquay")+"="+psMaquay
			+"&"+CesarCode.encode("ma_tn")+"="+psMaTN
			+"&"+CesarCode.encode("httt")+"="+psHTTT
			+"&"+CesarCode.encode("chungtu")+"="+psChungtu

			+"&"+CesarCode.encode("tientu")+"="+psTientu
			+"&"+CesarCode.encode("tienden")+"="+psTienden
			+"&"+CesarCode.encode("phieuidtu")+"="+psPhieuIDTu
			+"&"+CesarCode.encode("phieuidden")+"="+psPhieuIDDen
			+"&"+CesarCode.encode("sophieutu")+"="+psSophieuTu
			+"&"+CesarCode.encode("sophieuden")+"="+psSophieuDen
			+"&"+CesarCode.encode("soseritu")+"="+psSoseriTu
			+"&"+CesarCode.encode("soseriden")+"="+pssoseriDen
			+"&"+CesarCode.encode("gomhdtu")+"="+psGomHDTu
			+"&"+CesarCode.encode("gomhdden")+"="+psGomHDDen
			+"&"+CesarCode.encode("seri")+"="+psSeri
			+"&"+CesarCode.encode("chuain")+"="+psChuain
			+"&"+CesarCode.encode("order")+"="+psOrder

			+"&"+CesarCode.encode("page_num")+"="+psPageNum
			+"&"+CesarCode.encode("page_rec")+"="+psPageRec;
	}
	public String doc_buucuc(String psdonviql_id){
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/connos/ajax_buucuc"
			+"&"+CesarCode.encode("donviql_id")+"="+psdonviql_id;
	}
	public String doc_nhanvientc(String psbuucuc){
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/connos/ajax_nhanvientc"
			+"&"+CesarCode.encode("ma_bc")+"="+psbuucuc;
	}
	public String doc_tuyenthu(String psma_nv){
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/connos/ajax_tuyenthu"
			+"&"+CesarCode.encode("ma_nv")+"="+psma_nv;
	}
	public String doc_conno(
		String pskyhoadon,
		String psdonviql_id,
		String psma_bc,
		String psma_nv,
		String psma_t,
		String psma_kh,
		String psma_tb,
		String psten_tt,
		String psdiachi_tt,
		String pskieuno,
		String pstientu,
		String pstienden,
		String pspage_num,
		String pspage_rec
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/connos/ajax_conno"
			+"&"+CesarCode.encode("kyhoadon")+"="+pskyhoadon
			+"&"+CesarCode.encode("donviql_id")+"="+psdonviql_id
			+"&"+CesarCode.encode("ma_bc")+"="+psma_bc
			+"&"+CesarCode.encode("ma_nv")+"="+psma_nv
			+"&"+CesarCode.encode("ma_t")+"="+psma_t
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("ten_tt")+"="+psten_tt
			+"&"+CesarCode.encode("diachi_tt")+"="+psdiachi_tt
			+"&"+CesarCode.encode("kieuno")+"="+pskieuno
			+"&"+CesarCode.encode("tientu")+"="+pstientu
			+"&"+CesarCode.encode("tienden")+"="+pstienden
			+"&"+CesarCode.encode("page_rec")+"="+pspage_rec
			+"&"+CesarCode.encode("page_num")+"="+pspage_num;
	}
	public String doc_conno_new(
		String pskyhoadon,		
		String psma_kh,
		String psma_tb,		
		String pskieuno,
		String pstientu,
		String pstienden,
		String pspage_num,
		String pspage_rec
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/connos/ajax_conno_new"
			+"&"+CesarCode.encode("kyhoadon")+"="+pskyhoadon			
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb			
			+"&"+CesarCode.encode("kieuno")+"="+pskieuno
			+"&"+CesarCode.encode("tientu")+"="+pstientu
			+"&"+CesarCode.encode("tienden")+"="+pstienden
			+"&"+CesarCode.encode("page_rec")+"="+pspage_rec
			+"&"+CesarCode.encode("page_num")+"="+pspage_num;
	}
	
	public String doc_tonghop(
		String pstuKyHD,
        String psdenKyHD,
        String psma_nv,
        String psma_t,
        String psma_cq,
        String psma_kh,
        String psma_tb,
        String psten_tt,
        String psdiachi_tt,
        String pspage_num,
        String pspage_rec
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/lichsu_notras/ajax_tonghop"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("denkyhd")+"="+psdenKyHD
			+"&"+CesarCode.encode("ma_nv")+"="+psma_nv
			+"&"+CesarCode.encode("ma_t")+"="+psma_t
			+"&"+CesarCode.encode("ma_cq")+"="+psma_t
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("ten_tt")+"="+psten_tt
			+"&"+CesarCode.encode("diachi_tt")+"="+psdiachi_tt
			+"&"+CesarCode.encode("page_rec")+"="+pspage_rec
			+"&"+CesarCode.encode("page_num")+"="+pspage_num;
	}
	public String doc_no(
		String pstuKyHD,
        String psdenKyHD,
        String psma_nv,
        String psma_t,
        String psma_cq,
        String psma_kh,
        String psma_tb,
        String psten_tt,
        String psdiachi_tt,
        String pspage_num,
        String pspage_rec
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/lichsu_notras/ajax_no"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("denkyhd")+"="+psdenKyHD
			+"&"+CesarCode.encode("ma_nv")+"="+psma_nv
			+"&"+CesarCode.encode("ma_t")+"="+psma_t
			+"&"+CesarCode.encode("ma_cq")+"="+psma_t
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("ten_tt")+"="+psten_tt
			+"&"+CesarCode.encode("diachi_tt")+"="+psdiachi_tt
			+"&"+CesarCode.encode("page_rec")+"="+pspage_rec
			+"&"+CesarCode.encode("page_num")+"="+pspage_num;
	}
	public String doc_phieudc(
		String pstuKyHD,
		String psdenKyHD,
		String psma_kh,
		String psma_tb,
		String psten_tt,
		String psdiachi_tt,
		String pstuNgayTT,
		String psdenNgayTT,
		String psLydo,
		String psMaTN,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/phieudcs/ajax_phieudc"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("denkyhd")+"="+psdenKyHD
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("ten_tt")+"="+psten_tt
			+"&"+CesarCode.encode("diachi_tt")+"="+psdiachi_tt
			+"&"+CesarCode.encode("tungay")+"="+pstuNgayTT
			+"&"+CesarCode.encode("denngay")+"="+psdenNgayTT
			+"&"+CesarCode.encode("lydo")+"="+psLydo
			+"&"+CesarCode.encode("ma_tn")+"="+psMaTN
			+"&"+CesarCode.encode("page_num")+"="+psPageNum
			+"&"+CesarCode.encode("page_rec")+"="+psPageRec;
	}
	public String doc_chuyentuyen(
		String psMaNV,
		String psMaT,
		String psMaKH,
		String psCbo
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_chuyentuyen/ajax_chuyentuyen"
			+"&"+CesarCode.encode("ma_nv")+"="+psMaNV
			+"&"+CesarCode.encode("ma_t")+"="+psMaT
			+"&"+CesarCode.encode("ma_kh")+"="+psMaKH
			+"&"+CesarCode.encode("cbo")+"="+psCbo;
	}
	public String doc_thuho(
		String pskyhoadon,
		String psTungay,
		String psdenngay,
		String psma_tb,
		String psma_bc,
		String psmaquay,
		String psma_tn,
		String pstinh,
		String pstientu,
		String pstienden,
		String psphieuidtu,
		String psphieuidden,
		String pssoseritu,
		String pssoseriden,
		String pstrangthai,
		String psloai,
		String pspagenum,
		String pspagerec
	){
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/thuhos/ajax_phieuthu"
			+"&"+CesarCode.encode("kyhoadon")+"="+pskyhoadon
			+"&"+CesarCode.encode("tungay")+"="+psTungay
			+"&"+CesarCode.encode("denngay")+"="+psdenngay
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb

			+"&"+CesarCode.encode("ma_bc")+"="+psma_bc
			+"&"+CesarCode.encode("maquay")+"="+psmaquay
			+"&"+CesarCode.encode("ma_tn")+"="+psma_tn
			+"&"+CesarCode.encode("tinh")+"="+pstinh

			+"&"+CesarCode.encode("tientu")+"="+pstientu
			+"&"+CesarCode.encode("tienden")+"="+pstienden
			+"&"+CesarCode.encode("phieuidtu")+"="+psphieuidtu
			+"&"+CesarCode.encode("phieuidden")+"="+psphieuidden
			+"&"+CesarCode.encode("soseritu")+"="+pssoseritu
			+"&"+CesarCode.encode("soseriden")+"="+pssoseriden
			+"&"+CesarCode.encode("trangthai")+"="+pstrangthai
			+"&"+CesarCode.encode("loai")+"="+psloai

			+"&"+CesarCode.encode("page_num")+"="+pspagenum
			+"&"+CesarCode.encode("page_rec")+"="+pspagerec;
	}
	public String doc_cuocnong(
		String psKyhoadon,
		String pstuNgayTT,
		String psdenNgayTT,
		String psma_kh,
		String psma_tb,

		String psMaBC,
		String psMaquay,
		String psMaTN,

		String psTientu,
		String psTienden,
		String psPhieuIDTu,
		String psPhieuIDDen,
		String psSoseriTu,
		String pssoseriDen,
		String psTrangthai,

		String psPageNum,
		String psPageRec
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/cuocnongs/ajax_ds_cuocnong"
			+"&"+CesarCode.encode("kyhoadon")+"="+psKyhoadon
			+"&"+CesarCode.encode("tungay")+"="+pstuNgayTT
			+"&"+CesarCode.encode("denngay")+"="+psdenNgayTT
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb

			+"&"+CesarCode.encode("ma_bc")+"="+psMaBC
			+"&"+CesarCode.encode("maquay")+"="+psMaquay
			+"&"+CesarCode.encode("ma_tn")+"="+psMaTN

			+"&"+CesarCode.encode("tientu")+"="+psTientu
			+"&"+CesarCode.encode("tienden")+"="+psTienden
			+"&"+CesarCode.encode("phieuidtu")+"="+psPhieuIDTu
			+"&"+CesarCode.encode("phieuidden")+"="+psPhieuIDDen
			+"&"+CesarCode.encode("soseritu")+"="+psSoseriTu
			+"&"+CesarCode.encode("soseriden")+"="+pssoseriDen
			+"&"+CesarCode.encode("trangthai")+"="+psTrangthai

			+"&"+CesarCode.encode("page_num")+"="+psPageNum
			+"&"+CesarCode.encode("page_rec")+"="+psPageRec;
	}

	public String doc_notra(
		String pschukyno,
		String psma_kh,
		String pskieu_ht,
		String psma_tb
	)
	{
		String s= "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tracuu/notra/ajax_notra"
				+"&"+CesarCode.encode("chukyno")+"="+pschukyno
				+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
				+"&"+CesarCode.encode("kieu_ht")+"="+pskieu_ht
				+"&"+CesarCode.encode("ma_tb")+"="+psma_tb;
		//System.out.println(s);
		return s;
	}

	//thiennt
public String doc_notra_ttkh_ma_tb(
		String pschukyno,
		String psma_kh,
		String pskieu_ht,
		String psma_tb
	)
	{
		String s= "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tracuuttkh/notra_new/ajax_notra_tb"
				+"&"+CesarCode.encode("chukyno")+"="+pschukyno
				+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
				+"&"+CesarCode.encode("kieu_ht")+"="+pskieu_ht
				+"&"+CesarCode.encode("ma_tb")+"="+psma_tb;
		System.out.println(s);
		return s;
	}


	public String doc_notra_ttkh(
		String pschukyno,
		String psma_kh,
		String pskieu_ht,
		String psma_tb
	)
	{
		String s= "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tracuuttkh/notra/ajax_notra"
				+"&"+CesarCode.encode("chukyno")+"="+pschukyno
				+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
				+"&"+CesarCode.encode("kieu_ht")+"="+pskieu_ht
				+"&"+CesarCode.encode("ma_tb")+"="+psma_tb;
		System.out.println(s);
		return s;
	}
//bo xung cho tra cuu no tong hop	
public String doc_notra_ttkh_ma_tb1(
		String pschukyno,
		String psma_kh,
		String pskieu_ht,
		String psma_tb
	)
	{
		String s= "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tracuuttkh/notra_new/ajax_notra_tb"
				+"&"+CesarCode.encode("chukyno")+"="+pschukyno
				+"&"+CesarCode.encode("userinput")+"="+psma_kh
				+"&"+CesarCode.encode("kieu_ht")+"="+pskieu_ht
				+"&"+CesarCode.encode("ma_tb")+"="+psma_tb;
		System.out.println(s);
		return s;
	}

	public String doc_notra_ttkh1(
		String pschukyno,
		String psma_kh,
		String pskieu_ht,
		String psma_tb
	)
	{
		String s= "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tracuuttkh/notra/ajax_notra"
				+"&"+CesarCode.encode("chukyno")+"="+pschukyno
				+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
				+"&"+CesarCode.encode("kieu_ht")+"="+pskieu_ht
				+"&"+CesarCode.encode("ma_tb")+"="+psma_tb;
		System.out.println(s);
		return s;
	}


//INFORM	
	 public String getUrlDsInform() {
		return "/main?"+CesarCode.encode("configFile")+"=qltn/dulieu/inform/ajax_ds_inform";
	}
	 
	 public NEOCommonData getUrlDsInform(String status, String matinh, String typeId) {
		String s = "/main?"+CesarCode.encode("configFile")+"=qltn/dulieu/inform/ajax_ds_inform"
			+"&"+CesarCode.encode("status") + "=" + status
			+"&"+CesarCode.encode("matinh") + "=" + matinh
			+"&"+CesarCode.encode("typeId") + "=" + typeId;
		
	    NEOExecInfo nei_ = new NEOExecInfo(s);
	    return new NEOCommonData(nei_);
	}
	 
	public String addInform(String funcSchema, String userId, String userIp, String dispOrder, String status, String matinh, String typeid, 
				String title, String icon, String popupLink, String popupProp, String command, String content, String userids, String display_date) {
		
		String s= "begin ? := admin_v2.info.add_inform('"
				+ userId + "',"
				+"'"+ userIp+"',"
				+"'"+ CesarCode.decode(funcSchema)+"',"
			   	+"'"+ dispOrder +"',"	   
				+"'"+ status +"',"	
				+"'"+ matinh +"',"		
				+"'"+ typeid +"',"
				+"'"+ title +"',"		
				+"'"+ icon +"',"		
				+"'"+ popupLink +"',"
				+"'"+ popupProp +"',"
				+"'"+ command +"',"
				+"'"+ content +"',"
				+"'"+ userids +"',"
				+"'"+ display_date +"'"
		        +");"
		        +"end;";
		return s;
	}
		public String addInform(String funcSchema, String userId, String userIp, String dispOrder, String status, String matinh, String typeid, 
				String title, String icon, String popupLink, String popupProp, String command, String content, String userids, String upload_id,String loginfile) {
		
		String s= "begin ? := "+CesarCode.decode(funcSchema)+"info.add_inform('"
				+ userId + "',"
				+"'"+ userIp+"',"
				+"'"+ CesarCode.decode(funcSchema)+"',"
			   	+"'"+ dispOrder +"',"	   
				+"'"+ status +"',"	
				+"'"+ matinh +"',"		
				+"'"+ typeid +"',"
				+"'"+ title +"',"		
				+"'"+ icon +"',"		
				+"'"+ popupLink +"',"
				+"'"+ popupProp +"',"
				+"'"+ command +"',"
				+"'"+ content +"',"
				+"'"+ userids +"',"
				+"'"+ upload_id +"',"
				+"'"+ loginfile +"'"
		        +");"
		        +"end;";
		return s;
	}
	
	public String updInform(String funcSchema, String userId, String userIp, String id, String dispOrder, String status, String matinh, String typeid, 
			String title, String icon, String popupLink, String popupProp, String command, String content, String userids,String display_date) {
	
		String s= "begin ? := admin_v2.info.upd_inform('"
				+ userId + "',"
				+"'"+ userIp+"',"
				+"'"+ CesarCode.decode(funcSchema)+"',"
				+"'"+ id +"',"
			   	+"'"+ dispOrder +"',"	   
				+"'"+ status +"',"	
				+"'"+ matinh +"',"		
				+"'"+ typeid +"',"
				+"'"+ title +"',"		
				+"'"+ icon +"',"		
				+"'"+ popupLink +"',"
				+"'"+ popupProp +"',"
				+"'"+ command +"',"
				+"'"+ content +"',"
				+"'"+ userids +"',"+
				+"'"+display_date+"'"+
		        +");"
		        +"end;";
		return s;
	}
	public String updInform(String funcSchema, String userId, String userIp, String id, String dispOrder, String status, String matinh, String typeid, 
			String title, String icon, String popupLink, String popupProp, String command, String content, String userids, String upload_id,String loginfile) {
	
		String s= "begin ? := "+CesarCode.decode(funcSchema)+"info.upd_inform('"
				+ userId + "',"
				+"'"+ userIp+"',"
				+"'"+ CesarCode.decode(funcSchema)+"',"
				+"'"+ id +"',"
			   	+"'"+ dispOrder +"',"	   
				+"'"+ status +"',"	
				+"'"+ matinh +"',"		
				+"'"+ typeid +"',"
				+"'"+ title +"',"		
				+"'"+ icon +"',"		
				+"'"+ popupLink +"',"
				+"'"+ popupProp +"',"
				+"'"+ command +"',"
				+"'"+ content +"',"
				+"'"+ userids +"',"
				+"'"+ upload_id +"',"
				+"'"+ loginfile +"'"
		        +");"
		        +"end;";
		return s;
	}
	public String delInform(String funcSchema, String userId, String userIp, String id) {
	
		String s= "begin ? := "+CesarCode.decode(funcSchema)+"info.del_inform('"
				+ userId + "',"
				+"'"+ userIp+"',"
				+"'"+ CesarCode.decode(funcSchema)+"',"
				+"'"+ id +"'"			   	
		        +");"
		        +"end;";
		return s;
	}
	/*
	//Update & Insert inform	
	 public String update_inform(String schema
								,String piID
								,String psTITLE
								,String psCONTENT
								,String psCOMMAND
								,String psSTATUS
								,String psPOPUP_LINK
								,String psPOPUP_PROP
								,String psTINHS
								,String psICON						
							  ) {
	    String s= "begin ? := "+CesarCode.decode(schema)+"info.update_inform('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
	    +"','"+getUserVar("sys_dataschema")+"',"       
	   	+"'"+piID+"',"	   
		+"'"+psTITLE+"',"	
		+"'"+psCONTENT+"',"		
		+"'"+psCOMMAND+"',"		
		+"'"+psSTATUS+"',"		
		+"'"+psPOPUP_LINK+"',"		
		+"'"+psPOPUP_PROP+"',"
		+"'"+getUserVar("userID")+"',"
		+"'"+getUserVar("userIP")+"',"
		+"'"+psTINHS+"',"
		+"'"+psICON+"'"
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;
  }	
	

		public String delete_inform(String schema,String piID) 
	{ String s= "begin ? := "+CesarCode.decode(schema)+"info.xoa_inform('"+getUserVar("userID")
			+"','"+getUserVar("sys_agentcode")
			+"','"+getUserVar("sys_dataschema")+"',"
       		+piID
			+");"
			+"end;";
		return s;
	} 
	*/
	
//dkv
	public String doc_tt_dvvt(
		
		String psma_tb,
		String psma_kh,
		String psuserid
	)
	{
		String s= "/main?"+ CesarCode.encode("configFile")+ "=pttb/dkygoi/ajax_ls"
				//+"&"+CesarCode.encode("userid")+"="+psuserid
					+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
				+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
				+"&"+CesarCode.encode("userid")+"="+psuserid;
		//System.out.println(s);
		return s;
	}
public String doc_tt_dvvt1(
		
		String psma_tb,
		String psma_kh,
		String psuserid,
		String psuserip
	)
	{
		/*String s= "/main?"+ CesarCode.encode("configFile")+ "=pttb/dkygoi/ajax_tt"
				//+"&"+CesarCode.encode("userid")+"="+psuserid
					+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
				+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
				+"&"+CesarCode.encode("userid")+"="+psuserid
				+"&"+CesarCode.encode("userip")+"="+psuserip;
		//System.out.println(s);
		return s;*/
		return "begin ?:= CCS_ADMIN.WEB_SERVICES.thongtin('"+psma_tb+"','"+psma_kh+"','"+psuserid+"','"+psuserip+"'); end;";
	}	
	
	public String thongtin( String schema,String psma_tb,
		String psma_kh,
		String psuserid,
		String psuserip)
	{		
		 String s= "begin ? := "+schema+"WEB_SERVICES.thongtin("
			+"'"+psma_tb+"',"	
			+"'"+psma_kh+"',"
			+"'"+psuserid+"',"
			+"'"+psuserip+"'"
			+");"
			+"end;"
			;
		//System.out.println(s);
		return s;
	} 	


public String value_delete(String psgoi_id,String psnguoith,String psuser_tinh,String psmayth) 


	{ 
	String s = "begin ?:="+getSysConst("FuncSchema")+"WEB_SERVICES.huy_dangky('"
		+psgoi_id+"','"
		+getUserVar("userID")+"','"
		+getUserVar("userID")+"','"
		//+getUserVar("userIP")+"'); end;";
		//	+getIP()+"'); end;"; 
		+psmayth+"');end;";
		System.out.println(s);
		return s;
	} 	
public String getIP(){
		EnvInfo ei_= this.getEnvInfo();
		return ei_.getServletRequest().getRemoteAddr();
		//return "";
	}		
public String themmoi_goi3dv(	 String psgoi_id
								,String psma_phu
								,String psgoi_dvvt_id
								,String psmuc_cuoc
								,String pdngay_apdung
								,String pssothutu
								,String psma_kh
								,String psma_tb
								,String psnguoith
								,String psuser_tinh
								,String pstrangthai								
								,String psmayth
													
							  ) {
	    String s = "begin ?:="+getSysConst("FuncSchema")+"WEB_SERVICES.dangky_goi3dv('"
			+psgoi_id+"','"	   
			+psma_phu+"','"	
			+psgoi_dvvt_id+"','"	
			+psmuc_cuoc+"','"
			+pdngay_apdung+"','"
			+pssothutu+"','"		
			+psma_kh+"','"	
			+psma_tb+"','"	
			+getUserVar("userID")+"','" 
			+getUserVar("userID")+"','" 
			+pstrangthai+"','"		
			+psmayth+"'); end;";
		
    System.out.println(s);
    return s;
  }	
    public String doc_notra_cmu(
		String pschukyno,
		String psma_kh,		
		String psma_tb,
		String pskieu_ht
	)
	{
		String s= "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tracuu/notra/ajax_notra_cmu"
				+"&"+CesarCode.encode("chukyno")+"="+pschukyno
				+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
				+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
				+"&"+CesarCode.encode("kieu_ht")+"="+pskieu_ht;
				
		System.out.println(s);
		return s;
	}
	public String doc_tinhkhacthuho(
	    String pschukyno,
		String pstungay,		
		String psdenngay,
		String pstinhthu,
		String pstrangthai,
        String pspagenum,
        String pspagerec		
	)
    {
	    String s= "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/thuhos/ajax_phieutinhkhacthu"
				+"&"+CesarCode.encode("chukyno")+"="+pschukyno
				+"&"+CesarCode.encode("tungay")+"="+pstungay
				+"&"+CesarCode.encode("denngay")+"="+psdenngay
				+"&"+CesarCode.encode("tinhthu")+"="+pstinhthu
				+"&"+CesarCode.encode("trangthai")+"="+pstrangthai
				+"&"+CesarCode.encode("pagenum")+"="+pspagenum
				+"&"+CesarCode.encode("pagerec")+"="+pspagerec;
				
		System.out.println(s);
		return s;
	
	}
	public String exp_tinhkhacthuho_txt(
		String pschukyno,
		String pstungay,		
		String psdenngay,
		String pstinhthu,
		String pstrangthai
	)
   {
   	    String s= "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_tienmat/thuhos/export_txt_sql"
				+"&"+CesarCode.encode("chukyno")+"="+pschukyno
				+"&"+CesarCode.encode("tungay")+"="+pstungay
				+"&"+CesarCode.encode("denngay")+"="+psdenngay
				+"&"+CesarCode.encode("tinhthu")+"="+pstinhthu
				+"&"+CesarCode.encode("trangthai")+"="+pstrangthai;
		System.out.println(s);
		return s;
   
   }
   	public String doc_ds_in(
		String psChukyno,
		String psMaKH,
		String pskh,
		String psKieu
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn_hni/tracuu/inhoadon/ajax_thongtin_in"
			+"&"+CesarCode.encode("chukyno")+"="+psChukyno
			+"&"+CesarCode.encode("ma_kh")+"="+psMaKH
			+"&"+CesarCode.encode("kh")+"="+pskh
			+"&"+CesarCode.encode("kieuin")+"="+psKieu;
  	}
	   
   	public String doc_ds_in_lai(
		String psChukyno,
		String psMaKH,
		String psdonviql_id,
		String psma_bc,
		String psPageNum,
		String psPageRec
	)

	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tienmat/invat/hoadon_tt/ajax_phieutra"
			+"&"+CesarCode.encode("tukyhd")+"="+psChukyno
			+"&"+CesarCode.encode("ma_kh")+"="+psMaKH
			+"&"+CesarCode.encode("donviql_id")+"="+psdonviql_id
			+"&"+CesarCode.encode("ma_bc")+"="+psma_bc
			+"&"+CesarCode.encode("page_num")+"="+psPageNum
			+"&"+CesarCode.encode("page_rec")+"="+psPageRec;
  	}
	public NEOCommonData taofile_export_xls(
		String psChukyno,
		String psMaKH,
		String psKieu
	)
	{
	String s="";
	String sDataschema = getUserVar("sys_dataschema");
	String sUserid = getUserVar("sys_userid");
	String sFuncSchema = getSysConst("FuncSchema");	
	String sAgentCode = getUserVar("sys_agentcode");
	
	s="begin ? := admin_v2.qltn_tracuu.tracuu_invat('"
													+psChukyno+"','"
													+psMaKH+"','"
													+psKieu+"','"
													+"','"
													+"','"
													+sDataschema+"','"
													+sUserid
													+"'); end;";
	NEOExecInfo ei_ = new NEOExecInfo(s);

    return  new NEOCommonData(ei_);
	}
}