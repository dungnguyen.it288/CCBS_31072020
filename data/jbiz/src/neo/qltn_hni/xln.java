package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class xln extends NEOProcessEx 
{
	public void run() {
		System.out.println("neo.qltn.qltn_tienmat was called");
	}
	public String doc_quanhuyen(
		String psDsTinh
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/xln/danhsachno/ajax_quanhuyen"
			+"&"+CesarCode.encode("tinhs")+"="+psDsTinh;
	}
	public String doc_danhsachno(
		String psKyhoadon,
		String psLoai,
		String psDsQuan,
		String psChukyno,
		String psPageNum,
		String psPageRec
	){
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/xln/danhsachno/ajax_danhsachno"
			+"&"+CesarCode.encode("kyhoadon")+"="+psKyhoadon
			+"&"+CesarCode.encode("loai")+"="+psLoai
			+"&"+CesarCode.encode("quans")+"="+psDsQuan
			+"&"+CesarCode.encode("chukyno")+"="+psChukyno
			+"&"+CesarCode.encode("page_num")+"="+psPageNum
			+"&"+CesarCode.encode("page_rec")+"="+psPageRec;
	}
	public String value_themmoi_hoso(
		String pskyhoadon,
		String psdsma_kh,
		String psdsma_tb,
		String psto_xln,
		String pschukyno,
		String psghichu,
		String psloai		
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_xln.themmoi_hoso('"
			+pskyhoadon+"','"
			+psdsma_kh+"','"
			+psdsma_tb+"','"
			+psto_xln+"','"
			+pschukyno+"','"
			+psghichu+"','"
			+psloai+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String doc_cbophuongxa(String quan_id) 
  	{
    	return "/main?"+CesarCode.encode("configFile")+"=ccbs_qltn/xln/timhoso/ajax_cbophuongxa"
        	+"&"+CesarCode.encode("quan_id")+"="+quan_id;
  	} 
  	public String doc_cboquanhuyen(String tinh_id) 
  	{
    	return "/main?"+CesarCode.encode("configFile")+"=ccbs_qltn/xln/timhoso/ajax_cboquanhuyen"
    		+"&"+CesarCode.encode("tinh_id")+"="+tinh_id;
  	}
  	public String doc_cbogiayto(String loaixl_id) 
  	{
    	return "/main?"+CesarCode.encode("configFile")+"=ccbs_qltn/xln/xulyhoso/ajax_cbogiayto"
    		+"&"+CesarCode.encode("loaixl_id")+"="+loaixl_id;
  	}
  	public String doc_form(String loaigt_id) 
  	{
    	return "/main?"+CesarCode.encode("configFile")+"=ccbs_qltn/xln/xulyhoso/ajax_form"
    		+"&"+CesarCode.encode("loaigt_id")+"="+loaigt_id;
  	}
  	public String doc_cbonhanvien() 
  	{
    	return "/main?"+CesarCode.encode("configFile")+"=ccbs_qltn/xln/xulyhoso/ajax_cbonhanvien";
  	}
  	public String doc_tracuu_hoso(
	  	String pskyhoadon,
		String psloaixuly,
		String pstinh,
		String psquantt,
		String psphuongtt,
		String psquanct,
		
		String psma_tb,
		String psmacq,
		String psdoituong,
		String psngayhen,
		String psloaigt,
		
		String psto_xln,
		String psghichu,
		String psthangkm,
		String psnotuthang,
		String psnamno,
		String pspagenum,
		String pspagerec 
  	){
  		return "/main?"+CesarCode.encode("configFile")+"=ccbs_qltn/xln/timhoso/ajax_tracuu_hoso"
    		+"&"+CesarCode.encode("kyhoadon")+"="+pskyhoadon
			+"&"+CesarCode.encode("loaixl")+"="+psloaixuly
			+"&"+CesarCode.encode("tinh")+"="+pstinh   
			+"&"+CesarCode.encode("quantt")+"="+psquantt  
			+"&"+CesarCode.encode("phuongtt")+"="+psphuongtt
			+"&"+CesarCode.encode("quanct")+"="+psquanct  
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb   
			+"&"+CesarCode.encode("ma_cq")+"="+psmacq    
			+"&"+CesarCode.encode("doituong")+"="+psdoituong
			+"&"+CesarCode.encode("ngayhen")+"="+psngayhen 
			+"&"+CesarCode.encode("loaigt")+"="+psloaigt  
			+"&"+CesarCode.encode("to_xln")+"="+psto_xln  
			+"&"+CesarCode.encode("ghichu")+"="+psghichu  
			+"&"+CesarCode.encode("thangkm")+"="+psthangkm 
			+"&"+CesarCode.encode("notuthang")+"="+psnotuthang
			+"&"+CesarCode.encode("namno")+"="+psnamno   
			+"&"+CesarCode.encode("pagenum")+"="+pspagenum 
			+"&"+CesarCode.encode("pagerec")+"="+pspagerec;
  	}
  	public String value_xulyhoso(
		String pskyhoadon,
		String psdshoso,
		String psloaixl,
		String psghichu,
		String psngay,
		String psgiayto_id,
		String psthongtin
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_xln.xulyhoso('"
			+pskyhoadon+"','"
			+psdshoso+"','"
			+psloaixl+"','"
			+psghichu+"','"
			+psngay+"','"
			+psgiayto_id+"','"
			+psthongtin+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String rec_khachhang(
		String pskyhoadon,
		String psma_kh,
		String psma_tb,
		String pshoso_id
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_xln.laytt_hoso('"
			+pskyhoadon+"','"
			+psma_kh+"','"
			+psma_tb+"','"
			+pshoso_id+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String doc_hentra(
		String psKyhoadon,
		String psHoso_id
	){
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/xln/hoso/ajax_hentra"
			+"&"+CesarCode.encode("kyhoadon")+"="+psKyhoadon
			+"&"+CesarCode.encode("hoso_id")+"="+psHoso_id;
	}
	public String doc_quatrinhxuly(
		String psKyhoadon,
		String psHoso_id
	){
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/xln/hoso/ajax_quatrinhxuly"
			+"&"+CesarCode.encode("kyhoadon")+"="+psKyhoadon
			+"&"+CesarCode.encode("hoso_id")+"="+psHoso_id;
	}
	public String doc_giayto(
		String psKyhoadon,
		String psHoso_id
	){
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/xln/hoso/ajax_giayto"
			+"&"+CesarCode.encode("kyhoadon")+"="+psKyhoadon
			+"&"+CesarCode.encode("hoso_id")+"="+psHoso_id;
	}
	public String value_capnhat_hentra(
		String pskyhoadon,
		String pshoso_id,
		String pslanhen,
		String psngayhen,
		String pssotien,
		String psghichu
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_xln.capnhat_hentra('"
			+pskyhoadon+"','"
			+pshoso_id+"','"
			+pslanhen+"','"
			+psngayhen+"','"
			+pssotien+"','"
			+psghichu+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_capnhat_ghichu(
		String pskyhoadon,
		String pshoso_id,
		String psxuly_id,
		String psghichu_id,
		String psngaynhap,
		String psnguoinhap,
		String psghichu,
		String psloai
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_xln.capnhat_ghichu('"
			+pskyhoadon+"','"
			+pshoso_id+"','"
			+psxuly_id+"','"
			+psghichu_id+"','"
			+psngaynhap+"','"
			+psnguoinhap+"','"
			+psghichu+"','"
			+psloai+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_xoa_hentra(
		String pskyhoadon,
		String pshoso_id,
		String pslanhen
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_xln.xoa_hentra('"
			+pskyhoadon+"','"
			+pshoso_id+"','"
			+pslanhen+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_xoa_quatrinh(
		String pskyhoadon,
		String pshoso_id,
		String psxuly_id,
		String psghichu_id
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_xln.xoa_quatrinh('"
			+pskyhoadon+"','"
			+pshoso_id+"','"
			+psxuly_id+"','"
			+psghichu_id+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_capnhatkq(
		String pskyhoadon,
		String pshoso_id,
		String psgiayto_id,
		String psketqua
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_xln.capnhat_ketqua('"
			+pskyhoadon+"','"
			+pshoso_id+"','"
			+psgiayto_id+"','"
			+psketqua+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_xoa_giayto(
		String pskyhoadon,
		String pshoso_id,
		String psgiayto_id
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_xln.xoa_giayto('"
			+pskyhoadon+"','"
			+pshoso_id+"','"
			+psgiayto_id+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_xoa_hoso(
		String pskyhoadon,
		String pshoso_id
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_xln.xoa_hoso('"
			+pskyhoadon+"','"
			+pshoso_id+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String doc_chuyento(
		String psKyhoadon,
		String psto_xln,
		String psloai
	){
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/xln/hoso/ajax_chuyento"
			+"&"+CesarCode.encode("kyhoadon")+"="+psKyhoadon
			+"&"+CesarCode.encode("to_xln")+"="+psto_xln
			+"&"+CesarCode.encode("loai")+"="+psloai;
	}
	public String value_themmoi_chuyento(
		String pskyhoadon,
		String pshoso_id,
		String pstuto,
		String psngay,
		String psdento,
		String pslydo
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_xln.themmoi_chuyento('"
			+pskyhoadon+"','"
			+pshoso_id+"','"
			+pstuto+"','"
			+psngay+"','"
			+psdento+"','"
			+pslydo+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_xoa_chuyento(
		String pskyhoadon,
		String psdsrowid
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_xln.xoa_chuyento('"
			+pskyhoadon+"','"
			+psdsrowid+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_nhan_chuyento(
		String pskyhoadon,
		String psdsrowid
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_xln.nhan_chuyento('"
			+pskyhoadon+"','"
			+psdsrowid+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_capnhat_toaan(
		String pskyhoadon,
		String pshoso_id,
		
		String psngaytamunganphi, 
		String pssotien_tamung_anphi,
		String psbienlai_tamung_anphi,
		String psbienlai_hoan_anphi,
		
		String psngayxetxu,
		String psbanan, 
		String psngaybanan,  
		
		String psngay_tamthu_phithihanhan,
		String pssotien_tamthu_phithihanhan,
		String psbienlai_tamthu_phithihanhan, 
		String psbienlai_hoan_phithihanhan
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_xln.capnhat_toaan('"
			+pskyhoadon+"','"
			+pshoso_id+"','"
			
			+psngaytamunganphi+"','"
			+pssotien_tamung_anphi+"','"
			+psbienlai_tamung_anphi+"','"
			+psbienlai_hoan_anphi+"','"
			
			+psngayxetxu+"','"
			+psbanan+"','"
			+psngaybanan+"','"
			
			+psngay_tamthu_phithihanhan+"','"
			+pssotien_tamthu_phithihanhan+"','"
			+psbienlai_tamthu_phithihanhan+"','"
			+psbienlai_hoan_phithihanhan+"','"

			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String rec_thuebaono(
		String pskyhoadon,
		String psma_kh,
		String psma_tb
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_xln.laytt_thuebaono('"
			+pskyhoadon+"','"
			+psma_kh+"','"
			+psma_tb+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String doc_lskhoamo(
		String psKyhoadon,
		String psma_kh,
		String psma_tb
	){
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/xln/thuebao/ajax_lskhoamo"
			+"&"+CesarCode.encode("kyhoadon")+"="+psKyhoadon
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb;
	}
	public String value_yeucaubill(
		String pskyhoadon,
		String psma_kh,
		String psma_tb,
		String psten_tt,
		String psto_xln,
		String pstrangthai,
		String pslydo
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_xln.yeucau_bill('"
			+pskyhoadon+"','"
			+psma_kh+"','"
			+psma_tb+"','"
			+psten_tt+"','"
			+psto_xln+"','"
			+pstrangthai+"','"
			+pslydo+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String doc_tratien(
	  	String pskyhoadon,
		String pstungay,
		String psdenngay,
		String pstoxl,
		String psloaixln,
		String pspagenum,
		String pspagerec 
  	){
  		return "/main?"+CesarCode.encode("configFile")+"=ccbs_qltn/xln/danhsachtra/ajax_danhsachtra"
    		+"&"+CesarCode.encode("kyhoadon")+"="+pskyhoadon
			+"&"+CesarCode.encode("tungay")+"="+pstungay
			+"&"+CesarCode.encode("denngay")+"="+psdenngay   
			+"&"+CesarCode.encode("toxln")+"="+pstoxl  
			+"&"+CesarCode.encode("loaixln")+"="+psloaixln
			+"&"+CesarCode.encode("pagenum")+"="+pspagenum 
			+"&"+CesarCode.encode("pagerec")+"="+pspagerec;
  	}
}