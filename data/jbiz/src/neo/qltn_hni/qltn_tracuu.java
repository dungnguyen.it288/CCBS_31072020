package neo.qltn_hni;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class qltn_tracuu extends NEOProcessEx 
{
	public void run() {
		System.out.println("neo.qltn.qltn_tienmat was called");
	} 
  
 	public String luotthanhtoan_ds(
		String schema,
		String chukyno,
		String ma_nv,
		String dvql_id,
		String capquanly,
		String ma_bc)
	{	
		System.out.println("neo.xln was called-Updated 16/07/2007");    
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/common/qltn/tracuu/frmLuotThanhToan_ajax"
			+"&"+CesarCode.encode("chukyno")+"="+chukyno
			+"&"+CesarCode.encode("ma_nv")+"="+ma_nv
			+"&"+CesarCode.encode("donviql_id")+"="+dvql_id
			+"&"+CesarCode.encode("capquanly")+"="+capquanly
			+"&"+CesarCode.encode("ma_bc")+"="+ma_bc; 	
	}  
   	

	public String luotthanhtoan_luottra(
		String schema,
		String chukyno,
		String ma_nv,
		String tungay,
		String denngay,
		String luotthu,
		String dvql_id,
		String capquanly,
		String ma_bc)
	{	
		System.out.println("neo.xln was called-Updated 16/07/2007");    
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/common/qltn/tracuu/frmLuotThanhToan_ajax1"
			+"&"+CesarCode.encode("chukyno")+"="+chukyno
			+"&"+CesarCode.encode("ma_nv")+"="+ma_nv
			+"&"+CesarCode.encode("ngay_tt")+"="+tungay
			+"&"+CesarCode.encode("DEN_NGAY_TT")+"="+denngay
			+"&"+CesarCode.encode("donviql_id")+"="+dvql_id
			+"&"+CesarCode.encode("capquanly")+"="+capquanly
			+"&"+CesarCode.encode("ma_bc")+"="+ma_bc
			+"&"+CesarCode.encode("luotthanhtoan")+"="+luotthu; 	
	}
 	public String value_capnhat_thuho(
		String pskyhoadon,
		String psphieu_id,
		String psloaitien_id,
		String pshttt_id,
		String psma_kh,
		String psma_tb,
		String pstenkh,
		String psdiachi,
		String psms_thue,
		String pssotien,
		String pschukyno,
		String psma_tn,
		String psmaquay,
		String pstinh_id,
		String psngaythu,
		String psghichu		
	)
	{
		return "begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.capnhat_thuho('"
			+pskyhoadon+"','"
			+psphieu_id+"','"
			+psloaitien_id+"','"
			+pshttt_id+"','"
			+psma_kh+"','"
			+psma_tb+"','"
			+pstenkh+"','"
			+psdiachi+"','"
			+psms_thue+"','"
			+pssotien+"','"
			+pschukyno+"','"
			+psma_tn+"','"
			+psmaquay+"','"
			+pstinh_id+"',to_date('"
			+psngaythu+"','DD/MM/YYYY'),'"
			+psghichu+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
	}
   	public String notonghop_dssomay(
		String schema,
		String chukyno,
		String tukhoa,
		String ma_khachhang,
		String loaitien)
	{	
		System.out.println("neo.xln was called-Updated 16/07/2007");    
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/common/ajax_tra_somay"
			+"&"+CesarCode.encode("chukyno")+"="+chukyno
			+"&"+CesarCode.encode("makh")+"="+ma_khachhang
			+"&"+CesarCode.encode("matb")+"="+tukhoa
			+"&"+CesarCode.encode("loaitien")+"="+loaitien; 	
	}
	public String notonghop_ttnokhoanmuc(
		String schema,
		String chukyno,
		String tukhoa,
		String ma_khachhang,
		String loaitien)
	{	
		System.out.println("neo.xln was called-Updated 16/07/2007");    
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/common/ajax_noth_khoanmuc"
			+"&"+CesarCode.encode("chukyno")+"="+chukyno
			+"&"+CesarCode.encode("makh")+"="+ma_khachhang
			+"&"+CesarCode.encode("matb")+"="+tukhoa
			+"&"+CesarCode.encode("loaitien")+"="+loaitien; 	
	}
	public String notonghop_layttkhach(
		String schema,
		String userinput,
		String chukyno)
	{	
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/common/ajax_laytt_kh_tracuu_no_tonghop"
			+"&"+CesarCode.encode("chukyno")+"="+chukyno
			+"&"+CesarCode.encode("userinput")+"="+userinput; 	
	}
	public String notonghop_notheotb(
		String schema,
		String chukyno,
		String tukhoa,
		String ma_khachhang,
		String thieu_db)
	{	
		System.out.println("neo.xln was called-Updated 16/07/2007");    
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/common/ajax_no_somay"
			+"&"+CesarCode.encode("chukyno")+"="+chukyno
			+"&"+CesarCode.encode("makh")+"="+ma_khachhang
			+"&"+CesarCode.encode("matb")+"="+tukhoa
			+"&"+CesarCode.encode("thieu_db")+"="+thieu_db; 	
	}
	public String notonghop_dieuchinh(
		String schema,
		String chukyno,
		String tukhoa,
		String ma_khachhang)
	{	
		System.out.println("neo.xln was called-Updated 16/07/2007");    
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/common/ajax_noth_dieuchinh"
			+"&"+CesarCode.encode("chukyno")+"="+chukyno
			+"&"+CesarCode.encode("makh")+"="+ma_khachhang
			+"&"+CesarCode.encode("matb")+"="+tukhoa; 	
	}
	public String value_xoa_thuho(
		String pskyhoadon,
		String psdsphieu_id		
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.xoa_thuho('"
			+pskyhoadon+"','"			
			+psdsphieu_id+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}



	public String value_xoa_cnphat(
		String pskyhoadon,
		String psdsphieu_id	,
		String psnguoinhap	
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_hni.xoa_cnphat('"
			+pskyhoadon+"','"			
			+psdsphieu_id+"','"
			+psnguoinhap+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}	


	public String value_giaolai_cnphat(
		String pskyhoadon,
		String psdsphieu_id	,
		String psnguoinhap	
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.giaolai_cnphat('"
			+pskyhoadon+"','"			
			+psdsphieu_id+"','"
			+psnguoinhap+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}	

	public String doc_phieutra(
		String pstuKyHD,
		String psdenKyHD,
		String psdsphieu_id,
		String psma_kh,
		String psma_tb,
		String psten_tt,
		String psdiachi_tt,
		String pstuNgayTT,
		String psdenNgayTT,
		String psLuotTT,
		String psMaNV,
		String psMaTN,
		String psLoaitien,
		String psquydoi,
		String psPageNum,
		String psPageRec	
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn_hni/qltn_tienmat/phieutras/ajax_phieutra"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("denkyhd")+"="+psdenKyHD
			+"&"+CesarCode.encode("phieus")+"="+psdsphieu_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("ten_tt")+"="+psten_tt
			+"&"+CesarCode.encode("diachi_tt")+"="+psdiachi_tt
			+"&"+CesarCode.encode("tungay")+"="+pstuNgayTT
			+"&"+CesarCode.encode("denngay")+"="+psdenNgayTT
			+"&"+CesarCode.encode("luottt")+"="+psLuotTT
			+"&"+CesarCode.encode("ma_nv")+"="+psMaNV
			+"&"+CesarCode.encode("matn")+"="+psMaTN
			+"&"+CesarCode.encode("loaitien")+"="+psLoaitien
			+"&"+CesarCode.encode("quydoi")+"="+psquydoi
			+"&"+CesarCode.encode("page_num")+"="+psPageNum
			+"&"+CesarCode.encode("page_rec")+"="+psPageRec;
	}
	public String doc_lichsuno(
		String pstuKyHD,
		String psdenKyHD,
		String psma_kh,
		String psma_tb
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn_hni/qltn_tienmat/lichsu_notras/ls_no"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("denkyhd")+"="+psdenKyHD
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb;
	}
	public String doc_lichsutra(
		String pstuKyHD,
		String psdenKyHD,
		String psma_kh,
		String psma_tb
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn_hni/qltn_tienmat/lichsu_notras/ls_tra"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("denkyhd")+"="+psdenKyHD
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb;
	}
	public String doc_phieutra2(
		String psKyhoadon,
		String psma_kh,
		String psma_tb,
		String pstuNgayTT,
		String psdenNgayTT,
		String psten_tt,
		
		String psDonvi,
		String psMaBC,
		String psMaquay,
		String psMaTN,
		String psHTTT,
		String psChungtu,
		
		String psTientu,
		String psTienden,
		String psPhieuIDTu,
		String psPhieuIDDen,
		String psSophieuTu,
		String psSophieuDen,
		String psSoseriTu,
		String pssoseriDen,
		String psGomHDTu,
		String psGomHDDen,
		String psSeri,
		String psChuain,
		
		String psPageNum,
		String psPageRec	
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn_hni/qltn_tienmat/phieuthus/ajax_phieutra"
			+"&"+CesarCode.encode("kyhoadon")+"="+psKyhoadon
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("tungay")+"="+pstuNgayTT
			+"&"+CesarCode.encode("denngay")+"="+psdenNgayTT
			+"&"+CesarCode.encode("ten_tt")+"="+psten_tt
			
			+"&"+CesarCode.encode("donvi")+"="+psDonvi
			+"&"+CesarCode.encode("ma_bc")+"="+psMaBC
			+"&"+CesarCode.encode("maquay")+"="+psMaquay
			+"&"+CesarCode.encode("ma_tn")+"="+psMaTN
			+"&"+CesarCode.encode("httt")+"="+psHTTT
			+"&"+CesarCode.encode("chungtu")+"="+psChungtu
			
			+"&"+CesarCode.encode("tientu")+"="+psTientu
			+"&"+CesarCode.encode("tienden")+"="+psTienden
			+"&"+CesarCode.encode("phieuidtu")+"="+psPhieuIDTu
			+"&"+CesarCode.encode("phieuidden")+"="+psPhieuIDDen
			+"&"+CesarCode.encode("sophieutu")+"="+psSophieuTu
			+"&"+CesarCode.encode("sophieuden")+"="+psSophieuDen
			+"&"+CesarCode.encode("soseritu")+"="+psSoseriTu
			+"&"+CesarCode.encode("soseriden")+"="+pssoseriDen
			+"&"+CesarCode.encode("gomhdtu")+"="+psGomHDTu
			+"&"+CesarCode.encode("gomhdden")+"="+psGomHDDen
			+"&"+CesarCode.encode("seri")+"="+psSeri
			+"&"+CesarCode.encode("chuain")+"="+psChuain
			
			+"&"+CesarCode.encode("page_num")+"="+psPageNum
			+"&"+CesarCode.encode("page_rec")+"="+psPageRec;
	}
	public String doc_buucuc(String psdonviql_id){
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn_hni/qltn_tienmat/connos/ajax_buucuc"
			+"&"+CesarCode.encode("donviql_id")+"="+psdonviql_id;
	}
	public String doc_nhanvientc(String psbuucuc){
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn_hni/qltn_tienmat/connos/ajax_nhanvientc"
			+"&"+CesarCode.encode("ma_bc")+"="+psbuucuc;
	}
	public String doc_tuyenthu(String psma_nv){
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn_hni/qltn_tienmat/connos/ajax_tuyenthu"
			+"&"+CesarCode.encode("ma_nv")+"="+psma_nv;
	}
	public String doc_conno(
		String pskyhoadon,
		String psdonviql_id,
		String psma_bc,
		String psma_nv,
		String psma_t,
		String psma_kh,
		String psma_tb,
		String psten_tt,
		String psdiachi_tt,
		String pskieuno,
		String pstientu,
		String pstienden,
		String pspage_num,
		String pspage_rec
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn_hni/qltn_tienmat/connos/ajax_conno"
			+"&"+CesarCode.encode("kyhoadon")+"="+pskyhoadon
			+"&"+CesarCode.encode("donviql_id")+"="+psdonviql_id
			+"&"+CesarCode.encode("ma_bc")+"="+psma_bc
			+"&"+CesarCode.encode("ma_nv")+"="+psma_nv
			+"&"+CesarCode.encode("ma_t")+"="+psma_t
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("ten_tt")+"="+psten_tt
			+"&"+CesarCode.encode("diachi_tt")+"="+psdiachi_tt
			+"&"+CesarCode.encode("kieuno")+"="+pskieuno
			+"&"+CesarCode.encode("tientu")+"="+pstientu
			+"&"+CesarCode.encode("tienden")+"="+pstienden
			+"&"+CesarCode.encode("page_rec")+"="+pspage_rec
			+"&"+CesarCode.encode("page_num")+"="+pspage_num;
	}
	public String doc_tonghop(
		String pstuKyHD,
        String psdenKyHD,
        String psma_nv,
        String psma_t,
        String psma_cq,
        String psma_kh,
        String psma_tb,
        String psten_tt,
        String psdiachi_tt,
        String pspage_num,
        String pspage_rec	
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn_hni/qltn_tienmat/lichsu_notras/ajax_tonghop"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("denkyhd")+"="+psdenKyHD
			+"&"+CesarCode.encode("ma_nv")+"="+psma_nv
			+"&"+CesarCode.encode("ma_t")+"="+psma_t
			+"&"+CesarCode.encode("ma_cq")+"="+psma_t
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("ten_tt")+"="+psten_tt
			+"&"+CesarCode.encode("diachi_tt")+"="+psdiachi_tt
			+"&"+CesarCode.encode("page_rec")+"="+pspage_rec
			+"&"+CesarCode.encode("page_num")+"="+pspage_num;
	}
	public String doc_no(
		String pstuKyHD,
        String psdenKyHD,
        String psma_nv,
        String psma_t,
        String psma_cq,
        String psma_kh,
        String psma_tb,
        String psten_tt,
        String psdiachi_tt,
        String pspage_num,
        String pspage_rec
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn_hni/qltn_tienmat/lichsu_notras/ajax_no"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("denkyhd")+"="+psdenKyHD
			+"&"+CesarCode.encode("ma_nv")+"="+psma_nv
			+"&"+CesarCode.encode("ma_t")+"="+psma_t
			+"&"+CesarCode.encode("ma_cq")+"="+psma_t
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("ten_tt")+"="+psten_tt
			+"&"+CesarCode.encode("diachi_tt")+"="+psdiachi_tt
			+"&"+CesarCode.encode("page_rec")+"="+pspage_rec
			+"&"+CesarCode.encode("page_num")+"="+pspage_num;
	}
	public String doc_phieudc(
		String pstuKyHD,
		String psdenKyHD,
		String psma_kh,
		String psma_tb,
		String psloaitien_id,
		String psten_tt,
		String psdiachi_tt,
		String pstuNgayTT,
		String psdenNgayTT,
		String psLydo,
		String psPageNum,
		String psPageRec	
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn_hni/qltn_tienmat/phieudcs/ajax_phieudc"
			+"&"+CesarCode.encode("tukyhd")+"="+pstuKyHD
			+"&"+CesarCode.encode("denkyhd")+"="+psdenKyHD
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("loaitien_id")+"="+psloaitien_id
			+"&"+CesarCode.encode("ten_tt")+"="+psten_tt
			+"&"+CesarCode.encode("diachi_tt")+"="+psdiachi_tt
			+"&"+CesarCode.encode("tungay")+"="+pstuNgayTT
			+"&"+CesarCode.encode("denngay")+"="+psdenNgayTT
			+"&"+CesarCode.encode("lydo")+"="+psLydo
			+"&"+CesarCode.encode("page_num")+"="+psPageNum
			+"&"+CesarCode.encode("page_rec")+"="+psPageRec;
	}
	public String doc_chuyentuyen(
		String psMaNV,
		String psMaT,
		String psMaKH,
		String psCbo	
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn_hni/qltn_chuyentuyen/ajax_chuyentuyen"
			+"&"+CesarCode.encode("ma_nv")+"="+psMaNV
			+"&"+CesarCode.encode("ma_t")+"="+psMaT
			+"&"+CesarCode.encode("ma_kh")+"="+psMaKH
			+"&"+CesarCode.encode("cbo")+"="+psCbo;
	}
	public String doc_thuho(
		String pskyhoadon,
		String psTungay,
		String psdenngay,
		String psma_tb,
		String psma_bc,
		String psmaquay,
		String psma_tn,
		String pstinh,
		String pstientu,
		String pstienden,
		String psphieuidtu,
		String psphieuidden,
		String pssoseritu,
		String pssoseriden,
		String pstrangthai,
		String psloai,
		String pspagenum,
		String pspagerec
	){
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn_hni/qltn_tienmat/thuhos/ajax_phieuthu"
			+"&"+CesarCode.encode("kyhoadon")+"="+pskyhoadon
			+"&"+CesarCode.encode("tungay")+"="+psTungay
			+"&"+CesarCode.encode("denngay")+"="+psdenngay
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			
			+"&"+CesarCode.encode("ma_bc")+"="+psma_bc
			+"&"+CesarCode.encode("maquay")+"="+psmaquay
			+"&"+CesarCode.encode("ma_tn")+"="+psma_tn
			+"&"+CesarCode.encode("tinh")+"="+pstinh
			
			+"&"+CesarCode.encode("tientu")+"="+pstientu
			+"&"+CesarCode.encode("tienden")+"="+pstienden
			+"&"+CesarCode.encode("phieuidtu")+"="+psphieuidtu
			+"&"+CesarCode.encode("phieuidden")+"="+psphieuidden
			+"&"+CesarCode.encode("soseritu")+"="+pssoseritu
			+"&"+CesarCode.encode("soseriden")+"="+pssoseriden
			+"&"+CesarCode.encode("trangthai")+"="+pstrangthai
			+"&"+CesarCode.encode("loai")+"="+psloai
			
			+"&"+CesarCode.encode("page_num")+"="+pspagenum
			+"&"+CesarCode.encode("page_rec")+"="+pspagerec;
	}
	public String doc_cuocnong(
		String psKyhoadon,
		String pstuNgayTT,
		String psdenNgayTT,
		String psma_kh,
		String psma_tb,
		
		String psMaBC,
		String psMaquay,
		String psMaTN,
		
		String psTientu,
		String psTienden,
		String psPhieuIDTu,
		String psPhieuIDDen,
		String psSoseriTu,
		String pssoseriDen,
		String psTrangthai,
		
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn_hni/qltn_tienmat/cuocnongs/ajax_ds_cuocnong"
			+"&"+CesarCode.encode("kyhoadon")+"="+psKyhoadon
			+"&"+CesarCode.encode("tungay")+"="+pstuNgayTT
			+"&"+CesarCode.encode("denngay")+"="+psdenNgayTT
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			
			+"&"+CesarCode.encode("ma_bc")+"="+psMaBC
			+"&"+CesarCode.encode("maquay")+"="+psMaquay
			+"&"+CesarCode.encode("ma_tn")+"="+psMaTN
			
			+"&"+CesarCode.encode("tientu")+"="+psTientu
			+"&"+CesarCode.encode("tienden")+"="+psTienden
			+"&"+CesarCode.encode("phieuidtu")+"="+psPhieuIDTu
			+"&"+CesarCode.encode("phieuidden")+"="+psPhieuIDDen
			+"&"+CesarCode.encode("soseritu")+"="+psSoseriTu
			+"&"+CesarCode.encode("soseriden")+"="+pssoseriDen
			+"&"+CesarCode.encode("trangthai")+"="+psTrangthai
			
			+"&"+CesarCode.encode("page_num")+"="+psPageNum
			+"&"+CesarCode.encode("page_rec")+"="+psPageRec;
	}
}