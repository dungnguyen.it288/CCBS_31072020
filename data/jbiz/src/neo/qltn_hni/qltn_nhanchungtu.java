package neo.qltn_hni;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class qltn_nhanchungtu extends NEOProcessEx 

{
  	public void run() 
  	{
    	System.out.println("neo.qltn.qltn_tienmat was called");
    	//	getSysConst("FuncSchema");
    	//	getUserVar("sys_dataschema");
    	//	getUserVar("userID");
  	}



public String doc_notheothang(
       String psKyhoadon,
       String psmakh
        )
	{		
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_chuyenkhoan/ajax_notheothang"
			+"&"+CesarCode.encode("kyhoadon")+"="+psKyhoadon
			+"&"+CesarCode.encode("ma_kh")+"="+psmakh;
			
	}

public String doc_ds_chungtu(
       String psKyhoadon,
       String psnganhang_id,	
       String psma_tb,
       String psnoidung,
       String psso_ct,
       String pssobienlai,
       String pssotk_dvh,
       String pssotk_dvph,
       String pstrangthai,
       String psngaynhan,
       String psloaict_id,
       String psma_kh,
       String pspagenum,
       String pspagerec
    )
	{		
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_chuyenkhoan/ajax_ds_chungtu"
			+"&"+CesarCode.encode("kyhoadon")+"="+psKyhoadon
			+"&"+CesarCode.encode("nganhang_id")+"="+psnganhang_id
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("noidung")+"="+psnoidung
			+"&"+CesarCode.encode("so_ct")+"="+psso_ct
			+"&"+CesarCode.encode("sobienlai")+"="+pssobienlai
			+"&"+CesarCode.encode("sotk_dvh")+"="+pssotk_dvh
			+"&"+CesarCode.encode("sotk_dvp")+"="+pssotk_dvph
			+"&"+CesarCode.encode("trangthai")+"="+pstrangthai
			+"&"+CesarCode.encode("ngaynhan")+"="+psngaynhan
			+"&"+CesarCode.encode("loaict_id")+"="+psloaict_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("page_num")+"="+pspagenum
			+"&"+CesarCode.encode("page_rec")+"="+pspagerec; 	
	}
	
	public String value_themmoi_chuyenkhoan(  
		String   pskyhoadon,
		String   pschukyno,
		String    psso_ct,
		String    pstientruocthue,
		String    pstienthue,
		String    psma_bc,
		String    psmatb,
		String    psghichu,
		String    psloaitien_id,
		String    pstennh_phat,
		String    psdonvi_huong,
		String    psdiachi_dvh,
		String    pssotk_dvh,
		String    psnganhang_dvh,
		String    psnganhang_dvp,
		String    psdonvi_phat,
		String    psdiachi_dvp,
		String    pssotk_dvp,
		String    pitrangthai,
		String    psngaygui,
		String    psma_kh,
		String   psloai_chungtu,
		String  psphat,
		String  psphinganhang,
		String   psthangno,
		String   tiennotheothang
	) 						  
 	{ 
 		String s = "begin ?:= "+getSysConst("FuncSchema")+"qltn_hni.themmoi_chuyenkhoan("
	        +"'"+pskyhoadon+"'"
	        +",'"+pschukyno+"'"
			+",'"+psso_ct+"'"
	        +",'"+pstientruocthue.replace(",","")+"'"
			+",'"+pstienthue.replace(",","")+"'"
			+",'"+psma_bc+"'"
			+",'"+psmatb+"'"
			+",'"+psghichu+"'"
	        +","+psloaitien_id
			+","+pstennh_phat
	        +",'"+psdonvi_huong+"'"
	        +",'"+psdiachi_dvh+"'"
	        +",'"+pssotk_dvh+"'"
			+","+psnganhang_dvh
			+","+psnganhang_dvp
			+",'"+psdonvi_phat+"'"
			+",'"+psdiachi_dvp+"'"
	        +",'"+pssotk_dvp+"'"
	       	+",'"+pitrangthai+"'"
			+",'"+psngaygui+"'"
			+",'"+psma_kh+"'"
		    +","+psloai_chungtu
		    +","+psphat	
		    +","+psphinganhang
			+",'"+psthangno+"'"
			+",'"+tiennotheothang+"'"
		    +",'"+getUserVar("sys_dataschema")+"'"
	        +",'"+getUserVar("userID")+"'"
		    +"); end;";
		System.out.println(s);
		return s;
 	}
	public String value_xoa_chuyenkhoan
	( 
		String psKyhoadon,
		String psDsPhieu_ID,
		String psMa_BC,
		String psHinhthucTT
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_hni.xoa_chuyenkhoan("
			+"'"+psKyhoadon+"',"
	 	    +"'"+psDsPhieu_ID+"'"
	 	    +",'"+psMa_BC+"'"
	 	    +",'"+psHinhthucTT+"'"		
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
	public String  laydc_nh
	( 
		
		String psma_nh
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_hni.laydc_nganhang("
			+"'"+psma_nh+"'"
	 	 	+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}


	
	public String  laysotaikhoan_dvh
	( 
		
		String psma_nh
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_hni.laysotaikhoan_dvh("
			+"'"+psma_nh+"'"
	 	 	+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}

	/*
	public String  laydc_nh
	( 
		
	String psnganhang_id
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_hni.laytaikhoan_nganhang("
			+"'"+psnganhang_id+"'"
	 	 	+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
	*/
	public String value_capnhat_chungtu(  
		String    pskyhoadon,
		String    pschukyno,
		String    psso_ct,		
		String    pstientruocthue,
		String    pstienthue,
		String    psma_bc,
		String    psmatb,
		String    psghichu,
		String    psloaitien_id,
		String    pstennh_phat,
		String    psdonvi_huong,
		String    psdiachi_dvh,
		String    pssotk_dvh,
		String    psnganhang_dvh,
		String    psdonvi_phat,
		String    psdiachi_dvp,
		String    pssotk_dvp,
		String    pitrangthai,
		String    psngaygui,
		String    psma_kh,
		String    psloai_chungtu,
		String    psphieu_id,
		String   psphat,
		String   psphinganhang	
		
	) 						  
 	{ 
 	 String s ="begin ?:= "+getSysConst("FuncSchema")+"qltn_hni.capnhat_chuyenkhoan("
	      		  +"'"+pskyhoadon+"'"
	     		  +",'"+pschukyno+"'"
			+",'"+psso_ct+"'" 			
	       		 +",'"+pstientruocthue.replace(",","")+"'"
			+",'"+pstienthue.replace(",","")+"'"
			+",'"+psma_bc+"'"
			+",'"+psmatb+"'"
			+",'"+psghichu+"'"
	        +","+psloaitien_id
	         +","+0/*pstennh_phat*/
	        +",'"+psdonvi_huong+"'"
	        +",'"+psdiachi_dvh+"'"
	        +",'"+pssotk_dvh+"'"
	        +","+psnganhang_dvh
	        +",'"+psdonvi_phat+"'"
	        +",'"+psdiachi_dvp+"'"
	        +",'"+pssotk_dvp+"'"
	       	+",'"+pitrangthai+"'"
		    +",'"+psngaygui+"'"
		    +",'"+psma_kh+"'"
		    +","+psloai_chungtu
		    +","+psphieu_id
		   +","+psphat
		   +","+psphinganhang	
		   +",'"+getUserVar("sys_dataschema")+"'"
	       	   +",'"+getUserVar("userID")+"'"
		    +"); end;";
	System.out.println(s);
	return s;
 	}

  	public String RecTTKhachHang_ChuyenKhoan(String InMaKh)	
	{
		return "begin ? := "+getSysConst("FuncSchema")+"qltn_tracuu.tracuu_thongtin_KH_TB(null,null,null,'"+InMaKh+"',null,null,null,null,null,null,null,'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
	}
	public String rec_notonghop(
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psLoaiTienID		
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.laytt_notonghop('"
			+psChukyno+"','"
			+psMaKH+"','"
			+psMaTB+"','"
			+""+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
  	}

	public String rec_tttamthu(
		String psChukyno,
		String psMaKH
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_hni.laytt_tamthu('"
			+psChukyno+"','"
			+psMaKH+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"'); end;";
  	}


  	public String doc_cuocnong()
  	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_nhanchungtu/ajax_tamthu_cuocnong";
  	}
  	public String doc_chuyenkhoan()
  	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_nhanchungtu/ajax_chuyenkhoan";
  	}
	public String doc_tamthu()
  	{
  		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_nhanchungtu/ajax_kyquy";
  	}
  	public String value_themmoi_phieuthu(
		String psma_kh           ,
		String psma_tb           ,
		String pstien            ,				
		String psnoidung         ,
		String psso_ct           ,
		String pschukyno         ,
		String psloaitien_id     ,
		String psngaynhap        ,
		String pshinhthuctt		 ,
		String psma_bc           ,
		String psma_tn           ,
		String psmaquay          ,
		String psloai            
	)
	{
		String s="begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.themmoi_phieuthu("
			+"'"+ psma_kh         +"',"
			+"'"+ psma_tb         +"',"			
			+"'"+ pstien          +"',"
			+"'"+ psnoidung       +"',"
			+"'"+ psso_ct         +"',"
			+"'"+ pschukyno       +"',"
			+"'"+ psloaitien_id   +"',"
			+"to_date('"+ psngaynhap      +"','DD/MM/YYYY'),"
			+"'"+ pshinhthuctt    +"',"
			+"'"+ psma_bc         +"',"
			+"'"+ psma_tn         +"',"
			+"'"+ psmaquay        +"',"
			+"'"+ psloai          +"',"  
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String doc_layds_phieuthu(
		String psphieu_id   ,
	    String psma_kh      ,
		String psma_tb      ,
		String psso_ct      ,
		String psngaynhap   ,
		String psloai       ,	
		String pspagenum    ,
		String pspagerec    
	)
	{
		String file="ajax_ds_cuocnong";
		if (psloai=="1") file="ajax_ds_kyquy";
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_nhanchungtu/"+file
			+"&"+CesarCode.encode("phieu_id")+"="+psphieu_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("so_ct")+"="+psso_ct			
			+"&"+CesarCode.encode("ngaynhan")+"="+psngaynhap
			+"&"+CesarCode.encode("loai")+"="+psloai			
			+"&"+CesarCode.encode("page_num")+"="+pspagenum
			+"&"+CesarCode.encode("page_rec")+"="+pspagerec;
	}
	public String value_capnhat_phieuthu(
		String psphieu_id		 ,
		String psma_kh           ,
		String psma_tb           ,
		String pstien            ,				
		String psnoidung         ,
		String psso_ct           ,
		String pschukyno         ,
		String psloaitien_id     ,
		String psngaynhap        ,
		String pshinhthuctt      ,
		String psma_bc           ,
		String psma_tn           ,
		String psmaquay          ,
		String psloai            
	)
	{
		String s="begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.capnhat_phieuthu("
			+"'"+ psphieu_id      +"',"
			+"'"+ psma_kh         +"',"
			+"'"+ psma_tb         +"',"			
			+"'"+ pstien          +"',"
			+"'"+ psnoidung       +"',"
			+"'"+ psso_ct         +"',"
			+"'"+ pschukyno       +"',"
			+"'"+ psloaitien_id   +"',"
			+"to_date('"+ psngaynhap      +"','DD/MM/YYYY'),"
			+"'"+ pshinhthuctt    +"',"
			+"'"+ psma_bc         +"',"
			+"'"+ psma_tn         +"',"
			+"'"+ psmaquay        +"',"
			+"'"+ psloai          +"',"  
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_xoa_cuocnong
	( 
		String psKyhoadon,
		String psDsPhieu_ID,
		String psMa_BC
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.xoa_cuocnong("			
		    +"'"+psKyhoadon+"'"
	 	    +",'"+psDsPhieu_ID+"'"
	 	    +",'"+psMa_BC+"'"
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}


	public String  value_hoantra
	( 
		String psKyhoadon,
		String pstienhoantra,
		String psmakh
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_hni.hoantra_tamthu("			
		    +"'"+psKyhoadon+"'"
	 	    +",'"+pstienhoantra+"'"
		    +",'"+psmakh+"'"	
	 	   +",'"+getUserVar("sys_dataschema")+"'"
		   +",'"+getUserVar("userID")+"'"
		  +");"
	        +"end;";
	}

	public String value_xoa_phieuchi
	( 
		String psKyhoadon,
		String psDsPhieu_ID,
		String psMa_BC,
		String psLoai
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.xoa_phieuchi("			
		    +"'"+psKyhoadon+"'"
	 	    +",'"+psDsPhieu_ID+"'"
	 	    +",'"+psMa_BC+"'"
	 	    +",'"+psLoai+"'"		
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
	public String value_muctien_kyquy(String psDichvuKQ_ID)		
	{
		return "begin select sotien into ? from "
			+getUserVar("sys_dataschema")
			+"dichvu_kqs where dichvukq_id="+psDichvuKQ_ID
			+"; end;";
	}
	public String value_themmoi_phieuchi(
		String psma_kh           ,
		String psma_tb           ,
		String psphieuchi		 ,
		String pstien            ,				
		String psnoidung         ,
		String psso_ct           ,
		String pschukyno         ,
		String psloaitien_id     ,
		String psngaynhap        ,
		String pshinhthuctt		 ,
		String psma_bc           ,
		String psma_tn           ,
		String psmaquay          ,
		String psloai            
	)
	{
		String s="begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.themmoi_phieuchi("
			+"'"+ psma_kh         +"',"
			+"'"+ psma_tb         +"',"			
			+"'"+ psphieuchi      +"',"
			+"'"+ pstien          +"',"
			+"'"+ psnoidung       +"',"
			+"'"+ psso_ct         +"',"
			+"'"+ pschukyno       +"',"
			+"'"+ psloaitien_id   +"',"
			+"to_date('"+ psngaynhap      +"','DD/MM/YYYY'),"
			+"'"+ pshinhthuctt    +"',"
			+"'"+ psma_bc         +"',"
			+"'"+ psma_tn         +"',"
			+"'"+ psmaquay        +"',"
			+"'"+ psloai          +"',"  
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}	
	public String doc_layds_kyquy(
		String psphieu_id   ,
	    String psma_kh      ,
		String psma_tb      ,
		String psso_ct      ,
		String psngaynhap   ,
		String pspagenum    ,
		String pspagerec    
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_kyquy/ajax_ds_kyquy"
			+"&"+CesarCode.encode("phieu_id")+"="+psphieu_id
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("so_ct")+"="+psso_ct			
			+"&"+CesarCode.encode("ngaynhan")+"="+psngaynhap			
			+"&"+CesarCode.encode("page_num")+"="+pspagenum
			+"&"+CesarCode.encode("page_rec")+"="+pspagerec;
	}
	public String value_themmoi_kyquy(
		String psma_kh           ,
		String psma_tb           ,
		String pstien            ,				
		String psnoidung         ,
		String pschukyno         ,
		String psloaitien_id     ,
		String psngaynhap        ,
		String psdichvukq_id	 ,
		String psma_bc           ,
		String psma_tn           ,
		String psmaquay          
	)
	{
		String s="begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.themmoi_kyquy("
			+"'"+ psma_kh         +"',"
			+"'"+ psma_tb         +"',"			
			+"'"+ pstien          +"',"
			+"'"+ psnoidung       +"',"
			+"'"+ pschukyno       +"',"
			+"'"+ psloaitien_id   +"',"
			+"to_date('"+ psngaynhap      +"','DD/MM/YYYY'),"
			+"'"+ psdichvukq_id    +"',"
			+"'"+ psma_bc         +"',"
			+"'"+ psma_tn         +"',"
			+"'"+ psmaquay        +"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_themmoi_chikyquy(
		String psma_kh           ,
		String psma_tb           ,
		String psphieuchi		 ,
		String pstien            ,				
		String psnoidung         ,
		String pschukyno         ,
		String psloaitien_id     ,
		String psngaynhap        ,
		String psdichvukq_id     ,
		String psma_bc           ,
		String psma_tn           ,
		String psmaquay                      
	)
	{
		String s="begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.themmoi_chikyquy("
			+"'"+ psma_kh         +"',"
			+"'"+ psma_tb         +"',"			
			+"'"+ psphieuchi      +"',"
			+"'"+ pstien          +"',"
			+"'"+ psnoidung       +"',"
			+"'"+ pschukyno       +"',"
			+"'"+ psloaitien_id   +"',"
			+"to_date('"+ psngaynhap      +"','DD/MM/YYYY'),"
			+"'"+ psdichvukq_id   +"',"
			+"'"+ psma_bc         +"',"
			+"'"+ psma_tn         +"',"
			+"'"+ psmaquay        +"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}
	public String value_xoa_kyquy
	( 
		String psKyhoadon,
		String psDsPhieu_ID,
		String psMa_BC
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.xoa_kyquy("			
		    +"'"+psKyhoadon+"'"
	 	    +",'"+psDsPhieu_ID+"'"
	 	    +",'"+psMa_BC+"'"		
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
	public String value_xoa_chikyquy
	( 
		String psKyhoadon,
		String psDsPhieu_ID,
		String psMa_BC
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.xoa_chikyquy("			
		    +"'"+psKyhoadon+"'"
	 	    +",'"+psDsPhieu_ID+"'"
	 	    +",'"+psMa_BC+"'"	
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
	public String doc_layds_ctcuocnong(
	    String psma_kh,
		String psma_tb      
	)
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_tamthu/ajax_chitietcuoc"
			+"&"+CesarCode.encode("ma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("ma_tb")+"="+psma_tb;
	}
	public String value_tamthu1(
		String pskyhoadon,
		String psdsma_kh,
		String psdsma_tb,
		String pstientra,
		String pdngay_tt,
		String pihttt_id,
		String piloaitien_id,
		String psgomhd,
		String psnoidung
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.tamthu1("			
		    +"'"+pskyhoadon+"'"
	 	    +",'"+psdsma_kh+"'"
	 	    +",'"+psdsma_tb+"'"
	 	    +",'"+pstientra+"'"
	 	    +",'"+pdngay_tt+"'"
	 	    +",'"+pihttt_id+"'"
	 	    +",'"+piloaitien_id+"'"
	 	    +",'"+psgomhd+"'"
	 	    +",'"+psnoidung+"'"
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
	public String value_chicuocnong1(
		String pskyhoadon,
		String psphieuthu_id,
		String psdsma_kh,
		String psdsma_tb,
		String pstientra,
		String pdngay_tt,
		String pihttt_id,
		String piloaitien_id,
		String psgomhd,
		String psnoidung
	)
	{
		String sql="begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.chicuocnong1("			
		    +"'"+pskyhoadon+"'"
		    +",'"+psphieuthu_id+"'"
	 	    +",'"+psdsma_kh+"'"
	 	    +",'"+psdsma_tb+"'"
	 	    +",'"+pstientra+"'"
	 	    +",'"+pdngay_tt+"'"
	 	    +",'"+pihttt_id+"'"
	 	    +",'"+piloaitien_id+"'"
	 	    +",'"+psgomhd+"'"
	 	    +",'"+psnoidung+"'"
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	    System.out.println(sql);
	    return sql;
	}
	public String value_xoa_chicuocnong
	( 
		String psKyhoadon,
		String psDsPhieu_ID,
		String psMa_BC
	)
	{
		return "begin ?:= "+getSysConst("FuncSchema")+"qltn_tamthu.xoa_chicuocnong("			
		    +"'"+psKyhoadon+"'"
	 	    +",'"+psDsPhieu_ID+"'"
	 	    +",'"+psMa_BC+"'"	
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+");"
	        +"end;";
	}
}