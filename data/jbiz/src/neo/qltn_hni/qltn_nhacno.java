package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class qltn_nhacno extends NEOProcessEx 
{
	public void run() 
	{
		System.out.println("neo.qltn.qltn_tienmat was called");
	}
	public String value_capnhat_nhacno
	(
		String pschukyno
	) 
	{  
 		String  s=  "begin ?:= "+getSysConst("FuncSchema")+"qltn_tracuu.capnhat_nhacno("
	 	    +"'"+pschukyno+"'"
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+"); end;";
		return s;	
	}
  
	public String value_xoa_dsnhacno
	(
		String  pschukyno
		,String psds_tb						        
	) 
	{  	
		String s="begin ?:= "+getSysConst("FuncSchema")+"qln_kmm.xoa_ds_nhacno("
	 	    +"'"+pschukyno+"'"
			+",'"+psds_tb+"'"
			+",'"+getUserVar("sys_dataschema")+"'"
			+",'"+getUserVar("userID")+"'"
			+"); end;";
		return s;	
	}
   
	public String value_them_tb_nhacno( 
		String loai_tb 
		,String kyhoadon 
		,String thang 
		,String tienmin 
		,String tienmax
		,String thangmin 
		,String thangmax 
		,String tuyenthu	
		,String loaikh
		,String mabc 
		,String khachhangdb 
		,String kieutien 
		,String ht_nhac
		,String chuky
	) 					  
	{ 
		String s =  "begin ?:= "+getSysConst("FuncSchema")+"QLN_kmm.tao_ds_nhacno("
			+"'"+kyhoadon+"'"
			+","+loai_tb 
			+","+khachhangdb
	        +",'"+thang+"'"
	        +",'"+ht_nhac+"'"
			+",'"+chuky+"'"
	        +","+tienmin
	        +","+tienmax
	        +","+thangmin
			+","+thangmax
			+",'"+tuyenthu+"'"
			+","+loaikh
	        +","+kieutien
			+",'"+mabc+"'"
	        +",'"+getUserVar("sys_dataschema")+"'"
	        +",'"+getUserVar("userID")+"'"
		    +"); end;" ;
		return s;
	}	
 
	public String doc_ds_nhacno
	(
		String psChukyno,
		String psPageNum,
		String psPageRec
	)
	{
			return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/khoamomay/nhacno/ajax_ds_nhacno"
				+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
				+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
				+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
  	}
	public String value_them_ds_nhacno(
		String psds_matb,
        String pi_KY_HD,
        String pi_LOAI_TB,
        String ps_THANG,
        String pi_KIEU_NHAC,
        String pi_CHU_KY_ID
	)
	{
		String s="begin ?:="+getSysConst("FuncSchema")+"qln_kmm.them_ds_nhacno("
			+"'"+psds_matb+"',"
			+"'"+pi_KY_HD+"',"
			+"'"+pi_LOAI_TB+"',"
			+"'"+ps_THANG+"',"
			+"'"+pi_KIEU_NHAC+"',"
			+"'"+pi_CHU_KY_ID+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";
		return s;
	}
}
