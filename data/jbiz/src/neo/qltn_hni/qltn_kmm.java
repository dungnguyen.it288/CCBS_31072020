package neo.qltn;

import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class qltn_kmm extends NEOProcessEx {

    public void run() {
        System.out.println("neo.qltn.qltn_tienmat was called");
    }

    public String value_taods_kmtd(
            String dvvt,
            String kyhoadon,
            String thang,
            String kieukhoa,
            String kieunhac,
            String ngaygh,
            String tienMin,
            String tienMax,
            String thangMin,
            String thangMax,
            String manv,
            String loaikh,
            String kieutien,
            String mabc,
            String khdb,
            String hinhthuckhoa,
            String psDonvi) {
        String s = "begin ?:= " + getSysConst("FuncSchema") + "QLN_KMM.tao_ds_kmtd(" + "'" + kyhoadon + "'," + "'" + thang + "'," + "'danhba_dds_pttb'," + dvvt + "," + kieukhoa + "," + kieunhac + "," + ngaygh + "," + tienMin + "," + tienMax + "," + thangMin + "," + thangMax + "," + "'" + manv + "'," + "'" + loaikh + "'," + kieutien + "," + "'somay'," + "'" + mabc + "'," + khdb + "," + hinhthuckhoa + "," + "'" + psDonvi + "'," + "'" + getUserVar("sys_dataschema") + "'," + "'" + getUserVar("userID") + "'" + "); end;";
        return s;
    }

    public String ref_layds_kmtd(
            String psKieu,
            String psMaBC,
            String loai_tb,
            String ht_khoa,
            String psPageNum,
            String psPageRec) {
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/khoamomay/khoamaytudong/ajax_ds_chuakhoa" + "&" + CesarCode.encode("kieu") + "=" + psKieu + "&" + CesarCode.encode("ma_bc") + "=" + psMaBC + "&" + CesarCode.encode("dvvt") + "=" + loai_tb + "&" + CesarCode.encode("hinhthuckhoa") + "=" + ht_khoa + "&" + CesarCode.encode("page_num") + "=" + psPageNum + "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
    }

    public String value_xoa_tbs_kmtd(
            String psds_thuebao, String pihinhthuckhoa, String loai_tb, String ma_bc) {
        String s = "begin ?:=" + getSysConst("FuncSchema") + "qln_kmm.xoa_ds_kmtd(" + "'" + psds_thuebao + "'" + ",'" + pihinhthuckhoa + "'" + ",'" + loai_tb + "'" + ",'" + ma_bc + "'" + ",'" + getUserVar("sys_dataschema") + "'," + "'" + getUserVar("userID") + "'" + ");" + " end;";
        return s;
    }

    public String value_khoamay_tudong(
            String pi_DVVT,
            String ps_MA_BC,
            String pn_Hinh_Thuc_Khoa) {
        String s = "begin ?:=" + getSysConst("FuncSchema") + "qln_kmm.khoamay_tudong(" + "'" + pi_DVVT + "'," + "'" + ps_MA_BC + "'," + "'" + pn_Hinh_Thuc_Khoa + "'," + "'" + getUserVar("sys_dataschema") + "'," + "'" + getUserVar("userID") + "'); end;";
        return s;
    }

    public String ds_theodoi_kmm(
            String pikieuds,
            String piloaitb,
            String pidieukienloc,
            String pstungay,
            String psdenngay,
            String pidvql_id,
            String psPageNum,
            String psPageRec) {
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/khoamomay/theodoichemmay/ajax_lay_ds" + "&" + CesarCode.encode("kieuds") + "=" + pikieuds + "&" + CesarCode.encode("loaitb") + "=" + piloaitb + "&" + CesarCode.encode("dkloc") + "=" + pidieukienloc + "&" + CesarCode.encode("tungay") + "=" + pstungay + "&" + CesarCode.encode("denngay") + "=" + psdenngay + "&" + CesarCode.encode("dvql_id") + "=" + pidvql_id + "&" + CesarCode.encode("page_num") + "=" + psPageNum + "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
    }

    public String value_capnhat_kmm(
            String ps_DanhSach_TB, String psTrangThai, String psKhoaMay, String pi_LoaiDB) {
        String s = "begin ?:= " + getSysConst("FuncSchema") + "QLN_KMM.capnhat_theodoi_kmm(" + "'" + ps_DanhSach_TB + "'" + ",'" + psTrangThai + "'" + ",'" + psKhoaMay + "'" + ",'" + pi_LoaiDB + "'" + ",'" + getUserVar("sys_dataschema") + "'" + ",'" + getUserVar("userID") + "'" + "); end;";
        return s;
    }

    public String value_luulichsu_kmm(
            String ps_DanhSach_TB,
            String psKieuDs) {
        String s = "begin ?:= " + getSysConst("FuncSchema") + "QLN_KMM.luulichsu_kmm(" + "'" + ps_DanhSach_TB + "'," + psKieuDs + ",'" + getUserVar("sys_dataschema") + "'" + ",'" + getUserVar("userID") + "'" + "); end;";
        return s;
    }

    public String value_khoamay_tb(
            String psDsMaTB,
            String pi_DVVT,
            String ps_MA_BC,
            String pn_Hinh_Thuc_Khoa) {
        String s = "begin ?:=" + getSysConst("FuncSchema") + "qln_kmm.khoamay_theo_tb(" + "'" + psDsMaTB + "'," + "'" + pi_DVVT + "'," + "'" + ps_MA_BC + "'," + "'" + pn_Hinh_Thuc_Khoa + "'," + "'" + getUserVar("sys_dataschema") + "'," + "'" + getUserVar("userID") + "'); end;";
        return s;
    }

    public String value_them_tbs_kmm(
            String psDVVT,
            String psMaBC,
            String psDsMaTB,
            String psHinhThucKM,
            String psDonviQL) {
        String s = "begin ?:=" + getSysConst("FuncSchema") + "qln_kmm.them_tb_kmm(" + "'" + psDVVT + "'," + "'" + psMaBC + "'," + "'" + psDsMaTB + "'," + "'" + psHinhThucKM + "'," + "'" + psDonviQL + "'," + "'" + getUserVar("sys_dataschema") + "'," + "'" + getUserVar("userID") + "'); end;";
        return s;
    }
    /*Cac ham cua Mo may*/

    public String value_tao_ds_mmtd(
            String kyhoadon,
            String loai_tb,
            String kieunhac,
            String tien,
            String ma_bc) {
        String s = "begin ?:= " + getSysConst("FuncSchema") + "QLN_KMM.tao_ds_mmtd(" + "'" + kyhoadon + "'" + "," + loai_tb + "," + kieunhac + "," + tien + ",'" + ma_bc + "'," + "'" + getUserVar("sys_dataschema") + "'," + "'" + getUserVar("userID") + "'); end;";
        return s;
    }

    public String doc_ds_momay(
            String loai_tb,
            String ma_bc,
            String psPageNum,
            String psPageRec) {
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/khoamomay/momaytudong/ajax_tao_ds" + "&" + CesarCode.encode("loai_tb") + "=" + loai_tb + "&" + CesarCode.encode("ma_bc") + "=" + ma_bc + "&" + CesarCode.encode("page_num") + "=" + psPageNum + "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
    }

    public String value_momay_tudong(
            String psDsMaTB,
            String psLoai_TB,
            String ma_bc) {
        String s = "begin ?:= " + getSysConst("FuncSchema") + "QLN_KMM.momay_tudong(" + "'" + psDsMaTB + "'" + ",'" + psLoai_TB + "'" + ",'" + ma_bc + "'" + ",'" + getUserVar("sys_dataschema") + "'" + ",'" + getUserVar("userID") + "'); end;";
        return s;

    }

    public String value_xoa_tbs_mmtd(
            String psDsMaTB,
            String ma_bc,
            String loai_tb) {
        String s = "begin ?:= " + getSysConst("FuncSchema") + "QLN_KMM.xoa_ds_mmtd(" + "'" + psDsMaTB + "'" + ",'" + ma_bc + "'" + ",'" + getUserVar("sys_dataschema") + "'," + "'" + getUserVar("userID") + "'); end;";
        return s;
    }

    public String value_them_tbs_mmtd(
            String psDVVT,
            String psMaBC,
            String psDsMaTB,
            String psHinhThucKM,
            String psDonviQL,
            String psChukyno) {
        String s = "begin ?:=" + getSysConst("FuncSchema") + "qln_kmm.them_tb_mmtd(" + "'" + psDVVT + "'," + "'" + psMaBC + "'," + "'" + psDsMaTB + "'," + "'" + psHinhThucKM + "'," + "'" + psDonviQL + "'," + "'" + psChukyno + "'," + "'" + getUserVar("sys_dataschema") + "'," + "'" + getUserVar("userID") + "'); end;";
        return s;
    }
    
    public String doc_layds_kmm(
            String psKyhoadon,
            String psKieuno,
            String psMa_nv,
            String psNgayphat,
            String psKhDB,
            String psKhDBT,
            String psKhUN,
            String psKhKMYC,
            String psLoaiKH,
            String psThangnotu,
            String psThangnoden,
            String psTiennotu,
            String psTiennoden,
            String psMaCQ,
            String psMa_kh,
            String psMa_tb,
            String psTenTT,
            String psDiachiTT,
            String psPageNum,
            String psPageRec
          ){
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/ajax_thuebao" 
                    + "&" + CesarCode.encode("Kyhoadon") + "=" +psKyhoadon 
                    + "&" + CesarCode.encode("Kieuno") + "=" + psKieuno
                    + "&" + CesarCode.encode("Ma_nv") + "=" + psMa_nv
                    + "&" + CesarCode.encode("Ngayphat") + "=" + psNgayphat
                    + "&" + CesarCode.encode("KhDB") + "=" + psKhDB
                    + "&" + CesarCode.encode("KhDBT") + "=" + psKhDBT
                    + "&" + CesarCode.encode("KhUN") + "=" + psKhUN
                    + "&" + CesarCode.encode("KhKMYC") + "=" + psKhKMYC
                    + "&" + CesarCode.encode("LoaiKH") + "=" + psLoaiKH
                    + "&" + CesarCode.encode("Thangnotu") + "=" + psThangnotu
                    + "&" + CesarCode.encode("Thangnoden") + "=" + psThangnoden
                    + "&" + CesarCode.encode("Tiennotu") + "=" + psTiennotu
                    + "&" + CesarCode.encode("Tiennoden") + "=" + psTiennoden
                    + "&" + CesarCode.encode("MaCQ") + "=" + psMaCQ
                    + "&" + CesarCode.encode("Ma_kh") + "=" + psMa_kh
                    + "&" + CesarCode.encode("Ma_tb") + "=" + psMa_tb
                    + "&" + CesarCode.encode("TenTT") + "=" + psTenTT
                    + "&" + CesarCode.encode("DiachiTT") + "=" + psDiachiTT
                    + "&" + CesarCode.encode("PageNum") + "=" + psPageNum
                    + "&" + CesarCode.encode("PageRec") + "=" + psPageRec;
    }
    public String doc_layds_kmm(
            String psKmmID,
            String psKyhoadon,
            String psPageNum,
            String psPageRec
          ){
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/caclankhoa/ajax_dakhoa" 
                    + "&" + CesarCode.encode("Kyhoadon") + "=" +psKyhoadon 
                    + "&" + CesarCode.encode("kmm_id") + "=" + psKmmID
                    + "&" + CesarCode.encode("page_num") + "=" + psPageNum
                    + "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
    }
    public String them_khdb(
    	String psDsMaKH,
    	String psDsSomay,
    	String psKyhoadon,
    	String psLoai
    )
    {
    	String return_="begin ?:=" + getSysConst("FuncSchema") + "QLN_KMM.them_khdb("
    		 + "'" + psDsMaKH + "'" 
    		 + ",'" + psDsSomay + "'"
    		 + ",'" + psKyhoadon + "'"
    		 + ",'" + psLoai + "'"
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	System.out.println(return_);
    	return return_;
    }
    public String them_khdb(
    	String psLoaiKHDB,
    	String psKyhoadon,
    	String psMaKH,
    	String psMaTB,
    	String psTrangthai,
    	String psNguoiYC,
    	String psLoai
    )
    {
    	String return_="begin ?:=" + getSysConst("FuncSchema") + "QLN_KMM.them_khdb("
    		 + "'" + psLoaiKHDB + "'" 
    		 + ",'" + psKyhoadon + "'"
    		 + ",'" + psMaKH + "'"
    		 + ",'" + psMaTB + "'"
    		 + ",'" + psTrangthai + "'"
    		 + ",'" + psNguoiYC + "'"
    		 + ",'" + psLoai + "'"
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	System.out.println(return_);
    	return return_;
    }
    public String doc_layds_khdb(
            String psLoaiKHDB,
            String psKyhoadon,
            String psMa_kh,
            String psMa_tb,
            String psTenTT,
            String psDiachiTT,
            String psLoai,
            String psPageNum,
            String psPageRec
          ){
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/khdb/ajax_dskhdb" 
            + "&" + CesarCode.encode("loaikhdb") + "=" +psLoaiKHDB 
            + "&" + CesarCode.encode("kyhoadon") + "=" +psKyhoadon 
            + "&" + CesarCode.encode("ma_kh") + "=" +psMa_kh 
            + "&" + CesarCode.encode("ma_tb") + "=" +psMa_tb
            + "&" + CesarCode.encode("ten_tt") + "=" +psTenTT 
            + "&" + CesarCode.encode("diachi_tt") + "=" +psDiachiTT
            + "&" + CesarCode.encode("loai") + "=" +psLoai 
            + "&" + CesarCode.encode("page_num") + "=" +psPageNum
            + "&" + CesarCode.encode("page_rec") + "=" +psPageRec;
	}
	public String xoa_khdb(
		String psLoaiKHDB,
		String psDsSomay,
		String psLoai
	){
		String return_="declare ret varchar2(1000); begin begin"
			 + " delete from "+getUserVar("sys_dataschema")+psLoai+" where "
			 + " ma_tb in ("+psDsSomay+") and nvl(kyhoadon,'0')='"+psLoaiKHDB+"'; "
			 + " ret:='1'; exception when others then "
			 + " ret:=TO_CHAR (SQLCODE)||': '||SQLERRM; end; ? := ret; end;";
    	System.out.println(return_);
    	return return_;
	}
	public String rec_ttkhachhang(
		String psKyhoadon,
		String psMaKH,
		String psMaTB
	){
		String s="select kh.ma_kh,db.somay,kh.ten_tt,kh.diachi_tt from "
			+getUserVar("sys_dataschema")+"khachhangs_"+psKyhoadon+" kh, "
			+"(select max(somay) somay,ma_kh from "+getUserVar("sys_dataschema")+
				"danhba_dds_"+psKyhoadon;
		if (psMaTB!="") s+=" where '"+psMaTB+"'=somay";
		s+=" group by ma_kh) db where kh.ma_kh=db.ma_kh";
		if (psMaKH!="") s+=" and kh.ma_kh='"+psMaKH+"'";
		return s;
	}
	public String value_kmm_theodanhsach(
        String psKyhoadon,
        String psDsSomay,
        String psDsMaKH,
        String psDsSotien,
        String psGoiDen,
        String psGoiDi,
        String pskieukhoa
	){
		String return_="begin ?:=" + getSysConst("FuncSchema") + "QLN_KMM.kmm_theodanhsach("
    		 + "'" + psKyhoadon + "'" 
    		 + ",'" + psDsSomay + "'"
    		 + ",'" + psDsMaKH + "'"
    		 + ",'" + psDsSotien + "'"
    		 + ",'" + psGoiDen + "'"
    		 + ",'" + psGoiDi + "'"
    		 + ",'" + pskieukhoa + "'"
    		 + ",'" + getUserVar("sys_dataschema") + "'" 
    		 + ",'" + getUserVar("userID") + "'); end;";
    	System.out.println(return_);
    	return return_;
	}
	 public String doc_tracuu_kmm(
            String psKyhoadon,
            String psKMM,
            String psChieu,
            String psLankhoa,
            String psNgaykhoa,
            String psNguoikhoa,
            String psMaKH,
            String psMaTB,
            String psTenTT,
            String psPageNum,
            String psPageRec
          ){
        return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/qltn_kmm/tracuu/ajax_tracuu" 
                    + "&" + CesarCode.encode("chukyno") + "=" +psKyhoadon 
                    + "&" + CesarCode.encode("kmm") + "=" + psKMM
                    + "&" + CesarCode.encode("chieu") + "=" + psChieu
                    + "&" + CesarCode.encode("lankhoa") + "=" + psLankhoa
                    + "&" + CesarCode.encode("ngaykhoa") + "=" + psNgaykhoa
                    + "&" + CesarCode.encode("nguoi_th") + "=" + psNguoikhoa
                    + "&" + CesarCode.encode("ma_kh") + "=" + psMaKH
                    + "&" + CesarCode.encode("ma_tb") + "=" + psMaTB
                    + "&" + CesarCode.encode("ten_tt") + "=" + psTenTT
                    + "&" + CesarCode.encode("PageNum") + "=" + psPageNum
                    + "&" + CesarCode.encode("PageRec") + "=" + psPageRec;
    }
}
