package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import com.svcon.jdbf.*;
import javax.sql.RowSet;
import java.sql.*;
import java.util.zip.*;
import java.io.*;
import java.net.*;

public class qltn_common extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("QLTN Gach no");
		//funcschema=getSysConst("FuncSchema");
    	//dataschema=getUserVar("sys_dataschema");
    	//userid=getUserVar("userID");		
	}
	public String doc_tuyenthu(
		String psDonviQL
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_lay_ds_nv"
  			+ "&" + CesarCode.encode("donviql_id") + "=" + psDonviQL;
	}
	public String doc_ds_khachhang(
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_ds_KHACHHANGS_s"
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String doc_ds_thuebao(
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_ds_thuebao_s"
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String doc_tim_khachhang(
		String psSomay,
		String psTen_TB,
		String psDiachi_CT,
		String psMa_KH,
		String psCoquan,
		String psDiachi_KH,
		String psMa_TT,
		String psTen_TT,
		String psDiachi_TT,
		String psDonvi,
		String psMa_nv,		
		String psChukyno,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_tim_KHACHHANGS_s"
			+ "&" + CesarCode.encode("SOMAY") + "=" + psSomay
			+ "&" + CesarCode.encode("TEN_TB") + "=" + psTen_TB
			+ "&" + CesarCode.encode("DIACHI_CT") + "=" + psDiachi_CT
			+ "&" + CesarCode.encode("MA_KH") + "=" + psMa_KH
			+ "&" + CesarCode.encode("COQUAN") + "=" + psCoquan
			+ "&" + CesarCode.encode("DIACHI_KH") + "=" + psDiachi_KH
			+ "&" + CesarCode.encode("MA_TT") + "=" + psMa_TT
			+ "&" + CesarCode.encode("TEN_TT") + "=" + psTen_TT
			+ "&" + CesarCode.encode("DIACHI_TT") + "=" + psDiachi_TT
			+ "&" + CesarCode.encode("DONVI") + "=" + psDonvi
			+ "&" + CesarCode.encode("ma_nv") + "=" + psMa_nv
			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno	
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String doc_tim_thuebao(
		String psSomay,
		String psTen_TB,
		String psDiachi_CT,
		String psMa_KH,
		String psCoquan,
		String psDiachi_KH,
		String psMa_TT,
		String psTen_TT,
		String psDiachi_TT,
		String psDonvi,
		String psMa_nv,	
		String psChukyno,	
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/common/ajax_tim_thuebao_s"
			+ "&" + CesarCode.encode("SOMAY") + "=" + psSomay
			+ "&" + CesarCode.encode("TEN_TB") + "=" + psTen_TB
			+ "&" + CesarCode.encode("DIACHI_CT") + "=" + psDiachi_CT
			+ "&" + CesarCode.encode("MA_KH") + "=" + psMa_KH
			+ "&" + CesarCode.encode("COQUAN") + "=" + psCoquan
			+ "&" + CesarCode.encode("DIACHI_KH") + "=" + psDiachi_KH
			+ "&" + CesarCode.encode("MA_TT") + "=" + psMa_TT
			+ "&" + CesarCode.encode("TEN_TT") + "=" + psTen_TT
			+ "&" + CesarCode.encode("DIACHI_TT") + "=" + psDiachi_TT
			+ "&" + CesarCode.encode("DONVI") + "=" + psDonvi
			+ "&" + CesarCode.encode("ma_nv") + "=" + psMa_nv
			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String value_capnhat_seri
	       (String seri
           ,String quyen
           ,String soseri)
	{		
	 	return  "begin ?:="+getSysConst("FuncSchema")+"qltn.capnhat_seri('"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"','"+seri+"','"+quyen+"','"+soseri+"'); end;";	 
	}
	public String laytt_seri()
	{
		return "select seri,quyen,soseri from "+getUserVar("sys_dataschema")+"nguoigachnos where nguoigach='"+getUserVar("userID")+"'";
	}
	public String doc_ds_chukyno(
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_gachno_chungtu/ajax_ds_chukyno"
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String rec_tt_khachhang_doituyen(
		String psUserInput,
		String psDonviQL
	)
	{
		return "begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.laytt_kh_doituyen('"+psUserInput+"',"+psDonviQL+",'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
	}
	public String value_capnhat_congno(
		String psMaKH,
        String psTenTT,
        String psDiachiTT,
        String psMSThue,
        String psSodaidien,
        String psMaBC,
        String psMaNV_Cu,
        String psChukyno,
        String psMaNV_Moi,
        String psThang,
        String psChuyenTTKH,
        String psCMTND
	)
	{
		return "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.chuyentuyen_congno_kh('"+psMaKH
			+"','"+psTenTT+"','"+psDiachiTT+"','"+psMSThue+"','"+psSodaidien+"','"+psMaBC+"','"
			+psMaNV_Cu+"' ,'"+psChukyno+"','"+psMaNV_Moi+"','"+psThang+"','"
			+psChuyenTTKH+"','"+psCMTND+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"') ; end;";
	}
	public String value_check_trangthai(String psTrangthai)
	{
		String s= "begin ? := "
			+getSysConst("FuncSchema")+"qltn_donno.check_trang_thai('"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"','"
			+psTrangthai
			+"'); end; ";
		System.out.println(s);
		return s;
	}
	public String value_taobang_kyhoadon(String psKyhoadon)
	{
		String s= "begin ? := "
			+getSysConst("FuncSchema")+"qltn_donno.tao_bang_all('"
			+psKyhoadon+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")
			+"'); end; ";
		System.out.println(s);
		return s;
	}
	public String value_chotno_cuoiky(String psKyhoadon)
	{
		String s= "begin ? := "
			+getSysConst("FuncSchema")+"qltn_donno.chotno_cuoiky('"
			+psKyhoadon+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")
			+"'); end; ";
		System.out.println(s);
		return s;
	}
	public String value_agent_id(String psSomay)
	{
		return "begin ? := "+getSysConst("FuncSchema")+
			"qltn_tracuu.lay_agent_parameter('"+psSomay+"','agent_id'); end;";
	}
	public String doc_tracuu_phieuhuy(
		String psForm,
		String psTungay,
		String psDenngay,
		String psChukyno,
		String psMaKH,
		String psMaTB,
		String psSeri,
		String psPageNum,
		String psPageRec
	)
	{			
		int psForm_=1;
		if (psForm=="ajax_vat") psForm_=1;
		if (psForm=="ajax_cuocnong") psForm_=2;
		if (psForm=="ajax_chuyenkhoan") psForm_=3;
		if (psForm=="ajax_kyquy") psForm_=4;
		if (psForm=="ajax_thuho") psForm_=5;
		if (psForm=="ajax_chicuocnong") psForm_=6;
		if (psForm=="ajax_chikyquy") psForm_=7;
		
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_tienmat/phieuhuys/"+psForm
			+ "&" + CesarCode.encode("form") + "=" + psForm_
			+ "&" + CesarCode.encode("tungay") + "=" + psTungay
			+ "&" + CesarCode.encode("denngay") + "=" + psDenngay
			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
			+ "&" + CesarCode.encode("ma_kh") + "=" + psMaKH
			+ "&" + CesarCode.encode("ma_tb") + "=" + psMaTB
			+ "&" + CesarCode.encode("soseri") + "=" + psSeri
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String value_capnhat_seri_phieutra(
		String psKyhoadon,
		String psDsPhieuID,
		String psDsMaKH,
		String psDsSoeriCu,
		String psDsSoeriMoi,
		String psDsSeri
    )
    {
    	String a= "begin ? :="+getSysConst("FuncSchema")+"qltn.capnhat_seri_phieutra("
			+"'"+psKyhoadon+"',"
			+"'"+psDsPhieuID+"',"
			+"'"+psDsMaKH+"',"
			+"'"+psDsSoeriCu+"',"
			+"'"+psDsSoeriMoi+"',"	
			+"'"+psDsSeri+"',"			
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
    }
    public String value_capnhat_seri_phieuthu(
		String psKyhoadon,
		String psbang,
		String psloai,
		String psdsphieu,
		String psdssoseri
    )
    {
    	String a= "begin ? :="+getSysConst("FuncSchema")+"qltn.capnhat_seri_phieuthu("
			+"'"+psKyhoadon+"',"
			+"'"+psbang+"',"
			+"'"+psloai+"',"
			+"'"+psdsphieu+"',"
			+"'"+psdssoseri+"',"		
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
    }
    public String value_upd_trangthai_ckn(
		String chukynos,
		String dis_ckns
	)
	{
		try {
			String path=getSysConst("FileUploadDir")+"NoDK\\";
			String ftp=getSysConst("FtpDir");
			String chukysau="";
			if (ftp=="") ftp="D:\\CuocVinaphone\\";
			String ccs=getUserVar("sys_dataschema");
			ftp	+= ccs.replace(".","");
			String count=reqValue("declare a number; begin select instr('"+chukynos+"',"+getSysConst("FuncSchema")
				+"qltn.chukyno_dangso("+getSysConst("FuncSchema")+"qltn.lay_ckn_hientai('"+getUserVar("sys_dataschema")
				+"','"+getUserVar("userID")+"'),'','')+1) into a from dual; ? := a; end;");
			if (Integer.parseInt(count)>0){
				String psKyhoadon=reqValue("declare a varchar2(100); begin select "+getSysConst("FuncSchema")+"qltn.lay_ckn_hientai('"+getUserVar("sys_dataschema")
					+"','"+getUserVar("userID")+"') into a from dual; ? := a; end;");
				if (Integer.parseInt(psKyhoadon.substring(0,2))==12){
					chukysau="01"+String.valueOf(Integer.parseInt(psKyhoadon.substring(2))+1);
					ftp+="/"+chukysau;
				}else if (Integer.parseInt(psKyhoadon.substring(0,2))>=9){
					chukysau=String.valueOf(Integer.parseInt(psKyhoadon.substring(0,2))+1)
						+psKyhoadon.substring(2);
					ftp+="/"+chukysau;
				}else{
					chukysau="0"+String.valueOf(Integer.parseInt(psKyhoadon.substring(0,2))+1)
						+psKyhoadon.substring(2);
					ftp+="/"+chukysau;
				}
				System.out.print("Using Dbf Jdbc: "+export(path+ccs.replace(".","")+"\\"+chukysau+"\\"+"NODK_"+chukysau+".DBF",chukysau));
				doZip(path+ccs.replace(".","")+"\\"+chukysau+"\\"+"NODK_"+chukysau+".DBF",path+ccs.replace(".","")+"\\"+chukysau+"\\"+"NODK_"+chukysau+".ZIP");
			}
		} catch (Exception ex){
			System.out.println(ex.getMessage());
		}
		return "begin ?:="+getSysConst("FuncSchema")+"qltn.capnhat_trangthai_chukyno('"+chukynos+"','"+dis_ckns+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
	}
	public char GetType(String types)
    {
        char return_value='C';
        if (types=="VARCHAR2")
        	return_value='C';
        else if(types=="NUMBER")
        	return_value='N';
        else if (types=="DATE")
        	return_value='D';
        else
           return_value='C';
        return return_value;
    }
    public void doZip(String filename,String zipfilename) throws IOException {
    	
        try {
            FileInputStream fis = new FileInputStream(filename);
            File file=new File(filename);
            byte[] buf = new byte[fis.available()];
            fis.read(buf,0,buf.length);
            
            CRC32 crc = new CRC32();
            ZipOutputStream s = new ZipOutputStream(
                    (OutputStream)new FileOutputStream(zipfilename));
            
            s.setLevel(6);
            
            ZipEntry entry = new ZipEntry(file.getName());
            entry.setSize((long)buf.length);
            crc.reset();
            crc.update(buf);
            entry.setCrc( crc.getValue());
            s.putNextEntry(entry);
            s.write(buf, 0, buf.length);
            s.finish();
            s.close();
            fis.close();
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String export(String psPath,String psKyhoadon)
  	{	
  		try{
	  		String sSql="begin ? := "+getSysConst("FuncSchema")
	  			+"QLTN_DONNO.laydulieu_cuoiky('"+psKyhoadon+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
	  		System.out.println(sSql);
	  		RowSet rows = reqRSet(sSql);
	  		ResultSetMetaData metas=rows.getMetaData();
	  		com.svcon.jdbf.JDBField[] fields = new com.svcon.jdbf.JDBField[metas.getColumnCount()];
	  		for (int i=1;i<=metas.getColumnCount();i++){
	  			String cname=metas.getColumnName(i);
	  			char ctype=GetType(metas.getColumnTypeName(i));
	  			if (cname.length()>9) cname=cname.substring(0,9);
	  			int clength=metas.getColumnDisplaySize(i);
	  			if (ctype=='N')
	  				clength=20;
	  			else if (ctype=='D')
	  				clength=8;
	  			else if (clength>=254)
	  				clength=253;
	  			fields[i-1]=new com.svcon.jdbf.JDBField(cname,ctype,clength,0);
	  		}
	  		com.svcon.jdbf.DBFWriter writer = new com.svcon.jdbf.DBFWriter(psPath,fields);
	  		Object[] record=new Object[metas.getColumnCount()];
  			while (rows.next()){
  				for (int j=1;j<=metas.getColumnCount();j++){
  					if (GetType(metas.getColumnTypeName(j))=='N'){
  						record[j-1]=new Integer(rows.getString(metas.getColumnName(j)));
  					}else{
  						record[j-1]=(rows.getString(metas.getColumnName(j)));
  					}
  				}
  				writer.addRecord(record);
  			}
	  		writer.close();
	  		return "Complete";
	  	}catch (Exception ex){
	  		return ex.getMessage();
	  	}
  	}
  	public void xCopy(String Host,int Port,String sFile) throws UnknownHostException{
  		try {
  			InetAddress host = InetAddress.getByName(Host);
		  	Socket socket = new Socket(host, Port);
			InputStream in = (InputStream)(new FileInputStream(sFile));
			OutputStream out = new BufferedOutputStream(socket.getOutputStream());
			byte[] buffer = new byte[in.available()];
			while (true) {
			int nBytes = in.read(buffer, 0, in.available());
			if (nBytes < 0)
			break;
				out.write(buffer, 0, nBytes);
			}
			out.flush();
			out.close();
			in.close();
		}catch (IOException ex){
			System.out.println(ex.getMessage());
		}
	}
	public String layds_tuyenthus(
		String psManv
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_thutukh/ajax_tuyenthus"
  			+ "&" + CesarCode.encode("manv") + "=" + psManv;
	}
	public String layds_nhanvientc(
		String psMabc
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_thutukh/ajax_nhanvientcs"
  			+ "&" + CesarCode.encode("mabc") + "=" + psMabc;
	}
	public String layds_khachhangs(
		String psMabc,
		String psManv,
		String psMat
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn/qltn_thutukh/ajax_layds_khachhangs"
  			+ "&" + CesarCode.encode("mabc") + "=" + psMabc
			+ "&" + CesarCode.encode("manv") + "=" + psManv
			+ "&" + CesarCode.encode("mat") + "=" + psMat;
	}
	public String capnhat_thutu_kh(
		String psChukyno,
		String psDsMA_KH,
		String psDsSTT)
	{
		String s= "begin ? := "
			+getSysConst("FuncSchema")+"qltn_thtoan.capnhat_thutu_kh('"
			+psChukyno+"','"
			+psDsMA_KH+"','"
			+psDsSTT+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")
			+"'); end; ";
		return s;
		
	}
	public String capnhat_hinhthuctt(
    	String psDsHTTT,
    	String psDsUser,
    	String psXoaphieului,
    	String psGachnolui){
    		String users=psDsUser.replace(",","','");
    		String out_ = "declare out_ varchar2(1000); begin begin "
    			+ "	update "+getUserVar("sys_dataschema")+"nguoigachnos set "
    			+ "	HINHTHUCTT_IDS='"+psDsHTTT+"' where nguoigach in ('"
    			+ users +"'); "
    			+ " update "+getUserVar("sys_dataschema")+"users set "
    			+ " gachno_pre="+psGachnolui+",xoaphieu_pre="+psXoaphieului+" where name in ('"
    			+ users + "'); out_ := 1; "
    			+ " exception when others then "
    			+ " out_ := TO_CHAR (SQLCODE) || ': ' || SQLERRM; "
    			+ " end; ?:=out_; end;";
    		return out_;    		
    }
    public String value_chuyentuyen(
		String psKyhoadon,
        String psdsmakh,
        String psdsmanv_cu,
        String psdsmanv_moi,
        String psdsmat_cu,
        String psdsmat_moi,
        String pschuyentuyen
	)
	{
		return "begin ? :="+getSysConst("FuncSchema")+"qltn.chuyentuyen("+
			"'"+psKyhoadon+"',"+
			"'"+psdsmakh+"',"+
			"'"+psdsmanv_cu+"',"+
			"'"+psdsmat_cu+"',"+
			"'"+psdsmanv_moi+"',"+
			"'"+psdsmat_moi+"',"+
			"'"+pschuyentuyen+"',"+
			"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"') ; end;";
	}
}