package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class qltn_nhanchungtu extends NEOProcessEx 

{
  public void run() 
  {
    System.out.println("neo.qltn.qltn_tienmat was called");
  }
  
  
  public String capnhat_sec( String   funcSchema ,
							String	  pschukyno,
							String    psso_ct,
							String    psso_bienlai,
							String    pisotien,
							String    psloaitien_id,
							String    pstennh_phat,
							String    psdonvi_huong,
							String    psdiachi_dvh,
							String    pssotk_dvh,
							String    psnganhang_dvh,
							String    psdonvi_phat,
							String    psdiachi_dvp,
							String    pssotk_dvp,
							String    pitrangthai,
							String    psngaygui,
							String    psma_kh,
							String    psnguoi_cn ,
							String    psma_hd ,
							String    phieu_id,
							String 	  psloai_chuyenkhoan,
							String    psSchema ,
							String    psUserID 
							                 
							  ) 						  
							  
							  
 { 
 String s =  "begin ?:= "+funcSchema+"qltn_tracuu.capnhat_sec("
        +"'"+pschukyno+"'"
		+",'"+psso_ct+"'" 
		+","+psso_bienlai
        +","+pisotien
        +","+psloaitien_id
		+",'"+pstennh_phat+"'"
        +",'"+psdonvi_huong+"'"
        +",'"+psdiachi_dvh+"'"
        +","+pssotk_dvh
		+",'"+psnganhang_dvh+"'"
		+",'"+psdonvi_phat+"'"
		+",'"+psdiachi_dvp+"'"
        +","+pssotk_dvp
       	+","+pitrangthai
		+",'"+psngaygui+"'"
		+",'"+psma_kh+"'"
        +",'"+psnguoi_cn+"'"
	    +",'"+psma_hd+"'"
		+","+phieu_id
		+",'"+psloai_chuyenkhoan+"'"
	    +",'"+psSchema+"'"
        +",'"+psUserID+"'"
	    +"); "  
        +" end;" ;
		
		
    return s;

 }	
	
 
 	public String tracuu_thuho(
		String schema,
		String chukyno,
		String ma_khachhang,
		String ma_thuebao,
		String ma_sophieu,
		String thutungay,
		String thudenngay,
		String ma_tinh,
		
		String buucuc,
		String ma_quay,
		String ma_thungan
		)
		{	
		System.out.println("neo.xln was called-Updated 16/07/2007");    
		
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_thuho/ajax_tracuuthuho"
				+"&"+CesarCode.encode("chukyno")+"="+chukyno
				+"&"+CesarCode.encode("ma_kh")+"="+ma_khachhang
				+"&"+CesarCode.encode("ma_tb")+"="+ma_thuebao
				+"&"+CesarCode.encode("ma_phieu")+"="+ma_sophieu
				+"&"+CesarCode.encode("tungay")+"="+thutungay
				+"&"+CesarCode.encode("denngay")+"="+thudenngay
				+"&"+CesarCode.encode("ma_tinh")+"="+ma_tinh
				
				+"&"+CesarCode.encode("buucuc")+"="+buucuc
				+"&"+CesarCode.encode("ma_quay")+"="+ma_quay
				+"&"+CesarCode.encode("ma_thungan")+"="+ma_thungan
		; 	
		}

 
 
  	
		
	public String ds_sec(
		String schema,
		String chukyno,
		String ma_kh,
		String tungay,
		String denngay,
		String trangthai_id,
		String loai_chuyenkhoan
		)
		{	
			
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_nhanchungtu/ajax_ds_sec"
				+"&"+CesarCode.encode("chukyno")+"="+chukyno
				+"&"+CesarCode.encode("ma_kh")+"="+ma_kh
				+"&"+CesarCode.encode("tungay")+"="+tungay
				+"&"+CesarCode.encode("denngay")+"="+denngay
				+"&"+CesarCode.encode("trangthai_id")+"="+trangthai_id
				+"&"+CesarCode.encode("loai_chuyenkhoan")+"="+loai_chuyenkhoan
				; 	
		}
	public String doc_ds_chungtu(
		String schema,
		String chukyno,		
		String tungay,
		String denngay,
		String trangthai_id,
		String loai_chuyenkhoan,
		String taikhoan_dvp,		
		String nganhang_ph,
		String dv_phathanh,
		String loaitien_id
		)
		{	
			
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_gachno_chuyenkhoan/ajax_ds_chungtu"
				+"&"+CesarCode.encode("chukyno")+"="+chukyno				
				+"&"+CesarCode.encode("tungay")+"="+tungay
				+"&"+CesarCode.encode("denngay")+"="+denngay
				+"&"+CesarCode.encode("trangthai_id")+"="+trangthai_id
				+"&"+CesarCode.encode("loai_chuyenkhoan")+"="+loai_chuyenkhoan
				+"&"+CesarCode.encode("taikhoan_dvp")+"="+taikhoan_dvp
				+"&"+CesarCode.encode("nganhang_ph")+"="+nganhang_ph
				+"&"+CesarCode.encode("dv_phathanh")+"="+dv_phathanh
				+"&"+CesarCode.encode("loaitien_id")+"="+loaitien_id
				; 	
		}
		
		
	public String danhsach_phieuhuy(
		String schema,
		String chukyno
		)
		{	
			
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_thuho/ajax_phieuhuy"
				+"&"+CesarCode.encode("chukyno")+"="+chukyno
				; 	
		}	
	
public String themmoi_sec(  String   funcSchema ,
							String	  pschukyno,
							String    psso_ct,
							String    psso_bienlai,
							String    pisotien,
							String    psloaitien_id,
							String    pstennh_phat,
							String    psdonvi_huong,
							String    psdiachi_dvh,
							String    pssotk_dvh,
							String    psnganhang_dvh,
							String    psdonvi_phat,
							String    psdiachi_dvp,
							String    pssotk_dvp,
							String    pitrangthai,
							String    psngaygui,
							String    psma_kh,
							String    psnguoi_cn ,
							String    psma_hd ,
							String 	  psloai_chuyenkhoan,
							String    psSchema ,
							String    psUserID 
							                 
							  ) 						  
							  
							  
 { 
 String s =  "begin ?:= "+funcSchema+"qltn_tracuu.themmoi_sec("
        +"'"+pschukyno+"'"
		+",'"+psso_ct+"'" 
		+","+psso_bienlai
        +","+pisotien
        +","+psloaitien_id
		+",'"+pstennh_phat+"'"
        +",'"+psdonvi_huong+"'"
        +",'"+psdiachi_dvh+"'"
        +","+pssotk_dvh
		+",'"+psnganhang_dvh+"'"
		+",'"+psdonvi_phat+"'"
		+",'"+psdiachi_dvp+"'"
        +","+pssotk_dvp
       	+","+pitrangthai
		+",'"+psngaygui+"'"
		+",'"+psma_kh+"'"
        +",'"+psnguoi_cn+"'"
	    +",'"+psma_hd+"'"
	    +",'"+psloai_chuyenkhoan+"'"	    	
	    +",'"+psSchema+"'"
        +",'"+psUserID+"'"
	    +"); "  
        +" end;" ;				
    return s;

 }	
	
	
	
public String suadoi_phieuthuhotinh( String funcSchema 
									,String pschukyno 
									,String psht_thanhtoan_id 
								    ,String psloaitien_id 
								    ,String psthungan_id 
								    ,String psten_khachhang 
									,String psma_thuebao
								    ,String psdiachi 
								    ,String pitinh_id 
								    ,String psmaso_thue
								    ,String pisotien 
								    ,String psphieu_id
								    ,String psghichu 
									,String psSchema
								    ,String psUserID 
								                    
							  ) 
										  
							  
							  
							  
 { 
 String s =  "begin ?:= "+funcSchema+"qltn_tracuu.capnhat_phieuthuho_tinhkhac("
        +"'"+pschukyno+"'"
		+","+psht_thanhtoan_id 
		+","+psloaitien_id
		+","+pisotien
        +",'"+psthungan_id+"'"
		+",'"+psma_thuebao+"'"
        +",'"+psten_khachhang+"'"
		+",'"+psdiachi+"'"
        +","+pitinh_id
		+",'"+psmaso_thue+"'"
		+",'"+psphieu_id+"'"
        +",'"+psghichu+"'"
        +",'"+psSchema+"'"
        +",'"+psUserID+"'"
	    +"); "  
        +" end;" ;
		
    return s;

 }	
	
	 public String xoaphieu
							( String funcschema
							 ,String psphieu_id						        
							 ,String psloai_chuyenkhoan
                             ,String psschema
							 ,String psuserID							   
							  ) 
	{  
 	
 	  String  s=  "begin ?:= "+funcschema+"qltn_tracuu.xoasec("
 	    +"'"+psphieu_id+"'"
 	    +",'"+psloai_chuyenkhoan+"'"	
		+",'"+psschema+"'"
		+",'"+psuserID+"'"
		+");"
        +"end;"
        ;
		   return s;	
	}
	
	
	
	public String dsphieuhuy(
		String schema,
		String ma_khachhang,
		String ma_thuebao,
		String ma_sophieu,
		String thutungay,
		String thudenngay,
		String ma_tinh,
		String buucuc,
		String ma_quay,
		String ma_thungan,
		String trangthai_id
		)
		{	
		System.out.println("neo.xln was called-Updated 16/07/2007");    
		
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_thuho/ajax_phieuhuy"
				+"&"+CesarCode.encode("ma_kh")+"="+ma_khachhang
				+"&"+CesarCode.encode("ma_tb")+"="+ma_thuebao
				+"&"+CesarCode.encode("ma_phieu")+"="+ma_sophieu
				+"&"+CesarCode.encode("tungay")+"="+thutungay
				+"&"+CesarCode.encode("denngay")+"="+thudenngay
				+"&"+CesarCode.encode("ma_tinh")+"="+ma_tinh
				+"&"+CesarCode.encode("buucuc")+"="+buucuc
				+"&"+CesarCode.encode("ma_quay")+"="+ma_quay
				+"&"+CesarCode.encode("ma_thungan")+"="+ma_thungan
				+"&"+CesarCode.encode("trangthai_id")+"="+trangthai_id
				; 	
		}
		
		public String RecTTKhachHang_ChuyenKhoan(String InMaKh)	
		{
			return "begin ? := "+getSysConst("FuncSchema")+"qltn_tracuu.tracuu_thongtin_KH_TB(null,null,null,'"+InMaKh+"',null,null,null,null,null,null,null,'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		}
	
}
