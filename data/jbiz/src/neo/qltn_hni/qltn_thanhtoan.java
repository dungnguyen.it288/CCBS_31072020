package neo.qltn_hni;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class qltn_thanhtoan extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("QLTN Gach no");
		//funcschema=getSysConst("FuncSchema");
    	//dataschema=getUserVar("sys_dataschema");
    	//userid=getUserVar("userID");		
	}
	public String value_thanhtoan4(
		String pskyhoadon,
		String psdsma_kh,
		String psdsma_tb,
		String psdschukyno,
		String psdstientra,
		String psdskieutra,
		String psdskhoanmuc,
		String psdskmck,
		String pichucnang_gachno,
		String pdngay_tt,
		String pihttt_id,
		String piloaitien_id,
		String piquydoi,
		String pinganhang_id,
		String pschungtu,
		String pdngaynganhang,
		String psgomhd,
		String psluotthanhtoan,
		String pstheokhoanmuc
	)
	{
		String s = "begin ? :="+getSysConst("FuncSchema")+"qltn_hni.thanhtoan4("+
			"'"+pskyhoadon+"',"+
			"'"+psdsma_kh+"',"+
			"'"+psdsma_tb+"',"+
			"'"+psdschukyno+"',"+
			"'"+psdstientra+"',"+
			"'"+psdskieutra+"',"+
			"'"+psdskhoanmuc+"',"+
			"'"+psdskmck+"',"+
			"'"+pichucnang_gachno+"',"+
			"to_date('"+pdngay_tt+"','DD/MM/YYYY'),"+
			"'"+pihttt_id+"',"+
			"'"+piloaitien_id+"',"+
			"'"+piquydoi+"',"+
			""+pinganhang_id+","+
			"'"+pschungtu+"',"+
			"to_date('"+pdngaynganhang+"','DD/MM/YYYY'),"+
			"'"+psgomhd+"',"+
			"'"+psluotthanhtoan+"',"+
			"'"+pstheokhoanmuc+"',"+
			"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		System.out.println(s);
		return s;
	}



	public String doc_layds_kqphat
	(
		String pskyhoadon,
		String psnguoinhan,
		String psma_kh,
		String psma_tb,
		String pstenkh,
		String psdiachi,
		String psma_nv,
		String psma_tuyen,
		String psngaycn_bd,
		String psngaycn_kt,
		String psngayphat_bd,
		String psngayphat_kt,
		String psketqua,
		String psghichu,
		String psloai,
		String pschukyno,
		String psgiaolai,
		String pspage_num,
		String pspage_rec		
	)		
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_cnketqua/ajax_dskq_phatphieu"
			+"&"+CesarCode.encode("pskyhoadon")+"="+pskyhoadon
			+"&"+CesarCode.encode("psnguoinhan")+"="+psnguoinhan
			+"&"+CesarCode.encode("psma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("psma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("pstenkh")+"="+pstenkh
			+"&"+CesarCode.encode("psdiachi")+"="+psdiachi
			+"&"+CesarCode.encode("psma_nv")+"="+psma_nv
			+"&"+CesarCode.encode("psma_tuyen")+"="+psma_tuyen
			+"&"+CesarCode.encode("psngaycn_bd")+"="+psngaycn_bd
			+"&"+CesarCode.encode("psngaycn_kt")+"="+psngaycn_kt
			+"&"+CesarCode.encode("psngayphat_bd")+"="+psngayphat_bd
			+"&"+CesarCode.encode("psngayphat_kt")+"="+psngayphat_kt
			+"&"+CesarCode.encode("psketqua")+"="+psketqua
			+"&"+CesarCode.encode("psghichu")+"="+psghichu
			+"&"+CesarCode.encode("ghichu")+"="+psghichu
			+"&"+CesarCode.encode("psloai")+"="+psloai
			+"&"+CesarCode.encode("psckn")+"="+pschukyno
			+"&"+CesarCode.encode("psgiaolai")+"="+psgiaolai
			+"&"+CesarCode.encode("page_num")+"="+pspage_num
			+"&"+CesarCode.encode("page_rec")+"="+pspage_rec;				
	}

	
	
	
	
	public String doc_layds_kqphat1
	(
		String pskyhoadon,
		String psnguoinhan,
		String psdonviql,
		String psbuucuc,
		String psma_kh,
		String psma_tb,
		String pstenkh,
		String psdiachi,
		String psma_nv,
		String psma_tuyen,
		String psngaycn_bd,
		String psngaycn_kt,
		String psngayphat_bd,
		String psngayphat_kt,
		String psketqua,
		String psghichu,
		String psloai,
		String pschukyno1,
		String pschukyno2,
		String psgiaolai,
		String pspage_num,
		String pspage_rec		
	)		
	{
		return "/main?"+ CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_cnketqua/ajax_tracuu"
			+"&"+CesarCode.encode("pskyhoadon")+"="+pskyhoadon
			+"&"+CesarCode.encode("psnguoinhan")+"="+psnguoinhan
			+"&"+CesarCode.encode("psdonviql")+"="+psdonviql
			+"&"+CesarCode.encode("psbuucuc")+"="+psbuucuc
			+"&"+CesarCode.encode("psbuucuc")+"="+psbuucuc
			+"&"+CesarCode.encode("psma_kh")+"="+psma_kh
			+"&"+CesarCode.encode("psma_tb")+"="+psma_tb
			+"&"+CesarCode.encode("pstenkh")+"="+pstenkh
			+"&"+CesarCode.encode("psdiachi")+"="+psdiachi
			+"&"+CesarCode.encode("psma_nv")+"="+psma_nv
			+"&"+CesarCode.encode("psma_tuyen")+"="+psma_tuyen
			+"&"+CesarCode.encode("psngaycn_bd")+"="+psngaycn_bd
			+"&"+CesarCode.encode("psngaycn_kt")+"="+psngaycn_kt
			+"&"+CesarCode.encode("psngayphat_bd")+"="+psngayphat_bd
			+"&"+CesarCode.encode("psngayphat_kt")+"="+psngayphat_kt
			+"&"+CesarCode.encode("psketqua")+"="+psketqua
			+"&"+CesarCode.encode("psghichu")+"="+psghichu
			+"&"+CesarCode.encode("ghichu")+"="+psghichu
			+"&"+CesarCode.encode("psloai")+"="+psloai
			+"&"+CesarCode.encode("psckn1")+"="+pschukyno1
			+"&"+CesarCode.encode("psckn2")+"="+pschukyno2
			+"&"+CesarCode.encode("psgiaolai")+"="+psgiaolai
			+"&"+CesarCode.encode("page_num")+"="+pspage_num
			+"&"+CesarCode.encode("page_rec")+"="+pspage_rec;				
	}


	public String value_themmoi_kqphat(
		String pstype,
		String idsua,	
		String pskyhoadon,
		String psma_kh,
		String psma_tb,
		String psma_nv,
		String psma_t,
		String psnguoinhap,
		String psloaict,
		String psngayphat,
		String psngaycn,
		String psluotphat,
		String psketqua,
		String pssotrave,
		String psnguoinhan,
		String psykien,
		String pskyphat,
		String psnoidung,
		String psma_bc,
		String psuserip
	)
	{
		String s = "begin ?:="+getSysConst("FuncSchema")+"qltn_hni.themmoi_kqphat('"
			+pstype+"','"
			+idsua+"','"
			+pskyhoadon+"','"
			+psma_kh+"','"
			+psma_tb+"','"
			+psma_nv+"','"
			+psma_t+"','"
			+psnguoinhap+"','"
			+psloaict+"','"
			+ psngayphat+"','"
			+psngaycn+"','"
			+psluotphat+"','"
			+psketqua+"','"
			+pssotrave+"','"
			+psnguoinhan+"','"
			+psykien+"','"
			+pskyphat+"','"
			+psnoidung+"','"
			+psma_bc+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")+"','"
			+psuserip+"'); end;";
		System.out.println("Luat:" +s);
		return s;
	}



	public String doc_nochitiet(
		String psMaKH,
		String psMaTB,		
		String psKyhoadon,
		String psLoaiTienID,
		String psTheokhoanmuc
	)
	{
  		String out_="/main?" + CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_tienmat/ajax_laytt_nochitiet"
  			+ "&" + CesarCode.encode("ma_khs") + "=" + psMaKH
  			+ "&" + CesarCode.encode("ma_tbs") + "=" + psMaTB
  			+ "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
  			+ "&" + CesarCode.encode("loaitien_ids") + "=" + psLoaiTienID
  			+ "&" + CesarCode.encode("theokhoanmuc") + "=" + psTheokhoanmuc;
  		System.out.println(out_);
  		return out_;
  	}
	
	public String doc_nochitiet_usd(
		String psMaKH,
		String psMaTB,		
		String psKyhoadon,
		String psLoaiTienID,
		String psTheokhoanmuc
	)
	{
  		String out_="/main?" + CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_tienmat/ajax_laytt_nochitiet_usd"
  			+ "&" + CesarCode.encode("ma_khs") + "=" + psMaKH
  			+ "&" + CesarCode.encode("ma_tbs") + "=" + psMaTB
  			+ "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
  			+ "&" + CesarCode.encode("loaitien_ids") + "=" + psLoaiTienID
  			+ "&" + CesarCode.encode("theokhoanmuc") + "=" + psTheokhoanmuc;
  		System.out.println(out_);
  		return out_;
  	}

	public String  lay_loaitien_id
	(
		String psMaKH,
		String psMaTB,		
		String psKyhoadon
		
	)
	{
  		
		String a= "begin ? :="+getSysConst("FuncSchema")+"qltn_hni.lay_loaitien_id_str("
			+"'"+psMaKH+"',"
			+"'"+psMaTB+"',"
			+"'"+psKyhoadon+"',"	
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;

  	}	


	public String doc_nodieuchinh(
		String psMaKH,
		String psMaTB,		
		String psKyhoadon,
		String psthangchuyenno,
		String psLoaiTienID
	)
	{
  		String out_="/main?" + CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_dieuchinh/ajax_laytt_nochitiet"
  			+ "&" + CesarCode.encode("ma_khs") + "=" + psMaKH
  			+ "&" + CesarCode.encode("ma_tbs") + "=" + psMaTB
  			+ "&" + CesarCode.encode("kyhoadon") + "=" + psKyhoadon
			+ "&" + CesarCode.encode("kychuyenno") + "=" + psthangchuyenno
  			+ "&" + CesarCode.encode("loaitien_ids") + "=" + psLoaiTienID;
  		System.out.println(out_);
  		return out_;
  	}
  	public String doc_layds_giaophieu(  		
		String psChukyno,
		String pskylayphieu,
		String psMaNV,
		String psMaT,
		String psDsTrangthai,
		String psPageNum,
		String psPageRec
  	)
  	{  		
  		String config_="ccbs_qltn_hni/qltn_giaohoadon/ajax_layds_giaophieu";  				
		return priv_doc_layds_giaophieu1("0",psChukyno,pskylayphieu,psMaNV,psMaT,"",psDsTrangthai,psPageNum,psPageRec,config_);
  	}



	public String priv_doc_layds_giaophieu1(
		String psLuotgiao,
		String psChukyno,
		String pskylayphieu,
		String psMaNV,
		String psMaT,
		String psNgayGiao,
		String psDsTrangthai,
		String psPageNum,
		String psPageRec,
		String psConfigFile
	)		
	{
		
		return "/main?" + CesarCode.encode("configFile")+"="+psConfigFile
  			+ "&" + CesarCode.encode("luotgiao_id") + "=" + psLuotgiao
  			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
			+ "&" + CesarCode.encode("kylayphieu") + "=" + pskylayphieu
			+ "&" + CesarCode.encode("ma_nv") + "=" + psMaNV
  			+ "&" + CesarCode.encode("ma_t") + "=" + psMaT
  			+ "&" + CesarCode.encode("ngaygiao") + "=" + psNgayGiao
  			+ "&" + CesarCode.encode("trangthai") + "=" + psDsTrangthai
			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}

  	public String doc_layds_giaophieu_gachno(  		

		String psLuotgiao,
		String psChukyno,
		String psMaNV,
		String psMaT,
		String psNgayGiao,
		String psDsTrangthai,
		String psPageNum,
		String psPageRec
  	)
  	{  		
  		String config_="ccbs_qltn_hni/qltn_thudaily/ajax_layds_giaophieu_theo_lg_de_gno";  				
		return priv_doc_layds_giaophieu(psLuotgiao,psChukyno,psMaNV,psMaT,psNgayGiao,psDsTrangthai,psPageNum,psPageRec,config_);
  	}
  	public String priv_doc_layds_giaophieu(
		String psLuotgiao,
		String psChukyno,
		String psMaNV,
		String psMaT,
		String psNgayGiao,
		String psDsTrangthai,
		String psPageNum,
		String psPageRec,
		String psConfigFile
	)		
	{
		
		return "/main?" + CesarCode.encode("configFile")+"="+psConfigFile
  			+ "&" + CesarCode.encode("luotgiao_id") + "=" + psLuotgiao
  			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
			+ "&" + CesarCode.encode("ma_nv") + "=" + psMaNV
  			+ "&" + CesarCode.encode("ma_t") + "=" + psMaT
  			+ "&" + CesarCode.encode("ngaygiao") + "=" + psNgayGiao
  			+ "&" + CesarCode.encode("trangthai") + "=" + psDsTrangthai
			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String rec_laytt_giaophieu(
		String psma_nv,
   		String psma_t,
   		String psluotgiao_id,
   		String psngaygiao   		
	){
		String out_="begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.laytt_giaophieu("
			+"'"+psma_nv+"',"
			+"'"+psma_t+"',"
			+"'"+psluotgiao_id+"',"
			+"'"+psngaygiao+"',"
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		System.out.println(out_);
		return out_;
	}

	public String rec_laytt_phat(
		String psuserinput,
		String pschukyno,
		String psloai_ct,
   		String psma_tb
   		
	){
		String out_="begin ?:="+getSysConst("FuncSchema")+"qltn_hni.laytt_kh_phatphieu("
			+"'"+psuserinput+"',"
			+"'"+pschukyno+"',"
			+"'"+psloai_ct+"',"
			+"'"+ psma_tb+"',"
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		System.out.println(out_);
		return out_;
	}
	

	public String rec_laytt_phat_vat01(
		String psuserinput,
		String pschukyno,
		String kyhoadon,
		String psma_nv,
		String sohd,
   		String psma_tb
   		
	){
		String out_="begin ?:="+getSysConst("FuncSchema")+"qltn_tracuu.laytt_kh_phatphieu_vat01("
			+"'"+psuserinput+"',"
			+"'"+pschukyno+"',"
			+"'"+kyhoadon+"',"
			+"'"+psma_nv+"',"
			+"'"+sohd+"',"
			+"'"+ psma_tb+"',"
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		System.out.println(out_);
		return out_;
	}

	public String rec_laytt_khcn(
		String psuserinput,
		String pschukyno,
   		String psma_tb
   		
	){
		String out_="begin ?:="+getSysConst("FuncSchema")+"qltn_hni.laytt_kh_cn("
			+"'"+psuserinput+"',"
			+"'"+pschukyno+"',"
			+"'"+ psma_tb+"',"
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		System.out.println(out_);
		return out_;
	}
	

	public String gachno_thutainha(
  		String pskyhoadon,
		String psdsma_kh,		
		String psdstientra,
		String psdskmck,
		String psngay_tt,
		String psloaitien,
		String psquydoi,
		String psma_nv,
		String psma_t,
		String pschucnang_gachno,
		String piluotthanhtoan	
	)
	{
		String a= "begin ? := "+getSysConst("FuncSchema")+"qltn_hni.thanhtoan3("
			+"'"+pskyhoadon+"',"
			+"'"+psdsma_kh+"',"
			+"'"+psdstientra+"',"	
			+"'"+psdskmck+"',"		
			+"'"+psngay_tt+"',"
			+"'"+psloaitien+"',"
			+"'"+psquydoi+"',"
			+"'"+psma_nv+"',"
			+"'"+psma_t+"',"
			+"'"+piluotthanhtoan+"',"
			+"'"+pschucnang_gachno+"',"			
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
	}

	public String gachno_chungtu(
  		String pskyhoadon,
		String psdsma_kh,		
		String psdstientra,
		String psdskmck,
		String psngay_tt,
		String psloaitien,
		String psquydoi,
		String pssoct,
		String pshttt,
		String pschucnang_gachno
	)
	{
		String a= "begin ? := "+getSysConst("FuncSchema")+"qltn_hni.thanhtoanck("
			+"'"+pskyhoadon+"',"
			+"'"+psdsma_kh+"',"
			+"'"+psdstientra+"',"
			+"'"+psdskmck+"',"		
			+"'"+psngay_tt+"',"
			+"'"+psloaitien+"',"
			+"'"+psquydoi+"',"
			+"'"+pssoct+"',"
			+"'"+pshttt+"',"
			+"'"+pschucnang_gachno+"',"			
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
	}
	
	public String value_xoaphieu(
		String pskyhoadon,
		String psdsphieu_id,
		String psdschukyno,
		String pskieuhuy,
		String psloaitien_id
	)
	{
		String a= "begin ? :="+getSysConst("FuncSchema")+"qltn_hni.xoaphieu("
			+"'"+pskyhoadon+"',"
			+"'"+psdsphieu_id+"',"
			+"'"+psdschukyno+"',"	
			+"'"+pskieuhuy+"',"	
			+"'"+psloaitien_id+"',"			
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
	}
	public String value_dieuchinh(
		String pskyhoadon,
		String psdsma_kh,
		String psdsma_tb,
		String psdschukyno,
		String psdstragoc,
		String psdstrathue,
		String psdskieutra,
		String psdskhoanmuc,
		String psdsdonviql_id,
		String pdngay_tt,
		String pihttt_id,
		String piloaitien_id,
		String pslydo
	)
	{
		String a= "begin ? :="+getSysConst("FuncSchema")+"qltn_hni.thanhtoan5("
			+"'"+pskyhoadon+"',"
			+"'"+psdsma_kh+"',"
			+"'"+psdsma_tb+"',"	
			+"'"+psdschukyno+"',"		
			+"'"+psdstragoc+"',"
			+"'"+psdstrathue+"',"
			+"'"+psdskieutra+"',"	
			+"'"+psdskhoanmuc+"',"
			+"'1',"
			+"'"+psdsdonviql_id+"',"
			+"to_date('"+pdngay_tt+"','DD/MM/YYYY'),"
			+"'"+pihttt_id+"',"	
			+"'"+piloaitien_id+"',"	
			+"'"+pslydo+"',"
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
	}
	public String value_xoaphieu_dc(
		String pskyhoadon,
		String psdsphieu_id,
		String psdschukyno,
		String pskieuhuy,
		String psloaitien_id
	)
	{
		String a= "begin ? :="+getSysConst("FuncSchema")+"qltn_hni.xoaphieu_dc("
			+"'"+pskyhoadon+"',"
			+"'"+psdsphieu_id+"',"
			+"'"+psdschukyno+"',"	
			+"'"+pskieuhuy+"',"	
			+"'"+psloaitien_id+"',"			
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
	}
		public String layds_kh_hd(
		String psuserinput,
		String psngaylap,
		String psbuucuc,
   		String psma_tb
   		
	){
		String out_="begin ?:="+getSysConst("FuncSchema")+"qltn_hni.laytt_kh_phat_hopdong("
			+"'"+psuserinput+"',"
			+"'"+psngaylap+"',"
			+"'"+psbuucuc+"',"
			+"'"+ psma_tb+"',"
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		System.out.println(out_);
		return out_;
	}
	public String doc_tracuu_kqphat(
		String pschukyno,
		String psdonviql,
		String psbuucuc,
		String psma_kh,
		String psma_tb,
		String pstentt,
		String psdiachitt,
		String psma_nv,
		String psma_tuyen,
		String psngaycn_bd,
		String psngaycn_kt,
		String psngayphat_bd,
		String psngayphat_kt,
		String psketqua,
		String psnguoitt,
		String psloai,
		String pschukyno1,
		String pschukyno2,
		String psgiaolai,
		String pspage_num,
		String pspage_rec
		)
		{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_cnketqua/ajax_tracuu"
		+ "&" + CesarCode.encode("chukyno") + "=" + pschukyno
		+ "&" + CesarCode.encode("donviql") + "=" + psdonviql
		+ "&" + CesarCode.encode("buucuc") + "=" + psbuucuc
		+ "&" + CesarCode.encode("ma_kh") + "=" + psma_kh
		+ "&" + CesarCode.encode("ma_tb") + "=" + psma_tb
		+ "&" + CesarCode.encode("tentt") + "=" + pstentt
	    + "&" + CesarCode.encode("diachitt") + "=" + psdiachitt
		+ "&" + CesarCode.encode("ma_nv") + "=" + psma_nv
		+ "&" + CesarCode.encode("ma_tuyen") + "=" + psma_tuyen
		+ "&" + CesarCode.encode("ngaycn_bd") + "=" + psngaycn_bd
		+ "&" + CesarCode.encode("ngaycn_kt") + "=" + psngaycn_kt
		+ "&" + CesarCode.encode("ngayphat_bd") + "=" + psngayphat_bd
		+ "&" + CesarCode.encode("ngayphat_kt") + "=" + psngayphat_kt
		+ "&" + CesarCode.encode("ketqua") + "=" + psketqua
		+ "&" + CesarCode.encode("nguoitt") + "=" + psnguoitt
		+ "&" + CesarCode.encode("loai") + "=" + psloai
		+ "&" + CesarCode.encode("chukyno1") + "=" + pschukyno1
		+ "&" + CesarCode.encode("chukyno2") + "=" + pschukyno2
		+ "&" + CesarCode.encode("giaolai") + "=" + psgiaolai
		+ "&" + CesarCode.encode("page_num") + "=" + pspage_num
		+ "&" + CesarCode.encode("page_rec") + "=" + pspage_rec;
		}
}