package neo.qltn;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class qltn_chemmay extends NEOProcessEx 

{
  public void run() 
  {
    System.out.println("neo.qltn.qltn_tienmat was called");
  }
  
   
	public String xoads_motd
							( String funcschema
							,String psds_thuebao	
							,String psschema
							,String psuserID							   
							  ) 
	{  
 	
 	  String  s=  "begin ?:= "+funcschema+"qltn_tracuu.mtd_xoadstb("
 	  	+"'"+psds_thuebao+"'"
		+",'"+psschema+"'"
		+",'"+psuserID+"'"
		+");"
        +"end;"
        ;
		   return s;	
	}
	
   
   
  	public String mochemtudong
		(
		String funcschema,
		String ma_bc,
		String loai_tb,
		String psschema,
		String psuserID
		)
		{	
		
		 String  s=  "begin ?:= "+funcschema+"QLN_KMM.capnhat_mm_td("
 	  	+loai_tb
		+",'"+ma_bc+"'"
		+",'"+psschema+"'"
		+",'"+psuserID+"'"
		+");"
        +"end;"
        ;
		System.out.println("THUNGHIEM:"+s);
		   return s;
		
		}

  
   
  
  	public String ds_hienthi_chemtudong
		(
		String schema,
		String loai_tb,
		String ma_bc
		)
		{	
			
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/khoamomay/momaytudong/ajax_tao_ds"
				+"&"+CesarCode.encode("loai_tb")+"="+loai_tb
				+"&"+CesarCode.encode("ma_bc")+"="+ma_bc
				; 	
		}

  
  	public String ds_mochemtudong
		(
		String funcschema,
		String kyhoadon,
		String loai_tb,
		String kieunhac,
		String tien,
		String ma_bc,
		String psschema,
		String psuserID
		)
		{	
		
		 String  s=  "begin ?:= "+funcschema+"QLN_KMM.capnhat_tao_ds_mmtd("
 	   	+"'"+kyhoadon+"'"
		+","+loai_tb
		+","+kieunhac
		+","+tien
		+",'"+ma_bc+"'"
		+",'"+psschema+"'"
		+",'"+psuserID+"'"
		+");"
        +"end;"
        ;
		System.out.println("THUNGHIEM:"+s);
		   return s;
		
		}

  
  
    public String chemtudong_khoamay
	(
		String funcschema
		,String form_cn
		,String psds_thuebao
		,String pihinhthuckhoa		
		,String loai_tb
		,String pikieu
		,String ma_bc
		,String ma_dvql
		,String psschema
		,String psuserID							   
	) 
	{  
	 	String  s=  "begin ?:= "+funcschema+"QLN_KMM.khoamay_tudong("
	 	   	+loai_tb
			+","+pikieu
			+",'"+ma_bc+"'"
			+",'"+form_cn+"'"
			+",'"+psuserID+"'"
			+","+pihinhthuckhoa
			+","+ma_dvql	
			+",'"+psds_thuebao+"'"	
			+",'"+psschema+"'"
			+",'"+psuserID+"'"
			+");"
	        +"end;"
	        ;
		return s;	
	}
    public String xoadanhsach
							(String funcschema
							,String form_cn
							,String psds_thuebao
							,String pihinhthuckhoa		
							,String loai_tb
							,String psschema
							,String psuserID							   
							  ) 
	{  
 	
 	  String  s=  "begin ?:= "+funcschema+"qltn_tracuu.chemtudong_xoads("
 	   	+"'"+form_cn+"'"
		+",'"+psds_thuebao+"'"
		+",'"+pihinhthuckhoa+"'"
		+",'"+loai_tb+"'"
		+",'"+psschema+"'"
		+",'"+psuserID+"'"
		+");"
        +"end;"
        ;
		   return s;	
	}
	
	
  
  	 public String capnhat_dskhoa
							(String funcschema
							,String form_cn
							,String psds_thuebao
							,String pikieu						
							,String loai_tb
							,String psschema
							,String psuserID							   
							  ) 
	{  
 	
 	  String  s=  "begin ?:= "+funcschema+"qltn_tracuu.capnhat_dskhoa("
 	   	+"'"+form_cn+"'"
		+",'"+psds_thuebao+"'"
		+",'"+pikieu+"'"
		+",'"+loai_tb+"'"
		+",'"+psschema+"'"
		+",'"+psuserID+"'"
		+");"
        +"end;"
        ;
		   return s;	
	}
	public String chemtudong_taodskhoa
	(  
		String dvvt,
		String kyhoadon,
		String thang,
		String kieukhoa,
		String kieunhac,
		String ngaygh,
		String tienMin,
		String tienMax,
		String thangMin,
		String thangMax,
		String manv,
		String loaikh,
		String kieutien,
		String mabc,
		String formcn,
		String khdb,
		String hinhthuckhoa
	) 
	{ 
		String s = "begin ?:= "+getSysConst("FuncSchema")+"QLN_KMM.tao_ds_kmtd("
			+dvvt+","
			+kyhoadon+","
			+"'"+thang+"',"
			+kieukhoa+","
			+kieunhac+","
			+ngaygh+","
			+tienMin+","
			+tienMax+","
			+thangMin+","
			+thangMax+","
			+"'"+manv+"',"
			+loaikh+","
			+kieutien+","
			+"'"+mabc+"',"
			+"'"+formcn+"',"
			+khdb+","
			+hinhthuckhoa+","
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'"
			+"); end;";
		return s;
	}	
  	public String ds_chemtudong_dschuakhoa
	(
		String schema,
		String form_cn,
		String kieu,
		String loai_tb,
	String ht_khoa
	)
	{	
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/khoamomay/khoamaytudong/ajax_ds_chuakhoa"
			+"&"+CesarCode.encode("formcn")+"="+form_cn
			+"&"+CesarCode.encode("kieu")+"="+kieu
			+"&"+CesarCode.encode("dvvt")+"="+loai_tb
			+"&"+CesarCode.encode("hinhthuckhoa")+"="+ht_khoa; 	
	}
  	public String ds_chemtudong_dskhoa
	(
	String schema,
	String form_cn,
	String kieu,
	String loai_tb,
	String ht_khoa
	)
	{	
		
		return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/khoamomay/khoamaytudong/ajax_ds_khoa"
			+"&"+CesarCode.encode("formcn")+"="+form_cn
			+"&"+CesarCode.encode("kieu")+"="+kieu
			+"&"+CesarCode.encode("dvvt")+"="+loai_tb
			+"&"+CesarCode.encode("hinhthuckhoa")+"="+ht_khoa
			; 	
	}
	public String theodoi_capnhat( String funcSchema 
							,String ps_DanhSach_TB 
							,String pi_Luu_LS
							,String pi_KhoaMay
							,String pi_LoaiDB
							,String psschema	
							,String psuserid
						   								                    
						  ) 
		  
							  
 { 
 System.out.println("thu ngniem capnaht: "+getSysConst("FuncSchema")+getSysConst("DataSchema"));
 String s =  "begin ?:= "+funcSchema+"QLN_KMM.capnhat_theodoi_kmm("
		+"'"+ps_DanhSach_TB+"'"
		+",'"+pi_Luu_LS+"'" 
		+",'"+pi_KhoaMay+"'"
		+",'"+pi_LoaiDB+"'"
		+",'"+psschema+"'"
        +",'"+psuserid+"'"
	    +"); "  
        +" end;" ;
	System.out.println(s);
    return s;

 }	
	
  
  
  	public String ds_theodoichem(
		String schema,
		String pikieuds,
		String piloaitb,
		String pidieukienloc,
		String pstungay,
		String psdenngay,
		String pidvql_id
		
		)
		{	
			System.out.println("thu ngniem: "+getSysConst("FuncSchema")+getSysConst("DataSchema"));
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/khoamomay/theodoichemmay/ajax_lay_ds"
				+"&"+CesarCode.encode("kieuds")+"="+pikieuds
				+"&"+CesarCode.encode("loaitb")+"="+piloaitb
				+"&"+CesarCode.encode("dkloc")+"="+pidieukienloc
				
				+"&"+CesarCode.encode("tungay")+"="+pstungay
				+"&"+CesarCode.encode("denngay")+"="+psdenngay
				+"&"+CesarCode.encode("dvql_id")+"="+pidvql_id
				; 	
		}

  
	 public String xoa_khdbtt
							( String funcschema
							,String pskyhoadon
							,String psds_thuebao	
							,String psschema
							,String psuserID							   
							  ) 
	{  
 	
 	  String  s=  "begin ?:= "+funcschema+"QLN_KMM.del_khdb_tt("
 	   	+"'"+pskyhoadon+"'"
		+",'"+psds_thuebao+"'"
		+",'"+psschema+"'"
		+",'"+psuserID+"'"
		+");"
        +"end;"
        ;
		   return s;	
	}
	
  
  
  	public String ds_khbdtheothag
		(
		String schema,
		String psma_bc,
		String pskyhoadon
		)
		{	
			
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/khoamomay/khdb_tt/ajax_ds_khtt"
				+"&"+CesarCode.encode("ma_bc")+"="+psma_bc
				+"&"+CesarCode.encode("kyhoadon")+"="+pskyhoadon
				; 	
		}

  
  	
public String them_khdacbiet_tt(  String funcSchema 
							,String psma_bc
							,String pskyhoadon 
							,String pitrangthai
							,String psma_tb
							,String psma_kh
							,String psschema	
							,String psuserid
						   								                    
						  ) 
							  
							  
							  
							  
							  
 { 
 System.out.println("thu ngniem: "+getSysConst("FuncSchema")+getSysConst("DataSchema"));
 String s =  "begin ?:= "+funcSchema+"QLN_KMM.ins_khdb_tt("
		+"'"+pskyhoadon+"'"
		+","+pitrangthai
		+",'"+psma_tb+"'"
		+",'"+psma_kh+"'"
		+",'"+psma_bc+"'"
		+",'"+psuserid+"'"
       	+",'"+psschema+"'"
        +",'"+psuserid+"'"
	    +"); "  
        +" end;" ;
	System.out.println(s);
    return s;

 }	
	
  
  	
public String them_khdacbiet(   String funcSchema 
							,String psma_tb 
							,String psma_tt
							,String psma_bc
							,String pitrangthai_id
							,String psschema	
							,String psuserid
						   								                    
						  ) 
							  
							  
							  
							  
							  
 { 
 System.out.println("thu ngniem: "+getSysConst("FuncSchema")+getSysConst("DataSchema"));
 String s =  "begin ?:= "+funcSchema+"QLN_KMM.ins_khdb("
		+"'"+psma_tb+"'"
		+",'"+psma_tt+"'" 
		+",'"+psma_bc+"'"
		+",'"+psuserid+"'"
		+","+pitrangthai_id
       	+",'"+psschema+"'"
        +",'"+psuserid+"'"
	    +"); "  
        +" end;" ;
	System.out.println(s);
    return s;

 }	
	
  
 
  	public String ds_mochemnhancong
		(
		String schema,
		String psform_cn,
		String piloaihinhtb
		)
		{	
			
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/khoamomay/momaynhancong/ajax_ds_chem"
				+"&"+CesarCode.encode("loaihinhtb")+"="+piloaihinhtb
				+"&"+CesarCode.encode("form_cn")+"="+psform_cn
				; 	
		}



 

 
  	public String ds_chemnhancong(
		String schema,
		String chukyno,
		String loaihinhtb,
		String hinhthuckhoa
		
		)
		{	
			System.out.println("thu ngniem: "+getSysConst("FuncSchema")+getSysConst("DataSchema"));
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/khoamomay/khoamaynhancong/ajax_ds_chem"
				+"&"+CesarCode.encode("chukyno")+"="+chukyno
				+"&"+CesarCode.encode("loaihinhtb")+"="+loaihinhtb
				+"&"+CesarCode.encode("hinhthuckhoa")+"="+hinhthuckhoa
				; 	
		}

		
		public String danhsach_thuhotinhkhac(
		String schema,
		String chukyno,
		String ma_tb
		)
		{	
			System.out.println("thu ngniem: "+getSysConst("FuncSchema")+getSysConst("DataSchema"));
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_thuho/ajax_thuhotinhkhac"
				+"&"+CesarCode.encode("chukyno")+"="+chukyno+"&"+CesarCode.encode("ma_tb")+"="+ma_tb
				; 	
		}
		
		
	public String danhsach_phieuhuy(
		String schema,
		String chukyno
		)
		{	
			
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_thuho/ajax_phieuhuy"
				+"&"+CesarCode.encode("chukyno")+"="+chukyno
				; 	
		}	
	
public String themthuebao_momay(  String funcSchema 
							,String psma_tb 
							,String psma_tt
							,String picungma_tt 
							,String psloaihinh_tb 
							,String psnguoicn
							,String psma_bc
							,String psform_cn 
							,String psschema	
							,String psuserid
								    								                    
						  ) 
							  
							  
							  
							  
							  
 { 
 System.out.println("thu ngniem: "+getSysConst("FuncSchema")+getSysConst("DataSchema"));
 String s =  "begin ?:= "+funcSchema+"QLN_KMM.ins_tmp_qltn_mmnc("
		+psloaihinh_tb
	    +",'"+psma_tb+"'"
		+",'"+psma_tt+"'" 
		+","+picungma_tt
        +",'"+psnguoicn+"'"
		+",'"+psform_cn+"'"
		+",'"+psma_bc+"'"
       	+",'"+psschema+"'"
        +",'"+psuserid+"'"
	    +"); "  
        +" end;" ;
	System.out.println(s);
    return s;

 }	
	
	
public String themthuebao(  String funcSchema 
							,String psma_tb 
							,String psma_tt
							,String picungma_tt 
							,String psloaihinh_tb 
							,String psnguoicn
							,String psma_bc
							,String psform_cn 
							,String pihinhthuckhoa 
							,String psschema	
							,String psuserid
								    								                    
						  ) 
							  
							  
							  
							  
							  
 { 
 System.out.println("thu ngniem: "+getSysConst("FuncSchema")+getSysConst("DataSchema"));
 String s =  "begin ?:= "+funcSchema+"QLN_KMM.ins_tmp_qltn_kmnc("
		+psloaihinh_tb
	    +",'"+psma_tb+"'"
		+",'"+psma_tt+"'" 
		+",'"+psma_bc+"'"
		+","+picungma_tt
        +",'"+psnguoicn+"'"
		+",'"+psform_cn+"'"
        +","+pihinhthuckhoa
		+",'"+psschema+"'"
        +",'"+psuserid+"'"
	    +"); "  
        +" end;" ;
	System.out.println(s);
    return s;

 }	
	
	
public String chemmay(     String funcSchema 
							,String psma_bc
							,String psloaihinh_tb 
							,String psform_cn
							,String psdsma_tb
							,String pihinhthuckhoa 
							,String pdvql_id 
							,String psschema	
							,String psuserid
								    								                    
						  ) 
		 
							  
 { 
 System.out.println("thu ngniem: "+getSysConst("FuncSchema")+getSysConst("DataSchema"));
 String s =  "begin ?:= "+funcSchema+"QLN_KMM.khoamay_nhancong("
		+psloaihinh_tb
	    +",'"+psma_bc+"'"
		+",'"+psform_cn+"'"
		+",'"+psdsma_tb+"'"
		+",'"+psuserid+"'"
        +","+pihinhthuckhoa
		+","+pdvql_id
		+",'"+psschema+"'"
		+",'"+psuserid+"'"
	    +"); "  
        +" end;" ;
	System.out.println(s);
    return s;

 }	

 
 	
public String momay( String funcSchema 
					,String psma_bc
					,String psloaihinh_tb 
					,String psform_cn
					,String psds_tb
					,String psschema	
					,String psuserid
						                    
					) 
		 
							  
 { 
 System.out.println("thu ngniem: "+getSysConst("FuncSchema")+getSysConst("DataSchema"));
 String s =  "begin ?:= "+funcSchema+"QLN_KMM.momay_nhancong("
		+psloaihinh_tb
	    +",'"+psform_cn+"'"
		+",'"+psuserid+"'"
		+",'"+psds_tb+"'"
		+",'"+psschema+"'"
		+",'"+psuserid+"'"
	    +"); "  
        +" end;" ;
	System.out.println(s);
    return s;

 }	
 
 
	 public String xoads
							( String funcschema
							,String psform_cn
							,String psds_thuebao	
							,String psloai_tb
                            ,String psschema
							,String psuserID							   
							  ) 
	{  
 	
 	  String  s=  "begin ?:= "+funcschema+"qltn_tracuu.chemnhancong_xoadstb("
 	   	+"'"+psform_cn+"'"
		+",'"+psds_thuebao+"'"
		+",'"+psloai_tb+"'"
		+",'"+psschema+"'"
		+",'"+psuserID+"'"
		+");"
        +"end;"
        ;
		   return s;	
	}
	
	
	
	public String dsphieuhuy(
		String schema,
		String ma_khachhang,
		String ma_thuebao,
		String ma_sophieu,
		String thutungay,
		String thudenngay,
		String ma_tinh,
		String buucuc,
		String ma_quay,
		String ma_thungan,
		String trangthai_id
		)
		{	
		System.out.println("neo.xln was called-Updated 16/07/2007");    
		
			return "/main?"+ CesarCode.encode("configFile")+ "=ccbs_qltn/qltn_thuho/ajax_phieuhuy"
				+"&"+CesarCode.encode("ma_kh")+"="+ma_khachhang
				+"&"+CesarCode.encode("ma_tb")+"="+ma_thuebao
				+"&"+CesarCode.encode("ma_phieu")+"="+ma_sophieu
				+"&"+CesarCode.encode("tungay")+"="+thutungay
				+"&"+CesarCode.encode("denngay")+"="+thudenngay
				+"&"+CesarCode.encode("ma_tinh")+"="+ma_tinh
				+"&"+CesarCode.encode("buucuc")+"="+buucuc
				+"&"+CesarCode.encode("ma_quay")+"="+ma_quay
				+"&"+CesarCode.encode("ma_thungan")+"="+ma_thungan
				+"&"+CesarCode.encode("trangthai_id")+"="+trangthai_id
				; 	
		}
		
		
	
	
	
	
	
}
