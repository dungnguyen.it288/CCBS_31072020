package neo.qltn_hni;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class qltn_gachno_thudaily extends NEOProcessEx {

    public void run() 
	{
    	System.out.println("Gach no thu dai ly (Gach no thu tai nha) was called-Updated 10/09/2007");    	
  	}    
  	public String gachno_thucuoc_tainha(
  		String pschukyno,
		String psdsma_kh,
		String psdschukyno,
		String psdstientra,
		String dsLoaiHD,
		String pdngay_tt,
		String piluotthanhtoan,
		String sMaNV,
		String sMaBC
  		)
	{
		String a= "begin ? := "+getSysConst("FuncSchema")+"qltn_thtoan.gachno_thucuoc_tainha("
			+"'"+pschukyno+"',"
			+"'"+psdsma_kh+"',"
			+"'"+psdschukyno+"',"
			+"'"+psdstientra+"',"
			+"'"+dsLoaiHD+"',"
			+"'"+pdngay_tt+"',"
			+"'"+sMaNV+"',"
			+"'"+sMaBC+"',"
			+"'"+piluotthanhtoan+"',"
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
			System.out.println(a);
		return a;
	}
	public String value_gachno_chungtu(
		String dsPhieu,
		String psNgayTT,		
		String psLuotTT,
		String psHTTT_ID,
		String psChucNangGachNo
		)
		{
			return "begin ?:= "+getSysConst("FuncSchema")+"qltn_thtoan.gachno_chungtu('"+dsPhieu+"','"+psNgayTT+"','"
				+psLuotTT+"','"+psHTTT_ID+"','"+psChucNangGachNo
				+"','"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		}
	public String doc_layds_giaophieu(
		String psLuotgiao,
		String psChukyno,
		String psMaNV,
		String psMaT,
		String psPageNum,
		String psPageRec
	)		
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_thudaily/ajax_layds_giaophieu_theo_lg_de_gno"
  			+ "&" + CesarCode.encode("luotgiao_id") + "=" + psLuotgiao
  			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
  			+ "&" + CesarCode.encode("ma_nv") + "=" + psMaNV
  			+ "&" + CesarCode.encode("ma_t") + "=" + psMaT
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}	
	public String value_nhanvientc(
		String psMaNV,
		String psDonvi
	)
	{
		String s= "begin ?:="
			+getSysConst("FuncSchema")
			+"qltn_tracuu.laytt_nhanvienthucuoc('"
			+psDonvi+"',"
			+"0,0,'"
			+psMaNV+"','"
			+getUserVar("sys_dataschema")+"','"
			+getUserVar("userID")
			+"'); end;";		
		return s;
	}
	public String doc_layds_giaophieu_theotuyen(
		String psChukyno,
		String psTrangthai,		
		String psMaNV,
		String psMa_T,
		String psPageNum,
		String psPageRec
	)		
	{			
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_giaohoadon/ajax_layds_giaophieu"
			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno
  			+ "&" + CesarCode.encode("trangthai") + "=" + psTrangthai
  			+ "&" + CesarCode.encode("ma_nv") + "=" + psMaNV
  			+ "&" + CesarCode.encode("ma_t") + "=" + psMa_T
			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String value_xoaphieu_thutainha(
		String psChukyno,
		String psDsMaKH,
		String psDsKyHD
	)	
	{
		return "begin ? := "+getSysConst("FuncSchema")+"qltn_thtoan.xoaphieu_thutainha('"+psChukyno+"','"+psDsMaKH+"','"
			+psDsKyHD+"' ,'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"') ; end;";	
	}
	public String value_laygiatri_luotgiao(
		String psChukyno		
	)	
	{
		return "begin ? :="+getSysConst("FuncSchema")+"qltn_thtoan.lay_luotgiao('"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"','"+psChukyno+"'); end;";		
	}
	public String value_giaophieu_thutainha(
		String psChukyno,
		String psDsMaKH,
		String psDsKyHD,
		String psNgayGiao,
		String psMaNV,
		String psLuotGiao_ID,
		String psMa_T
	)
	{
		return "begin ? := "+getSysConst("FuncSchema")+"qltn_thtoan.giaophieu_thutainha('"
			+psChukyno+"','"
			+psDsMaKH+"','"
			+psDsKyHD+"','"
			+psLuotGiao_ID+"','"
			+psNgayGiao+"','"
			+psMaNV+"','"
			+psMa_T+"','"			
			+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"') ; end;";
	}
	
	public String doc_layds_giaophieu_theotuyen_lg
	(
		String psLuotGiao,
		String psChukyno
	)		
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_giaohoadon/ajax_layds_giaophieu_theo_lg"
  			+ "&" + CesarCode.encode("luotgiao_id") + "=" + psLuotGiao
			+ "&" + CesarCode.encode("chukyno") + "=" + psChukyno;
	}
	public String value_xoaphieu_thutainha_lg(
		String psChukyno,
		String psLuotGiao,
		String psma_nv,
		String psma_t
	)	
	{		
		return "begin ? := "+getSysConst("FuncSchema")+"qltn_thtoan.xoaphieu_thutainha_theo_lg('"+psChukyno+"','"+psLuotGiao+"','"+psma_nv+"','"+psma_t+"','"
			+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"') ; end;";
	}
	public String value_tuyenthu(
	String psMaT,
	String psMaNV
	)
	{
		return "declare psResult varchar2(200); begin SELECT ten_T into psResult FROM "
				+getUserVar("sys_dataschema")+"tuyens a WHERE a.ma_t = "
					+psMaT+" AND trim(a.ma_nv) = '"
						+psMaNV+"' AND  rownum<2; ? := nvl(psResult,''); end;";
	}
	public String gachno_thucuoc_tienmat(
  		String pschukyno,
		String psdsma_kh,
		String psdstientra,
		String pdngay_tt,
		String piluotthanhtoan
  		)
	{
		String a= "begin ? := "+getSysConst("FuncSchema")+"qltn_thtoan.thanhtoan3("
			+"'"+pschukyno+"',"
			+"'"+psdsma_kh+"',"
			+"'"+psdstientra+"',"
			+"'1',"
			+"'"+pdngay_tt+"',"
			+"'"+piluotthanhtoan+"',"
			+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		System.out.println(a);
		return a;
	}
	public String layds_luotgiao_manv(
		String psma_nv,
		String psma_t
	)
	{
            ReportLayoutParser a= (ReportLayoutParser) getUserVarObj("uParser");
            return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_giaohoadon/ajax_luotgiao_manv"
                    + "&" + CesarCode.encode("ma_nv") + "=" + psma_nv
                    + "&" + CesarCode.encode("ma_t") + "=" + psma_t;
	}
	public String layds_luotgiao_mat(
		String psma_nv,
		String psma_t
	)		
	{
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_qltn_hni/qltn_giaohoadon/ajax_luotgiao_mat"
  			+ "&" + CesarCode.encode("ma_nv") + "=" + psma_nv
			+ "&" + CesarCode.encode("ma_t") + "=" + psma_t;
	}
	public String value_capnhat_seri_gachno(
		String psKyhoadon,
    	String psDsSeriCu,
    	String psDsSeriMoi,
    	String pstuSoseri,
    	String psdenSoseri,
    	String psthaydoi,
    	String psSeri,
    	String psThung,
    	String psMaNv    	
    )
    {
    	String out_="begin ? := "+getSysConst("FuncSchema")+"qltn.capnhat_seri_gachno("
    	+"'"+psKyhoadon+"',"
    	+"'"+psDsSeriCu+"',"
    	+"'"+psDsSeriMoi+"',"
    	+"'"+pstuSoseri+"',"
    	+"'"+psdenSoseri+"',"
    	+"'"+psthaydoi+"',"
    	+"'"+psSeri+"',"
    	+"'"+psThung+"',"
    	+"'"+psMaNv+"',"			
		+"'"+getUserVar("sys_dataschema")+"','"+getUserVar("userID")+"'); end;";
		System.out.println(out_);
		return out_;
    }
}