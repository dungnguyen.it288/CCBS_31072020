package neo.qltn_new;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class qltn_config_donno extends NEOProcessEx 

{
  	public void run() 
  	{
    	System.out.println("neo.qltn_new.qltn_config_donno was called");
   
  	}

	//hientt 
	// 14/01/2013
	//- cau hinh don no		
	public String config_donno(
		String time_,
		String timeTo,
		String type_,
		String donno,
		String dstinh,
		String note_,
		String userid		
	)
	{	   
		String val_ = "";  
		
		String s = "begin ? :=admin_v2.PKG_AUTO_INCREMENT_DEBT.config_increment_debt_func("+
			"'"+time_+"',"+
			"'"+timeTo+"',"+
			"'"+type_+"',"+
			"'"+donno+"',"+
			"'"+dstinh+"',"+
			"'"+note_+"',"+
			"'"+userid+"'); end;";
	     		
		
		try
			{		    
				 val_=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       val_ = ex.getMessage();
			}
		return val_;
	}

public String config_rollback(
		String actions, 
        String status,
		String userid 
	)
	{	   
		String val_ = "";  
		
		String s = "begin ? :=admin_v2.PKG_AUTO_INCREMENT_DEBT.config_rollback_data_func("+
			"'"+actions+"',"+
			"'"+status+"',"+
			"'"+userid+"'); end;";
	     		
		
		try
			{		    
				 val_=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       val_ = ex.getMessage();
			}
		return val_;
	}
public String config_method_warn(
		String methods, 
        String listUser,
		String userid 
	)
	{	   
		String val_ = "";  
		
		String s = "begin ? :=admin_v2.PKG_AUTO_INCREMENT_DEBT.config_method_warn_func("+
			"'"+methods+"',"+
			"'"+listUser+"',"+
			"'"+userid+"'); end;";
	     		
		
		try
			{		    
				 val_=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       val_ = ex.getMessage();
			}
		return val_;
	}	
public String themmoi_kmck(      String pschukyno 
								    ,String strinput 
									,String dskm								
								   	,String dstruong
								    ,String  psUserID
									,String loaitru
									,String kieugiamtru
									,String novat
								                    
								) 
	 
 { 
  String s =  "begin ?:= admin_v2.pkg_auto_increment_debt.add_kmck_func("
        +"'"+strinput+"'"
		+",'"+dskm+"'"
		+",'"+dstruong+"'"
		+",'"+loaitru+"'"
		+",'"+kieugiamtru+"'"
		+",'"+novat+"'"
		+",'"+psUserID+"'"
	    +"); "  
        +" end;" ;
		String val_ = "";  
	try
			{		    
				 val_=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       val_ = ex.getMessage();
			}
		return val_;

 }	
public String capnhat_cauhinh(      String pschukyno 
								    ,String strinput 
									,String type								
								   	,String psUserID
								    ,String  psSchema
								                    
								) 
	 
 { 
  String s =  "begin ?:= admin_v2.pkg_auto_increment_debt.capnhat_cauhinh("
        +"'"+pschukyno+"'"
		+",'"+strinput+"'"
		+",'"+type+"'"
	    +",'"+psUserID+"'"
		+",'"+psSchema+"'"
	    +"); "  
        +" end;" ;
		String val_ = "";  
	try
			{		    
				 val_=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       val_ = ex.getMessage();
			}
		return val_;

 }	
}