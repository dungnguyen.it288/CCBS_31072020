/*
  -- Create by: thuyvtt2 
  -- create date: 01/06/2016
  -- content: quan ly thue bao IR han muc
 */

package neo.subscriber_ir;

import neo.smartui.process.*;
import neo.smartui.common.*;
public class quanlytbir extends NEOProcessEx{

    public String insert( String msisdn, String start_time,
            String end_time,String notes, String psuip)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_subscriber_ir.addsub("
                + "'" + getUserVar("userID") + "'"
                + ",'" + psuip + "'"
                + ",'" + getUserVar("sys_agentcode") + "'"
                + ",'" + msisdn + "'" 
                + ",'" + start_time + "'"
                + ",'" + end_time + "'" 
                + ",'" + notes + "'" 
                + ");"
                + "end;";
       try
			{		    
				 result=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
    public String update( 
	        String msisdn,
			String start_time,
            String end_time,
			String notes,
			String psuip)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_subscriber_ir.updateSub("
                + "'" + getUserVar("userID") + "'"
               // + ",'" + getUserVar("userIP") + "'"
			    + ",'" + psuip + "'"
                + ",'" + getUserVar("sys_agentcode") + "'" 
                + ",'" + msisdn + "'" 
                + ",'" + start_time + "'"
                + ",'" + end_time + "'" 
                + ",'" + notes + "'" 
                + ");"
                + "end;";
		System.out.println("Thuyvtt---"+s);
        try
			{		    
				 result=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
	public String delSub( 
	        String msisdn,
			String notes,
			String psuip)
    {
        String result="";
        String s = "begin ? := " 
                + "admin_v2.pkg_subscriber_ir.deleteSub("
                + "'" + getUserVar("userID") + "'"
                + ",'" + psuip + "'"
                + ",'" + getUserVar("sys_agentcode") + "'" 
                + ",'" + msisdn + "'" 
                + ",'" + notes + "'" 
                + ");"
                + "end;";
        try
			{		    
				 result=	this.reqValue("",s);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;
    }
   
	
}
