package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class capsimso
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.capsimso was called");
  }

  public String layds_simsomoi(String value,String dauso,String tudai,String dendai) {
    String s= "/main?"+CesarCode.encode("configFile")+"=qlsimso/ccsim/capsim_ajax_moi"
        +"&"+CesarCode.encode("kieucap")+"="+value
		+"&"+CesarCode.encode("dauso_id")+"="+dauso
		+"&"+CesarCode.encode("tudaiso")+"="+tudai
		+"&"+CesarCode.encode("dendaiso")+"="+dendai
        ;
	//System.out.println(s);
	return s;		
  }
  
   public String layds_simsodaphan(String value) {
    String s= "/main?"+CesarCode.encode("configFile")+"=qlsimso/ccsim/capsim_ajax_moi_danhan"
        +"&"+CesarCode.encode("ma_dh")+"="+value		
        ;
	//System.out.println(s);
	return s;		
  }
  
  public String laytt_donhang_simso(String schema,String psMA_DH) {
    return "begin ?:="+CesarCode.decode(schema)+"simso.lay_ds_simsomoi_dacap('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+psMA_DH+"'); end;";
  }
  
  
 public String capnhat_somay_buucuc(String schema,String psSoMay,String psSoSim,String psMa_DH,String psMa_BC,String psNgayCap,String psNguoiCap)
 {
    String s= "begin ? := "+CesarCode.decode(schema)+"simso.capnhat_somay_buucuc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"                         
		+"'"+psSoMay+"',"
		+"'"+psSoSim+"',"
		+"'"+psMa_DH+"',"  
		+"'"+psMa_BC+"',"                         		                 
		+"'"+psNgayCap+"',"                   
		+"'"+psNguoiCap+"'"
		+");"
        +"end;"
        ;
    System.out.println(s);
    return s;
  } 
  
}
