
package neo;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSetMetaData;
import javax.sql.RowSet;
import neo.smartui.common.CesarCode;
import neo.smartui.process.NEOProcessEx;
import neo.smartui.report.NEOCommonData;
import neo.smartui.report.NEOExecInfo;

public class ccbs_km extends NEOProcessEx {
  public void run() {
    System.out.println("neo.khuyenmai was called");
  }	
	
	public NEOCommonData cauhinh_dk_pttb(
  							String  schema
  							,String schemaCommon 
  							,String pikmcv_id																  						   			        
					       	,String pikmht_id
							,String psap_dung
							,String pskp_km
							,String psngay_batdau
							,String psngay_hethan
							,String psngay_khoiphuc
							,String psNoidung
							,String psDieukien
							,String piTienHM							
							,String piTienCD
  							,String psNguoi_cn
							,String psmay_cn			
									 ) {
	psNoidung = psNoidung.replace("'","''");
    String s=    "begin ? := admin_v2.promo_config.cauhinh_dieukien('"+schemaCommon
		+"','"+psNguoi_cn
		+"','"+psmay_cn
		+"','"+getUserVar("sys_agentcode")
		+"','"+pikmcv_id
		+"','"+pikmht_id
		+"','"+psap_dung
		+"','"+pskp_km
		+"',"+psngay_batdau
		+","+psngay_hethan
		+","+psngay_khoiphuc	
		+",'"+psNoidung
		+"','"+psDieukien
		+"','"+piTienHM
		+"','"+piTienCD+"'"
        +");end;";
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 return new NEOCommonData(nei_);
  }
	
	public NEOCommonData ds_dieukiens(int pikmcv_id)
	{		
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/config_km_dieukien/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
		
		NEOExecInfo nei_ = new NEOExecInfo(url_);
		nei_.setDataSrc("VNPBilling");
		return new NEOCommonData(nei_);
	}	
	
	public NEOCommonData layds_dieukiens(String pikmcv_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/config_km_dieukien/km_dieukiens_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmcv_id+"&sid="+Math.random();		
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);	
	    return new NEOCommonData(nei_);
  	}
	/*
	public NEOCommonData laytt_dieukiens(String kmcv_id) 
 	{
		String s= "begin ?:= admin_v2.promo_config.laytt_dieukiens('"+getUserVar("sys_agentcode")		
			+"','"+kmcv_id
			+");end;";			
		 NEOExecInfo nei_ = new NEOExecInfo(s);
		 return new NEOCommonData(nei_);
	}
	*/
	public NEOCommonData laytt_hinhthucs(
 										 String func_schema,
 										 String schemaCommon,
										 String kmcv_id,
 										 String kmht_id) 
 	{
	String s= "begin ?:= admin_v2.promo_config.laytt_hinhthuc('"+getUserVar("sys_agentcode")	
		+"','"+schemaCommon
		+"',"+kmcv_id
		+","+kmht_id
		+");end;";
		
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 return new NEOCommonData(nei_);
    }
	
	public NEOCommonData chuyen_cauhinh_pttb(
  							String  schema
  							,String schemaCommon 
  							,String pikmcv_id																  						   			        
					       	,String psten_congvan
							,String psngay_batdau
							,String psngayy_kt
							,String pscon_hieuluc
							,String psnoidung							
							,String psnguoi_cn
  							,String psmay_cn				                                   
									 ) {
									
    String s=    "begin ? := admin_v2.promo_config.cauhinh_congvan('"+schemaCommon
		+"','"+psnguoi_cn
		+"','"+getUserVar("sys_agentcode")
		+"','"+psmay_cn
		+"','"+pikmcv_id
		+"','"+psten_congvan
		+"','"+pscon_hieuluc+"'"
        +","+psngay_batdau
        +","+psngayy_kt         
		+",'"+psnoidung+"'"
        +");"
        +"end;"
        ;	   
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 return new NEOCommonData(nei_);
  }
	
	public NEOCommonData cauhinh_kmht_pttb(
  							String  schema
  							,String schemaCommon 
  							,String pikmcv_id
							,String pikmht_id							
					       	,String psten_hinhthuc													
							,String psnoidung							
							,String psnguoi_cn
  							,String psmay_cn				                                   
									 ) {
									
    String s=    "begin ? := admin_v2.promo_config.cauhinh_hinhthuc('"+schemaCommon
		+"','"+psnguoi_cn
		+"','"+psmay_cn
		+"','"+getUserVar("sys_agentcode")
		+"','"+pikmcv_id
		+"','"+pikmht_id
		+"','"+psten_hinhthuc     
		+"','"+psnoidung+"'"
        +");"
        +"end;"
        ;		
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 return new NEOCommonData(nei_);
  }	
}

