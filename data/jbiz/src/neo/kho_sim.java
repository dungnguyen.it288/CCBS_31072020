package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class kho_sim
    extends NEOProcessEx {
	public void run() {
		System.out.println("neo.pttb_tthd was called");
	}

	public String doc_khosim(String psDauma, String psWindows, String psLevel){
		return "/main?" + CesarCode.encode("configFile")+"=qlsimso/khosim/ajax_khosim_data"
  			+ "&" + CesarCode.encode("windows") + "=" + psWindows
			+ "&" + CesarCode.encode("level") + "=" + psLevel
			+ "&" + CesarCode.encode("dau_ma") + "=" + psDauma;
	}
	public NEOCommonData value_capnhatkho(String psLevel, String psDsId){
		String xs_ = "begin ? := neo.kho_sim.capnhat_kho('"+
			psLevel+"','"+
			psDsId+"','"+
			getUserVar("sys_agentcode")+"','"+
			getUserVar("userID")+"','"+
			getUserVar("userIP")+"'); end;";
		System.out.println(xs_);
			
		NEOExecInfo ei_ = new NEOExecInfo(xs_);		
        ei_.setDataSrc("CMDVOracleDS");	
		return new NEOCommonData(ei_);
	}
}
