package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class prepayment extends NEOProcessEx {
  public void run() {
    System.out.println("neo.prepayment was called");
  }
  
 public String kiemtra_thuebao(String psso_tb){
    String s=  "begin ? := admin_v2.pkg_prepay_numstore.kiemtra_thuebao('"+psso_tb+"','"+getUserVar("userID")+"','"+getUserVar("sys_agentcode")+"'); end;";	
    return s;
}
  public String laytt_kieuso_thuebao(String pssotb) {
    return "begin ?:=admin_v2.pkg_prepay_numstore.laytt_kieuso_thuebao('"+pssotb+"'); end;";
  }
  
  public String laytt_tratruoc(String pssotb) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/numstore/prepayment/ajax_tt_tratruoc"
     		+"&"+CesarCode.encode("sotb")+"="+pssotb
        ;
  }
				
 public String dangky_tratruoc(String psso_tb
						,String pskieuso_id                      
						,String psmuc_chietkhau                    						                      
						,String psghichu
						,String psuserip
						){
    String s=  "begin ? := admin_v2.pkg_prepay_numstore.dangky_tratruoc('"+psso_tb+"','"+pskieuso_id+"','"+psmuc_chietkhau+"','"+getUserVar("sys_agentcode")+"','"+getUserVar("userID")+"','"+psuserip+"','"+psghichu+"'); end;";
    return s;
}
 public String huy_tratruoc(String psso_tb
						,String ps_id                      						                  						                      
						,String psghichu
						,String psuserip
						){
    String s=  "begin ? := admin_v2.pkg_prepay_numstore.huy_tratruoc('"+psso_tb+"','"+ps_id+"','"+psghichu+"','"+getUserVar("userID")+"','"+psuserip+"'); end;";
    return s;
}

  public String laytt_log(String pssotb) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/numstore/prepayment/ajax_log"
     		+"&"+CesarCode.encode("sotb")+"="+pssotb
        ;
  }
  public String layds_log_tratruoc(String pssotb,String psdatefrom,String psdateto) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/numstore/prepayment/ajax_ds_log_tratruoc"
     		+"&"+CesarCode.encode("sotb")+"="+pssotb
			+"&"+CesarCode.encode("date_from")+"="+psdatefrom
			+"&"+CesarCode.encode("date_to")+"="+psdateto
        ;
  }
}