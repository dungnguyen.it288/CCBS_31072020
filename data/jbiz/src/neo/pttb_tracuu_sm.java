package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_yeucau_ks
extends NEOProcessEx {
	public void run()
	{
		System.out.println("neo.pttb_yeucau_ks was called");
	}
	public String getUrlttHopdongtl(String schema, String ma_hd, String ma_tb, String user_id)
	{
		String s = "begin ? := " + CesarCode.decode(schema) + "CCBS.laytt_tb("				
		+ "'" + ma_hd + "'"
		+ ",'" + ma_tb + "'"
		+ ",'" + user_id + "'"
		+");"
		+ " end;";
		return s;
	}
	public String hoanThanhHopdongTL(String schema, String ma_hd, String ngay_ht)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "CCBS.hoanthanh_hdtl("
		+ "'" + ma_hd + "'"
		+ ",'" + ngay_ht + "'"
		+ ");"
		+ " end;";
		System.out.println(s);
		return s;	
	}
	public String xoaYeucau_ks(String schema, String psYeucauks_id)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "pttb_test_kien.xoa_yeucauks("
		+ "'" + psYeucauks_id + "'" + ");"
		+ " end;";
		System.out.println(s);
		return s;
	}
	public String capnhatHDThanhLy(String schema   
        ,String psMA_HD
        ,String psCOQUAN
        ,String pdNGAY_LAPHD
        ,String piLOAITB_ID
        ,String psMA_TB
        ,String psMA_KH
        ,String psGHICHU
        ,String psDIACHI_CT
        ,String psDIACHI_TT
        ,String psTEN_TT
        ,String psTEN_TB
        ,String psDIACHI
        ,String psUSER_ID
		)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "PTTB_TEST_KIEN.CAPNHAT_HOPDONG_TL('"
					+ psMA_HD + "'"
					+ ",'" + psCOQUAN + "'"
					+ ",'" + pdNGAY_LAPHD + "'"
					+ ",'" + piLOAITB_ID + "'"
					+ ",'" + psMA_TB + "'"
					+ ",'" + psMA_KH + "'"
					+ "," + psGHICHU
					+ ",'" + psDIACHI_CT + "'"
					+ ",'" + psDIACHI_TT + "'"
					+ ",'" + psTEN_TT + "'"
					+ ",'" + psTEN_TB + "'"
					+ ",'" + psDIACHI + "'"
					+ ",'" + psUSER_ID + "'"
					+ ");"
					+ "end;";			
		System.out.println(s);
		return s;		
	}
}