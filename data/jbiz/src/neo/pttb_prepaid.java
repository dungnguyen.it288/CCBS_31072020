package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_prepaid
extends NEOProcessEx {
public void run()
	{
		System.out.println("neo.pttb_prepaid was called");
	}
	
// Lay danh sach tra truoc	
public String layds_tratruoc(String pageid,String recperpage, String somay)
	{
		return "/main?"+CesarCode.encode("configFile")+"=PTTB/laphd/Prepaid/DSTraTruoc_ajax"
		 +"&"+CesarCode.encode("pageid")+"="+pageid
		 +"&"+CesarCode.encode("recperpage")+"="+recperpage
		 +"&"+CesarCode.encode("somay")+"="+somay
		;
	}

//Tim kiem theo so may
public String timsomay(String schema,String psSomay,String sSchema,String sMaTinhNgDung,String sUserId) 
{ String s= "begin ? := "+schema+"PTTB_PREPAID.timkiem_tratruoc("
							   +"'"+psSomay+"',"
							   +"'"+sSchema+"',"
							   +"'"+sMaTinhNgDung+"',"
							   +"'"+sUserId+"'"
							   +");"
							   +"end;"
	                           ;
	System.out.println(s);
	return s;						   
}	
	
// Them moi vao bang thue bao tra truoc
public String themtratruoc(String schema,String psSomay,String psNgay_tthoai,String psTen_tb,String psDiachi,String psCuahang,String psGhichu,String psSoHD,String psTel,String psIDNO,String psNgaycap,String psNCap,String psNgaysinh,String psSim,String psLoaiGT,String psDoituong_id,String sSchema,String sMaTinhNgDung,String sUserId)
{ String s= "begin ? := "+schema+"PTTB_PREPAID.them_tratruoc("
       							+"'"+psSomay+"',"
								+"'"+psNgay_tthoai+"',"
								+"'"+psTen_tb+"',"
								+"'"+psDiachi+"',"
								+"'"+psCuahang+"',"								
								+"'"+psGhichu+"',"
								+"'"+psSoHD+"',"
								+"'"+psTel+"',"
								+"'"+psIDNO+"',"
								+"'"+psNgaycap+"',"
								+"'"+psNCap+"',"
								+"'"+psNgaysinh+"',"
								+"'"+psSim+"',"
								+"'"+psLoaiGT+"',"												
								+"'"+psDoituong_id+"',"
								+"'"+sSchema+"',"
								+"'"+sMaTinhNgDung+"',"
								+"'"+sUserId+"'"
								+");"
								+"end;"
								;
	//System.out.println(s);
	return s;
}	

//Sua doi vao bang thue bao tra truoc
public String suatratruoc(String schema,String psSomay,String psNgay_tthoai,String psTen_tb,String psDiachi,String psCuahang,String psGhichu,String psSoHD,String psTel,String psIDNO,String psNgaycap,String psNCap,String psNgaysinh,String psSim,String psLoaiGT,String psDoituong_id,String psMaHD,String sSchema,String sMaTinhNgDung,String sUserId)
{ String s= "begin ? := "+schema+"PTTB_PREPAID.sua_tratruoc("
       							+"'"+psSomay+"',"
								+"'"+psNgay_tthoai+"',"
								+"'"+psTen_tb+"',"
								+"'"+psDiachi+"',"
								+"'"+psCuahang+"',"								
								+"'"+psGhichu+"',"
								+"'"+psSoHD+"',"
								+"'"+psTel+"',"
								+"'"+psIDNO+"',"
								+"'"+psNgaycap+"',"
								+"'"+psNCap+"',"
								+"'"+psNgaysinh+"',"
								+"'"+psSim+"',"
								+"'"+psLoaiGT+"',"												
								+"'"+psDoituong_id+"',"
								+"'"+psMaHD+"',"
								+"'"+sSchema+"',"
								+"'"+sMaTinhNgDung+"',"
								+"'"+sUserId+"'"
								+");"
								+"end;"
								;
	System.out.println(s);
	return s;
}	

//Sang nhuong thue bao tra truoc
public String SangNhuong(String schema,String psSomay,String psNgay_tthoai,String psTen_tb,String psDiachi,String psCuahang,String psGhichu,String psSoHD,String psTel,String psIDNO,String psNgaycap,String psNCap,String psNgaysinh,String psSim,String psLoaiGT,String psDoituong_id,String psMaHD,String sSchema,String sMaTinhNgDung,String sUserId)
{ String s= "begin ? := "+schema+"PTTB_PREPAID.capnhatsangnhuong("
       							+"'"+psSomay+"',"
								+"'"+psNgay_tthoai+"',"
								+"'"+psTen_tb+"',"
								+"'"+psDiachi+"',"
								+"'"+psCuahang+"',"								
								+"'"+psGhichu+"',"
								+"'"+psSoHD+"',"
								+"'"+psTel+"',"
								+"'"+psIDNO+"',"
								+"'"+psNgaycap+"',"
								+"'"+psNCap+"',"
								+"'"+psNgaysinh+"',"
								+"'"+psSim+"',"
								+"'"+psLoaiGT+"',"												
								+"'"+psDoituong_id+"',"
								+"'"+psMaHD+"',"
								+"'"+sSchema+"',"
								+"'"+sMaTinhNgDung+"',"
								+"'"+sUserId+"'"
								+");"
								+"end;"
								;
	System.out.println(s);
	return s;
}	

//Xoa danh sach tra truoc
public String xoatratruoc(String schema,String sSoMay,String sSchema,String sMaTinhNgDung,String sUserId)
{ String s= "begin ? := "+schema+"PTTB_PREPAID.xoa_tratruoc("						        
							+"'"+sSoMay+"',"	
							+"'"+sSchema+"',"
							+"'"+sMaTinhNgDung+"',"
							+"'"+sUserId+"'"	
							+");"
							+"end;"	
							;
	//System.out.println(s);
return s;
}	
	
}