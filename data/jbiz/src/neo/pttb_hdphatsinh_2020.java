package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_hdphatsinh_2020
extends NEOProcessEx {
	
	public String capnhatHDPhatSinh(String schema, String psUSER_ID ,String psMA_HD ,String psNGAY_LAPHD,String psNGAY_SINH ,String psCOQUAN ,String psTEN_TT 
		,String psDIACHI_TT ,String psTEN_TB ,String psTENTTMOI ,String psDIACHITT_MOI ,String psTEN_DB ,String psTENTB_MOI 
		,String piDIADIEMTT_ID ,String psSODAIDIEN ,String psMS_THUE ,String psTAIKHOAN ,String psMA_TB ,String psMA_KH 
		,String psGHICHU ,String piQUANTT_ID ,String piPHUONGTT_ID ,String piPHOTT_ID ,String psSOTT_NHA ,String piNGANHANG_ID 
		,String psCOQUAN_MOI ,String psDIACHICT_MOI ,String piQUAN_ID,String piPHUONG_ID,String piPHO_ID,String psSONHA 
		,String psINCHITIET,String piQUANLD_ID,String piPHUONGLD_ID,String piPHOLD_ID,String psSOLD_NHA,String psDIACHILD_MOI
		,String psEmail,String piKieuBC_ID,String piChuyenKhoan_id,String psNguoi_GT,String psSo_Giayto,String psMAY_CN, 
		String psQUOC_TICH, 
		String psLOAIGT_ID,
		String psSO_GT,
		String psDIACHI_TR,
		String psNGAYCAP_GT,
		String psNOICAP_GT,
		String psDOITUONG_SD,
		String psMUCPHI)
	{
 
		// String s = "begin ? := admin_v2.pttb_laphd.capnhat_hdphatsinh('"+getUserVar("userID")
		String s = "begin ? := admin_v2.pttb_laphd_duongdv.capnhat_hdphatsinh_2020('" + getUserVar("userID")
		+"','"+ getUserVar("sys_agentcode")
		+"','"+ getUserVar("sys_dataschema")
			+ "','" + psMA_HD + "'"
			+ ",'" + psNGAY_LAPHD + "'"
			+ ",'" + psNGAY_SINH + "'"
			+ ",'" + psCOQUAN + "'"
			+ ",'" + psTEN_TT + "'"
			+ ",'" + psDIACHI_TT + "'"
			+ ",'" + psTEN_TB + "'"
			+ ",'" + psTENTTMOI + "'"
			+ ",'" + psDIACHITT_MOI + "'"
			+ ",'" + psTEN_DB + "'"
			+ ",'" + psTENTB_MOI + "'"
			+ ",'" + piDIADIEMTT_ID + "'"
			+ ",'" + psSODAIDIEN + "'"
			+ ",'" + psMS_THUE + "'"
			+ ",'" + psTAIKHOAN + "'"
			+ ",'" + psMA_TB + "'"
			+ ",'" + psMA_KH + "'"
			+ "," + psGHICHU
			+ ",'" + piQUANTT_ID + "'"
			+ ",'" + piPHUONGTT_ID + "'"
			+ ",'" + piPHOTT_ID + "'"
			+ ",'" + psSOTT_NHA + "'"
			+ ",'" + piNGANHANG_ID + "'"
			+ ",'" + psCOQUAN_MOI + "'"
			+ ",'" + psDIACHICT_MOI + "'"
			+ ",'" + piQUAN_ID + "'"
			+ ",'" + piPHUONG_ID + "'"
			+ ",'" + piPHO_ID + "'"
			+ ",'" + psSONHA + "'"
			+ ",'" + psINCHITIET + "'"
			+ ",'" + piQUANLD_ID + "'"
			+ ",'" + piPHUONGLD_ID + "'"
			+ ",'" + piPHOLD_ID + "'"
			+ ",'" + psSOLD_NHA + "'"
			+ ",'" + psDIACHILD_MOI + "'"			
			+ ",'" + psEmail + "'"
			+ ",'" + piKieuBC_ID + "'"
			+ ",'" + piChuyenKhoan_id + "'"	
			+ ",'" + psNguoi_GT + "'"
			+ ",'" + psSo_Giayto + "'"		
			+ ",'" + psMAY_CN + "'"
			// Edited	: duongdv
			// Date		: 20170704
			// Update	: Bo xung thong tin thue bao
			+ ",'" + psQUOC_TICH + "'"
			+ ",'" + psLOAIGT_ID + "'"
			+ ",'" + psSO_GT + "'"
			+ ",'" + psDIACHI_TR + "'"
			+ ",'" + psNGAYCAP_GT + "'"
			+ ",'" + psNOICAP_GT + "'"
			+ ",'" + psDOITUONG_SD + "'"
			+ ",'" + psMUCPHI + "'"
			+ ");"
			+ "end;";			
		// System.out.println("test:" + s);
		return s;		
	}	
	
	public String laytt_mucphi(
		String ma_kh
		) throws Exception {
			String s ="begin ? :=admin_v2.pkg_thuphi_tainha.get_khachhang_thuphis('"+ma_kh+"'); end;";
		System.out.println(s);
		return s; 
	}
}