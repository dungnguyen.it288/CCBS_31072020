package neo;

import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

import javax.sql.RowSet;

public class agent extends NEOProcessEx {

	public String rec_laytt_agent(			
			String psTB
	){
        String s = "begin ?:= prepaid.PREPAID_FUNCTION.laytt_agent('"+ psTB+"'); end;";
        return s;
    }
	public NEOCommonData rec_laytt_agent_new(String psTB)
	{
        String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := prepaid.PREPAID_FUNCTION.laytt_agent(?); ";
		xs_ = xs_ + " end; ";
		
		
		
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		nei_.setDataSrc("PPS_DBMS");
		nei_.bindParameter(2,psTB);
		
		RowSet rs_ = null;
		try{
			rs_ = reqRSet(nei_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return new NEOCommonData(rs_);
    }
	public NEOCommonData value_capnhat_agent_new(
		String psagentid, 
        String psten_dai_ly, 
        String pskhu_vuc, 
        String psdai_dien, 
        String pschuc_vu,
        String psngay_sinh, 
        String psloai_gt, 
        String psso_gt, 
        String psngay_cap, 
        String psnoi_cap, 
        String pstel,
        String pswebsite, 
		String psquestion,
        String pseloadmsisdn, 
        String psso_gpkd, 
        String psso_hd_web,
        String psso_imsi, 
        String psuser_web, 
        String psso_hd, 
        String psngay_ky_hd, 
        String psma_dai_ly,
        String psprovinceid, 
        String psaddress, 
        String psfax, 
        String psemail, 
        String pscomments,
		String pspham_vi
	){
		String s = "begin ?:= prepaid.PREPAID_FUNCTION.capnhat_agent('"+
			psagentid+"','"+
	        psten_dai_ly+"','"+
	        pskhu_vuc+"','"+
	        psdai_dien+"','"+
	        pschuc_vu+"','"+
	        psngay_sinh+"','"+
	        psloai_gt+"','"+
	        psso_gt+"','"+
	        psngay_cap+"','"+
	        psnoi_cap+"','"+
	        pstel+"','"+
	        pswebsite+"','"+
			psquestion+"','"+
	        pseloadmsisdn+"','"+
	        psso_gpkd+"','"+
	        psso_hd_web+"','"+
	        psso_imsi+"','"+
	        psuser_web+"','"+
	        psso_hd+"','"+
	        psngay_ky_hd+"','"+
	        psma_dai_ly+"','"+
	        psprovinceid+"','"+
	        psaddress+"','"+
	        psfax+"','"+
	        psemail+"','"+
	        pscomments+"','"+
			pspham_vi+"','"+
			getUserVar("userID")+"','"+
			getUserVar("userIP")+"'); end;";		
		NEOExecInfo ei_ = new NEOExecInfo(s);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
	}
	public String value_capnhat_agent(
		String psagentid, 
        String psten_dai_ly, 
        String pskhu_vuc, 
        String psdai_dien, 
        String pschuc_vu,
        String psngay_sinh, 
        String psloai_gt, 
        String psso_gt, 
        String psngay_cap, 
        String psnoi_cap, 
        String pstel,
        String pswebsite, 
		String psquestion,
        String pseloadmsisdn, 
        String psso_gpkd, 
        String psso_hd_web,
        String psso_imsi, 
        String psuser_web, 
        String psso_hd, 
        String psngay_ky_hd, 
        String psma_dai_ly,
        String psprovinceid, 
        String psaddress, 
        String psfax, 
        String psemail, 
        String pscomments,
		String pspham_vi
	){
		String s = "begin ?:= prepaid.PREPAID_FUNCTION.capnhat_agent('"+
			psagentid+"','"+
	        psten_dai_ly+"','"+
	        pskhu_vuc+"','"+
	        psdai_dien+"','"+
	        pschuc_vu+"','"+
	        psngay_sinh+"','"+
	        psloai_gt+"','"+
	        psso_gt+"','"+
	        psngay_cap+"','"+
	        psnoi_cap+"','"+
	        pstel+"','"+
	        pswebsite+"','"+
			psquestion+"','"+
	        pseloadmsisdn+"','"+
	        psso_gpkd+"','"+
	        psso_hd_web+"','"+
	        psso_imsi+"','"+
	        psuser_web+"','"+
	        psso_hd+"','"+
	        psngay_ky_hd+"','"+
	        psma_dai_ly+"','"+
	        psprovinceid+"','"+
	        psaddress+"','"+
	        psfax+"','"+
	        psemail+"','"+
	        pscomments+"','"+
			pspham_vi+"','"+
			getUserVar("userID")+"','"+
			getUserVar("userIP")+"'); end;";		
		return s;
	}
	public String checktb_itc(String stb) {
		String s = "begin ?:= admin_v2.pkg_api_qlsp_itc.check_dau_itc('"+stb+"'); end;";	
		System.out.println(s);		
		return s;
	}
	
	public String checktb_itc_new(String stb) {
		String result="";
		String s = "begin ?:= admin_v2.pkg_api_qlsp_itc.check_dau_itc('"+stb+"'); end;";	
		System.out.println(s);		
		try
			{		    
				 result= this.reqValue("",s);			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
        return result;	
	}
	
	public String checktb_itc_s(String stb) {
		String s = "begin ?:= admin_v2.pkg_api_qlsp_itc.check_chuoi_itc('"+stb+"'); end;";	
		System.out.println(s);		
		return s;
	}
}