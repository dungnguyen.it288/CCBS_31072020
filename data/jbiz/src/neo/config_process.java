package neo;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class config_process extends NEOProcessEx {
	public String new_config_process(
		String pschema,
		String pname,
		String pstatus,
		String powner,
		String ptime,
		String precords,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pk_config_process.new_config_process("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(pname)+"',"
				+"'"+pstatus+"',"
				+"'"+ConvertFont.UtoD(powner)+"',"
				+"'"+ptime+"',"
				+"'"+precords+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	
	public String del_config_process(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pk_config_process.del_config_process("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String edit_config_process(
	String pschema,
		String pid,
		String pname,
		String pstatus,
		String powner,
		String ptime,
		String precords,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pk_config_process.edit_config_process("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pname)+"',"
				+"'"+pstatus+"',"
				+"'"+ConvertFont.UtoD(powner)+"',"
				+"'"+ptime+"',"
				+"'"+precords+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	//ngocntn----------
	public String new_config_process_dwh(
		String pschema,
		String pname,
		String pstatus,
		String powner,
		String ptime,
		String precords,
		String pjobid,
		String pfuncid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ccs_dwh.pk_config_process.new_config_process("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(pname)+"',"
				+"'"+pstatus+"',"
				+"'"+ConvertFont.UtoD(powner)+"',"
				+"'"+ptime+"',"
				+"'"+precords+"',"
				+"'"+pjobid+"',"
				+"'"+pfuncid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("DWHCCBS", s);
	}
	public String del_config_process_dwh(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ccs_dwh.pk_config_process.del_config_process("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("DWHCCBS", s);
	}
	public String edit_config_process_dwh(
	String pschema,
		String pid,
		String pname,
		String pstatus,
		String powner,
		String ptime,
		String precords,
		String pjobid,
		String pfuncid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ccs_dwh.pk_config_process.edit_config_process("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pname)+"',"
				+"'"+pstatus+"',"
				+"'"+ConvertFont.UtoD(powner)+"',"
				+"'"+ptime+"',"
				+"'"+precords+"',"
				+"'"+pjobid+"',"
				+"'"+pfuncid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("DWHCCBS", s);
	}
	
	public String record_ccbs(
			String puserid) throws Exception {
			String s ="begin ? :=ADMIN_V2.pk_config_process.GetRecordCCBS("
				+"'"+puserid+"'"
				+"); end;";
		return this.reqValue("", s);
	}
	
	public String record_rating(
			String puserid) throws Exception {
			String s ="begin ? :=ADMIN_V2.pk_config_process.GetRecordRating("
				+"'"+puserid+"'"
				+"); end;";
		return this.reqValue("", s);
	}
}