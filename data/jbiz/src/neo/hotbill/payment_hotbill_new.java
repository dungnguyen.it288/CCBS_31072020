package neo.hotbill;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;
import java.util.*;
import javax.sql.*;
import java.lang.Object.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.io.*;
public class payment_hotbill_new extends NEOProcessEx 

{
  	public void run() 
  	{
    	System.out.println("neo.qltn_new.payment_hotbill was called");
    	
  	}
public String get_agent_tb(String ma_tb )
			{
		 
			   String s1="declare "+
						"s varchar2(2000); "+
						" ma_tinh varchar2(200);"+
							"begin  begin "+
							"	s:='select ma_tinh from ccs_common.thue_bao where so_tb =''"+ma_tb+"''';"+
							"  execute immediate s into ma_tinh; "+
							" end; ?:=ma_tinh; end;";
			   	 
		String rs1=null;
		try
			{		    
				rs1 = "CCS_"+reqValue("", s1)+".";
			 
			} 
		catch (Exception ex) 
		    {            	
		       rs1 = getUserVar("sys_dataschema");
			}
		return rs1; 
	}
public String value_tamthuno(
		String psma_kh           ,
		String psma_tb           ,
		String pschukyno         ,
		String pstongtien	     ,			
		String psghichu          ,
		String pshttt        	 ,
		String ngannhangid       , 
		String ngaynganhang      ,
		String psngaytt        ,
		String userip		 , 
		String donviql   , 
		String mabc 

		
	)
	{
	String str ="";
	
	
	try 
	 {
		String s="begin ?:= admin_v2.pkg_hotbill_new.themmoi_tamthu_no("
		    +"'"+pschukyno+"',"
			+"'"+ psma_kh         +"',"
			+"'"+ psma_tb         +"',"	
			+"'"+ pstongtien          +"',"
			+"'"+ psghichu        +"',"					
			+"'"+ pshttt   	    +"',"			
			+"'"+ ngannhangid    +"',"			
			+"'"+ ngaynganhang          +"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"',"
			+"'"+ userip       +"',"
			+"'"+ donviql       +"',"
			+"'"+ mabc       +"'"
			+"); end;";			
		str = this.reqValue("",s);
		}
		catch (Exception ex) {
             str=ex.getMessage();		
		}
		return str;
	}
public String xoa_tamthuno(
		String psma_kh           ,
		String psma_tb           ,
		String psphieu         ,
		String psma_tn   ,
		String userip
		
		
	)
	{
	String str ="";
	try 
	 {
		String s="begin ?:= admin_v2.pkg_hotbill_new.delete_phieu_tamthuno_func("
			+"'"+ psma_kh         +"',"
			+"'"+ psma_tb         +"',"	
			+"'"+ psphieu         +"',"						
			+"'"+ psma_tn         +"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"',"
			+"'"+ userip          +"'"
			
			+"); end;";			
		str = this.reqValue("",s);
		}
		catch (Exception ex) {
             str=ex.getMessage();		
		}
		return str;
	}	
public String  check_ngay_tt( String ngaytt, String chuky)
		    {
			 
					String s= "declare last_date date :=last_day(sysdate); out_ varchar2(200);  begin begin if(to_date('"+ngaytt+"','dd/mm/yyyy')>last_date) or to_date('"+ngaytt+"','dd/mm/yyyy')<to_date('"+chuky+"','mmyyyy')  then out_:=1; else out_:=0; end if;  end;  ?:=out_; end;";
					System.out.println(s);
				String out_="0";
				try 
				{ 
				out_=this.reqValue("",s);
				}
				catch (Exception ex) {
					out_="1";	
				}
				return out_;
			}
public String value_themmoi_tamthu(
		String psma_kh           ,
		String psma_tb           ,
		String pschukyno         ,
		String pstien            ,
		String psvat             ,	
		String pstongtien	     ,			
		String psghichu          ,
		String pshttt        	 ,	
		String psngaynhap        ,
		String type              ,
		String psma_tn    ,
		String pstinhtl    ,
		String userip		
		
	)
	{
	String str ="";
	
	
	try 
	 {
		String s="begin ?:= admin_v2.pkg_hotbill_new.themmoi_tamthu("
		    +"'',"
			+"'"+ psma_kh         +"',"
			+"'"+ psma_tb         +"',"	
			+"'"+ pstien          +"',"
			+"'"+ psvat          +"',"
			+"'"+ pstongtien          +"',"
			+"'"+ psghichu        +"',"					
			+"'"+ pschukyno.replace(" ","") +"',"
			+"'"+ pshttt   	    +"',"			
			+"'"+ psngaynhap    +"',"			
			+"'"+ psma_tn          +"',"
			+""+ type       +","
			+"'"+ pstinhtl       +"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"',"
			+"'"+ userip       +"'"
			+"); end;";			
		str = this.reqValue("",s);
		}
		catch (Exception ex) {
             str=ex.getMessage();		
		}
		return str;
	}
public String value_chitra_tamthu(
        String phieuid           ,
		String psma_kh           ,
		String psma_tb           ,
		String pschukyno         ,
		String pstongtien	,			
		String psghichu          ,
		String pshttt        	 ,	
		String psngaynhap        ,
	    String type               ,
		String psma_tn        ,
		String pstinhtl    ,
		String userip 		
		
	)
	{
	String str ="";
	try 
	 {
		String s="begin ?:= admin_v2.pkg_hotbill_new.themmoi_tamthu("
		    +"'"+ phieuid         +"',"
			+"'"+ psma_kh         +"',"
			+"'"+ psma_tb         +"',"	
			+"0,"
			+"0,"
			+"'"+ Integer.parseInt(pstongtien)*-1+"',"
			+"'"+ psghichu        +"',"					
			+"'"+ pschukyno.replace(" ","") +"',"
			+"'"+ pshttt   	    +"',"			
			+"'"+ psngaynhap    +"',"			
			+"'"+ psma_tn       +"',"			
			+"'"+ type          +"',"
			+"'"+ pstinhtl      +"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"',"
			+"'"+ userip          +"'"
			+"); end;";			
		str = this.reqValue("",s);
		}
		catch (Exception ex) {
             str=ex.getMessage();		
		}
		return str;
	}
		
      
public String value_xoa_tamthu(
		String psma_kh           ,
		String psma_tb           ,
		String psphieu         ,
		String psma_tn   ,
		String pstinhtl   ,
		String userip
		
		
	)
	{
	String str ="";
	try 
	 {
		String s="begin ?:= admin_v2.pkg_hotbill_new.delete_vote_hotbill_func("
			+"'"+ psma_kh         +"',"
			+"'"+ psma_tb         +"',"	
			+"'"+ psphieu         +"',"						
			+"'"+ psma_tn         +"',"
			+"'"+ pstinhtl        +"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"',"
			+"'"+ userip          +"'"
			
			+"); end;";			
		str = this.reqValue("",s);
		}
		catch (Exception ex) {
             str=ex.getMessage();		
		}
		return str;
	}

	
}