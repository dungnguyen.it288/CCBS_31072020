package neo;

import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class user_infor
    extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb_capnhat was called");
  }
  
public String update_user( String 		 schema
						    ,String      userid
							,String      dap_check	
							,String      user_name
							,String      dob	
							,String      address
							,String      mobile
							,String      tel
							,String      email
							,String      sex
							,String      fax
							,String      company_name
							,String      company_address
							,String      company_tel
							,String      status	
							,String      donviql_id	
							,String      level_number
							,String      Role_thaotac
							,String      ma_bc
							,String      donvi
							,String      buucuc
							,String      chuyendb	
							,String      inhoadon	
							,String      gachno_pre	
							,String      xoaphieu_pre	
							,String      khaibao	
							,String      maso_nv
							,String      donvi_ct
							,String      gachht	
							,String      ngaythanhtoan	
							,String      seri
							,String      quyen	
							,String      soseri	
							,String      capquanly	
							,String      ma_tn
							,String      sys_userid
							,String      sys_userip
	) {
    String s="begin ? := "+getSysConst("FuncSchema")+"QTHT.capnhat_nguoidung("
							+ "'" +userid+ "'"
							+ ",'" +dap_check+ "'"	
							+ ",'" +user_name+ "'"
							+ "," +dob+ ""
							+ ",'" +address+ "'"
							+ ",'" +mobile+ "'"
							+ ",'" +tel+ "'"
							+ ",'" +email+ "'"
							+ ",'" +sex+ "'"
							+ ",'" +fax+ "'"
							+ ",'" +company_name+ "'"
							+ ",'" +company_address+ "'"
							+ ",'" +company_tel+ "'"
							+ ",'" +status+ "'"	
							+ ",'" +donviql_id+ "'"	
							+ ",'" +level_number+ "'"	
							+ ",'" +Role_thaotac+ "'"
							+ ",'" +ma_bc+ "'"
							+ ",'" +donvi+ "'"
							+ ",'" +buucuc+ "'"
							+ ",'" +chuyendb+ "'"	
							+ ",'" +inhoadon+ "'"	
							+ ",'" +gachno_pre+ "'"	
							+ ",'" +xoaphieu_pre+ "'"	
							+ ",'" +khaibao+ "'"	
							+ ",'" +maso_nv+ "'"
							+ ",'" +donvi_ct+ "'"
							+ ",'" +gachht+ "'"	
							+ "," +ngaythanhtoan+ ""	
							+ ",'" +seri+ "'"
							+ ",'" +quyen+ "'"	
							+ ",'" +soseri+ "'"	
							+ ",'" +capquanly+ "'"
							+ ",'" +ma_tn+ "'"
							+ ",'" +sys_userid+ "'"
							+ ",'" +sys_userip+ "'"
							+ ",'" +getUserVar("sys_dataschema")+ "'"
        +");"
        +"end;"
        ;
    return s;
  } 
  public String myEncodePwd(String pass) {
	return pass+pass;
 }
  
  public String encode_mk(String pass) {
    return "/main?"+CesarCode.encode("configFile")+"=admin/change_pass_ajax1"
        +"&"+CesarCode.encode("password")+"="+pass
		
        ;
   }
public String change_pass(  String 		 schema
							,String      psSYS_USERID	
							,String      psUSERID
						    ,String      psNEW_PASSWORD
							,String      psSYS_USERIP	
							
	) {
    String s="begin ? := "+CesarCode.decode(schema)+"QTHT.doi_matkhau1("
							+ "'" +psSYS_USERID+ "'"	
							+ ",'" +psUSERID+ "'"
							+ ",'" +psNEW_PASSWORD+ "'"	
							+ ",'" +psSYS_USERIP+ "'"
							+",'"+getUserVar("sys_dataschema")+"'"
							
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;
  }
  
  public String insert_user( String 	 schema
								,String      userid
								,String      password
								,String      ldap_check
								,String      user_name
								,String      dob
								,String      address
								,String      mobile
								,String      tel
								,String      email
								,String      sex
								,String      fax
								,String      company_name
								,String      company_address
								,String      company_tel
								,String      status
								,String      capnhat_user_pttb
								,String      donviql_id
								,String      ma_bc
								,String      donvi
								,String      buucuc
								,String      chuyendb
								,String      inhoadon
								,String      gachno_pre
								,String      xoaphieu_pre
								,String      khaibao
								,String      capnhat_user_gachno
								,String      donvi_ct
								,String      gachht
								,String      ngaythanhtoan
								,String      seri
								,String      quyen
								,String      soseri
								,String      capquanly
								,String      ma_tn
								,String      sys_userid
								,String      sys_userip
								

	) {
    String s="begin ? := "+CesarCode.decode(schema)+"QTHT.themmoi_nguoidung("
								+"'"+userid+"'"
								+",'"+password+"'"
								+",'"+ldap_check+"'"
								+",'"+user_name+"'"
								+","+dob
								+",'"+address+"'"
								+",'"+mobile+"'"
								+",'"+tel+"'"
								+",'"+email+"'"
								+",'"+sex+"'"
								+",'"+fax+"'"
								+",'"+company_name+"'"
								+",'"+company_address+"'"
								+",'"+company_tel+"'"
								+",'"+status+"'"
								+",'"+capnhat_user_pttb+"'"
								+",'"+donviql_id+"'"
								+",'"+ma_bc+"'"
								+",'"+donvi+"'"
								+",'"+buucuc+"'"
								+",'"+chuyendb+"'"
								+",'"+inhoadon+"'"
								+",'"+gachno_pre+"'"
								+",'"+xoaphieu_pre+"'"
								+",'"+khaibao+"'"
								+",'"+capnhat_user_gachno+"'"
								+",'"+donvi_ct+"'"
								+",'"+gachht+"'"
								+","+ngaythanhtoan
								+",'"+seri+"'"
								+",'"+quyen+"'"
								+",'"+soseri+"'"
								+",'"+capquanly+"'"
								+",'"+ma_tn+"'"
								+",'"+sys_userid+"'"
								+",'"+sys_userip+"'"
								+",'"+getUserVar("sys_dataschema")+"'"
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;
  }
  
public String phanquyen_nguoidung(  
							 String 	 schema 
							,String 	 psDsUser
							,String      psDsROLE	
							,String      psUSERID
						    ,String      psUSERIP
														
	) {
	//"+ CesarCode.decode(schema)+"
    String s=  "begin ? := "+CesarCode.decode(schema)+"QTHT.phanquyen_nguoidung("
						    + "'"  +psDsUser+ "'"
							+ ",'" +psDsROLE+ "'"
							+ ",'" +psUSERID+ "'"	
							+ ",'" +psUSERIP+ "'"
							+");"
							+"end;";
							System.out.println(s);
    return s;
  }  
  
  public String layds_phanquyen_nguoidung(String danhsach_user1)
	{
	String url_ ="/main";
	url_ = url_ + "?"+CesarCode.encode("configFile")+"=admin/frmuser_role_e_ajax1";
	url_ = url_ +"&"+CesarCode.encode("danhsach_user1")+"='" + danhsach_user1 +"'";
	url_ = url_ + "&sid="+Math.random();
	return url_;
	}
	
  public String check_user(String schema,String psuserid) 
	{
     String s=    "begin ? := "+schema+"QTHT.chkuser_exists("		
		+"'"+psuserid+"'"
		+");"
        +"end;"
        ;
		System.out.println(s);
    return s;
	}	
  	public String doi_matkhau(  String      psOLD_PASSWORD
						    ,String      psNEW_PASSWORD
							,String      psSYS_USERIP
							
	) {
	//System.out.println(PasswordProcess.passwordEncode(psNEW_PASSWORD));
    String s="begin ? := QTHT.doi_matkhau2("
							+ "'" +getUserVar("userID")+ "'"	
							+ ",'" +PasswordProcess.passwordEncode(psOLD_PASSWORD)+ "'"
							+ ",'"  +PasswordProcess.passwordEncode(psNEW_PASSWORD)+ "'"	
							+ ",'" +getUserVar("userID")+ "'"	
							+ ",'" +psSYS_USERIP+ "'"
							
        +");"
        +"end;"
        ;
	//System.out.println("-----"+s+"-----");
	//if ( s.equals("0") ) { //thanh cong
	//	setUserVar("sys_needchangepass","0");
	//	System.out.println("aaaaa---"+getUserVar("sys_needchangepass"));
		
	//}
    //System.out.println(s);
    return s;
  }
}
