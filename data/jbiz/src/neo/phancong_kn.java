package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class phancong_kn
    extends NEOProcessEx {

  public void run() {
    System.out.println("neo.pttb_tthd was called");
  }
	public String lay_dskn(int trangthai_kn)
	{
	String url_ ="/main";
	url_ = url_ + "?"+CesarCode.encode("configFile")+"=dongnd/phancong_kn_ajax";
	url_ = url_ +"&"+CesarCode.encode("trangthai_kn")+"=" + trangthai_kn ;
	url_ = url_ + "&sid="+Math.random();
	return url_;
	}
  
   public String lay_dskn_by_makn(String ma_kn) {
    return "begin ?:= QUANGBINH_ADMIN.GQKN_PC.lay_dskn_by_makn('"+CesarCode.decode(ma_kn)+"'); end;";
  }
  
  
  /*
  public String layds_httt(String kieugach_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tthd/common/ajax_httt"
        +"&"+CesarCode.encode("kieugach_id")+"="+kieugach_id
        ;
  }
  public String laytt_hdld(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hdld('"+userInput+"','"+CesarCode.decode(userid)+"'); end;";
  }
  public String laytt_hdcq(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hdcq('"+userInput+"','"+CesarCode.decode(userid)+"'); end;";
  }

  public String laytt_hdbm(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hdbm('"+userInput+"','"+CesarCode.decode(userid)+"'); end;";
  }

  public String laytt_hddc(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hddc('"+userInput+"','"+CesarCode.decode(userid)+"'); end;";
  }
  public String laytt_hddv(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hddv('"+userInput+"','"+CesarCode.decode(userid)+"'); end;";
  }

  public String laytt_hdld_theo_matb(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hdld_theo_matb('"+userInput+"','"+CesarCode.decode(userid)+"'); end;";
  }
  public String laytt_hdcq_theo_matb(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hdcq_theo_matb('"+userInput+"','"+CesarCode.decode(userid)+"'); end;";
  }

  public String laytt_hddv_theo_matb(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hddv_theo_matb('"+userInput+"','"+CesarCode.decode(userid)+"'); end;";
  }
  public String laytt_hddc_theo_matb(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hddc_theo_matb('"+userInput+"','"+CesarCode.decode(userid)+"'); end;";
  }

  public String layds_tbld(String mahd) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tthd/hdld/hdld_ajax_thuebao"
        +"&"+CesarCode.encode("ma_hd")+"="+mahd
        ;
  }
  public String layds_tbcq(String mahd) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tthd/hdcq/hdcq_ajax_thuebao"
        +"&"+CesarCode.encode("ma_hd")+"="+mahd
        ;
  }

  public String layds_tbbm(String mahd) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tthd/hdbm/hdbm_ajax_thietbi"
        +"&"+CesarCode.encode("ma_hd")+"="+mahd
        ;
  }

  public String layds_tbdc(String mahd) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tthd/hddc/hddc_ajax_thuebao"
        +"&"+CesarCode.encode("ma_hd")+"="+mahd
        ;
  }

  public String thanhtoanHDLD(String schema
									, String psMA_HD
                                     ,String psDsTHUEBAO_ID
									 ,String psUSERID
									 ,String psUSERIP
                                     ,String piChucnang_gachno
									 ,String pdNGAY_TT
                                     ,String pdNGAY_NH
									 ,String piHTTT_ID
                                     ,String piLOAITIEN_ID
									 ,String piNGANHANG_ID
									 ,String psSOSEC
                                     ,String psCHUNGTU
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.gachno_hdld("
        +"'"+psMA_HD+"'"
        +",'"+psDsTHUEBAO_ID+"'"
        +",'"+psUSERID+"'"
        +",'"+psUSERIP+"'"
        +","+piChucnang_gachno
        +","+pdNGAY_TT
        +","+pdNGAY_NH
        +","+piHTTT_ID
        +","+piLOAITIEN_ID
        +","+piNGANHANG_ID
        +",'"+psSOSEC+"'"
        +",'"+psCHUNGTU+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String xoaphieuHDLD(String schema
									, String psMA_HD
                                     ,String piPHIEU_ID
									 ,String psKIEUHUY
									 ,String psUSERID
									 ,String psUSERIP
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.xoaphieu_thanhtoan_hdld("
        +"'"+psMA_HD+"'"
        +",'"+piPHIEU_ID+"'"
		+",'"+psKIEUHUY+"'"
        +",'"+psUSERID+"'"
		+",'"+psUSERIP+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String thanhtoanHDDC(String schema
									, String psMA_HD
                                     ,String psDsTHUEBAO_ID
									 ,String psUSERID
									 ,String psUSERIP
                                     ,String piChucnang_gachno
									 ,String pdNGAY_TT
                                     ,String pdNGAY_NH
									 ,String piHTTT_ID
                                     ,String piLOAITIEN_ID
									 ,String piNGANHANG_ID
									 ,String psSOSEC
                                     ,String psCHUNGTU
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.gachno_hddc("
        +"'"+psMA_HD+"'"
        +",'"+psDsTHUEBAO_ID+"'"
        +",'"+psUSERID+"'"
        +",'"+psUSERIP+"'"
        +","+piChucnang_gachno
        +","+pdNGAY_TT
        +","+pdNGAY_NH
        +","+piHTTT_ID
        +","+piLOAITIEN_ID
        +","+piNGANHANG_ID
        +",'"+psSOSEC+"'"
        +",'"+psCHUNGTU+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String xoaphieuHDDC(String schema
									, String psMA_HD
                                     ,String piPHIEU_ID
									 ,String psKIEUHUY
									 ,String psUSERID
									 ,String psUSERIP
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.xoaphieu_thanhtoan_hddc("
        +"'"+psMA_HD+"'"
        +",'"+piPHIEU_ID+"'"
		+",'"+psKIEUHUY+"'"
        +",'"+psUSERID+"'"
		+",'"+psUSERIP+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String thanhtoanHDDV(String schema
									, String psMA_HD
                                     ,String psUSERID
									 ,String psUSERIP
                                     ,String piChucnang_gachno
									 ,String pdNGAY_TT
                                     ,String pdNGAY_NH
									 ,String piHTTT_ID
                                     ,String piLOAITIEN_ID
									 ,String piNGANHANG_ID
									 ,String psSOSEC
                                     ,String psCHUNGTU
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.gachno_hddv("
        +"'"+psMA_HD+"'"
        +",'"+psUSERID+"'"
        +",'"+psUSERIP+"'"
        +","+piChucnang_gachno
        +","+pdNGAY_TT
        +","+pdNGAY_NH
        +","+piHTTT_ID
        +","+piLOAITIEN_ID
        +","+piNGANHANG_ID
        +",'"+psSOSEC+"'"
        +",'"+psCHUNGTU+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String xoaphieuHDDV(String schema
									, String psMA_HD
                                     ,String piPHIEU_ID
									 ,String psKIEUHUY
									 ,String psUSERID
									 ,String psUSERIP
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.xoaphieu_thanhtoan_hddv("
        +"'"+psMA_HD+"'"
        +",'"+piPHIEU_ID+"'"
		+",'"+psKIEUHUY+"'"
        +",'"+psUSERID+"'"
		+",'"+psUSERIP+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String thanhtoanHDBM(String schema
									, String psMA_HD
                                     ,String psUSERID
									 ,String psUSERIP
                                     ,String piChucnang_gachno
									 ,String pdNGAY_TT
                                     ,String pdNGAY_NH
									 ,String piHTTT_ID
                                     ,String piLOAITIEN_ID
									 ,String piNGANHANG_ID
									 ,String psSOSEC
                                     ,String psCHUNGTU
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.gachno_hdbm("
        +"'"+psMA_HD+"'"
        +",'"+psUSERID+"'"
        +",'"+psUSERIP+"'"
        +","+piChucnang_gachno
        +","+pdNGAY_TT
        +","+pdNGAY_NH
        +","+piHTTT_ID
        +","+piLOAITIEN_ID
        +","+piNGANHANG_ID
        +",'"+psSOSEC+"'"
        +",'"+psCHUNGTU+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String xoaphieuHDBM(String schema
									, String psMA_HD
                                     ,String piPHIEU_ID
									 ,String psKIEUHUY
									 ,String psUSERID
									 ,String psUSERIP
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.xoaphieu_thanhtoan_hdbm("
        +"'"+psMA_HD+"'"
        +",'"+piPHIEU_ID+"'"
		+",'"+psKIEUHUY+"'"
        +",'"+psUSERID+"'"
		+",'"+psUSERIP+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String thanhtoanHDCQ(String schema
									, String psMA_HD
                                     ,String psDsMATB
									 ,String psUSERID
									 ,String psUSERIP
                                     ,String piChucnang_gachno
									 ,String pdNGAY_TT
                                     ,String pdNGAY_NH
									 ,String piHTTT_ID
                                     ,String piLOAITIEN_ID
									 ,String piNGANHANG_ID
									 ,String psSOSEC
                                     ,String psCHUNGTU
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.gachno_hdcq("
        +"'"+psMA_HD+"'"
        +",'"+psDsMATB+"'"
        +",'"+psUSERID+"'"
        +",'"+psUSERIP+"'"
        +","+piChucnang_gachno
        +","+pdNGAY_TT
        +","+pdNGAY_NH
        +","+piHTTT_ID
        +","+piLOAITIEN_ID
        +","+piNGANHANG_ID
        +",'"+psSOSEC+"'"
        +",'"+psCHUNGTU+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String xoaphieuHDCQ(String schema
									, String psMA_HD
                                     ,String piPHIEU_ID
									 ,String psKIEUHUY
									 ,String psUSERID
									 ,String psUSERIP
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.xoaphieu_thanhtoan_hdcq("
        +"'"+psMA_HD+"'"
        +",'"+piPHIEU_ID+"'"
		+",'"+psKIEUHUY+"'"
        +",'"+psUSERID+"'"
		+",'"+psUSERIP+"'"
        +");"
        +"end;"
        ;
    return s;

  }
    */
}
