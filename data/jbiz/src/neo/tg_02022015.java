//CMDVOracleDS
package neo;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import neo.ws.WSDL;
import java.util.*;
import javax.sql.*;

public class tg extends NEOProcessEx {
	public void run() {
		System.out.println("neo.gadmin was called");
	}

	public String doc_goi(String psdichvu)
	{
		return "/main?" + CesarCode.encode("configFile")+"=vinacore/ajax_goi_3gs"
			+ "&" + CesarCode.encode("dichvu_id") + "=" + psdichvu;
	} 
	public NEOCommonData value_dangky(
		String psSoTB,
		String psDichvu,
		String psGoi,
		String psThaotac,
		String psGhichu,
		String psUserid,
		String psAgent,
		String psIP)
	{
		String rs_="";
		String sGoi_="";
		if (psGoi=="MT")
			sGoi_ = "MT_DF";
		else
			sGoi_ = psGoi;
		
		try{			
			rs_ = reqValue("CMDVOracleDS","begin ?:= neo.tg.dangky('"+
						psSoTB+"','"+						
						psDichvu+"','"+
						psGoi+"','"+
						psThaotac+ "','"+
						psGhichu+"','"+
						psUserid+"','"+
						psAgent+"','"+
						psIP+"'); end;");
			
			if (rs_.equals("1")) {
				WSDL wsdl=new WSDL();
			
				String id_=reqValue("CMDVOracleDS","begin ?:=getSeq('WSDL_3G_SEQ'); end;");
				
				String methodName = reqValue("CMDVOracleDS","begin ?:=neo.tg.laytt_phuongthucdk('"+psGoi+"','"+psThaotac+"'); end;");
				
				rs_=wsdl.dangky3G(id_,psSoTB,sGoi_, methodName);		
				
				if (rs_.indexOf("|") !=-1) {
					rs_ = rs_.substring(0,rs_.indexOf("|"));
				}
				if (rs_.equals("0")){
					rs_="1";
				}else{
					rs_=reqValue("CMDVOracleDS","begin ?:=neo.tg.capnhat_tt('"+psSoTB+"','"+psGoi+"','"+rs_+"'); end;");
				}
			}
		} catch (Exception e) {
        	System.err.println(e.toString());
			rs_="Co loi trong qua trinh thuc hien, ma loi: TG004";
      	}
		
		
		String xs_ = "begin ?:='"+rs_+"'; end;";
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		nei_.setDataSrc("CMDVOracleDS");
		return new NEOCommonData(nei_);
	}
	public NEOCommonData rec_layds(
		String psSoTB,
		String psUserid,
		String psAgent,
		String psIP)
	{
		String xs_ = "begin ?:= neo.tg.layds('"+
						psSoTB+"','"+						
						psUserid+"','"+
						psAgent+"','"+
						psIP+"'); end;";
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		nei_.setDataSrc("CMDVOracleDS");

		RowSet rs_ = null;
		try{
			rs_ = reqRSet(nei_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}
	
		public String doc_ds_goi(String psSoTB)
	{
		return "/main?" + CesarCode.encode("configFile")+"=vinacore/ajax_ds_goi_3gs"
			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB;
	} 
	public String doc_lichsu_3g(String psSoTB)
	{
		return "/main?" + CesarCode.encode("configFile")+"=vinacore/ajax_lichsu_3gs"
			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB;
	}
	
	/*	
	public NEOCommonData value_reset_service(
		String psSoTB,
		String psGoi)
	{
		String rs_="";
		try{			

			WSDL wsdl=new WSDL();
			String id_=reqValue("CMDVOracleDS","begin ?:=getSeq('WSDL_3G_SEQ'); end;");
			rs_=wsdl.dangky3G(id_,psSoTB,psGoi,"1");				
			if (rs_.equals("0")){
				rs_="1";
			}else{
				rs_=reqValue("CMDVOracleDS","begin ?:=neo.tg.capnhat_tt('"+psSoTB+"','"+psGoi+"','"+rs_+"'); end;");
			}

		} catch (Exception e) {
        	System.err.println(e.toString());
			rs_="Co loi trong qua trinh thuc hien, ma loi: TG004";
      	}
		String xs_ = "begin ?:='"+rs_+"'; end;";
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		nei_.setDataSrc("CMDVOracleDS");
		return new NEOCommonData(nei_);
	}

	
	public NEOCommonData value_dk_dv_3gs(
		String psSoTB,
		String psGoi,
		String psThaotac
		)
	{
		String rs_="";
		try{			
				WSDL wsdl=new WSDL();
				String id_=reqValue("CMDVOracleDS","begin ?:=getSeq('WSDL_3G_SEQ'); end;");
				rs_=wsdl.dangky3G(id_,psSoTB,psGoi,psThaotac);	
				if (rs_.equals("0")){
					rs_="1";
				}
			
		} catch (Exception ex) {
			ex.printStackTrace();
      	}
		String xs_ = "begin ?:='"+rs_+"'; end;";
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		nei_.setDataSrc("CMDVOracleDS");
		return new NEOCommonData(nei_);
	}
	*/
}