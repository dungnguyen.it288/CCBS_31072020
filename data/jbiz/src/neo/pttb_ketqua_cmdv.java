package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class pttb_ketqua_cmdv
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb_ketqua_cmdv");
  }

  
  public String gui_cmdv(String schema
							,String psMA_HD
							,String psMA_TB
							,String piTRANGTHAITH_ID
							,String pstungay
							,String psdenngay
							,String piLOAIYC_ID
							,String piLOAIHD_ID
							, String piSONGAY_CANHBAO
							,String piDICHVUVT_ID
							
							) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"cmdv_tracuu.gui_cmdv('"+getUserVar("userID")+"'"
		+",'"+getUserVar("sys_agentcode")+"'"
		+",'"+getUserVar("sys_dataschema")+"'"
		+",'"+psMA_HD+"'"
		+",'"+psMA_TB+"'"
		+",'"+piTRANGTHAITH_ID+"'"
		+",'"+pstungay+"'"
		+",'"+psdenngay+"'"
		+",'"+piLOAIYC_ID+"'"
		+",'"+piLOAIHD_ID+"'"
		+",'"+piSONGAY_CANHBAO+"'"
		+",'"+piDICHVUVT_ID+"'"
        +");"
        +"end;"
        ;
    return s;
  }	
}
