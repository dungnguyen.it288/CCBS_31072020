package neo.NEOConvert.HSSF;

import neo.smartui.process.NEOProcessEx;
import neo.NEOConvert.*;
import oracle.jdbc.driver.*;
import oracle.jdbc.*;
import java.sql.*;
import org.apache.poi.hssf.usermodel.*;
import java.io.*;


public class OracFuncToExcell  extends NEOProcessEx 
{
	oracle.jdbc.driver.OracleConnection conn;
	String ServerName;
	String Port;
	String SID;
	String User;
	String Schema;
	String Password;
	String fileName;
	int FetchSize=1000;
	int sheetmaxrow = 65535;
	String callFuncSql = "";
	java.util.Vector<Parameter> parameters = new java.util.Vector();
	
	public void removeAllParameter()
	{
		parameters.removeAllElements();
	}
	public void addParameter(String type,Object value, int mode)
	{
		Parameter par = new Parameter();
		par.setType(type);
		if (value != null)
		{
			par.setValue(value);
		}
		else
		{
			par.setValue("null");
		}
		par.setMode(mode);
		parameters.addElement((Parameter)par);
	}
	
	public String getCallFuncSql()
	{
		return callFuncSql;
	}
	public void setCallFuncSql(String var)
	{
		callFuncSql = var;
	}
	
	public String getServerName()
	{
		return ServerName;
	}
	public void setServerName(String var)
	{
		ServerName = var;
	}
	
	public String getPort()
	{
		return Port;
	}
	public void setPort(String var)
	{
		Port = var;
	}
	
	public String getSID()
	{
		return SID;
	}
	public void setSID(String var)
	{
		SID = var;
	}
	
	public String getUser()
	{
		return User;
	}
	public void setUser(String var)
	{
		User = var;
	}
	
	public String getSchema()
	{
		return Schema;
	}
	public void setSchema(String var)
	{
		Schema = var;
	}
	
	public String getPassword()
	{
		return Password;
	}
	public void setPassword(String var)
	{
		Password = var;
	}
	
	public String getFileName()
	{
		return fileName;
	}
	public void setFileName(String var)
	{
		fileName = var;
	}
	
	public int getFetchSize()
	{
		return FetchSize;
	}
	public void setFetchSize(int var)
	{
		FetchSize = var;
	}
	
	public OracFuncToExcell()
		throws IOException
	{
		ServerName = "laptop";
		Port = "1521";
		SID = "home";
		User= "SLA_ADMIN";
		Schema = "SLA_GDKH";
		Password = "a";
		fileName = "D:\\DBF\\EXPORT\\SONLA\\dm2.xls";
		callFuncSql = "begin ? := sla_admin.pttb_dulieu.layds_doituong_muccuoc(?,?); end;";
		
	}
	public void connectOracle()
		throws SQLException,java.io.IOException
	{	
		DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
		conn = (oracle.jdbc.driver.OracleConnection)DriverManager.getConnection("jdbc:oracle:thin:@//"+ServerName+":"+Port+"/"+SID,User,Password);
	}
	public int FillDataFromOracleFunc()
		throws java.io.IOException, java.sql.SQLException
	{
		int count=0;
		int sheetnumber=0;
		int rownum = 0;
		int numCol = 0;
		int i = 0;
		String colname;
		String coltype;
		HSSFCell cell;
		HSSFRow row;
		HSSFSheet sh;
		HSSFWorkbook wb = new HSSFWorkbook();
		FileOutputStream fo = new FileOutputStream(fileName);
		java.sql.ResultSet rset;
		oracle.jdbc.OracleResultSetMetaData oraMetaData;
		
		//oracle.jdbc.driver.OracleCallableStatement stmt =(oracle.jdbc.driver.OracleCallableStatement) conn.prepareCall(selsql);
		java.sql.CallableStatement stmt = conn.prepareCall(callFuncSql);
		int parnum = parameters.size();
		for(i=0; i<parnum; i++)
		{
			int parIndex = i+1;
			Parameter par = (Parameter) parameters.elementAt(i);
			int mode = par.getMode();
			String type = par.type;
			Object value = par.getValue();
			if (mode==java.sql.ParameterMetaData.parameterModeOut)
			{
				
				if (type == "sys_refcursor")
				{
					try
					{
						stmt.registerOutParameter(parIndex,oracle.jdbc.driver.OracleTypes.CURSOR);
					}
					catch(Exception ex)
					{
						System.out.println(type + " " + ex.getMessage());
					}					
				}
				else if (type == "varchar2")
				{
					stmt.registerOutParameter(parIndex,java.sql.Types.VARCHAR);
				}
				else if (type == "number")
				{
					stmt.registerOutParameter(parIndex,java.sql.Types.NUMERIC);
				}
				else if (type == "date")
				{
					stmt.registerOutParameter(parIndex,java.sql.Types.DATE);
				}
				else
				{
					System.out.println("invalid parameter type:"+ type);
					stmt.close();
					conn.close_statements();
					conn.close();
					wb.write(fo);
					fo.close();
					return  -1;
				}
			}
			else if (mode == java.sql.ParameterMetaData.parameterModeIn)
			{
				if(type == "varchar2")
				{
					stmt.setString(parIndex,(String)value);
				}
				else if(type == "number")
				{
					stmt.setDouble(parIndex,Double.parseDouble(value.toString()));
				}				
				else if (type == "date")
				{
					stmt.setDate(parIndex,Date.valueOf(value.toString()) );
				}
				else
				{
					System.out.println("invalid parameter type:"+ type);
					stmt.close();
					conn.close_statements();
					conn.close();
					wb.write(fo);
					fo.close();
					return  -1;
				}
			}
			else
			{
				System.out.println("invalid parameter mode:"+ mode);
				stmt.close();
				conn.close_statements();
				conn.close();
				wb.write(fo);
				fo.close();
				return  -1;
			}
		}
		try
		{
			stmt.execute();
			rset = (ResultSet)stmt.getObject(1);
			sheetnumber++;
			sh = wb.createSheet("sheet" + sheetnumber);
			rownum = 0;
			row = sh.createRow(rownum);
			rownum++;
			oraMetaData = (oracle.jdbc.OracleResultSetMetaData)rset.getMetaData();
			numCol = oraMetaData.getColumnCount();
			for(i = 0;i<numCol-1;i++)
			{
				cell = row.createCell(i);
				colname = oraMetaData.getColumnName(i+2);
				cell.setCellValue(colname);			
			}
			boolean cont = true;
			while(rset.next())
			{
				if(rownum>sheetmaxrow)
				{
					sheetnumber++;
					sh = wb.createSheet("sheet" + sheetnumber);
					rownum = 0;
					row = sh.createRow(rownum);
					rownum++;
					for(i = 0;i<numCol-1;i++)
					{
						cell = row.createCell(i);
						colname = oraMetaData.getColumnName(i+2);
						cell.setCellValue(colname);			
					}
				}				
				row = sh.createRow(rownum);
				rownum++;					
				count++;
				for(i = 0;i<numCol-1;i++)
				{
					cell = row.createCell(i);
					if ((oraMetaData.getColumnTypeName(i+2)).equalsIgnoreCase("NUMBER"))
					{						
						cell.setCellValue(rset.getDouble(i+2));
					}
					else if ((oraMetaData.getColumnTypeName(i+2)).equalsIgnoreCase("VARCHAR2"))
					{
						cell.setCellValue(rset.getString(i+2));
					}	
					else if ((oraMetaData.getColumnTypeName(i+1)).equalsIgnoreCase("DATE"))
					{
						cell.setCellValue(rset.getDate(i+2));
					}	
					else
					{
						cell.setCellValue(rset.getString(i+2));
					}
				}
			}
			rset.close();
		}
		catch(Exception ex)
		{
			System.out.println("Error on execute:"+ex.getMessage());
			stmt.close();
			conn.close_statements();
			conn.close();
			wb.write(fo);
			fo.close();
			return  -1;
		}
		stmt.close();
		conn.close_statements();
		conn.close();
		wb.write(fo);
		fo.close();	
		return count;
	}
	public String run()		
		throws java.io.IOException,java.sql.SQLException
	{
		try
		{
			this.connectOracle();
			int count =this.FillDataFromOracleFunc();
			return "da xuat "+count+" ban ghi.";
		}
		catch(Exception ex)
		{
			return  ex.getMessage();
		}		
		
	}
}
