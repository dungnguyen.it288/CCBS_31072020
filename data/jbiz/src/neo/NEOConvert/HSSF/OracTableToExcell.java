package neo.NEOConvert.HSSF;

import neo.smartui.process.NEOProcessEx;
import neo.NEOConvert.*;
import oracle.jdbc.driver.*;
import oracle.jdbc.*;
import java.sql.*;
import org.apache.poi.hssf.usermodel.*;
import java.io.*;
public class OracTableToExcell extends NEOProcessEx 
{	
	oracle.jdbc.driver.OracleConnection conn;
	String ServerName;
	String Port;
	String SID;
	String User;
	String Schema;
	String Password;
	String TableName;
	String fileName;
	int FetchSize=1000;
	int sheetmaxrow = 65535;
	String selsql = "";
	
	MyLog log = new MyLog();
	
	public MyLog getLogFile()
	{
		return log;
	}
	
	public String getSelsql()
	{
		return selsql;
	}
	public void setSelsql(String var)
	{
		selsql = var;
	}
	
	public String getServerName()
	{
		return ServerName;
	}
	public void setServerName(String var)
	{
		ServerName = var;
	}
	
	public String getPort()
	{
		return Port;
	}
	public void setPort(String var)
	{
		Port = var;
	}
	
	public String getSID()
	{
		return SID;
	}
	public void setSID(String var)
	{
		SID = var;
	}
	
	public String getUser()
	{
		return User;
	}
	public void setUser(String var)
	{
		User = var;
	}
	
	public String getSchema()
	{
		return Schema;
	}
	public void setSchema(String var)
	{
		Schema = var;
	}
	
	public String getPassword()
	{
		return Password;
	}
	public void setPassword(String var)
	{
		Password = var;
	}
	
	public String getTableName()
	{
		return TableName;
	}
	public void setTableName(String var)
	{
		TableName = var;
	}
	
	public String getFileName()
	{
		return fileName;
	}
	public void setFileName(String var)
	{
		fileName = var;
	}
	
	public int getFetchSize()
	{
		return FetchSize;
	}
	public void setFetchSize(int var)
	{
		FetchSize = var;
	}
	
	public OracTableToExcell()
		throws IOException
	{
		ServerName = "laptop";
		Port = "1521";
		SID = "home";
		User= "SLA_ADMIN";
		Schema = "SLA_GDKH";
		Password = "a";
		TableName = "KHACHHANGS_PTTB";
		fileName = "D:\\DBF\\EXPORT\\SONLA\\dm2.xls";
		selsql = "SELECT * FROM "+Schema+"."+TableName+" where rownum < 100000";		
	}
	
	public void connectOracle()
		throws SQLException,java.io.IOException
	{
		log.writeln("Connecting to database..");	
		DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
		conn = (oracle.jdbc.driver.OracleConnection)DriverManager.getConnection("jdbc:oracle:thin:@//"+ServerName+":"+Port+"/"+SID,Schema,Password);
	}
	
	public int FillData()
		throws java.io.IOException, java.sql.SQLException
	{
		java.sql.Statement stmt = conn.createStatement();

		oracle.jdbc.OracleResultSet rset = (oracle.jdbc.OracleResultSet)stmt.executeQuery("select count(1) from (" + selsql + ")");
		String colname;
		String sql;
		long recordnumber = 0;
		int rownum = 0;
		int numCol = 0;	
		if (rset.next())
		{
			recordnumber = rset.getInt(1);
		}

		int recordpoint=1;
		
		sql = "select * from (" + selsql + ")";
		stmt = conn.createStatement();
		rset = (oracle.jdbc.OracleResultSet)stmt.executeQuery(sql);
		oracle.jdbc.OracleResultSetMetaData oraMetaData;
		oraMetaData = (oracle.jdbc.OracleResultSetMetaData)rset.getMetaData();
		numCol = oraMetaData.getColumnCount();
		HSSFCell cell;
		HSSFRow row;
		
		HSSFWorkbook wb = new HSSFWorkbook();
		
		int count=0;
		int i;
		int sheetnumber;
		recordpoint = 0;
		while (recordpoint<recordnumber)
		{
			sheetnumber = recordpoint/sheetmaxrow + 1;
			HSSFSheet sh = wb.createSheet("sheet" + sheetnumber);
			rownum = 0;
			row = sh.createRow(rownum);
			rownum++;		
			for(i = 0;i<numCol-1;i++)
			{
				cell = row.createCell(i);
				colname = oraMetaData.getColumnName(i+2);
				cell.setCellValue(colname);
				log.writeln(colname);				
			}
			
			int sheetTotalRow = recordpoint + sheetmaxrow;
			rset = null;
			sql = "select * from (" + selsql + ") where rownum<=" + recordnumber + " and rownum>"+ recordpoint +" and rownum<=" + sheetTotalRow;
			//stmt = conn.createStatement();
			rset = (oracle.jdbc.OracleResultSet)stmt.executeQuery(sql);
			
			while(rset.next())
			{
				
				row = sh.createRow(rownum);

				rownum++;				
				
				count++;
				for(i = 0;i<numCol-1;i++)
				{
					cell = row.createCell(i);
					if ((oraMetaData.getColumnTypeName(i+2)).equalsIgnoreCase("NUMBER"))
					{						
						cell.setCellValue(rset.getDouble(i+2));
					}
					else if ((oraMetaData.getColumnTypeName(i+2)).equalsIgnoreCase("VARCHAR2"))
					{
						cell.setCellValue(rset.getString(i+2));
					}	
					else if ((oraMetaData.getColumnTypeName(i+1)).equalsIgnoreCase("DATE"))
					{
						cell.setCellValue(rset.getDate(i+2));
					}	
					else
					{
						cell.setCellValue(rset.getString(i+2));
					}
				}
				
			}
			
			recordpoint = recordpoint + sheetmaxrow;
			rset.close();
			//stmt.close();
		}
		FileOutputStream fo = new FileOutputStream(fileName);
		wb.write(fo);
		fo.close();
		//stmt.close();
		return count;
	}	
	
	public String run()		
		throws java.io.IOException,java.sql.SQLException
	{
		try
		{
			this.connectOracle();
			log.clear();
			int count =this.FillData();
			return "da xuat "+count+" ban ghi.";
		}
		catch(Exception ex)
		{
			return  ex.getMessage();
		}		
		
	}
}
