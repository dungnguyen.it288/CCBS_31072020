package neo.NEOConvert.HSSF;


class Parameter 
{
	Object value;
	public Object getValue()
	{
		return value;
	}
	public void setValue(Object _value)
	{
		value = _value;
	}
	String type;
	public String getType()
	{
		return type;
	}
	public void setType(String _type)
	{
		type = _type;
	}
	int mode;
	public int getMode()
	{
		return  mode;
	}
	public void setMode(int _mode)
	{
		mode = _mode;
	}
}
