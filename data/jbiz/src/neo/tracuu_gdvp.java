package neo;

//import java.io.File;
//import java.io.IOException;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.RowSet;

import org.apache.commons.io.FileUtils;

import neo.smartui.SessionProcess;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

import neo.qlsp.*;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import javax.xml.parsers.DocumentBuilder; 
import javax.xml.parsers.DocumentBuilderFactory; 
import javax.xml.parsers.ParserConfigurationException; 
import org.w3c.dom.Document; 
import org.w3c.dom.Element; 
import org.w3c.dom.Node; 
import org.w3c.dom.NodeList; 
import org.xml.sax.InputSource; 

public class tracuu_gdvp extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("Search");		
	}
	
	public String getSession(){
		String session = "";
		File file = new File (this.getSysConst("ConfigCompiledDir")+"pttb/laphd/chonso/session_api.neo");
		if (!file.exists() || file.lastModified()<System.currentTimeMillis()-50000){
			boolean del = true;
			if (file.exists()){
				del=file.delete();
			}			
			session = Get_API_Search.login();			
			if (del){
				try {
					FileUtils.writeStringToFile(file,session);
				} catch (IOException e) {
					session = e.getMessage();
				}
			}
		}else{
			try {
				session = FileUtils.readFileToString(file);
			} catch (IOException e) {}
		}
		return session;
	}
	

	public String doc_thongtin_goi(String psSoTB)
	{

		try{
			String session = getSession();
			StringBuffer ret = new StringBuffer();
			String jsonString = Get_API_Search.getSubCCBS(session,psSoTB);	
			System.out.println(jsonString);
			String tpl = FileUtils.readFileToString(new File(this.getSysConst("ConfigDir")+"pttb/tracuu/tracuu_gdvp/tc_data_ajax_api.htm"));
			JSONParser jsonParser = new JSONParser();	            
			JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonString);	                           
		    String error_code = (String) jsonObject.get("error_code");
			if(error_code.equals("0")) {
				JSONArray jsonArray = (JSONArray) jsonObject.get("result");
				int stt;
				int i;
				for (i = 0; i < jsonArray.size(); i++) {
					JSONObject jsonObject_ = (JSONObject) jsonArray.get(i);				
					stt				= i+1;
					String familyid 			= getString(jsonObject_.get("familyid"));
					String data_con		= getString(jsonObject_.get("package_remain"));
					String data_cap		= getString(jsonObject_.get("volume"));
					String tong_subpool		= getString(jsonObject_.get("subpool"));	
					String dung_subpool			= getString(jsonObject_.get("subpool_use"));	
					String packageid			= getString(jsonObject_.get("packageid"));
					String con_subpool	= getString(jsonObject_.get("subpool_free"));				
					String service_code			= getString(jsonObject_.get("service_code"));	
											
					ret.append(tpl.replace("{f01packageid}",packageid+"").replace("{f01service_code}",service_code).replace("{f01data_con}",data_con).replace("{f01data_cap}",data_cap).replace("{f01tong_subpool}",tong_subpool).replace("{f01dung_subpool}",dung_subpool).replace("{f01con_subpool}",con_subpool));			
				}
			} else {
				
				ret.append(tpl.replace("{f01packageid}","").replace("{f01service_code}","").replace("{f01data_con}","").replace("{f01data_cap}","").replace("{f01tong_subpool}","").replace("{f01dung_subpool}","").replace("{f01con_subpool}",""));
			}
				
			getEnvInfo().setUserVar("tracuu_data_api",ret.toString());
			System.out.println(ret.toString());		
			//return ret.toString();
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		return "/main?" + CesarCode.encode("configFile")+"=pttb/tracuu/tracuu_gdvp/tc_data_ajax";
	}
	
	private String getString(Object ojb) {
		if(ojb == null) return "";
		return (String) ojb;
	}
	
	public String doc_lichsu_goi(String psSoTB)
	{

		try{
			String session = getSession();
			StringBuffer ret = new StringBuffer();
			String jsonString = Get_API_Search.getLogAPI(session,psSoTB);	
			System.out.println("RS: \n"+jsonString);
			System.out.println("-----------------------------");
			String tpl = FileUtils.readFileToString(new File(this.getSysConst("ConfigDir")+"pttb/tracuu/tracuu_gdvp/tc_data_ajax_log_api.htm"));
			JSONParser jsonParser = new JSONParser();	            
			JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonString);
			String error_code = (String) jsonObject.get("error_code");
			if(error_code.equals("0")) {
		            
				JSONArray jsonArray = (JSONArray) jsonObject.get("result");
				System.out.println("-----------------------------------" + jsonArray.size());
				int size_ = 0;
				if(jsonArray.size() > 100) size_ = 100;
				else size_ = jsonArray.size();
				int stt;
				int i;
				for (i = 0; i < size_; i++) {
					JSONObject jsonObject_ = (JSONObject) jsonArray.get(i);				
					stt				= i+1;
					String MSISDN 			= getString(jsonObject_.get("MSISDN"));
					String METHOD		= getString(jsonObject_.get("METHOD"));
					String CODE		= getString(jsonObject_.get("CODE"));
					String LOG_DATE		= getString(jsonObject_.get("LOG_DATE"));	
					String CONTENT			= getString(jsonObject_.get("CONTENT"));
					String REQ = getString(jsonObject_.get("REQ"));
					ret.append(tpl.replace("{f01MSISDN}",MSISDN+"").replace("{f01METHOD}",METHOD).replace("{f01CODE}",CODE).replace("{f01LOG_DATE}",LOG_DATE).replace("{f01CONTENT}",CONTENT).replace("{f01REQ}",REQ));			
				}
			}
			else {
				ret.append(tpl.replace("{f01MSISDN}","").replace("{f01METHOD}","").replace("{f01CODE}","").replace("{f01LOG_DATE}","").replace("{f01CONTENT}","").replace("{f01REQ}",""));	
			}	
			getEnvInfo().setUserVar("tracuu_data_api2",ret.toString()); 
			//System.out.println(ret.toString());		
			//return ret.toString();
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		return "/main?" + CesarCode.encode("configFile")+"=pttb/tracuu/tracuu_gdvp/tc_lssd_data_log";
	}
	
	public String doc_lichsu_pool_goi(String psSoTB)
	{

		try{
			String session = getSession();
			StringBuffer ret = new StringBuffer();
			String jsonString = Get_API_Search.getListSubCCBS(session,psSoTB);	
			System.out.println(jsonString);
			String tpl = FileUtils.readFileToString(new File(this.getSysConst("ConfigDir")+"pttb/tracuu/tracuu_gdvp/tc_data_ajax_pol_api.htm"));
			JSONParser jsonParser = new JSONParser();	            
			JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonString);	                           
		    String error_code = (String) jsonObject.get("error_code");
			if(error_code.equals("0")) {
				JSONArray jsonArray = (JSONArray) jsonObject.get("result");
				int stt;
				int i;
				for (i = 0; i < jsonArray.size(); i++) {
					JSONObject jsonObject_ = (JSONObject) jsonArray.get(i);				
					stt				= i+1;
					String FAMILY_ID 			= getString(jsonObject_.get("FAMILY_ID"));
					String MEMBER		= getString(jsonObject_.get("MEMBER"));
					String SUBPOOL_NAME		= getString(jsonObject_.get("SUBPOOL_NAME"));
					String UPDATED_TIME		= getString(jsonObject_.get("UPDATED_TIME"));	
					String SERVICE_CODE			= getString(jsonObject_.get("SERVICE_CODE"));	
											
					ret.append(tpl.replace("{f01FAMILY_ID}",FAMILY_ID+"").replace("{f01MEMBER}",MEMBER).replace("{f01SUBPOOL_NAME}",SUBPOOL_NAME).replace("{f01UPDATED_TIME}",UPDATED_TIME).replace("{f01SERVICE_CODE}",SERVICE_CODE));			
				}
			}
			else {
				ret.append(tpl.replace("{f01FAMILY_ID}","").replace("{f01MEMBER}","").replace("{f01SUBPOOL_NAME}","").replace("{f01UPDATED_TIME}","").replace("{f01SERVICE_CODE}",""));	
			}
				
			getEnvInfo().setUserVar("tracuu_data_api3",ret.toString());
			System.out.println(ret.toString());		
			//return ret.toString();
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		return "/main?" + CesarCode.encode("configFile")+"=pttb/tracuu/tracuu_gdvp/tc_lssd_data";
	}
	
	
	
	public static String getName(String pstrTagName, Element peNode) {
         NodeList nodeList = peNode.getElementsByTagName(pstrTagName);
         Element element1 = (Element) nodeList.item(0);

         if (element1 != null) {
             return element1.getChildNodes().item(0).getNodeValue();
         }
         return null;
     }
	 
	public String doc_layds_vascloud(
		String psMsisdn,
		String psMonth,
		String psYear,
		String psUserId,
		String psIP
	) 
	{
		try{
			boolean blnCheck = SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"doc_layds_vascloud", getUserVar("userID"),
					getUserVar("sys_agentcode"),"10|1|30");
			if(!blnCheck)
			return  "/main?"+CesarCode.encode("configFile")+"=admin/not-access";
			reqValue("begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
						+psMsisdn+"','doc_layds_vascloud'," 
						+"'"+psMsisdn+";',"
						+"'','"
						+getUserVar("userID")+"','"
						+getEnvInfo().getParserParam("sys_userip")+"','"
						+getUserVar("sys_agentcode")+"'); "
						+ "   commit; "
						+ " end; ");
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		
		try{
			String xml = Get_VasCloud.getusehist(psMsisdn, psMonth, psYear, psUserId, psIP);
			StringBuffer ret = new StringBuffer();
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(new InputSource(new StringReader(xml)));			
	        NodeList entrieErrors = document.getElementsByTagName("RPLY");			
			String tpl = FileUtils.readFileToString(new File(this.getSysConst("ConfigDir")+"ccbs_inhoadon/tracuu_vascloud/ajax_danhsach_data.htm"));
			int stt;
	        for (int j = 0; j < entrieErrors.getLength(); j++) 
	        {
	           Element nodeError = (Element) entrieErrors.item(j);	           
	            if (nodeError.getNodeType() == Node.ELEMENT_NODE) {
	                String error = getName("error", nodeError);
	                    if ("0".equals(error)) {
	                        NodeList entries = document.getElementsByTagName("use_hist_item");
	                        for (int i = 0; i < entries.getLength(); i++) 
							{
	                            Element node = (Element) entries.item(i);									                            
	                            if (node.getNodeType() == Node.ELEMENT_NODE) 
								{	                                
									stt					= i+1;
	                                String msisdn 		= getName("msisdn", node);
									String ten_dvhd 	= "VAS CLOUD";
									String service_name = getName("service_name", node);
									String channel 		= getName("channel", node);
									String action_date 	= getName("action_date", node);
									String price 		= getName("price", node);									
									ret.append(tpl.replace("{f01stt}",stt+"").replace("{f01msisdn}",msisdn).replace("{f01ten_dvhd}",ten_dvhd).replace("{f01service_name}",service_name).replace("{f01channel}",channel).replace("{f01action_date}",action_date).replace("{f01price}",price));
	                            }
	                        }   
	                    }                   
	             }
	        }  
			
			getEnvInfo().setUserVar("print_api",ret.toString());
			//System.out.println(xml);
		} catch (Exception e) {
		    e.printStackTrace();
		}
		return "/main?" + CesarCode.encode("configFile")+"=ccbs_inhoadon/tracuu_vascloud/ajax_danhsach";			
	}
	
}