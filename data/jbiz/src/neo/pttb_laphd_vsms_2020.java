package neo;

import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

import java.util.*;
import javax.sql.*;
import javax.sql.*;
import java.util.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.io.*;
import java.util.*;
import java.util.regex.*;


public class pttb_laphd_vsms_2020
extends NEOProcessEx {
 
  
  public String themmoiHDLD_ky(String schema
	, String psMA_HD, String psMA_KH
	,String psMA_CQ,String psCOQUAN,String  psNGUOI_DD
	,String psCHUCDANH,String psDIENTHOAI_LH,String piQUAN_ID
	,String piPHUONG_ID,String piPHO_ID
	,String psSO_NHA,String psDIACHI_CT,String psSO_GT
	,String pdNGAYCAP_GT,String psNOICAP_GT,String piLOAIGT_ID
	,String pdNGAYSINH,String piPHAI,String piDICHVUVT_ID
	,String piKIEU_LD,String psSODAIDIEN,String piLOAIKH_ID
	,String piKHLON_ID,String piNGANHNGHE_ID,String piUUTIEN_ID
	,String piDANGKY_DB,String piDANGKY_TV,String piDIADIEMTT_ID
	,String psMA_BC,String psTEN_TT, String psDIACHI_TT,String piQUANTT_ID
	,String piPHUONGTT_ID,String piPHOTT_ID,String psSOTT_NHA
	,String psMS_THUE,String piNGANHANG_ID
	,String psTAIKHOAN,String pdNGAY_LAPHD,String psGHICHU
	,String psnguoi_cn,String psmay_cn, String piDONVIQL_ID
	,String psMA_NV, String piYEUCAUKS_ID,String psEMAIL,String piKHRR
	,String psso_gt1  ,String psngaycap_gt1 ,String psnoicap_gt1  ,int piLOAIGT_ID1,String piCHUYENKHOAN_ID 
	,String psKyTen , String psNguoiGT, String psNguoiKHD, String psMaT, String psTTTuyen, String psMa_NS, String piMucPhi
	) {
    String s="begin ? := admin_v2.pttb_laphd.themmoi_hopdong_ld_ky_2020('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+psMA_KH+"'"
        +",'"+psMA_CQ+"'"
        +",'"+psCOQUAN+"'"
        +",'"+psNGUOI_DD+"'"
        +",'"+psCHUCDANH+"'"
        +",'"+psDIENTHOAI_LH+"'"
        +","+piQUAN_ID
        +","+piPHUONG_ID
        +","+piPHO_ID
        +",'"+psSO_NHA+"'"
        +",'"+psDIACHI_CT+"'"
		+",'"+psSO_GT+"'"
		+",'"+pdNGAYCAP_GT+"'"
		+",'"+psNOICAP_GT+"'"
		+","+piLOAIGT_ID
		+","+pdNGAYSINH
		+","+piPHAI
		
        +","+piDICHVUVT_ID
		+","+piKIEU_LD
		
        +",'"+psSODAIDIEN+"'"
        +","+piLOAIKH_ID
		+","+piKHLON_ID
		+","+piNGANHNGHE_ID
		+","+piUUTIEN_ID
		+","+piDANGKY_DB
		+","+piDANGKY_TV
		+","+piDIADIEMTT_ID
		
		+",'"+psMA_BC+"'"
		+",'"+psTEN_TT+"'"
		+",'"+psDIACHI_TT+"'"
		
        +",'"+piQUANTT_ID+"'"
        +",'"+piPHUONGTT_ID+"'"
        +",'"+piPHOTT_ID+"'"
        +",'"+psSOTT_NHA+"'"
        +",'"+psMS_THUE+"'"
        
        +","+piNGANHANG_ID
        +",'"+psTAIKHOAN+"'"
		+","+pdNGAY_LAPHD
        +","+psGHICHU
        
        +",'"+psnguoi_cn+"'"
        +",'"+psmay_cn+"'"
		+",'"+piDONVIQL_ID+"'"
		+",'"+psMA_NV+"'"
		+","+piYEUCAUKS_ID
		+",'"+psEMAIL+"'"
		+","+piKHRR
		+",'"+psso_gt1+"'"
		+",'"+psngaycap_gt1+"'"
		+",'"+psnoicap_gt1+"'"
		+","+piLOAIGT_ID1
		+","+piCHUYENKHOAN_ID
		+",'"+psKyTen+"'"
		+",'"+psNguoiGT+"'"
		+",'"+psNguoiKHD+"'"
		+",'"+psMaT+"'"
		+",'"+psTTTuyen+"'"
		+",'"+psMa_NS+"'"		
		+",'"+piMucPhi+"'"
        +");"
        +"end;"
        ;
	System.out.println(s);
    return s;
  }
public String capnhatHDLD_ky(String schema
	,String psMA_HD                         
	,String psMA_KH                         
	,String psTEN_TT                        
	,String psNGUOI_DD                      
	,String psDIACHI_CT                     
	,String psDIACHI_TT                     
	,String psSO_GT                           
	,String psCOQUAN                        
	,String psDIENTHOAI_LH                  
	,String psMS_THUE                       
	,String psTAIKHOAN                      
	,String pdNGAY_LAPHD                    
	,String psGHICHU                        
	,String piDIADIEMTT_ID                  
	,String piDICHVUVT_ID                   
	,String piNGANHANG_ID                   
	,String piTONGTIEN_HD                   
	,String piLOAIKH_ID                     
	,String piQUAN_ID                       
	,String piPHUONG_ID                     
	,String piPHO_ID                        
	,String psNGUOI_CN                      
	,String piLOAIGT_ID                        
	,String piKIEU_LD                       
	,String psSO_NHA                        
	,String psNGAYCAP_GT                   
	,String psNOICAP_GT                    
	,String pdNGAYSINH                
	,String piPHAI                 
	,String piMA_CQ                         
	,String psMA_BC                         
	,String piKHLON_ID                      
	,String psMAY_CN                        
	
	,String piNGANHNGHE_ID                  
	,String piUUTIEN_ID                     
	,String piDANGKY_TV                     
	,String piDANGKY_DB                     
	,String psCHUCDANH                      
	,String psSODAIDIEN                     
	,String piQUANTT_ID                     
	,String piPHUONGTT_ID                   
	,String piPHOTT_ID                      
	,String psSOTT_NHA   
	,String piDONVIQL_ID   	
	,String psMA_NV
	,String psEMAIL
	,String piKH_RR 
	,String psso_gt1  ,String psngaycap_gt1 ,String psnoicap_gt1  ,int piLOAIGT_ID1,String piCHUYENKHOAN_ID      
	,String psKyTen , String psNguoiGT, String psNguoiKHD, String psMa_T, String psTTTuyen,String psMa_ns, String piMucPhi
	) {


    String s=    "begin ? := admin_v2.pttb_laphd.capnhat_hopdong_ld_ky_2020('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"',"                         
		+"'"+psMA_KH+"',"                         
		+"'"+psTEN_TT+"',"                        
		+"'"+psNGUOI_DD+"',"                      
		+"'"+psDIACHI_CT+"',"                     
		+"'"+psDIACHI_TT+"',"                     
		+"'"+psSO_GT+"',"                           
		+"'"+psCOQUAN+"',"                        
		+"'"+psDIENTHOAI_LH+"',"                  
		+"'"+psMS_THUE+"',"                       
		+"'"+psTAIKHOAN+"',"                      
		+pdNGAY_LAPHD+","                         
		+psGHICHU+","                        
		+piDIADIEMTT_ID+","                       
		+piDICHVUVT_ID+","                        
		+piNGANHANG_ID+","                        
		+piTONGTIEN_HD+","                        
		+piLOAIKH_ID+","                          
		+piQUAN_ID+","                            
		+piPHUONG_ID+","                          
		+piPHO_ID+","                             
		+"'"+psNGUOI_CN+"',"                      
		+piLOAIGT_ID+","                        
		+piKIEU_LD+","                            
		+"'"+psSO_NHA+"',"                        
		+"'"+psNGAYCAP_GT+"',"                   
		+"'"+psNOICAP_GT+"',"                    
		+pdNGAYSINH+","                
		+piPHAI+","                 
		+piMA_CQ+","                              
		+"'"+psMA_BC+"',"                         
		+piKHLON_ID+","                           
		+"'"+psMAY_CN+"',"                        
		+piNGANHNGHE_ID+","                       
		+piUUTIEN_ID+","                          
		+piDANGKY_TV+","                          
		+piDANGKY_DB+","                          
		+"'"+psCHUCDANH+"',"                      
		+"'"+psSODAIDIEN+"',"                     
		+"'"+piQUANTT_ID+"',"                          
		+"'"+piPHUONGTT_ID+"',"                        
		+"'"+piPHOTT_ID+"',"                           
		+"'"+psSOTT_NHA+"',"                      
		+piDONVIQL_ID
		+",'"+psMA_NV+"'"
		+",'"+psEMAIL+"'"
		+","+piKH_RR 
		+",'"+psso_gt1+"'"
		+",'"+psngaycap_gt1+"'"
		+",'"+psnoicap_gt1+"'"
		+","+piLOAIGT_ID1
		+","+piCHUYENKHOAN_ID
		+",'"+psKyTen+"'"
		+",'"+psNguoiGT+"'"
		+",'"+psNguoiKHD+"'"
		+",'"+psMa_T+"'"
		+",'"+psTTTuyen+"'"
		+",'"+psMa_ns+"'"	
		+",'"+piMucPhi+"'"		
	+");"
        +"end;"
        ;
    //System.out.println(s);
    return s;

  }  
  
  	public String laytt_mucphi(
		String ma_hd
		) throws Exception {
			String s ="begin ? :=admin_v2.pkg_thuphi_tainha.get_pttb_thuphis('"+ma_hd+"'); end;";
		System.out.println(s);
		return s; 
	}
	
}
