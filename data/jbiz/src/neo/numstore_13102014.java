package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class numstore extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("NumStore");		
	}
	
	public String doc_layds_thuebao(
		String psSoTB,
		String psKieuso_ID,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=dangky/ajax_ds_thuebao"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("kieuso_id") + "=" + psKieuso_ID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	
	public String doc_layth_thuebao(
		String psSoTB,
		String psKieuso_ID,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=dangky/ajax_th_thuebao"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("kieuso_id") + "=" + psKieuso_ID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	
	public String doc_validate(String psCode)
	{
		return "/jcaptchaValid?j_captcha_response="+psCode;
	}
	
	public String value_dangky(
		String psSoTB,
		String psTen,
		String psDiachi,
		String psDienthoai,
		String psCMT,
		String psNgaycap,
		String psNoicap,
		String psEmail,
		String psCode,
		String psUserIP
	){
		
	 	return  "begin ?:="+getSysConst("FuncSchema")+"store.dangky('"
	 				+psSoTB+"','"
	 				+psTen+"','"
	 				+psDiachi+"','"
	 				+psDienthoai+"','"
	 				+psCMT+"','"
	 				+psNgaycap+"','"
	 				+psNoicap+"','"
	 				+psEmail+"','"
	 				+psCode+"','"
	 				+getUserVar("sys_dataschema")+"','"
	 				+psUserIP+"'); end;";
	}
	public String doc_layds_daiso(
		String psSoTB,
		String psDaiso_ID,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/numstore/dangky_daiso/ajax_ds_daiso"
  			+ "&" + CesarCode.encode("soluong_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("daiso_id") + "=" + psDaiso_ID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String doc_layth_daiso(
		String psSoTB,
		String psDaiso_ID,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/numstore/dangky_daiso/ajax_th_daiso"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("daiso_id") + "=" + psDaiso_ID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	
	public NEOCommonData dangky_daiso(
		String psIDDaiSo,
		String psDaiSoTB,
		String psSoluongTB,
		String psTen,
		String psDiachi,
		String psTenVietTat,
		String psTenDangKy,
		String psDienthoai,
		String psMaSoThue,
		String psWebsite,
		String psEmail,
		String psCode,
		String psSchema,
		String psUserID,
		String psUserIP
	){
		String s = "begin ?:= dms_public.store.dangky_daiso_admin('"
	 				+psDaiSoTB+"','"
					+psSoluongTB+"','"
					+psIDDaiSo+"','"
	 				+psTen+"','"
	 				+psDiachi+"','"
					+psTenVietTat+"','"
					+psTenDangKy+"','"
	 				+psDienthoai+"','"
	 				+psMaSoThue+"','"
	 				+psWebsite+"','"
	 				+psEmail+"','"
	 				+psCode+"','"
	 				+psSchema+"','"
					+psUserID+"','"
	 				+psUserIP+"'); end;";	
		NEOExecInfo nei_ = new NEOExecInfo(s);
		nei_.setDataSrc("NUMSTOREDBMS");
		return new NEOCommonData(nei_);
	}
	public String doc_layds_chitiet_daiso(
		String psKhodaisoID,
		String psPageNum,
		String psPageRec,
		String pssoluong
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/numstore/chitiet_daiso/ajax_ds_chitiet_daiso"
  			+ "&" + CesarCode.encode("khodaiso_id") + "=" + psKhodaisoID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec
			+ "&" + CesarCode.encode("soluong") + "=" + pssoluong;
	}
	public String doc_layth_chitiet_daiso(
		String psKhodaisoID,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/numstore/chitiet_daiso/ajax_th_chitiet_daiso"
  			+ "&" + CesarCode.encode("khodaiso_id") + "=" + psKhodaisoID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
}