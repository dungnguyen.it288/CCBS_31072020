
package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;



public class khuyenmai extends NEOProcessEx {
  public void run() {
    System.out.println("neo.khuyenmai was called");
  }

  
 
  public NEOCommonData addNewHinhThuc(
  							String  schema,    																	  						   			        
					       	String  ten_khuyenmai,
							String  pdNgay_BatDau,
							String  pdNgay_KT,
							String  piHinhThucID,
							String  piSoThang,
							String  chk_camKetSd,
							String  chkhl_sauthanghm,
							String  piTienCTP,
							String  pdNgay_KH,
							String  piTienKPL,
							String  pdNgay_Khoa2Chieu,		
							String  piTienHM,
							String  pskhoanmuctt_id,
							String  doituong_id,
							String  psNoiDungKM,
							String  psnguoi_cn,
  							String  psmay_cn
							
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schema)+"dulieu_qltn.addNewHinhThuc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+CesarCode.decode(schema)+"','"+ten_khuyenmai+"'"                      
        +","+pdNgay_BatDau
        +","+pdNgay_KT
        +",'"+piHinhThucID+"'"
        +","+piSoThang
		+","+chk_camKetSd
		+","+chkhl_sauthanghm
		+","+piTienCTP
		+","+pdNgay_KH
		+","+piTienKPL
		+","+pdNgay_Khoa2Chieu
		+","+piTienHM
		+",'"+pskhoanmuctt_id+"'"
		+",'"+doituong_id+"'"
		+","+psNoiDungKM
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    System.out.println(s); 
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);
  }   
  
  
 public String layds_hinhthuc_kmid_tinhs(
 										 String func_schema,
 										 String schemaCommon,
 										 String khuyenmai_id) 
 										{
   String s= "begin ?:="+func_schema+"khuyenmai.layds_hinhthuc_kmid_tinhs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+khuyenmai_id
		+");end;";
	
	System.out.println(s); 	
	return s;
  }


	public String lay_ngay_apdung(          String func_schema,
											String schemaCommon,
											String khuyenmai_id)
	{
		String s = "begin ?:=" + func_schema + "khuyenmai.lay_ngay_apdung('" + getUserVar("userID")
			 + "','" + getUserVar("sys_agentcode")
			 + "','" + schemaCommon
			 + "','" + getUserVar("sys_dataschema")
			 + "'," + khuyenmai_id
			 + ");end;";

		System.out.println(s);
		return s;
	}
	
public String lay_tt_khuyenmai(          String func_schema,
											String schemaCommon,
											String khuyenmai_id)
	{
		String s = "begin ?:=" + func_schema + "khuyenmai.lay_tt_khuyenmai('" + getUserVar("userID")
			 + "','" + getUserVar("sys_agentcode")
			 + "','" + schemaCommon
			 + "','" + getUserVar("sys_dataschema")
			 + "'," + khuyenmai_id
			 + ");end;";

		System.out.println(s);
		return s;
	}



public String lay_matb_khuyenmais ( String func_schema,
 										 String schemaCommon,
 										 String ma_tb,
 										 String khuyenmai_id) 
 										{
   String s= "begin ?:="+func_schema+"khuyenmai.lay_matb_khuyenmais('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"','"+ma_tb+"'"
		+","+khuyenmai_id
		+");end;";
	
	System.out.println(s); 	
	return s;
  }
  
  
//laytt_thuebao_theo_somay('{p0sys_funcschema}','{p0sys_userid}','{p0sys_agentcode}','{p0sys_dataschema}','"+ma_tb+"','"+piKhuyenMai_ID+"')";	


public String laytt_thuebao_theo_somay ( String func_schema,
 										 String schemaCommon,
 										 String ma_tb,
 										 String khuyenmai_id) 
 										{
   String s= "begin ?:="+func_schema+"khuyenmai.laytt_thuebao_theo_somay('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"','"+ma_tb+"'"
		+","+khuyenmai_id
		+");end;";
	
	System.out.println(s); 	
	return s;
  }
  
  
    
   public String layds_hinhthuc_km_tinhs()
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=khuyenmai/km_hinhthucs_ajax";	
		url_ = url_ + "&sid="+Math.random();	    
		return url_;
  	}
  
  public String themmoi_hinhthuc_km_tinhs(
  							String  schema,
  							String  schemaCommon,   																	  						   			        
					       	String  ten_khuyenmai,
							String  pdNgay_BatDau,
							String  pdNgay_KT,
							String  piHinhThucID,
							String  piLoaiKM_ID,
							String  piSoThang,
							String  pikieu_apdung_id,
							String  chk_camKetSd,
							String  chkhl_sauthanghm,
							String  piTienCTP,
							String  pdNgay_KH,
							String  piTienKPL,
							String  pdNgay_Khoa2Chieu,		
							String  piTienHM,
							String  pskhoanmuctt_id,
							String  doituong_id,
							String  psNoiDungKM,
							String  chktienkm_duochuong,
							String  chktinh_taods,
							String  chk_hienthi,
							String  psnguoi_cn,
  							String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schema)+"khuyenmai.themmoi_hinhthuc_km_tinhs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"','"+ten_khuyenmai+"'"                      
        +","+pdNgay_BatDau
        +","+pdNgay_KT
        +",'"+piHinhThucID+"'"
        +",'"+piLoaiKM_ID+"'"
        +","+piSoThang
        +",'"+pikieu_apdung_id+"'"
		+","+chk_camKetSd
		+","+chkhl_sauthanghm
		+","+piTienCTP
		+","+pdNgay_KH
		+","+piTienKPL
		+","+pdNgay_Khoa2Chieu
		+","+piTienHM
		+",'"+pskhoanmuctt_id+"'"
		+",'"+doituong_id+"'"
		+","+psNoiDungKM
		+","+chktienkm_duochuong
		+","+chktinh_taods
		+","+chk_hienthi
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
    System.out.println(s);    
    return s;
  }   
  
  
  public String capnhat_hinhthuc_km_tinhs(
  							String  schema,
  							String  schemaCommon,
  							String  piKhuyenMaiID,   																	  						   			        
					       	String  ten_khuyenmai,
							String  pdNgay_BatDau,
							
							String  pdNgay_KT,
							String  piHinhThucID,
						    String  piLoaiKM_ID,
							String  piSoThang,
							String  pikieu_apdung_id,
						    String  chk_camKetSd,
							
							String  chkhl_sauthanghm,
							String  piTienCTP,
							String  pdNgay_KH,
							String  piTienKPL,
							String  pdNgay_Khoa2Chieu,	
								
							String  piTienHM,
							String  pskhoanmuctt_id,
							String  doituong_id,
							String  psNoiDungKM,
							String  chktienkm_duochuong,
														
							String  chktinh_taods,
							String  chkcon_hieuluc,
							String  psnguoi_cn,
  							String  psmay_cn			
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schema)+"khuyenmai.capnhat_hinhthuc_km_tinhs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+piKhuyenMaiID+",'"+ten_khuyenmai+"'"                      
        +","+pdNgay_BatDau
        +","+pdNgay_KT
        +",'"+piHinhThucID+"'"  
        +",'"+piLoaiKM_ID+"'"       
        +","+piSoThang
        +",'"+pikieu_apdung_id+"'"
		+","+chk_camKetSd
		+","+chkhl_sauthanghm
		+","+piTienCTP
		+","+pdNgay_KH
		+","+piTienKPL
		+","+pdNgay_Khoa2Chieu
		+","+piTienHM
		+",'"+pskhoanmuctt_id+"'"
		+",'"+doituong_id+"'"
		+","+psNoiDungKM
		+","+chktienkm_duochuong
		+","+chktinh_taods
		+","+chkcon_hieuluc
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    System.out.println(s);    
    return s;
  }   
  
 /*Nhu thanh the:
  *chuc nang: lay thong tin thue bao khuyen mai.
  *phuc vu nut tim kiem
  *ngay_pt: 09/10/2008. 
  						
  */
    public String layds_tb_khuyenmais(String piPAGEID,
    							   String piREC_PER_PAGE,    							
    							   String ma_tb,
    							   String ngay_apdung, 
    							   String ngay_kt,   							  
    							   String khuyenmai_id,
    							   String trangthai_id) {
  String  s= "/main?"+CesarCode.encode("configFile")+"=khuyenmai/xuly_danhsach_kms/ajax_layds_tb_khuyenmais"
		+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
		+ "&" +CesarCode.encode("ma_tb")+"="+ma_tb
		+ "&" +CesarCode.encode("ngay_apdung")+"="+ngay_apdung
		+ "&" +CesarCode.encode("ngay_kt")+"="+ngay_kt
		+ "&" +CesarCode.encode("khuyenmai_id")+"="+khuyenmai_id
		+ "&" +CesarCode.encode("trangthai_id")+"="+trangthai_id
        ;   
    System.out.println(s);
   return s;    
     
  }
  
 public String themmoi_ds_khuyenmais(	 String psschema,
  										 String psschemaCommon,   
  										 String psma_tb_1   										 
										,String pdngay_ld
										,String pikhuyenmai_id_1
										,String pitien_km
										,String chkp_to_ttruoc
										,String chkkhoa_icoc
										,String psghichu
										,String psnguoi_cn
										,String psmay_cn
									
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschema)+"khuyenmai.themmoi_ds_khuyenmais('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
        +",'"+psma_tb_1+"'"
        +","+pdngay_ld
        +",'"+pikhuyenmai_id_1+"'"
        +","+pitien_km
		+","+chkp_to_ttruoc
		+","+chkkhoa_icoc
		+","+psghichu
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
    System.out.println(s);    
    return s;
  	}
 
 public String capnhat_ds_khuyenmais(
     String psschema
	,String psschemaCommon  
	,String psdsSomay   									
	,String psdsKMID
	,String psdsApdung
	,String psghichu
	,String psnguoi_cn
	,String psmay_cn
									
 ){
  			
    String s=    "begin ? := "+CesarCode.decode(psschema)+"khuyenmai.capnhat_ds_khuyenmais('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)
        +"','"+psdsKMID
        +"','"+psdsApdung
        +"','"+psdsSomay       
		+"','"+psghichu
		+"','"+psnguoi_cn
		+"','"+psmay_cn
        +"');"
        +"end;";
        
    System.out.println(s);    
    return s;     
  }   
  
  
   public String xoa_ds_khuyenmais( String psschema,String psma_tb,String pikhuyenmai_id)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschema)+"khuyenmai.xoa_ds_khuyenmais('"+getUserVar("userID")
		+"','"+getUserVar("sys_dataschema")+"'"
        +",'"+psma_tb+"'"
        +","+pikhuyenmai_id      
        +");"
        +"end;"
        ;        
    System.out.println(s);    
    return s;     
  }   
  
  public String themmoi_tb_khuyenmais(	 String psschema,
  										 String psschemaCommon,   
  										 String psma_tb_1   										 
										,String pdngay_ld
										,String pikhuyenmai_id_1
										,String pitien_km
										,String chkp_to_ttruoc
										,String chkkhoa_icoc
										,String psghichu
										,String psnguoi_cn
										,String psmay_cn
									
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschema)+"khuyenmai.themmoi_tb_khuyenmais('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
        +",'"+psma_tb_1+"'"
        +","+pdngay_ld
        +",'"+pikhuyenmai_id_1+"'"
        +","+pitien_km
		+","+chkp_to_ttruoc
		+","+chkkhoa_icoc
		+","+psghichu
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
    System.out.println(s);    
    return s;
  	}
  
   public String themmoi_tb_khuyenmai_dacbiet(	 String psschema,
  										 String psschemaCommon,   
  										 String psma_tb_1
										,String pikhuyenmai_id_1
										,String pitien_km										
										,String psghichu
										,String pdthang_bd
										,String pdthang_kt
										,String psnguoi_cn
										,String psmay_cn
									
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschema)+"khuyenmai.themmoi_tb_khuyenmai_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
        +",'"+psma_tb_1+"'"
        +",'"+pikhuyenmai_id_1+"'"
        +","+pitien_km	
		+","+psghichu
	    +","+pdthang_bd
		+","+pdthang_kt
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
    System.out.println(s);    
    return s;
  	}
  
  public String capnhat_tb_khuyenmais(
  								 String psschema,
  										 String psschemaCommon,   
  										 String psma_tb  										 
										,String pdngay_ld
										,String pikhuyenmai_id
										,String pitien_km
										,String chkp_to_ttruoc
										,String chkkhoa_icoc
										,String psghichu
										,String psnguoi_cn
										,String psmay_cn
									
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschema)+"khuyenmai.capnhat_tb_khuyenmais('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
        +",'"+psma_tb+"'"
        +","+pdngay_ld
        +",'"+pikhuyenmai_id+"'"
        +","+pitien_km
		+","+chkp_to_ttruoc
		+","+chkkhoa_icoc
		+","+psghichu	
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
        
    System.out.println(s);    
    return s;     
  }   
  
  public String capnhat_tb_khuyenmai_dacbiet(
  								 String psschema,
  										 String psschemaCommon,   
  										 String psma_tb  										 
										,String pikhuyenmai_id
										,String pitien_km									
										,String psghichu
										,String pdthang_bd
										,String pdthang_kt
										,String psnguoi_cn
										,String psmay_cn
									
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschema)+"khuyenmai.capnhat_tb_khuyenmai_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
        +",'"+psma_tb+"'"
        +",'"+pikhuyenmai_id+"'"
        +","+pitien_km	
		+","+psghichu
	   +","+pdthang_bd
		+","+pdthang_kt
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
        
    System.out.println(s);    
    return s;     
  }   
  
   public String xoa_tb_khuyenmai_dacbiet( String psschema,String psma_tb,String pikhuyenmai_id){  			
    String s=    "begin ? := "+CesarCode.decode(psschema)+"khuyenmai.xoa_tb_khuyenmai_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_dataschema")+"'"
        +",'"+psma_tb+"'"
        +","+pikhuyenmai_id      
        +");"
        +"end;"
        ;        
    System.out.println(s);    
    return s;     
  }

	public String ds_nhanvien(int donviql_id){
		return "/main?" + CesarCode.encode("configFile") + "=khuyenmai/baocao/danhsach_tien_kms/ds_nhanviens&" + CesarCode.encode("donviql_id") + "=" + donviql_id;
	}
	
	public String ds_nhanvien_inphieu(int donviql_id){
		return "/main?" + CesarCode.encode("configFile") + "=khuyenmai/baocao/in_hoadon_kms/ds_nhanviens&" + CesarCode.encode("donviql_id") + "=" + donviql_id;
	}
  
/*
  public NEOCommonData layds_tinhs_(String psobj){
		String s="/main?"+ CesarCode.encode("configFile")+ "=ccbs_kmck/km_baocaos/Report_doanhthu_km/ajax_tinh_2column"
			+"&"+CesarCode.encode("psMaTinh")+"="+psobj;		
		System.out.println(s); 	
		NEOExecInfo nei_ = new NEOExecInfo(s);
		nei_.setDataSrc("VNPBilling");
		return new NEOCommonData(nei_);		
	}  
  */
    
public NEOCommonData layds_tinhs_(String psobj, String khuvuc){
	String s="/main?"+ CesarCode.encode("configFile")+ "=ccbs_kmck/km_baocaos/Report_km/ajax_tinh_2column"
					 +"&"+CesarCode.encode("psMaTinh")+"="+psobj
					 +"&"+CesarCode.encode("psKhuvuc")+"="+khuvuc;		
	System.out.println(s); 	
	NEOExecInfo nei_ = new NEOExecInfo(s);
	nei_.setDataSrc("VNPBilling");
	return new NEOCommonData(nei_);		
}  
}