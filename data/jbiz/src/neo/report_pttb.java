package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class report_pttb
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb.goi_truyen_solieu was called");
  }

  public String th_dl_pttb_thang(
		String tungay,
        String th_lai		
	 ) throws Exception {	
		
		String s=    "begin ? := admin_v2.pttb_baocao.tonghop_pttb_thang("
			+"'" + tungay +"',"
			+"'" + th_lai+"'"			
			+"); end;";
		
		System.out.println("NEO PORTAL "+s);
		return this.reqValue("", s);
  	}
   public String th_alo_dn_thang(
		String tungay,
        String th_lai		
	 ) throws Exception {	
		
		String s=    "begin ? := admin_v2.pttb_baocao.tonghop_alodn_thang("
			+"'" + tungay +"',"
			+"'" + th_lai+"'"			
			+"); end;";
		
		System.out.println("NEO PORTAL "+s);
		return this.reqValue("", s);
  	}
  public String th_thongke_alo_thang(
		String tungay,
        String th_lai		
	 ) throws Exception {	
		
		String s=    "begin ? := admin_v2.pttb_baocao.baocao_tao_thongke_alo("
			+"'" + tungay +"',"
			+"'" + th_lai+"'"			
			+"); end;";
		
		System.out.println("NEO PORTAL "+s);
		return this.reqValue("", s);
  	}
  public String tao_dl_alo(
		String user,
		String agent,
        String thang		
	 ) throws Exception {	
		
		String s=    "begin ? := admin_v2.pkg_regis_data.baocao_tao_thuebao_goi("
			+"'" + agent +"',"
			+"'" + thang +"',"
			+"'" + user+"'"			
			+"); end;";
		
		System.out.println("NEO PORTAL "+s);
		return this.reqValue("", s);
  	}
  
}
