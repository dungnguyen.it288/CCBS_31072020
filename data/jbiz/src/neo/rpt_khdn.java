package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import com.svcon.jdbf.*;
import javax.sql.RowSet;
import java.sql.*;
import java.util.zip.*;
import java.io.*;
import java.net.*;
import org.apache.commons.net.*;
import neo.qltn.ftp;
import neo.qltn.FTPupload;
import sun.misc.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.*;
import java.text.SimpleDateFormat;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

public class rpt_khdn extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("-------------------------");
		//funcschema=getSysConst("FuncSchema");
    	//dataschema=getUserVar("sys_dataschema");
    	//userid=getUserVar("userID");		
	}
	//HashMap<String, Object> parameters, String jasperFile, String reportName
	
	public String uploadFTP(String id, String chuky) {
		System.out.println("----------------------STARTING PUT FILE TO FTP SERVER----------------------------");
		System.out.println("----------------------PARAMS: " + id + "|" + chuky);
		String realPath = this.getEnvInfo().sysConst("ConfigDir");
		System.out.println("-----------------------------Path " + realPath);
		String rs = "Hoan thanh";
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		OutputStream os = null;
		Connection connection = null;
		try {
			String ftp_="", user_="", pass_="",jasperFile="",ftp_folder="",save_name = "",save_type = "";
			RowSet rs_ = this.reqRSet("SELECT save_name,save_type,thumuc_ftp,ftp,js_file,ftp_user,ftp_pass FROM admin_v2.rptdn_export_config WHERE id="+id);
			
	  		while (rs_.next()){
	  			ftp_ = rs_.getString("ftp");
				user_ = rs_.getString("ftp_user");
	  			pass_ = rs_.getString("ftp_pass");
				jasperFile = rs_.getString("js_file");
				ftp_folder = rs_.getString("thumuc_ftp");
				save_name = rs_.getString("save_name");
				save_type = rs_.getString("save_type");
	  		}
			System.out.println("---------------------RPT_KHDN_CONFIGURE: " + ftp_ + "|" + user_+ "|" +pass_+ "|" +jasperFile+ "|" +ftp_folder+ "|" +save_name+ "|" +save_type);
			jasperFile = realPath + "report/rpt_dn/jaspers/" + jasperFile;
			
			HashMap parameters = new HashMap();
			parameters.put(new String("chuky"), chuky);
			System.out.println("------------------Starting export to excel...");
			connection = getConnection("VNPReport");
			
			JasperCompileManager.compileReportToFile(jasperFile);
			JasperPrint print = JasperFillManager.fillReport(jasperFile.substring(0, jasperFile.lastIndexOf(".")) + ".jasper", parameters, connection);
			
			//Cau hinh parameters
			JRXlsExporter exporterXLS = new JRXlsExporter();
			exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
			exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, bos);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporterXLS.exportReport(); 
			
			// Ghi file
			
			String fileName = chuky;
			realPath += "upload/export_khdn/" + save_name+fileName;
			os = new FileOutputStream(realPath + save_type);
			bos.writeTo(os);
			System.out.println("------------------Done!!");
			
			try{
				connection.close();
				System.out.println("------------------Close connection----------------------");
			} catch(SQLException e){
				e.printStackTrace();
			}
			
			BASE64Decoder decoder = new BASE64Decoder(); 
			byte[] decodedBytes = decoder.decodeBuffer(pass_);
			String pass_d = new String(decodedBytes);
	  		doZip( realPath+save_type , realPath+".zip" );	  	
			System.out.println("--------------------------------PUT FILE TO FTP SRVER...");
			
	  		new FTPupload(ftp_, user_, pass_, realPath+".zip" , ftp_folder + save_name+fileName + ".zip" );
			
			System.out.println("--------------------------------DONE!!--------------------------------------");
			
		} catch (Exception e) {
			e.printStackTrace();
			rs = e.getMessage();
		} finally {
			// it's your responsibility to close the connection, don't forget
			// it!
			if(os != null) {
				try {
					os.close();
				} catch (IOException e) {
				}
			}
			if(bos != null) {
				try {
					bos.close();
				} catch (IOException e) {
				}
			}
		}
		return "begin ? :='"+rs.replace("'","''")+"'; end;";
	}
	
	public void doZip(String filename,String zipfilename) throws IOException {
    	
        try {
            FileInputStream fis = new FileInputStream(filename);
            File file=new File(filename);
            byte[] buf = new byte[fis.available()];
            fis.read(buf,0,buf.length);
            
            CRC32 crc = new CRC32();
            ZipOutputStream s = new ZipOutputStream(
                    (OutputStream)new FileOutputStream(zipfilename));
            
            s.setLevel(6);
            
            ZipEntry entry = new ZipEntry(file.getName());
            entry.setSize((long)buf.length);
            crc.reset();
            crc.update(buf);
            entry.setCrc( crc.getValue());
            s.putNextEntry(entry);
            s.write(buf, 0, buf.length);
            s.finish();
            s.close();
            fis.close();
            //file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
}