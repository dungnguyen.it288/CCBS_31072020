package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

//Import cho phan OCS
import neo.qlsp.Get_PPGW;

public class pttb_tthd extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb_tthd was called");
  }

   /*public String guiCMDVHDLD(		  
									  String psDataSchema
									 ,String psFuncSchema
									 ,String psUserid
                                     ,String psAgentCode									 
									 ,String psDSTBID
									 ,String psDSTB
                                     ,String psDSTBNhanhTK
									 ) {


    String s=    "begin ? := admin_v2.pttb_hmm.gui_cmdv_hdld('"
		+	   psUserid
		+"','"+psAgentCode
		+"','"+psDataSchema		
		+"','"+psDSTBID
		+"','"+psDSTB
		+"','"+psDSTBNhanhTK
        +"');"
        +"end;"
        ;
	return s;

  }   
  */
  public NEOCommonData guiCMDVHDLD(    
           String psDataSchema
          ,String psFuncSchema
          ,String psUserid
                                     ,String psAgentCode          
          ,String psDSTBID
          ,String psDSTB
                                     ,String psDSTBNhanhTK
          ,String psMaHD
          ) {
 String rq_=null;
 String s = null;
try 
   {
    s=    "begin ? := admin_v2.pkg_api_interface.gui_cmdv_hdld('"
  +    psUserid
  +"','"+psAgentCode
  +"','"+psDataSchema  
  +"','"+psDSTBID
  +"','"+psDSTB
  +"','"+psDSTBNhanhTK
  +"','"+psMaHD
        +"');"
        +"end;"
        ;
 rq_=this.reqValue("",s); 
 }
 catch (Exception ex) 
     {             
      //return ex.getMessage();
      return new NEOCommonData(ex.getMessage());
     } 
 /*String s2="";
 String rq1_=null;
 System.out.println("ket qua gui cat mo"+rq_);
  if(rq_.equals("0"))
    {
      
     try
     {      
      // s2 = "begin ?:= admin_v2.pttb_tracuu.create_data_rigs_package("+
       s2 = "begin ?:= admin_v2.pkg_api_interface.create_data_rigs_package("+
       "'"+psDataSchema+"',"+
       "'"+psUserid+"',"+
       "'"+psDSTB+"',"+
       "'"+psMaHD+"',"+
       "'"+psAgentCode+"',"+
       "'"+getUserVar("userID")+"'"+
       "); end;";
        System.out.println("tao package_data:"+s2);
       rq1_=this.reqValue("",s2); 
     } 
    catch (Exception ex) 
     {             
      return new NEOCommonData(ex.getMessage());
     }

    }
       */
    
return new NEOCommonData(rq_);

  }
  public String guiCMDVHD_NCDV(		  
									  String psDataSchema
									 ,String psFuncSchema
									 ,String psUserid
                                     ,String psAgentCode									 
									 ,String psDSTBID
									 ,String psDSTB
                                     ,String psDSTBNhanhTK
									 ) {


   // String s=    "begin ? := admin_v2.pttb_hmm.gui_cmdv_hdncdv('"
    String s=    "begin ? := admin_v2.pkg_api_interface.gui_cmdv_hdncdv('"
		+	   getUserVar("userID")
		+"','"+psAgentCode
		+"','"+psDataSchema		
		+"','"+psDSTBID
		+"','"+psDSTB
		+"','"+psDSTBNhanhTK
        +"');"
        +"end;"
        ;
	return s;

  }
  public String layds_httt(String kieugach_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tthd/common/ajax_httt"
        +"&"+CesarCode.encode("kieugach_id")+"="+kieugach_id
        ;
  }
  public String laytt_hdld(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hdld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }
  //Start hiepdh
  public String laytt_hdld1(String schema,String psMaHD,String psMatb, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hdld1('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMaHD+"','"+psMatb+"'); end;";
  }
  //End
  public String laytt_hdcdlh(String schema, String userInput, String userid)
  {
 	return "begin ?:=" + CesarCode.decode(schema) + "pttb_thtoan.laytt_hdcdlh('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + userInput  + "'); end;";
  }
  public String laytt_hdcq(String schema,String userInput, String userid) {
    return "begin ?:=admin_v2.pttb_capnhat.laytt_hdcq('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }

  public String laytt_hdbm(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hdbm('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }
  
  // Lay thong tin hop dong doi sim
  public String laytt_hdds(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hddoisim('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }
  
   public String layds_tbdoisim(String mahd) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tthd/hdds/hddoisim_ajax"
        +"&"+CesarCode.encode("ma_hd")+"="+mahd
        ;
  }

  public String laytt_hddc(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hddc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }
  public String laytt_hddv(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hddv('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }

  public String laytt_hdld_theo_matb(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hdld_theo_matb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }
  public String laytt_hdcdlh_theo_matb(String schema, String userInput, String userid)
  {
	  return "begin ?:=" + CesarCode.decode(schema) + "pttb_thtoan.laytt_hdcdlh_theo_matb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + userInput + "'); end;";
  }
  public String laytt_hdcq_theo_matb(String schema,String userInput, String userid) {
    return "begin ?:=admin_v2.pttb_capnhat.laytt_hdcq_theo_matb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }

  public String laytt_hddv_theo_matb(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hddv_theo_matb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }
  public String laytt_hddc_theo_matb(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_thtoan.laytt_hddc_theo_matb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }

  public String layds_tbld(String mahd) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tthd/hdld/hdld_ajax_thuebao"
        +"&"+CesarCode.encode("ma_hd")+"="+mahd
        ;
  }
  
    public String layds_tbld1(String mahd) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tthd/hdld/hdld_ajax_thuebao1"
        +"&"+CesarCode.encode("ma_hd")+"="+mahd
        ;
  }
  public String layds_tbld1(String mahd, String phieu_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tthd/hdld/hdld_ajax_thuebao_phieu"
        +"&"+CesarCode.encode("ma_hd")+"="+mahd
		+"&"+CesarCode.encode("phieu_id")+"="+phieu_id
        ;
  }
  
  public String layds_tbcq(String mahd) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tthd/hdcq/hdcq_ajax_thuebao"
        +"&"+CesarCode.encode("ma_hd")+"="+mahd
        ;
  }

  public String layds_tbbm(String mahd) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tthd/hdbm/hdbm_ajax_thietbi"
        +"&"+CesarCode.encode("ma_hd")+"="+mahd
        ;
  }

  public String layds_tbdc(String mahd) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tthd/hddc/hddc_ajax_thuebao"
        +"&"+CesarCode.encode("ma_hd")+"="+mahd
        ;
  }

  public String thanhtoanHDLD(String schema
									, String psMA_HD
                                     ,String psDsTHUEBAO_ID
									 ,String psUSERID
									 ,String psUSERIP
                                     ,String piChucnang_gachno
									 ,String pdNGAY_TT
                                     ,String pdNGAY_NH
									 ,String piHTTT_ID
                                     ,String piLOAITIEN_ID
									 ,String piNGANHANG_ID
									 ,String psSOSEC
                                     ,String psCHUNGTU
									 ,String psHOPDONG
									 ,String psnguoinhan
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.gachno_hdld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+psDsTHUEBAO_ID+"'"
        +",'"+psUSERIP+"'"
        +","+piChucnang_gachno
        +","+pdNGAY_TT
        +","+pdNGAY_NH
        +","+piHTTT_ID
        +","+piLOAITIEN_ID
        +","+piNGANHANG_ID
        +",'"+psSOSEC+"'"
        +",'"+psCHUNGTU+"'"
		+",'"+psHOPDONG+"'"
		+",'"+psnguoinhan+"'"
        +");"
        +"end;"
        ;
    return s;

  }
	public String thanhtoanHDCDLH(String schema
									  , String psMA_HD									   
									   , String psUSERID
									   , String psUSERIP
									   , String piChucnang_gachno
									   , String pdNGAY_TT
									   , String pdNGAY_NH
									   , String piHTTT_ID
									   , String piLOAITIEN_ID
									   , String piNGANHANG_ID
									   , String psSOSEC
									   , String psCHUNGTU
									   )
	{


		String s = "begin ? := " + CesarCode.decode(schema) + "pttb_thtoan.gachno_hdcdlh('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + psMA_HD + "'"			
			+ ",'" + psUSERIP + "'"
			+ "," + piChucnang_gachno
			+ "," + pdNGAY_TT
			+ "," + pdNGAY_NH
			+ "," + piHTTT_ID
			+ "," + piLOAITIEN_ID
			+ "," + piNGANHANG_ID
			+ ",'" + psSOSEC + "'"
			+ ",'" + psCHUNGTU + "'"
			+ ");"
			+ "end;"
			;
		return s;

	}
  public String xoaphieuHDLD(String schema
									, String psMA_HD
                                     ,String piPHIEU_ID
									 ,String psKIEUHUY
									 ,String psUSERID
									 ,String psUSERIP
									 ) {


    String s=    "begin ? := admin_v2.pttb_thtoan.xoaphieu_thanhtoan_hdld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+piPHIEU_ID+"'"
		+",'"+psKIEUHUY+"'"
		+",'"+psUSERIP+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  
  public String xoaphieuHDLD(String schema
									, String psMA_HD
                                     ,String piPHIEU_ID
									 ,String psKIEUHUY
									 ,String psUSERID
									 ,String psUSERIP
									 ,String pstype_del
									 ) {


    //String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.xoaphieu_thanhtoan_hdld('"+getUserVar("userID")
	String s=    "begin ? := admin_v2.pttb_thtoan.xoaphieu_thanhtoan_hdld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+piPHIEU_ID+"'"
		+",'"+psKIEUHUY+"'"
		+",'"+psUSERIP+"'"
		+",'"+pstype_del+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String thanhtoanHDDC(String schema
									, String psMA_HD
                                     ,String psDsTHUEBAO_ID
									 ,String psUSERID
									 ,String psUSERIP
                                     ,String piChucnang_gachno
									 ,String pdNGAY_TT
                                     ,String pdNGAY_NH
									 ,String piHTTT_ID
                                     ,String piLOAITIEN_ID
									 ,String piNGANHANG_ID
									 ,String psSOSEC
                                     ,String psCHUNGTU
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.gachno_hddc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+psDsTHUEBAO_ID+"'"
        +",'"+psUSERIP+"'"
        +","+piChucnang_gachno
        +","+pdNGAY_TT
        +","+pdNGAY_NH
        +","+piHTTT_ID
        +","+piLOAITIEN_ID
        +","+piNGANHANG_ID
        +",'"+psSOSEC+"'"
        +",'"+psCHUNGTU+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String xoaphieuHDDC(String schema
									, String psMA_HD
                                     ,String piPHIEU_ID
									 ,String psKIEUHUY
									 ,String psUSERID
									 ,String psUSERIP
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.xoaphieu_thanhtoan_hddc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+piPHIEU_ID+"'"
		+",'"+psKIEUHUY+"'"
		+",'"+psUSERIP+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String thanhtoanHDDV(String schema
									, String psMA_HD
                                     ,String psUSERID
									 ,String psUSERIP
                                     ,String piChucnang_gachno
									 ,String pdNGAY_TT
                                     ,String pdNGAY_NH
									 ,String piHTTT_ID
                                     ,String piLOAITIEN_ID
									 ,String piNGANHANG_ID
									 ,String psSOSEC
                                     ,String psCHUNGTU
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.gachno_hddv('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+psUSERIP+"'"
        +","+piChucnang_gachno
        +","+pdNGAY_TT
        +","+pdNGAY_NH
        +","+piHTTT_ID
        +","+piLOAITIEN_ID
        +","+piNGANHANG_ID
        +",'"+psSOSEC+"'"
        +",'"+psCHUNGTU+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String xoaphieuHDDV(String schema
									, String psMA_HD
                                     ,String piPHIEU_ID
									 ,String psKIEUHUY
									 ,String psUSERID
									 ,String psUSERIP
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.xoaphieu_thanhtoan_hddv('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+piPHIEU_ID+"'"
		+",'"+psKIEUHUY+"'"
		+",'"+psUSERIP+"'"
        +");"
        +"end;"
        ;
    return s;

  }
	public String xoaphieuHDCDLH(String schema
									  , String psMA_HD
									   , String piPHIEU_ID
									   , String psKIEUHUY
									   , String psUSERID
									   , String psUSERIP
									   )
	{


		String s = "begin ? := " + CesarCode.decode(schema) + "pttb_thtoan.xoaphieu_thanhtoan_hdcdlh('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + psMA_HD + "'"
			+ ",'" + piPHIEU_ID + "'"
			+ ",'" + psKIEUHUY + "'"
			+ ",'" + psUSERIP + "'"
			+ ");"
			+ "end;"
			;
		return s;

	}
  public String thanhtoanHDBM(String schema
									, String psMA_HD
                                     ,String psUSERID
									 ,String psUSERIP
                                     ,String piChucnang_gachno
									 ,String pdNGAY_TT
                                     ,String pdNGAY_NH
									 ,String piHTTT_ID
                                     ,String piLOAITIEN_ID
									 ,String piNGANHANG_ID
									 ,String psSOSEC
                                     ,String psCHUNGTU
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.gachno_hdbm('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+psUSERIP+"'"
        +","+piChucnang_gachno
        +","+pdNGAY_TT
        +","+pdNGAY_NH
        +","+piHTTT_ID
        +","+piLOAITIEN_ID
        +","+piNGANHANG_ID
        +",'"+psSOSEC+"'"
        +",'"+psCHUNGTU+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String xoaphieuHDBM(String schema
									, String psMA_HD
                                     ,String piPHIEU_ID
									 ,String psKIEUHUY
									 ,String psUSERID
									 ,String psUSERIP
									 ,String type_del
									 ) {


    String s=    "begin ? := admin_v2.pttb_thtoan.xoaphieu_thanhtoan_hdbm('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+piPHIEU_ID+"'"
		+",'"+psKIEUHUY+"'"
		+",'"+psUSERIP+"'"
		+",'"+type_del+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String thanhtoanHDCQ(String schema
									, String psMA_HD
                                     ,String psDsMATB
									 ,String psUSERID
									 ,String psUSERIP
                                     ,String piChucnang_gachno
									 ,String pdNGAY_TT
                                     ,String pdNGAY_NH
									 ,String piHTTT_ID
                                     ,String piLOAITIEN_ID
									 ,String piNGANHANG_ID
									 ,String psSOSEC
                                     ,String psCHUNGTU
									 ,String psHOPDONG
									 ,String psnguoinhan
									 ) {


    String s=    "begin ? := admin_v2.pttb_hmm.gachno_hdcq('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+psDsMATB+"'"
        +",'"+psUSERIP+"'"
        +","+piChucnang_gachno
        +","+pdNGAY_TT
        +","+pdNGAY_NH
        +","+piHTTT_ID
        +","+piLOAITIEN_ID
        +","+piNGANHANG_ID
        +",'"+psSOSEC+"'"
        +",'"+psCHUNGTU+"'"
		+",'"+psHOPDONG+"'"
		+",'"+psnguoinhan+"'"
        +");"
        +"end;"
        ;
    return s;

  }
  public String xoaphieuHDCQ(String schema
									, String psMA_HD
                                     ,String piPHIEU_ID
									 ,String psKIEUHUY
									 ,String psUSERID
									 ,String psUSERIP
									 ,String type_del
									 ) {


    String s=    "begin ? := admin_v2.pttb_thtoan.xoaphieu_thanhtoan_hdcq('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+piPHIEU_ID+"'"
		+",'"+psKIEUHUY+"'"
		+",'"+psUSERIP+"'"
		+",'"+type_del+"'"	
        +");"
        +"end;"
        ;
    return s;

  }
 
public String thanhtoanHDDOISIM(String schema
									, String psMA_HD
                                     ,String psUSERID
									 ,String psUSERIP
                                     ,String piChucnang_gachno
									 ,String pdNGAY_TT
                                     ,String pdNGAY_NH
									 ,String piHTTT_ID
                                     ,String piLOAITIEN_ID
									 ,String piNGANHANG_ID
									 ,String psSOSEC
                                     ,String psCHUNGTU
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.gachno_hddoisim('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+psUSERIP+"'"
        +","+piChucnang_gachno
        +","+pdNGAY_TT
        +","+pdNGAY_NH
        +","+piHTTT_ID
        +","+piLOAITIEN_ID
        +","+piNGANHANG_ID
        +",'"+psSOSEC+"'"
        +",'"+psCHUNGTU+"'"
        +");"
        +"end;"
        ;
    return s;
  }
  
  public String xoaphieuHDDOISIM(String schema
									, String psMA_HD
                                     ,String piPHIEU_ID
									 ,String psKIEUHUY
									 ,String psUSERID
									 ,String psUSERIP
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_thtoan.xoaphieu_thanhtoan_hddoisim('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"'"
        +",'"+piPHIEU_ID+"'"
		+",'"+psKIEUHUY+"'"
		+",'"+psUSERIP+"'"
        +");"
        +"end;"
        ;
    return s;

  }
   public String laytt_kh_matb(String schema,String matb,String piTRACUU_DB) {
    return "begin ?:="+CesarCode.decode(schema)+"PTTB_LAPHD.laytt_kh_matb('"+getUserVar("sys_dataschema")
	+"','"+matb+"'"
	+","+piTRACUU_DB+");"    	
	+" end;";
  }
  /*public String laytt_tb(String schema,String matb) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_matb('"+matb+"'"
	+");"    	
	+" end;";
  }*/
  public String laytt_tb(String schema,String matb) {
    return "begin ?:="+CesarCode.decode(schema)+"laytt_matb('"+matb+"'"
	+");"    	
	+" end;";
  }
  
  public String laytt_hdncdv(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"PTTB_VSMS.laytt_ht_hdncdv('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }
  public String laytt_hdncdv_theo_matb(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"PTTB_VSMS.laytt_ht_hdncdv_theo_matb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }
  
  //Ham luu thuc hien gui lenh NEW sang OCS
  public String HDLD_New_Sub_OCS(String matb, String loaitb)
	{		
		String logs="";
		/*  Kiem tra dau so qua OCS */
		String scheck="begin ? :=admin_v2.CHECK_OCS_SUB('"+matb+"'); end;";		
		String check_ = "";
		try{
			check_ = this.reqValue("",scheck);
		}catch(Exception exc){				
			check_= exc.getMessage();				
		}
		
		if(check_.equals("1")){
			try{
				String jsonString = Get_PPGW.NEW_Sub_OCS(matb,loaitb,getUserVar("sys_agentcode"));				
				//Insert log
				String request=matb+"|"+loaitb+"|"+getUserVar("sys_agentcode");
				logs= "begin ? :=admin_v2.pkg_ocs.insert_log('NEW','"+matb+"','"+getUserVar("userID")+ "','"+getEnvInfo().getParserParam("sys_userip").toString()+"','"+request+"','"+jsonString.replace("\"","")+"'); end;";
					
				if (jsonString.indexOf("SubExits")<0 && !jsonString.equals("\"0\"")){					
					String s = "begin ? :=admin_v2.insert_queue_ocs_ccbs('"+matb+"','"+loaitb+"','NEW','"+getUserVar("sys_agentcode")+"'); end;";					
					String return_ = this.reqValue("",s);					
				}
			} catch (Exception e) {
				e.printStackTrace();				
			}		
		}else {
			logs= "begin ? :='not exit'; end;";
		}//END IF
		//System.out.println("AAAAAAAA:"+logs);
		return logs;	
	}
	
}
