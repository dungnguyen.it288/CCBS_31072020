package neo.billing_doisoat;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;
import neo.smartui.report.SvgParser;
import javax.servlet.ServletException;
import neo.smartui.chart.servlet.ChartServlet;

public class chart_report extends NEOProcessEx {
  public void run() {
    System.out.println("neo.chart_report was called");
  }
  
  
  
  
      
 public NEOCommonData laytt_chart_active(
	  String psAgentCode  ,
      String psUserid  ,
      String psTuNgay  ,
      String psDenNgay ,
      String psReportType,    
	  String psAgentList 
 ) 
 { 	
	//String sFuncSchema = getSysCosnt("Funcschema") ;	
	String url = "begin ?:= billing_doisoat.report.laytt_chart_active('"+ psAgentCode
																 +"','"+ psUserid
																 +"','"+ psTuNgay
																 +"','"+ psDenNgay
																 +"','"+ psReportType
																 +"','"+ psAgentList																 
																 +"'); end;";
    	System.out.println(url);	
	    NEOExecInfo nei_ = new NEOExecInfo(url);
		nei_.setDataSrc("VNPBilling");
		return new NEOCommonData(nei_);
 }										 
	
 
  
  
      
 public NEOCommonData laytt_baocao(
	  String psAgentCode  ,
      String psUserid  ,
      String psTuNgay  ,
      String psDenNgay ,
      String psReportType,    
	  String psAgentList 
 ) 
 { 	
	//String sFuncSchema = getSysCosnt("Funcschema") ;	
	String url = "begin ?:= billing_doisoat.report.laytt_baocao('"+ psAgentCode
																 +"','"+ psUserid
																 +"','"+ psTuNgay
																 +"','"+ psDenNgay
																 +"','"+ psReportType
																 +"','"+ psAgentList																 
																 +"'); end;";
    	System.out.println(url);	
	    NEOExecInfo nei_ = new NEOExecInfo(url);
		nei_.setDataSrc("VNPBilling");
		return new NEOCommonData(nei_);
 }										 




 public NEOCommonData laytt_chart_sogiay(
	  String psAgentCode  ,
      String psUserid  ,
      String psTuNgay  ,
      String psDenNgay ,
      String psReportType,    
	  String psAgentList 
 ) 
 { 	
	//String sFuncSchema = getSysCosnt("Funcschema") ;	
	String url = "begin ?:= billing_doisoat.report.laytt_chart_sogiay('"+ psAgentCode
																 +"','"+ psUserid
																 +"','"+ psTuNgay
																 +"','"+ psDenNgay
																 +"','"+ psReportType
																 +"','"+ psAgentList																 
																 +"'); end;";
    	System.out.println(url);	
	    NEOExecInfo nei_ = new NEOExecInfo(url);
		nei_.setDataSrc("VNPBilling");
		return new NEOCommonData(nei_);
 }										 





 public NEOCommonData laytt_chart_socuoc(
	  String psAgentCode  ,
      String psUserid  ,
      String psTuNgay  ,
      String psDenNgay ,
      String psReportType,    
	  String psAgentList 
 ) 
 { 	
	//String sFuncSchema = getSysCosnt("Funcschema") ;	
	String url = "begin ?:= billing_doisoat.report.laytt_chart_socuoc('"+ psAgentCode
																 +"','"+ psUserid
																 +"','"+ psTuNgay
																 +"','"+ psDenNgay
																 +"','"+ psReportType
																 +"','"+ psAgentList																 
																 +"'); end;";
    	System.out.println(url);	
	    NEOExecInfo nei_ = new NEOExecInfo(url);
		nei_.setDataSrc("VNPBilling");
		return new NEOCommonData(nei_);
 }										 





      
 public NEOCommonData laytt_chart_action(
	  String psAgentCode  ,
      String psUserid  ,
      String psTuNgay  ,
      String psDenNgay ,
      String psReportType,    
	  String psAgentList 
 ) 
 { 	
	//String sFuncSchema = getSysCosnt("Funcschema") ;	
	String url = "begin ?:= billing_doisoat.report.laytt_chart_action('"+ psAgentCode
																 +"','"+ psUserid
																 +"','"+ psTuNgay
																 +"','"+ psDenNgay
																 +"','"+ psReportType
																 +"','"+ psAgentList																 
																 +"'); end;";
    	System.out.println(url);	
	    NEOExecInfo nei_ = new NEOExecInfo(url);
		nei_.setDataSrc("VNPBilling");
		return new NEOCommonData(nei_);
 }										 


 public String laydulieu(String psAgentCode
						, String psUserid
						, String psTuNgay 
						, String psDenNgay 
						, String psReportType
						, String psAgentList 

									 ) {
    String url_ ="/main?"+CesarCode.encode("configFile")+"=report/chart_report/ajax_chart_report"
		+"&"+CesarCode.encode("psAgentCode")+"="+psAgentCode
		+"&"+CesarCode.encode("psUserid")+"="+psUserid
		+"&"+CesarCode.encode("psTuNgay")+"="+psTuNgay
		+"&"+CesarCode.encode("psDenNgay")+"="+psDenNgay
		+"&"+CesarCode.encode("sReportType")+"="+psReportType        
		+"&"+CesarCode.encode("psAgentList")+"="+psAgentList        
    ;
    return url_;	
  }  
 
 
 
									 
 public NEOCommonData startChartServlet() throws ServletException
 {
	ChartServlet sp= new ChartServlet();		
	sp.init();
	
	String s ="select 1 from dual";	
						 

    NEOExecInfo nei_ = new NEOExecInfo(s);
	nei_.setDataSrc("VNPBilling");
	return new NEOCommonData(nei_);		
		
 }										 
  
  
}

