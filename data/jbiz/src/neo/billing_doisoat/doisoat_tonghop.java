package neo.billing_doisoat;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class doisoat_tonghop extends NEOProcessEx 
{
	public void run() {
		System.out.println("billing_doisoat.doisoat_tonghop was called");
	}
	public NEOCommonData doc_huong_dts(
		String psDsNCC,
		String chuky
	)
	{
		String s="/main?"+ CesarCode.encode("configFile")+ "=ccbs_doisoat/report/tonghop/ajax_huong_dts"
			+"&"+CesarCode.encode("NCCs")+"="+psDsNCC
			+"&"+CesarCode.encode("chuky")+"="+chuky;		
	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
		
	}
	
	
	public NEOCommonData doc_thang_bc(
		String psDsNCC
	)
	{
		String s="/main?"+ CesarCode.encode("configFile")+ "=ccbs_doisoat/report/capnhat_bc/ajax_thang_bc"
			+"&"+CesarCode.encode("NCCs")+"="+psDsNCC;		
	 
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
		
	}
	
	
	
	public NEOCommonData layds_baocao_tonghop( String CHUKY,
			String sTuNgay,
			String sDenNgay,		
			String sNCCs,
			String sHuong,
			String chkIOC,
			String chkNOC,
			String chkNTC,
			String chkSMO,
			String chkSMT,
			String chkOMO,
			String chkMOC,
			String chkMTC,
			String chknvu,
			String chkRoaDi,
			String chkRoaDen,
			String chkIRCall)	
	{
		
			String s="/main?"+ CesarCode.encode("configFile")+ "=ccbs_doisoat/report/tonghop/doisoat_tonghop_ajax1"
							+"&"+CesarCode.encode("CHUKY")+"="+CHUKY
							+"&"+CesarCode.encode("sTuNgay")+"="+sTuNgay
							+"&"+CesarCode.encode("sDenNgay")+"="+sDenNgay
							+"&"+CesarCode.encode("sNCCs")+"="+sNCCs
							+"&"+CesarCode.encode("sHuong")+"="+sHuong
							+"&"+CesarCode.encode("chkIOC")+"="+chkIOC
							+"&"+CesarCode.encode("chkNOC")+"="+chkNOC
							+"&"+CesarCode.encode("chkNTC")+"="+chkNTC
							+"&"+CesarCode.encode("chkSMO")+"="+chkSMO
							+"&"+CesarCode.encode("chkSMT")+"="+chkSMT
							+"&"+CesarCode.encode("chkOMO")+"="+chkOMO
							
							+"&"+CesarCode.encode("chkMOC")+"="+chkMOC
							+"&"+CesarCode.encode("chkMTC")+"="+chkMTC
							+"&"+CesarCode.encode("chknvu")+"="+chknvu
							+"&"+CesarCode.encode("chkRoaDi")+"="+chkRoaDi
							+"&"+CesarCode.encode("chkRoaDen")+"="+chkRoaDen
							+"&"+CesarCode.encode("chkIRCall")+"="+chkIRCall;
				
			 System.out.println(s); 	
			 NEOExecInfo nei_ = new NEOExecInfo(s);
			 nei_.setDataSrc("VNPBilling");
			 return new NEOCommonData(nei_);
							
	}
	
	
	 public NEOCommonData themmoi_report_doisoat(String  DoisoatSchema
 												,String  sMONTH_1
												,String  sSERVICE_ID_2
												,String  sB_VPEAK_CUOC_3
												,String  sB_DPEAK_CUOC_4
												,String  sB_VPEAK_PHUT_5							
												,String  sB_DPEAK_PHUT_6
												,String  sB_VOPEAK_CUOC_7
												,String  sB_DOPEAK_CUOC_8
												,String  sB_VOPEAK_PHUT_9
												,String  sB_DOPEAK_PHUT_10
												,String  sV_PEAK_CUOC_11
												,String  sD_PEAK_CUOC_12
												,String  sV_OPEAK_CUOC_13
												,String  sD_OPEAK_CUOC_14
												,String  sV_PEAK_PHUT_15
												,String  sD_PEAK_PHUT_16
												,String  sV_OPEAK_PHUT_17						
												,String  sD_OPEAK_PHUT_18
												,String  sDV_PEAK_CUOC_19
												,String  sDD_PEAK_CUOC_20
												,String  sDV_OPEAK_CUOC_21
												,String  sDD_OPEAK_CUOC_22
												,String  sDV_PEAK_PHUT_23
												,String  sDD_PEAK_PHUT_24
												,String  sDV_OPEAK_PHUT_25						
												,String  sDD_OPEAK_PHUT_26
												,String  sRATE_VPEAK_CUOC_27
												,String  sRATE_VOPEAK_CUOC_28
												,String  sRATE_DPEAK_CUOC_29
												,String  sRATE_DOPEAK_CUOC_30
												,String  sV_RTOTAL_31
												,String  sD_RTOTAL_32							
												,String  sRATE_VPEAK_PHUT_37
												,String  sRATE_VOPEAK_PHUT_38							
												,String  sRATE_DPEAK_PHUT_39
												,String  sRATE_DOPEAK_PHUT_40
												,String  sLOAIDS_ID_41																						
												)
			{
				String s = "begin ?:="  + DoisoatSchema + "report.themmoi_report_doisoat('" + getUserVar("userID")
									    + "','" + DoisoatSchema
										+ "','" +  sMONTH_1
										+ "','" +  sSERVICE_ID_2
										+ "','" +  sB_VPEAK_CUOC_3
										+ "','" +  sB_DPEAK_CUOC_4
										+ "','" +  sB_VPEAK_PHUT_5							
										+ "','" +  sB_DPEAK_PHUT_6
										+ "','" +  sB_VOPEAK_CUOC_7
										+ "','" +  sB_DOPEAK_CUOC_8
										+ "','" +  sB_VOPEAK_PHUT_9
										+ "','" +  sB_DOPEAK_PHUT_10
										+ "','" +  sV_PEAK_CUOC_11
										+ "','" +  sD_PEAK_CUOC_12
										+ "','" +  sV_OPEAK_CUOC_13
										+ "','" +  sD_OPEAK_CUOC_14
										+ "','" +  sV_PEAK_PHUT_15
										+ "','" +  sD_PEAK_PHUT_16
										+ "','" +  sV_OPEAK_PHUT_17						
										+ "','" +  sD_OPEAK_PHUT_18
										+ "','" +  sDV_PEAK_CUOC_19
										+ "','" +  sDD_PEAK_CUOC_20
										+ "','" +  sDV_OPEAK_CUOC_21
										+ "','" +  sDD_OPEAK_CUOC_22
										+ "','" +  sDV_PEAK_PHUT_23
										+ "','" +  sDD_PEAK_PHUT_24
										+ "','" +  sDV_OPEAK_PHUT_25						
										+ "','" +  sDD_OPEAK_PHUT_26
										+ "','" +  sRATE_VPEAK_CUOC_27
										+ "','" +  sRATE_VOPEAK_CUOC_28
										+ "','" +  sRATE_DPEAK_CUOC_29
										+ "','" +  sRATE_DOPEAK_CUOC_30
										+ "','" +  sV_RTOTAL_31
										+ "','" +  sD_RTOTAL_32							
										+ "','" +  sRATE_VPEAK_PHUT_37
										+ "','" +  sRATE_VOPEAK_PHUT_38							
										+ "','" +  sRATE_DPEAK_PHUT_39
										+ "','" +  sRATE_DOPEAK_PHUT_40
										+ "','" +  sLOAIDS_ID_41					 			 
						 				+ "');end;";	
						 
					 System.out.println(s);	
			    NEOExecInfo nei_ = new NEOExecInfo(s);
				nei_.setDataSrc("VNPBilling");
				return new NEOCommonData(nei_);
			}
         
         
         public NEOCommonData xoa_report_doisoat(String  DoisoatSchema
 												,String  sMONTH_1
												,String  sSERVICE_ID_2
												,String  sLOAIDS_ID_41																							
												)
			{
				String s = "begin ?:=" + DoisoatSchema + "report.xoa_report_doisoat('" + getUserVar("userID")
									    + "','" + DoisoatSchema
										+ "','" +  sMONTH_1
										+ "','" +  sSERVICE_ID_2
										+ "','" +  sLOAIDS_ID_41					 			 
										+ "');end;";	
						 
					 System.out.println(s);	
			    NEOExecInfo nei_ = new NEOExecInfo(s);
				nei_.setDataSrc("VNPBilling");
				return new NEOCommonData(nei_);
			}
			
		
		 public NEOCommonData capnhat_report_doisoat(String  DoisoatSchema
 												,String  sMONTH_1
												,String  sSERVICE_ID_2
												,String  sB_VPEAK_CUOC_3
												,String  sB_DPEAK_CUOC_4
												,String  sB_VPEAK_PHUT_5							
												,String  sB_DPEAK_PHUT_6
												,String  sB_VOPEAK_CUOC_7
												,String  sB_DOPEAK_CUOC_8
												,String  sB_VOPEAK_PHUT_9
												,String  sB_DOPEAK_PHUT_10
												,String  sV_PEAK_CUOC_11
												,String  sD_PEAK_CUOC_12
												,String  sV_OPEAK_CUOC_13
												,String  sD_OPEAK_CUOC_14
												,String  sV_PEAK_PHUT_15
												,String  sD_PEAK_PHUT_16
												,String  sV_OPEAK_PHUT_17						
												,String  sD_OPEAK_PHUT_18
												,String  sDV_PEAK_CUOC_19
												,String  sDD_PEAK_CUOC_20
												,String  sDV_OPEAK_CUOC_21
												,String  sDD_OPEAK_CUOC_22
												,String  sDV_PEAK_PHUT_23
												,String  sDD_PEAK_PHUT_24
												,String  sDV_OPEAK_PHUT_25						
												,String  sDD_OPEAK_PHUT_26
												,String  sRATE_VPEAK_CUOC_27
												,String  sRATE_VOPEAK_CUOC_28
												,String  sRATE_DPEAK_CUOC_29
												,String  sRATE_DOPEAK_CUOC_30
												,String  sV_RTOTAL_31
												,String  sD_RTOTAL_32							
												,String  sRATE_VPEAK_PHUT_37
												,String  sRATE_VOPEAK_PHUT_38							
												,String  sRATE_DPEAK_PHUT_39
												,String  sRATE_DOPEAK_PHUT_40
												,String  sLOAIDS_ID_41																					
												)
			{
				String s = "begin ?:=" + DoisoatSchema + "report.capnhat_report_doisoat('" + getUserVar("userID")
						+ "','" + DoisoatSchema
						+ "','" +  sMONTH_1
						+ "','" +  sSERVICE_ID_2
						+ "','" +  sB_VPEAK_CUOC_3
						+ "','" +  sB_DPEAK_CUOC_4
						+ "','" +  sB_VPEAK_PHUT_5							
						+ "','" +  sB_DPEAK_PHUT_6
						+ "','" +  sB_VOPEAK_CUOC_7
						+ "','" +  sB_DOPEAK_CUOC_8
						+ "','" +  sB_VOPEAK_PHUT_9
						+ "','" +  sB_DOPEAK_PHUT_10
						+ "','" +  sV_PEAK_CUOC_11
						+ "','" +  sD_PEAK_CUOC_12
						+ "','" +  sV_OPEAK_CUOC_13
						+ "','" +  sD_OPEAK_CUOC_14
						+ "','" +  sV_PEAK_PHUT_15
						+ "','" +  sD_PEAK_PHUT_16
						+ "','" +  sV_OPEAK_PHUT_17						
						+ "','" +  sD_OPEAK_PHUT_18
						+ "','" +  sDV_PEAK_CUOC_19
						+ "','" +  sDD_PEAK_CUOC_20
						+ "','" +  sDV_OPEAK_CUOC_21
						+ "','" +  sDD_OPEAK_CUOC_22
						+ "','" +  sDV_PEAK_PHUT_23
						+ "','" +  sDD_PEAK_PHUT_24
						+ "','" +  sDV_OPEAK_PHUT_25						
						+ "','" +  sDD_OPEAK_PHUT_26
						+ "','" +  sRATE_VPEAK_CUOC_27
						+ "','" +  sRATE_VOPEAK_CUOC_28
						+ "','" +  sRATE_DPEAK_CUOC_29
						+ "','" +  sRATE_DOPEAK_CUOC_30
						+ "','" +  sV_RTOTAL_31
						+ "','" +  sD_RTOTAL_32							
						+ "','" +  sRATE_VPEAK_PHUT_37
						+ "','" +  sRATE_VOPEAK_PHUT_38							
						+ "','" +  sRATE_DPEAK_PHUT_39
						+ "','" +  sRATE_DOPEAK_PHUT_40
						+ "','" +  sLOAIDS_ID_41					 			 
						 + "');end;";	
						 
					 System.out.println(s);	
			    NEOExecInfo nei_ = new NEOExecInfo(s);
				nei_.setDataSrc("VNPBilling");
				return new NEOCommonData(nei_);
			}
         

   public NEOCommonData laytt_thang_nhacc_dv_id (String  DoisoatSchema, 
   												 String nhacungcap_id,
   												 String loaids_id,
   												 String month) {
   	
   String s= "begin ?:="+DoisoatSchema+"report.laytt_thang_nhacc_dv_id('"+getUserVar("userID")
				+"','"+DoisoatSchema		
				+"','" +nhacungcap_id
				+"','" +loaids_id			
				+"','" +month
				+"');end;";

	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
     
  }
  
  	
  	
}