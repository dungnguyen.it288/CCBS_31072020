package neo.billing_doisoat;
import javax.sql.RowSet;

import neo.ccbs_export;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class khuyenmai extends NEOProcessEx {

    public void run() {
        System.out.println("neo.qltn.qltn_tienmat was called");
    }
    

	public NEOCommonData rec_upload_khuyenmai(String psUploadID){
	String s  ="begin ?:= khuyenmai.laytt_kiemtra_khuyenmai('"+psUploadID+"'); end;";

	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
	}
	
	public NEOCommonData value_accept_upload_khuyenmai(String psUploadID, String use_schema, String com_schema, String psuserip, int isOverride){				
		String s=  "begin ?:= khuyenmai.accept_upload_khuyenmai("
			+"'"+ psUploadID+"',"
			+isOverride+","
			+"'"+use_schema+ "'," 
			+"'"+com_schema+ "'," 			
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";
			
	     NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	 	return new NEOCommonData(nei_);
	}
	
	/*
	public NEOCommonData laydanhsach_tbkm(String user_id, String agent ){
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_upload_tbs/ds_tbkm&" + CesarCode.encode("user") + "=" + user_id+"&agent="+agent;
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}*/
	
	public NEOCommonData layds_upload_error(String uploadId, String agent){
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_upload_tbs/ds_upload_error&" + CesarCode.encode("uploadId") + "="+uploadId+"&agent="+agent;
		NEOExecInfo nei_ = new NEOExecInfo(url_);
 		nei_.setDataSrc("VNPBilling");
    	return new NEOCommonData(nei_);
	}
	
	public NEOCommonData layds_upload(String upload_id )
	{
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_upload_tbs/ds_upload&" + CesarCode.encode("id") + "=" + upload_id;
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}
	
	public String export_dsupload_error(String uploadId, String agent,String dataSchema, String comSchema) {	  
		  	  	
	    try {
	    	String s = "begin ?:=ccs_common.khuyenmai.layds_upload_error(" 
				+ "'" + uploadId + "'"
				+ ",'" + dataSchema + "'"
				+ ",'" + comSchema + "'"
				+ ",'" + agent + "'"				
				+ ");end;";
	    	
			RowSet rowSet = reqRSet("VNPBilling",s);
			
			String fileUploadDir = getSysConst("FileUploadDir");
			String ccs = getUserVar("sys_dataschema");
			ccs = ccs.replace(".", "");
			fileUploadDir = fileUploadDir + "Export_DSKM" + "\\" + ccs;
			String fileName = "dskm_upload_error.xls";
			
			Runtime.getRuntime().exec("cmd /C mkdir " + fileUploadDir);
			
			String fileOutput = fileUploadDir + "\\" + fileName;

			ccbs_export.exportToExcel(fileOutput, fileName, rowSet);
			
			String ret = "begin ?:='" + "{fileurl:\"" + fileOutput.replace("\\", "\\\\") + "\",filename:\"" + fileName +"\"}" + "'; end;";
			
			return ret;
		
	    } catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}	
		
	/*--upload tinh*/
	public NEOCommonData layds_tb_km(String psmsisdn,
									String psnguoicn,
									String pstungay,
									String psdenngay,
									String pskieuth)
	{
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_upload_tinh/ds_tb_km&" 
															+ CesarCode.encode("msisdn") + "=" + psmsisdn
														+"&"+ CesarCode.encode("psnguoicn") + "=" + psnguoicn
														+"&"+ CesarCode.encode("pstungay") + "=" + pstungay
														+"&"+ CesarCode.encode("psdenngay") + "=" + psdenngay
														+"&"+ CesarCode.encode("pskieuth") + "=" + pskieuth;
			System.out.println(url_);
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
	public NEOCommonData rec_upload_khuyenmai_tinh(String psUploadID){
	String s  ="begin ?:= ccs_common.khuyenmai.laytt_kiemtra_khuyenmai_tinh('"+psUploadID+"'); end;";

	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
	}
	public NEOCommonData layds_upload_tinh_error(String uploadId, String agent){
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_upload_tinh/ds_upload_error&" + CesarCode.encode("uploadId") + "="+uploadId+"&agent="+agent;
		NEOExecInfo nei_ = new NEOExecInfo(url_);
 		nei_.setDataSrc("VNPBilling");
    	return new NEOCommonData(nei_);
	}
	public NEOCommonData accept_upload_khuyenmai_tinh(String psUploadID, String use_schema, String com_schema, String psuserip, int isOverride){				
		String s=  "begin ?:= ccs_common.khuyenmai.accept_upload_khuyenmai_tinh("
			+"'"+ psUploadID+"',"
			+isOverride+","
			+"'"+use_schema+ "'," 
			+"'"+com_schema+ "'," 			
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";
			
	     NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	 	return new NEOCommonData(nei_);
	}
	public String export_dsupload_tinh_error(String uploadId, String agent,String dataSchema, String comSchema) {	  
		  	  	
	    try {
	    	String s = "begin ?:=ccs_common.khuyenmai.layds_upload_tinh_error(" 
				+ "'" + uploadId + "'"
				+ ",'" + dataSchema + "'"
				+ ",'" + comSchema + "'"
				+ ",'" + agent + "'"				
				+ ");end;";
	    	
			RowSet rowSet = reqRSet("VNPBilling",s);
			
			String fileUploadDir = getSysConst("FileUploadDir");
			String ccs = getUserVar("sys_dataschema");
			ccs = ccs.replace(".", "");
			fileUploadDir = fileUploadDir + "Export_DSKM" + "\\" + ccs;
			String fileName = "dskm_upload_error.xls";
			
			Runtime.getRuntime().exec("cmd /C mkdir " + fileUploadDir);
			
			String fileOutput = fileUploadDir + "\\" + fileName;

			ccbs_export.exportToExcel(fileOutput, fileName, rowSet);
			
			String ret = "begin ?:='" + "{fileurl:\"" + fileOutput.replace("\\", "\\\\") + "\",filename:\"" + fileName +"\"}" + "'; end;";
			
			return ret;
		
	    } catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
		public String export_dsuploadKM(String kmhtId, String agent,String dataSchema, String comSchema) {	  
		  	  	
	    try {
	    	String s = "begin ?:=ccs_common.khuyenmai.layds_upload_km(" 
				+ "'" + kmhtId + "'"
				+ ",'" + dataSchema + "'"
				+ ",'" + comSchema + "'"
				+ ",'" + agent + "'"				
				+ ");end;";
	    	
			RowSet rowSet = reqRSet("VNPBilling",s);
			
			String fileUploadDir = getSysConst("FileUploadDir");
			String ccs = getUserVar("sys_dataschema");
			ccs = ccs.replace(".", "");
			fileUploadDir = fileUploadDir + "Export_DSKM" + "\\" + ccs;
			String fileName = "ds_khuyenmai.txt";
			
			Runtime.getRuntime().exec("cmd /C mkdir " + fileUploadDir);
			
			String fileOutput = fileUploadDir + "\\" + fileName;

			ccbs_export.exportToTextFile(fileOutput, fileName, rowSet);
			
			String ret = "begin ?:='" + "{fileurl:\"" + fileOutput.replace("\\", "\\\\") + "\",filename:\"" + fileName +"\"}" + "'; end;";
			
			return ret;
		
	    } catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}	
	
	public String export_SimKit(String psuploadId, String pstype, String psUserId, String psAgent, String psDate) 
	{
		try {
	    	String s = "begin ?:=admin_v2.pkg_upload_msisdn.exportTXT_data_func(" 
				+ "'" + psuploadId + "'"
				+ ",'" + pstype + "'"
				+ ",'" + psUserId + "'"
				+ ",'" + psAgent + "'"				
				+ ");end;";
	    	
			RowSet rowSet = reqRSet("",s);			
			String fileUploadDir = getSysConst("FileUploadDir");
			String ccs = getUserVar("sys_dataschema");
			ccs = ccs.replace(".", "");
			fileUploadDir = fileUploadDir + "Export_DSKM" + "\\" + ccs;
			String fileName = psDate + "_" + psAgent + "_" + psUserId + ".txt";			
			Runtime.getRuntime().exec("cmd /C mkdir " + fileUploadDir);			
			String fileOutput = fileUploadDir + "\\" + fileName;
			ccbs_export.exportToTextFile(fileOutput, fileName, rowSet);			
			String ret = "{filepath:\"" + fileOutput.replace("\\", "\\\\") + "\",filename:\"" + fileName +"\"}";			
			return ret;
		
	    } catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}	
	
	    /* Export du lieu khach hang doanh nghiep. Export dang excel */
	public String export_excel_khdn_ct(String psschema,
									   String psmadn,
									   String pstungay,
									   String psdenngay,
									   String psnguoith,
									   String psquanid,
									   String psphuongid,
									   String pskieubc) {	  
		  	  	
	    try {
			String s = "begin ? := admin_v2.pkg_doanhnghieps.report_doanhnghiep_func_('" 
															+ psschema +"','"
															+ psmadn  +"','"
															+ pstungay +"','"
															+ psdenngay +"','"
															+ psnguoith +"','"
															+ psquanid +"','"
															+ psphuongid  +"','"
															+ pskieubc +"'); end;";										
	    		    	
			RowSet rowSet = reqRSet("",s);
			
			String fileUploadDir = getSysConst("FileUploadDir");
			String ccs = getUserVar("sys_dataschema");
			ccs = ccs.replace(".", "");
			fileUploadDir = fileUploadDir + "Export_DSKM" + "\\" + ccs;
			String fileName = "khdn_chitiet.xls";
			
			Runtime.getRuntime().exec("cmd /C mkdir " + fileUploadDir);
			
			String fileOutput = fileUploadDir + "\\" + fileName;

			ccbs_export.exportToExcel(fileOutput, fileName, rowSet);
			
			String ret = "begin ?:='" + "{fileurl:\"" + fileOutput.replace("\\", "\\\\") + "\",filename:\"" + fileName +"\"}" + "'; end;";
			
			return ret;
		
	    } catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public String export_excel_khdn_th(String psschema,
									   String psmadn,
									   String pstungay,
									   String psdenngay,
									   String psnguoith,
									   String psquanid,
									   String psphuongid,
									   String pskieubc) {	  
		  	  	
	    try {
			String s = "begin ? := admin_v2.pkg_doanhnghieps.report_doanhnghiep_func_('" 
															+ psschema +"','"
															+ psmadn  +"','"
															+ pstungay +"','"
															+ psdenngay +"','"
															+ psnguoith +"','"
															+ psquanid +"','"
															+ psphuongid  +"','"
															+ pskieubc +"'); end;";										
	    		    	
			RowSet rowSet = reqRSet("",s);
			
			String fileUploadDir = getSysConst("FileUploadDir");
			String ccs = getUserVar("sys_dataschema");
			ccs = ccs.replace(".", "");
			fileUploadDir = fileUploadDir + "Export_DSKM" + "\\" + ccs;
			String fileName = "khdn_tonghop.xls";
			
			Runtime.getRuntime().exec("cmd /C mkdir " + fileUploadDir);
			
			String fileOutput = fileUploadDir + "\\" + fileName;

			ccbs_export.exportToExcel(fileOutput, fileName, rowSet);
			
			String ret = "begin ?:='" + "{fileurl:\"" + fileOutput.replace("\\", "\\\\") + "\",filename:\"" + fileName +"\"}" + "'; end;";
			
			return ret;
		
	    } catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
/*	public String rec_upload_khuyenmai(String psUploadID){
		String out_ ="begin ?:= khuyenmai.laytt_kiemtra_khuyenmai('"+psUploadID+"'); end;";
		return out_;
	}  
public String value_accept_upload_khuyenmai(String psUploadID, String psuserip){				
		return "begin ?:= banmay.accept_upload_khuyenmai("
			+"'"+ psUploadID+"',"
			+"'"+getSysConst("DataSchema") + "'," 
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";			
	}	*/
}