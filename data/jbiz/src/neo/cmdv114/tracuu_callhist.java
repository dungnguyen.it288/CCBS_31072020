package neo.cmdv114;

import java.io.*;
import java.sql.SQLException; 
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import javax.sql.RowSet;
import java.util.Date;
import java.text.*;

import org.apache.commons.io.FileUtils;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

import cosaldap.Base64.*;
import neo.qlsp.*;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import javax.xml.parsers.DocumentBuilder; 
import javax.xml.parsers.DocumentBuilderFactory; 
import javax.xml.parsers.ParserConfigurationException; 
import org.w3c.dom.Document; 
import org.w3c.dom.Element; 
import org.w3c.dom.Node; 
import org.w3c.dom.NodeList; 
import org.xml.sax.InputSource; 


public class tracuu_callhist extends NEOProcessEx
{
  public String getUrlDanhSachCH(String sysPageID, String sysRecPerPage,
                                 String so_tb, String tu_ngay,
                                 String den_ngay, String so_tk1,
                                 String so_tk2, String so_tk3,
                                 String so_tk4, String so_tk5
      ) {
    String url_ ="/main?"+CesarCode.encode("configFile")+"=common/tracuu_callhist_lsugoi_ajax"
        + "&"+CesarCode.encode("sysPageID")+"="+sysPageID
        + "&"+CesarCode.encode("sysRecPerPage")+"="+sysRecPerPage
        + "&"+CesarCode.encode("so_tb")+"="+so_tb
        + "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
        + "&"+CesarCode.encode("den_ngay")+"="+den_ngay
        + "&"+CesarCode.encode("so_tk1")+"="+so_tk1
        + "&"+CesarCode.encode("so_tk2")+"="+so_tk2
        + "&"+CesarCode.encode("so_tk3")+"="+so_tk3
        + "&"+CesarCode.encode("so_tk4")+"="+so_tk4
        + "&"+CesarCode.encode("so_tk5")+"="+so_tk5
        +"&sid="+Math.random()
    ;
    return url_;
  }
  
  public String getUrlDanhSach5so(String sysPageID, String sysRecPerPage,
                                 String so_tb, String tu_ngay,
                                 String den_ngay, String so_tk1,
                                 String so_tk2, String so_tk3,
                                 String so_tk4, String so_tk5
      ) {
    String url_ ="/main?"+CesarCode.encode("configFile")+"=common/tracuu_callhist_5so_ajax"
        + "&"+CesarCode.encode("sysPageID")+"="+sysPageID
        + "&"+CesarCode.encode("sysRecPerPage")+"="+sysRecPerPage
        + "&"+CesarCode.encode("so_tb")+"="+so_tb
        + "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
        + "&"+CesarCode.encode("den_ngay")+"="+den_ngay
        + "&"+CesarCode.encode("so_tk1")+"="+so_tk1
        + "&"+CesarCode.encode("so_tk2")+"="+so_tk2
        + "&"+CesarCode.encode("so_tk3")+"="+so_tk3
        + "&"+CesarCode.encode("so_tk4")+"="+so_tk4
        + "&"+CesarCode.encode("so_tk6")+"="+so_tk5
        +"&sid="+Math.random()
		
    ;
	System.out.println("dongnd :"+url_);
    return url_;
  }  
  public NEOCommonData kiemTra5So(String so_tb, String tu_ngay,
                                 String den_ngay, String so_tk1,
                                 String so_tk2, String so_tk3,
                                 String so_tk4, String so_tk5
      ) {
    String sFuncSchema = getSysConst("FuncSchema");
	String sUserId = getUserVar("userID");
	String sDataSchema = getSysConst("sys_dataschema");	
	String sAgentCode = getUserVar("sys_agentcode");
	
    String s=    "begin ? := PKG_TRACUU_CALLHIST.ktra5so_layds_lsugoi_ex("
		+"  '"+sUserId +"'"
		+",'"+sAgentCode+"'"
		+",'"+so_tb+"'"
		+",'"+tu_ngay+"'"
		+",'"+den_ngay+"'"
		+",'"+so_tk1+"'"
		+",'"+so_tk2+"'"
		+",'"+ so_tk3+"'"
		+",'"+so_tk4+"'"		
		+",'"+so_tk5+"'"		
        +");"
        +"end;"   ;
    
	NEOExecInfo ei_ = new NEOExecInfo( s);
    ei_.setDataSrc("CALL_HIST");		
	
	return new NEOCommonData(ei_);
	
  }
	private String getString(Object ojb) {
		if(ojb == null) return "";
		return (String) ojb;
	}
  public String getHistcall(String ma_tb, String month, String so_tracuu)
  {
	  
	    try{
			//String session = getSession();
			StringBuffer ret = new StringBuffer();			 
			System.out.println("-------------------GET API-THUYVTT_____5 so tra cuu ="+so_tracuu+"---tu_ngay="+month);
			String jsonString = Get_API_tracuu_5so.get5so(ma_tb,month,so_tracuu);	
			System.out.println("-------------------GET API-THUYVTT_____ketqua="+jsonString);
			//String tpl = FileUtils.readFileToString(new File(this.getSysConst("ConfigDir")+"pttb/tracuu/tracuu_gdvp/tc_data_ajax_api.htm"));
			String tpl = FileUtils.readFileToString(new File(this.getSysConst("ConfigDir")+"common/tracuu_callhist_5so_ajax_data_new.htm"));
			JSONParser jsonParser = new JSONParser();	            
			JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonString);	                           
		    String error_code = (String) jsonObject.get("error_code");
			if(error_code.equals("0")) {
				try{
					JSONArray jsonArray = (JSONArray) jsonObject.get("result");
					int stt;
					int i;
					int max =jsonArray.size();
					System.out.println("-------------------GET API-THUYVTT_____max ="+max);
					if(max>0) {
						for (i = 0; i < jsonArray.size(); i++) {
							JSONObject jsonObject_ = (JSONObject) jsonArray.get(i);				
							stt				= i+1;
							String numofcall  = getString(jsonObject_.get("count"));
							String call		= getString(jsonObject_.get("msisdn")); 											
							ret.append(tpl.replace("{p0so_tb}",ma_tb+"").replace("{f01b_number}",call).replace("{f01numofcall}",numofcall).replace("{seq}",String.valueOf(stt)));			
						}
					} else
					{
						ret.append(tpl.replace("{p0so_tb}",ma_tb+"").replace("{f01b_number}","").replace("{f01numofcall}","").replace("{seq}","0"));					
					}
				}catch(Exception e){
					ret.append(tpl.replace("{p0so_tb}",ma_tb+"").replace("{f01b_number}","").replace("{f01numofcall}","").replace("{seq}","0"));					
				}
			} else {			
				ret.append(tpl.replace("{p0so_tb}",ma_tb+"").replace("{f01b_number}","").replace("{f01numofcall}","").replace("{seq}","0"));
			}
				
			getEnvInfo().setUserVar("tracuu_data_api",ret.toString());
			//System.out.println(ret.toString());	
			//return ret.toString();
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		String time_start = "01/"+month.substring(0,2)+"/"+month.substring(2);
	    System.out.println("THUYVTTTTTTTTT-------------time_start="+time_start);
		Date date = new Date();
		SimpleDateFormat ft = new SimpleDateFormat ("dd/MM/yyyy");
		String time_end = ft.format(date);
		System.out.println("THUYVTTTTTTTTT-------------time_end="+time_end);
		String url_ ="/main?"+CesarCode.encode("configFile")+"=common/tracuu_callhist_5so_ajax_new"
					+ "&"+CesarCode.encode("tu_ngay")+"="+time_start
					+ "&"+CesarCode.encode("den_ngay")+"="+time_end
					+ "&"+CesarCode.encode("so_tb")+"="+ma_tb
		;
		
	    return url_;
  }   
}











