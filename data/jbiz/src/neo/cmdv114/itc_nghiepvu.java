package neo.cmdv114;

import neo.smartui.process.*;
import neo.smartui.report.*;
import javax.sql.*;
import java.io.*;
import java.net.*;

import neo.smartui.common.CesarCode;
import java.util.concurrent.TimeUnit;
public class itc_nghiepvu extends NEOProcessEx 
{
	

	public NEOCommonData rec_upload_tbthuhoi(String psUploadID){
	String s  ="begin ?:= admin_v2.itc_thuhoi_tb.laytt_kiemtra_tbthuhoi('"+psUploadID+"'); end;";

	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	// nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
	}
	
	public NEOCommonData value_accept_upload_tbthuhoi(String psUploadID, String use_schema, String com_schema, String psuserid, String psuserip) throws Exception{
		RowSet count = this.reqRSet("SELECT msisdn, note FROM admin_v2.upload_tb_thuhoi where upload_id ='"+psUploadID+"' and status is null ");
		String res="";
		String sub="";
		String note="";
		while (count.next()){
			System.out.println("--------------------------------------------thu hoi kho:"); 
			sub = count.getString("msisdn");
			note = "upload:"+psUploadID+ +"/" +count.getString("note") ;
			System.out.println("thuc hien cap nhat:"+sub+" "+note); 
			res=itc_daylaikho_tb(sub,"",note,psuserid,psuserip);
			//res="test";
			if (!res.equals("1"))
			{
				String s  ="begin ?:= admin_v2.itc_thuhoi_tb.update_upload_tbthuhoi('"+psUploadID+"','"+sub+"','"+res+"'); end;";

				System.out.println(s); 	
				String ss_= this.reqValue("", s);				
				System.out.println("--------------------------------------------thu hoi kho:"+ss_); 
			}
		}
			
	 	return new NEOCommonData("1");
	}
	
	
	
	public NEOCommonData layds_upload_tbthuhoi_error(String uploadId, String agent){
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=vinacore/doi_sim_ktnv/thuhoi_kho_itc/thuhoi_tb/ds_upload_error&" + CesarCode.encode("uploadId") + "="+uploadId+"&agent="+agent;
		NEOExecInfo nei_ = new NEOExecInfo(url_); 		
    	return new NEOCommonData(nei_);
	}
		
	public String itc_daylaikho_tb(String psMsisdn, String msidn, String ghichu, String psuserid, String psuserip) throws Exception{				
		
		String soap="";
		String url="http://10.156.4.100:9966/ws/in";
		String s=    "begin ? := subadmin.put_sub_numstore("		
		+"'"+psMsisdn+"'"		
		+");"
        +"end;";		
		//String result = this.reqValue("CMDVOracleDS", s);
		String result = this.reqValue("CMDVOracleDS", s);
		System.out.println("--------Xoa thog tin:"+result); 
		if (result.equals("1")) {
			//huy KMCb
			String sub=psMsisdn.substring(2);
			String xml="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws.seconds.vnp.com/\">";
			xml=xml+"<soapenv:Header/>";
			xml=xml+"<soapenv:Body>";
			xml=xml+"<ws:destroySubs>";
			xml=xml+"<isdn>"+sub+"</isdn>";
			xml=xml+"<agent>TOOL_KHOSO_ITC</agent>";
			xml=xml+"<token>QVBJI1RPT0xfS0hPU09fSVRDI3B3ZA==</token>";
			xml=xml+"</ws:destroySubs>";
			xml=xml+"</soapenv:Body>";
			xml=xml+"</soapenv:Envelope>";
			soap=apiPost(xml,url);
			System.out.println("--------KMCB:"+soap); 
		}
		
		//insert log thu hien
		String str=    "begin ? := admin_v2.itc_thuhoi_tb.log_thuhoi_tb("		
		+"'"+psMsisdn+"',"
		+"'"+msidn+"',"
		+"'"+ghichu+"',"
		+"'"+result+"',"
		+"'"+psuserid+"',"
		+"'"+psuserip+"'"	
		+");"
        +"end;";
		String ss_= this.reqValue("", str);
		return result;
	}
	
	
	
	public String apiPost(String input, String sUrl) {
		
		try {
			byte[] postData = input.getBytes("UTF-8");
			//byte[] postData = input.getBytes();
			int postDataLength = postData.length;
			// URL url = new URL(urlHttp + method);
			URL url = new URL(sUrl );
			HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
			httpcon.setDoOutput(true);

			// add reuqest header
			httpcon.setRequestMethod("POST");
			httpcon.addRequestProperty("Content-Type", "text/xml;charset=UTF-8");
			httpcon.addRequestProperty("SOAPAction", "");
			httpcon.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			// httpcon.setUseCaches( false );
			OutputStream os = httpcon.getOutputStream();
			os.write(postData);
			//os.write(input.getBytes());
			os.flush();
			// Send post request
			int responseCode = httpcon.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + sUrl);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(httpcon.getInputStream()));
			String inputLine;
			String resp = "";
			while ((inputLine = in.readLine()) != null) {
				resp += (inputLine);
			}
			System.out.println("Response  : " + resp);
			in.close();
			// String kq =getValue(resp,"SYORI_STS");
			// String id_member =getValue(resp,"KAIIN_NO");
			// String out = kq+":"+id_member;
			return resp;

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}

	}

	
}