package neo.cmdv114;

import neo.smartui.process.*;
import neo.smartui.report.*;
import javax.sql.*;
import java.util.concurrent.TimeUnit;
public class vinacore_new extends NEOProcessEx 
{
	
	public NEOCommonData layTTThueBao_v5(String so_tb, String captcha)
	{
		RowSet rs_ = null;
		try {
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"layTTThueBao_v5", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return new NEOCommonData(rs_);
			reqValue("begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
					+so_tb+"','layTTThueBao_v5'," 
					+"'"+so_tb+";',"
					+"'','"
					+getUserVar("userID")+"','"
					+getEnvInfo().getParserParam("sys_userip")+"','"
					+getUserVar("sys_agentcode")+"'); "
					+ "   commit; "
					+ " end; ");
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage()) ;
			// TODO: handle exception
		}
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.LAYTT_THUEBAO_v5(?,"+"'"+captcha+"'); ";
		xs_ = xs_ + " end; ";
		String agent = getUserVar("sys_agentcode"); 
		String userid = getUserVar("userID"); 
		
		
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		//Thuyvtt comment cho test ko cho thue bao VCC doi sim
		nei_.setDataSrc("CMDVOracleDS"); 
		nei_.bindParameter(2,so_tb);
	
		
		
		try{
			rs_ = reqRSet(nei_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println(xs_);
		
		return new NEOCommonData(rs_);

	}
	
	public NEOCommonData layTTThueBao_v6(String so_tb, String captcha)
	{
		RowSet rs_ = null;
		try {
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"layTTThueBao_v6", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return new NEOCommonData(rs_);
			reqValue("begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
					+so_tb+"','layTTThueBao_v6'," 
					+"'"+so_tb+";',"
					+"'','"
					+getUserVar("userID")+"','"
					+getEnvInfo().getParserParam("sys_userip")+"','"
					+getUserVar("sys_agentcode")+"'); "
					+ "   commit; "
					+ " end; ");
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage()) ;
			// TODO: handle exception
		}
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.LAYTT_THUEBAO_v6(?,"+"'"+captcha+"'); ";
		xs_ = xs_ + " end; ";
		
		
		
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		nei_.setDataSrc("CMDVOracleDS");
		nei_.bindParameter(2,so_tb);
		
		
		try{
			rs_ = reqRSet(nei_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println(xs_);
		
		return new NEOCommonData(rs_);

	}
	
	public NEOCommonData layTTThueBao_v4(String so_tb, String captcha)
	{
		RowSet rs_ = null;
		try {
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"layTTThueBao_v3", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return new NEOCommonData(rs_);
			reqValue("begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
					+so_tb+"','layTTThueBao_v4'," 
					+"'"+so_tb+";',"
					+"'','"
					+getUserVar("userID")+"','"
					+getEnvInfo().getParserParam("sys_userip")+"','"
					+getUserVar("sys_agentcode")+"'); "
					+ "   commit; "
					+ " end; ");
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage()) ;
			// TODO: handle exception
		}
	
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.laytt_thuebao_ccbs_qlsp_v4(?,'"+getUserVar("userID")+"','"+captcha+"'); ";
		xs_ = xs_ + " end; ";
			System.out.println(xs_);	
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		nei_.setDataSrc("CMDVOracleDS");
		nei_.bindParameter(2,so_tb);
		
				
		try{
			rs_ = reqRSet(nei_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		try {			
			TimeUnit.MILLISECONDS.sleep(300);    
			} catch (InterruptedException e) {			
			}
		//System.out.println(xs_);
		return new NEOCommonData(rs_);
	} 
	
	
}