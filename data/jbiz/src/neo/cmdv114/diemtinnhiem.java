package neo.cmdv114;

import neo.smartui.process.*;
import neo.smartui.report.*;
import javax.sql.*;

import java.net.InetAddress;
import java.net.UnknownHostException;

import neo.qlsp.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import cosaldap.Base64.*;
import neo.smartui.common.*;

public class diemtinnhiem extends NEOProcessEx 
{
	
	public String chkTBTonTai(String so_tb)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_utils.CHECK_TB_TONTAI('"+so_tb+"'); ";
		xs_ = xs_ + " end; ";
		
		return xs_;
	} 

	public NEOCommonData layTTThueBao(String so_tb)
	{
		RowSet rs_ = null;
		/*try {
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"layTTThueBao", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return new NEOCommonData(rs_);
			reqValue("begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
					+so_tb+"','layTTThueBao'," 
					+"'"+so_tb+";',"
					+"'','"
					+getUserVar("userID")+"','"
					+getEnvInfo().getParserParam("sys_userip")+"','"
					+getUserVar("sys_agentcode")+"'); "
					+ "   commit; "
					+ " end; ");
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage()) ;
			// TODO: handle exception
		} tuyetptm cmt ngay 2/12/2015*/
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_utils.LAYTT_THUEBAO(?); ";
		xs_ = xs_ + " end; ";
		
		
		
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		nei_.setDataSrc("CMDVOracleDS");
		nei_.bindParameter(2,so_tb);
		
		
		try{
			rs_ = reqRSet(nei_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println(xs_);
		
		return new NEOCommonData(rs_);

	} 	
	public NEOCommonData layTTThueBao_v2(String so_tb)
	{
	
		RowSet rs_ = null;
		try {
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"layTTThueBao_v2", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return new NEOCommonData(rs_);
			reqValue("begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
					+so_tb+"','layTTThueBao_v2'," 
					+"'"+so_tb+";',"
					+"'','"
					+getUserVar("userID")+"','"
					+getEnvInfo().getParserParam("sys_userip")+"','"
					+getUserVar("sys_agentcode")+"'); "
					+ "   commit; "
					+ " end; ");
		} catch (Exception e) {}
		
		//1. Lay Flag cho tich hop QLSP
		String s1="";
		String rq_=null;
		try {
			s1 = "begin ?:= admin_v2.pkg_chonso.lay_flag(18); end;";
			rq_=this.reqValue("",s1); 
		}catch (Exception ex){} 	
		
		String xs_ = "";
		System.out.println("FLAG:"+rq_);
		System.out.println("FLAG:"+rq_);
		System.out.println("FLAG:"+rq_);
		System.out.println("FLAG:"+rq_);
		System.out.println("FLAG:"+rq_);
		System.out.println("FLAG:"+rq_);
		System.out.println("FLAG:"+rq_);
		System.out.println("FLAG:"+rq_);
		System.out.println("FLAG:"+rq_);
		System.out.println("FLAG:"+rq_);
		System.out.println("FLAG:"+rq_);
		System.out.println("FLAG:"+rq_);
		if(rq_.equals("1"))
		{
			xs_ = xs_ + " begin ";
			//xs_ = xs_ + " 	? := subadmin.LAYTT_THUEBAO_V2(?); ";
			xs_ = xs_ + " 	? := subadmin.LAYTT_THUEBAO_V2_30102016(?,'"+getUserVar("userID")+"'); ";
			xs_ = xs_ + " end; ";
		}else{	
			String status=""; 
			String khoId="";
			String kieusoId="";
			String provice ="";
			try {		
				Connect cl = new Connect();
				String sessionId =cl.readSession();
				String checkIsdn=cl.checkIsdnLite(sessionId.trim(),so_tb);
				checkIsdn=checkIsdn.replace("||","#");
				String[] array_str = checkIsdn.split("#");
				status	= array_str[1];
				khoId 	= array_str[2];				
				provice  = array_str[4];			
			} catch (Exception ex) {}
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := subadmin.laytt_thuebao_qlsp_v2(?,'"+getUserVar("userID")+"','"+status+"','"+provice+"','"+khoId+"'); ";			
			xs_ = xs_ + " end; ";
		}
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		nei_.setDataSrc("CMDVOracleDS");
		nei_.bindParameter(2,so_tb);		
		
		try{
			rs_ = reqRSet(nei_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		System.out.println(xs_);		
		return new NEOCommonData(rs_);
	} 
	public String layTTKhTb(String so_tb, String ma_tinh)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := pttb_ccbs.laytt_khtb('"+so_tb+"','"+ma_tinh+"'); ";
		xs_ = xs_ + " end; ";

		return xs_;
	}
	public String layTTNoCuoi(String so_tb)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := vinaxdc.xdc.laytt_nocuoi('"+so_tb+"'); ";
		xs_ = xs_ + " end; ";

		return xs_;
	} 	
	
	//NATuan 31/10/2016: Dang ky thue bao cho goi NVKD dia ban VNPT
	public String reg_vplus_pps(String psmsisdn, String pspackage, String psuserip)
	{		
		String _userid = getUserVar("userID");
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.pkg_cskh_pps.reg_vplus_pps('"+psmsisdn+"','"+pspackage+"','"+_userid+"','"+psuserip+"'); ";
		xs_ = xs_ + " end; ";			
		return xs_;		
	}
	
	public String get_ds_goi(String so_tb){
		String rs_ = null;
			String xs_ = "";
		try{
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.get_ds_goi('"+so_tb+"'); ";
			//xs_ = xs_ + " 	commit; ";
			xs_ = xs_ + " end; ";
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			//rs_  =  reqValue(xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String get_used(String so_tb){
		String rs_ = null;
			String xs_ = "";
		try{
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.check_used('"+so_tb+"'); ";
			//xs_ = xs_ + " 	commit; ";
			xs_ = xs_ + " end; ";
			System.out.println(xs_);
			//rs_  =  reqValue(xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String dangkygoicuoc(String so_tb,String idgoi){
		String rs_ = null;
			String xs_ = "";
		InetAddress ip;
		
		try{
			ip = InetAddress.getLocalHost();
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.dangkygoicuoc('"+so_tb+"','"+getUserVar("userID")+"','"+idgoi+"','"+getUserVar("sys_agentcode")+"','"+ip.getHostAddress()+"'); ";
			//xs_ = xs_ + " 	commit; ";
			xs_ = xs_ + " end; ";
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			//rs_  =  reqValue(xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String docLsTb(String so_tb_daydu,String page_num, String page_rec) {
		String x_ = "begin ? := admin_v2.dk_tbtt.get_log(?,?,?); end;";
		setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=tracuu/tc_dk_tb_tratruoc/lstb";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("page_num")+"="+page_num;
		url_ = url_ + "&"+CesarCode.encode("page_rec")+"="+page_rec;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	public String customer_kind(String so_tb){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.customer_kind_report('"+so_tb+"'); ";
			//xs_ = xs_ + " 	commit; ";
			xs_ = xs_ + " end; ";
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			//rs_  =  reqValue(xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	*/
	public String get_tonghop_tgsd_dv(String page_num, String page_rec,String msisdn,String diem,String chuyky) {
		String x_ = "begin ? := admin_v2.pk_diemtin_nhiem.get_diem_tin_nhiem(?,?,?,?,?,?); end;";
		setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=tracuu/diem_tin_nhiem/ds_tonghop_tgsd_dv_data";
		//url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("page_num")+"="+page_num;
		url_ = url_ + "&"+CesarCode.encode("page_rec")+"="+page_rec;
		url_ = url_ + "&"+CesarCode.encode("msisdn")+"="+msisdn;
		url_ = url_ + "&"+CesarCode.encode("diem")+"="+diem;
		url_ = url_ + "&"+CesarCode.encode("chuky")+"="+chuyky;
		url_ = url_ + "&"+CesarCode.encode("userID")+"="+getUserVar("userID");
		url_ = url_ + "&"+CesarCode.encode("agent_code")+"="+getUserVar("sys_agentcode");//HNI
		url_ = url_ + "&"+CesarCode.encode("schema")+"="+getUserVar("sys_dataschema");//ccs_hni.
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	

	
	public String get_diem_phanloai_kh(String ma_phanloai,String loaikh_id,String doanhthu_tu,String doanhthu_den, String diem, String agent,String is_group) {
		String x_ = "begin ? := admin_v2.pk_diemtin_nhiem.get_diem_phanloai_kh(?,?,?,?,?,?,?,?); end;";
		setUserVar("sqlDPL",x_);	
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=tracuu/diem_tin_nhiem/ds_diem_phanloai_kh";
		url_ = url_ + "&"+CesarCode.encode("agent_code")+"="+getUserVar("sys_agentcode");//HNI
		url_ = url_ + "&"+CesarCode.encode("ma_phanloai")+"="+ma_phanloai;
		url_ = url_ + "&"+CesarCode.encode("loaikh_id")+"="+loaikh_id;
		url_ = url_ + "&"+CesarCode.encode("doanhthu_tu")+"="+doanhthu_tu;
		url_ = url_ + "&"+CesarCode.encode("doanhthu_den")+"="+doanhthu_den;
		url_ = url_ + "&"+CesarCode.encode("diem")+"="+diem;
		url_ = url_ + "&"+CesarCode.encode("agent")+"="+agent;
		url_ = url_ + "&"+CesarCode.encode("userID")+"="+getUserVar("userID");
		url_ = url_ + "&"+CesarCode.encode("is_group")+"="+is_group;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	public String edit_diem_phanloai_kh(String id,String ma_phanloai,String loaikh_id,String doanhthu_tu,String doanhthu_den, String diem, String agent,String is_group){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.pk_diemtin_nhiem.edit_diem_phanloai_kh('"+id+"','"+ma_phanloai+"','"+loaikh_id+"','"+doanhthu_tu+"','"+doanhthu_den+"','"+diem+"','"+agent+"','"+getUserVar("userID")+"','"+is_group+"'); ";
			//xs_ = xs_ + " 	commit; ";
			xs_ = xs_ + " end; ";
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			//rs_  =  reqValue(xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String add_diem_phanloai_kh(String ma_phanloai,String loaikh_id,String doanhthu_tu,String doanhthu_den, String diem, String agent,String is_group){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.pk_diemtin_nhiem.add_diem_phanloai_kh('"+ma_phanloai+"','"+loaikh_id+"','"+doanhthu_tu+"','"+doanhthu_den+"','"+diem+"','"+agent+"','"+getUserVar("userID")+"','"+is_group+"'); ";
			//xs_ = xs_ + " 	commit; ";
			xs_ = xs_ + " end; ";
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			//rs_  =  reqValue(xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String del_diem_phanloai_kh(String id){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.pk_diemtin_nhiem.del_diem_phanloai_kh('"+id+"','"+getUserVar("userID")+"'); ";
			//xs_ = xs_ + " 	commit; ";
			xs_ = xs_ + " end; ";
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			//rs_  =  reqValue(xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String get_diem_tgtt() {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=tracuu/diem_tin_nhiem/ds_diem_tgtt_data";
		url_ = url_ + "&"+CesarCode.encode("agent_code")+"="+getUserVar("sys_agentcode");//HNI
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	public String edit_diem_tin_nhiem(String msisdn,String diem_b1, String diem_b2,String diem_b3,String ghichu,String chuky){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.pk_diemtin_nhiem.edit_diem_tin_nhiem('"+msisdn+"','"+diem_b1+"','"+diem_b2+"','"+diem_b3+"','"+ghichu+"','"+getUserVar("userID")+"','"+getUserVar("sys_dataschema")+"','"+chuky+"'); ";
			//xs_ = xs_ + " 	commit; ";
			xs_ = xs_ + " end; ";
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			//rs_  =  reqValue(xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String get_diem_tgsd() {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=tracuu/diem_tin_nhiem/ds_diem_tgsd_data";
		url_ = url_ + "&"+CesarCode.encode("agent_code")+"="+getUserVar("sys_agentcode");//HNI
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	public String add_tgtt(String tg_from,String tg_to, String diem){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.pk_diemtin_nhiem.add_tgtt('"+tg_from+"','"+tg_to+"','"+diem+"','"+getUserVar("userID")+"','"+getUserVar("sys_agentcode")+"'); ";
			//xs_ = xs_ + " 	commit; ";
			xs_ = xs_ + " end; ";
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			//rs_  =  reqValue(xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String add_tgsd(String tg_from,String tg_to, String diem){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.pk_diemtin_nhiem.add_tgsd('"+tg_from+"','"+tg_to+"','"+diem+"','"+getUserVar("userID")+"','"+getUserVar("sys_agentcode")+"'); ";
			//xs_ = xs_ + " 	commit; ";
			xs_ = xs_ + " end; ";
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			//rs_  =  reqValue(xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	
	public String edit_tgsd(String id,String tg_from,String tg_to, String diem){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.pk_diemtin_nhiem.edit_tgsd('"+id+"','"+tg_from+"','"+tg_to+"','"+diem+"','"+getUserVar("userID")+"','"+getUserVar("sys_agentcode")+"'); ";
			//xs_ = xs_ + " 	commit; ";
			xs_ = xs_ + " end; ";
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			//rs_  =  reqValue(xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}	
	
	public String edit_tgtt(String id,String tg_from,String tg_to, String diem){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.pk_diemtin_nhiem.edit_tgtt('"+id+"','"+tg_from+"','"+tg_to+"','"+diem+"','"+getUserVar("userID")+"','"+getUserVar("sys_agentcode")+"'); ";
			//xs_ = xs_ + " 	commit; ";
			xs_ = xs_ + " end; ";
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			//rs_  =  reqValue(xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String get_loai_kh(String id) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=tracuu/diem_tin_nhiem/ds_kh_id";
		url_ = url_ + "&"+CesarCode.encode("id")+"="+id;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	public String del_tgtt(String id){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.pk_diemtin_nhiem.del_tgtt('"+id+"','"+getUserVar("userID")+"'); ";
			//xs_ = xs_ + " 	commit; ";
			xs_ = xs_ + " end; ";
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			//rs_  =  reqValue(xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String del_tgsd(String id){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.pk_diemtin_nhiem.del_tgsd('"+id+"','"+getUserVar("userID")+"'); ";
			//xs_ = xs_ + " 	commit; ";
			xs_ = xs_ + " end; ";
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			System.out.println(xs_);
			//rs_  =  reqValue(xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String get_lichsu_diempl_kh(String page_num, String page_rec,String txt_search,String diem) {
		String x_ = "begin ? := admin_v2.pk_diemtin_nhiem.get_lichsu_diempl_kh(?,?,?,?,?); end;";
		setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=tracuu/diem_tin_nhiem/lichsu_diempl_kh";
		//url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("page_num")+"="+page_num;
		url_ = url_ + "&"+CesarCode.encode("page_rec")+"="+page_rec;
		url_ = url_ + "&"+CesarCode.encode("txt_search")+"="+txt_search;
		url_ = url_ + "&"+CesarCode.encode("diem")+"="+diem;
		url_ = url_ + "&"+CesarCode.encode("userID")+"="+getUserVar("userID");
		url_ = url_ + "&"+CesarCode.encode("agent_code")+"="+getUserVar("sys_agentcode");//HNI
		url_ = url_ + "&"+CesarCode.encode("schema")+"="+getUserVar("sys_dataschema");//ccs_hni.
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
		public String get_lichsu_tgts_tgtt(String page_num, String page_rec,String txt_search,String type) {
		String x_ = "begin ? := admin_v2.pk_diemtin_nhiem.get_lichsu_tgsd_tgtt(?,?,?,?,?); end;";
		setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=tracuu/diem_tin_nhiem/lichsu_tgts_tgtt";
		//url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("page_num")+"="+page_num;
		url_ = url_ + "&"+CesarCode.encode("page_rec")+"="+page_rec;
		url_ = url_ + "&"+CesarCode.encode("txt_search")+"="+txt_search;
		url_ = url_ + "&"+CesarCode.encode("type")+"="+type;
		url_ = url_ + "&"+CesarCode.encode("userID")+"="+getUserVar("userID");
		url_ = url_ + "&"+CesarCode.encode("agent_code")+"="+getUserVar("sys_agentcode");//HNI
		url_ = url_ + "&"+CesarCode.encode("schema")+"="+getUserVar("sys_dataschema");//ccs_hni.
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	public String get_lichsu_diemtinnhiem(String page_num, String page_rec,String txt_search) {
		String x_ = "begin ? := admin_v2.pk_diemtin_nhiem.get_lichsu_diemtinnhiem(?,?,?,?); end;";
		setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=tracuu/diem_tin_nhiem/lichsu_diemtinnhiem";
		//url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("page_num")+"="+page_num;
		url_ = url_ + "&"+CesarCode.encode("page_rec")+"="+page_rec;
		url_ = url_ + "&"+CesarCode.encode("txt_search")+"="+txt_search;
		url_ = url_ + "&"+CesarCode.encode("userID")+"="+getUserVar("userID");
		url_ = url_ + "&"+CesarCode.encode("agent_code")+"="+getUserVar("sys_agentcode");//HNI
		url_ = url_ + "&"+CesarCode.encode("schema")+"="+getUserVar("sys_dataschema");//ccs_hni.
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	
}