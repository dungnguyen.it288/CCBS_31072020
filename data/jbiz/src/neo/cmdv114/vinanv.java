package neo.cmdv114;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

import cosaldap.Base64.*;

import java.util.*;
import javax.sql.*;

import java.net.*;
import java.io.*;

public class vinanv extends NEOProcessEx
{

	public NEOCommonData layTTThe(String so_the)
	{
		String so_the_ = so_the.replace("'","''");
		String uid_ = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");

		String xs_ = "";
		xs_ = xs_ + "begin ?:= PKG_LVP.laytt_huythe('"+so_the_+"','"+uid_+"','GPC','"+matinh_ngdung_+"'); end;";
        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("LVP_DBMS");		

		return new NEOCommonData(ei_);
	}
	public String bc_nangcap_km(String psnguoith,String pstungay,String psdenngay) {
    return "/main?"+CesarCode.encode("configFile")+"=report/BC_PTTB/bc_nangcap_km/bc_nangcap_km_ajax"
        +"&"+CesarCode.encode("nguoi_dung") + "=" + psnguoith
		+"&"+CesarCode.encode("tu_ngay") + "=" + pstungay
		+"&"+CesarCode.encode("den_ngay") + "=" + psdenngay
		;
	}
	public String docBctkCDL_ALL_(String tu_ngay,String userid) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=report/service_rpts/bctk_slcttbcatmo_CDL_ALL_ajax_"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&"+CesarCode.encode("ma_nguoidung")+"="+userid
			+ "&sid="+Math.random();
		return url_;
	}
	public NEOCommonData huythe(String so_the)
	{
		String so_the_ = so_the.replace("'","''");
		String uid_ = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		
		String xs_ = "";
		xs_ = xs_ + "begin ?:= PKG_LVP.capnhat_huythe('"+so_the_+"','"+uid_+"','GPC','"+matinh_ngdung_+"'); end;";

        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("LVP_DBMS");		

		return new NEOCommonData(ei_);
	}	

	public String docDsHuyThe(String serial, String tu_ngay, String den_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dshuythe";
		url_ = url_ + "&"+CesarCode.encode("serial")+"="+serial;
		url_ = url_ + "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay;
		url_ = url_ + "&"+CesarCode.encode("den_ngay")+"="+den_ngay;
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}

	///NEW_SPS
	public NEOCommonData dmDV(String so_tb, String dsDvuThaydoi, String ghichu, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String dsDvuThaydoi_ = dsDvuThaydoi;//.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		String uid_ = getUserVar("userID");
		
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + "  ? := subadmin.pkg_nvucsc_services.dong_mo_ds_dvu('"+so_tb_+"','"+dsDvuThaydoi_+"','"+ghichu_+"','"+uid_+"','"+getUserVar("sys_agentcode")+"'); ";
		xs_ = xs_ + " end; ";
	
		
		String rs_ = null;
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}
	public NEOCommonData layTTThueBao_esim(String so_tb)
	{
		String so_tb_ = so_tb.replace("'","''");		
		String matinh_ngdung_ = getUserVar("sys_agentcode");

		String xs_ = "";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	? := subadmin.get_subsiminfo_func('"+so_tb_+"'); ";
		xs_ = xs_ + " end; ";

		RowSet rs_ = null;
		try{
			rs_ = reqRSet("CMDVOracleDS", xs_);
			rs_ = reqRSet("", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return new NEOCommonData(rs_);	
	}
	///NEW_SPS
	public NEOCommonData chtb(String so_tb, String loai_moi, String tinh_moi, String ghichu, String uid)
	{
		String decloai_moi_ = "";
		String dectinh_moi_ = "";

		String so_tb_ = so_tb.replace("'","''");
		String loai_moi_ = loai_moi.replace("'","''");
		String tinh_moi_ = tinh_moi.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		String uid_ = getUserVar("userID");
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		//xs_ = xs_ + " 	? := subadmin.PKG_NVUCSC_sps.chdoi_lhtb('"+so_tb+"','"+loai_moi+"','"+tinh_moi+"','"+ghichu+"','"+uid+"','"+getUserVar("sys_agentcode")+"'); ";
		xs_ = xs_ + " 	? := subadmin.pkg_change_msisdn_ccbs.chdoi_lhtb('"+so_tb+"','"+loai_moi+"','"+tinh_moi+"','"+ghichu+"','"+uid+"','"+getUserVar("sys_agentcode")+"'); ";
		xs_ = xs_ + " end; ";

		String rs_ = null;
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("dongnd cdlh: "+xs_);
		return new NEOCommonData(rs_);
	}


	public String layds_loaimoi(String so_tb, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String uid_ = getUserVar("userID");

		String xs_ = "";
		xs_ = xs_ + " <select class=textField name='-tinhmoi' id='-tinhmoi' ";
		//xs_ = xs_ + " 	sql='begin ?:=subadmin.pkg_nvucsc.chdoi_lhtb_layds_loai_moi('"+so_tb_+"','"+uid_+"'); end;' ";
		xs_ = xs_ + " 	sql='begin ?:=subadmin.pkg_change_msisdn_ccbs.chdoi_lhtb_layds_loai_moi('"+so_tb_+"','"+uid_+"'); end;' ";
		xs_ = xs_ + " 	style='background-color :#f1e8e6'  dbms-font='VN8VN3' ";
		xs_ = xs_ + " 	notusename='true' />";

		return xs_;
	}
	
	public String layds_tinhmoi(String so_tb, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String uid_ = uid.replace("'","''");
	
		String xs_ = "";
		xs_ = xs_ + " <select class=textField name='-tinhmoi' id='-tinhmoi' ";
		xs_ = xs_ + " 	sql='begin ?:=subadmin.pkg_nvucsc.chdoi_lhtb_layds_tinh_moi('"+so_tb_+"','"+uid_+"'); end;' ";
		xs_ = xs_ + " 	style='background-color :#f1e8e6'  dbms-font='VN8VN3' ";
		xs_ = xs_ + " 	notusename='true' />";

		return xs_;
	}
	///NEW_SPS
		public NEOCommonData dsimtb(String so_tb, String so_msin_moi, int simfree, String ghichu, String uid, String thuphi)
	{
		String so_tb_ = so_tb.replace("'","''");
		String so_msin_moi_ = so_msin_moi.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		String thuphi_ = thuphi.replace("'","''");
		String uid_ = getUserVar("userID");
	
		String xs_ = "";
		xs_ = xs_ + " begin ";
		//xs_ = xs_ + " 	? := subadmin.pkg_changesim_ccbs.doi_sim_tb('"+so_tb_+"','"+so_msin_moi_+"',"+simfree+",'"+ghichu_+"','"+uid_+"','"+getUserVar("sys_agentcode")+"'); ";
		xs_ = xs_ + " 	? := neo.pkg_changesim_qlsp_ccbs.doi_sim_tbtp('"+so_tb_+"','"+so_msin_moi_+"',"+simfree+",'"+ghichu_+"','"+uid_+"','"+thuphi_+"','"+getUserVar("sys_agentcode")+"'); ";
		xs_ = xs_ + " end; ";
		System.out.println(xs_);
		String rs_ = null;
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}

	public NEOCommonData chkSMOI(String so_tb, String so_msin_moi)
	{
		String so_tb_ = so_tb.replace("'","''");
		String so_msin_moi_ = so_msin_moi.replace("'","''");
		String matinh_ngdung_ = getUserVar("sys_agentcode");

		String xs_ = "";
		xs_ = xs_ + " begin ";
		//NATuan: Comment tich hop QLSP
		//xs_ = xs_ + " 	? := subadmin.pkg_nvucsc.doi_sim_ktra_SIM_moi('"+so_tb_+"','"+so_msin_moi_+"','"+matinh_ngdung_+"'); ";
		xs_ = xs_ + " 	? := 	neo.pkg_changesim_qlsp_ccbs.doi_sim_ktra_E_SIM_moi('"+so_tb_+"','"+so_msin_moi_+"','"+matinh_ngdung_+"'); ";
		xs_ = xs_ + " end; ";

		RowSet rs_ = null;
		try{
			rs_ = reqRSet("CMDVOracleDS", xs_);			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);		
	}
/*update by : dongnd 
   Date: 07/10/2008
   Description: Them bien dau vao ma tinh de xu ly truong hop thue bao hoa mang moi chuyen tinh ( ma tinh vao GPC)
*/
	public NEOCommonData khoitao_AC(String so_tb, String ghichu, String uid, String psma_tinh)
	{
		String so_tb_ = so_tb.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		String uid_ = getUserVar("userID");

		String xs_ = "";
		xs_ = xs_ + " begin ";
		//xs_ = xs_ + " 	? := subadmin.pkg_nvucsc.khoitao_AC('"+so_tb_+"','"+ghichu_+"','"+uid_+"','"+psma_tinh+"'); ";
		xs_ = xs_ + " 	? := subadmin.pkg_initialize_subscribers.khoitao_AC('"+so_tb_+"','"+ghichu_+"','"+uid_+"','"+psma_tinh+"'); ";
		
		//xs_ = xs_ + " 	commit; ";
		xs_ = xs_ + " end; ";

		String rs_ = null;
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}
	
/*update by : dongnd 
   Date: 07/10/2008
   Description: Them bien dau vao ma tinh de xu ly truong hop thue bao hoa mang moi chuyen tinh ( ma tinh nguoi dung la GPC)
*/
	public NEOCommonData khoitao_laiTB(String so_tb, String loai_moi, String tinh_moi, String ghichu, String uid, String psma_tinh)
	{
		String so_tb_ = so_tb.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		String uid_ = getUserVar("userID");

		String decloai_moi_ = "";
		String dectinh_moi_ = "";

		decloai_moi_ = loai_moi;
		dectinh_moi_ = tinh_moi;
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		//xs_ = xs_ + " 	? := subadmin.pkg_nvucsc.khoitao_laiTB('"+so_tb_+"','"
		xs_ = xs_ + " 	? := subadmin.pkg_initialize_subs_ccbs.khoitao_laiTB_CCBS('"+so_tb_+"','"
			+decloai_moi_+"','"+dectinh_moi_+"','"+ghichu_+"','"+uid_+"','"+psma_tinh+"'); ";
		//xs_ = xs_ + " 	commit; ";
		xs_ = xs_ + " end; ";
		
		String rs_ = null;
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}

	

	public String doitenTb(String so_tb, String tentb_moi)
	{
		String so_tb_ = so_tb.replace("'","''");
		String tentb_moi_ = tentb_moi.replace("'","''");
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc.cat_mo_cap_nhat_ten('"+so_tb_+"','"+tentb_moi_+"'); ";
		//xs_ = xs_ + " 	commit; ";
		xs_ = xs_ + " end; ";
		//System.out.println("ABC"+ xs_);
		return xs_;
	}

	public String doiDchiTb(String so_tb, String dchi_moi)
	{
		String so_tb_ = so_tb.replace("'","''");
		String dchi_moi_ = dchi_moi.replace("'","''");

		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc.cat_mo_cap_nhat_dia_chi('"+so_tb+"','"+dchi_moi_+"'); ";
		//xs_ = xs_ + " 	commit; ";
		xs_ = xs_ + " end; ";

		return xs_;
	}
	
	///NEW_SPS
	public NEOCommonData catmoICOC(String trth, String goidi, String goiden, String so_tb, String ma_lydo, String ghichu, String uid)
	{
		String ghichu_ = ghichu.replace("'","''");
		String decMa_lydo_ = CesarCode.decode(ma_lydo).trim();
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		String xs_ = "";
		String rs_ =null;
		String xs1_ = "";
		String rs1_ =null;
		//Ghi log cat mo IC OC TRINHHV 13/01/2014
		boolean blnCheck;
		try {
			blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"catmoICOC", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return new NEOCommonData(rs_);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (getUserVar("userID") == null){
			xs_ = "begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
								+so_tb+"','catmoICOC'," 
								+"'"+trth+";"+goidi+";"+goiden+";"+so_tb+";"+ma_lydo+";"+ghichu+"','"
								+uid+"','"
								+getUserVar("userID")+"','"
								+getEnvInfo().getParserParam("sys_userip")+"','"
								+getUserVar("sys_agentcode")+"'); "
								+ "   commit; "
								+ " end; ";		
			try{
				rs_ = reqValue(xs_);
			} catch (Exception ex) {
				ex.printStackTrace();	
			}
		}
		else if(!getUserVar("userID").equals(uid)){
			xs_ = "begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
							+so_tb+"','catmoICOC'," 
							+"'"+trth+";"+goidi+";"+goiden+";"+so_tb+";"+ma_lydo+";"+ghichu+"','"
							+uid+"','"
							+getUserVar("userID")+"','"
							+getEnvInfo().getParserParam("sys_userip")+"','"
							+getUserVar("sys_agentcode")+"'); "
							+ "   commit; "
							+ " end; ";		
			try {
				rs_ = reqValue(xs_);
			} catch (Exception ex) {
				ex.printStackTrace();	
			}
		}

		//kiem tra trang thai danh ba theo nghiep vu HCM: neu tb dang Khoa do no dong se khong duoc mo
		
		//if (matinh_ngdung_.equals("HCM")||matinh_ngdung_.equals("HNI")||matinh_ngdung_.equals("QNH")||matinh_ngdung_.equals("QNI")||matinh_ngdung_.equals("PTO")) {
			xs_ = " begin "
			+ " 	? := ADMIN_V2.PTTB_CHUCNANG.laytt_trangthaitb('"
				+so_tb+"','"+getUserVar("sys_dataschema")+"'); "
			+ "   commit; "
			+ " end; ";		
		
			try{
				rs_ = reqValue(xs_);
				if (rs_.equals("3") && (goidi.equals("1") || goiden.equals("1"))) {  //khoa 1 chieu do no cuoc
					return new NEOCommonData("HCM-1C-CUOC" );		
				}	
				
				if (rs_.equals("5") && (goidi.equals("1") || goiden.equals("1"))) {  //khoa 2 chieu do no cuoc
					return new NEOCommonData("HCM-2C-CUOC");		
				}	
			} catch (Exception ex) {
				ex.printStackTrace();	
				return new NEOCommonData("Loi khi khoa mo ICOC");
			}
		//}

		
		//Cap nhat lai trang thai danh ba
		xs1_ = " begin "
			+ " 	? := ADMIN_V2.PTTB_CHUCNANG.dong_bo_trang_thai_db('"
				+trth+"','"+goidi+"','"+goiden+"','"+so_tb+"','"
				+decMa_lydo_+"','"+ghichu_+"','"+getUserVar("userID")+"','"
				+getUserVar("sys_agentcode")+"',0,1); "
			+ "   commit; "
			+ " end; "
		;		
		
		try{
			rs1_ = reqValue(xs1_);
		} catch (Exception ex) {
			ex.printStackTrace();	
			rs1_ ="0";		
		}
		
		if (rs1_=="0") {
			return new NEOCommonData("Loi khong dong bo duoc trang thai danh ba.");		
		}
		
		xs_ = " begin "
			+ " 	? := subadmin.pkg_nvucsc_services.cat_mo_OC_IC_huy_HD2('"
				+trth+"','"+goidi+"','"+goiden+"','"+so_tb+"','"
				+decMa_lydo_+"','"+ghichu_+"','"+getUserVar("userID")+"','"
				+getUserVar("sys_agentcode")+"'); "
			+ "   commit; "
			+ " end; "
		;	
		System.out.println(xs_);
		
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();			
		}
		
		return new NEOCommonData(rs_);
	}
	///////
	public NEOCommonData dkyGoiIDD1714(String trth, String so_tb, String ma_goi, String ghichu, String uid, String uip)
	{
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		String sDataSchema_ = getUserVar("sys_dataschema");
		String sfuncschema_ = getSysConst("FuncSchema");	
		String ghichu_ = ghichu.replace("'","''");
		String goi_moi_ = ma_goi;

		//kiem tra  loai khach hang ca nhan 	
				
			String xs_ = "";
			xs_ = " begin "
				+ " 	? := ccs_admin.pttb_tracuu.laytt_doituong_tb('"+ sDataSchema_+"','"+matinh_ngdung_+"','','"+so_tb+"'); "
				+ "   commit; "
				+ " end; " ;
			
			String rs_ = null;
			try{
				rs_ = reqValue( xs_);
			} catch (Exception ex) {
				ex.printStackTrace();
				rs_="0";
			}	
		// kiem tra trang thai HUY thi khong check doi tuong khach hang		
		if (!trth.equals("2"))	
		{
			if (!rs_.equals("1")) {
				return new NEOCommonData("6");			
			}
		}
		
		xs_ = " begin "
			+ " 	? := subadmin.pkg_idd1714.dky_idd1714('"
				+ trth+"','"+so_tb+"','"+goi_moi_+"','"+ghichu_+"','"
				+getUserVar("userID")+"','"+uip+"'); "
			+ "   commit; "
			+ " end; "
		;		
		
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}
	
	public String docDsGoiIDD1714(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dsgoiidd1714";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		System.out.println(">>>>>>>>>");
		return url_;
	}
	
	public NEOCommonData dkyGoiGPRS(String trth, String so_tb, String ma_goi, String ghichu, String uid)
	{
		String ghichu_ = ghichu.replace("'","''");
		String goi_moi_ = ma_goi;
	
		String xs_ = "";
		xs_ = " begin "
			+ " 	? := subadmin.pkg_nvucsc.dky_goi_gprs('"
				+ trth+"','"+so_tb+"','"+goi_moi_+"','"+ghichu_+"','"
				+getUserVar("userID")+"'); "
			+ "   commit; "
			+ " end; "
		;
		System.out.println(xs_);
		String rs_ = null;
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}
	
	
	public String docDvDky(String so_tb_daydu) {		
		String url_ ="/main";
		
		try{
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"mod_dmdv_tb", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return  url_ + "?"+CesarCode.encode("configFile")+"=admin/not-access";
			reqValue("begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
						+so_tb_daydu+"','mod_dmdv_tb'," 
						+"'"+so_tb_daydu+";',"
						+"'','"
						+getUserVar("userID")+"','"
						+getEnvInfo().getParserParam("sys_userip")+"','"
						+getUserVar("sys_agentcode")+"'); "
						+ "   commit; "
						+ " end; ");
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dmdv_tb";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	
	public String docDkyDv() {
		String url_ ="/main";
		
		try{
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"mod_dkydv", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return  url_ + "?"+CesarCode.encode("configFile")+"=admin/not-access";
			reqValue("begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('','mod_dkydv'," 
						+"'',"
						+"'','"
						+getUserVar("userID")+"','"
						+getEnvInfo().getParserParam("sys_userip")+"','"
						+getUserVar("sys_agentcode")+"'); "
						+ "   commit; "
						+ " end; ");
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dkydv";
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}

	public String docHotBill(String so_tb_daydu, String thang, String nam) {
		String url_ ="/main";
		
		try{
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"mod_ttcuoc_dd", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return  url_ + "?"+CesarCode.encode("configFile")+"=admin/not-access";
			reqValue("begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
						+so_tb_daydu+"','mod_ttcuoc_dd'," 
						+"'"+so_tb_daydu+";',"
						+"'','"
						+getUserVar("userID")+"','"
						+getEnvInfo().getParserParam("sys_userip")+"','"
						+getUserVar("sys_agentcode")+"'); "
						+ "   commit; "
						+ " end; ");
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_ttcuoc_dd";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("thang")+"="+thang;
		url_ = url_ + "&"+CesarCode.encode("nam")+"="+ nam;	
        return url_;		
	}
	
	public String docHotBill_HD(String so_tb_daydu, String thang, String nam) {
		String url_ ="/main";
		
		try{
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"mod_ttcuoc_hddd", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return  url_ + "?"+CesarCode.encode("configFile")+"=admin/not-access";
			reqValue("begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
						+so_tb_daydu+"','mod_ttcuoc_hddd'," 
						+"'"+so_tb_daydu+";',"
						+"'','"
						+getUserVar("userID")+"','"
						+getEnvInfo().getParserParam("sys_userip")+"','"
						+getUserVar("sys_agentcode")+"'); "
						+ "   commit; "
						+ " end; ");
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_ttcuoc_hddd";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("thang")+"="+thang;
		url_ = url_ + "&"+CesarCode.encode("nam")+"="+ nam;	
        return url_;		
	}
	
	public NEOCommonData docCuocDaily(String url) {
		String url_= "http://190.10.10.86"+url;
		String doc_ = null;		
		try
		{
			java.net.URL cuocDoc_ = new java.net.URL(url_);
			BufferedReader in = new BufferedReader(
				new InputStreamReader(
				cuocDoc_.openStream()));

			String inputLine;
			StringBuilder sb_ = new StringBuilder();

			while ((inputLine = in.readLine()) != null) {
				sb_.append(inputLine);
			}
			in.close();		
		
			doc_ = sb_.toString();
		} catch (Exception ex) {
			doc_ = ex.toString();
		}
		return new NEOCommonData(doc_);	
	}
	
	public String docDvuTb(String so_tb_daydu) {
		String x_ = "begin ?:= subadmin.PKG_NVUCSC.layds_dvutb(?); end;";
		setUserVar("sqlDvuTb",x_);	
		String url_ ="/main";
		//Ghi log lich su TB 30/01/2014
		try{
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"mod_dvu_tb", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return  url_ + "?"+CesarCode.encode("configFile")+"=admin/not-access";
			reqValue("begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
						+so_tb_daydu+"','mod_dvu_tb'," 
						+"'"+so_tb_daydu+";',"
						+"'','"
						+getUserVar("userID")+"','"
						+getEnvInfo().getParserParam("sys_userip")+"','"
						+getUserVar("sys_agentcode")+"'); "
						+ "   commit; "
						+ " end; ");
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dvu_tb";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
    public String docLsTb(String so_tb_daydu,String page_num, String page_rec) {
		//String x_ = "begin ? := subadmin.LAYDS_LSU_THUEBAO(?,?,?); end;";
		String x_ = "begin ? := subadmin.layds_lsu_thuebao_chuyendoi(?,?,?); end;";
		setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		
		//Ghi log lich su TB 30/01/2014
		try{
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"mod_lstb1", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return  url_ + "?"+CesarCode.encode("configFile")+"=admin/not-access";
			reqValue("begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
						+so_tb_daydu+"','mod_lstb1'," 
						+"'"+so_tb_daydu+";"+page_num+";"+page_rec+";',"
						+"'','"
						+getUserVar("userID")+"','"
						+getEnvInfo().getParserParam("sys_userip")+"','"
						+getUserVar("sys_agentcode")+"'); "
						+ "   commit; "
						+ " end; ");
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_lstb1";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("page_num")+"="+page_num;
		url_ = url_ + "&"+CesarCode.encode("page_rec")+"="+page_rec;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	public String docLsTb_itc(String so_tb_daydu,String page_num, String page_rec) {
		//String x_ = "begin ? := subadmin.PKG_UTILS.LAYDS_LSU_THUEBAO(?,?,?); end;";
		String x_ = "begin ? := subadmin.pkg_itc.LAYDS_LSU_THUEBAO(?,?,?); end;";
		setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		
		//Ghi log lich su TB 30/01/2014
		try{
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"mod_lstb1", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return  url_ + "?"+CesarCode.encode("configFile")+"=admin/not-access";
			reqValue("begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
						+so_tb_daydu+"','mod_lstb1'," 
						+"'"+so_tb_daydu+";"+page_num+";"+page_rec+";',"
						+"'','"
						+getUserVar("userID")+"','"
						+getEnvInfo().getParserParam("sys_userip")+"','"
						+getUserVar("sys_agentcode")+"'); "
						+ "   commit; "
						+ " end; ");
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/itc/mod_lstb_itc";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("page_num")+"="+page_num;
		url_ = url_ + "&"+CesarCode.encode("page_rec")+"="+page_rec;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docLsTb(String so_tb_daydu) {
		//String x_ = "begin ? := subadmin.PKG_UTILS.LAYDS_LSU_THUEBAO(?); end;";
		String x_ = "begin ? := subadmin.LAYDS_LSU_THUEBAO_V2(?); end;";
		setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		
		//Ghi log lich su TB 30/01/2014
		try{
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"mod_lstb", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return  url_ + "?"+CesarCode.encode("configFile")+"=admin/not-access";
			reqValue("begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
						+so_tb_daydu+"','mod_lstb'," 
						+"'"+so_tb_daydu+";',"
						+"'','"
						+getUserVar("userID")+"','"
						+getEnvInfo().getParserParam("sys_userip")+"','"
						+getUserVar("sys_agentcode")+"'); "
						+ "   commit; "
						+ " end; ");
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_lstb";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docLsNo(String so_tb_daydu, String sp_name) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_ttlsno";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("sp_name")+"=VNSG";
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docDsGoiGPRS(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dsgoigprs";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		System.out.println(">>>>>>>>>");
		return url_;
	}

	public String docDsLoaiMoi(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dsloaimoi";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docDsTinhMoi(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dstinhmoi";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docDsLoaiDvuCMTF(int thao_tac) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dsdvu_cmtf";
		url_ = url_ + "&"+CesarCode.encode("thao_tac")+"="+thao_tac;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	public String docDsLoaiHD(String so_tb_daydu, String sp_name) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=xdc/mod_ds_loaihd";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("sp_name")+"=VNSG";
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docDsMaHD(String so_tb_daydu, String ma_loaihd, String sp_name) {
		String dec_ma_loai_hd_  = CesarCode.decode(ma_loaihd);
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=xdc/mod_ds_mahd";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("ma_loaihd")+"="+dec_ma_loai_hd_;
		url_ = url_ + "&"+CesarCode.encode("sp_name")+"=VNSG";
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	public String docDsLoaiKP(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dsloaiKP";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docDsTinhKP(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dstinhKP";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}	
	public String layTTSIM(String sim)
	{
		String sim_ = sim.replace("'","''");
		String uid_ = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");

		String xs_ = "";
		xs_ = xs_ + "begin ?:= subadmin.PKG_NVUCSC.khoitaoAC_ktraSIM('"+sim_+"','"+uid_+"','"+matinh_ngdung_+"'); end;";

		return xs_;
	}

	public String khoitaoAC(String sim, String ghi_chu)
	{
		String sim_ = sim.replace("'","''");
		String ghi_chu_ = ghi_chu.replace("'","''");
		String uid_ = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		
		String xs_ = "";
		xs_ = xs_ + "begin ?:= subadmin.PKG_NVUCSC.khoitaoAC_SIM('"+sim_+"','"
			+ghi_chu_+"','"+uid_+"','"+matinh_ngdung_+"'); end;";

		return xs_;
	}	

	public String docDkyDv_FullW() {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dkydv_fullw";
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}

	public String docBctkTienTb2Post(String so_tb, String tu_ngay, String den_ngay, String trg_sx) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=report/service_rpts/bctk_slct_tientb2post_ajax"
			+ "&"+CesarCode.encode("so_tb")+"="+so_tb
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&"+CesarCode.encode("den_ngay")+"="+den_ngay
			+ "&"+CesarCode.encode("trg_sx")+"="+trg_sx
			+ "&sid="+Math.random();
		return url_;
	}	
	
	public String docBctkCDL_S2T(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=report/service_rpts/bctk_slcttbcatmo_CDL_S2T_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&sid="+Math.random();
		return url_;
	}	

	public String docBctkCDL_T2S(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=report/service_rpts/bctk_slcttbcatmo_CDL_T2S_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&sid="+Math.random();
		return url_;
	}	
	
	public String docBctkCDL_ALL(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbcatmo_CDL_ALL_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&sid="+Math.random();
		return url_;
	}		
	
	public String docBctkCAN(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=report/service_rpts/bctk_slcttbcatmo_CAN_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&sid="+Math.random();
		return url_;
	}		
	
	public String docBctkSIM(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbcatmo_SIM_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&sid="+Math.random();
		return url_;
	}		
	public String docBctkSIM_(String tu_ngay,String user_id, String loai_tb) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=report/service_rpts/bctk_slcttbcatmo_SIM_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&"+CesarCode.encode("user_nguoidung")+"="+user_id
			+ "&"+CesarCode.encode("loai_tb")+"="+loai_tb
			+ "&sid="+Math.random();
		return url_;
	}	
	public String docBctk_IC_OC(String tu_ngay, String loaitb) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=report/service_rpts/bctk_slcttbcatmo_IC_OC_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&"+CesarCode.encode("loaitb")+"="+loaitb			
			+ "&sid="+Math.random();
		return url_;
	}	
	
	public String docBctkALL(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=report/service_rpts/bctk_slcttbcatmo_ALL_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay				
			+ "&sid="+Math.random();
		return url_;
	}
	public String docBctkNEW(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=report/service_rpts/bctk_slcttbcatmo_NEW_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&sid="+Math.random();
		return url_;
	}	
	
	public String docBctkTTK2C(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbpdtk2ch_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&sid="+Math.random();
		return url_;
	}	

	public String docBctkTBKH(String tu_ngay, String den_ngay, String so_tb) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbkh_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&"+CesarCode.encode("den_ngay")+"="+den_ngay
			+ "&"+CesarCode.encode("so_tb")+"="+so_tb
			+ "&sid="+Math.random();
		
		return url_;
	}		
	
	public String docBctkTBNT(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbnt_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&sid="+Math.random();
		return url_;
	}
	public String docLsTb1(String so_tb_daydu) {
		//String x_ = "begin ? := subadmin.PKG_UTILS.LAYDS_LSU_THUEBAO(?); end;";
		String x_ = "begin ? := subadmin.LAYDS_LSU_THUEBAO_V2(?); end;";
		setUserVar("sqlLsTb",x_);	
		
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_lstb1";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}	
	

	public String docLsIDD1714(String so_tb,String tu_ngay, String den_ngay) {		
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_lsidd1714";
		url_ = url_ + "&"+CesarCode.encode("so_tb")+"="+so_tb;
		url_ = url_ + "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay;
		url_ = url_ + "&"+CesarCode.encode("den_ngay")+"="+den_ngay;	
		return url_;
	}
	
	public NEOCommonData check_hotbill(	     
		String psMatb, String psthang, String psnam	) {
		
    String s= "begin ? := ccs_common.check_hotbill('"		
		+psMatb
		+"','"+psthang
		+"','"+psnam
		+"');"
		+"end;"
        ;
    NEOExecInfo nei_ = new NEOExecInfo(s);
	nei_.setDataSrc("VNPBilling");
	return new NEOCommonData(nei_);
 }
	
public String dockh_bd(String so_tb_daydu) {
  String url_ ="/main";
  url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_khbd";
  url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
   url_ = url_ + "&sid="+Math.random();
  return url_;
 }
 
 /*  Doc danh sach dich vu GTGT */
	public String docDvGTGT(String so_tb_daydu) {		
		String url_ ="/main";
		
		try{
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"mod_dmdv_tb", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return  url_ + "?"+CesarCode.encode("configFile")+"=admin/not-access";
			reqValue("begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
						+so_tb_daydu+"','mod_dv_gtgt'," 
						+"'"+so_tb_daydu+";',"
						+"'','"
						+getUserVar("userID")+"','"
						+getEnvInfo().getParserParam("sys_userip")+"','"
						+getUserVar("sys_agentcode")+"'); "
						+ "   commit; "
						+ " end; ");
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=code/huy_dv_gtgt/mod_dv_gtgt";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("userid")+"="+getUserVar("userID");
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
 
 public NEOCommonData huy_dv_gtgt(String so_tb, String dsdv, String ghichu, String userip)
	{
		String uid_ = getUserVar("userID");
				
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + "  ? := admin_v2.pkg_can_vasc.can_service('"+so_tb+"','"+dsdv+"','"+ghichu+"','"+uid_+"','"+userip+"'); ";
		xs_ = xs_ + " end; ";
			
		String rs_ = null;
		try{
			rs_ = this.reqValue("", xs_);
		} catch (Exception ex) {}
		
		return new NEOCommonData(rs_);
	}
	
	public String doc3gTbnew(String psSoTB)
	{
		return "/main?" + CesarCode.encode("configFile")+"=vinacore/ajax_lichsu_3gs_future"
			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB;
	}
	
	public NEOCommonData check_so_itc(String so_tb)
	{
		String so_tb_ = so_tb.replace("'","''"); 
	
		String xs_ = "";
		xs_ = xs_ + " begin "; 
		xs_ = xs_ + " 	? := subadmin.pkg_itc.check_dau_itc('"+so_tb_+"'); ";
		xs_ = xs_ + " end; ";
		//System.out.println(xs_);
		String rs_ = null; 
		NEOExecInfo ei_ = new NEOExecInfo(xs_);
		ei_.setDataSrc("CMDVOracleDS");
		return new NEOCommonData(ei_);		
		
	}
	
	public NEOCommonData check_so_chuyendoi(String so_tb)
	{
		String so_tb_ = so_tb.replace("'","''"); 
	
		String xs_ = "";
		xs_ = xs_ + " begin "; 
		xs_ = xs_ + " 	? := subadmin.check_dau_chuyendoi('"+so_tb_+"'); ";
		xs_ = xs_ + " end; ";        
		String rs_ = null; 
		NEOExecInfo ei_ = new NEOExecInfo(xs_);        
		ei_.setDataSrc("CMDVOracleDS");
		return new NEOCommonData(ei_);		
	}
	
	public NEOCommonData dsimtb_itc(String so_tb, String so_msin_moi, int simfree, String ghichu, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String so_msin_moi_ = so_msin_moi.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		String uid_ = getUserVar("userID");
	
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_itc.doi_sim_tb('"+so_tb_+"','"+so_msin_moi_+"',"+simfree+",'"+ghichu_+"','"+uid_+"','"+getUserVar("sys_agentcode")+"'); ";
		xs_ = xs_ + " end; ";
        //System.out.println(xs_);
		String rs_ = null;
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}
 
}
