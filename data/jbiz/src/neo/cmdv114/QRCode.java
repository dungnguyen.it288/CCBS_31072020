package neo.cmdv114;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

import cosaldap.Base64.*;

import java.util.*;
import javax.sql.*;

import java.net.*;
import java.io.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.keepautomation.barcode.BarCode;
import com.keepautomation.barcode.IBarCode;

public class QRCode extends NEOProcessEx 
{
	
public NEOCommonData inQRCode(String QRString,String Barcodestr)
	{
		/*String qrCodeData = QRString;//"LPA:1$rsp-0002.oberthur.net$N1MBH-GC4ZK-I0ZJF-PUFBA";
		String filePath = "QRCode.png";
		String charset = "UTF-8"; // or "ISO-8859-1"
		*/
		
		BarCode barcode = new BarCode();
         String barcodeString=Barcodestr;
		 String barcodefilePath = "D:/_app/vnp-ccbs/website/_download/EsimBarCode.gif";
		 barcode.setCodeToEncode(barcodeString);
	     barcode.setSymbology(IBarCode.CODE128);
		 barcode.setX(2);
		 barcode.setY(50);
		 barcode.setRightMargin(0);
		 barcode.setLeftMargin(0);
		 barcode.setTopMargin(0);
		 barcode.setBottomMargin(0);
		 barcode.setChecksumEnabled(false);
		 barcode.setFnc1(IBarCode.FNC1_NONE);
	     

	     try
	     {     barcode.draw(barcodefilePath);

	     }
	     catch (Exception e) 
	     {
	     e.printStackTrace();
	     }
		String qrCodeData = QRString;
		//String filePath = "D:/_app/vnp-ccbs/data/upload/QRCode.gif";
		String qrcodefilePath = "D:/_app/vnp-ccbs/website/_download/QRCode.gif";
		
		barcode = new BarCode();

		 barcode.setCodeToEncode(qrCodeData);
		 barcode.setSymbology(IBarCode.QRCODE);
		 barcode.setQrCodeDataMode(IBarCode.QR_MODE_AUTO);
		 barcode.setQrCodeEcl(IBarCode.QR_ECL_L);
		 barcode.setQrCodeVersion(1);

		 try
		 {     barcode.draw(qrcodefilePath);

		 }
		 catch (Exception e) 
		 {
		 e.printStackTrace();
		 }
		String xs_= "begin ?:='"+qrcodefilePath+"'; end;"; 
		String rs_ = null;
		try{
			rs_ = reqValue("", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
			
		}
		
		return new NEOCommonData(rs_);

	}

}
