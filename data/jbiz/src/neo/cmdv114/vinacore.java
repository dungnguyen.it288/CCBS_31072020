package neo.cmdv114;

import neo.smartui.process.*;
import neo.smartui.report.*;
import javax.sql.*;
import java.util.concurrent.TimeUnit;
public class vinacore extends NEOProcessEx 
{
	public String chkTBTonTai(String so_tb)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_utils.CHECK_TB_TONTAI('"+so_tb+"'); ";
		xs_ = xs_ + " end; ";
		
		return xs_;
	} 

	public NEOCommonData layTTThueBao(String so_tb)
	{
		RowSet rs_ = null;
		try {
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"layTTThueBao", getUserVar("userID"),
					getUserVar("sys_agentcode"),"30|1|30");
			if(!blnCheck)
				return new NEOCommonData(rs_);
			reqValue("begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
					+so_tb+"','layTTThueBao'," 
					+"'"+so_tb+";',"
					+"'','"
					+getUserVar("userID")+"','"
					+getEnvInfo().getParserParam("sys_userip")+"','"
					+getUserVar("sys_agentcode")+"'); "
					+ "   commit; "
					+ " end; ");
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage()) ;
			// TODO: handle exception
		}
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_utils.LAYTT_THUEBAO(?); ";
		xs_ = xs_ + " end; ";
		
		
		
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		nei_.setDataSrc("CMDVOracleDS");
		nei_.bindParameter(2,so_tb);
		
		
		try{
			rs_ = reqRSet(nei_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println(xs_);
		
		return new NEOCommonData(rs_);

	} 	
		
	
	
/*	
	public String layTTKhTb(String so_tb)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := vinaxdc.xdc_tracuu.laytt_khtb('"+so_tb+"'); ";
		xs_ = xs_ + " end; ";

		return xs_;
	} 	
*/
	public String layTTKhTb(String so_tb, String ma_tinh)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := pttb_ccbs.laytt_khtb('"+so_tb+"','"+ma_tinh+"'); ";
		xs_ = xs_ + " end; ";

		return xs_;
	}
	public String layTTNoCuoi(String so_tb)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := vinaxdc.xdc.laytt_nocuoi('"+so_tb+"'); ";
		xs_ = xs_ + " end; ";

		return xs_;
	} 	
	//NATuan 31/10/2016: Dang ky thue bao cho goi NVKD dia ban VNPT
	public String reg_vplus_pps(String psmsisdn, String pspackage, String psuserip)
	{		
		String _userid = getUserVar("userID");
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= admin_v2.pkg_cskh_pps.reg_vplus_pps('"+psmsisdn+"','"+pspackage+"','"+_userid+"','"+psuserip+"'); ";
		xs_ = xs_ + " end; ";			
		return xs_;		
	}
	
	public NEOCommonData layTTThueBao_itc(String so_tb)
	{
		RowSet rs_ = null;
		try {
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"layTTThueBao_v3", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return new NEOCommonData(rs_);
			reqValue("begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
					+so_tb+"','layTTThueBao_itc'," 
					+"'"+so_tb+";',"
					+"'','"
					+getUserVar("userID")+"','"
					+getEnvInfo().getParserParam("sys_userip")+"','"
					+getUserVar("sys_agentcode")+"'); "
					+ "   commit; "
					+ " end; ");
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage()) ;
			// TODO: handle exception
		}
		
		String xs_ = "";
		xs_ = xs_ + " begin "; 
		xs_ = xs_ + " 	? := subadmin.pkg_itc.LAYTT_THUEBAO_CCBS_QLSP(?,'"+getUserVar("userID")+"'); "; 
		xs_ = xs_ + " end; ";
				
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		nei_.setDataSrc("CMDVOracleDS");
		nei_.bindParameter(2,so_tb);
				
		try{
			rs_ = reqRSet(nei_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		try {			
			TimeUnit.MILLISECONDS.sleep(300);    
			} catch (InterruptedException e) {			
			} 
		return new NEOCommonData(rs_);
	} 
	
	public NEOCommonData layTTThueBao_v1(String so_tb)
	{
		RowSet rs_ = null;
		try {
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"layTTThueBao", getUserVar("userID"),
					getUserVar("sys_agentcode"),"30|1|30");
			if(!blnCheck)
				return new NEOCommonData(rs_);
			reqValue("begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
					+so_tb+"','layTTThueBao'," 
					+"'"+so_tb+";',"
					+"'','"
					+getUserVar("userID")+"','"
					+getEnvInfo().getParserParam("sys_userip")+"','"
					+getUserVar("sys_agentcode")+"'); "
					+ "   commit; "
					+ " end; ");
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage()) ;
			// TODO: handle exception
		}
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_itc.LAYTT_THUEBAO(?); ";
		xs_ = xs_ + " end; ";
		
		
		
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		nei_.setDataSrc("CMDVOracleDS");
		nei_.bindParameter(2,so_tb);
		
		
		try{
			rs_ = reqRSet(nei_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println(xs_);
		
		return new NEOCommonData(rs_);

	}
	
	public String checktb_itc(String so_tb)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_itc.check_dau_itc(?); ";
		xs_ = xs_ + " end; ";
		
		
		String rs_ = "";
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		nei_.setDataSrc("CMDVOracleDS");
		nei_.bindParameter(2,so_tb);
		try{
			rs_ = reqValue(nei_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		return "begin ?:="+rs_+";end;";
	
	}
	
	public String check_ATC(String so_tb)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := admin_v2.pkg_soa_api.getSubscriber('"+so_tb+"'); ";
		xs_ = xs_ + " end; ";

		return xs_;
	} 
	public String checktb_itc_ct(String so_tb)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_itc.check_dau_itc(?); ";
		xs_ = xs_ + " end; ";
		
		
		String rs_ = "";
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		nei_.setDataSrc("CMDVOracleDS");
		nei_.bindParameter(2,so_tb);
		try{
			rs_ = reqValue(nei_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		return rs_;
	
	}
		public NEOCommonData layTTThueBao_common_itc(String so_tb)
	{
		RowSet rs_ = null;
		try {
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"layTTThueBao", getUserVar("userID"),
					getUserVar("sys_agentcode"),"30|1|30");
			if(!blnCheck)
				return new NEOCommonData(rs_);
			reqValue("begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
					+so_tb+"','layTTThueBao'," 
					+"'"+so_tb+";',"
					+"'','"
					+getUserVar("userID")+"','"
					+getEnvInfo().getParserParam("sys_userip")+"','"
					+getUserVar("sys_agentcode")+"'); "
					+ "   commit; "
					+ " end; ");
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage()) ;
			// TODO: handle exception
		}
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_itc.LAYTT_THUEBAO_ITC(?); ";
		xs_ = xs_ + " end; ";
		String agent = getUserVar("sys_agentcode"); 
		String userid = getUserVar("userID"); 
		
		
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		//Thuyvtt comment cho test ko cho thue bao VCC doi sim
		nei_.setDataSrc("CMDVOracleDS"); 
		nei_.bindParameter(2,so_tb);
	
		
		
		try{
			rs_ = reqRSet(nei_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println(xs_);
		
		return new NEOCommonData(rs_);

	}
	
}