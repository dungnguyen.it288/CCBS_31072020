package neo.cmdv114;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import neo.smartui.report.NEOCommonData;
import cosaldap.Base64.*;

import java.util.*;
import java.util.prefs.Preferences;

import javax.sql.*;

import java.net.*;
import java.io.*;

public class vinanv_114 extends NEOProcessEx
{

	public NEOCommonData layTTThe(String so_the)
	{
		String so_the_ = so_the.replace("'","''");
		String uid_ = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");

		String xs_ = "";
		xs_ = xs_ + "begin ?:= PKG_LVP.laytt_huythe('"+so_the_+"','"+uid_+"','GPC','"+matinh_ngdung_+"'); end;";
        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("LVP_DBMS");		

		return new NEOCommonData(ei_);
	}
	public String docBctkCDL_ALL_(String tu_ngay,String userid) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbcatmo_CDL_ALL_ajax_"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&"+CesarCode.encode("ma_nguoidung")+"="+userid
			+ "&sid="+Math.random();
		return url_;
	}
	public NEOCommonData huythe(String so_the)
	{
		String so_the_ = so_the.replace("'","''");
		String uid_ = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		if (getUserVar("userID")==null){
			return null;
		}
		String xs_ = "";
		xs_ = xs_ + "begin ?:= PKG_LVP.capnhat_huythe('"+so_the_+"','"+uid_+"','GPC','"+matinh_ngdung_+"'); end;";

        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("LVP_DBMS");		

		return new NEOCommonData(ei_);
	}	

	public String docDsHuyThe(String serial, String tu_ngay, String den_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dshuythe";
		url_ = url_ + "&"+CesarCode.encode("serial")+"="+serial;
		url_ = url_ + "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay;
		url_ = url_ + "&"+CesarCode.encode("den_ngay")+"="+den_ngay;
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	public NEOCommonData doithe(String so_the, String ten_dl, String so_dt)
	{
		String so_the_ = so_the.replace("'","''");
		String ten_dl_ = ten_dl.replace("'","''");
		String so_dt_ = so_dt.replace("'","''");
		
		String uid_ = getUserVar("userID");
		if (getUserVar("userID")==null){
			return null;
		}
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		String xs_ = "";
		xs_ = xs_ + "begin ?:= PKG_LVP.capnhat_doithe('"+so_the_+"','"+uid_+"','GPC','"+matinh_ngdung_+"','"+ten_dl_+"','"+so_dt_+"'); end;";

        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("LVP_DBMS");		

		return new NEOCommonData(ei_);
	}	

	public String docDsDoiThe(String serial, String tu_ngay, String den_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/doi_the/mod_dshuythe";
		url_ = url_ + "&"+CesarCode.encode("serial")+"="+serial;
		url_ = url_ + "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay;
		url_ = url_ + "&"+CesarCode.encode("den_ngay")+"="+den_ngay;
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}

	public NEOCommonData dmDV(String so_tb, String dsDvuThaydoi, String ghichu, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String dsDvuThaydoi_ = dsDvuThaydoi;//.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		//String uid_ = uid.replace("'","''");
		String uid_ = getUserVar("userID");
		if (getUserVar("userID")==null){
			return null;
		}
		//Kiem tra quyen thuc hien tren menu
		try {
			String ckQuyen = this.reqValue("","begin ?:= neo.check_access_menu('"+uid_+"', '133'); end;"); 
			if(!ckQuyen.equals("1")) return null;
		}catch (Exception ex){}
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc_services.dong_mo_ds_dvu('"+so_tb_+"','"+dsDvuThaydoi_+"','"+ghichu_+"','"+uid_+"','VNP'); ";
		xs_ = xs_ + " end; ";
		
        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(ei_);
	}

	public NEOCommonData chtb(String so_tb, String loai_moi, String tinh_moi, String ghichu, String puid)
	{
		String decloai_moi_ = "";
		String dectinh_moi_ = "";
	decloai_moi_=loai_moi.trim();
		dectinh_moi_=tinh_moi.trim();
		String so_tb_ = so_tb.replace("'","''");
		String loai_moi_ = loai_moi.replace("'","''");
		String tinh_moi_ = tinh_moi.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		String uid = getUserVar("userID");
		if (getUserVar("userID")==null){
			return null;
		}
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.PKG_NVUCSC_SPS.chdoi_lhtb_vnp('"+so_tb+"','"+decloai_moi_+"','"+dectinh_moi_+"','"+ghichu+"','"+uid+"','"+getUserVar("sys_agentcode")+"'); ";
		//xs_ = xs_ + " 	? := neo.PKG_NVUCSC_SPS.chdoi_lhtb_vnp('"+so_tb+"','"+decloai_moi_+"','"+dectinh_moi_+"','"+ghichu+"','"+uid+"','"+getUserVar("sys_agentcode")+"'); ";
		xs_ = xs_ + " end; ";
        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(ei_);
	}

	public NEOCommonData layds_loaimoi(String so_tb, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String uid_ = uid.replace("'","''");

		String xs_ = "";
		xs_ = xs_ + " <select class=textField name='-tinhmoi' id='-tinhmoi' ";
		xs_ = xs_ + " 	sql='begin ?:=subadmin.pkg_nvucsc.chdoi_lhtb_layds_loai_moi('"+so_tb_+"','"+uid_+"'); end;' ";
		xs_ = xs_ + " 	style='background-color :#f1e8e6'  dbms-font='VN8VN3' ";
		xs_ = xs_ + " 	notusename='true' />";

        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(ei_);
	}
	
	public NEOCommonData layds_tinhmoi(String so_tb, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String uid_ = uid.replace("'","''");
	
		String xs_ = "";
		xs_ = xs_ + " <select class=textField name='-tinhmoi' id='-tinhmoi' ";
		xs_ = xs_ + " 	sql='begin ?:=subadmin.pkg_nvucsc.chdoi_lhtb_layds_tinh_moi('"+so_tb_+"','"+uid_+"'); end;' ";
		xs_ = xs_ + " 	style='background-color :#f1e8e6'  dbms-font='VN8VN3' ";
		xs_ = xs_ + " 	notusename='true' />";

        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(ei_);
	}

	/*public String dsimtb(String so_tb, String so_msin_moi, int simfree, String ghichu, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String so_msin_moi_ = so_msin_moi.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		//String uid_ = uid.replace("'","''");
		String uid_ = getUserVar("userID");
		if (getUserVar("userID")==null){
			return null;
		}
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc_qlsp.doi_sim_tb('"+so_tb_+"','"+so_msin_moi_+"',"+simfree+",'"+ghichu_+"','"+uid_+"','"+getUserVar("sys_agentcode")+"'); ";
		xs_ = xs_ + " end; ";

		return xs_;
	}*/
	
	public NEOCommonData dsimtb(String so_tb, String so_msin_moi, int simfree, String ghichu, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String so_msin_moi_ = so_msin_moi.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		//String uid_ = uid.replace("'","''");
		String uid_ = getUserVar("userID");
		if (getUserVar("userID")==null){
			return null;
		}
		/*
		//Lay Flag va Seq tra cuu thue bao QLSP
		String resultFlag = null,resultSeq=null;
		try {
			resultFlag = this.reqValue("","begin ?:= subadmin.get_flag_qlsp(2); end;"); 
			resultSeq = this.reqValue("","begin ?:= subadmin.get_seq_qlsp(2); end;"); 
		}catch (Exception ex){} 

		String getSiminfo = "";
		if(resultFlag.equals("2")){
			String url = getSysConst("ServerQLSP");
	        String responseQLSP = null;
			try {
				responseQLSP = createWSgetSimInfo(url,so_msin_moi_,resultSeq);
				getSiminfo = getStringbyNode(responseQLSP,"result");
				System.out.println("getSiminfo: "+getSiminfo);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc_qlsp_114.doi_sim_tb('"+so_tb_+"','"+so_msin_moi_+"',"+simfree+",'"+ghichu_+"','"+uid_+"','"+getUserVar("sys_agentcode")+"','"+getSiminfo+"'); ";
		xs_ = xs_ + " end; ";
		*/
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc_qlsp.doi_sim_tb('"+so_tb_+"','"+so_msin_moi_+"',"+simfree+",'"+ghichu_+"','"+uid_+"','VNP'); ";
		xs_ = xs_ + " end; ";

        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(ei_);
	}
/*
	public String chkSMOI(String so_tb, String so_msin_moi)
	{
		String so_tb_ = so_tb.replace("'","''");
		String so_msin_moi_ = so_msin_moi.replace("'","''");
		String matinh_ngdung_ = getUserVar("sys_agentcode");

		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc_qlsp.doi_sim_ktra_SIM_moi('"
			+so_tb_+"','"+so_msin_moi_+"','"+matinh_ngdung_+"'); ";
		xs_ = xs_ + " end; ";

		
		return xs_;		
	}*/
	public NEOCommonData chkSMOI(String so_tb, String so_msin_moi)
	{
		String so_tb_ = so_tb.replace("'","''");
		String so_msin_moi_ = so_msin_moi.replace("'","''");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		/*
		//Lay Flag va Seq tra cuu thue bao QLSP
		String resultFlag = null,resultSeq=null;
		try {
			resultFlag = this.reqValue("","begin ?:= subadmin.get_flag_qlsp(1); end;"); 
			resultSeq = this.reqValue("","begin ?:= subadmin.get_seq_qlsp(1); end;"); 
		}catch (Exception ex){} 

		String getSiminfo = "";
		if(resultFlag.equals("2")){
			String url = getSysConst("ServerQLSP");
	        String responseQLSP = null;
			try {
				responseQLSP = createWSgetSimInfo(url,so_msin_moi_,resultSeq);
				getSiminfo = getStringbyNode(responseQLSP,"result");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc_qlsp_114.doi_sim_ktra_SIM_moi('"
			+so_tb_+"','"+so_msin_moi_+"','"+matinh_ngdung_+"','"+getSiminfo+"'); ";
		xs_ = xs_ + " end; ";*/
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc_qlsp.doi_sim_ktra_SIM_moi('"
			+so_tb_+"','"+so_msin_moi_+"','GPC'); ";
		xs_ = xs_ + " end; ";
		
        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(ei_);
	}
	public static String getStringbyNode(String xml, String node){
		int begin = xml.indexOf(node);
		int end = xml.lastIndexOf(node);
		String result = xml.substring(begin, end);
		 begin = result.indexOf(">")+1;
		 end = result.lastIndexOf("<");
		return result.substring(begin, end);
	}
public static String createWSgetSimInfo(String purl, String pso_tb, String resultSeq) throws IOException{
		
		Preferences systemPref = Preferences.systemRoot();
		String sessionID = systemPref.get("sessionId", "sessionId" + " was not found.");
		
		String url = "http://"+purl+"/NumberStore";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("POST");
		con.setRequestProperty("Accept-Encoding", "gzip,deflate");
		con.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
		con.setRequestProperty("SOAPAction", "");
		con.setRequestProperty("Host", purl);
		con.setRequestProperty("Connection", "Keep-Alive");

		String urlParameters = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:NumberStore\">"
				+ "<soapenv:Header/>"
					+ "<soapenv:Body>"
						+ "<urn:getSimInfo soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
								+ "<sessionID xsi:type=\"xsd:string\">"+sessionID+"</sessionID>"
								+ "<p_ProcessID xsi:type=\"xsd:string\">"+resultSeq+"</p_ProcessID>"
								+ "<p_SystemID xsi:type=\"xsd:string\">2</p_SystemID>"
								+ "<p_msin xsi:type=\"xsd:string\">"+pso_tb+"</p_msin>"
						+ "</urn:getSimInfo>"
					+ "</soapenv:Body>"
				+ "</soapenv:Envelope>";
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		return response.toString();
	}
	
	public NEOCommonData doitenTb(String so_tb, String tentb_moi)
	{
		String so_tb_ = so_tb.replace("'","''");
		String tentb_moi_ = tentb_moi.replace("'","''");
		if (getUserVar("userID")==null){
			return null;
		}
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc.cat_mo_cap_nhat_ten('"+so_tb_+"','"+tentb_moi_+"'); ";
		//xs_ = xs_ + " 	commit; ";
		xs_ = xs_ + " end; ";
		//System.out.println("ABC"+ xs_);
        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(ei_);
	}

	public NEOCommonData doiDchiTb(String so_tb, String dchi_moi)
	{
		String so_tb_ = so_tb.replace("'","''");
		String dchi_moi_ = dchi_moi.replace("'","''");

		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc.cat_mo_cap_nhat_dia_chi('"+so_tb+"','"+dchi_moi_+"'); ";
		//xs_ = xs_ + " 	commit; ";
		xs_ = xs_ + " end; ";

        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(ei_);
	}
	
	
	
	public NEOCommonData catmoICOC(String trth, String goidi, String goiden, String so_tb, String ma_lydo, String ghichu, String puid)
	{
		String ghichu_ = ghichu.replace("'","''");
		String decMa_lydo_ = CesarCode.decode(ma_lydo).trim();
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		String xs_ = "";
		String rs_ =null;
		String xs1_ = "";
		String rs1_ =null;
		String uid = getUserVar("userID");
		//Kiem tra quyen thuc hien tren menu
		try {
			String ckQuyen = this.reqValue("","begin ?:= neo.check_access_menu('"+uid+"', '132'); end;"); 
			if(!ckQuyen.equals("1")) return null;
		}catch (Exception ex){}
		xs_ = "begin "
				+ " 	? := subadmin.ins_log_catmo_icoc('"
						+trth+"','"
						+goidi+"','"
						+goiden+"','"
						+so_tb+"','"
						+ma_lydo+"','"
						+ghichu+"','"
						+uid+"','"
						+puid+"','"
						+getEnvInfo().getParserParam("sys_userip")+"','VNP'); "
						+ "   commit; "
						+ " end; ";		
		try{
			rs_ = reqValue("CMDVOracleDS",xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		if (getUserVar("userID")==null){
			return null;
		}
		//kiem tra trang thai danh ba theo nghiep vu HCM: neu tb dang Khoa do no dong se khong duoc mo
		/*
		xs_ = " begin "
			+ " 	? := neo.pkg_catmo_ioc.laytt_trangthaitb('"+so_tb+"'); "
			+ "   commit; "
			+ " end; ";		
		
			try{
				rs_ = reqValue(xs_);
				if (rs_.equals("3") && (goidi.equals("1") || goiden.equals("1"))) {  //khoa 1 chieu do no cuoc
					return new NEOCommonData("HCM-1C-CUOC" );		
				}	
				
				if (rs_.equals("5") && (goidi.equals("1") || goiden.equals("1"))) {  //khoa 2 chieu do no cuoc
					return new NEOCommonData("HCM-2C-CUOC");		
				}	
			} catch (Exception ex) {
				ex.printStackTrace();	
			}*/
	//}
		//Cap nhat lai trang thai danh ba
		xs1_ = " begin "
			+ " 	? := neo.pkg_catmo_ioc.dong_bo_trang_thai_db('"
				+trth+"','"+goidi+"','"+goiden+"','"+so_tb+"','"
				+decMa_lydo_+"','"+ghichu_+"','"+uid+"',0,1); "
			+ "   commit; "
			+ " end; "
		;		
		
		try{
			rs1_ = reqValue("CMDVOracleDS",xs1_);
		} catch (Exception ex) {
			ex.printStackTrace();	
			rs1_ ="0";		
		}	
		if (rs1_=="0") {
			return new NEOCommonData("Loi khong dong bo duoc trang thai danh ba.");		
		}
		
		//gui yeu cau cat mo sang may CMDV		
		xs_ = " begin "
			+ " 	? := subadmin.pkg_nvucsc_qlsp_114.cat_mo_OC_IC_huy_HD2('"
				+trth+"','"+goidi+"','"+goiden+"','"+so_tb+"','"
				+decMa_lydo_+"','"+ghichu_+"','"+uid+"','"
				+getUserVar("sys_agentcode")+"'); "
			+ "   commit; "
			+ " end; "
		;	
		
		System.out.println(xs_);
		
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();			
		}
		
		//Cap nhat lai trang thai danh ba
		/*
		xs1_ = " begin "
			+ " 	? := ccs_admin.PTTB_TEST_DONG.dong_bo_trang_thai_db('"
				+trth+"','"+goidi+"','"+goiden+"','"+so_tb+"','"
				+decMa_lydo_+"','"+ghichu_+"','"+uid+"','"
				+getUserVar("sys_agentcode")+"',0); "
			+ "   commit; "
			+ " end; "
		;		
		
		try{
			rs1_ = reqValue("VNPCCBS_DBMS",xs1_);
		} catch (Exception ex) {
			ex.printStackTrace();			
		}*/
		
		
		
		return new NEOCommonData(rs_);
	}

	public NEOCommonData dkyGoiIDD1714(String trth, String so_tb, String ma_goi, String ghichu, String puid, String uip)
	{
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		String sDataSchema_ = getUserVar("sys_dataschema");
		String sfuncschema_ = getSysConst("FuncSchema");	
		String ghichu_ = ghichu.replace("'","''");
		String goi_moi_ = ma_goi;
		String uid = getUserVar("userID");
		if (getUserVar("userID")==null){
			return null;
		}
		//kiem tra  loai khach hang ca nhan 				
		String xs_ = "";
		xs_ = " begin "
			+ " 	? := ccs_admin.pttb_tracuu.laytt_doituong_tb('"+ sDataSchema_+"','"+matinh_ngdung_+"','','"+so_tb+"'); "
			+ "   commit; "
			+ " end; " ;
		System.out.println("dongnd : "+xs_);
		
		String rs_ = null;
		try{
			rs_ = reqValue( xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
			rs_="0";
		}		
		
		System.out.println("dongnd : "+rs_);
		
		if (!rs_.equals("1")) {
			return new NEOCommonData("6");			
		}
		//end
		
		xs_ = " begin "
			+ " 	? := subadmin.pkg_idd1714.dky_idd1714('"
				+ trth+"','"+so_tb+"','"+goi_moi_+"','"+ghichu_+"','"
				+uid+"','"+uip+"'); "
			+ "   commit; "
			+ " end; "
		;		
		
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}
	
	public String docDsGoiIDD1714(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dsgoiidd1714";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		System.out.println(">>>>>>>>>");
		return url_;
	}
	
	public NEOCommonData dkyGoiGPRS(String trth, String so_tb, String ma_goi, String ghichu, String puid)
	{
		String ghichu_ = ghichu.replace("'","''");
		String goi_moi_ = ma_goi;

		//goi_moi_ = CesarCode.decode(ma_goi).trim();
		String uid = getUserVar("userID");
		if (getUserVar("userID")==null){
			return null;
		}
		String xs_ = "";
		xs_ = " begin "
			+ " 	? := subadmin.pkg_nvucsc.dky_goi_gprs('"
				+ trth+"','"+so_tb+"','"+goi_moi_+"','"+ghichu_+"','"
				+uid+"'); "
			+ "   commit; "
			+ " end; "
		;
		
        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(ei_);
	}
	
	
	public String docDvDky(String so_tb_daydu) {
		//String so_tb_daydu_ = so_tb_daydu.replace("'","''");
		
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/cmdv114/vinacore/mod_dmdv_tb_114";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	
	public String docDkyDv() {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/cmdv114/vinacore/mod_dkydv_114";
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}

	public String docHotBill(String so_tb_daydu, String thang, String nam) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_ttcuoc_dd";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("thang")+"="+thang;
		url_ = url_ + "&"+CesarCode.encode("nam")+"="+ nam;	
		/*url_ = url_ + "&"+CesarCode.encode("+thang")+"="+thang;
		url_ = url_ + "&"+CesarCode.encode("+nam")+"="+ nam;		*/

        return url_;		
	}
	
	public String docHotBill_HD(String so_tb_daydu, String thang, String nam) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_ttcuoc_hddd";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("thang")+"="+thang;
		url_ = url_ + "&"+CesarCode.encode("nam")+"="+ nam;	
        return url_;		
	}
	
	public NEOCommonData docCuocDaily(String url) {
		String url_= "http://190.10.10.86"+url;
		//System.out.println("================== docCuocDaily:");
		//System.out.println(url_);

		String doc_ = null;
		
		try
		{
			java.net.URL cuocDoc_ = new java.net.URL(url_);
			BufferedReader in = new BufferedReader(
				new InputStreamReader(
				cuocDoc_.openStream()));

			String inputLine;
			StringBuilder sb_ = new StringBuilder();

			while ((inputLine = in.readLine()) != null) {
				//System.out.println("inputLine: "+ inputLine);
				sb_.append(inputLine);
			}
			in.close();		
		
			doc_ = sb_.toString();
		} catch (Exception ex) {
			doc_ = ex.toString();
		}
		//
		return new NEOCommonData(doc_);	
	}
	
	public String docDvuTb(String so_tb_daydu) {
		//String x_ = "begin ?:= subadmin.PKG_NVUCSC.layds_dvutb('"+so_tb_daydu+"'); end;";
		//setUserVar("sqlDvuTb",x_);	
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/cmdv114/vinacore/mod_dvu_tb_114";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docLsTb(String so_tb_daydu) {
		//String x_ = "begin ? := subadmin.PKG_UTILS.LAYDS_LSU_THUEBAO(?); end;";
		//setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/cmdv114/vinacore/mod_lstb_114";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
    public String docLsTb(String so_tb_daydu,String page_num, String page_rec) {

		//String x_ = "begin ? := subadmin.pkg_utils_114_cuongnc.LAYDS_LSU_THUEBAO(?,?,?); end;";
		String x_ = "begin ? := subadmin.LAYDS_LSU_TB_114_chuyendoi(?,?,?); end;";
		setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_lstb1";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("page_num")+"="+page_num;
		url_ = url_ + "&"+CesarCode.encode("page_rec")+"="+page_rec;
		url_ = url_ + "&sid="+Math.random();
		return url_;



	}
	public String docLsNo(String so_tb_daydu, String sp_name) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/cmdv114/vinacore/mod_ttlsno_114";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("sp_name")+"=VNSG";
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docDsGoiGPRS(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dsgoigprs";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		System.out.println(">>>>>>>>>");
		return url_;
	}

	public String docDsLoaiMoi(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dsloaimoi";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docDsTinhMoi(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dstinhmoi";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docDsLoaiDvuCMTF(int thao_tac) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/cmdv114/vinacore/mod_dsdvu_cmtf_114";
		url_ = url_ + "&"+CesarCode.encode("thao_tac")+"="+thao_tac;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	public String docDsLoaiHD(String so_tb_daydu, String sp_name) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=xdc/mod_ds_loaihd";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("sp_name")+"=VNSG";
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docDsMaHD(String so_tb_daydu, String ma_loaihd, String sp_name) {
		String dec_ma_loai_hd_  = CesarCode.decode(ma_loaihd);
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=xdc/mod_ds_mahd";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("ma_loaihd")+"="+dec_ma_loai_hd_;
		url_ = url_ + "&"+CesarCode.encode("sp_name")+"=VNSG";
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	public String docDsLoaiKP(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/cmdv114/vinacore/mod_dsloaiKP_114";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}

	public String docDsTinhKP(String so_tb_daydu) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dstinhKP";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}	
	public NEOCommonData layTTSIM(String sim)
	{
		String sim_ = sim.replace("'","''");
		String uid_ = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");

		String xs_ = "";
		xs_ = xs_ + "begin ?:= subadmin.PKG_NVUCSC.khoitaoAC_ktraSIM('"+sim_+"','"+uid_+"','"+matinh_ngdung_+"'); end;";

        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(ei_);
	}

	public NEOCommonData khoitaoAC(String sim, String ghi_chu)
	{
		String sim_ = sim.replace("'","''");
		String ghi_chu_ = ghi_chu.replace("'","''");
		String uid_ = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		if (getUserVar("userID")==null){
			return null;
		}
		String xs_ = "";
		xs_ = xs_ + "begin ?:= subadmin.PKG_NVUCSC_SPS.khoitaoAC_SIM('"+sim_+"','"
			+ghi_chu_+"','"+uid_+"','"+matinh_ngdung_+"'); end;";

        NEOExecInfo ei_ = new NEOExecInfo(xs_);
        ei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(ei_);
	}	

	public String docDkyDv_FullW() {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_dkydv_fullw";
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}

	public String docBctkTienTb2Post(String so_tb, String tu_ngay, String den_ngay, String trg_sx) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slct_tientb2post_ajax"
			+ "&"+CesarCode.encode("so_tb")+"="+so_tb
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&"+CesarCode.encode("den_ngay")+"="+den_ngay
			+ "&"+CesarCode.encode("trg_sx")+"="+trg_sx
			+ "&sid="+Math.random();
		return url_;
	}	
	
	public String docBctkCDL_S2T(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbcatmo_CDL_S2T_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&sid="+Math.random();
		return url_;
	}	

	public String docBctkCDL_T2S(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbcatmo_CDL_T2S_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&sid="+Math.random();
		return url_;
	}	
	
	public String docBctkCDL_ALL(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbcatmo_CDL_ALL_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&sid="+Math.random();
		return url_;
	}		
	
	public String docBctkCAN(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbcatmo_CAN_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&sid="+Math.random();
		return url_;
	}		
	
	public String docBctkSIM(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbcatmo_SIM_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&sid="+Math.random();
		return url_;
	}		
	public String docBctkSIM_(String tu_ngay,String user_id, String loai_tb) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbcatmo_SIM_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&"+CesarCode.encode("user_nguoidung")+"="+user_id
			+ "&"+CesarCode.encode("loai_tb")+"="+loai_tb
			+ "&sid="+Math.random();
		return url_;
	}	
	public String docBctk_IC_OC(String tu_ngay, String loaitb) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbcatmo_IC_OC_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&"+CesarCode.encode("loaitb")+"="+loaitb			
			+ "&sid="+Math.random();
		return url_;
	}	
	
	public String docBctkALL(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbcatmo_ALL_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay				
			+ "&sid="+Math.random();
		return url_;
	}
	public String docBctkNEW(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbcatmo_NEW_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&sid="+Math.random();
		return url_;
	}	
	
	public String docBctkTTK2C(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbpdtk2ch_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&sid="+Math.random();
		return url_;
	}	

	public String docBctkTBKH(String tu_ngay, String den_ngay, String so_tb) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbkh_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&"+CesarCode.encode("den_ngay")+"="+den_ngay
			+ "&"+CesarCode.encode("so_tb")+"="+so_tb
			+ "&sid="+Math.random();
		
		return url_;
	}		
	
	public String docBctkTBNT(String tu_ngay) {
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/bcao_catmo/bctk_slcttbnt_ajax"
			+ "&"+CesarCode.encode("tu_ngay")+"="+tu_ngay
			+ "&sid="+Math.random();
		return url_;
	}
	public String docLsTb1(String so_tb_daydu) {
		String x_ = "begin ? := subadmin.PKG_UTILS.LAYDS_LSU_THUEBAO(?); end;";
		setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/mod_lstb1";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}	
	public String laytt_loaitb(
			String psSOMAY                         
  	) {
     String s=    "begin ? := subadmin.PKG_NVUCSC.laytt_loaitb('"+psSOMAY+"'); end;";
    return s;
	}
	public NEOCommonData catmoICOC_MEGRATE(String trth, String goidi, String goiden, String so_tb, String ma_lydo, String ghichu, String puid)
	{
		String ghichu_ = ghichu.replace("'","''");
		String decMa_lydo_ = CesarCode.decode(ma_lydo).trim();
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		String xs_ = "";
		String rs_ =null;
		String xs1_ = "";
		String rs1_ =null;
		String uid = getUserVar("userID");
		if (getUserVar("userID")==null){
			return null;
		}
		//gui yeu cau cat mo sang may CMDV		
		xs_ = " begin "
			+ " 	? := subadmin.pkg_nvucsc_services.cat_mo_OC_IC_huy_HD3('"
				+trth+"','"+goidi+"','"+goiden+"','"+so_tb+"','"
				+decMa_lydo_+"','"+ghichu_+"','"+uid+"','"
				+getUserVar("sys_agentcode")+"'); "
			+ "   commit; "
			+ " end; "
		;	
		
		System.out.println(xs_);
		
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();			
		}
		
		//Cap nhat lai trang thai danh ba
		
		xs1_ = " begin "
			+ " 	? := ccs_admin.PTTB_TEST_DONG.dong_bo_trang_thai_db('"
				+trth+"','"+goidi+"','"+goiden+"','"+so_tb+"','"
				+decMa_lydo_+"','"+ghichu_+"','"+uid+"','"
				+getUserVar("sys_agentcode")+"',0); "
			+ "   commit; "
			+ " end; "
		;		
		
		try{
			rs1_ = reqValue("VNPCCBS_DBMS",xs1_);
		} catch (Exception ex) {
			ex.printStackTrace();			
		}
		
		
		
		return new NEOCommonData(rs_);
	}
}
