package neo.cmdv114;

import neo.smartui.process.*;
import neo.smartui.report.*;
import javax.sql.*;

import java.net.InetAddress;
import java.net.UnknownHostException;

import neo.qlsp.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import cosaldap.Base64.*;
import neo.smartui.common.*;

public class tttb extends NEOProcessEx 
{
	
	
	
	public String get_ds_goi(String so_tb,String type){
		String rs_ = null;
			String xs_ = "";
		try{
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.get_ds_goi('"+so_tb+"','"+type+"'); ";
			xs_ = xs_ + " end; ";
			 
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String get_used(String so_tb){
		String rs_ = null;
			String xs_ = "";
		try{
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.check_used('"+so_tb+"'); ";
			xs_ = xs_ + " end; ";
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String dangkygoicuoc(String so_tb,String idgoi){
		String rs_ = null;
			String xs_ = "";
		InetAddress ip;
		
		try{
			ip = InetAddress.getLocalHost();
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.dangkygoicuoc('"+so_tb+"','"+getUserVar("userID")+"','"+idgoi+"','"+getUserVar("sys_agentcode")+"','"+getEnvInfo().getParserParam("sys_userip")+"'); ";
			xs_ = xs_ + " end; ";
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String docLsTb(String so_tb_daydu,String page_num, String page_rec) {
		String x_ = "begin ? := admin_v2.dk_tbtt.get_log(?,?,?); end;";
		setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=tracuu/tc_dk_tb_tratruoc/lstb";
		url_ = url_ + "&"+CesarCode.encode("so_tb_daydu")+"="+so_tb_daydu;
		url_ = url_ + "&"+CesarCode.encode("page_num")+"="+page_num;
		url_ = url_ + "&"+CesarCode.encode("page_rec")+"="+page_rec;
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	public String customer_kind(String so_tb){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.customer_kind_report('"+so_tb+"'); ";
			xs_ = xs_ + " end; ";
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String get_danhsach_kmcb_data(String page_num, String page_rec,String status,String kieugoi,String service_code,String thoigian_sd,String ten_goi,
				String doi_tuong,String noidung,String gia,String loai,String ghi_chu,String phut_noimang,String phut_ngoaimang,String dungluong_gb,String chitiet,String date_from,String date_to) {
		String x_ = "begin ? := admin_v2.dk_tbtt.get_danhsach_kmcb_data(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); end;";
		setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=tracuu/tc_dk_tb_tratruoc/cauhinh/ds_kmcb_data";
		url_ = url_ + "&"+CesarCode.encode("page_num")+"="+page_num;
		url_ = url_ + "&"+CesarCode.encode("page_rec")+"="+page_rec;
		url_ = url_ + "&"+CesarCode.encode("status")+"="+status;
		url_ = url_ + "&"+CesarCode.encode("kieugoi")+"="+kieugoi;
		url_ = url_ + "&"+CesarCode.encode("service_code")+"="+service_code;
		url_ = url_ + "&"+CesarCode.encode("thoigian_sd")+"="+thoigian_sd;
		url_ = url_ + "&"+CesarCode.encode("ten_goi")+"="+ten_goi;
		url_ = url_ + "&"+CesarCode.encode("doi_tuong")+"="+doi_tuong;
		url_ = url_ + "&"+CesarCode.encode("noidung")+"="+noidung;
		url_ = url_ + "&"+CesarCode.encode("gia")+"="+gia;
		url_ = url_ + "&"+CesarCode.encode("loai")+"="+loai;
		url_ = url_ + "&"+CesarCode.encode("ghi_chu")+"="+ghi_chu;
		url_ = url_ + "&"+CesarCode.encode("phut_noimang")+"="+phut_noimang;
		url_ = url_ + "&"+CesarCode.encode("phut_ngoaimang")+"="+phut_ngoaimang;
		url_ = url_ + "&"+CesarCode.encode("dungluong_gb")+"="+dungluong_gb;
		url_ = url_ + "&"+CesarCode.encode("chitiet")+"="+chitiet;
		url_ = url_ + "&"+CesarCode.encode("date_from")+"="+date_from;
		url_ = url_ + "&"+CesarCode.encode("date_to")+"="+date_to;
		url_ = url_ + "&"+CesarCode.encode("userID")+"="+getUserVar("userID");
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	public String add_danhsach_kmcb_data(String status,String kieugoi,String service_code,String thoigian_sd,String ten_goi,String doi_tuong,String noidung,String gia,String loai,String ghi_chu,String phut_noimang,String phut_ngoaimang,String dungluong_gb,String chitiet,String date_from,String date_to,String sms_config){
		String rs_ = null;
		String xs_ = "";
		try{
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.add_kmcb_data('"+ status+"','"+ kieugoi+"','"+ service_code+"','"+ thoigian_sd+"','"+ ten_goi+"','"+ doi_tuong+"','"+ noidung+"','"+ gia+"','"+ loai+"','"+ ghi_chu+"','"+ phut_noimang+"','"+ phut_ngoaimang+"','"+ dungluong_gb+"','"+ chitiet+"','"+ date_from+"','"+ date_to+"','"+sms_config+"','"+getUserVar("userID")+"'); ";
			xs_ = xs_ + " end; ";
			System.out.println(xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
		
	}
	
	public String del_danhsach_kmcb_data(String psid){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.del_add_kmcb_data('"+psid+"','"+getUserVar("userID")+"'); ";
			xs_ = xs_ + " end; ";
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String edit_danhsach_kmcb_data(String psid ,String status,String kieugoi,String service_code,String thoigian_sd,String ten_goi,String doi_tuong,String noidung,String gia,String loai,String ghi_chu,String phut_noimang,String phut_ngoaimang,String dungluong_gb,String chitiet,String date_from,String date_to,String sms_config){
		String rs_ = null;
		String xs_ = "";
		try{
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.edit_kmcb_data('"+psid+"','"+ status+"','"+ kieugoi+"','"+ service_code+"','"+ thoigian_sd+"','"+ ten_goi+"','"+ doi_tuong+"','"+ noidung+"','"+ gia+"','"+ loai+"','"+ ghi_chu+"','"+ phut_noimang+"','"+ phut_ngoaimang+"','"+ dungluong_gb+"','"+ chitiet+"','"+ date_from+"','"+ date_to+"','"+sms_config+"','"+getUserVar("userID")+"'); ";
			xs_ = xs_ + " end; ";
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
		
	}
	
	public String check_sotb(String so_tb)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := admin_v2.dk_tbtt.check__tb('"+so_tb+"'); ";
		xs_ = xs_ + " end; ";
		
		return xs_;
	} 
	
	public String get_goimua() {
		String x_ = "begin ? := admin_v2.dk_tbtt.get_goimua; end;";
		setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=tracuu/tc_dk_tb_tratruoc/cauhinh/ds_goimua";
		
		return url_;
	}
	public String get_list_check(String page_num, String page_rec,String status,String kieugoi,String service_code,String thoigian_sd,String ten_goi,
				String doi_tuong,String noidung,String gia,String loai,String ghi_chu,String phut_noimang,String phut_ngoaimang,String dungluong_gb,String chitiet,String date_from,String date_to) {
		String x_ = "begin ? := admin_v2.dk_tbtt.get_danhsach_kmcb_data(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); end;";
		setUserVar("sqlLsTb",x_);	
		String url_ ="/main";
		
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=tracuu/tc_dk_tb_tratruoc/cauhinh/ds_goi_data";
		url_ = url_ + "&"+CesarCode.encode("page_num")+"="+page_num;
		url_ = url_ + "&"+CesarCode.encode("page_rec")+"="+page_rec;
		url_ = url_ + "&"+CesarCode.encode("status")+"="+status;
		url_ = url_ + "&"+CesarCode.encode("kieugoi")+"="+kieugoi;
		url_ = url_ + "&"+CesarCode.encode("service_code")+"="+service_code;
		url_ = url_ + "&"+CesarCode.encode("thoigian_sd")+"="+thoigian_sd;
		url_ = url_ + "&"+CesarCode.encode("ten_goi")+"="+ten_goi;
		url_ = url_ + "&"+CesarCode.encode("doi_tuong")+"="+doi_tuong;
		url_ = url_ + "&"+CesarCode.encode("noidung")+"="+noidung;
		url_ = url_ + "&"+CesarCode.encode("gia")+"="+gia;
		url_ = url_ + "&"+CesarCode.encode("loai")+"="+loai;
		url_ = url_ + "&"+CesarCode.encode("ghi_chu")+"="+ghi_chu;
		url_ = url_ + "&"+CesarCode.encode("phut_noimang")+"="+phut_noimang;
		url_ = url_ + "&"+CesarCode.encode("phut_ngoaimang")+"="+phut_ngoaimang;
		url_ = url_ + "&"+CesarCode.encode("dungluong_gb")+"="+dungluong_gb;
		url_ = url_ + "&"+CesarCode.encode("chitiet")+"="+chitiet;
		url_ = url_ + "&"+CesarCode.encode("date_from")+"="+date_from;
		url_ = url_ + "&"+CesarCode.encode("date_to")+"="+date_to;
		url_ = url_ + "&"+CesarCode.encode("userID")+"="+getUserVar("userID");
		url_ = url_ + "&sid="+Math.random();
		return url_;
	}
	
	public String getList_id(String psid){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.get_listID_goi('"+psid+"'); ";
			xs_ = xs_ + " end; ";
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String edit_goimua(String psid,String psten,String pstkc,String psthoai,String psdata,String pscombo,String pstkc_from ,String pstkc_to ,String psds_goi,String psds_id_goi){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.edit_goimua('"+psid+"','"+psten+"','"+pstkc+"','"+psthoai+"','"+psdata+"','"+pscombo+"','"+pstkc_from+"','"+pstkc_to+"','"+psds_goi+"','"+psds_id_goi+"','"+getUserVar("userID")+"'); ";
			xs_ = xs_ + " end; ";
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String up_ds_goi(String psid_muc,String psid_goi, String pssort){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.up_dsgoi_config('"+psid_muc+"','"+psid_goi+"','"+pssort+"','"+getUserVar("userID")+"'); ";
			xs_ = xs_ + " end; ";
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String down_ds_goi(String psid_muc,String psid_goi, String pssort){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.down_dsgoi_config('"+psid_muc+"','"+psid_goi+"','"+pssort+"','"+getUserVar("userID")+"'); ";
			xs_ = xs_ + " end; ";
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String add_goimua(String psten,String pstkc,String psthoai,String psdata,String pscombo,String pstkc_from ,String pstkc_to ,String psds_goi,String psds_id_goi){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.add_goimua('"+psten+"','"+pstkc+"','"+psthoai+"','"+psdata+"','"+pscombo+"','"+pstkc_from+"','"+pstkc_to+"','"+psds_goi+"','"+psds_id_goi+"','"+getUserVar("userID")+"'); ";
			xs_ = xs_ + " end; ";
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	
	public String del_goimua(String psid){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.dk_tbtt.del_goimua('"+psid+"','"+getUserVar("userID")+"'); ";
			xs_ = xs_ + " end; ";
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String get_goimoimua(String stb ,String tengoi){
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.pkg_ctkm.get_list_pkg('"+stb+"','"+tengoi+"'); ";
			xs_ = xs_ + " end; ";
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;
	}
	
	public String ds_ct_goi(String psUserInput) {
		String rs_ = null;
			String xs_ = "";

		try{
			
			xs_ = xs_ + " begin ";
			xs_ = xs_ + " 	? := admin_v2.pkg_ctkm.get_info_package('"+psUserInput+"'); ";
			xs_ = xs_ + " end; ";
						System.out.println(xs_);
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		return xs_;	
	}
	
}