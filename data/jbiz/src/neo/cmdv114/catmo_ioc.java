package neo.cmdv114;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

//import cosaldap.Base64.*;

import java.util.*;
import java.io.*;
import javax.sql.*;
import java.sql.*;


public class catmo_ioc extends NEOProcessEx
{
	//Lay ds chi tiet xu ly file Upload
	public String rec_layds_ctxl(String psUploadID)
	{
		return "/main?"+CesarCode.encode("configFile")+"=vinacore/ic_oc_theo_file/ajax_layds_chitiet"
		+"&"+CesarCode.encode("upload_id")+"="+psUploadID
        ;
	}
	
	//Lay thong tin sau khi kiem tra file UPload
	public String rec_laytt_kiemtra(String psUploadID){
		String out_ ="begin ?:= ADMIN_V2.PTTB_CHUCNANG.laytt_kiemtra_file('"+psUploadID+"'); end;";
		return out_;
	}
	
	//Thuc hien gui yeu cau theo file
	/* ///OLD
	public String value_thuchien_theofile(String psUploadID, String psthaotac, String psdichvu , String psHengio, String psghichu)
		{				
		String out_ = "begin ?:= ADMIN_V2.PTTB_CHUCNANG.catmo_ioc_file('"+
							psUploadID+"',"+
							"'"+psthaotac+"',"+
							"'"+psdichvu+"',"+
							"'"+psHengio+"',"+
							"'"+psghichu+"',"+
							"'"+getUserVar("sys_agentcode")+"',"+
							"'"+getUserVar("userID")+"'); end;";
		System.out.println("luattd : " + out_);
		return out_;					
	}	*/
	///NEW_SPS
	public String value_thuchien_theofile(String psUploadID, String psthaotac, String psdichvu , String psHengio, String psghichu)
		{				
		
		String out_ = "begin ?:= ADMIN_V2.PKG_API_HLR.catmo_ioc_file('"+
							psUploadID+"',"+
							"'"+psthaotac+"',"+
							"'"+psdichvu+"',"+
							"'"+psHengio+"',"+
							"'"+psghichu+"',"+
							"'"+getUserVar("sys_agentcode")+"',"+
							"'"+getUserVar("userID")+"'); end;";
		System.out.println("ket qua : " + out_);
		return out_;					
	}	
	///
		public String rec_laytt_kiemtra_dv(String psUploadID){
		String out_ ="begin ?:= ADMIN_V2.pkg_canhuy_tb.laytt_kiemtra_file_dv('"+psUploadID+"'); end;";
		return out_;
	}
	
	/* ///OLD
	public NEOCommonData dmDV_file(String psUploadID,String dsDvu, String psThaotac, String userid, String matinh)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.PKG_NVUCSC.dongmo_dvu_file('"+psUploadID+"','"+dsDvu+"','"+psThaotac+"','"+userid+"','"+matinh+"'); ";
		xs_ = xs_ + " end; ";		
		
		String rs_ = null;
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}*/
	/// STS_NEW
	public NEOCommonData dmDV_file(String psUploadID,String dsDvu, String psThaotac, String userid, String matinh)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_nvucsc_services.dongmo_dvu_file('"+psUploadID+"','"+dsDvu+"','"+psThaotac+"','"+userid+"','"+matinh+"'); ";
		xs_ = xs_ + " end; ";		
		
		String rs_ = null;
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}
	///
	public String rec_layds_ctxl_dv(String psUploadID)
	{
		return "/main?"+CesarCode.encode("configFile")+"=vinacore/dongmo_dv_file/ajax_layds_chitiet"
		+"&"+CesarCode.encode("upload_id")+"="+psUploadID
        ;
	}
}