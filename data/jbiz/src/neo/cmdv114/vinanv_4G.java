package neo.cmdv114;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

import cosaldap.Base64.*;

import java.util.*;
import javax.sql.*;

import java.net.*;
import java.io.*;

public class vinanv_4G extends NEOProcessEx
{

public NEOCommonData chkSMOI(String so_tb, String so_msin_moi)
	{
		String so_tb_ = so_tb.replace("'","''");
		String so_msin_moi_ = so_msin_moi.replace("'","''");
		String matinh_ngdung_ = getUserVar("sys_agentcode");

		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := neo.pkg_4G.doi_sim_ktra_SIM_moi('"
			+so_tb_+"','"+so_msin_moi_+"','"+matinh_ngdung_+"'); ";
		xs_ = xs_ + " end; ";

		RowSet rs_ = null;
		try{
			rs_ = reqRSet("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);		
	}
	
public NEOCommonData dsimtb(String so_tb, String so_msin_moi, String so_msin_cu, int simfree, String ghichu, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String so_msin_moi_ = so_msin_moi.replace("'","''");
		String so_msin_cu_ = so_msin_cu.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		String uid_ = getUserVar("userID");
	
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := neo.pkg_4G.doi_sim_tb('"+so_tb_+"','"+so_msin_moi_+"','"+so_msin_cu_+"',"+simfree+",'"+ghichu_+"','"+uid_+"','"+getUserVar("sys_agentcode")+"'); ";
		xs_ = xs_ + " end; ";
		
		System.out.println(xs_);
		
		String rs_ = null;
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}	
	
	public NEOCommonData catmoICOC(String trth, String goidi, String goiden, String so_tb, String ma_lydo, String ghichu, String uid)
	{
		String ghichu_ = ghichu.replace("'","''");
		String decMa_lydo_ = CesarCode.decode(ma_lydo).trim();		
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		String xs_ = "";
		String rs_ =null;
		String xs1_ = "";
		String rs1_ =null;
		//Ghi log cat mo IC OC TRINHHV 13/01/2014


		String chk_ = "";
		chk_ = chk_ + " begin ";
		chk_ = chk_ + " 	? := prepaid.CHK_KHS('"+so_tb+"'); ";
		chk_ = chk_ + " end; ";
		
		System.out.println(chk_);
		
		String chk_result = null;
		try{
			//chk_result = reqValue(chk_);
			chk_result = reqValue("CMDVOracleDS", chk_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println(chk_result);
		if (chk_result.equals("1")) {
			return new NEOCommonData("Thue bao dang khong duoc phep thac tac dong mo IC OC bang tay do dang bi khoa nghi KHS MNP. Vui long cap nhat thong tin thue bao!");	
		}
	
		boolean blnCheck;
		try {
			blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"catmoICOC", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return new NEOCommonData(rs_);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (getUserVar("userID") == null){
			xs_ = "begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
								+so_tb+"','catmoICOC'," 
								+"'"+trth+";"+goidi+";"+goiden+";"+so_tb+";"+ma_lydo+";"+ghichu+"','"
								+uid+"','"
								+getUserVar("userID")+"','"
								+getEnvInfo().getParserParam("sys_userip")+"','"
								+getUserVar("sys_agentcode")+"'); "
								+ "   commit; "
								+ " end; ";		
			try{
				rs_ = reqValue(xs_);
			} catch (Exception ex) {
				ex.printStackTrace();	
			}
		}
		else if(!getUserVar("userID").equals(uid)){
			xs_ = "begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
							+so_tb+"','catmoICOC'," 
							+"'"+trth+";"+goidi+";"+goiden+";"+so_tb+";"+ma_lydo+";"+ghichu+"','"
							+uid+"','"
							+getUserVar("userID")+"','"
							+getEnvInfo().getParserParam("sys_userip")+"','"
							+getUserVar("sys_agentcode")+"'); "
							+ "   commit; "
							+ " end; ";		
			try {
				rs_ = reqValue(xs_);
			} catch (Exception ex) {
				ex.printStackTrace();	
			}
		}

		//kiem tra trang thai danh ba theo nghiep vu HCM: neu tb dang Khoa do no dong se khong duoc mo
		
		//if (matinh_ngdung_.equals("HCM")||matinh_ngdung_.equals("HNI")||matinh_ngdung_.equals("QNH")||matinh_ngdung_.equals("QNI")||matinh_ngdung_.equals("PTO")) {
			xs_ = " begin "
			+ " 	? := ADMIN_V2.PTTB_CHUCNANG.laytt_trangthaitb('"
				+so_tb+"','"+getUserVar("sys_dataschema")+"'); "
			+ "   commit; "
			+ " end; ";		
		
			try{
				rs_ = reqValue(xs_);
				if (rs_.equals("3") && (goidi.equals("1") || goiden.equals("1"))) {  //khoa 1 chieu do no cuoc
					return new NEOCommonData("HCM-1C-CUOC" );		
				}	
				
				if (rs_.equals("5") && (goidi.equals("1") || goiden.equals("1"))) {  //khoa 2 chieu do no cuoc
					return new NEOCommonData("HCM-2C-CUOC");		
				}	
			} catch (Exception ex) {
				ex.printStackTrace();	
				return new NEOCommonData("Loi khi khoa mo ICOC");
			}
		//}

		
		//Cap nhat lai trang thai danh ba
		xs1_ = " begin "
			+ " 	? := ADMIN_V2.PTTB_CHUCNANG.dong_bo_trang_thai_db('"
				+trth+"','"+goidi+"','"+goiden+"','"+so_tb+"','"
				+decMa_lydo_+"','"+ghichu_+"','"+getUserVar("userID")+"','"
				+getUserVar("sys_agentcode")+"',0,1); "
			+ "   commit; "
			+ " end; "
		;		
		
		try{
			rs1_ = reqValue(xs1_);
		} catch (Exception ex) {
			ex.printStackTrace();	
			rs1_ ="0";		
		}
		
		if (rs1_=="0") {
			return new NEOCommonData("Loi khong dong bo duoc trang thai danh ba.");		
		}
		
		xs_ = " begin "
			+ " 	? := neo.pkg_4G.cat_mo_OC_IC_huy_HD('"
				+trth+"','"+goidi+"','"+goiden+"','"+so_tb+"','"
				+decMa_lydo_+"','"+ghichu_+"','"+getUserVar("userID")+"','"
				+getUserVar("sys_agentcode")+"'); "
			+ "   commit; "
			+ " end; "
		;	
		System.out.println(xs_);
		
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();			
		}
		
		return new NEOCommonData(rs_);
	}
	
	public NEOCommonData dmDV(String so_tb, String dsDvuThaydoi, String ghichu, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String dsDvuThaydoi_ = dsDvuThaydoi;//.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		String uid_ = getUserVar("userID");
		
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + "  ? := neo.pkg_4g.dong_mo_ds_dvu('"+so_tb_+"','"+dsDvuThaydoi_+"','"+ghichu_+"','"+uid_+"','"+getUserVar("sys_agentcode")+"'); ";
		xs_ = xs_ + " end; ";
	
		
		String rs_ = null;
		try{
			rs_ = reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}
	
}
