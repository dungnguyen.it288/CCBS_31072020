package neo.cmdv114;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import neo.smartui.report.NEOCommonData;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.prefs.Preferences;

import javax.sql.*;

public class vinacore_114 extends NEOProcessEx 
{
	public NEOCommonData chkTBTonTai(String so_tb)
	{
		if (getUserVar("userID")==null){
			return null;
		}
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_utils.CHECK_TB_TONTAI('"+so_tb+"'); ";
		xs_ = xs_ + " end; ";
		
        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	} 

	public NEOCommonData layTTThueBao(String so_tb)
	{
		/*String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_utils.LAYTT_THUEBAO(?); ";
		xs_ = xs_ + " end; ";
		
		xs_="begin open ? for select 1 so_tb from dual; end;";
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		//nei_.setDataSrc("CMDVOracleDS");
		nei_.bindParameter(2,so_tb);*/

		//RowSet rs_ = null;
		/*try{
			rs_ = reqRSet(nei_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}	*/
		if (getUserVar("userID")==null){
			return null;
		}
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_utils.LAYTT_THUEBAO('"+so_tb+"'); ";
		xs_ = xs_ + " end; ";
		
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		nei_.setDataSrc("CMDVOracleDS");		
		return new NEOCommonData(nei_);
	} 	

	public NEOCommonData layTTThueBao_v2(String so_tb )
	{
		String puserip =  getEnvInfo().getParserParam("sys_userip").toString();
		String puserid	=	getUserVar("userID");
		if (getUserVar("userID")==null){
			return null;
		}
		//Kiem tra quyen thuc hien tren menu
		try {
			String ckQuyen = this.reqValue("","begin ?:= neo.check_access_menu('"+puserid+"', '141'); end;"); 
			if(!ckQuyen.equals("1")) return null;
		}catch (Exception ex){}
		/*
		//Lay Flag va Seq tra cuu thue bao QLSP
		String resultFlag = null,resultSeq=null;
		try {
			resultFlag = this.reqValue("","begin ?:= subadmin.get_flag_qlsp(3); end;"); 
			resultSeq = this.reqValue("","begin ?:= subadmin.get_seq_qlsp(3); end;"); 
		}catch (Exception ex){} 

		String checkIsdn = "";
		if(resultFlag.equals("2")){
			String url = getSysConst("ServerQLSP");
	        String responseQLSP = null;
			try {
				responseQLSP = createWSCheckIsdnLite(url,so_tb,resultSeq);
				checkIsdn = getStringbyNode(responseQLSP,"result");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.laytt_thuebao_v5('"+checkIsdn+"','"+puserid+"','"+so_tb+"','"+puserip+"','"+puserip+"'); ";
		xs_ = xs_ + " end; ";*/
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.laytt_thuebao_v4('"+puserid+"','"+so_tb+"','"+puserip+"','"+puserip+"'); ";
		xs_ = xs_ + " end; ";
		
        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	} 	
	
	public static String createWSCheckIsdnLite(String purl, String pso_tb, String resultSeq) throws IOException{
		
		Preferences systemPref = Preferences.systemRoot();
		String sessionID = systemPref.get("sessionId", "sessionId" + " was not found.");
		
		String url = "http://"+purl+"/NumberStore";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("POST");
		con.setRequestProperty("Accept-Encoding", "gzip,deflate");
		con.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
		con.setRequestProperty("SOAPAction", "");
		con.setRequestProperty("Host", purl);
		con.setRequestProperty("Connection", "Keep-Alive");

		String urlParameters = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:NumberStore\">"
				+ "<soapenv:Header/>"
					+ "<soapenv:Body>"
						+ "<urn:checkIsdnLite soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
								+ "<sessionID xsi:type=\"xsd:string\">"+sessionID+"</sessionID>"
								+ "<p_ProcessID xsi:type=\"xsd:string\">"+resultSeq+"</p_ProcessID>"
								+ "<p_SystemID xsi:type=\"xsd:string\">2</p_SystemID>"
								+ "<p_isdn xsi:type=\"xsd:string\">"+pso_tb+"</p_isdn>"
						+ "</urn:checkIsdnLite>"
					+ "</soapenv:Body>"
				+ "</soapenv:Envelope>";
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		return response.toString();
	}
	
	public static String getStringbyNode(String xml, String node){
		int begin = xml.indexOf(node);
		int end = xml.lastIndexOf(node);
		String result = xml.substring(begin, end);
		 begin = result.indexOf(">")+1;
		 end = result.lastIndexOf("<");
		return result.substring(begin, end);
	}
	
	public NEOCommonData layTTThueBao1(String so_tb)
	{
		if (getUserVar("userID")==null){
			return null;
		}
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_utils.LAYTT_THUEBAO('"+so_tb+"'); ";
		xs_ = xs_ + " end; ";
		System.out.println(xs_);
        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	}

	public NEOCommonData layTTKhTb(String so_tb, String ma_tinh)
	{
		if (getUserVar("userID")==null){
			return null;
		}
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := pttb_ccbs.laytt_khtb('"+so_tb+"','"+ma_tinh+"'); ";
		xs_ = xs_ + " end; ";

		//return xs_;
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
                    nei_.setDataSrc("VNPCCBS_DBMS");
                    return new NEOCommonData(nei_);
	}
	
	public NEOCommonData n_layTTKhTb(String so_tb, String ma_tinh)
	  {
		if (getUserVar("userID")==null){
			return null;
		}
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := pttb_tracuu.laytt_khtb('"+so_tb+"','"+ma_tinh+"'); ";
		xs_ = xs_ + " end; ";
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
                    nei_.setDataSrc("VNPCCBS_DBMS");
                    return new NEOCommonData(nei_);
		}
	
	public NEOCommonData layTTNoCuoi(String so_tb)
	{
		if (getUserVar("userID")==null){
			return null;
		}
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := vinaxdc.xdc.laytt_nocuoi('"+so_tb+"'); ";
		xs_ = xs_ + " end; ";

        NEOExecInfo nei_ = new NEOExecInfo(xs_);
        nei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(nei_);
	} 	
}