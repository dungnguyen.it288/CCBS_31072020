package neo.cmdv114;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import neo.smartui.report.NEOCommonData;
//import cosaldap.Base64.*;

import java.util.*;
import java.io.*;
import javax.sql.*;
import java.sql.*;


public class catmo_dv extends NEOProcessEx
{

	//Lay ds chi tiet xu ly file Upload
	public String rec_layds_ctxl(String psUploadID)
	{
		return "/main?"+CesarCode.encode("configFile")+"=vinacore/cmdv114/dongmo_dv_file/ajax_layds_chitiet"
		+"&"+CesarCode.encode("upload_id")+"="+psUploadID
        ;
	}
	
	//Thuc hien gui yeu cau theo file
	public NEOCommonData value_thuchien_theofile(String pssotb,String psthaotac, String psdichvu , String psHengio, String psghichu,String psuser_id)
		{			
		String uid = getUserVar("userID");
		if (getUserVar("userID")==null){
			return null;
		}
		String out_ = "begin ?:= SUBADMIN.PKG_VSMS.catmo_dv_hengio('"+
							pssotb+"',"+
							"'"+psthaotac+"',"+
							"'"+psdichvu+"',"+
							"'"+psHengio+"',"+
							"'"+psghichu+"',"+
							"'"+uid+"'); end;";
		System.out.println(out_);
        NEOExecInfo ei_ = new NEOExecInfo(out_);
        ei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(ei_);				
	}
	
	//Thuc hien gui yeu cau hen gio
	public String value_thuchien_hengio(String pssotb,String psthaotac, String psdichvu , String psHengio, String psghichu,String psuser_id)
		{				
		/*String out_ = "begin ?:= SUBADMIN.PKG_VSMS.catmo_dv_hengio('"+
							pssotb+"',"+
							"'"+psthaotac+"',"+
							"'"+psdichvu+"',"+
							"'"+psHengio+"',"+
							"'"+psghichu+"',"+
							"'"+psuser_id+"'); end;"; */
		String uid = getUserVar("userID");
		if (getUserVar("userID")==null){
			return null;
		}
		String out_ = "begin ?:= SUBADMIN.PKG_VSMS.catmo_hengio('"+
							pssotb+"',"+
							"'"+psthaotac+"',"+
							"'"+psdichvu+"',"+
							"'"+psHengio+"',"+
							"'"+psghichu+"',"+
							"'"+uid+"'); end;"; 				
		System.out.println(out_);
		return out_;					
	}

	//Thuc hien gui yeu cau theo file cat mo dich vu
	public NEOCommonData value_thuchien_theofile_ioc(String psUploadID, String psthaotac, String psdichvu , String psghichu)
		{				
		String uid = getUserVar("userID");
		if (getUserVar("userID")==null){
			return null;
		}
		//Kiem tra quyen thuc hien tren menu
		try {
			String ckQuyen = this.reqValue("","begin ?:= neo.check_access_menu('"+uid+"', '1929'); end;"); 
			if(!ckQuyen.equals("1")) return null;
		}catch (Exception ex){}
		String out_ = "begin ?:= PKG_CATMO_DV.catmo_dv_file('"+
							psUploadID+"',"+
							"'"+psthaotac+"',"+
							"'"+psdichvu+"',"+
							"'"+psghichu+"',"+
							"'"+getUserVar("sys_agentcode")+"',"+
							"'"+getUserVar("userID")+"'); end;";

        NEOExecInfo ei_ = new NEOExecInfo(out_);
        ei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(ei_);					
	}
	//Lay thong tin sau khi kiem tra file UPload
	public NEOCommonData rec_laytt_kiemtra(String psUploadID){
		String out_ ="begin ?:= PKG_CATMO_DV.laytt_kiemtra_file('"+psUploadID+"'); end;";
        NEOExecInfo ei_ = new NEOExecInfo(out_);
        ei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(ei_);	
	}
	
	public String docDSTB() {
		String _userid = getUserVar("userID");
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/catmo_hengio/mod_list_tb";
		url_ = url_ + "&"+CesarCode.encode("user_id_")+"="+_userid;				
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	
		//Thuc hien xoa ban ghi hen gio
	public NEOCommonData value_delete_(String psuser_id,String psthutu)
		{				
		String uid = getUserVar("userID");
		if (getUserVar("userID")==null){
			return null;
		}
		String out_ = "begin ?:= SUBADMIN.PKG_VSMS.catmo_dv_delete('"+
							uid+"',"+
							"'"+psthutu+"'); end;";
		System.out.println(out_);
        NEOExecInfo ei_ = new NEOExecInfo(out_);
        ei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(ei_);					
	}	
	
	public NEOCommonData value_delete_hengio(String psuser_id,String psthutu)
		{				
		String uid = getUserVar("userID");
		if (getUserVar("userID")==null){
			return null;
		}
		String out_ = "begin ?:= SUBADMIN.PKG_VSMS.catmo_dv_hengio_delete('"+
							uid+"',"+
							"'"+psthutu+"'); end;";
		System.out.println(out_);
        NEOExecInfo ei_ = new NEOExecInfo(out_);
        ei_.setDataSrc("CMDVOracleDS");		

		return new NEOCommonData(ei_);					
	}	
	
}