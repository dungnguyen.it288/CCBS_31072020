package neo.cmdv114;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

import java.util.*;
import javax.sql.*;
import neo.qlsp.*;

public class khoitao_18001166 extends NEOProcessEx 
{
	
	public NEOCommonData kiemtra_MSIN_khoitao(String soMSIN, String matinh)
	{
		String msin_ = soMSIN.replace("'","''");
		String matinh_= matinh.replace("'","''");		
				
		String xs_ = "";
		xs_ = xs_ + " begin ";
		String pStatus = "";
		try {		
			Connect cl = new Connect();
			String sessionId = cl.readSession();
			String getInfo_ = cl.getSimInfo(sessionId.trim(),soMSIN);
			getInfo_= getInfo_.replace("||","#");
			String[] array_str = getInfo_.split("#");
			pStatus  = array_str[2];						
		} catch (Exception ex) {}
		xs_ = xs_ + " 	? := neo.khoitao_18001166.kiemtra_sim_khoitao('"+msin_+"','"+matinh_+"','"+pStatus+"'); "; 
		xs_ = xs_ + " end; ";
			
		RowSet rs_ = null;
		try{
			rs_ = reqRSet("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);		
	}
	
	public String timkiem_tk_numstore(String pPrefix, String so_tb, String pageNum)
	{
		String rs_ = "";
		try {
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"timkiem_tk_numstore_18001166", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return rs_;
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage()) ;
		}
				
		String userid_ = getUserVar("userID");
		String matinh_ = getUserVar("sys_agentcode");
		String url_ ="/main";
		
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/khoitao_18001166/mod_simtonumber";		
		url_ = url_ + "&"+CesarCode.encode("so_tb_")+"="+so_tb;		
		url_ = url_ + "&"+CesarCode.encode("userid_")+"="+userid_;
		url_ = url_ + "&"+CesarCode.encode("matinh_")+"="+matinh_;
		url_ = url_ + "&"+CesarCode.encode("pPrefix")+"="+pPrefix;
		url_ = url_ + "&"+CesarCode.encode("pageNum")+"="+pageNum;
		url_ = url_ +"&sid="+Math.random();
		return url_;
	} 
	
	public NEOCommonData thuchien_khoitao(String psMsin,String psSo_tb,String psDes,String psLoai)
	{		
		String _userid = getUserVar("userID");
		String matinh_ = getUserVar("sys_agentcode");
		String s1="";
		
		String psstatus ="";
		String pskieuso ="";
		String pscamket ="";
		String psthangck ="";
		String psstock_id ="";
		String psac_state ="";
		String psa4_ki ="";		
		String psalgorithm ="";
		
		//lay thong tin psac_state, psa4_ki, psalgorithm,  psstock_id,  psstatus,  pskieuso,  pscamket
		try {
			Connect cl = new Connect();
			String sessionId =cl.readSession();
			String getInfo_ = cl.checkIsdnLite(sessionId.trim(),psSo_tb);			
			String request_ = "";
			try {
				s1 = "begin ?:= admin_v2.pkg_chonso_18001166.checkIsdnFull('"+ getInfo_ +"'); end;";				
				request_=this.reqValue("",s1); 					
			}catch(Exception ex){
				System.out.println(">>>>>>----------String thong tin tu ket noi: Co loi "+ex.getMessage());	
			} 
			getInfo_ = request_; 					
			String[] array_str = getInfo_.split("#");
			psstatus  = array_str[1];
			psstock_id = array_str[2];
			pskieuso  = array_str[3];
			pscamket  = array_str[5];
			psthangck = array_str[6];
				
			getInfo_ = cl.getSimInfo(sessionId.trim(),psMsin);
			getInfo_= getInfo_.replace("||","#");
			array_str = getInfo_.split("#");
			psa4_ki = array_str[0];
			psalgorithm = array_str[1];	
			psac_state = array_str[2];								
		} catch (Exception ex) {}			
			
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= neo.khoitao_18001166.thuc_hien_khoitao('"+psMsin+"','"+psSo_tb+"','"+_userid+"','"+matinh_+"','"+psDes+"','"+psLoai+"','"+psac_state+"','"+psa4_ki+"','"+psstock_id+"','"+psstatus+"','"+pskieuso+"','"+pscamket+"','"+psthangck+"'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);
		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);    
		ei_.setDataSrc("CMDVOracleDS");			
		return new NEOCommonData(ei_);		
	}	
	 	
}