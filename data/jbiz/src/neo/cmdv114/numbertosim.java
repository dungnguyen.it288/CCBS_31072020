package neo.cmdv114;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

import java.util.*;
import javax.sql.*;

public class numbertosim extends NEOProcessEx 
{
	 
	 	public NEOCommonData check_thuchien_numstore_pps(String msin,String so_tb,String xacthuc,String psDescription, String loai)
	{		
		String _userid = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
	
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= subadmin.PKG_NUMBERS_STORE_PPS.thuc_hien_numstore_pps('"+msin+"','"+so_tb+"','"+xacthuc+"','"+_userid+"','"+matinh_ngdung_+"','"+psDescription+"','"+loai+"'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);
		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);    
		ei_.setDataSrc("CMDVOracleDS");			
		return new NEOCommonData(ei_);		
	}	
		
		/* ///OLD
		public NEOCommonData check_thuchien_numstore_pps_128(String msin,String so_tb,String xacthuc,String psDescription, String loai,String Lsim)
	{		
		String _userid = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
	
		String xs_ = "";
		xs_ = xs_ + " begin ";
		//TrinhHV: Cap nhat Khoi tao PPS loai Card, Xtra, Myzone 27/08/2013
		//xs_ = xs_ + " 	?:= subadmin.PKG_NUMBERS_STORE_PPS.thuc_hien_numstore_pps_sim('"+msin+"','"+so_tb+"','"+xacthuc+"','"+_userid+"','"+matinh_ngdung_+"','"+psDescription+"','"+loai+"','"+Lsim+"'); ";
		xs_ = xs_ + " 	?:= subadmin.PKG_NUMBERS_STORE_PPS.khoitao_numstore_pps('"+msin+"','"+so_tb+"','"+xacthuc+"','"+_userid+"','"+matinh_ngdung_+"','"+psDescription+"','"+loai+"','"+Lsim+"'); ";
		
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);
		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);    
		ei_.setDataSrc("CMDVOracleDS");			
		return new NEOCommonData(ei_);		
	}	*/
	////NEW_SPS
	public NEOCommonData check_thuchien_numstore_pps_128(String msin,String so_tb,String xacthuc,String psDescription, String loai,String Lsim)
	{		
		String _userid = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
	
		String xs_ = "";
		xs_ = xs_ + " begin ";
		//TrinhHV: Cap nhat Khoi tao PPS loai Card, Xtra, Myzone 27/08/2013
		//xs_ = xs_ + " 	?:= subadmin.PKG_NUMBERS_STORE_PPS.thuc_hien_numstore_pps_sim('"+msin+"','"+so_tb+"','"+xacthuc+"','"+_userid+"','"+matinh_ngdung_+"','"+psDescription+"','"+loai+"','"+Lsim+"'); ";
		//xs_ = xs_ + " 	?:= subadmin.pkg_initialize_subs_ccbs_2.khoitao_numstore_pps('"+msin+"','"+so_tb+"','"+xacthuc+"','"+_userid+"','"+matinh_ngdung_+"','"+psDescription+"','"+loai+"','"+Lsim+"'); ";
		xs_ = xs_ + " 	?:= neo.pkg_chonso_tratruoc.khoitao_numstore_pps('"+msin+"','"+so_tb+"','"+xacthuc+"','"+_userid+"','"+matinh_ngdung_+"','"+psDescription+"','"+loai+"','"+Lsim+"'); ";
		xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);
		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);    
		ei_.setDataSrc("CMDVOracleDS");			
		return new NEOCommonData(ei_);		
	}	
	////
	public NEOCommonData validateMaTB(String maTB)
	{		
		RowSet rs_ = null;
		try {
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"layTTThueBao", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return new NEOCommonData(rs_);
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage()) ;
		}
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		//xs_ = xs_ + " 	?:= subadmin.PKG_NUMBERS_STORE_PPS.check_thuebao_new('"+maTB+"','"+getUserVar("userID")+"'); ";
		xs_ = xs_ + " 	?:= neo.pkg_chonso_tratruoc.check_thuebao_new('"+maTB+"','"+getUserVar("userID")+"'); "; // ThuyVTT2 Sua cho chuc nang Khoi tao TB tra truoc kho QLSP
		xs_ = xs_ + " end; ";	
		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);  
		ei_.setDataSrc("CMDVOracleDS");					
		return new NEOCommonData(ei_);		
	} 
	
		public NEOCommonData validateMSIN(String soMSIN,String maTB)
	{		
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= subadmin.PKG_NUMBERS_STORE_PPS.check_msin('"+soMSIN+"','"+maTB+"'); ";
		xs_ = xs_ + " end; ";	
		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);  
		ei_.setDataSrc("CMDVOracleDS");					
		return new NEOCommonData(ei_);		
	} 
	
	public NEOCommonData validateMXT(String soMSIN, String maxt)
	{		
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= subadmin.PKG_NUMBERS_STORE_PPS.check_mxt('"+soMSIN+"','"+maxt+"'); ";
		xs_ = xs_ + " end; ";	
		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);  
		ei_.setDataSrc("CMDVOracleDS");					
		return new NEOCommonData(ei_);		
	} 
	
	//------------------------------------------------------------
	
	 
	public String timkiem_tk(String so_tb)
	{
		String userid_ = getUserVar("userID");
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumber/mod_simtonumber";
		url_ = url_ + "&"+CesarCode.encode("so_tb_")+"="+so_tb;
		url_ = url_ + "&"+CesarCode.encode("userid_")+"="+userid_;
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	
	
	public String timkiem_tk_numstore(String so_tb,String xac_thuc,String loai_kho)
	{
		String rs_ = "";
		try {
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"timkiem_tk_numstore", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return rs_;
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage()) ;
		}
		
		
		String userid_ = getUserVar("userID");
		String url_ ="/main";
		
		//url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumber_numstore/mod_simtonumber";
		
		//if ((loai_kho =="3")||(loai_kho =="4"))
		//{
			url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumber_numstore/mod_simtonumber_vtt";
		//}
		//else
		//{
		//	url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumber_numstore/mod_simtonumber";
		//}
		
		url_ = url_ + "&"+CesarCode.encode("so_tb_")+"="+so_tb;
		url_ = url_ + "&"+CesarCode.encode("xac_thuc_")+"="+xac_thuc;
		url_ = url_ + "&"+CesarCode.encode("userid_")+"="+userid_;
		url_ = url_ + "&"+CesarCode.encode("loai_kho")+"="+loai_kho;
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	public String timkiem_tk_numstore_qlsp(String pPrefix, String so_tb,String xac_thuc,String loai_kho)
	{
		String rs_ = "";
		try {
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"timkiem_tk_numstore", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return rs_;
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage()) ;
		}
		
		
		String userid_ = getUserVar("userID");
		String url_ ="/main";
		
		//url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumber_numstore/mod_simtonumber";
		
		//if ((loai_kho =="3")||(loai_kho =="4"))
		//{
			url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumber_numstore/mod_simtonumber_qlsp";
		//}
		//else
		//{
		//	url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumber_numstore/mod_simtonumber";
		//}
		
		url_ = url_ + "&"+CesarCode.encode("so_tb_")+"="+so_tb;
		url_ = url_ + "&"+CesarCode.encode("xac_thuc_")+"="+xac_thuc;
		url_ = url_ + "&"+CesarCode.encode("userid_")+"="+userid_;
		url_ = url_ + "&"+CesarCode.encode("loai_kho")+"="+loai_kho;
		url_ = url_ + "&"+CesarCode.encode("pPrefix")+"="+pPrefix;
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	
	public String timkiem_tk_numstore_next(String pPrefix, String so_tb,String xac_thuc,String loai_kho, String pageNum)
	{
		String rs_ = "";
		try {
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"timkiem_tk_numstore", getUserVar("userID"),
					getUserVar("sys_agentcode"),"100|1|30");
			if(!blnCheck)
				return rs_;
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage()) ;
		}
		
		
		String userid_ = getUserVar("userID");
		String url_ ="/main";
		
		//url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumber_numstore/mod_simtonumber";
		
		//if ((loai_kho =="3")||(loai_kho =="4"))
		//{
			url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumber_numstore/mod_simtonumber_qlsp_seq";
		//}
		//else
		//{
		//	url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumber_numstore/mod_simtonumber";
		//}
		
		url_ = url_ + "&"+CesarCode.encode("so_tb_")+"="+so_tb;
		url_ = url_ + "&"+CesarCode.encode("xac_thuc_")+"="+xac_thuc;
		url_ = url_ + "&"+CesarCode.encode("userid_")+"="+userid_;
		url_ = url_ + "&"+CesarCode.encode("loai_kho")+"="+loai_kho;
		url_ = url_ + "&"+CesarCode.encode("pPrefix")+"="+pPrefix;
		url_ = url_ + "&"+CesarCode.encode("pageNum")+"="+pageNum;
		url_ = url_ +"&sid="+Math.random();
		return url_;
	}
	/*
	public NEOCommonData check_thuchien_numstore(String msin,String so_tb,String xacthuc,String psDescription, String loai,String pkho)
	{		
		String _userid = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
	
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= subadmin.PKG_NUMBERS_STORE.thuc_hien_numstore_new2('"+msin+"','"+so_tb+"','"+xacthuc+"','"+_userid+"','"+matinh_ngdung_+"','"+psDescription+"','"+loai+"','"+pkho+"'); ";
		xs_ = xs_ + " end; ";	
			
		NEOExecInfo ei_ = new NEOExecInfo(xs_);    
		ei_.setDataSrc("CMDVOracleDS");			
		return new NEOCommonData(ei_);		
	}*/
	/* ---OLD
	public NEOCommonData check_thuchien_numstore(String msin,String so_tb,String xacthuc,String psDescription, String loai,String pkho,String pMeg)
	{		
		String _userid = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
	
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= subadmin.PKG_NUMBERS_STORE.thuc_hien_numstore_new3('"+msin+"','"+so_tb+"','"+xacthuc+"','"+_userid+"','"+matinh_ngdung_+"','"+psDescription+"','"+loai+"','"+pkho+"','"+pMeg+"'); ";
		xs_ = xs_ + " end; ";	
			
		NEOExecInfo ei_ = new NEOExecInfo(xs_);    
		ei_.setDataSrc("CMDVOracleDS");			
		return new NEOCommonData(ei_);		
	}*/
	// NEW_SPS
	public NEOCommonData check_thuchien_numstore(String msin,String so_tb,String xacthuc,String psDescription, String loai,String pkho,String pMeg)
	{		
		String _userid = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		String fd="";
		try {
			String s="begin ?:= numstore.pkg_khoitao.check_tb_freedoo('"+so_tb+"','"+_userid+"'); end;";		
			fd= this.reqValue("NUMSTOREDBMS",s);
			//fd= this.reqValue("",s);
		}catch (Exception ex){
			fd="0";
		}
		if (fd.equals("0")) {	
			String xs_ = "";
			xs_ = xs_ + " begin ";
			//xs_ = xs_ + " 	?:= subadmin.pkg_initialize_subs_ccbs.thuc_hien_numstore_new3('"+msin+"','"+so_tb+"','"+xacthuc+"','"+_userid+"','"+matinh_ngdung_+"','"+psDescription+"','"+loai+"','"+pkho+"','"+pMeg+"'); ";
			xs_ = xs_ + " 	?:= neo.pkg_chonso_trasau.thuc_hien_numstore_new('"+msin+"','"+so_tb+"','"+xacthuc+"','"+_userid+"','"+matinh_ngdung_+"','"+psDescription+"','"+loai+"','"+pkho+"','"+pMeg+"'); "; // code cho QLSP
			xs_ = xs_ + " end; ";	
				
			NEOExecInfo ei_ = new NEOExecInfo(xs_);    
			ei_.setDataSrc("CMDVOracleDS");			
			return new NEOCommonData(ei_);
		}
		else {
			return new NEOCommonData(fd);
			
		}	
	}
	/* ------------------------------------------------------------------------------------- */
		
	public NEOCommonData locked(String so_tb)
	{		
		String _userid = getUserVar("userID");
		
		//String _sys_userip = getUserVar("userIp");
	//	String clientIP_ = getRequest().getClientIp();
		//System.out.println("###################### "+ _sys_userip);
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= subadmin.PKG_NUMBERS_STORE.locked_numbers_store('1','"+so_tb+"'); ";
		xs_ = xs_ + " end; ";	
		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);  
		ei_.setDataSrc("CMDVOracleDS");					
		return new NEOCommonData(ei_);		
	}
	public NEOCommonData check_msin(String msin)
	{		
		String _userid = getUserVar("userID");
		
		//String _sys_userip = getUserVar("userIp");
	//	String clientIP_ = getRequest().getClientIp();
		//System.out.println("###################### "+ _sys_userip);
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= subadmin.PKG_NUMBERS_STORE.check_msin('"+msin+"'); ";
		xs_ = xs_ + " end; ";	
			
		NEOExecInfo ei_ = new NEOExecInfo(xs_);    
		ei_.setDataSrc("CMDVOracleDS");			
		return new NEOCommonData(ei_);		
	}
		public NEOCommonData check_thuchien(String msin,String so_tb, String psDescription, String loai)
	{		
		String _userid = getUserVar("userID");
		
		//String _sys_userip = getUserVar("userIp");
	//	String clientIP_ = getRequest().getClientIp();
		//System.out.println("###################### "+ _sys_userip);
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= subadmin.PKG_NUMBERS_STORE.thuc_hien('"+msin+"','"+so_tb+"','"+_userid+"','"+psDescription+"','"+loai+"'); ";
		xs_ = xs_ + " end; ";	
			
		NEOExecInfo ei_ = new NEOExecInfo(xs_);    
		ei_.setDataSrc("CMDVOracleDS");			
		return new NEOCommonData(ei_);		
	}
	//thongke bao cao
	
	public String layds_active(String fromdate,String todate)
	{
		String userid_ = getUserVar("userID");
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumber/mod_thongke";
		url_ = url_ + "&"+CesarCode.encode("userid")+"="+userid_;				
		url_ = url_ + "&"+CesarCode.encode("fromdate")+"="+fromdate;				
		url_ = url_ + "&"+CesarCode.encode("todate")+"="+todate;
		url_ = url_ +"&sid="+Math.random();
		System.out.println(url_);
		return url_;
	} 	

// Cac ham su dung cho gphone
	public String timkiem_tk_gphone(String so_tb)
	{
		String userid_ = getUserVar("userID");
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumbergphone/mod_simtonumbergphone";
		url_ = url_ + "&"+CesarCode.encode("so_tb_")+"="+so_tb;				
		url_ = url_ + "&"+CesarCode.encode("userid_")+"="+userid_;				
		url_ = url_ +"&sid="+Math.random();
		return url_;
	} 	
	
		
	public NEOCommonData check_thuchien_gphone(String msin,String so_tb, String psDescription,String loai)
	{		
		String _userid = getUserVar("userID");		
		//String _sys_userip = getUserVar("userIp");
	//	String clientIP_ = getRequest().getClientIp();
		//System.out.println("###################### "+ _sys_userip);
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= subadmin.PKG_NUMBER_GPHONE_STORE.thuc_hien('"+msin+"','"+so_tb+"','"+_userid+"','"+psDescription+"','"+loai+"'); ";
		xs_ = xs_ + " end; ";	
		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);    
		ei_.setDataSrc("CMDVOracleDS");			
		return new NEOCommonData(ei_);		
	}
		//lay ds loai thue bao khoi tao
	public String layds_loaitb_theokho(String psloaikho)
	{
		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=vinacore/simtonumber_numstore/ds_loaitb_sql";
		url_ = url_ + "&"+CesarCode.encode("psloaikho")+"="+psloaikho;								
		url_ = url_ +"&sid="+Math.random();
		return url_;
	} 
	
	public NEOCommonData kiemtra_MSIN_khoitao(String soMSIN, String matinh)
	{
		String msin_ = soMSIN.replace("'","''");
		String matinh_= matinh.replace("'","''");		
		
		String xs_ = "";
		xs_ = xs_ + " begin ";
		//xs_ = xs_ + " 	? := subadmin.PKG_NVUCSC.kiemtra_sim_khoitao('"
		//	+msin_+"','"+matinh_+"'); ";
			
		xs_ = xs_ + " 	? := neo.pkg_chonso_tratruoc.kiemtra_sim_khoitao('"	+msin_+"','"+matinh_+"'); "; 
		xs_ = xs_ + " end; ";
	
		System.out.println(xs_);
		RowSet rs_ = null;
		try{
			rs_ = reqRSet("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);		
	}
	public NEOCommonData check_thuchien_kt(String so_tb) {
        String _userid = getUserVar("userID");
        String matinh_ngdung_ = getUserVar("sys_agentcode");
        //check la thue bao tra truoc moi duoc khoi tao 
        String sql = "";

        sql = " begin ?:=numstore.pkg_khoitao.check_tb_khoitao('" + so_tb + "'); end;";
        
        NEOExecInfo ei_ = new NEOExecInfo(sql);
		ei_.setDataSrc("NUMSTOREDBMS");
       
        return new NEOCommonData(ei_);

        
    }
}