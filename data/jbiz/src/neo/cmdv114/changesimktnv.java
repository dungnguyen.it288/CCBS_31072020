package neo.cmdv114;

import neo.smartui.process.*;
import neo.smartui.report.*;
import javax.sql.*;
import neo.smartui.common.CesarCode;
import java.util.concurrent.TimeUnit;
public class changesimktnv extends NEOProcessEx 
{
	

	public NEOCommonData layTTThueBao(String so_tb)
	{
		RowSet rs_ = null;
		try {
			boolean blnCheck = neo.smartui.SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"layTTThueBao", getUserVar("userID"),
					getUserVar("sys_agentcode"),"30|1|30");
			if(!blnCheck)
				return new NEOCommonData(rs_);
			reqValue("begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
					+so_tb+"','layTTThueBao'," 
					+"'"+so_tb+";',"
					+"'','"
					+getUserVar("userID")+"','"
					+getEnvInfo().getParserParam("sys_userip")+"','"
					+getUserVar("sys_agentcode")+"'); "
					+ "   commit; "
					+ " end; ");
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage()) ;
			// TODO: handle exception
		}
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := subadmin.pkg_change_sim_ktnv.LAYTT_THUEBAO(?,?,?); ";
		xs_ = xs_ + " end; ";
		String agent = getUserVar("sys_agentcode"); 
		String userid = getUserVar("userID"); 
		
		
		NEOExecInfo nei_ = new NEOExecInfo(xs_);
		//Thuyvtt comment cho test ko cho thue bao VCC doi sim
		nei_.setDataSrc("CMDVOracleDS"); 
		nei_.bindParameter(2,so_tb);
		nei_.bindParameter(3,userid);
		nei_.bindParameter(4,agent);
		try{
			rs_ = reqRSet(nei_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println(xs_);
		
		return new NEOCommonData(rs_);

	}
	public String accept_upload_ktlai(String psUploadID, String use_schema, String com_schema, String psuserip){				
		String s=  "begin ?:= admin_v2.pkg_upload_service.accept_upload_ktlai("
			+"'"+ psUploadID+"',"	
			+"'"+use_schema+ "'," 				
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";
		    
	
	 	System.out.println(s);
		String rs_ = null;
		try{
			rs_ =reqValue("", s);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return rs_;
	}
	public NEOCommonData chkSMOI(String so_tb, String so_msin_moi)
	{
		String so_tb_ = so_tb.replace("'","''");
		String so_msin_moi_ = so_msin_moi.replace("'","''");
		String matinh_ngdung_ = getUserVar("sys_agentcode");

		String xs_ = "";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	? := 	subadmin.pkg_change_sim_ktnv.doi_sim_ktra_SIM_moi('"+so_tb_+"','"+so_msin_moi_+"','"+matinh_ngdung_+"'); ";
		xs_ = xs_ + " end; ";

		RowSet rs_ = null;
		try{
			rs_ = reqRSet("CMDVOracleDS", xs_);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);		
	}
	public NEOCommonData dsimtb(String so_tb, String so_msin_moi, int simfree, String ghichu, String uid)
	{
		String so_tb_ = so_tb.replace("'","''");
		String so_msin_moi_ = so_msin_moi.replace("'","''");
		String ghichu_ = ghichu.replace("'","''");
		String uid_ = getUserVar("userID");
	
		String xs_ = "";
		xs_ = xs_ + " begin ";
		
		xs_ = xs_ + " 	? := subadmin.pkg_change_sim_ktnv.doi_sim_tb('"+so_tb_+"','"+so_msin_moi_+"',"+simfree+",'"+ghichu_+"','"+uid_+"','"+getUserVar("sys_agentcode")+"'); ";
		
		xs_ = xs_ + " end; ";
      
		String rs_ = null;
		try{
			rs_ =reqValue("CMDVOracleDS", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}
	public NEOCommonData rec_upload_changesim(String psUploadID){
	String s  ="begin ?:= admin_v2.pkg_upload_service.laytt_kiemtra_changesim('"+psUploadID+"'); end;";

	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	// nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
	}
	
	public NEOCommonData value_accept_upload_changesim(String psUploadID, String use_schema, String com_schema, String psuserip){				
		String s=  "begin ?:= admin_v2.pkg_upload_service.accept_upload_changesim("
			+"'"+ psUploadID+"',"	
			+"'"+use_schema+ "'," 				
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";
			
	     NEOExecInfo nei_ = new NEOExecInfo(s);
	
	 	return new NEOCommonData(nei_);
	}
	
	public String itc_khoitaolai(String psMsisdn, String msidn,String subtype, String kittpe, String ghichu, String psuserid, String psuserip) throws Exception{				
		String s=  "begin ?:= subadmin.pkg_change_sim_ktnv.khoitaolai_tbitc_kit("
			+"'"+ psMsisdn+"',"			
			+"'"+ msidn+"',"
			+"'"+ subtype+"',"
			+"'"+ kittpe+"',"			
			+"'"+ghichu+ "'," 				
			+"'"+getUserVar("userID")+"',"			
			+"'"+psuserip+"',"
			+"'"+getUserVar("sys_agentcode")+"'); end;";
		String result = this.reqValue("CMDVOracleDS", s);
		return result;
	}
	
	public NEOCommonData layds_upload_changesim_error(String uploadId, String agent){
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=vinacore/doi_sim_ktnv/upload_doisim/ds_upload_error&" + CesarCode.encode("uploadId") + "="+uploadId+"&agent="+agent;
		NEOExecInfo nei_ = new NEOExecInfo(url_); 		
    	return new NEOCommonData(nei_);
	}
	public NEOCommonData rec_upload_cansub(String psUploadID){
	String s  ="begin ?:= admin_v2.pkg_upload_service.laytt_kiemtra_cansub('"+psUploadID+"'); end;";

	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	// nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
	}
	
	public NEOCommonData value_accept_upload_cansub(String psUploadID, String use_schema, String com_schema, String psuserip){				
		String s=  "begin ?:= admin_v2.pkg_upload_service.accept_upload_cansub("
			+"'"+ psUploadID+"',"	
			+"'"+use_schema+ "'," 				
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";
			
	     NEOExecInfo nei_ = new NEOExecInfo(s);
	
	 	return new NEOCommonData(nei_);
	}
	
	
	
	public NEOCommonData layds_upload_cansub_error(String uploadId, String agent){
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=vinacore/doi_sim_ktnv/upload_tb_can/ds_upload_error&" + CesarCode.encode("uploadId") + "="+uploadId+"&agent="+agent;
		NEOExecInfo nei_ = new NEOExecInfo(url_); 		
    	return new NEOCommonData(nei_);
	}
	public String itc_khoitaolai(String psMsisdn, String msidn, String ghichu, String psuserid, String psuserip) throws Exception{				
		String s=  "begin ?:= subadmin.pkg_change_sim_ktnv.khoitaolai_tbitc("
			+"'"+ psMsisdn+"',"
			+"'"+ msidn+"',"				
			+"'"+ghichu+ "'," 				
			+"'"+getUserVar("userID")+"',"			
			+"'"+psuserip+"',"
			+"'"+getUserVar("sys_agentcode")+"'); end;";
		String result = this.reqValue("CMDVOracleDS", s);
		return result;
	}
		public NEOCommonData rec_upload_ktlai(String psUploadID){
	String s  ="begin ?:= admin_v2.pkg_upload_service.laytt_kiemtra_ktlai('"+psUploadID+"'); end;";

	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	// nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
	}
	
	public NEOCommonData value_accept_upload_ktlai(String psUploadID, String use_schema, String com_schema, String psuserip){				
		String s=  "begin ?:= admin_v2.pkg_upload_service.accept_upload_ktlai("
			+"'"+ psUploadID+"',"	
			+"'"+use_schema+ "'," 				
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";
		
	     NEOExecInfo nei_ = new NEOExecInfo(s);
	
	 	return new NEOCommonData(nei_);
	}
	
	
	
	public NEOCommonData layds_upload_ktlai_error(String uploadId, String agent){
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=vinacore/doi_sim_ktnv/khoitaolai_file/ds_upload_error&" + CesarCode.encode("uploadId") + "="+uploadId+"&agent="+agent;
		NEOExecInfo nei_ = new NEOExecInfo(url_); 		
    	return new NEOCommonData(nei_);
	}
		public NEOCommonData rec_upload_ktvnp(String psUploadID){
	String s  ="begin ?:= admin_v2.pkg_upload_service.laytt_kiemtra_ktvnp('"+psUploadID+"'); end;";

	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	// nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
	}
	
	public NEOCommonData value_accept_upload_ktvnp(String psUploadID, String use_schema, String com_schema, String psuserip){				
		String s=  "begin ?:= admin_v2.pkg_upload_service.accept_upload_ktvnp("
			+"'"+ psUploadID+"',"	
			+"'"+use_schema+ "'," 				
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";
		
	     NEOExecInfo nei_ = new NEOExecInfo(s);
	
	 	return new NEOCommonData(nei_);
	}
	
	
	
	public NEOCommonData layds_upload_ktvnp_error(String uploadId, String agent){
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=vinacore/khoitao_theofile/ds_upload_error&" + CesarCode.encode("uploadId") + "="+uploadId+"&agent="+agent;
		NEOExecInfo nei_ = new NEOExecInfo(url_); 		
    	return new NEOCommonData(nei_);
	}
}