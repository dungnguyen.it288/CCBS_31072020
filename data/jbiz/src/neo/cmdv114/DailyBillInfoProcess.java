package neo.cmdv114;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

import com.sun.rowset.CachedRowSetImpl;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Connection;
import javax.sql.*;
import javax.sql.rowset.*;
import java.sql.SQLException;
import java.util.Date;
import org.mozilla.javascript.ImporterTopLevel;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.Context;

import java.text.SimpleDateFormat;
import java.text.NumberFormat;
import java.text.DecimalFormat;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class DailyBillInfoProcess extends NEOProcessEx {

    String to_char(double dbln, String spt)
    {
        DecimalFormat _dfFloat;

        if (spt == null) {
            _dfFloat = new DecimalFormat("###,###,##0.000");
        } else {
            _dfFloat = new DecimalFormat(spt);
        }

        return _dfFloat.format(dbln);
    }

    String to_char(Date dobj)
    {
        if (dobj == null) return "";
        SimpleDateFormat sdf_ = new SimpleDateFormat("dd/MM/yyyy");
        return sdf_.format(dobj);
    }

    public RowSet LayTTCuocNong(String so_tb, String thang, String nam) {
        RowSet result_ = null;
		
		return result_;

        String so_tb_ = so_tb;
        String mm_ = thang;
        String yy_ = nam;
        System.out.println("so_tb_: " + so_tb_);
        System.out.println("mm_: " + mm_);
        System.out.println("yy_: " + yy_);

        Date sNgayCuocCuoi_ = null;
        String sTenFileCuoc_ = "";

        double iCall_ = 0;
        double iBlock_ = 0;
        double iKbyte_ = 0;
        double iNatl_gprs_ = 0;
        double iNational_ = 0;
        double iIntl_ = 0;
        String sMatinh_ = "";
        String sKv_ = "";
        String sNvu_ = "";
        double iBlk_free_ = 0;

        double iIr_National_ = 0;
        double iIr_Intl_ = 0;
        double iIr_No_vat_ = 0;

        double iAir_ti_ = 0; //can xem lai nghiep vu cua bien nay

        double iIntl_VND_ = 0;
        double iIr_Intl_VND_ = 0;
		
        RowSet recToDay_, recCuoc_, recIrCall_;
        String sql_ = "";

        mm_ = mm_.trim();
        yy_ = yy_.trim();
        String mmyy_ = mm_ + yy_;

        try {
            sql_ = "select * from today where thang = '" + mmyy_ + "'";
            recToDay_ = reqRSet("DailyBill_ODBC", sql_);

            if (recToDay_.next())
            {
                sNgayCuocCuoi_ = recToDay_.getDate("Ngay");
                sTenFileCuoc_ = recToDay_.getString("Ten").trim();
            }
            //System.out.println("stToDay_ ok ");

            if ((sTenFileCuoc_ != null) && (sTenFileCuoc_.length() > 0)) {
				try {
			
					sql_ = "select * from " + sTenFileCuoc_
                           + " where A_Subs = '" + so_tb_ + "'";
					recCuoc_ = reqRSet("DailyBill_ODBC", sql_);
                
					if (recCuoc_.next()) {
                        iCall_ = recCuoc_.getDouble("Call");
                        iBlock_ = recCuoc_.getDouble("Block");
                        iKbyte_ = recCuoc_.getDouble("Kbyte");
                        iNatl_gprs_ = recCuoc_.getDouble("Natl_gprs");
                        iNational_ = recCuoc_.getDouble("National");
						//System.out.println("iNational_: "+iNational_);
                        iIntl_ = recCuoc_.getDouble("Intl");
                        iIntl_VND_ = recCuoc_.getDouble("Intl_VND");
                        sMatinh_ = recCuoc_.getString("Matinh");
                        sKv_ = recCuoc_.getString("Kv");
                        sNvu_ = recCuoc_.getString("Nvu");
                        iBlk_free_ = recCuoc_.getDouble("Blk_free");
					}
                    //System.out.println("stCuoc_ ok ");
                    //
					sql_ = "select sum(National) IR_National, sum(Intl) IR_Intl "
                            + " , sum(No_vat) IR_No_vat, sum(Intl_VND) IR_Intl_VND "
                            + " from ircalls where phone='" + so_tb_ + "' "
                            + " and hdthang = '" + mm_ + "/20" + yy_ + "' ";
					recIrCall_ = reqRSet("DailyBill_ODBC", sql_);                
					if (recIrCall_.next()) {
                        iIr_National_ = recIrCall_.getDouble("IR_National");
                        iIr_Intl_ = recIrCall_.getDouble("IR_Intl");
                        iIr_No_vat_ = recIrCall_.getDouble("IR_No_vat");
                        iIr_Intl_VND_ = recIrCall_.getDouble("IR_Intl_VND");
                        iIr_Intl_ += iIr_No_vat_/1.1;
					}
				} catch (Exception e) {}				
            } 

			double iSumVND_ = iNational_ + iIr_National_ + iNatl_gprs_ + iAir_ti_ + iIntl_VND_ + iIr_Intl_VND_;
            double iSumUSD_ = iIntl_ + iIr_Intl_;
              
			sql_ = "select  "
                       + "'" + to_char(sNgayCuocCuoi_) + "' ngayCuocCuoi, "
                       + "'" + to_char(iBlock_,"###,###,###") + "' Block, "
                       + "'" + to_char(iNational_,null) + "' National, "
                       //+ "'" + to_char(0,null) + "' National, "
                       + "'" + to_char(iNatl_gprs_,null) + "' Natl_gprs, "
                       + "'" + to_char(iIntl_,null) + "' Intl, "
                       + "'" + to_char(iIntl_VND_,null) + "' Intl_VND, "
                       + "'" + to_char(iAir_ti_,null) + "' Air_ti, "
                       + "'" + to_char(iIr_Intl_,null) + "' Ir_Intl, "
                       + "'" + to_char(iIr_Intl_VND_,null) + "' Ir_Intl_VND, "
                       + "'" + to_char(iSumVND_,null) + "' SumVND, "
                       + "'" + to_char(iSumUSD_,null) + "' SumUSD "
                       + " from today where thang = '0606'";
            result_ = reqRSet("DailyBill_ODBC", sql_);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //
        return result_;
    }
/*
    public static void main(String[] agrs)
    {
        try
        {
            long t1 = System.currentTimeMillis();
            BillDbfDataProcessEx bx = new BillDbfDataProcessEx();
            RowSet rs_ = bx.LayTTCuocNong("84913615413","12","06");
            long t2 = System.currentTimeMillis();
            while (rs_.next())
            {
                System.out.println(rs_.getString(1));
                System.out.println(rs_.getString(2));
            }
            System.out.println("Exec time: " + (t2-t1) + " (ms)");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//*/	
}