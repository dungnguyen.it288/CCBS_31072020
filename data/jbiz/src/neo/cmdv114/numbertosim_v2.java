package neo.cmdv114;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import java.util.*;
import javax.sql.*;

public class numbertosim_v2 extends NEOProcessEx 
{	 	 			
	public NEOCommonData thuchien_numstore(String msin,String so_tb,String psDescription, String loai)
	{		
		String _userid = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
	
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= subadmin.PKG_NUMBERS_STORE.thuc_hien_numstore_v2('"+msin+"','"+so_tb+"','"+_userid+"','"+matinh_ngdung_+"','"+psDescription+"','"+loai+"'); ";
		xs_ = xs_ + " end; ";	
			
		NEOExecInfo ei_ = new NEOExecInfo(xs_);    
		ei_.setDataSrc("CMDVOracleDS");			
		return new NEOCommonData(ei_);		
	}	
		
	public NEOCommonData check_msisdn_new(String so_tb,String mxt,String user)
	{		
		String _userid = getUserVar("userID");				
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= dms_admin.ADMIN_SEARCH.check_numbers_store('"+so_tb+"','"+mxt+"','"+user+"'); ";
		xs_ = xs_ + " end; ";	
		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);  
		ei_.setDataSrc("NUMSTOREOracleDS");					
		return new NEOCommonData(ei_);		
	}
		
}