package neo.cmdv114;
import javax.sql.RowSet;

import neo.ccbs_export;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class wifiroaming extends NEOProcessEx {

    public void run() {
        System.out.println("neo.wifiroaming was called");
    }
    

	public NEOCommonData rec_upload_khuyenmai(String psUploadID){
	String s  ="begin ?:= admin_v2.pkg_exp_wifiroaming.laytt_kiemtra_wr('"+psUploadID+"'); end;";

	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	// nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
	}
	
	public NEOCommonData value_accept_upload_khuyenmai(String psUploadID, String use_schema, String com_schema, String psuserip){				
		String s=  "begin ?:= admin_v2.pkg_exp_wifiroaming.accept_upload_wr("
			+"'"+ psUploadID+"',"	
			+"'"+use_schema+ "'," 				
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";
			
	     NEOExecInfo nei_ = new NEOExecInfo(s);
	
	 	return new NEOCommonData(nei_);
	}
	
	
	
	public NEOCommonData layds_upload_error(String uploadId, String agent){
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=vinacore/wifiroaming/upload_url/ds_upload_error&" + CesarCode.encode("uploadId") + "="+uploadId+"&agent="+agent;
		NEOExecInfo nei_ = new NEOExecInfo(url_); 		
    	return new NEOCommonData(nei_);
	}
	public String resendUrlSMS (String pc_id,
								String pc_userid,
								String pc_userip) throws Exception{
		String result =null;
		String s = " begin ? := admin_v2.pkg_exp_wifiroaming.ResendUrlSMS("
						+ "'" + pc_id + "'"									
						+ ",'" + pc_userid + "'"
						+ ",'" + pc_userip+"'); end;";
		result = this.reqValue("", s);
		return result;
	}
	public NEOCommonData rec_upload_khdn(String psUploadID){
	String s  ="begin ?:= admin_v2.pkg_exp_wifiroaming.laytt_kiemtra_wr_khdn('"+psUploadID+"'); end;";

	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	// nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
	}
	
	public NEOCommonData value_accept_upload_khdn(String psUploadID, String use_schema, String com_schema, String psuserip){				
		String s=  "begin ?:= admin_v2.pkg_exp_wifiroaming.accept_upload_wr_khdn("
			+"'"+ psUploadID+"',"	
			+"'"+use_schema+ "'," 				
			+"'"+getUserVar("userID")+"',"
			+"'"+psuserip+"'); end;";
			
	     NEOExecInfo nei_ = new NEOExecInfo(s);
	
	 	return new NEOCommonData(nei_);
	}
	
	
	
	public NEOCommonData layds_upload_khdn_error(String uploadId, String agent){
		String url_ =  "/main?";
		url_ = url_ + CesarCode.encode("configFile") + "=vinacore/wifiroaming/upload_khdn/ds_upload_error&" + CesarCode.encode("uploadId") + "="+uploadId+"&agent="+agent;
		NEOExecInfo nei_ = new NEOExecInfo(url_); 		
    	return new NEOCommonData(nei_);
	}
	
	
	
}