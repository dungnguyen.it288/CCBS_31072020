package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;



public class Inhoadon extends NEOProcessEx {
  public void run() {
    System.out.println("neo.inhoadon was called");
  }

  
  public NEOCommonData Themmoi_VAT(String schema ,  						   			        
					        String   psanyTT,
					        String   psanyKH,
					        String   psanySM,
					        String   psngayin,
					        String   psthangin,                
					        String   psdonviqlpsid,
					        String   psloaikh_id,
					        String   pssvr,
					        String   psbanke_id,
							String   pspwd,
							String   isVpeText
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schema)+"Inhoadon.Themmoi_VAT('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psanyTT+"'"                      
        +",'"+psanyKH+"'"
        +",'"+psanySM+"'"
        +",'"+psngayin+"'"
        +",'"+psthangin+"'"
		+",'"+psdonviqlpsid+"'"
		+",'"+psloaikh_id+"'"	
		+",'"+pssvr+"'"
		+",'"+psbanke_id+"'"
		+",'"+pspwd+"'"
		+",'"+isVpeText+"'"
        +");"
        +"end;"
        ;
    System.out.println(s); 
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);

  }
  
  
  public NEOCommonData Themmoi_cuoc_tb(String schema ,  						   
					        String   psngayin,
					        String   psthangin,                
					        String   psdonviqlpsid,
					        String   psdoituong_id,
					        String   pssvr,
					        String   psbanke_id,
							String   pspwd
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schema)+"Inhoadon.Themmoi_cuoc_tb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psngayin+"'"              
        +",'"+psthangin+"'"
		+",'"+psdonviqlpsid+"'"
		+",'"+psdoituong_id+"'"	
		+",'"+pssvr+"'"
		+",'"+psbanke_id+"'"
		+",'"+pspwd+"'"
        +");"
        +"end;"
        ;
    System.out.println(s);
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);
  }
  
  
  
  public NEOCommonData Themmoi_km(String schema ,  						   				        
					        String   psanyTT,
					        String   psanyKH,
					        String   psanySM,
					        String   psngayin,
					        String   psthangin,                
					        String   psdonviqlpsid,
					        String   psloaikh_id,
					        String   pssvr,
					        String   psbanke_id,
							String   pspwd,
							String   isVpeText
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schema)+"Inhoadon.Themmoi_km('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psanyTT+"'"             
        +",'"+psanyKH+"'"
        +",'"+psanySM+"'"
        +",'"+psngayin+"'"
        +",'"+psthangin+"'"
		+",'"+psdonviqlpsid+"'"
		+",'"+psloaikh_id+"'"	
		+",'"+pssvr+"'"
		+",'"+psbanke_id+"'"
		+",'"+pspwd+"'"	
		+",'"+isVpeText+"'"	
        +");"
        +"end;"
        ;
    System.out.println(s);
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);
  }
  
  
  public NEOCommonData Themmoi_CT(String schema ,  						   				        
					        String   psanyTT,
					        String   psanyKH,
					        String   psanySM,
					        String   psngayin,
					        String   psthangin,                
					        String   psdonviqlpsid,					        
					        String   psloaikh_id,					        
					        String   pskhoanmuctt_id,
					        String   pssvr,
					        String   psbanke_id,
							String   pspwd,
							String   isVpeText,
							String   pschk_khongdau
							
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schema)+"Inhoadon.Themmoi_CT('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psanyTT+"'"                
        +",'"+psanyKH+"'"
        +",'"+psanySM+"'"
        +",'"+psngayin+"'"
        +",'"+psthangin+"'"
		+",'"+psdonviqlpsid+"'"	
		+",'"+psloaikh_id+"'"		
		+",'"+pskhoanmuctt_id+"'"
		+",'"+pssvr+"'"		
		+",'"+psbanke_id+"'"	
		+",'"+pspwd+"'"
		+",'"+isVpeText+"'"	
		+",'"+pschk_khongdau+"'"
        +");"
        +"end;"
        ;
    System.out.println(s);
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);

  }
  
   public NEOCommonData Themmoi_pbc_hcm(String schema ,
  						    String	 psfromTT,						
							String	 pstoTT,
					   		String   psfromKH,
							String	 pstoKH,
					        String   psfromSM,
					        String   pstoSM,					        
					        String   psanyTT,
					        String   psanyKH,
					        String   psanySM,
					        String   psngayin,
					        String   psthangin,                
					        String   psdonviqlpsid,
					        String   pschk_inchitiet,
					        String   psdoituong_id,					        
					        String   pskhoanmuctt_id,
					        String   psphuong_id,
					        String   pssvr,
					        String   psbanke_id,
							String   pspwd,
							String   isVpeText,
							String   pschk_khongdau
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schema)+"Inhoadon.Themmoi_pbc_hcm('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psfromTT+"'"
        +",'"+pstoTT+"'"
        +",'"+psfromKH+"'"        
        +",'"+pstoKH+"'"
        +",'"+psfromSM+"'"
        +",'"+pstoSM+"'"        
        +",'"+psanyTT+"'"        
        +",'"+psanyKH+"'"
        +",'"+psanySM+"'"
        +",'"+psngayin+"'"
        +",'"+psthangin+"'"
		+",'"+psdonviqlpsid+"'"
		+",'"+pschk_inchitiet+"'"
		+",'"+psdoituong_id+"'"		
		+",'"+pskhoanmuctt_id+"'"
		+",'"+psphuong_id+"'"
		+",'"+pssvr+"'"		
		+",'"+psbanke_id+"'"
		+",'"+pspwd+"'"
		+",'"+isVpeText+"'"
		+",'"+pschk_khongdau+"'"
        +");"
        +"end;"
        ;
    System.out.println(s);
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);

  }
  
  
  
     public NEOCommonData Themmoi_ct_hcm(String schema ,
  						    String	 psfromTT,						
							String	 pstoTT,
					   		String   psfromKH,
							String	 pstoKH,
					        String   psfromSM,
					        String   pstoSM,					        
					        String   psanyTT,
					        String   psanyKH,
					        String   psanySM,
					        String   psngayin,
					        String   psthangin,                
					        String   psdonviqlpsid,
					        String   pschk_inchitiet,
					        String   psdoituong_id,					        
					        String   pskhoanmuctt_id,
					        String   psphuong_id,
					        String   pssvr,
					        String   psbanke_id,
							String   pspwd,
							String   isVpeText,
							String   pschk_khongdau
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schema)+"Inhoadon.Themmoi_ct_hcm('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psfromTT+"'"
        +",'"+pstoTT+"'"
        +",'"+psfromKH+"'"        
        +",'"+pstoKH+"'"
        +",'"+psfromSM+"'"
        +",'"+pstoSM+"'"        
        +",'"+psanyTT+"'"        
        +",'"+psanyKH+"'"
        +",'"+psanySM+"'"
        +",'"+psngayin+"'"
        +",'"+psthangin+"'"
		+",'"+psdonviqlpsid+"'"
		+",'"+pschk_inchitiet+"'"
		+",'"+psdoituong_id+"'"		
		+",'"+pskhoanmuctt_id+"'"
		+",'"+psphuong_id+"'"
		+",'"+pssvr+"'"		
		+",'"+psbanke_id+"'"
		+",'"+pspwd+"'"
		+",'"+isVpeText+"'"
		+",'"+pschk_khongdau+"'"
        +");"
        +"end;"
        ;
    System.out.println(s);
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);
  }
  
  
public NEOCommonData Themmoi_ct_daily_hcm(String schema ,
  						    String   psanyKH,
					        String   psanySM,
					        String   psngayin,
					        String   psthangin,                					        					        
					        String   pssvr,
					        String   psbanke_id,
							String   pspwd,
							String   isVpeText,
							String   pschk_khongdau
					               
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schema)+"Inhoadon.Themmoi_ct_daily_hcm('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psanyKH+"'"             
        +",'"+psanySM+"'"
        +",'"+psngayin+"'"
        +",'"+psthangin+"'"			
		+",'"+pssvr+"'"		
		+",'"+psbanke_id+"'"
        +",'"+pspwd+"'"		
		+",'"+isVpeText+"'"
		+",'"+pschk_khongdau+"'"
        +");"
        +"end;"
        ;
    System.out.println(s);
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);

  }
  
    
public NEOCommonData Themmoi_pbc_daily_hcm(String schema ,
  						    String   psanyKH,
					        String   psanySM,
					        String   psngayin,
					        String   psthangin,                					        					      
					        String   pssvr,
					        String   psbanke_id,
							String   pspwd,
							String   isVpeText,
							String   pschk_khongdau
					               
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schema)+"Inhoadon.Themmoi_pbc_daily_hcm('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psanyKH+"'"             
        +",'"+psanySM+"'"
        +",'"+psngayin+"'"
        +",'"+psthangin+"'"			
		+",'"+pssvr+"'"		
		+",'"+psbanke_id+"'"
		+",'"+pspwd+"'"	
		+",'"+isVpeText+"'"
		+",'"+pschk_khongdau+"'"		
        +");"
        +"end;"
        ;
    System.out.println(s);
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);

  }
  
  
   public NEOCommonData Themmoi_data_post(String schema ,
  						    String   psthangin,
  						    String   pssvr,
					        String   psbanke_id,
							String   pspwd		
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schema)+"Inhoadon.Themmoi_data_post('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psthangin+"'"
		+",'"+pssvr+"'"		
		+",'"+psbanke_id+"'"  
		+",'"+pspwd+"'"		
        +");"
        +"end;"
        ;
    System.out.println(s);
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);

  }
  
  
  
   public NEOCommonData Themmoi_to_fox(String schema ,
  						    String   psthangin,
  						    String   pssvr,
					        String   psbanke_id,
							String   pspwd
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schema)+"Inhoadon.Themmoi_to_fox('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psthangin+"'"  
		+",'"+pssvr+"'"		
		+",'"+psbanke_id+"'"   
		+",'"+pspwd+"'"	
        +");"
        +"end;"
        ;
    System.out.println(s);
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);

  }
  
 public NEOCommonData xoa_yc(String schema ,
  						  String   yeucau_id
  						  
						) {

    String s=    "begin ? := "+CesarCode.decode(schema)+"Inhoadon.xoa_yeucau('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+yeucau_id+"'" 		  
        +");"
        +"end;"
        ;
    System.out.println(s);
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);

  }
  //lay danh sach nhan vien thu cuoc tai nha
  public String ds_nhanvien(int donviql_id) {
    return "/main?"+CesarCode.encode("configFile")+"=ccbs_inhoadon/CCBS_DS_KM/ds_nhanvien&"+CesarCode.encode("donviql_id")+"="+donviql_id;
   }
  public String yeucau_ht(int banke_id1) {
    return "/main?"+CesarCode.encode("configFile")+"=ccbs_inhoadon/yeucau_ht&"+CesarCode.encode("banke_id")+"="+banke_id1;
   }  
   
 public String yeucau_dang_xl(int banke_id1) {
    return "/main?"+CesarCode.encode("configFile")+"=ccbs_inhoadon/yeucau_dang_xl&"+CesarCode.encode("banke_id")+"="+banke_id1;
   }  
   
public String yeucau_error(int banke_id1) {
    return "/main?"+CesarCode.encode("configFile")+"=ccbs_inhoadon/yeucau_error&"+CesarCode.encode("banke_id")+"="+banke_id1;
   }  
  
public String ds_phuongs(int quan_id) {
    return "/main?"+CesarCode.encode("configFile")+"=ccbs_inhoadon/ds_phuongs&"+CesarCode.encode("quan_id")+"="+quan_id;
   }

public String ds_khoanmuctts() {
    return "/main?"+CesarCode.encode("configFile")+"=ccbs_inhoadon/ds_khoanmuctts";
   }

public String ds_doituongs() {
    return "/main?"+CesarCode.encode("configFile")+"=ccbs_inhoadon/ds_doituongs";
   }
   
public String ds_loaikhs() {
    return "/main?"+CesarCode.encode("configFile")+"=ccbs_inhoadon/ds_loaikhs";
   }

//thuyvtt 19/09/2008
  public String tracuu1(String psSChema
						   ,String psDonvi
						   ,String psChuky) {    
	return "/main?"+CesarCode.encode("configFile")+"=ccbs_inhoadon/TRACUUCUOC/ds_thuebao_ajax1"
	
		+"&"+CesarCode.encode("donvi")+"="+psDonvi
		+"&"+CesarCode.encode("chuky")+"="+psChuky;
    }

	//thuyvtt 19/09/2008
	public String tracuu2(String psSchema
						   ,String psDonvi
						   ,String psChuky) {
    
	return "/main?"+CesarCode.encode("configFile")+"=ccbs_inhoadon/TRACUUCUOC/ds_thuebao_ajax2"
		
		+"&"+CesarCode.encode("donvi")+"="+psDonvi
		+"&"+CesarCode.encode("chuky")+"="+psChuky;  
    }
	
	public String tracuu_th1(String psSchema, String psDonvi, String psChuky)
	{
		return "/main?"+CesarCode.encode("configFile")+"=ccbs_inhoadon/TRACUUCUOC/ds_thuebao_th_ajax1"
					   +"&"+CesarCode.encode("donvi")+"="+psDonvi
					   +"&"+CesarCode.encode("chuky")+"="+psChuky;
	}
	
	public String tracuu_th2(String psSchema, String psDonvi, String psChuky)
	{
		return "/main?"+CesarCode.encode("configFile")+"=ccbs_inhoadon/TRACUUCUOC/ds_thuebao_th_ajax2"
					   +"&"+CesarCode.encode("donvi")+"="+psDonvi
					   +"&"+CesarCode.encode("chuky")+"="+psChuky;
	}
	
	public String log_inchitiet(String psMatinh, String psma_tb, String psNguoi_cn, String psNgay_cn, String psChuky, String psKieuin, String psIP){
		String s = "begin ?:="+ getSysConst("FuncSchema") + "inhoadon.log_inchitiet("+"'"+ psMatinh + "'"
		+",'"+ psma_tb + "'"+",'"+psNguoi_cn+"'"+",'"+psNgay_cn+"'"+",'"+psChuky+"'"+",'"+psKieuin+"','"+psIP+"'); end;";
		return s;
	}
	
	public String get_ds_inchitiet(String psMatinh, String psMaTB) {
		return "/main?"+CesarCode.encode("configFile")+"=ccbs_inhoadon/common/in_chitiet_cuoc/ds_inchitiet_ajax"
				  +"&"+CesarCode.encode("tinh")+"="+psMatinh
				  +"&"+CesarCode.encode("matb")+"="+psMaTB;
	}
	
}
