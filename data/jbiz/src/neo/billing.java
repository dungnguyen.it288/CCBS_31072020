package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class billing
extends NEOProcessEx {
	public void run()
	{
		System.out.println("neo.billing was called");
	}


   public NEOCommonData layds_log_create_tables(String piPAGEID,
    							   String piREC_PER_PAGE,    							
    							   String psThangTC,
    							   String pstinh_tc) 
      {
			  String  s= "/main?"+CesarCode.encode("configFile")+"=ccbs_billing/ajax_log_billing"
					+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
					+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
					+ "&" +CesarCode.encode("thang_tc")+"="+psThangTC
					+ "&" +CesarCode.encode("tinh_tc")+"="+pstinh_tc		
			        ;   
			    NEOExecInfo nei_ = new NEOExecInfo(s);
				nei_.setDataSrc("VNPBilling");
				return new NEOCommonData(nei_);
       }
   
   
      
   	public NEOCommonData layds_agent_process(
   								            String func_schema,
											String schemaCommon,
											String psThangTC,
											String psTinh_TC)
			{
				String s = "begin ?:=" + schemaCommon + "billing.layds_agent_process('" + getUserVar("userID")
					 + "','" + getUserVar("sys_agentcode")
					 + "','" + schemaCommon
					 + "','" + getUserVar("sys_dataschema")
					 + "','" + psThangTC+"'"
					 + ",'" + psTinh_TC+"'"					 
					 + ");end;";
		
			    NEOExecInfo nei_ = new NEOExecInfo(s);
				nei_.setDataSrc("VNPBilling");
				return new NEOCommonData(nei_);
			}
			
			
    public NEOCommonData capnhat_agent_process(
									            String   func_schema
												,String  schemaCommon
												,String  pichot_ds_loitc
												,String  pitinhcuoc_xong
												,String  pichot_danhba_nhieulan
												,String  pichot_danhba_khi_conloi
												,String  pingay_chot_loitc
												,String  pingay_tinhcuoc_xong
												,String  psghichu
												,String  psThangTC
												,String  psTinh_TC
												)
			{
				String s = "begin ?:=" + schemaCommon + "billing.capnhat_agent_process('" + getUserVar("userID")
					 + "','" + getUserVar("sys_agentcode")					
					 + "','" + getUserVar("sys_dataschema")
					  + "','" + schemaCommon
					 + "','"  + psThangTC
					 + "','"  + psTinh_TC
					 + "',"  + pichot_ds_loitc
					 + ","  + pitinhcuoc_xong
					 + ","  + pichot_danhba_nhieulan
					 + ","  + pichot_danhba_khi_conloi
					 + ","  + pingay_chot_loitc
					 + ","  + pingay_tinhcuoc_xong
					 + ",'"  + psghichu+"'"					 
					 + ");end;";
		
			    NEOExecInfo nei_ = new NEOExecInfo(s);
				nei_.setDataSrc("VNPBilling");
				return new NEOCommonData(nei_);
			}
         
       
        /*!-- 
													    radMV constant number:=1;
														radMABC constant number:=2;
														radDONVIQL constant number:=3;
														radDOTKM constant number:=4;
														radTKCDLH_CONLAI constant number:=5;--*/
 
         public NEOCommonData layds_thongtin_lois(String piPAGEID,
    							   String piREC_PER_PAGE,    							
    							   String psThangTC,
    							   String pstinh_tc,
    							   String piOption) 
				{
					String s="";
					  if ((piOption=="1") || (piOption=="3") || (piOption=="5") )
					  {
						    s= "/main?"+CesarCode.encode("configFile")+"=ccbs_billing/ajax_thongtin_kts"
								+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
								+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
								+ "&" +CesarCode.encode("thang_tc")+"="+psThangTC
								+ "&" +CesarCode.encode("tinh_tc")+"="+pstinh_tc
								+ "&" +CesarCode.encode("Option")+"="+piOption		
						        ;  					
					  }else if ((piOption=="4"))
					  {
						    s= "/main?"+CesarCode.encode("configFile")+"=ccbs_billing/ajax_dot_km_apdung"
								+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
								+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
								+ "&" +CesarCode.encode("thang_tc")+"="+psThangTC
								+ "&" +CesarCode.encode("tinh_tc")+"="+pstinh_tc
								+ "&" +CesarCode.encode("Option")+"="+piOption		
						        ;  					
					  }else if ((piOption=="2"))
					  {
						    s= "/main?"+CesarCode.encode("configFile")+"=ccbs_billing/ajax_mabc_null"
								+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
								+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
								+ "&" +CesarCode.encode("thang_tc")+"="+psThangTC
								+ "&" +CesarCode.encode("tinh_tc")+"="+pstinh_tc
								+ "&" +CesarCode.encode("Option")+"="+piOption		
						        ;  					
					  }
					  
					  		NEOExecInfo nei_ = new NEOExecInfo(s);
							nei_.setDataSrc("VNPBilling");
							return new NEOCommonData(nei_);
				}
				
				
   public NEOCommonData layds_mavungs(String piPAGEID,
    							   String piREC_PER_PAGE,
    							   String pikhoanmuctc_id,
    							   String psmavung							
    							   ) 
					{
						  String  s= "/main?"+CesarCode.encode("configFile")+"=ccbs_billing/ajax_mavungs"
								+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
								+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  	
								+ "&" +CesarCode.encode("khoanmuctc_id")+"="+pikhoanmuctc_id
								+ "&" +CesarCode.encode("mavung")+"="+psmavung								
						        ;   
						    NEOExecInfo nei_ = new NEOExecInfo(s);
							nei_.setDataSrc("VNPBilling");
							return new NEOCommonData(nei_);
					}	
					
	 public NEOCommonData themmoi_mavungs(
									            String   func_schema
												,String  schemaCommon
												,String  pikhoanmuctc_id
												,String  psmavung
												,String  pstenvung											
												)
			{
				String s = "begin ?:=" + schemaCommon + "billing.themmoi_mavungs('" + getUserVar("userID")
					 + "','" + getUserVar("sys_agentcode")					
					 + "','" + getUserVar("sys_dataschema")
					  + "','" + schemaCommon
					 + "','"  + pikhoanmuctc_id
					 + "','"  + psmavung
					 + "',"  + pstenvung					 			 
					 + ");end;";
		
			    NEOExecInfo nei_ = new NEOExecInfo(s);
				nei_.setDataSrc("VNPBilling");
				return new NEOCommonData(nei_);
			}
         
         
          public NEOCommonData capnhat_mavungs(
									            String   func_schema
												,String  schemaCommon
												,String  pikhoanmuctc_id
												,String  psmavung
												,String  pstenvung											
												)
			{
				String s = "begin ?:=" + schemaCommon + "billing.capnhat_mavungs('" + getUserVar("userID")
					 + "','" + getUserVar("sys_agentcode")					
					 + "','" + getUserVar("sys_dataschema")
					  + "','" + schemaCommon
					 + "','"  + pikhoanmuctc_id
					 + "','"  + psmavung
					 + "',"  + pstenvung					 			 
					 + ");end;";
		
			    NEOExecInfo nei_ = new NEOExecInfo(s);
				nei_.setDataSrc("VNPBilling");
				return new NEOCommonData(nei_);
			}
         
         
          public NEOCommonData xoa_mavungs(
									             String   func_schema
												,String  schemaCommon
												,String  psmavung
																					
												)
			{
				String s = "begin ?:=" + schemaCommon + "billing.xoa_mavungs('" + getUserVar("userID")
					 + "','" + getUserVar("sys_agentcode")					
					 + "','" + getUserVar("sys_dataschema")
					  + "','" + schemaCommon				
					 + "','"  + psmavung+"'"					 			 			 
					 + ");end;";
		
			    NEOExecInfo nei_ = new NEOExecInfo(s);
				nei_.setDataSrc("VNPBilling");
				return new NEOCommonData(nei_);
			}
         	
         	
         	
				

				
}
