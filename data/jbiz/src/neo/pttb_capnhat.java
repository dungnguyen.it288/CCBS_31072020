package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_capnhat
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb_capnhat was called");
  }

  public String getUrlBuuCucThu(String value) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/capnhat/khachhang/ajax_buucucthu"
        +"&"+CesarCode.encode("donviql_id")+"="+value
        ;
  }
  public String getUrlNganhNghe(String value) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/capnhat/khachhang/ajax_nganhnghe"
        +"&"+CesarCode.encode("+loainn_id")+"="+value
        ;
  }
  public String layDsKhachHang(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.layds_kh_trong_cq('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }
  public String getUrlDsKhachHang(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/capnhat/khachhang/ajax_layds_kh_trong_cq"
        +"&"+CesarCode.encode("userinput")+"="+userInput
        ;
  }
  public String getUrlDsThueBaoCD(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/capnhat/danhba/ajax_layds_tbcd_trong_cq"
        +"&"+CesarCode.encode("userinput")+"="+userInput
        ;
  }
  public String getUrlDsThueBaoDD(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/capnhat/danhba/ajax_layds_tbdd_trong_cq"
        +"&"+CesarCode.encode("userinput")+"="+userInput
        ;
  }
  public String getUrlDsThueBaoIN(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/capnhat/danhba/ajax_layds_tbin_trong_cq"
        +"&"+CesarCode.encode("userinput")+"="+userInput
        ;
  }
  public String getUrlDsThueBaoEM(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/capnhat/danhba/ajax_layds_tbem_trong_cq"
        +"&"+CesarCode.encode("userinput")+"="+userInput
        ;
  }

   public String capnhatKhachHangPTTB(String schema, String pima_cq, String pscoquan
                                     ,String pssodaidien,String psdienthoai_lh,String psloaigt
                                     ,String psso_gt,String psngaycap_gt,String psnoicapgt
                                     ,String piquan_id,String piphuong_id,String pipho_id
                                     ,String psso_nha,String psdiachi_ct,String pidichvuvt_id
                                     ,String psma_kh,String psten_tt,String piquantt_id
                                     ,String piphuongtt_id,String piphott_id,String pssott_nha
                                     ,String psdiachi_tt,String psma_bc,String pidiadiemtt_id
                                     ,String psma_nv,String psma_t,String pinganhang_id
                                     ,String pstaikhoan,String psms_thue,String pidonviql_id
                                     ,String piloaikh_id,String pikhlon_id,String pichuky_id
                                     ,String pidangky_tv,String pitrangthai_id
                                     ,String piuutien_id,String pinganhnghe_id,String pidangky_db
                                     ,String psghichu,String pdngay_cn,String psnguoi_cn,String psmay_cn,String psemail,String pskh_rr
									 ,String psso_gt1  ,String psngaycap_gt1 ,String psnoicap_gt1  ,int    piLOAIGT_ID1
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_capnhat.capnhat_kh_pttb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
        +pima_cq
        +",'"+pscoquan+"'"
        +",'"+pssodaidien+"'"
        +",'"+psdienthoai_lh+"'"
        +",'"+psloaigt+"'"
        +",'"+psso_gt+"'"
        +",'"+psngaycap_gt+"'"
        +",'"+psnoicapgt+"'"        
        +","+piquan_id
        +","+piphuong_id
        +","+pipho_id
        +",'"+psso_nha+"'"
        +",'"+psdiachi_ct+"'"
        +","+pidichvuvt_id
        +",'"+psma_kh+"'"
        +",'"+psten_tt+"'"
        +","+piquantt_id
        +","+piphuongtt_id
        +","+piphott_id
        +",'"+pssott_nha+"'"
        +",'"+psdiachi_tt+"'"
        +",'"+psma_bc+"'"
        +","+pidiadiemtt_id
        +",'"+psma_nv+"'"
        +",'"+psma_t+"'"
        +","+pinganhang_id
        +",'"+pstaikhoan+"'"
        +",'"+psms_thue+"'"
        +","+pidonviql_id
        +","+piloaikh_id
        +","+pikhlon_id
        +","+pichuky_id
        +","+pidangky_tv
        +","+pitrangthai_id
        +","+piuutien_id
        +","+pinganhnghe_id
        +","+pidangky_db
        +",'"+psghichu+"'"		
        +","+pdngay_cn
        +",'"+psnguoi_cn+"'"
        +",'"+psmay_cn+"'"
		+",'"+psemail+"'"
		+","+pskh_rr		
		
		+",'"+psso_gt1+"'"
		+",'"+psngaycap_gt1+"'"
		+",'"+psnoicap_gt1+"'"
		+","+piLOAIGT_ID1
		
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;

  }
  public String capnhatDanhBaCDPTTB(String schema
				,String psSOMAY
				,String piTYLE_VAT
				,String piCUOCNH_ID
				,String piQUAN_ID
				,String piPHUONG_ID
				,String piPHO_ID
				,String piTHUTU_IN
				,String psSO_NHA
				,String piGIAPDANH
				,String piDANGKY_DB
				,String psTEN_DB
				,String piTRAMVT_ID
				,String psTEN_TB
				,String psDIACHI
				,String piCUOC_TB
				,String piCUOC_DV
				,String piLOAITB_ID
				,String pdNGAY_LD
				,String pdNGAY_TD
				,String pdNGAY_TTHOAI
				,String piINCHITIET
				,String psGHICHU
				,String piTRANGTHAI_ID
				,String piDONVIQL_ID
				,String piDOITUONG_ID
				,String piDONVITC_ID
				,String psNGUOI_CN
	) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_capnhat.capnhat_tbcd_pttb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psSOMAY+"',"
		+piTYLE_VAT+","
		+piCUOCNH_ID+","
		+piQUAN_ID+","
		+piPHUONG_ID+","
		+piPHO_ID+","
		+piTHUTU_IN+","
		+"'"+psSO_NHA+"',"
		+piGIAPDANH+","
		+piDANGKY_DB+","
		+"'"+psTEN_DB+"',"
		+piTRAMVT_ID+","
		+"'"+psTEN_TB+"',"
		+"'"+psDIACHI+"',"
		+piCUOC_TB+","
		+piCUOC_DV+","
		+piLOAITB_ID+","
		+pdNGAY_LD+","
		+pdNGAY_TD+","
		+pdNGAY_TTHOAI+","
		+piINCHITIET+","
		+psGHICHU+","
		+piTRANGTHAI_ID+","
		+piDONVIQL_ID+","
		+piDOITUONG_ID+","
		+piDONVITC_ID+","
                +"'"+psNGUOI_CN+"'"
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;

  }
  public String capnhatDanhBaDDPTTB(String schema
                                ,String psSOMAY
                                ,String piTYLE_VAT
                                ,String piQUAN_ID
                                ,String piPHUONG_ID
                                ,String piPHO_ID
                                ,String piTHUTU_IN
                                ,String psSO_NHA
                                ,String piDANGKY_DB
                                ,String psTEN_DB
                                ,String psTEN_TB
                                ,String psDIACHI
                                ,String piCUOC_TB
                                ,String piCUOC_DV
                                ,String piLOAITB_ID
                                ,String pdNGAY_LD
                                ,String pdNGAY_TD
                                ,String pdNGAY_TTHOAI
                                ,String piINCHITIET
                                ,String psGHICHU
                                ,String piTRANGTHAI_ID
                                ,String piDONVIQL_ID
                                ,String piDOITUONG_ID
                                ,String psNGUOI_CN								
								,String pdNgaySN
        ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_capnhat.capnhat_tbdd_pttb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psSOMAY+"',"
                +piTYLE_VAT+","
                +piQUAN_ID+","
                +piPHUONG_ID+","
                +piPHO_ID+","
                +piTHUTU_IN+","
                +"'"+psSO_NHA+"',"
                +piDANGKY_DB+","
                +"'"+psTEN_DB+"',"
                +"'"+psTEN_TB+"',"
                +"'"+psDIACHI+"',"
                +piCUOC_TB+","
                +piCUOC_DV+","
                +piLOAITB_ID+","
                +pdNGAY_LD+","
                +pdNGAY_TD+","
                +pdNGAY_TTHOAI+","
                +piINCHITIET+","
                +psGHICHU+","
                +piTRANGTHAI_ID+","
                +piDONVIQL_ID+","
                +piDOITUONG_ID+","
                +"'"+psNGUOI_CN+"',"
				+pdNgaySN
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;

  }
  public String capnhatDanhBaINPTTB(String schema
                                ,String psCALLING
                                ,String piTYLE_VAT
                                ,String piQUAN_ID
                                ,String piPHUONG_ID
                                ,String piPHO_ID
                                ,String piTHUTU_IN
                                ,String psSO_NHA
                                ,String piDANGKY_DB
                                ,String psTEN_DB
                                ,String piTRAMVT_ID
                                ,String psTEN_TB
                                ,String psDIACHI
                                ,String piCUOC_TB
                                ,String piCUOC_DV
                                ,String piLOAITB_ID
                                ,String pdNGAY_LD
                                ,String pdNGAY_TD
                                ,String pdNGAY_TTHOAI
                                ,String piINCHITIET
                                ,String psGHICHU
                                ,String piTRANGTHAI_ID
                                ,String piDONVIQL_ID
                                ,String piDOITUONG_ID
                                ,String psSOMAY_TN
                                ,String psACCOUNT

                                ,String psNGUOI_CN
        ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_capnhat.capnhat_tbin_pttb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psCALLING+"',"
                +piTYLE_VAT+","
                +piQUAN_ID+","
                +piPHUONG_ID+","
                +piPHO_ID+","
                +piTHUTU_IN+","
                +"'"+psSO_NHA+"',"
                +piDANGKY_DB+","
                +"'"+psTEN_DB+"',"
                +piTRAMVT_ID+","
                +"'"+psTEN_TB+"',"
                +"'"+psDIACHI+"',"
                +piCUOC_TB+","
                +piCUOC_DV+","
                +piLOAITB_ID+","
                +pdNGAY_LD+","
                +pdNGAY_TD+","
                +pdNGAY_TTHOAI+","
                +piINCHITIET+","
                +psGHICHU+","
                +piTRANGTHAI_ID+","
                +piDONVIQL_ID+","
                +piDOITUONG_ID+","
                +"'"+psSOMAY_TN+"',"
                +"'"+psACCOUNT+"',"
                +"'"+psNGUOI_CN+"'"
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;

  }
  public String capnhatDanhBaEMPTTB(String schema
                                ,String psACCOUNT
                                ,String piTYLE_VAT
                                ,String piQUAN_ID
                                ,String piPHUONG_ID
                                ,String piPHO_ID
                                ,String piTHUTU_IN
                                ,String psSO_NHA
                                ,String piDANGKY_DB
                                ,String psTEN_DB
                                ,String psTEN_TB
                                ,String psDIACHI
                                ,String piCUOC_TB
                                ,String piCUOC_DV
                                ,String piLOAITB_ID
                                ,String pdNGAY_LD
                                ,String pdNGAY_TD
                                ,String pdNGAY_TTHOAI
                                ,String piINCHITIET
                                ,String psGHICHU
                                ,String piTRANGTHAI_ID
                                ,String piDONVIQL_ID
                                ,String piDOITUONG_ID
                                ,String psSOMAY_TN
                                ,String psNGUOI_CN
        ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_capnhat.capnhat_tbem_pttb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psACCOUNT+"',"
                +piTYLE_VAT+","
                +piQUAN_ID+","
                +piPHUONG_ID+","
                +piPHO_ID+","
                +piTHUTU_IN+","
                +"'"+psSO_NHA+"',"
                +piDANGKY_DB+","
                +"'"+psTEN_DB+"',"
                +"'"+psTEN_TB+"',"
                +"'"+psDIACHI+"',"
                +piCUOC_TB+","
                +piCUOC_DV+","
                +piLOAITB_ID+","
                +pdNGAY_LD+","
                +pdNGAY_TD+","
                +pdNGAY_TTHOAI+","
                +piINCHITIET+","
                +psGHICHU+","
                +piTRANGTHAI_ID+","
                +piDONVIQL_ID+","
                +piDOITUONG_ID+","
                +"'"+psSOMAY_TN+"',"
                +"'"+psNGUOI_CN+"'"
        +");"
        +"end;"
        ;
    //System.out.println(s);
    return s;

  }
  
    //quan ly khoi co quan
    public String lay_ma_cq_moi(String schema,String piDONVIQL_ID) {
    return "begin ?:= "+schema+"PTTB.lay_ma_coquan('"+getUserVar("userID")+"','"+getUserVar("sys_agentcode")+"','"+getUserVar("sys_dataschema")+"','"+piDONVIQL_ID+"'); end;";
  }
	// Quan ly trang thai hop dong theo kenh ban hang
	public String lay_ds_baocao_banhang(String tungay,String denngay,String nguoi_th,String donvi,String buucuc,String loai_hd,String trangthai_hd,String hinhthuc,String page_num,String page_rec) {
    return "/main?"+CesarCode.encode("configFile")+"=PTTB/capnhat/trangthai_hopdong/bc_ct_ql_pttb_dv_ajax"
        +"&"+CesarCode.encode("tungay")+"="+tungay
		+"&"+CesarCode.encode("denngay")+"="+denngay
		+"&"+CesarCode.encode("nguoi_th")+"="+nguoi_th
		+"&"+CesarCode.encode("donvi")+"="+donvi
		+"&"+CesarCode.encode("buucuc")+"="+buucuc
		+"&"+CesarCode.encode("loai_hd")+"="+loai_hd
		+"&"+CesarCode.encode("trangthai_hd")+"="+trangthai_hd
		+"&"+CesarCode.encode("hinhthuc")+"="+hinhthuc
		+"&"+CesarCode.encode("page_num")+"="+page_num
		+"&"+CesarCode.encode("page_rec")+"="+page_rec
        ;
  }
	public String capnhat_hopdong_theo_kenh(String psInput) {
    String s=    "begin ? := ccs_admin.pttb_capnhat.capnhat_hopdong_kenh_bh('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"
		+psInput+"'"               
        +");"
        +"end;"
        ;
	//System.out.println("luattd" +s);	
    return s;

  }
   public String huy_hopdong_theo_kenh(String psInput) {
    String s=    "begin ? := ccs_admin.pttb_capnhat.huy_hopdong_kenh_bh('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"
		+psInput+"'"               
        +");"
        +"end;"
        ;
	//System.out.println("luattd" +s);	
    return s;

  }
}
