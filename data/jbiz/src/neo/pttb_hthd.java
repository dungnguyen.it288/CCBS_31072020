package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class pttb_hthd
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb_hthd was called");
  }

  public String layds_tbld(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/hthd/hdld/hdld_ajax_dstb"
        +"&"+CesarCode.encode("ma_hd")+"="+userInput
        ;
  }
  public String layds_dichvu_pdv(String piLOAITB_ID, String piTRANGTHAI_ID) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/dhanh/dhdv/dhdv_ajax_dsdv"
        +"&"+CesarCode.encode("loaitb_id")+"="+piLOAITB_ID
		+"&"+CesarCode.encode("trangthai_id")+"="+piTRANGTHAI_ID
        ;
  }
  public String layds_tb_pdv(String piPAGEID
							,String piREC_PER_PAGE
							,String psMA_HD
							,String psMA_TB
							,String piLOAITB_ID
							,String piDONVILD_ID
							,String psMANV_ID
							,String psNGAY_PDV
							, String piTRANGTHAI_DAPDV
							,String piLOAIHD_ID
							,String psUSERID
							,String piPHUONG_ID
							,String piTOQL_ID
							) {
	String hd="";
	
	if(piLOAIHD_ID=="0") { //HD_LAPDAT
		hd="ld";
	} else if(piLOAIHD_ID=="1") { //HD_DICHVU
		hd="dv";
	} else if(piLOAIHD_ID=="2") { //HD_DICHUYEN
		hd="dc";
	} else if(piLOAIHD_ID=="3") { //HD_CQSD
		hd="cq";
	} else if(piLOAIHD_ID=="4") { //HD_DOISO
		hd="ds";
	} else if(piLOAIHD_ID=="5") { //HD_THANHLY
		hd="tl";
	} else if(piLOAIHD_ID=="6") { //HD_DOILH
		hd="cdlh";
	//} else if(piLOAIHD_ID=="7") { //HD_BANMAY
	//	return;
	} else if(piLOAIHD_ID=="7") { //HD_DOISIM
		hd="doisim";
	//} else if(piLOAIHD_ID=="9") { //HD_PHATSINH
	//	return;

	}

    return "/main?"+CesarCode.encode("configFile")+"=pttb/dhanh/hd"+hd+"_ajax_dstb"
        +"&"+CesarCode.encode("PAGEID")+"="+piPAGEID
		+"&"+CesarCode.encode("REC_PER_PAGE")+"="+piREC_PER_PAGE
		+"&"+CesarCode.encode("ma_hd")+"="+psMA_HD
        +"&"+CesarCode.encode("ma_tb")+"="+psMA_TB
		+"&"+CesarCode.encode("loaitb_id")+"="+piLOAITB_ID
		+"&"+CesarCode.encode("donvild_id")+"="+piDONVILD_ID
        +"&"+CesarCode.encode("manv_id")+"="+psMANV_ID
		+"&"+CesarCode.encode("ngay_pdv")+"="+psNGAY_PDV
		
		+"&"+CesarCode.encode("trangthai_dapdv")+"="+piTRANGTHAI_DAPDV
		+"&"+CesarCode.encode("userid")+"="+psUSERID
		+"&"+CesarCode.encode("phuong_id")+"="+piPHUONG_ID
		+"&"+CesarCode.encode("toql_id")+"="+piTOQL_ID

        ;
  }
  public String layds_tb_ht(String piPAGEID
							,String piREC_PER_PAGE
							,String psMA_HD
							,String psMA_TB
							,String piLOAITB_ID
							,String piDONVILD_ID
							,String psMANV_ID
							,String psNGAY_PDV
							,String psNGAY_PH
							, String piSONGAY_CANHBAO
							,String piTRANGTHAITH_ID
							,String piLOAIHD_ID) {
	String hd="";
	
	if(piLOAIHD_ID=="0") { //HD_LAPDAT
		hd="ld";
	} else if(piLOAIHD_ID=="1") { //HD_DICHVU
		hd="dv";
	} else if(piLOAIHD_ID=="2") { //HD_DICHUYEN
		hd="dc";
	//} else if(piLOAIHD_ID=="3") { //HD_CQSD
	//	return;
	} else if(piLOAIHD_ID=="4") { //HD_DOISO
		hd="ds";
	} else if(piLOAIHD_ID=="5") { //HD_THANHLY
		hd="tl";
	} else if(piLOAIHD_ID=="6") { //HD_DOILH
		hd="cdlh";
	//} else if(piLOAIHD_ID=="7") { //HD_BANMAY
	//	return;
	//} else if(piLOAIHD_ID=="7") { //HD_DOISIM
	} else if(piLOAIHD_ID=="7") { //HD_DOISIM
		hd="doisim";

	//} else if(piLOAIHD_ID=="9") { //HD_PHATSINH
	//	return;

	}
	
    return "/main?"+CesarCode.encode("configFile")+"=pttb/hthd/hd"+hd+"_ajax_dstb"
        +"&"+CesarCode.encode("PAGEID")+"="+piPAGEID
		+"&"+CesarCode.encode("REC_PER_PAGE")+"="+piREC_PER_PAGE
		+"&"+CesarCode.encode("ma_hd")+"="+psMA_HD
        +"&"+CesarCode.encode("ma_tb")+"="+psMA_TB
		+"&"+CesarCode.encode("loaitb_id")+"="+piLOAITB_ID
		+"&"+CesarCode.encode("donvild_id")+"="+piDONVILD_ID
        +"&"+CesarCode.encode("manv_id")+"="+psMANV_ID
		+"&"+CesarCode.encode("ngay_pdv")+"="+psNGAY_PDV
		+"&"+CesarCode.encode("ngay_ph")+"="+psNGAY_PH
		
		+"&"+CesarCode.encode("songay_canhbao")+"="+piSONGAY_CANHBAO
		+"&"+CesarCode.encode("trangthaith_id")+"="+piTRANGTHAITH_ID
        ;
  }
  public String layds_ketnoi_catmo(String piPAGEID
							,String piREC_PER_PAGE
							,String psMA_HD
							,String psMA_TB
							,String piTRANGTHAITH_ID
							,String psNGAYGUI
							,String psNGAYTH
							,String piLOAIYC_ID
							,String piLOAIHD_ID
							, String piSONGAY_CANHBAO
							,String piDICHVUVT_ID
							
							) {
	
    return "/main?"+CesarCode.encode("configFile")+"=pttb/hthd/cmdv_ajax_dsyc"
        +"&"+CesarCode.encode("PAGEID")+"="+piPAGEID
		+"&"+CesarCode.encode("REC_PER_PAGE")+"="+piREC_PER_PAGE
		+"&"+CesarCode.encode("ma_hd")+"="+psMA_HD
        +"&"+CesarCode.encode("ma_tb")+"="+psMA_TB
		+"&"+CesarCode.encode("trangthaith_id")+"="+piTRANGTHAITH_ID
		+"&"+CesarCode.encode("loaiyc_id")+"="+piLOAIYC_ID
		+"&"+CesarCode.encode("loaihd_id")+"="+piLOAIHD_ID
		+"&"+CesarCode.encode("ngaygui")+"="+psNGAYGUI
		+"&"+CesarCode.encode("ngayth")+"="+psNGAYTH
		+"&"+CesarCode.encode("songay_canhbao")+"="+piSONGAY_CANHBAO
		+"&"+CesarCode.encode("dichvuvt_id")+"="+piDICHVUVT_ID
		
        ;
  }

  public String laytt_dvth_dv(String schema,String piLOAITB_ID) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_dhanh.laytt_dvth_dv('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"+piLOAITB_ID+"); end;";
  }
  public String phan_dvth_dv(String schema
									, String psDsRID, String psDsLOAIHD, String pdNGAY_PDV
                                     ,String psUSERID
									 ) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_dhanh.phan_dvth_dv('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psDsRID+"'"
			+",'"+psDsLOAIHD+"'"
			+","+pdNGAY_PDV
        +");"
        +"end;"
        ;
    return s;
  }

  public String layds_tbdc(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/hthd/hddc/hddc_ajax_dstb"
        +"&"+CesarCode.encode("ma_hd")+"="+userInput
        ;
  }
  public String layds_tbcq(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/hthd/hdcq/hdcq_ajax_dstb"
        +"&"+CesarCode.encode("ma_hd")+"="+userInput
        ;
  }

 /*public String hoanthanhHDLD(String schema
	,String psMA_HD                         
	,String psDsTHUEBAO_ID                         
	,String psGHICHU                        
	,String pdNGAY_HT 
	,String psMaNV	
	,String psUSERID                   
	,String psUSERIP                   
	) {


   // String s=    "begin ? := admin_v2.pttb_tracuu.chuyendb_hdld('"+getUserVar("userID")
    String s=    "begin ? := admin_v2.pkg_contact_info.chuyendb_hdld('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"',"                         
		+"'"+psDsTHUEBAO_ID+"',"                         
		+psGHICHU+","                        
		+pdNGAY_HT+","                       
		+"'"+psMaNV+"',"
		+"'"+psUSERIP+"'"                       
	+");"
        +"end;"
        ;
    return s;

  }  
  */
  public NEOCommonData hoanthanhHDLD(String schema
 ,String psMA_HD                         
 ,String psDsTHUEBAO_ID                         
 ,String psGHICHU                        
 ,String pdNGAY_HT 
 ,String psMaNV 
 ,String psUSERID                   
 ,String psUSERIP                   
 ) {

 String rq_=null;
 try 
   {
    String s=    "begin ? := admin_v2.pkg_contact_info.chuyendb_hdld('"+getUserVar("userID")
  +"','"+getUserVar("sys_agentcode")
  +"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"',"                         
  +"'"+psDsTHUEBAO_ID+"',"                         
  +psGHICHU+","                        
  +pdNGAY_HT+","                       
  +"'"+psMaNV+"',"
  +"'"+psUSERIP+"'"                       
 +");"
        +"end;"
        ;
 rq_=this.reqValue("",s);
 }
 catch (Exception ex) 
     {             
      //return ex.getMessage();
      return new NEOCommonData(ex.getMessage());
     } 
  
 String s2="";
 String rq1_=null;
	rq1_=rq_;
/*  if(rq_.equals("0"))
    {
      
     try
     {      
      
       s2 = "begin ?:= admin_v2.pkg_api_interface.create_data_rigs_package("+
       "'"+getUserVar("sys_dataschema")+"',"+
       "'"+psUSERID+"',"+
       "'"+psDsTHUEBAO_ID+"',"+
       "'"+psMA_HD+"',"+
       "'"+getUserVar("sys_agentcode")+"',"+
       "'"+psUSERIP+"'"+
       "); end;";
       
       rq1_=this.reqValue("",s2); 
      System.out.println("ketqua:"+rq1_);
     } 
    catch (Exception exs) 
     {             
      return new NEOCommonData(exs.getMessage());
     }

    }
else {
	rq1_=rq_;
}
       */
    
    return new NEOCommonData(rq1_);

  }
  public String hoanthanhHDDC(String schema
	,String psMA_HD                         
	,String psDsTHUEBAO_ID                         
	,String psGHICHU                        
	,String pdNGAY_HT                  
	,String psUSERID                   
	,String psUSERIP                   
	) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_hthd.chuyendb_hddc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"',"                         
		+"'"+psDsTHUEBAO_ID+"',"                         
		+psGHICHU+","                        
		+pdNGAY_HT+","                       
		+"'"+psUSERIP+"'"                       
	+");"
        +"end;"
        ;
    return s;

  }
	public String hoanthanh_phieuks(String schema
										, String psDsRID
										, String psDsNOIDUNG
										, String piTRANGTHAI
										, String pdNGAYHT
										, String psUSERID
										, String piDONVITH_ID
										, String piNHANVIEN_ID
										   )
	{
		String s = "begin ? := " + CesarCode.decode(schema) + "PTTB_DUNGNV.hoanthanh_phieuks('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + psDsRID + "'"
				+ ",'" + psDsNOIDUNG + "'"
				+ "," + piTRANGTHAI
				+ ",'" + pdNGAYHT + "'"
				+ "," + piDONVITH_ID
				+ "," + piNHANVIEN_ID
			+ ");"
			+ "end;"
			;
		return s;
	}
 public String hoanthanhHDCQ(String schema
	,String psMA_HD                         
	,String psDsTHUEBAO_ID                         
	,String psGHICHU                        
	,String pdNGAY_HT                  
	,String psUSERID                   
	,String psUSERIP   	
	) {


    String s=    "begin ? := admin_v2.pkg_contact_info.chuyendb_hdcq('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"',"                         
		+"'"+psDsTHUEBAO_ID+"',"                         
		+psGHICHU+","                        
		+pdNGAY_HT+","                       
		+"'"+psUSERIP+"'"                       
	+");"
        +"end;"
        ;
    return s;

  } 
public String hoanthanhHDCQ_NEW(String schema
	,String psMA_HD                         
	,String psDsTHUEBAO_ID                         
	,String psGHICHU                        
	,String pdNGAY_HT                  
	,String psUSERID                   
	,String psUSERIP   	
	,String psLydoanh   	
	) {


    String s=    "begin ? := admin_v2.pkg_contact_info.chuyendb_hdcq_new('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"',"                         
		+"'"+psDsTHUEBAO_ID+"',"                         
		+psGHICHU+","                        
		+pdNGAY_HT+","                       
		+"'"+psUSERIP+"'"                       
		+","+psLydoanh+"" 
	+");"
        +"end;"
        ;
    return s;

  }  
  public String layds_nhanvien_dd(String donviql_id,String toql_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/dhanh/hd_ajax_nvdd"
        +"&"+CesarCode.encode("donviql_id")+"="+donviql_id
		+"&"+CesarCode.encode("toql_id")+"="+toql_id
	    ;
  }
  public String layds_to_dd(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/dhanh/hd_ajax_todd"
        +"&"+CesarCode.encode("donviql_id")+"="+userInput
        ;
  }
  public String layds_phuongs(String pidonviql_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/dhanh/hd_ajax_phuongs"
        +"&"+CesarCode.encode("donviql_id")+"="+pidonviql_id
	
        ;
  }
  public String layds_nvdh(String pidonviql_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/dhanh/hd_ajax_nvdh"
        +"&"+CesarCode.encode("donviql_id")+"="+pidonviql_id
	
        ;
  }

  public String layds_tramvts(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/dhanh/hd_ajax_tramvts"
        +"&"+CesarCode.encode("donviql_id")+"="+userInput
        ;
  }
  public String layds_nhanvien_ks(String donviql_id, String toql_id)
  {
	  return "/main?" + CesarCode.encode("configFile") + "=pttb/dhanh/hd_ajax_nvdd"
		  + "&" + CesarCode.encode("donviql_id") + "=" + donviql_id
		  + "&" + CesarCode.encode("toql_id") + "=" + toql_id
		  ;
  }
  public String layds_to_ks(String userInput)
  {
	  return "/main?" + CesarCode.encode("configFile") + "=pttb/dhanh/hd_ajax_todd"
		  + "&" + CesarCode.encode("donviql_id") + "=" + userInput
		  ;
  }
	public String layds_phieuks_daphandv(String piDONVIQL_ID
											,String piToqly_id
											,String piNhanvien_id
											,String pdngay_pdv
											,String piTrangThaiKS
										)
  {
	  return "/main?" + CesarCode.encode("configFile") + "=pttb/hthd/khaosat/ajax_dsphieuks_theodv"
			+ "&" + CesarCode.encode("donviql_id") + "=" + piDONVIQL_ID
			+ "&" + CesarCode.encode("toqly_id") + "=" + piToqly_id
			+ "&" + CesarCode.encode("nhanvien_id") + "=" + piNhanvien_id
			+ "&" + CesarCode.encode("ngay_pdv") + "=" + pdngay_pdv
			+ "&" + CesarCode.encode("trangthai_ks") + "=" + piTrangThaiKS
			;
  }
  public String phan_dvth_hd(String schema
									, String psDsRID, String piDONVILD_ID,String piMANV_ID, String pdNGAY_PDV
                                     ,String psUSERID
									 ,String piLOAIHD_ID
									 ) {

	String hd="";
	
	if(piLOAIHD_ID=="0") { //HD_LAPDAT
		hd="ld";
	} else if(piLOAIHD_ID=="1") { //HD_DICHVU
		hd="dv";
	} else if(piLOAIHD_ID=="2") { //HD_DICHUYEN
		hd="dc";
	} else if(piLOAIHD_ID=="3") { //HD_CQSD
		hd="cq";
	} else if(piLOAIHD_ID=="4") { //HD_DOISO
		hd="ds";
	} else if(piLOAIHD_ID=="5") { //HD_THANHLY
		hd="tl";
	} else if(piLOAIHD_ID=="6") { //HD_DOILH
		hd="cdlh";
	//} else if(piLOAIHD_ID=="7") { //HD_BANMAY
	//	return;
	} else if(piLOAIHD_ID=="7") { //HD_DOISIM
		hd="doisim";
	//} else if(piLOAIHD_ID=="9") { //HD_PHATSINH
	//	return;

	}

    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_dhanh.phan_dvth_hd"+hd+"('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psDsRID+"'"
			+","+piDONVILD_ID
			+","+piMANV_ID
			+","+pdNGAY_PDV
        +");"
        +"end;"
        ;
    return s;
  }
  
   public String phanlai_dvth_hd(String schema
									, String psDsRID, String piDONVILD_ID,String piMANV_ID, String pdNGAY_PDV
                                     ,String psUSERID
									 ,String piLOAIHD_ID
									 ) {

	String hd="";
	
	if(piLOAIHD_ID=="0") { //HD_LAPDAT
		hd="ld";
	} else if(piLOAIHD_ID=="1") { //HD_DICHVU
		hd="dv";
	} else if(piLOAIHD_ID=="2") { //HD_DICHUYEN
		hd="dc";
	} else if(piLOAIHD_ID=="3") { //HD_CQSD
		hd="cq";
	} else if(piLOAIHD_ID=="4") { //HD_DOISO
		hd="ds";
	} else if(piLOAIHD_ID=="5") { //HD_THANHLY
		hd="tl";
	} else if(piLOAIHD_ID=="6") { //HD_DOILH
		hd="cdlh";
	//} else if(piLOAIHD_ID=="7") { //HD_BANMAY
	//	return;
	} else if(piLOAIHD_ID=="7") { //HD_DOISIM
		hd="doisim";
	//} else if(piLOAIHD_ID=="9") { //HD_PHATSINH
	//	return;

	}

    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_dhanh.phanlai_dvth_hd"+hd+"('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psDsRID+"'"
			+",'"+piDONVILD_ID+"'"
			+",'"+piMANV_ID+"'"
			+","+pdNGAY_PDV
        +");"
        +"end;"
        ;
    return s;
  }
	public String hoanthanhHDDV(String schema
	,String psMA_HD                         
	,String piDsDICHVU_ID                         
	,String pdNGAY_HT                  
	,String psUSERID                   
	,String psUSERIP                   
	) {


    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_hthd.chuyendb_hddv('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"',"                         
		+"'"+piDsDICHVU_ID+"',"                         
		+pdNGAY_HT+","                       
		+"'"+psUSERIP+"'"                       
	+");"
        +"end;"
        ;
    return s;

  }  
  public String layds_dichvu_da_dk(String psMA_TB,String piLOAITB_ID) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/hthd/hddv/hddv_ajax_da_dk"
        +"&"+CesarCode.encode("ma_tb")+"="+psMA_TB
		+"&"+CesarCode.encode("loaitb_id")+"="+piLOAITB_ID
        ;
  }
	public String phan_dvth_ks(String schema
										, String psDsRID, String piDONVILD_ID, String piMANV_ID, String pdNGAY_PDV
										 , String psUSERID
										   )
	{
		String s = "begin ? := " + CesarCode.decode(schema) + "PTTB_TEST_KIEN.phan_dvth_ycks('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + psDsRID + "'"
				+ "," + piDONVILD_ID
				+ "," + piMANV_ID
				+ ",'" + pdNGAY_PDV + "'"
			+ ");"
			+ "end;"
			;
		return s;
	}
	public String huyphan_dvth_ks(String schema
										, String psDsRID, String piDONVILD_ID
										 , String psUSERID
										   )
	{
		String s = "begin ? := " + CesarCode.decode(schema) + "PTTB_TEST_KIEN.huyphan_dvth_ycks('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + psDsRID + "'"
				+ "," + piDONVILD_ID
			+ ");"
			+ "end;"
			;
		return s;
	}
	
public String chuyenhinhthuc_ttptdv(String schema
									, String psDsRID, String pssotb_n)
	{

    String s=    "begin ? := "+CesarCode.decode(schema)+"PTTB_DHANH.chuyenhinhthuc_ttptdv('"+getUserVar("sys_dataschema")
		+"','"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")+"','"+psDsRID+"'"
		+",'"+pssotb_n+"');"
        +"end;"
        ;
    return s;
  }		
  
  /* Ham thuc hien lay danh sach  khach hang ban ke */
  public String laytt_khachhang(String makh)
	{
		String s = "begin ? := admin_v2.pkg_kh_banke.laytt_khachhang('"+getUserVar("sys_dataschema")+"','" +makh+"'); end;";
		System.out.println(s);
		return s;
	}
	
	 public String laytt_log(String psmakh)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/hthd/khachhang_banke/ajax_ds_banke_log"
		+"&"+CesarCode.encode("makh")+"="+psmakh
        ;
	}
	
	public String xoa_kh_banke(
		String pschema,
		String pmakh) {
			String s ="begin ? :=admin_v2.pkg_kh_banke.xoa_kh_banke("
				+"'"+pschema+"',"
				+"'"+pmakh+"'"
				+"); end;";
		System.out.println(s);
		return s;
	}
	
	
	public String capnhat_khbk(
		String pschema,
		String pma_kh,
		String ploaibk,
		String puserid,
		String puserip) 
	{
			String s ="begin ? :=admin_v2.pkg_kh_banke.capnhat_kh_banke("
				+"'"+pschema+"',"
				+"'"+pma_kh+"',"
				+"'"+ploaibk+"',"
				+"'"+puserid+"',"
				+"'"+puserip+"'"
				+"); end;";
		System.out.println(s);
       return s;	
	} 
  
	
}
