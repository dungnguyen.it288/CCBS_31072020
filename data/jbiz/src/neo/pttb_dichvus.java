package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class pttb_dichvus
extends NEOProcessEx {
	public void run()
	{
		System.out.println("neo.pttb_hopdongcdlh was called");
	}
	public String getUrlnvdiday(String donviql_id)
	{
		return "/main?" + CesarCode.encode("configFile") + "=pttb/danhmuc/ajax_toql"
			+ "&" + CesarCode.encode("donviql_id") + "=" + donviql_id;			
	}
	public String xoa_dichvus(String schema, String piDICHVU_ID)
	{
		String s = "begin ? := " + CesarCode.decode(schema) + "PTTB_TEST_KIEN.xoa_dichvugt('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
			+ piDICHVU_ID
			+ ");"
			+ "end;";
		System.out.println(s);
		return s;	
	}
	public String laytt_dv(String schema,String userInput, String userid) 
	{
		return "begin ?:="+CesarCode.decode(schema)+"pttb_test_hiep.laytt_dv('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
	}
	public String capnhatdichvus(String schema,
		String piDICHVU_ID
		, String piLOAITB_ID
		, String psTEN_DV
		, String piCUOC_SD
		, String piCUOC_LD
		, String piVAT_SD
		, String piVAT_LD
		, String piTRANGTHAI_ID
		, String piCACHLUU
		, String pcNOIDUNG		
		, String piMIENCUOC
		, String psMA_DVGT
		, String piINSERTORUPDATE
		)
	{
		String s = "begin ? := " + CesarCode.decode(schema) + "PTTB_TEST_KIEN.capnhat_dichvus('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
					+ piDICHVU_ID
					+ "," + piLOAITB_ID
					+ ",'" + psTEN_DV + "'"
					+ "," + piCUOC_SD
					+ "," + piCUOC_LD
					+ "," + piVAT_SD
					+ "," + piVAT_LD
					+ "," + piTRANGTHAI_ID
					+ "," + piCACHLUU
					+ ",'" + pcNOIDUNG + "'"
					+ "," + piMIENCUOC
					+ ",'" + psMA_DVGT + "'"
					+ "," + piINSERTORUPDATE
			+ ");"
			+ "end;";			
		System.out.println(s);
		return s;		
	}
	
}