package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class pttb_ht_hddc
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb_hddso was called");
  }

public String laytt_ma_hd(String schema, String pmahd, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"PTTB_TEST_VIET.laytt_frmHT_HDDC('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+pmahd+"'); end;";
  }
  
  public String check_somay(String schema,String psSOMAY) {
    return "begin ?:="+CesarCode.decode(schema)+"PTTB_TEST_VIET.check_somay_frmHT_HDDC('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psSOMAY+"'); end;";
  }
  

  public String getUrlDsHD(String psMAHD) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/HT_HDDC/ajax_layds_frm_HT_HDDC"
        +"&"+CesarCode.encode("ma_hd")+"="+psMAHD
        ;
  }
  
//ham nay dung de thuc hien doi so cho from hopdong doi so
  public 	 String thuchien_doiso( String schema
									,String psMA_HD
									,String piLOAITB_ID
									,String psSOMAY
									,String psSOMOI
									,String pdNGAY_LAPHD
									,String piCUOC_DV
									,String psGHICHU
									,String piVAT
									,String piTRANGTHAIHD_ID
									,String psuserid

									)
									{
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.hddoiso_chapnhan('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMA_HD+"',"
									+"'"+piLOAITB_ID+"',"
									+"'"+psSOMAY+"',"
									+"'"+psSOMOI+"',"
									+"'"+pdNGAY_LAPHD+"',"
									+"'"+piCUOC_DV+"',"
									+"'"+psGHICHU+"',"
									+"'"+piVAT+"',"
									+"'"+piTRANGTHAIHD_ID+"',"
									+"'"+psuserid+"'"
									+");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }
  //function nay dung de xoa hopdong doi so
  public String hddso_xoa(String schema
						,String psMAHD
						,String psMATB
				
) 

{
String s=    "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.hddoiso_xoa('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psMAHD+"',"
						+"'"+psMATB+"'"
	        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }

}
