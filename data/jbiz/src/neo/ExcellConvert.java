package neo;

import neo.smartui.process.*;

import neo.smartui.process.NEOProcessEx;
import oracle.jdbc.driver.*;
import oracle.jdbc.*;
import java.sql.*;
import org.apache.poi.hssf.usermodel.*;

import java.io.*;
public class ExcellConvert extends NEOProcessEx
{	
	int sheetmaxrow = 65535;
	public String baocao_chonso(String callFuncSql
	                              ,String varFileName
								  ,String psschema
								  )
		throws IOException,java.sql.SQLException
	{
	
		String str = "begin ?:= ccs_admin.RETURNFUNCS.StringReturnFunc('" + varFileName+ "'); end;";
		try
		{
		setCallFuncSql(callFuncSql);		
		setFileName(varFileName);

		 str = "begin ?:= ccs_admin.RETURNFUNCS.StringReturnFunc('" + varFileName+":"+run()+ "'); end;";
		}
		catch(Exception ex)
		{
			str = ex.getMessage();
		}
		return str;
	}
	public String pps_baocao_chonso(String callFuncSql
	                              ,String varFileName
								  ,String psschema
								  )
		throws IOException,java.sql.SQLException
	{
	
		String str = "begin ?:= ccs_admin.RETURNFUNCS.StringReturnFunc('" + varFileName+ "'); end;";
		try
		{
		setCallFuncSql(callFuncSql);		
		setFileName(varFileName);

		 str = "begin ?:= ccs_admin.RETURNFUNCS.StringReturnFunc('" + varFileName+":"+run()+ "'); end;";
		}
		catch(Exception ex)
		{
			str = ex.getMessage();
		}
		return str;
	}
		public String vnp_baocao_chonso_pps(String callFuncSql
	                              ,String varFileName
								  ,String psschema
								  )
		throws IOException,java.sql.SQLException
	{
	
		String str = "begin ?:= ccs_admin.RETURNFUNCS.StringReturnFunc('" + varFileName+ "'); end;";
		try
		{
		setCallFuncSql(callFuncSql);		
		setFileName(varFileName);

		 str = "begin ?:= ccs_admin.RETURNFUNCS.StringReturnFunc('" + varFileName+":"+run()+ "'); end;";
		}
		catch(Exception ex)
		{
			str = ex.getMessage();
		}
		return str;
	}

	String callFuncSql = "";
	public void setCallFuncSql(String var)
	{
		callFuncSql = var;
	}
	
	String fileName;
	public void setFileName(String var)
	{
		fileName = var;
	}
	public String run()		
		throws java.io.IOException,java.sql.SQLException
	{
		try
		{
			int count =this.FillDataFromOracleFunc();
			return "da xuat "+count+" ban ghi.";
		}
		catch(Exception ex)
		{
			return  "run:" + ex.getMessage();
		}		
		
	}
	public int FillDataFromOracleFunc()
		throws java.io.IOException, java.sql.SQLException
	{
		int count=0;
		int sheetnumber=0;
		int rownum = 0;
		int numCol = 0;
		int i = 0;
		String colname;
		String coltype;
		HSSFCell cell;
		HSSFRow row;
		HSSFSheet sh;
		HSSFWorkbook wb = new HSSFWorkbook();
		FileOutputStream fo = new FileOutputStream(fileName);
		javax.sql.RowSet rset;
		java.sql.ResultSetMetaData oraMetaData;
//		java.sql.CallableStatement stmt = conn.prepareCall(callFuncSql);

		String svalue;
		
		try
		{
			rset = reqRSet(callFuncSql);
			sheetnumber++;
			sh = wb.createSheet("sheet" + sheetnumber);
			rownum = 0;
			row = sh.createRow(rownum);
			rownum++;
			oraMetaData = rset.getMetaData();
			numCol = oraMetaData.getColumnCount();
			for(i = 0;i<numCol-1;i++)
			{
				cell = row.createCell(i);
				colname = oraMetaData.getColumnName(i+2);
				cell.setCellValue(colname);			
			}
			boolean cont = true;
			while(rset.next())
			{
				if(rownum>sheetmaxrow)
				{
					sheetnumber++;
					sh = wb.createSheet("sheet" + sheetnumber);
					rownum = 0;
					row = sh.createRow(rownum);
					rownum++;
					for(i = 0;i<numCol-1;i++)
					{
						cell = row.createCell(i);
						colname = oraMetaData.getColumnName(i+2);
						cell.setCellValue(colname);			
					}
				}				
				row = sh.createRow(rownum);
				rownum++;					
				count++;
				for(i = 0;i<numCol-1;i++)
				{
					cell = row.createCell(i);
					if ((oraMetaData.getColumnTypeName(i+2)).equalsIgnoreCase("NUMBER"))
					{						
						cell.setCellValue(rset.getDouble(i+2));
					}
					else if ((oraMetaData.getColumnTypeName(i+2)).equalsIgnoreCase("VARCHAR2"))
					{
						cell.setCellValue(rset.getString(i+2));
					}	
					else if ((oraMetaData.getColumnTypeName(i+1)).equalsIgnoreCase("DATE"))
					{
						cell.setCellValue(rset.getDate(i+2));
					}	
					else
					{
						cell.setCellValue(rset.getString(i+2));
					}
				}
			}
			rset.close();
		}
		catch(Exception ex)
		{
			System.out.println("Error on execute:"+ex.getMessage());
			wb.write(fo);
			fo.close();
			return  -1;
		}
		wb.write(fo);
		fo.close();	
		return count;
	}
}
