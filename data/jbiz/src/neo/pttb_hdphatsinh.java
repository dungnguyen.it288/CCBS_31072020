package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_hdphatsinh
extends NEOProcessEx {
	public void run()
	{
		System.out.println("neo.pttb_hdphatsinh was called");
	}
	
	public String getUrlttThueBao1(String schema,  int piTraCuu_HD,String ma_hd, String ma_tb, String user_id)
	{
		// Duong dv thay doi
		// String s = "begin ? := admin_v2.pttb_laphd.laytt_tb_ps('"+getUserVar("userID")
		String s = "begin ? := admin_v2.pttb_laphd_duongdv.laytt_tb_ps('" + getUserVar("userID")
		+ "','" + getUserVar("sys_agentcode")
		+ "','" + getUserVar("sys_dataschema") + "'," + piTraCuu_HD + ",'" + ma_hd + "'"
		+ ",'"  + ma_tb + "'"
		+ ");"
		+ " end;";
		
		return s;
	}
	
	public String getUrlttThueBao(String schema,  int piTraCuu_HD,String ma_hd, String ma_tb, String user_id)
	{
		// String s = "begin ? := " + CesarCode.decode(schema) + "pttb_laphd.laytt_tb_ps('"+getUserVar("userID")
		String s = "begin ? := " + CesarCode.decode(schema).toLowerCase() + "pttb_laphd_duongdv.laytt_tb_ps('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"+piTraCuu_HD+",'" + ma_hd + "'"
		+ ",'" + ma_tb + "'"
		+");"
		+ " end;";
			
		// System.out.println( "Duongdv _____________________________________ getUrlttThueBao: " + s);
		
		return s;
	}
	public String hoanThanhHD_PhatSinh(String schema, String ma_hd)
	{
		// String s = "begin ? :=admin_v2.pkg_contact_info.hoanthanhhd_ps('"+getUserVar("userID")
		String s = "begin ? :=admin_v2.pkg_contact_info.hoanthanhhd_ps_duongdv('"+ getUserVar("userID")
		+ "','" + getUserVar("sys_agentcode")
		+ "','" + getUserVar("sys_dataschema")+"','" + ma_hd + "'" + ");"
		+ " end;";
		System.out.println("DuongDv: " +  s);
		return s;	
	}
	public String xoaHD_PhatSinh(String schema, String ma_hd)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "pttb_laphd.xoa_hdphatsinh('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_hd + "'" + ");"
		+ " end;";
		System.out.println(s);
		return s;
	}
	
	public String capnhatHDPhatSinh(String schema, String psUSER_ID ,String psMA_HD ,String psNGAY_LAPHD,String psNGAY_SINH ,String psCOQUAN ,String psTEN_TT 
		,String psDIACHI_TT ,String psTEN_TB ,String psTENTTMOI ,String psDIACHITT_MOI ,String psTEN_DB ,String psTENTB_MOI 
		,String piDIADIEMTT_ID ,String psSODAIDIEN ,String psMS_THUE ,String psTAIKHOAN ,String psMA_TB ,String psMA_KH 
		,String psGHICHU ,String piQUANTT_ID ,String piPHUONGTT_ID ,String piPHOTT_ID ,String psSOTT_NHA ,String piNGANHANG_ID 
		,String psCOQUAN_MOI ,String psDIACHICT_MOI ,String piQUAN_ID,String piPHUONG_ID,String piPHO_ID,String psSONHA 
		,String psINCHITIET,String piQUANLD_ID,String piPHUONGLD_ID,String piPHOLD_ID,String psSOLD_NHA,String psDIACHILD_MOI
		,String psEmail,String piKieuBC_ID,String piChuyenKhoan_id,String psNguoi_GT,String psSo_Giayto,String psMAY_CN, 
		String psQUOC_TICH, 
		String psLOAIGT_ID,
		String psSO_GT,
		String psDIACHI_TR,
		String psNGAYCAP_GT,
		String psNOICAP_GT,
		String psDOITUONG_SD)
	{
 
		// String s = "begin ? := admin_v2.pttb_laphd.capnhat_hdphatsinh('"+getUserVar("userID")
		String s = "begin ? := admin_v2.pttb_laphd_duongdv.capnhat_hdphatsinh('" + getUserVar("userID")
		+"','"+ getUserVar("sys_agentcode")
		+"','"+ getUserVar("sys_dataschema")
			+ "','" + psMA_HD + "'"
			+ ",'" + psNGAY_LAPHD + "'"
			+ ",'" + psNGAY_SINH + "'"
			+ ",'" + psCOQUAN + "'"
			+ ",'" + psTEN_TT + "'"
			+ ",'" + psDIACHI_TT + "'"
			+ ",'" + psTEN_TB + "'"
			+ ",'" + psTENTTMOI + "'"
			+ ",'" + psDIACHITT_MOI + "'"
			+ ",'" + psTEN_DB + "'"
			+ ",'" + psTENTB_MOI + "'"
			+ ",'" + piDIADIEMTT_ID + "'"
			+ ",'" + psSODAIDIEN + "'"
			+ ",'" + psMS_THUE + "'"
			+ ",'" + psTAIKHOAN + "'"
			+ ",'" + psMA_TB + "'"
			+ ",'" + psMA_KH + "'"
			+ "," + psGHICHU
			+ ",'" + piQUANTT_ID + "'"
			+ ",'" + piPHUONGTT_ID + "'"
			+ ",'" + piPHOTT_ID + "'"
			+ ",'" + psSOTT_NHA + "'"
			+ ",'" + piNGANHANG_ID + "'"
			+ ",'" + psCOQUAN_MOI + "'"
			+ ",'" + psDIACHICT_MOI + "'"
			+ ",'" + piQUAN_ID + "'"
			+ ",'" + piPHUONG_ID + "'"
			+ ",'" + piPHO_ID + "'"
			+ ",'" + psSONHA + "'"
			+ ",'" + psINCHITIET + "'"
			+ ",'" + piQUANLD_ID + "'"
			+ ",'" + piPHUONGLD_ID + "'"
			+ ",'" + piPHOLD_ID + "'"
			+ ",'" + psSOLD_NHA + "'"
			+ ",'" + psDIACHILD_MOI + "'"			
			+ ",'" + psEmail + "'"
			+ ",'" + piKieuBC_ID + "'"
			+ ",'" + piChuyenKhoan_id + "'"	
			+ ",'" + psNguoi_GT + "'"
			+ ",'" + psSo_Giayto + "'"		
			+ ",'" + psMAY_CN + "'"
			// Edited	: duongdv
			// Date		: 20170704
			// Update	: Bo xung thong tin thue bao
			+ ",'" + psQUOC_TICH + "'"
			+ ",'" + psLOAIGT_ID + "'"
			+ ",'" + psSO_GT + "'"
			+ ",'" + psDIACHI_TR + "'"
			+ ",'" + psNGAYCAP_GT + "'"
			+ ",'" + psNOICAP_GT + "'"
			+ ",'" + psDOITUONG_SD + "'"
			
			+ ");"
			+ "end;";			
		// System.out.println("test:" + s);
		return s;		
	}	
	
  public String layds_hopdong() {
    return "/main?"+CesarCode.encode("configFile")+"=PTTB/laphd/HDPHATSINH/ajax_layds_hopdong";
  }
  public String layds_nguoigt(String _makh) {
    //return "/main?"+CesarCode.encode("configFile")+"=PTTB/laphd/HDPHATSINH/ajax_nguoigt";
	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=PTTB/laphd/HDPHATSINH/ajax_nguoigt";
		url_ = url_ + "&"+CesarCode.encode("makh")+"="+_makh;		
		return url_;
  }
  public String check_role( String PsMakh)
	{
		//String s="begin ? :=admin_v2.pttb_laphd.check_user('"+getUserVar("userID")+"','"+getUserVar("sys_agentcode")+"','"+PsMakh+"');"+"end;";
		String s="begin ? :=admin_v2.pttb_laphd_duongdv.check_user('"+getUserVar("userID")+"');"+"end;";
		
		System.out.println(s);
		return s;
	} 
}