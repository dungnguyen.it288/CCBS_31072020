package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import com.svcon.jdbf.*;
import javax.sql.RowSet;
import java.util.zip.*;
import java.io.*;
import java.net.*;
import org.apache.commons.net.*;
import neo.qltn.ftp;
import neo.qltn.FTPupload;
import sun.misc.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.*;
import java.text.*;
import java.nio.charset.StandardCharsets;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;

public class exp_rpt_gdvp_ao extends NEOProcessEx{	
	
public String exportCSV(
		String psthang,
		String pstinh
	){
		String fileName = new String("");
		String file = "";
		try{
			String sql = "";
			String fileUploadDir = getSysConst("FileUploadDir");
			//Lay ten file
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//ddMMyyyyHH
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//ddMMyyyyHH
				
			java.util.Date now = cal.getTime();
			String time = dateFormat.format(now).toString().replace("/","");
			fileName = "Export_RPT_DOTUOI_"+ time+ ".csv";			
			// creat folder to save csv file
			String folderPath = fileUploadDir +"report_news/";
			File f = new File(folderPath);
			if(!f.exists())
				f.mkdir();				
			file = fileUploadDir +"report_news/" +fileName;				
			File file1 = new File(file);
			if (file1.exists()) {
				file1.delete();
			}
			
		//FileWriter writer = new FileWriter(file1);
		
		OutputStream os = new FileOutputStream(file1);
		os.write(239);
		os.write(187);
		os.write(191);
		PrintWriter out = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
		
		try{
			int counter = 0;
			out.print(ConvertFont.decodeW1252("STT,M&#227; t&#7881;nh,Ch&#7911; nh&#243;m, Lo&#7841;i g&#243;i,Lo&#7841;i TB,Account Fiber, Ng&#224;y ho&#224;n c&#244;ng,T&#234;n KH, &#272;&#7883;a ch&#7881; KH,Th&#7901;i gian &#273;&#259;ng k&#253;,M&#227; ng&#432;&#7901;i GT, T&#234;n ng&#432;&#7901;i GT\n"));
			sql = "begin ? := admin_v2.pkg_rpt_sharing.get_data_gdvp_ao("
					+ "'"+pstinh+"',"
					+ "'"+psthang+"'"
					+ ");" 
					+ "end;";	
			System.out.println(sql);				
			RowSet rs = this.reqRSet(sql);	
			while (rs.next()) {				
				counter++;
				out.print(String.valueOf(counter)+"," 
						+ rs.getString("AGENT") + ","
						+ rs.getString("MSISDN") + ","
						+ rs.getString("PACKAGE_NAME") + ","
						+ rs.getString("TYPE_MSISDN") + ","						
						+ rs.getString("ACCOUNT") + ","
						+ rs.getString("FIBER_DATE") + ",\""
						+ rs.getString("ACC_NAME") + "\",\""
						+ rs.getString("ACC_ADD") + "\","
						+ rs.getString("DATE_CREATE") + ","
						+ rs.getString("MA_NGUOI") + ",\""						
						+ rs.getString("TEN_NGUOI") + "\"\n"
				);
			}			
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}finally {
			out.flush();
			out.close();
		}
		return file;
	}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}
	}
	
	

}