package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;
import com.svcon.jdbf.*;
import javax.sql.RowSet;
import java.sql.*;
import java.util.zip.*;
import java.io.*;
import  java.util.*;
import java.net.*;
import org.apache.poi.*;
public class report
    extends NEOProcessEx {
  public void run() {
    System.out.println("neo.report was called");
  }
  public  String export_xls_detal(String psPath,String func_name,String func_val)
     {	
     //	java.io.FileOutputStream fo = new java.io.FileOutputStream(psPath);
    
      
     	 int numcol =0;
     	 int rownum = 0;
		 int countnum=0;
     	 
     	 String result="";
     	try{
     		String sSql=excel_func(func_name,func_val);
			System.out.print(sSql);
      //	String sSql="begin ? := "+getSysConst("FuncSchema")
	  //			+"BCKT_TONGHOP.baobao_dk_giahan_km('"+getUserVar("sys_dataschema")+"','"+getUserVar("sys_userid")+"','"+pskhuyenmai_id+"','"+pstrangthai+"','"+tungay+"','"+denngay+"','"+pssum+"'); end;";
	  				System.out.println(sSql);
	  	       	RowSet rowes = reqRSet(sSql);	  	   
	  	         		
	  			ResultSetMetaData metas = rowes.getMetaData();
	  			org.apache.poi.hssf.usermodel.HSSFCell cell = null;
	            org.apache.poi.hssf.usermodel.HSSFRow row=null;
	            org.apache.poi.hssf.usermodel.HSSFWorkbook wb = new org.apache.poi.hssf.usermodel.HSSFWorkbook();
				org.apache.poi.hssf.usermodel.HSSFSheet sh = wb.createSheet("sheet"+"1");
				org.apache.poi.hssf.usermodel.HSSFSheet sh1 = wb.createSheet("sheet"+"2");
				
				
	  	 numcol = metas.getColumnCount();
	         
	  		 	row = sh.createRow(rownum);
	  	 	for(int i = (short)0;i<numcol;i++)
				{
				cell = row.createCell(i);
			
		      	cell.setCellValue(metas.getColumnName(i+1));
		      	
				}
			
				
		
			while(rowes.next() )
			
				{
					rownum++;
					if(rownum<=65000)
					{
					row = sh.createRow(rownum);
					
					
				for(int j = (short)0;j<numcol;j++)
					{
					char ctype=GetType(metas.getColumnTypeName(j+1));
					int clength=metas.getColumnDisplaySize(j+1);
					 cell = row.createCell(j);
						 
					 if(ctype=='N')
					 {
					 
					 cell.setCellValue(rowes.getDouble(j+1));
					 }
				  else if(ctype=='C')
					  {
					  	cell.setCellValue(rowes.getString(j+1));
					  	
					  	}
					 else if(ctype=='D')
					  {
					  	cell.setCellValue(rowes.getDate(j+1));
					  	}
				
					
					}
				 }
				
				 
				else
				 {
					row = sh1.createRow(rownum-65000);
					
					
				for(int j = (short)0;j<numcol;j++)
					{
					char ctype=GetType(metas.getColumnTypeName(j+1));
					int clength=metas.getColumnDisplaySize(j+1);
					 cell = row.createCell(j);
						 
					 if(ctype=='N')
					 {
					 
					 cell.setCellValue(rowes.getDouble(j+1));
					 }
				  else if(ctype=='C')
					  {
					  	cell.setCellValue(rowes.getString(j+1));
					  	
					  	}
					 else if(ctype=='D')
					  {
					  	cell.setCellValue(rowes.getDate(j+1));
					  	}
				
					
					}
				 }
		
			
					}	
					
				FileOutputStream out = new FileOutputStream(psPath);

		wb.write(out);
	    out.close();
		
				result = "Complete";
	  			return result;
				
	  				}catch (Exception ex){
	  		return ex.getMessage();
	  	}
     	}

  public String taofile_export_xls(String psKyhoadon,String ori_name, String func_name, String func_val){
		try{
		String path=getSysConst("FileUploadDir")+"\\TIENNO\\";
			//String chukysau="";
			String ccs=getUserVar("sys_dataschema");
			//chukysau=psKyhoadon;
			System.out.print(ccs);
			Process p = Runtime.getRuntime().exec("cmd /C mkdir "+path+ccs.replace(".","")+"\\"+psKyhoadon);
			String file=path+ccs.replace(".","")+"\\"+psKyhoadon+"\\"+ori_name+".xls";
			String zipfile=path+ccs.replace(".","")+"\\"+psKyhoadon+"\\"+ori_name+".ZIP";
	     	String out_="Tao du lieu export Excel: "+export_xls_detal(file, func_name, func_val);
			doZip(file,zipfile);
		//   String ftp="190.10.10.86 8181";
		//	String script=path+ccs.replace(".","")+"\\"+psKyhoadon+"\\script.src";
		//	String destfile=ccs.replace(".","")+"\\"+psKyhoadon+"\\khuyenmais.ZIP";
		//	ftpCopy(script,ftp,zipfile,destfile);
		
		
			return "begin ?:='"+out_+"'; end;";
		
		}catch (Exception ex){
	  		return ex.getMessage();
	  	}
	  	
	}
  public String excel_func( String var_func, String var_value)
	  {
	  String func_name="";
      String [] temp = null;
      temp = var_value.split(";");
    if (var_func=="xls_pttb_daily") 
	{
	func_name="bckt_tonghop.bc_pttb_daily";
	}
	if (var_func=="xls_km_vnp")
	{
	func_name="bctk_pttb.ds_khuyenmai_vnp";
	}
	if (var_func=="exp_ck")
	{
	func_name="bctk.lay_ds_ct";
	}
	if (var_func=="dauky_cuoiky_cuocnong")
	{
	func_name="qltn_baocao.cuocnong_dauky_cuoiky";
	}
	System.out.print(func_name);
	
   	String s=""; 	 	 		
	String sDataSchema = getUserVar("sys_dataschema");
	String sUserId = getUserVar("userID");
	String sFuncSchema = getSysConst("FuncSchema");		
	String sAgentCode = getUserVar("sys_agentcode");
	s = "begin ?:="+sFuncSchema+func_name+"('";	
	 for (int i = 0 ; i < temp.length ; i++) {
      s+=temp[i] +"','"; 
      	 
    }
    s = s.substring(0,s.length()-2);

	s+="); end;";	
		System.out.println(s);
	
	return  s;
   	  }
   public String value_dangkyfile(
		String psKyhoadon,
		String psLantra		
	)
	{ 
		return "begin ? := ccs_admin.pttb_km.dk_goikm_file("
			+"'"+psKyhoadon+"',"
			+"'"+psLantra+"',"
			+"'"+getUserVar("sys_dataschema")+"',"
			+"'"+getUserVar("userID")+"'); end;";	
	}
  public  NEOCommonData export_excel_common_2( String var_func, String var_value)
	  {
	  String func_name="";
      String [] temp = null;
      temp = var_value.split(";");
    if (var_func=="1") 
	{
	func_name="bckt_congno.ban_co";
	}
	else if(var_func=="2")
	{
	func_name="BCKT_CONGNO.tongtop_thuho_dv";
	}
	else if(var_func=="3")
	{
	    func_name="BCTK_CONGNO1.tonghop_conno_qnh_thang";
	}
	else if(var_func=="4")
	{
	   func_name="BCTK_CONGNO1.tonghop_thu_no_qnh_thang";
	}
	else if(var_func=="5")
	{
	   func_name="BCKT_CONGNO.tonghop_ht_tn";
	}
	else if (var_func=="6")
    {
	   func_name="BCTK.tiennothang_export";
	}
   	String s=""; 	 	 		
	String sDataSchema = getUserVar("sys_dataschema");
	String sUserId = getUserVar("userID");
	String sFuncSchema = getSysConst("FuncSchema");		
	String sAgentCode = getUserVar("sys_agentcode");
	s = "begin ?:="+sFuncSchema+func_name+"('";	
	 for (int i = 0 ; i < temp.length ; i++) {
      s+=temp[i] +"','"; 
      	 
    }
    s = s.substring(0,s.length()-2);

	s+="); end;";	
		System.out.println(s);
	NEOExecInfo ei_ = new NEOExecInfo(s);
	return  new NEOCommonData(ei_);
   	  }
	public  NEOCommonData export_excel_common( String func_name, String chukyno, String donvi, String nhanvien, String thungan, String tungay, String denngay,String nhombc, String kieubc, String htbc, String thuho,String schema, String user)
	  {
   	String s="";
	String sDataSchema = getUserVar("sys_dataschema");
	String sUserId = getUserVar("userID");
	String sFuncSchema = getSysConst("FuncSchema");		
	String sAgentCode = getUserVar("sys_agentcode");	
	
	s = "begin ?:="+sFuncSchema+func_name+"('"
	+chukyno+"','"+donvi+"','"+nhanvien+"','"+thungan+"','"+tungay+"','"+denngay+"','"+thuho+"','"+schema+"','"+user+"','"+nhombc
	+"','"+kieubc+"','"+htbc+"','0'); end;";	
	NEOExecInfo ei_ = new NEOExecInfo(s);
	return  new NEOCommonData(ei_);
   	  }
   public NEOCommonData getRealSQLBCCuoNgay(   String psTuNgay ,
											   String psDenNgay ,
											   String psDonViQL, 
											   String psMaBC ,
											   String psUser,									   
											   String psTrangThaiHD, 
											   String psBaoCaoCT ,
											   String psDSHD,
											   String psLoaiBC
									 ) 
	{

	String s="";
	String sDataSchema = getUserVar("sys_dataschema");	
	String sUserId = getUserVar("userID");
	String sFuncSchema = getSysConst("FuncSchema");		
	String sAgentCode = getUserVar("sys_agentcode");
	
	if (psLoaiBC =="1") {
		s = "begin ?:="+sFuncSchema+"bckt_tonghop.layds_bcth_cuoingay('"+sDataSchema+"','"
																		+sUserId 	+"','"
																		+sAgentCode +"','"																		
																		+psTuNgay	+"','"
																		+psDenNgay	+"','"
																		+psDonViQL	+"','"
																		+psMaBC		+"','"
																		+psUser		+"','"
																		+psTrangThaiHD	+"','"
																		+psBaoCaoCT	+"','"
																		+psDSHD		+"'); end;";
	}
	if (psLoaiBC =="2") {
		s = "";
	}
	if (psLoaiBC =="3") {
		s = "begin ?:={p0sys_funcschema}bckt_tonghop.layds_bcth_cuoingay('{p0sys_dataschema}','{p0sys_userid}','{p0sys_agentcode}','{p0sTuNgay}','{p0sDenNgay}','{p0sDonViQL}','{p0sMaBC}','{p0sUser}','{p0sTrangThaiHD}','{p0sBaoCaoCT}','{p0sDSHD}'); end;";
	}
	if (psLoaiBC =="4") {
		s ="";
	}
	
	NEOExecInfo ei_ = new NEOExecInfo(s);

    return  new NEOCommonData(ei_);



  }

  
   public String layds_ls_dangkygoikm(String psschema , String CommonSchema, String ma_tb)
  	{
	return "/main?"+CesarCode.encode("configFile")+"=PTTB/laphd/khuyenmai_goi/ls_khuyenmai_goi_ajax"
   				+"&"+CesarCode.encode("ma_tb") + "=" + ma_tb;
  	}
  public  String bc_thu2(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/ds_danhba_thang/bc_thu"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   			}
  public String layDauky(String psKyhoadon, String donvi, String ma_bc, String ma_nv){
		try{
		String path=getSysConst("FileUploadDir")+"\\TIENNO\\";
			//String chukysau="";
			String ccs=getUserVar("sys_dataschema");
			//chukysau=psKyhoadon;
			Process p = Runtime.getRuntime().exec("cmd /C mkdir "+path+ccs.replace(".","")+"\\"+psKyhoadon);
			String file=path+ccs.replace(".","")+"\\"+psKyhoadon+"\\TIENNO_"+psKyhoadon+".xls";
			String zipfile=path+ccs.replace(".","")+"\\"+psKyhoadon+"\\TIENNO_"+psKyhoadon+".ZIP";
	     	String out_="Tao du lieu export Excel: "+export_xls(file,psKyhoadon, donvi, ma_bc, ma_nv);
			doZip(file,zipfile);
			String ftp="190.10.10.86 8181";
			String script=path+ccs.replace(".","")+"\\"+psKyhoadon+"\\script.src";
			String destfile=ccs.replace(".","")+"\\"+psKyhoadon+"\\TIENNO_"+psKyhoadon+".ZIP";
			ftpCopy(script,ftp,zipfile,destfile);
		
		
			return "begin ?:='"+out_+"'; end;";
		
		}catch (Exception ex){
	  		return ex.getMessage();
	  	}
	  	
	}
	
	public String ftpCopy(String script, String ftp, String file, String source){
		try {
			String[] cmd={"cmd /C echo open "+ftp+">>"+script,
						  "cmd /C echo ductt>>"+script,
						  "cmd /C echo qwer1234>>"+script,
						  "cmd /C echo put "+file+" "+source+">>"+script,
						  "cmd /C ftp -s:"+script};
            Process p = Runtime.getRuntime().exec("cmd /C del "+ftp+">>"+script);
            p = Runtime.getRuntime().exec("cmd /C echo open "+ftp+">>"+script);
            p = Runtime.getRuntime().exec("cmd /C echo ductt>>"+script);
            p = Runtime.getRuntime().exec("cmd /C echo qwer1234>>"+script);
            p = Runtime.getRuntime().exec("cmd /C echo put "+file+" "+source+">>"+script);
            p = Runtime.getRuntime().exec("cmd /C ftp -s:"+script);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "0";
    }
	public void doZip(String filename,String zipfilename) throws IOException {

        try {
            FileInputStream fis = new FileInputStream(filename);
            File file=new File(filename);
            byte[] buf = new byte[fis.available()];
            fis.read(buf,0,buf.length);

            CRC32 crc = new CRC32();
            ZipOutputStream s = new ZipOutputStream(
                    (OutputStream)new FileOutputStream(zipfilename));

            s.setLevel(6);

            ZipEntry entry = new ZipEntry(file.getName());
            entry.setSize((long)buf.length);
            crc.reset();
            crc.update(buf);
            entry.setCrc( crc.getValue());
            s.putNextEntry(entry);
            s.write(buf, 0, buf.length);
            s.finish();
            s.close();
            fis.close();
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	public  String export_khuyenmais(String psPath, String pskhuyenmai_id, String pstrangthai, String tungay, String denngay,String pssum)
     {	
     //	java.io.FileOutputStream fo = new java.io.FileOutputStream(psPath);
    
      
     	 int numcol =0;
     	 int rownum = 0;
		 int countnum=0;
     	 
     	 String result="";
     	try{
     		
     		String sSql="begin ? := "+getSysConst("FuncSchema")
	  			+"BCKT_TONGHOP.baobao_dk_giahan_km('"+getUserVar("sys_dataschema")+"','"+getUserVar("sys_userid")+"','"+pskhuyenmai_id+"','"+pstrangthai+"','"+tungay+"','"+denngay+"','"+pssum+"'); end;";
	  				System.out.println(sSql);
	  	       	RowSet rowes = reqRSet(sSql);	  	   
	  	         		
	  			ResultSetMetaData metas = rowes.getMetaData();
	  			org.apache.poi.hssf.usermodel.HSSFCell cell = null;
	            org.apache.poi.hssf.usermodel.HSSFRow row=null;
	            org.apache.poi.hssf.usermodel.HSSFWorkbook wb = new org.apache.poi.hssf.usermodel.HSSFWorkbook();
				org.apache.poi.hssf.usermodel.HSSFSheet sh = wb.createSheet("sheet"+"1");
				org.apache.poi.hssf.usermodel.HSSFSheet sh1 = wb.createSheet("sheet"+"2");
				
				
	  	 numcol = metas.getColumnCount();
	         
	  		 	row = sh.createRow(rownum);
	  	 	for(int i = (short)0;i<numcol;i++)
				{
				cell = row.createCell(i);
			
		      	cell.setCellValue(metas.getColumnName(i+1));
		      	
				}
			
				
		
			while(rowes.next() )
			
				{
					rownum++;
					if(rownum<=65000)
					{
					row = sh.createRow(rownum);
					
					
				for(int j = (short)0;j<numcol;j++)
					{
					char ctype=GetType(metas.getColumnTypeName(j+1));
					int clength=metas.getColumnDisplaySize(j+1);
					 cell = row.createCell(j);
						 
					 if(ctype=='N')
					 {
					 
					 cell.setCellValue(rowes.getDouble(j+1));
					 }
				  else if(ctype=='C')
					  {
					  	cell.setCellValue(rowes.getString(j+1));
					  	
					  	}
					 else if(ctype=='D')
					  {
					  	cell.setCellValue(rowes.getDate(j+1));
					  	}
				
					
					}
				 }
				
				 
				else
				 {
					row = sh1.createRow(rownum-65000);
					
					
				for(int j = (short)0;j<numcol;j++)
					{
					char ctype=GetType(metas.getColumnTypeName(j+1));
					int clength=metas.getColumnDisplaySize(j+1);
					 cell = row.createCell(j);
						 
					 if(ctype=='N')
					 {
					 
					 cell.setCellValue(rowes.getDouble(j+1));
					 }
				  else if(ctype=='C')
					  {
					  	cell.setCellValue(rowes.getString(j+1));
					  	
					  	}
					 else if(ctype=='D')
					  {
					  	cell.setCellValue(rowes.getDate(j+1));
					  	}
				
					
					}
				 }
		
			
					}	
					
				FileOutputStream out = new FileOutputStream(psPath);

		wb.write(out);
	    out.close();
		
				result = "Complete";
	  			return result;
				
	  				}catch (Exception ex){
	  		return ex.getMessage();
	  	}
     	}
	public String lay_khuyenmais(String psKyhoadon, String pskhuyenmai_id, String pstrangthai, String tungay, String denngay,String pssum){
		try{
		String path=getSysConst("FileUploadDir")+"\\TIENNO\\";
			//String chukysau="";
			String ccs=getUserVar("sys_dataschema");
			//chukysau=psKyhoadon;
			Process p = Runtime.getRuntime().exec("cmd /C mkdir "+path+ccs.replace(".","")+"\\"+psKyhoadon);
			String file=path+ccs.replace(".","")+"\\"+psKyhoadon+"\\khuyenmais.xls";
			String zipfile=path+ccs.replace(".","")+"\\"+psKyhoadon+"\\khuyenmais.ZIP";
	     	String out_="Tao du lieu export Excel: "+export_khuyenmais(file, pskhuyenmai_id, pstrangthai, tungay, denngay, pssum);
			doZip(file,zipfile);
			String ftp="190.10.10.86 8181";
			String script=path+ccs.replace(".","")+"\\"+psKyhoadon+"\\script.src";
			String destfile=ccs.replace(".","")+"\\"+psKyhoadon+"\\khuyenmais.ZIP";
			ftpCopy(script,ftp,zipfile,destfile);
		
		
			return "begin ?:='"+out_+"'; end;";
		
		}catch (Exception ex){
	  		return ex.getMessage();
	  	}
	  	
	}
	public  String export_xls(String psPath, String psKyhoadon, String donvi, String ma_bc, String ma_nv)
     {	
     //	java.io.FileOutputStream fo = new java.io.FileOutputStream(psPath);
    
      
     	 int numcol =0;
     	 int rownum = 0;
     	 
     	 String result="";
     	try{
     		
     		String sSql="begin ? := "+getSysConst("FuncSchema")
	  			+"bctk.tiennothang_export('"+psKyhoadon+"','"+getUserVar("sys_userid")+"','"+getUserVar("sys_dataschema")+"',"+donvi+",'"+ma_bc+"','"+ma_nv+"'); end;";
	  				System.out.println(sSql);
	  	       	RowSet rowes = reqRSet(sSql);	  	   
	  	         		
	  			ResultSetMetaData metas = rowes.getMetaData();
	  			org.apache.poi.hssf.usermodel.HSSFCell cell = null;
	            org.apache.poi.hssf.usermodel.HSSFRow row=null;
	              org.apache.poi.hssf.usermodel.HSSFWorkbook wb = new org.apache.poi.hssf.usermodel.HSSFWorkbook();
	            org.apache.poi.hssf.usermodel.HSSFSheet sh = wb.createSheet("sheet"+"1");
	        
	    
	  		 numcol = metas.getColumnCount();
	 
	  		 	row = sh.createRow(rownum);
	  	 	for(int i = (short)0;i<numcol;i++)
				{
				cell = row.createCell(i);
			
		      	cell.setCellValue(metas.getColumnName(i+1));
		      	
				}
			while(rowes.next())
			
				{
					rownum++;
					 row = sh.createRow(rownum);
				for(int j = (short)0;j<numcol;j++)
					{
					char ctype=GetType(metas.getColumnTypeName(j+1));
					int clength=metas.getColumnDisplaySize(j+1);
					 cell = row.createCell(j);
						 
					 if(ctype=='N')
					 {
					 
					 cell.setCellValue(rowes.getDouble(j+1));
					 }
				  else if(ctype=='C')
					  {
					  	cell.setCellValue(rowes.getString(j+1));
					  	
					  	}
					 else if(ctype=='D')
					  {
					  	cell.setCellValue(rowes.getDate(j+1));
					  	}
				
					
					}
				 
					}
					
				FileOutputStream out = new FileOutputStream(psPath);

		wb.write(out);
	    out.close();
		
				result = "Complete";
	  			return result;
				
	  				}catch (Exception ex){
	  		return ex.getMessage();
	  	}
     	}
		public char GetType(String types)
    {
        char return_value='C';
        if (types=="VARCHAR2")
        	return_value='C';
        else if(types=="NUMBER")
        	return_value='N';
        else if (types=="DATE")
        	return_value='D';
        else
           return_value='C';
        return return_value;
    }
	public  String layds_loaikh(String ma_cq, String loaikh, String doituong, String nganhnghe, String nguoivandong, String khthanthiet, String khcuoccao, String khnhieutb)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=PTTB/capnhat/ma_coquan/dsloaikh_ajax"
   				+"&"+CesarCode.encode("ma_cq") + "=" + ma_cq
				+"&"+CesarCode.encode("loaikh") + "=" +  loaikh
				+"&"+CesarCode.encode("doituong") + "=" + doituong
				+"&"+CesarCode.encode("nganhnghe") + "=" + nganhnghe
				+"&"+CesarCode.encode("nguoivandong") + "=" + nguoivandong
				+"&"+CesarCode.encode("khthanthiet") + "=" + khthanthiet
				+"&"+CesarCode.encode("khcuoccao") + "=" + khcuoccao
				+"&"+CesarCode.encode("khnhieutb") + "=" + khnhieutb
				;
   			}
public  String taods_them(
	            String ma_cq, String loaikh, String doituong, 
	            String nganhnghe, String nguoivandong, String khthanthiet, 
				String khcuoccao, String khnhieutb, String sotbtu, String sotbden, String capnhat, String ngayld)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=PTTB/capnhat/ma_coquan/ds_themmoi_kh_ajax"
   				+"&"+CesarCode.encode("ma_cq") + "=" + ma_cq
				+"&"+CesarCode.encode("loaikh") + "=" +  loaikh
				+"&"+CesarCode.encode("doituong") + "=" + doituong
				+"&"+CesarCode.encode("nganhnghe") + "=" + nganhnghe
				+"&"+CesarCode.encode("nguoivandong") + "=" + nguoivandong			
				+"&"+CesarCode.encode("khthanthiet") + "=" + khthanthiet
				+"&"+CesarCode.encode("khcuoccao") + "=" +khcuoccao
				+"&"+CesarCode.encode("khnhieutb") + "=" + khnhieutb
				+"&"+CesarCode.encode("sotbtu") + "=" + sotbtu
				+"&"+CesarCode.encode("sotbden") + "=" + sotbden
				+"&"+CesarCode.encode("capnhat") + "=" + capnhat
				+"&"+CesarCode.encode("ngayld") + "=" + ngayld
				;
   			}
		public  String themmoi_dslkh(
                String ma_cq, String loaikh, String doituong, 
				String nganhnghe, String nguoivandong, 
				String sotbtu, String sotbden, String khthanthiet, String khcuoccao, 
				String khnhieutb, String ds_km )
   		{
   			
				return "begin ? := ccs_admin.BCKT_TONGHOP.themmoi_dscskh('"+ma_cq
								 +"','"+loaikh+"',"
								 +"'"+ doituong+"',"
								 +"'"+nganhnghe+"',"
								 +"'"+nguoivandong+"',"
								 +"'"+sotbtu+"',"
								 +"'"+sotbden+"',"
								 +"'"+getUserVar("sys_dataschema")+"',"
								 +"'"+getUserVar("userID")+"',"														
								 +"'"+khthanthiet+"',"
								 +"'"+khcuoccao+"',"
								 +"'"+khnhieutb+"',"
								 +"'"+ds_km+"'); end;";
   			}
		public  String xoadl_dslkh(String ma_cq)
   		{
   			
				return "begin ? := ccs_admin.BCKT_TONGHOP.xoadl_dscskh('"+ma_cq
								 +"','"+getUserVar("sys_dataschema")+"',"
								 +"'"+getUserVar("userID")+"'); end;";
   			}
			public  String capnhat_dslkh(
                String ma_cq, String loaikh, String doituong, 
				String nganhnghe, String nguoivandong, String sotbtu, 
				String sotbden, String khthanthiet, String khcuoccao, 
				String khnhieutb, String dskhoamay)
   		{
   			
				return "begin ? := ccs_admin.BCKT_TONGHOP.capnhat_dscskh('"+ma_cq
								 +"','"+loaikh+"',"
								 +"'"+ doituong+"',"
								 +"'"+nganhnghe+"',"
								 +"'"+nguoivandong+"',"
								 +"'"+sotbtu+"',"
								 +"'"+sotbden+"',"
								 +"'"+getUserVar("sys_dataschema")+"',"
								 +"'"+getUserVar("userID")+"',"														
								 +"'"+khthanthiet+"',"
								 +"'"+khcuoccao+"',"
								 +"'"+khnhieutb+"',"
								 +"'"+dskhoamay+"'); end;";
   			}
	public  String layds_macq(String ma_cq, String ma_tb, String chuky, String trangthai, String sotbtu, String sotbden)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/bc_tonghop_hcm/ds_macq_ajax"
   				+"&"+CesarCode.encode("ma_cq") + "=" + ma_cq
				+"&"+CesarCode.encode("ma_tb") + "=" + ma_tb
				+"&"+CesarCode.encode("chuky") + "=" + chuky
				+"&"+CesarCode.encode("trangthai_id") + "=" + trangthai
				+"&"+CesarCode.encode("sotbtu") + "=" + sotbtu
				+"&"+CesarCode.encode("sotbden") + "=" + sotbden
   					;
   			}
	public  String layds_loaikh_macq(String ma_cq, String ma_kh,String ma_tb,String ma_bc, String psschema, String userid, String pssum)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/bc_tonghop_hcm/bc_loaikh_macq_form"
   				+"&"+CesarCode.encode("ma_cq") + "=" + ma_cq
				+"&"+CesarCode.encode("ma_kh") + "=" + ma_kh
				+"&"+CesarCode.encode("ma_tb") + "=" + ma_tb
				+"&"+CesarCode.encode("ma_bc") + "=" + ma_bc
				+"&"+CesarCode.encode("psschema") + "=" + psschema
				+"&"+CesarCode.encode("userid") + "=" + userid
				+"&"+CesarCode.encode("pssum") + "=" + pssum
				;
   			}
	public  String layds_loaikh_macq_ct(String ma_cq, String ma_kh,String ma_tb,String ma_bc,String psschema, String userid, String pssum)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/bc_tonghop_hcm/bc_loaikh_macq_form_ct"
   				+"&"+CesarCode.encode("ma_cq") + "=" + ma_cq
				+"&"+CesarCode.encode("ma_kh") + "=" + ma_kh
				+"&"+CesarCode.encode("ma_tb") + "=" + ma_tb
				+"&"+CesarCode.encode("ma_bc") + "=" + ma_bc
				+"&"+CesarCode.encode("psschema") + "=" + psschema
				+"&"+CesarCode.encode("userid") + "=" + userid
				+"&"+CesarCode.encode("pssum") + "=" + pssum
				;
   			}
public  String dskh_tracuoc(String tuky, String denky, String loaikh, String sotb, String cuoctu, String cuocden,
String tongcuoctu, String tongcuocden)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=ccbs_qltn/tracuu_cuoc/ds_khcuoc_ajax"
   				+"&"+CesarCode.encode("tuky") + "=" + tuky
				+"&"+CesarCode.encode(" denky") + "=" + denky
				+"&"+CesarCode.encode("loaikh") + "=" + loaikh
				+"&"+CesarCode.encode("sotb") + "=" + sotb
				+"&"+CesarCode.encode("cuoctu") + "=" + cuoctu
				+"&"+CesarCode.encode("cuocden") + "=" + cuocden
				+"&"+CesarCode.encode("tongcuoctu") + "=" + tongcuoctu
				+"&"+CesarCode.encode("tongcuocden") + "=" + tongcuocden
   					;
   			}
   public String tigia(
   String funcdata,
   String thang, String nam)
     {
	return "begin ? := "+funcdata+"bctk.tigia('"+thang+"',"
								 +"'"+nam+"'); end;";
	 }
	  public  String chon_thuquys(String thuquy)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/chon_thuquy_s"
   				+"&"+CesarCode.encode("thuquy") + "=" + thuquy 	   				
				
   					;
   			}
	public String change_donvi_bc(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/change_donvi_bc"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   			}
	public  String change_nguoithuchien(String donvi_ql, String bc_thu)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/ajax_nguoithuchien"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql 	   				
				+"&"+CesarCode.encode("ma_bc") + "=" + bc_thu 
   					;
   			}
	 public  String tuyenthu_bc(String buucuc)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_tuyen_buucuc"
   				+"&"+CesarCode.encode("ma_bc") + "=" + buucuc 	   				
   					;
   			}	
	public  String change_thungan_donvi(String donvi_ql, String chukyno)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/ds_thungan_data"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql
				+"&"+CesarCode.encode("chukyno") + "=" + chukyno
				;
   			}
public  String change_thungan_dv(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/ds_thungan_dv"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql
				;
   			}
public  String lay_kq_th_manv_bdp_(String schema ,String chuky, String donvi, String ma_bc
                      , String ma_nv, String duno, String page_num, String page_rec)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/bc_congno/THBDPHI_TENNV_form_inclu_"
   				+"&"+CesarCode.encode("chukyno") + "=" + chuky 	   				
				+"&"+CesarCode.encode("dvql") + "=" + donvi
				+"&"+CesarCode.encode("ma_bc") + "=" + ma_bc 
				+"&"+CesarCode.encode("duno") + "=" + duno
				+"&"+CesarCode.encode("ma_nv") + "=" + ma_nv 
				+"&"+CesarCode.encode("page_num") + "=" + page_num
 				+"&"+CesarCode.encode("page_rec") + "=" + page_rec
   					;
   			}

//tuyetptm them
public  String lay_kq_th_manv_bdp_hth(String schema ,String chuky, String donvi, String ma_bc
                      , String ma_nv, String duno, String page_num, String page_rec)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/bc_congno/THBDPHI_TENNV_form_inclu_hth"
   				+"&"+CesarCode.encode("chukyno") + "=" + chuky 	   				
				+"&"+CesarCode.encode("dvql") + "=" + donvi
				+"&"+CesarCode.encode("ma_bc") + "=" + ma_bc 
				+"&"+CesarCode.encode("duno") + "=" + duno
				+"&"+CesarCode.encode("ma_nv") + "=" + ma_nv 
				+"&"+CesarCode.encode("page_num") + "=" + page_num
 				+"&"+CesarCode.encode("page_rec") + "=" + page_rec
   					;
   			}			
			
public  String change_bc(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_change_bc"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql 	   				
   					;
   			}			
	public  String lay_kq_th_makh_bdp_(String schema ,String chuky, String donvi, String ma_bc, String duno, 
                       String ma_nv, String ma_t, String page_num, String page_rec)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/bc_congno/THBDPHI_MAKH_form_inclu_"
   				+"&"+CesarCode.encode("chukyno") + "=" + chuky 	   				
				+"&"+CesarCode.encode("dvql") + "=" + donvi
				+"&"+CesarCode.encode("ma_bc") + "=" + ma_bc 
				+"&"+CesarCode.encode("duno") + "=" + duno
				+"&"+CesarCode.encode("ma_nv") + "=" + ma_nv 
				+"&"+CesarCode.encode("ma_t") + "=" + ma_t
				+"&"+CesarCode.encode("page_num") + "=" + page_num
 				+"&"+CesarCode.encode("page_rec") + "=" + page_rec
   					;
   			}
    public  String lay_kq_th_thuebao_bdp_(String schema ,String chuky, String donvi, String ma_bc, String duno, 
                       String ma_nv, String ma_t, String page_num, String page_rec)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/THBDPHI_TB_form_inclu_"
   				+"&"+CesarCode.encode("chukyno") + "=" + chuky 	   				
				+"&"+CesarCode.encode("dvql") + "=" + donvi
				+"&"+CesarCode.encode("ma_bc") + "=" + ma_bc 
				+"&"+CesarCode.encode("duno") + "=" + duno
				+"&"+CesarCode.encode("ma_nv") + "=" + ma_nv 
				+"&"+CesarCode.encode("ma_t") + "=" + ma_t
				+"&"+CesarCode.encode("page_num") + "=" + page_num
 				+"&"+CesarCode.encode("page_rec") + "=" + page_rec
   					;
   			}	
public  String chamsockh( String tuky, String denky,String thangqui, String chitiet, String nganhnghe, String doituong,String khlon,String page_num, String page_rec)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/CHAMSOCKHACHHANG_form_cuoc_trang"
   				+"&"+CesarCode.encode("chuky") + "=" + tuky 	   				
				+"&"+CesarCode.encode("denky") + "=" + denky
				+"&"+CesarCode.encode("thang_quy") + "=" + thangqui
				+"&"+CesarCode.encode("khlon") + "=" + khlon
				+"&"+CesarCode.encode("chitiet") + "=" + chitiet
				+"&"+CesarCode.encode("nganhnghe") + "=" + nganhnghe 				
				+"&"+CesarCode.encode("doituong") + "=" + doituong 				
				+"&"+CesarCode.encode("page_num") + "=" + page_num				
 				+"&"+CesarCode.encode("page_rec") + "=" + page_rec
   					;
   			}				
public  String lay_tiennothang(String chuky, String donvi, String ma_bc,
                       String ma_nv,String page_num, String page_rec)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/TIENNOKHACHHANG_ajax_trang"
   				+"&"+CesarCode.encode("chukyno") + "=" + chuky 	   				
				+"&"+CesarCode.encode("dvql") + "=" + donvi
				+"&"+CesarCode.encode("buucuc") + "=" + ma_bc 				
				+"&"+CesarCode.encode("ma_nv") + "=" + ma_nv 				
				+"&"+CesarCode.encode("page_num") + "=" + page_num
 				+"&"+CesarCode.encode("page_rec") + "=" + page_rec
   					;
   			}		
	 public  String lay_kq_thuno_theotuyen_(String chuky, String donvi, String tuyen, String ma_tn, 
                        String tungay, String denngay, String dieukien, String loaibc, String loainhom, 
						String schema ,String userid,String ma_bc,String kyhd, String kttien, String page_num, String page_rec)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/xulytrang/page_num_form"
   				+"&"+CesarCode.encode("chuky") + "=" + chuky 	   				
				+"&"+CesarCode.encode("donvi") + "=" + donvi 
				+"&"+CesarCode.encode("tuyen") + "=" + tuyen 
				+"&"+CesarCode.encode("ma_tn") + "=" + ma_tn 
				+"&"+CesarCode.encode("tungay") + "=" + tungay 
				+"&"+CesarCode.encode("denngay") + "=" + denngay 
				+"&"+CesarCode.encode("dieuken") + "=" + dieukien 
				+"&"+CesarCode.encode("loaibc") + "=" + loaibc
				+"&"+CesarCode.encode("loainhom") + "=" + loainhom
				+"&"+CesarCode.encode("ma_bc") + "=" + ma_bc
                +"&"+CesarCode.encode("kttien") + "=" + kttien				
				+"&"+CesarCode.encode("page_num") + "=" + page_num
				+"&"+CesarCode.encode("kyhd") + "=" + kyhd
 				+"&"+CesarCode.encode("page_rec") + "=" + page_rec
   					;
   			}
 public  String thuno_common(String chuky, String donvi, String tuyen, String ma_tn, 
                        String tungay, String denngay, String dieukien, String loaibc, String loainhom, 
						String schema ,String userid,String ma_bc,String kyhd, String kttien, String page_num, String page_rec)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/thunotheotuyen_trang"
   				+"&"+CesarCode.encode("chuky") + "=" + chuky 	   				
				+"&"+CesarCode.encode("donvi") + "=" + donvi 
				+"&"+CesarCode.encode("tuyen") + "=" + tuyen 
				+"&"+CesarCode.encode("ma_tn") + "=" + ma_tn 
				+"&"+CesarCode.encode("tungay") + "=" + tungay 
				+"&"+CesarCode.encode("denngay") + "=" + denngay 
				+"&"+CesarCode.encode("dieukien") + "=" + dieukien 
				+"&"+CesarCode.encode("loaibc") + "=" + loaibc
				+"&"+CesarCode.encode("loainhom") + "=" + loainhom
				+"&"+CesarCode.encode("ma_bc") + "=" + ma_bc
                +"&"+CesarCode.encode("kttien") + "=" + kttien				
				+"&"+CesarCode.encode("page_num") + "=" + page_num
				+"&"+CesarCode.encode("kyhd") + "=" + kyhd
 				+"&"+CesarCode.encode("page_rec") + "=" + page_rec
   					;
   			}				
	   public String chukyno_truoc(
				String funcdata,
				String psSchema,
				String thang)
     {
	       return "begin ? := "+funcdata+"qltn.chukyno_kytruoc('"+thang+"','"+psSchema+"',"
								 +"''); end;";
	 }//cap nhat khach hang theo ma co quan
		 public String capnhat_macqkh(
				String macq,
				String ma_tb,
				String loaikh, 
				String loaikhlon,
				String msthue, 
				String ngaysinh, 
				String diachi_tt, 
				String diachi_ct, 
				String quantt_id, 
				String phuongtt_id, 
				String phott_id, 
				String sonhatt_id, 
				String quanct_id, 
				String phuongct_id, 
				String phoct_id, 
				String sonhact_id,
				String diachi_bc,
				String quanbc_id, 
				String phuongbc_id, 
				String phobc_id, 
				String sonha_bc, 
				String nguoivd,
				String doituong_id, 
				String nganhnghe_id, 
				String diadiemtt, 
				String chuyenkhoan_id,
				String userid
				)
     {
	       return "begin ? := ccs_admin.BCKT_CONGNO.capnhat_kh_cq('"+macq
		                     +"','"+ma_tb+"'"
							 +",'"+loaikh+"'"
							 +",'"+loaikhlon+"'"
							 +",'"+msthue+"'"
							 +",'"+ngaysinh+"'"
							 +",'"+diachi_tt+"'"
							 +",'"+diachi_ct+"'"
							 +",'"+quantt_id+"'"
							 +",'"+phuongtt_id+"'"
							 +",'"+phott_id+"'"
							 +",'"+sonhatt_id+"'"
							 +",'"+quanct_id+"'"
							 +",'"+phuongct_id+"'"
							 +",'"+phoct_id+"'"
							 +",'"+sonhact_id+"'"
							 +",'"+diachi_bc+"'"
							 +",'"+quanbc_id+"'"
							 +",'"+phuongbc_id+"'"
							 +",'"+phobc_id+"'"
							 +",'"+sonha_bc+"'"
							 +",'"+nguoivd+"'"
							 +",'"+doituong_id+"'"
							 +",'"+nganhnghe_id+"'"
							 +",'"+diadiemtt+"'"
							 +",'"+chuyenkhoan_id+"'"
							 +",'"+getUserVar("sys_dataschema")
							 +"','"+ userid+ "'); end;";
	 }
	 public  String lay_ds_nganhang(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/bc_nganhang/ajax_nganhangs"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql 	   				
   					;
   			}
		public  String ajax_hinhthuc_chon(String hinhthuc)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/htts_chon"
   				+"&"+CesarCode.encode("chukyno") + "="+hinhthuc;
   			}
		public  String ajax_nhanvientcs(String donviql_id)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/ajax_nhanvientcs"
   				+"&"+CesarCode.encode("donviql_id") + "="+donviql_id;
   			}
	 	 public  String ajax_hinhthuc(String hinhthuc)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/htts"
   				+"&"+CesarCode.encode("hinhthuc") + "="+hinhthuc;
   			}
	 public  String lay_ds_dv_di(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/bc_no/lay_ds_dv_ajax"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql 	   				
   					;
   			}	
   public  String bc_thu(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/bc_thu"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   			}
	public  String bc_thu1(String donvi_ql){
		return "/main?"+CesarCode.encode("configFile")+"=report/BC_PTTB/Duongthu/bc_thu"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
	}
   public  String bc_thu_user(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+  	"=report/BC_PTTB/bc_duongthu/bc_thu_users"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   			}			
   public  String bc_thu_t()
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/bc_thu_s";
   			}			
   public  String bc_thu_hni(String phuongid)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/bc_thu_hn"
   				+"&"+CesarCode.encode("phuongid") + "=" + phuongid;
   			}			
	 public  String change_user(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_user"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   			}
	 public  String layds_giaodich(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/BC_PTTB/BC_PTTB_HNI/param_user"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   			}			
	public  String change_user1(String bc)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_user1"
   				+"&"+CesarCode.encode("bc") + "=" + bc;
   			}
	public  String change_user_HNI(String donviql_id, String ma_bc){
   		return "/main?"+CesarCode.encode("configFile")+"=report/BC_PTTB/Duongthu/param_user"
			    +"&"+CesarCode.encode("donviql_id") + "=" + donviql_id
   				+"&"+CesarCode.encode("ma_bc") + "=" + ma_bc;
   	}	
/* Bao cao thay doi thong tin khach hang hni: id = 2015 */
//thay doi user theo don vi			
	 public  String change_user_donvi(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/BC_PTTB/thaydoi_ttkh_hni/change_user_dvql"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   			}
//thay doi user theo buu cuc		
	 public  String change_user_buucuc(String ma_bc)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/BC_PTTB/thaydoi_ttkh_hni/change_user_bc"
   				+"&"+CesarCode.encode("ma_bc") + "=" + ma_bc;
   			}			
/* thay doi buu cuc theo don vi */
public  String change_bc_tdtt_hni(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/BC_PTTB/thaydoi_ttkh_hni/bc_thu"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   			}
/* thay doi tuyen theo don vi */
public  String change_tuyen(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/rpt_bankehoadon/change_tuyen"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   			}
/* thay doi nhan vien thu theo don vi */
public  String change_nvtc(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/rpt_bankehoadon/change_nvthu"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   			}
   public  String nguoi_bc(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_nguoi_bc"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   			}
   public  String nguoi_ld(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_nguoi_ld"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   			}
  public  String param_nv_tc(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_nv_tc"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   			}
public  String param_nv_tc_theobc(String donvi_ql)
   	{
   	return "/main?"+CesarCode.encode("configFile")+"=report/param_nv_tc_theobc"
   		+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   	}	
public  String param_tuyen_tc(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_tuyen_tc"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql
				;
   			}			
   public String capnhat_thtkbc(
    String psuserip
	,String psten_bc
	,String psmatinh
	,String bc_count
	,String psuser_id
   )
   {
   return "begin ? := CCS_ADMIN.TRY.them_bc('"+psuserip+"',"
								 +"'"+psten_bc+"',"
								 +"'"+psmatinh+"',"
								 +"'"+bc_count+"',"
								 +"'"+psuser_id+"'); end;";
   }
   	public  String tuyen_ndh(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/tuyen_ndh"
   				+"&"+CesarCode.encode("maquay") + "=" + donvi_ql 	   				
   					;
   			}
  public String lay_ttno (
  String ma_kh, 
  String ma_tb
  )
   {
      return "/main?"+CesarCode.encode("configFile")+"=report/thanhtoan_cuocphidd_ht"
   		    	+"&"+CesarCode.encode("ma_kh") + "=" + ma_kh
				+"&"+CesarCode.encode("ma_tb") + "=" + ma_tb
		;
   }
   public String lay_ttno_th(
  String ma_kh, 
  String ma_tb
  )
   {
      return "/main?"+CesarCode.encode("configFile")+"=report/thanhtoan_cuocphidd_ht_th"
   		    	+"&"+CesarCode.encode("ma_kh") + "=" + ma_kh
				+"&"+CesarCode.encode("ma_tb") + "=" + ma_tb
		;
   }
  public String get_tenhopdong(
     String datafunc
	,String agentcode
   )
   {
   return "begin ? := "+datafunc+"BCTK_ITEXT.get_tenhopdong("
								  +"'"+datafunc+"',"
								 +"'"+agentcode+"'); end;";
   } 
    public String laytenhopdong(
     String datafunc
	,String agentcode
   )
   {
   return "begin ? := "+datafunc+"BCTK_ITEXT.gettenhopdong("
								  +"'"+datafunc+"',"
								 +"'"+agentcode+"'); end;";
   } 
   public String get_hd_chuyenquyen(
    String datafunc
	,String agentcode
   )
   {
   return "begin ? := "+datafunc+"BCTK_ITEXT.get_hd_chuyenquyen("
								  +"'"+datafunc+"',"
								 +"'"+agentcode+"'); end;";
   }
   public String get_hdthanhly(
    String datafunc
	,String agentcode
   )
   {
   return "begin ? := "+datafunc+"BCTK_ITEXT.get_hdthanhly("
								  +"'"+datafunc+"',"
								 +"'"+agentcode+"'); end;";
   }
 
   public String thuno_theotuyen(
   	String chukyno,
   	String manv,
   	String donvi_ql)
   {
   	
   		return "/main?"+CesarCode.encode("configFile")+"=report/ds_thunott_ajax"
   		    	+"&"+CesarCode.encode("pschukyno") + "=" + chukyno
   		        + "&"+CesarCode.encode("manv") + "=" + manv
   		        +"&"+CesarCode.encode("donvi_ql") + "=" + donvi_ql 	   				
		;
   	}

public  String phuongxa(String dvql)
   		{
 return "/main?"+CesarCode.encode("configFile")+"=report/param_phuongxa"
   				+"&"+CesarCode.encode("donviql_id") + "=" + dvql 	   				
   					;                      

   			}
	 public  String tuyenthu_tthu(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/tuyenthu"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql 	   				
   					;
   			}
	 public  String layds_manv_cs(String ma_cq,String ma_tb, String ma_vd, String chukyno, String loaikh_id, String doituong , String nganhnghe,String trangthai)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/baocao_macq/ds_kh_manvncs"
   				+"&"+CesarCode.encode("ma_cq") + "=" + ma_cq 	   				
				+"&"+CesarCode.encode("ma_tb") + "=" + ma_tb
				+"&"+CesarCode.encode("ma_vd") + "=" + ma_vd
				+"&"+CesarCode.encode("chukyno") + "=" + chukyno
				+"&"+CesarCode.encode("loaikh_id") + "=" + loaikh_id
				+"&"+CesarCode.encode("doituong_id") + "=" + doituong
				+"&"+CesarCode.encode("nganhnghe_id") + "=" + nganhnghe
				+"&"+CesarCode.encode("trangthai") + "=" + trangthai
   				;
   			}
	 public  String layds_manv_cs_ct(String ma_cq,String ma_tb, String ma_vd, String chukyno, String loaikh_id, String doituong , String nganhnghe,String trangthai)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/baocao_macq/ds_kh_manvncs_ct"
   				+"&"+CesarCode.encode("ma_cq") + "=" + ma_cq 	   				
				+"&"+CesarCode.encode("ma_tb") + "=" + ma_tb
				+"&"+CesarCode.encode("ma_vd") + "=" + ma_vd
				+"&"+CesarCode.encode("chukyno") + "=" + chukyno
				+"&"+CesarCode.encode("loaikh_id") + "=" + loaikh_id
				+"&"+CesarCode.encode("doituong_id") + "=" + doituong
				+"&"+CesarCode.encode("nganhnghe_id")+ "= " + nganhnghe
				+"&"+CesarCode.encode("trangthai") + "=" + trangthai
   				;
   			}
	public  String layds_manv_cs_ck(String ma_cq,String ma_tb, String ma_vd, String chukyno, String loaikh_id, String doituong , String nganhnghe,String trangthai)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/baocao_macq/ds_kh_manvncs_ck"
   				+"&"+CesarCode.encode("ma_cq") + "=" + ma_cq 	   				
				+"&"+CesarCode.encode("ma_tb") + "=" + ma_tb
				+"&"+CesarCode.encode("ma_vd") + "=" + ma_vd
				+"&"+CesarCode.encode("chukyno") + "=" + chukyno
				+"&"+CesarCode.encode("loaikh_id") + "=" + loaikh_id
				+"&"+CesarCode.encode("doituong_id") + "=" + doituong
				+"&"+CesarCode.encode("nganhnghe_id")+ "= " + nganhnghe
				+"&"+CesarCode.encode("trangthai") + "=" + trangthai
   				;
   			}
	 public  String layds_khcq(String ma_cq,String ma_tb)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=PTTB/capnhat/ma_coquan/ds_kh_capnhatmacq"
   				+"&"+CesarCode.encode("ma_cq") + "=" + ma_cq 	   				
				+"&"+CesarCode.encode("ma_tb") + "=" + ma_tb
   				;
   			}
	public  String layls_khcq(String ma_cq,String ma_tb,String tungay, String denngay)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=PTTB/capnhat/ma_coquan/ds_ls_capnhatmacq"
   				+"&"+CesarCode.encode("ma_cq") + "=" + ma_cq 	   				
				+"&"+CesarCode.encode("ma_tb") + "=" + ma_tb
				+"&"+CesarCode.encode("tungay") + "=" + tungay
				+"&"+CesarCode.encode("denngay") + "=" +denngay
   				;
   			}
	
   public  String tuyenthu(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_tuyen_luyke_tn"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql 	   				
   					;
   			}
	public  String nguoithuchien(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_nguoithuchien"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql 	   				
   					;
   			}		
    public  String tuyenthu_tn(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_tuyen_tn"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql 	   				
   					;
   			}
	 public  String tuyenthu_nv(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_tuyen_nv"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql 	   				
   					;
   			}
	  public  String tuyenthu_tnhnm(String donvi_ql,String chuky)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_tuyen_tnhnm"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql 
				+"&"+CesarCode.encode("chuky") + "=" + chuky 
   					;
   			}
	 public  String thungan(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_thungan"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql 	   				
   					;
   			}
			public  String tuyenthu_hoadon(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_tuyen_hoadon"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql 	   				
   					;
   			}
			 public  String quaythu(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_quaythu"
   				+"&"+CesarCode.encode("maquay") + "=" + donvi_ql 	   				
   					;
   			}
public  String quaythu_ghino(String donvi_ql)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_tuyen_ghino"
   				+"&"+CesarCode.encode("maquay") + "=" + donvi_ql		
   					;
   			}
	public  String quaythu_ghino_km(String donvi_ql, String thangnam, String namthang)
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/param_tuyen_ghino_km"
   				+"&"+CesarCode.encode("maquay") + "=" + donvi_ql
				+"&"+CesarCode.encode("thangnam") + "=" + thangnam
				+"&"+CesarCode.encode("namthang") + "=" + namthang
   					;
   			}
	public  String layds_tb_theo_lhdt(String tungay, String denngay, String doituong_id, String tinh
									, String donvi, String ma_bc, String nguoi_th, String nguoi_bc
									,String nguoi_tt, String donvi_ql, String user_tinh_thanhpho)
	  

   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/dstb_lhdt_ajax"
				+"&"+CesarCode.encode("tungay") + "=" + tungay
   				+"&"+CesarCode.encode("denngay") + "=" + denngay
				+"&"+CesarCode.encode("doituong") + "=" + doituong_id
				+"&"+CesarCode.encode("tinh") + "=" + tinh
				+"&"+CesarCode.encode("donvi") + "=" + donvi
				
				+"&"+CesarCode.encode("ma_bc") + "=" + ma_bc
   				+"&"+CesarCode.encode("nguoi_th") + "=" + nguoi_th
				+"&"+CesarCode.encode("nguoi_bc") + "=" + nguoi_bc
				+"&"+CesarCode.encode("nguoi_tt") + "=" + nguoi_tt
				
				+"&"+CesarCode.encode("donvi_ql") + "=" + donvi_ql
				+"&"+CesarCode.encode("lbltinh_tp") + "=" + user_tinh_thanhpho
   					;
   			}
	
	public String layds_tong_nhom(String tungay, String denngay  )
   		{
   			 return "/main?"+CesarCode.encode("configFile")+"=report/goinhom/frmnhom_ajax_data"
   				+"&"+CesarCode.encode("ngaybd") + "=" + tungay
				+"&"+CesarCode.encode("ngaykt") + "=" + denngay
   					;
   			}
	
	public  String tonghop(String psSchema, String tungay, String denngay)
	  
   		{
   			return "begin ?:="+CesarCode.decode(psSchema)+"pttb_baocao.baocao_pttb('"+psSchema
		+"','','','"+tungay+"','"+denngay+"'); end;";
   			}
	
	public  String laytt_tonghop()
	  {
   			return " select ccs_admin.util.frows_to_str('select b.name from ccs_common.baocao_pttb a, ccs_admin.agent b where a.ma_tinh=b.short_name  and   a.dang_tong_hop=0 order by tt_tonghop',',')  dsTinh ,(select count(1) from ccs_common.baocao_pttb b where b.dang_tong_hop=0  ) tong_da_th from dual ";
   	  }
	public  String trangthaihd(String loaihd){
		return "/main?"+CesarCode.encode("configFile")+"=report/param_trangthaiHD"
   				+"&"+CesarCode.encode("loaihd") + "=" +loaihd;
   	}
	public  String change_buucuc_donvi(String donvi_ql){
   		return "/main?"+CesarCode.encode("configFile")+"=report/BC_PTTB/BC_PTTB_HNI/ds_buucuc_data"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   	}
	public  String change_quaythu(String donvi_ql){
   		return "/main?"+CesarCode.encode("configFile")+"=report/BC_PTTB/km_vnpt/ds_buucuc_data"
   				+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   	}
	public  String layds_km(String cvkm_id)
    {
	 return "/main?"+CesarCode.encode("configFile")+"=report/BC_PTTB/km_vnpt/hinhthuc_km_sql"
		+"&"+CesarCode.encode("cvkm_id") + "=" + cvkm_id;
	}
	public  String layds_cvkm(String loaikm)
    {
	 return "/main?"+CesarCode.encode("configFile")+"=report/BC_PTTB/km_vnpt/ds_cvkm_sql"
		+"&"+CesarCode.encode("loaikm") + "=" + loaikm;
	}

  //lan anh 3337
	public  String change_buucuc_donviql(String donvi_ql){
   		return "/main?"+CesarCode.encode("configFile")+"=ccbs_qltn/qltn_baocao/chonso_thuebaophattrien/ds_buucuc_data"
   		+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   	}	
	 public  String layds_giaodich2(String donvi_ql)
   	{
   	        return "/main?"+CesarCode.encode("configFile")+"=ccbs_qltn/qltn_baocao/chonso_thuebaophattrien/param_user"
   		+"&"+CesarCode.encode("donviql_id") + "=" + donvi_ql;
   	}		
	public  String layds_giaodich_bc(String donviql_id,String ma_bc)
   	{
   		return "/main?"+CesarCode.encode("configFile")+"=ccbs_qltn/qltn_baocao/chonso_thuebaophattrien/param_user_bc"   			
		+"&"+CesarCode.encode("donviql_id") + "=" + donviql_id
		+"&"+CesarCode.encode("ma_bc") + "=" + ma_bc;
   	}	
	
}