package neo.hssv;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

import cosaldap.Base64.*;

import java.util.*;
import javax.sql.*;

import java.net.*;
import java.io.*;

public class qtht_hssv extends NEOProcessEx {
	 public void run() {
		    System.out.println("neo.hssv.qtht_hssv was called");
		  }
	
	public NEOCommonData nguoidung_cuahang(String psNguoidung,String psStoreId, String psUserId, String psIp)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := voucher_hssv.pkg_diem.nguoidung_cuahang('"+psNguoidung+"','"+psStoreId+"','"+psUserId+"','"+psIp+"'); ";
		xs_ = xs_ + " end; ";		
		
		String rs_ = null;
		try{
			rs_ = reqValue("HSSV", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}	
	
	public NEOCommonData doi_voucher(String psMsisdn,String psVoucherType, String psUserId, String psIp)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := voucher_hssv.pkg_diem.doi_voucher_diem('"+psMsisdn+"','"+psVoucherType+"','"+psUserId+"','"+psIp+"'); ";
		xs_ = xs_ + " end; ";		
		
		String rs_ = null;
		try{
			rs_ = reqValue("HSSV", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}	
	
	public NEOCommonData update_voucher(
		String psStoreFrom,
		String psVoucherType, 
		String psVoucherCount, 
		String psTimeStart,
		String psTimeEnd,
		String psUserId, 
		String psIp)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := voucher_hssv.pkg_diem.update_voucher('"+psStoreFrom+"','"+psVoucherType+"','"+psVoucherCount+"','"+psTimeStart+"','"+psTimeEnd+"','"+psUserId+"','"+psIp+"'); ";
		xs_ = xs_ + " end; ";		
		
		String rs_ = null;
		try{
			rs_ = reqValue("HSSV", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}	
	
	public NEOCommonData dieuphoi_voucher(
		String psStoreFrom,
		String psVoucherType, 
		String psVoucherCount, 
		String psStoreTo,
		String psTimeStart,
		String psTimeEnd,
		String psUserId, 
		String psIp)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := voucher_hssv.pkg_diem.dieuphoi_voucher('"+psStoreFrom+"','"+psVoucherType+"','"+psVoucherCount+"','"+psStoreTo+"','"+psTimeStart+"','"+psTimeEnd+"','"+psUserId+"','"+psIp+"'); ";
		xs_ = xs_ + " end; ";		
		
		String rs_ = null;
		try{
			rs_ = reqValue("HSSV", xs_);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return new NEOCommonData(rs_);
	}		
}