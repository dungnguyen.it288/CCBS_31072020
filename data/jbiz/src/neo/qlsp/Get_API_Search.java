package neo.qlsp;

import neo.smartui.process.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

public class Get_API_Search extends NEOProcessEx 
 {
		public static String apiPost(String input, String method) {
				String urlHttp="http://10.156.3.61:83/mapi";
		        StringBuffer jsonString;
		        try {
		            URL url = new URL(urlHttp + method);
		            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		            conn.setDoOutput(true);
		            conn.setRequestMethod("POST");
		            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

		            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
		            writer.write(input);
		            writer.close();
		            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		            jsonString = new StringBuffer();
		            String line;
		            while ((line = br.readLine()) != null) {
		                jsonString.append(line);
		            }
		            br.close();
		            conn.disconnect();

		        } catch (Exception e) {
		            throw new RuntimeException(e.getMessage());
		        }
		        return jsonString.toString();
		}
			
	    public static String login(){
			String session="";  
			try {
				String user="webccbs";
				String pass="webccbs#2017";			
		        String input = "{\"username\":'"+user+"',\"password\":'"+pass+"'}";
		        String res = apiPost(input, "/login");	        
	            JSONParser jsonParser = new JSONParser();	     
	            JSONObject jsonObject = (JSONObject) jsonParser.parse(res);
	            session = (String) jsonObject.get("session");	        
			  } catch (Exception e) {
		            e.printStackTrace(); }
			  return session;
		}
		
		public static String getDataCCBS(String session, String msisdn){			
			String res=""; 
			try {				
		        String channel="CCBS";	
			    String input = "{\"session\":'"+session+"',\"channel\":'"+channel+"',\"msisdn\":'"+msisdn+"'}";
			    res = apiPost(input, "/services/GetDataCCBS");
			} catch (Exception e) {
			    e.printStackTrace();
			}
			return res;
		}
		
		public static String checkKM(String session, String msisdn, String product_promo){
		   String res = "";
		   try{
			String channel="CCBS";
			String input = "{\"session\":'"+session+"',\"channel\":'"+channel+"',\"msisdn\":'"+msisdn+"',\"product\":'"+product_promo+"'}";
			res = apiPost(input, "/services/CheckPromoData");
		   } catch(Exception e){
			e.printStackTrace();
		   }
		   return res;
		  }
		  
		public static String putKMbig70(String session, String msisdn, String timestart, String timeend){
		   String res = "";
		   try{
			 String channel="CCBS";
			 String input = "{\"session\":'"+session+"',\"msisdn\":'"+msisdn+"',\"time_start\":'"+timestart+"',\"time_end\":'"+timeend+"',\"direction_id\":'50'}";
			 res = apiPost(input, "/services/AddKmBig70ccbs");
		   } catch(Exception e){
			 e.printStackTrace();
		   }
		   return res;
		  }
		  
		public static String getListSubCCBS(String session, String msisdn){
		   String res = "";
		   try{
			 String input = "{\"session\":'"+session+"',\"msisdn\":'"+msisdn+"'}";
			 res = apiPost(input, "/services/getListSubCCBS");
		   } catch(Exception e){
			 e.printStackTrace();
		   }
		   return res;
		  }
		
		public static String getSubCCBS(String session, String msisdn){
		   String res = "";
		   try{
			 String input = "{\"session\":'"+session+"',\"msisdn\":'"+msisdn+"'}";
			 res = apiPost(input, "/services/getSubCCBS");
		   } catch(Exception e){
			 e.printStackTrace();
		   }
		   return res;
		  }
		  
		 public static String getLogAPI(String session, String msisdn){
		   String res = "";
		   try{
			 String input = "{\"session\":'"+session+"',\"msisdn\":'"+msisdn+"'}";
			 res = apiPost(input, "/services/getLogAPI");
		   } catch(Exception e){
			 e.printStackTrace();
		   }
		   return res;
		  }
 
}