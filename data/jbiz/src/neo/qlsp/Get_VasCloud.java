package neo.qlsp;

import neo.smartui.process.*;
import java.io.*;
import java.net.*;
import java.util.Calendar;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class Get_VasCloud extends NEOProcessEx 
 {
	
    public static String apiPost(String input) {
		//String urlHttp="http://10.186.32.62/services/VINAPORTAL_GW_PROXY?wsdl";
		String urlHttp="http://10.186.32.62/services/CHARGING_CCBS_API_PROXY?wsdl";
        StringBuffer xml_;
        try {
            URL url = new URL(urlHttp);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(3000);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/xml; charset=UTF-8");
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            writer.write(input);
            writer.close();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            xml_ = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
            	xml_.append(line);
            }
            br.close();
            conn.disconnect();

        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return xml_.toString();
    }
	
	public static int maxDay(int month, int year) {
	        Calendar cal = Calendar.getInstance();
	        cal.set(Calendar.MONTH, month - 1);
	        cal.set(Calendar.DAY_OF_MONTH, month - 1);
	        cal.set(Calendar.YEAR, year);
	        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
	        return maxDay;
	    }
		
    public static String getusehist(String msisdn, String month, String year, String userid, String ip){			
		String res="";		
		try {	
			String fromdate="01/"+month+"/"+year;
			String todate= maxDay(Integer.parseInt(month), Integer.parseInt(year))+"/"+month+"/"+year;
			
			String input = "<RQST>\n"
	                + "<name>getusehist</name>\n"
	                + "<requestid>119</requestid>\n"
	                + "<msisdn>"+msisdn+"</msisdn>\n"
	                + "<service>-1</service>\n"
	                + "<package>-1</package>\n"
	                + "<fromdate>"+fromdate+"</fromdate>\n"
	                + "<todate>"+todate+"</todate>\n"
	                + "<application>CCBS</application>\n"
	                + "<channel>API</channel>\n"
	                + "<username>"+userid+"</username>\n"
	                + "<userip>"+ip+"</userip>\n"
	                + "</RQST>";
			 //System.out.println(input);		
			 res = apiPost(input);		   
		} catch (Exception e) {
		    e.printStackTrace();
		}
		return res;
	}
}