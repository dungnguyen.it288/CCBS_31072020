package neo.qlsp;

import neo.smartui.process.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class Get_PPGW extends NEOProcessEx 
 {
		public static String apiPost(String input, String method) {
				String urlHttp="http://10.156.4.125:8000/ppgw";
		        StringBuffer jsonString;
		        try {
		            URL url = new URL(urlHttp + method);
		            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		            conn.setDoOutput(true);
		            conn.setRequestMethod("POST");
		            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

		            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
		            writer.write(input);
		            writer.close();
		            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		            jsonString = new StringBuffer();
		            String line;
		            while ((line = br.readLine()) != null) {
		                jsonString.append(line);
		            }
		            br.close();
		            conn.disconnect();

		        } catch (Exception e) {
		            throw new RuntimeException(e.getMessage());
		        }
		        return jsonString.toString();
		}
			
	    public static String CAN_Sub_OCS(String msisdn){			
			String res=""; 			
			try {						        
			    String input = "{\"msisdn\":'"+msisdn+"'}";
			    res = apiPost(input, "/services/cancel");				
			} catch (Exception e) {
			    e.printStackTrace();
			}
			return res;
		}
				 
		public static String NEW_Sub_OCS(String msisdn, String contractTemplate, String provinceCode){
		   String res = "";
		   String contractTemp="";
		   String name_="";
		   try{			 
			 if (contractTemplate.equals("E")){
				contractTemp="CONTR0000000003";		
				name_ ="EZCOM";					
			 }else{
				contractTemp="CONTR0000000002";				
				name_ ="POSTPAID";			
			 }
			 String input = "{\"msisdn\":'"+msisdn+"',\"contractTemplate\":'"+contractTemp+"',\"provinceCode\":'"+provinceCode+"',\"name\":'"+name_+"'}";			 
			 res = apiPost(input, "/services/new");
		   } catch(Exception e){
			 e.printStackTrace();
		   }
		   return res;
		}
	
		
		
}