package neo.qlsp;

import neo.smartui.process.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import neo.qlsp.*;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

public class Get_API_tracuu_5so extends NEOProcessEx 
 {
		public static String apiPost(String input, String method) {
				//String urlHttp="http://190.10.10.89:9092/mapi";
				String urlHttp="http://10.159.22.66:9092/mapi";
		        StringBuffer jsonString;
		        try {
		            URL url = new URL(urlHttp + method);
		            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setConnectTimeout(3000);
		            conn.setDoOutput(true);
		            conn.setRequestMethod("POST");
		            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

		            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
		            writer.write(input);
		            writer.close();
		            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		            jsonString = new StringBuffer();
		            String line;
		            while ((line = br.readLine()) != null) {
		                jsonString.append(line);
		            }
		            br.close();
		            conn.disconnect();

		        } catch (Exception e) {
		            throw new RuntimeException(e.getMessage());
		        }
		        return jsonString.toString();
		}
			
	    public static String login(){
			String session="";  
			try {
				String user="webccbs";
				String pass="webccbs#2017";			
		        String input = "{\"username\":'"+user+"',\"password\":'"+pass+"'}";
		        String res = apiPost(input, "/login");	        
	            JSONParser jsonParser = new JSONParser();	     
	            JSONObject jsonObject = (JSONObject) jsonParser.parse(res);
	            session = (String) jsonObject.get("session");	        
			  } catch (Exception e) {
		            e.printStackTrace(); }
			  return session;
		}
		
		public static String get5so(String msisdn, String months,String str1){			
			String res=""; 
			try {				
		        String channel="CCBS";	
			    String input = "{\"msisdn\":'"+msisdn+"',\"month\":'"+months+"',\"b_numbers\":'"+str1+"'}";
			    res = apiPost(input, "/services/tracuu_5so_detail");
			} catch (Exception e) {
			    e.printStackTrace();
			}
			return res;
		}
		
		public static String getHiscall(String msisdn, String month){
		   String res = "";
		   try{
			String channel="CCBS";
			String input = "{\"msisdn\":'"+msisdn+"',\"month\":'"+month+"'}";
			res = apiPost(input, "/services/tracuu_callhist");
		   } catch(Exception e){
			e.printStackTrace();
		   }
		   return res;
		  } 
 
}