package neo.qlsp;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import java.util.concurrent.TimeUnit;

public class Connect_SMCS extends NEOProcessEx 
 {
	
	public static NodeList getXmlNodes(String xml, String path){
		try{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(new InputSource(new StringReader(xml)));
			return document.getDocumentElement().getElementsByTagName(path);
		}catch(Exception e){
			return null;
		}
    }
	
	
	//1. Ham ket noi API QLSP
	public static String postSOAP(String Request, String method){
		String Host="10.149.251.220";
		String Port="8000";
		String Uri="/VNPServices";
		StringBuffer buf = new StringBuffer();	    
	    try {
	    	URL url = new URL("http://"+Host+":"+Port+"/"+Uri);
	        URLConnection urlc = url.openConnection();
	        urlc.setConnectTimeout(8000);
	        urlc.setRequestProperty("Content-Type","text/xml");
	        urlc.setRequestProperty("SOAPAction", Uri+"/"+method);	        
	        urlc.setDoOutput(true);
	        urlc.setDoInput(true);
	        OutputStreamWriter writer = new OutputStreamWriter( urlc.getOutputStream() );
	        writer.write( Request );
	        writer.flush();
	        BufferedReader in = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
	        String res_line;
	        while ((res_line = in.readLine()) != null)
	            buf.append(res_line);
	        in.close();
	        return buf.toString();
	    }catch (Exception e) {
	        e.printStackTrace();
	        return e.getMessage();
	    }
	  }
	//6. Ham doc session
	public String readSession() throws IOException{
		System.out.println("Goi ham readSession");	 		
		String session = "";
		File file = new File (this.getSysConst("ConfigCompiledDir")+"pttb/prepaid/session.neo");
		
		if (!file.exists()){
			System.out.println("File khong ton tai");	
			session = login();
			try {
				FileUtils.writeStringToFile(file,session);
			} catch (IOException e) {
				session = e.getMessage();
			}
		}
		else if (file.exists()&&file.lastModified()<System.currentTimeMillis()-1800000){
			System.out.println("File ton tai va lon hon 30phut");	
			boolean del = true;
			if (file.exists()){
				del=file.delete();
			}
			session = login();
			if (del){
				try {
					FileUtils.writeStringToFile(file,session);
				} catch (IOException e) {
					session = e.getMessage();
				}
			}
		}else{
			System.out.println("ELSE File ton tai va lon hon 30phut");
			System.out.println("Ten file ="+file.getName());
			FileReader fr =null;
			BufferedReader br = null;
			try {
				fr = new FileReader(this.getSysConst("ConfigCompiledDir")+"pttb/prepaid/session.neo");
				br = new BufferedReader(fr);
				
				String sCurrentLine;

				while ((sCurrentLine = br.readLine()) != null) {
					session =sCurrentLine;
				}				
				//session = FileUtils.readFileToString(file);
			} catch (IOException ex) {
				session = ex.getMessage();
				ex.printStackTrace();
				System.out.println("Co loi lay session ="+ex.getMessage());	
				
			} finally {

			try {

				if (br != null)
					br.close();

				if (fr != null)
					fr.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}

		}
			
			
		}
		System.out.println("Lay session ="+session);
		return session;
		 
	}
	
	//2. Ham login
	public String login(){
		 try{
			 String reques_="<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:VNPServices\"><soapenv:Header/><soapenv:Body><urn:login soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"><login xsi:type=\"xsd:string\">ccbs</login><password xsi:type=\"xsd:string\">Ccbs@#123</password></urn:login></soapenv:Body></soapenv:Envelope>";
  		 	 String response_= postSOAP(reques_,"login");
			 System.out.println("response_ login: " + response_);
			 return response_.substring(response_.indexOf("<sessionID xsi:type=\"xsd:string\">") + ("<sessionID xsi:type=\"xsd:string\">").length(),response_.indexOf("</sessionID>"));			 				
		 }catch(Exception e){
			   return e.getMessage(); 
		 }
	}
	
	//3. Ham lay tinh
	public String getProvince(){
		try{
			String sessionId = readSession();
			System.out.println("sessionId getProvince: " + sessionId);
			String reques_= "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:VNPServices\">";
				   reques_= reques_ + "<soapenv:Header/><soapenv:Body>";
				   reques_= reques_ + "<urn:getProvince soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
				   reques_= reques_ + "<sessionID xsi:type=\"xsd:string\">"+sessionId+"</sessionID>";
				   reques_= reques_ + "</urn:getProvince></soapenv:Body></soapenv:Envelope>"; 	      
			String response_ =postSOAP(reques_,"getProvince");
			System.out.println("response_ getProvince: " + response_);
			String result = response_.substring(response_.indexOf("<responseStatus xsi:type=\"xsd:string\">")+("<responseStatus xsi:type=\"xsd:string\">").length(),response_.indexOf("</responseStatus>"));
			if (result.equals("1")){
				NodeList node = getXmlNodes(response_,"item");				 
				StringBuffer ret = new StringBuffer();				 
				ret.append("<select name=\"--cboTinh\" id=\"cboTinh\" style=\"width:98%\" class=textField  default=\"{F01matinh}\" onchange=\"loadQuan();capnhat_diachi('quan','phuong','cboTinh','diachi');\" onkeydown=\"ChangeFromTabToEnter()\" cache-name=\"TINH_THANHPHO_\"  cache-mode=\"SYSTEM\">");
				String selected ="";
				for (int i=0;i<node.getLength();i++){
					 
					String[] it = node.item(i).getTextContent().split("\\|");
					if (i==0) {
						selected ="selected";
					} else {
						selected ="";
					}
					if (!it[0].equals("2")&&!it[0].equals("8232328")){
					ret.append("<OPTION "+selected+" value='"+it[0]+"'>"+it[2]+"</OPTION>");
					}					 
				}
				ret.append("</select>");
				getEnvInfo().setUserVar("dsTinh",ret.toString()); 
				return "/main?" + CesarCode.encode("configFile")+"=pttb/prepaid/common/ajax_tinh";
				
			} else {				
				return "/main?" + CesarCode.encode("configFile")+"=pttb/prepaid/common/ajax_tinh_";
			} 
		}catch(Exception e){
			System.out.println("Co loi getProvince="+e.getMessage());		
			return e.getMessage();
		}		
	}
	
	//4. Ham lay thong tin Quan/huyen
	public String getState(String pstinhid){
		try{		       
			String sessionId = readSession(); 
			String reques_= "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:VNPServices\">";
				   reques_= reques_ + "<soapenv:Header/><soapenv:Body>";
				   reques_= reques_ + "<urn:getState soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
				   reques_= reques_ + "<sessionID xsi:type=\"xsd:string\">"+sessionId+"</sessionID>";
				   reques_= reques_ + "<p_area_id xsi:type=\"xsd:string\">"+pstinhid+"</p_area_id>";
				   reques_= reques_ + "</urn:getState></soapenv:Body></soapenv:Envelope>";			 
	        String response_ =postSOAP(reques_,"getState");
			System.out.println("response_ getState: " + response_);
			String result = response_.substring(response_.indexOf("<responseStatus xsi:type=\"xsd:string\">")+("<responseStatus xsi:type=\"xsd:string\">").length(),response_.indexOf("</responseStatus>"));
			if (result.equals("1")){
				NodeList node = getXmlNodes(response_,"item");
				StringBuffer ret = new StringBuffer();
				ret.append("<select name=\"--QUAN_ID\" id=\"quan\" style=\"width:98%\" class=textField default= \"{p0quan_id}\" onchange=\"loadPhuong();capnhat_diachi('quan','phuong','cboTinh','diachi');\" onkeydown=\"ChangeFromTabToEnter()\" CACHE-NAME=\"LAPHD_QUAN_\" cache-mode=\"AGENT\" >");
				String selected ="";
				for (int i=0;i<node.getLength();i++){
					String[] it = node.item(i).getTextContent().split("\\|");
					if (i==0) {
						selected ="selected";
					} else {
						selected ="";
					}
					ret.append("<OPTION "+selected+" value='"+it[0]+"'>"+it[2]+"</OPTION>");
				}
				ret.append("</select>");
				getEnvInfo().setUserVar("dsQuan",ret.toString());
				System.out.println("response_ getState: " + response_);
				return "/main?" + CesarCode.encode("configFile")+"=pttb/prepaid/common/ajax_quan";
				
			} else {
				return "/main?" + CesarCode.encode("configFile")+"=pttb/prepaid/common/ajax_quan_";
			} 
			 
		}catch(Exception e){
			return e.getMessage();
		}		
	}
	
	//5. Ham lay thong tin phuong/xa
	public String getCounty(String pshuyenid) 
	{
		try{
			String sessionId = readSession();  			
			String reques_= "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:VNPServices\">";
				   reques_= reques_ + "<soapenv:Header/><soapenv:Body>";
				   reques_= reques_ + "<urn:getCounty soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
				   reques_= reques_ + "<sessionID xsi:type=\"xsd:string\">"+sessionId+"</sessionID>";
				   reques_= reques_ + "<p_state_id xsi:type=\"xsd:string\">"+pshuyenid+"</p_state_id>";
				   reques_= reques_ + "</urn:getCounty></soapenv:Body></soapenv:Envelope>";			        
			String response_ = postSOAP(reques_,"getCounty");		
			System.out.println("response_ getState: " + response_);
			String result = response_.substring(response_.indexOf("<responseStatus xsi:type=\"xsd:string\">")+("<responseStatus xsi:type=\"xsd:string\">").length(),response_.indexOf("</responseStatus>"));
			if (result.equals("1")){
				NodeList node = getXmlNodes(response_,"item");
				StringBuffer ret = new StringBuffer();
				ret.append("<SELECT name=\"--phuong\" id=\"phuong\"  onchange=\"capnhat_diachi('quan','phuong','cboTinh','diachi');\" onkeydown=\"ChangeFromTabToEnter()\" style='width:98%' class=textField '>");
				String selected ="";
				for (int i=0;i<node.getLength();i++){
					String[] it = node.item(i).getTextContent().split("\\|");
					if (i==0) {
						selected ="selected";
					} else {
						selected ="";
					}
					ret.append("<OPTION "+selected+" value='"+it[0]+"'>"+it[2]+"</OPTION>");
				}
				ret.append("</select>");
				getEnvInfo().setUserVar("dsPhuong",ret.toString());
				System.out.println("response_ getState: " + response_);
				return "/main?" + CesarCode.encode("configFile")+"=pttb/prepaid/common/ajax_phuong";
				
			} else {
				return "/main?" + CesarCode.encode("configFile")+"=pttb/prepaid/common/ajax_phuong_";
			} 
		}catch(Exception e){
			return e.getMessage();
		}
	}
	
	
	
	 
 
	
}