package neo.qlsp;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

import java.util.*;
import javax.sql.*;
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Connect extends NEOProcessEx 
 {
	//1. Ham ket noi API QLSP
	public static String postSOAP(String Request, String method){
		String Host="smcs.vnpt.vn";
		String Port="8008";
		String Uri="/NumberStore";
		String pass="";
		StringBuffer buf = new StringBuffer();	    
	    try {
	    	URL url = new URL("http://"+Host+":"+Port+"/"+Uri);
	        URLConnection urlc = url.openConnection();
	        urlc.setConnectTimeout(8000);
	        char[] buffer = new char[1024*10];
	        urlc.setRequestProperty("Content-Type","text/xml");
	        urlc.setRequestProperty("SOAPAction", Uri+"/"+method);	        
	        urlc.setDoOutput(true);
	        urlc.setDoInput(true);
	        OutputStreamWriter writer = new OutputStreamWriter( urlc.getOutputStream() );
	        writer.write( Request );
	        writer.flush();
	        BufferedReader in = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
	        String res_line;
	        while ((res_line = in.readLine()) != null)
	            buf.append(res_line);
	        in.close();
	        return buf.toString();
	    }catch (Exception e) {
	        e.printStackTrace();
	        return e.getMessage();
	    }
	  }
	
	//2. Ham login
	public static String login(){
		 try{
			 String reques_="<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:NumberStore\"><soapenv:Header/><soapenv:Body><urn:login soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"><login xsi:type=\"xsd:string\">ccbs</login><password xsi:type=\"xsd:string\">vnpnum@1235</password></urn:login></soapenv:Body></soapenv:Envelope>";
  		 	 String response_= postSOAP(reques_,"login");
			 return response_.substring(response_.indexOf("<sessionID xsi:type=\"xsd:string\">") + ("<sessionID xsi:type=\"xsd:string\">").length(),response_.indexOf("</sessionID>"));			 				
		 }catch(Exception e){
			   return e.getMessage(); 
		 }
	}
	
	//3. Ham checkIsdn
	public static String checkIsdnLite(String sessionId, String msisdn){
		try{
			String processId = "1";
			String reques_= "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:NumberStore\"><soapenv:Header/><soapenv:Body><urn:checkIsdnLite soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
			reques_= reques_ + "<sessionID xsi:type=\"xsd:string\">"+sessionId+"</sessionID>";
			reques_= reques_ + "<p_ProcessID xsi:type=\"xsd:string\">"+processId+"</p_ProcessID>";
			reques_= reques_ + "<p_SystemID xsi:type=\"xsd:string\">1</p_SystemID>";
			reques_= reques_ + "<p_isdn xsi:type=\"xsd:string\">"+msisdn+"</p_isdn>";
	        reques_= reques_ + "</urn:checkIsdnLite></soapenv:Body></soapenv:Envelope>";
	      
			String response_ =postSOAP(reques_,"checkIsdnLite");
			String return_ = response_.substring(response_.indexOf("<result xsi:type=\"xsd:string\">") + ("<result xsi:type=\"xsd:string\">").length(),response_.indexOf("</result>"));
			return return_;
		}catch(Exception e){
			return e.getMessage();
		}		
	}
	
	//4. Ham lay thong tin Sim
	public static String getSimInfo(String sessionId, String psmsin){
		try{		       
			String processId="1";
			String reques_= "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:NumberStore\"><soapenv:Header/><soapenv:Body><urn:getSimInfo soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
			reques_= reques_ + "<sessionID xsi:type=\"xsd:string\">"+sessionId+"</sessionID>";
			reques_= reques_ + "<p_ProcessID xsi:type=\"xsd:string\">"+processId+"</p_ProcessID>";
			reques_= reques_ + "<p_SystemID xsi:type=\"xsd:string\">1</p_SystemID>";
			reques_= reques_ + "<p_imsi xsi:type=\"xsd:string\">"+psmsin+"</p_imsi>";
	        reques_= reques_ + "</urn:getSimInfo>></soapenv:Body></soapenv:Envelope>";
	        String response_ =postSOAP(reques_,"getSimInfo");
			String return_ = response_.substring(response_.indexOf("<result xsi:type=\"xsd:string\">") + ("<result xsi:type=\"xsd:string\">").length(),response_.indexOf("</result>"));
			return return_;
		}catch(Exception e){
			return e.getMessage();
		}		
	}
	
	//5. Ham tim kiem thue bao trong kho
	public String searchIsdnLite(String sessionId, String msisdn, String stock_id, String numbertype_id, String prefix, String page_num, String page_rec, String status) 
	{
		try{
			String processId="1";				
			String re_="<result soapenc:arrayType=\"tns1:ArrayOf_xsd_string[10]\" xsi:type=\"soapenc:Array\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">";
			String reques_= "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:NumberStore\"><soapenv:Header/><soapenv:Body><urn:searchIsdnLite soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
			reques_= reques_ + "<sessionID xsi:type=\"xsd:string\">"+sessionId+"</sessionID>";
			reques_= reques_ + "<p_ProcessID xsi:type=\"xsd:string\">"+processId+"</p_ProcessID>";
			reques_= reques_ + "<p_SystemID xsi:type=\"xsd:string\">1</p_SystemID>";
			reques_= reques_ + "<p_isdn xsi:type=\"xsd:string\">"+msisdn+"</p_isdn>";
			reques_= reques_ + "<p_stock_id xsi:type=\"xsd:string\">"+stock_id+"</p_stock_id>";
			reques_= reques_ + "<p_numbertype_id xsi:type=\"xsd:string\">"+numbertype_id+"</p_numbertype_id>";
			reques_= reques_ + "<p_prefix xsi:type=\"xsd:string\">"+prefix+"</p_prefix>";
			reques_= reques_ + "<p_page_num xsi:type=\"xsd:string\">"+page_num+"</p_page_num>";
			reques_= reques_ + "<p_page_rec xsi:type=\"xsd:string\">"+page_rec+"</p_page_rec>";
			reques_= reques_ + "<p_status xsi:type=\"xsd:string\">"+status+"</p_status>";			
	        reques_= reques_ + "</urn:searchIsdnLite></soapenv:Body></soapenv:Envelope>";	        
			String response_ = postSOAP(reques_,"searchIsdnLite");		
			String return_ = response_;//response_.substring(response_.indexOf(re_) + (re_).length(),response_.indexOf("</result>")); 
			return return_;
		}catch(Exception e){
			return e.getMessage();
		}
	}
	
	//6. Ham cap nhat thong tin DOI SIM
	public static String changeSIM(String sessionId, String msisdn, String old_imsi, String new_imsi, String username){
		try{
			String processId="1";
			String description="DOI SIM";
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = new Date();
			String reques_= "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:NumberStore\"><soapenv:Header/><soapenv:Body><urn:changeSIM soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
			reques_= reques_ + "<sessionID xsi:type=\"xsd:string\">"+sessionId+"</sessionID>";
			reques_= reques_ + "<p_ProcessID xsi:type=\"xsd:string\">"+processId+"</p_ProcessID>";
			reques_= reques_ + "<p_SystemID xsi:type=\"xsd:string\">1</p_SystemID>";
			reques_= reques_ + "<p_isdn xsi:type=\"xsd:string\">"+msisdn+"</p_isdn>";
			reques_= reques_ + "<p_old_imsi xsi:type=\"xsd:string\">"+old_imsi+"</p_old_imsi>";
			reques_= reques_ + "<p_new_imsi xsi:type=\"xsd:string\">"+new_imsi+"</p_new_imsi>";
			reques_= reques_ + "<p_username xsi:type=\"xsd:string\">"+username+"</p_username>";
			reques_= reques_ + "<p_create_date xsi:type=\"xsd:string\">"+dateFormat.format(date)+"</p_create_date>";
			reques_= reques_ + "<p_description xsi:type=\"xsd:string\">"+description+"</p_description>";	      
	        reques_= reques_ + "</urn:changeSIM></soapenv:Body></soapenv:Envelope>";
	        
			String response_ =postSOAP(reques_,"changeSIM");
			String kq_ = response_.substring(response_.indexOf("<ErrCode xsi:type=\"xsd:string\">") + ("<ErrCode xsi:type=\"xsd:string\">").length(),response_.indexOf("</ErrCode>"));
			String return_="";
			if (kq_.equals("0")){
				return_="OK";
			}else{
				return_ = response_.substring(response_.indexOf("<ErrContent xsi:type=\"xsd:string\">") + ("<ErrContent xsi:type=\"xsd:string\">").length(),response_.indexOf("</ErrContent>"));
			}		
			return return_;
		}catch(Exception e){
			return e.getMessage();
		}		
	}
	
	//7. Ham khoi tao so
	public static String createSubs(String sessionId, String msisdn, String imsi, String stock_id, String sub_type, String province, String username){
		try{
			String processId="1";
			String description="KHOI TAO LOAI:"+ sub_type;
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = new Date();			     	           
			String reques_= "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:NumberStore\"><soapenv:Header/><soapenv:Body><urn:createSubs soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
			reques_= reques_ + "<sessionID xsi:type=\"xsd:string\">"+sessionId+"</sessionID>";
			reques_= reques_ + "<p_ProcessID xsi:type=\"xsd:string\">"+processId+"</p_ProcessID>";
			reques_= reques_ + "<p_SystemID xsi:type=\"xsd:string\">1</p_SystemID>";
			reques_= reques_ + "<p_isdn xsi:type=\"xsd:string\">"+msisdn+"</p_isdn>";			
			reques_= reques_ + "<p_imsi xsi:type=\"xsd:string\">"+imsi+"</p_imsi>";
			reques_= reques_ + "<p_stock_id xsi:type=\"xsd:string\">"+stock_id+"</p_stock_id>";			
			reques_= reques_ + "<p_sub_type xsi:type=\"xsd:string\">"+sub_type+"</p_sub_type>";
			reques_= reques_ + "<p_create_date xsi:type=\"xsd:string\">"+dateFormat.format(date)+"</p_create_date>";
			reques_= reques_ + "<p_username xsi:type=\"xsd:string\">"+username+"</p_username>";
			reques_= reques_ + "<p_province xsi:type=\"xsd:string\">"+province+"</p_province>";
			reques_= reques_ + "<p_description xsi:type=\"xsd:string\">"+description+"</p_description>";
	        reques_= reques_ + "</urn:createSubs></soapenv:Body></soapenv:Envelope>";	        
			String response_ =postSOAP(reques_,"createSubs");
			String return_ = response_.substring(response_.indexOf("<ErrCode xsi:type=\"xsd:string\">") + ("<ErrCode xsi:type=\"xsd:string\">").length(),response_.indexOf("</ErrCode>"));
			return return_;
		}catch(Exception e){
			return e.getMessage();
		}		
	}
	
	//8. Ham khoi tao dai so
	public static String createSubsCorp(String sessionId, String msisdn, String imsi, String stock_id, String sub_type, String corp_id, String province, String username){
		try{
			String processId="1";
			String description="KHOI TAO DAI SO LOAI:"+ sub_type;
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = new Date();		
	
			String reques_= "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:NumberStore\"><soapenv:Header/><soapenv:Body><urn:createSubsCorp soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
			reques_= reques_ + "<sessionID xsi:type=\"xsd:string\">"+sessionId+"</sessionID>";
			reques_= reques_ + "<p_ProcessID xsi:type=\"xsd:string\">"+processId+"</p_ProcessID>";
			reques_= reques_ + "<p_SystemID xsi:type=\"xsd:string\">1</p_SystemID>";
			reques_= reques_ + "<p_isdn xsi:type=\"xsd:string\">"+msisdn+"</p_isdn>";			
			reques_= reques_ + "<p_imsi xsi:type=\"xsd:string\">"+imsi+"</p_imsi>";
			reques_= reques_ + "<p_stock_id xsi:type=\"xsd:string\">"+stock_id+"</p_stock_id>";			
			reques_= reques_ + "<p_sub_type xsi:type=\"xsd:string\">"+sub_type+"</p_sub_type>";
			reques_= reques_ + "<p_corp_id xsi:type=\"xsd:string\">"+corp_id+"</p_corp_id>";
			reques_= reques_ + "<p_create_date xsi:type=\"xsd:string\">"+dateFormat.format(date)+"</p_create_date>";
			reques_= reques_ + "<p_username xsi:type=\"xsd:string\">"+username+"</p_username>";
			reques_= reques_ + "<p_province xsi:type=\"xsd:string\">"+province+"</p_province>";
			reques_= reques_ + "<p_description xsi:type=\"xsd:string\">"+description+"</p_description>";			
	        reques_= reques_ + "</urn:createSubsCorp></soapenv:Body></soapenv:Envelope>";	        
			String response_ =postSOAP(reques_,"createSubsCorp");
			String return_ = response_.substring(response_.indexOf("<ErrCode xsi:type=\"xsd:string\">") + ("<ErrCode xsi:type=\"xsd:string\">").length(),response_.indexOf("</ErrCode>"));
			return return_;
		}catch(Exception e){
			return e.getMessage();
		}		
	}
	
	//9. Ham huy thue bao doi thu hoi so
	public static String deactiveSubs(String sessionId, String msisdn, String imsi, String description, String username){
		try{
			String processId="1";			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = new Date();		
	
			String reques_= "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:NumberStore\"><soapenv:Header/><soapenv:Body><urn:deactiveSubs soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
			reques_= reques_ + "<sessionID xsi:type=\"xsd:string\">"+sessionId+"</sessionID>";
			reques_= reques_ + "<p_ProcessID xsi:type=\"xsd:string\">"+processId+"</p_ProcessID>";
			reques_= reques_ + "<p_SystemID xsi:type=\"xsd:string\">1</p_SystemID>";			
			reques_= reques_ + "<p_isdn xsi:type=\"xsd:string\">"+msisdn+"</p_isdn>";		
			reques_= reques_ + "<p_imsi xsi:type=\"xsd:string\">"+imsi+"</p_imsi>";
			reques_= reques_ + "<p_username xsi:type=\"xsd:string\">"+username+"</p_username>";			
			reques_= reques_ + "<p_create_date xsi:type=\"xsd:string\">"+dateFormat.format(date)+"</p_create_date>";
			reques_= reques_ + "<p_description xsi:type=\"xsd:string\">"+description+"</p_description>";						
	        reques_= reques_ + "</urn:deactiveSubs></soapenv:Body></soapenv:Envelope>";	        
			String response_ =postSOAP(reques_,"deactiveSubs");
			String return_ = response_.substring(response_.indexOf("<ErrCode xsi:type=\"xsd:string\">") + ("<ErrCode xsi:type=\"xsd:string\">").length(),response_.indexOf("</ErrCode>"));
			return return_;
		}catch(Exception e){
			return e.getMessage();
		}		
	}
	
	//10. Ham cap nhat trang thai kho so
	public static String updateIsdn(String sessionId, String msisdn, String stock_id, String status, String description){
		try{
			String processId="1";		
			String reques_= "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:NumberStore\"><soapenv:Header/><soapenv:Body><urn:updateIsdn soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
			reques_= reques_ + "<sessionID xsi:type=\"xsd:string\">"+sessionId+"</sessionID>";
			reques_= reques_ + "<p_ProcessID xsi:type=\"xsd:string\">"+processId+"</p_ProcessID>";
			reques_= reques_ + "<p_SystemID xsi:type=\"xsd:string\">1</p_SystemID>";				
			reques_= reques_ + "<p_isdn xsi:type=\"xsd:string\">"+msisdn+"</p_isdn>";		
			reques_= reques_ + "<p_stock_id xsi:type=\"xsd:string\">"+stock_id+"</p_stock_id>";
			reques_= reques_ + "<p_status xsi:type=\"xsd:string\">"+status+"</p_status>";			
			reques_= reques_ + "<p_description xsi:type=\"xsd:string\">"+description+"</p_description>";
			reques_= reques_ + "</urn:updateIsdn></soapenv:Body></soapenv:Envelope>";	        
			String response_ =postSOAP(reques_,"updateIsdn");
			String kq_ = response_.substring(response_.indexOf("<ErrCode xsi:type=\"xsd:string\">") + ("<ErrCode xsi:type=\"xsd:string\">").length(),response_.indexOf("</ErrCode>"));
			if (kq_.equals("0")){
				return "0";
			} else{
				String return_ = response_.substring(response_.indexOf("<ErrContent xsi:type=\"xsd:string\">") + ("<ErrContent xsi:type=\"xsd:string\">").length(),response_.indexOf("</ErrContent>"));
				return return_;
			}				
		}catch(Exception e){
			return e.getMessage();
		}		
	}
	
	//100. Ham doc ma xac thuc
	public String readSession() throws IOException{
		String fileName = "D:\\_app\\vnp-ccbs\\data\\jbiz\\src\\neo\\qlsp\\session.txt";
		BufferedReader br = new BufferedReader(new FileReader(fileName));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }			
	        return sb.toString();
	    }finally {
	        br.close();
	    }			
	}
	//3.1 Ham checkIsdnFull
	public static String checkIsdn(String sessionId, String msisdn){
		try{
			String processId = "1";
			String reques_= "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:NumberStore\"><soapenv:Header/><soapenv:Body><urn:checkIsdn soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
			reques_= reques_ + "<sessionID xsi:type=\"xsd:string\">"+sessionId+"</sessionID>";
			reques_= reques_ + "<p_ProcessID xsi:type=\"xsd:string\">"+processId+"</p_ProcessID>";
			reques_= reques_ + "<p_SystemID xsi:type=\"xsd:string\">1</p_SystemID>";
			reques_= reques_ + "<p_isdn xsi:type=\"xsd:string\">"+msisdn+"</p_isdn>";
			reques_= reques_ + "</urn:checkIsdn></soapenv:Body></soapenv:Envelope>";			
			String response_ =postSOAP(reques_,"checkIsdn");			
			String return_ = response_.substring(response_.indexOf("<result xsi:type=\"xsd:string\">") + ("<result xsi:type=\"xsd:string\">").length(),response_.indexOf("</result>"));
			
			return return_;
		}catch(Exception e){
			return e.getMessage();
		}		
	}
	//5. Ham tim kiem thue bao trong kho
	public String getStock(String sessionId, String province) 
	{
		try{
			String processId="1";				
			String re_="<result soapenc:arrayType=\"tns1:ArrayOf_xsd_string[10]\" xsi:type=\"soapenc:Array\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">";
			String reques_= "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:NumberStore\"><soapenv:Header/><soapenv:Body><urn:getStock soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
			reques_= reques_ + "<sessionID xsi:type=\"xsd:string\">"+sessionId+"</sessionID>";
			reques_= reques_ + "<p_ProcessID xsi:type=\"xsd:string\">"+processId+"</p_ProcessID>";
			reques_= reques_ + "<p_SystemID xsi:type=\"xsd:string\">1</p_SystemID>";
			reques_= reques_ + "<p_ProvinceCode xsi:type=\"xsd:string\">"+province+"</p_ProvinceCode>";	
	        reques_= reques_ + "</urn:getStock></soapenv:Body></soapenv:Envelope>";	        
			String response_ = postSOAP(reques_,"getStock");	
			String return_ = response_.substring(response_.indexOf(re_) + (re_).length(),response_.indexOf("</result>"));
			String re_1="<item xsi:type=\"xsd:string\">";
			String return_1= return_.substring(return_.indexOf(re_1)+ (re_1).length(),return_.indexOf("</item>"));		
			return return_1;
		}catch(Exception e){
			return e.getMessage();
		}
	}
	//lay thong tin kho so
	public String getStockInfo(String sessionId, String stockid) 
	{
		try{
			String processId="1";
				
			String reques_= "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:NumberStore\"><soapenv:Header/><soapenv:Body><urn:getStockInfo soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
			reques_= reques_ + "<sessionID xsi:type=\"xsd:string\">"+sessionId+"</sessionID>";
			reques_= reques_ + "<p_ProcessID xsi:type=\"xsd:string\">"+processId+"</p_ProcessID>";
			reques_= reques_ + "<p_SystemID xsi:type=\"xsd:string\">1</p_SystemID>";			
			reques_= reques_ + "<p_stockid xsi:type=\"xsd:string\">"+stockid+"</p_stockid>";				
	        reques_= reques_ + "</urn:getStockInfo></soapenv:Body></soapenv:Envelope>";	  
			//System.out.println("reques_: " + reques_);			
			String response_ = postSOAP(reques_,"getStockInfo");
			//System.out.println("response_: " + response_);	
			String return_ = response_.substring(response_.indexOf("<result xsi:type=\"xsd:string\">") + ("<result xsi:type=\"xsd:string\">").length(),response_.indexOf("</result>"));	
			
			return return_;
		}catch(Exception e){
			return e.getMessage();
		}
	}		
}