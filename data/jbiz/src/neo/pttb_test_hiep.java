package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class pttb_test_hiep
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb_test_hiep was called");
  }

  public String getUrlKieuLD(String value) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/hdld/hdld_ajax_kieuld"
        +"&"+CesarCode.encode("dichvuvt_id")+"="+value
        ;
  }
  public String layds_port_bras(String ps_trangthai
			,String ps_vpi_nni
			,String ps_vci_nni
			,String ps_card_adsl
			,String ps_port_adsl
			,String ps_port_bras
			,String ps_hethong_adsl_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/dhanh/visa/ds_port_bras"
			+"&"+CesarCode.encode("trangthai")+"="+ps_trangthai
			+"&"+CesarCode.encode("vpi_nni")+"="+ps_vpi_nni
			+"&"+CesarCode.encode("vci_nni")+"="+ps_vci_nni
			+"&"+CesarCode.encode("card_adsl")+"="+ps_card_adsl
			+"&"+CesarCode.encode("port_adsl")+"="+ps_port_adsl
			+"&"+CesarCode.encode("port_bras")+"="+ps_port_bras
			+"&"+CesarCode.encode("hethong_adsl_id")+"="+ps_hethong_adsl_id
			;
  }

  public String layds_tramvt(String ps_donviql_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/dhanh/visa/ds_tramvt_ajax"
			+"&"+CesarCode.encode("donviql_id")+"="+ps_donviql_id
			;
  }

  public String layds_dslam(String ps_tramvt_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/dhanh/visa/ds_dslam_ajax"
			+"&"+CesarCode.encode("tramvt_id")+"="+ps_tramvt_id
			;
  }

public String laytt_port_bras(String schema,
								String userid,
								String ps_port_bras,
								String ps_vpi_nni,
								String ps_vci_nni,
								String ps_card_adsl,
								String ps_port_adsl) {
   return "begin ?:=" + CesarCode.decode(schema) + "pttb_test_hiep.laytt_port_bras('" + CesarCode.decode(userid) + "','" 
																+ps_port_bras +"',"
																+ps_vpi_nni +","
																+ps_vci_nni +","
																+ps_card_adsl +","
																+ps_port_adsl+"); end;";
  }

public String lay_port_bras_cuoi(String schema,String ps_dslam_vpi) {
   return "begin ?:=" + CesarCode.decode(schema) + "pttb_test_hiep.lay_port_bras_cuoi(" +ps_dslam_vpi + "); end;";
  }

//sinh_port_bras
  public String sinh_port_bras(String ps_hethong_adsl_id,String ps_dslam_vpi,String ps_socard) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/dhanh/visa/ds_port_bras_moi"
			+"&"+CesarCode.encode("hethong_adsl_id")+"="+ps_hethong_adsl_id
			+"&"+CesarCode.encode("dslam_vpi")+"="+ps_dslam_vpi
			+"&"+CesarCode.encode("socard")+"="+ps_socard
			;
 }
 
public String getUrlDanhSachDVLD(String schema, String userInput) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_test_hiep.layds_dichvuld(" 
				+userInput+"); end;";
  }

public String xoa_port_bras_moi(String schema,
								String ps_userid,
								String pi_num_dslam_id,
								String pi_num_vpi_nni,
								String pi_num_vci_nni) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_test_hiep.xoa_port_bras_moi('"
								+ps_userid+"',"
								+pi_num_dslam_id+","
								+pi_num_vpi_nni+","
								+pi_num_vci_nni
								+"); end;";
  }

public String capnhat_account_visa(String schema,
				String ps_userid,
				String piSLOT_BRAS,
				String psPORT_BRAS,
				String piVPI_NNI,
				String piVCI_NNI,
				String piCARD_ADSL,
				String piPORT_ADSL,
				String pdNGAY_KHAI,
				String psMA_HD,
				String psTHUEBAO_ID,
				String psTEN_TB,
				String psACCOUNT,
				String psPASSWORD,
				String psDIACHI,
				String psTEL_TN,
				String psHINHTHUC,
				String psLOAIGOI,
				String piHETHONG_ADSL_ID
			) {
    return "begin ?:="+CesarCode.decode(schema)+
			"pttb_test_hiep.capnhat_account_visa('"
				+ps_userid+"',"
				+piSLOT_BRAS+",'"
				+psPORT_BRAS+"',"
				+piVPI_NNI+","
				+piVCI_NNI+","
				+piCARD_ADSL+","
				+piPORT_ADSL+","
				+pdNGAY_KHAI+",'"
				+psMA_HD+"','"
				+psTHUEBAO_ID+"','"
				+psTEN_TB+"','"
				+psACCOUNT+"','"
				+psPASSWORD+"','"
				+psDIACHI+"','"
				+psTEL_TN+"','"
				+psHINHTHUC+"','"
				+psLOAIGOI+"',"
				+piHETHONG_ADSL_ID
			+"); end;";
  }

  //Start hiepdh
  public String loaibo_dichvu_dadangky(
					 String schema
					,String psMA_TB                         
					,String psDsDichvu                       
  ) {
  String s=    "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_HIEP.loaibo_dichvu_dadangky("
				+"'"+psMA_TB+"',"                         
				+"'"+psDsDichvu+"'"
		+");"
        +"end;"
        ;
    return s;

  }
  //end hiepdh
public String capnhatTBLD(String schema
						,String psNGUOI_CN                      
						,String piDONVIQL_ID                    
						,String psSO_NHA                        
						,String piTHUTU_IN                      
						,String psSOMAY_TN                      
						,String psMA_TB                         
						,String psMAY_CN                        
						,String psMA_BC                         
						,String piDONVITC_ID                    
						,String piTRACUU_DB                     
						,String psGIAPDANH                      
						,String piCUOCNH_ID                     
						,String piKHUYENMAI                     
						,String piDANGKY_DB                     
						,String psTEN_DB                        
						,String piTRAMVT_ID                     
						,String piVISA_ID                       
						,String piTHUEBAO_ID                    
						,String psMA_HD                         
						,String psTEN_TB                        
						,String psDIACHI                        
						,String piLOAITB_ID                     
						,String psDOITUONG_ID                   
						,String piDANGKY_DV                       
						,String piTRAGOP                          
						,String piCUOC_LD                       
						,String piCUOC_DV                       
						,String piTRANGTHAI                     
						,String piVAT_LD                        
						,String piVAT_DV                        
						,String piLOAIDB_ID                     
						,String piINCHITIET                     
						,String piCHUNGLOAI_ID                  
						,String piTIENMAY                       
						,String piVAT_MAY                       
						,String psGHICHU                        
						,String piQUAN_ID                       
						,String piPHUONG_ID                     
						,String piPHO_ID                        
						,String piKIEULD_ID   
						,String psDANHSACH_DV
						,String psDANHSACH_DVNOIDUNG
						//,String piDICHVUVT_ID                        
						,String piLOAIKH_ID   
						,String psMA_TB_CU   
						
						,String piLOAIDAY_ID    
						,String piSLDAY   
						,String piDONVILD_ID
						,String pdNGAY_PDV 
						,String piMANV_ID
						, String piKHUYENMAI_ID
						, String piTIEN_KM
						
									 ) {
    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_test_hiep.capnhat_thuebao_ld("
			+"'"+psNGUOI_CN+"',"                      
			+piDONVIQL_ID+","                         
			+"'"+psSO_NHA+"',"                        
			+piTHUTU_IN+","                           
			+"'"+psSOMAY_TN+"',"                      
			+"'"+psMA_TB+"',"                         
			+"'"+psMAY_CN+"',"                        
			+"'"+psMA_BC+"',"                         
			+piDONVITC_ID+","                         
			+piTRACUU_DB+","                          
			+"'"+psGIAPDANH+"',"                      
			+piCUOCNH_ID+","                          
			+piKHUYENMAI+","                          
			+piDANGKY_DB+","                          
			+"'"+psTEN_DB+"',"                        
			+piTRAMVT_ID+","                          
			+piVISA_ID+","                            
			+piTHUEBAO_ID+","                         
			+"'"+psMA_HD+"',"                         
			+"'"+psTEN_TB+"',"                        
			+"'"+psDIACHI+"',"                        
			+piLOAITB_ID+","                          
			+"'"+psDOITUONG_ID+"',"                   
			+piDANGKY_DV+","                            
			+piTRAGOP+","                               
			+piCUOC_LD+","                            
			+piCUOC_DV+","                            
			+piTRANGTHAI+","                          
			+piVAT_LD+","                             
			+piVAT_DV+","                             
			+piLOAIDB_ID+","                          
			+piINCHITIET+","                          
			+piCHUNGLOAI_ID+","                       
			+piTIENMAY+","                            
			+piVAT_MAY+","                            
			+psGHICHU+","                        
			+"'"+piQUAN_ID+"',"                            
			+"'"+piPHUONG_ID+"',"                          
			+"'"+piPHO_ID+"',"                             
			+piKIEULD_ID+","     
			+"'"+psDANHSACH_DV+"',"
			+"'"+psDANHSACH_DVNOIDUNG+"',"
			//+piDICHVUVT_ID+","                             
			+piLOAIKH_ID+","                             
			+"'"+psMA_TB_CU+"',"     

			+piLOAIDAY_ID+","    
			+piSLDAY+","   
			+piDONVILD_ID+","
			+pdNGAY_PDV+","
			+ piMANV_ID + "," 
			+ piKHUYENMAI_ID + ","
			+ piTIEN_KM
        +");"
        +"end;"
        ;
    return s;

  }
  public String themmoiTBLD(String schema
						,String psNGUOI_CN                      
						,String piDONVIQL_ID                    
						,String psSO_NHA                        
						,String piTHUTU_IN                      
						,String psSOMAY_TN                      
						,String psMA_TB                         
						,String psMAY_CN                        
						,String psMA_BC                         
						,String piDONVITC_ID                    
						,String piTRACUU_DB                     
						,String psGIAPDANH                      
						,String piCUOCNH_ID                     
						,String piKHUYENMAI                     
						,String piDANGKY_DB                     
						,String psTEN_DB                        
						,String piTRAMVT_ID                     
						,String piVISA_ID                       
						,String piTHUEBAO_ID                    
						,String psMA_HD                         
						,String psTEN_TB                        
						,String psDIACHI                        
						,String piLOAITB_ID                     
						,String psDOITUONG_ID                   
						,String piDANGKY_DV                       
						,String piTRAGOP                          
						,String piCUOC_LD                       
						,String piCUOC_DV                       
						,String piTRANGTHAI                     
						,String piVAT_LD                        
						,String piVAT_DV                        
						,String piLOAIDB_ID                     
						,String piINCHITIET                     
						,String piCHUNGLOAI_ID                  
						,String piTIENMAY                       
						,String piVAT_MAY                       
						,String psGHICHU                        
						,String piQUAN_ID                       
						,String piPHUONG_ID                     
						,String piPHO_ID                        
						,String piKIEULD_ID   
						,String psDANHSACH_DV
						,String psDANHSACH_DVNOIDUNG
						//,String piDICHVUVT_ID                        
						,String piLOAIKH_ID   
						
						,String piLOAIDAY_ID    
						,String piSLDAY   
						,String piDONVILD_ID
						,String pdNGAY_PDV 
						,String piMANV_ID
						,String piKHUYENMAI_ID
						,String piTIEN_KM
									 ) {
    String s=    "begin ? := "+CesarCode.decode(schema)+"pttb_test_hiep.themmoi_thuebao_ld("
			+"'"+psNGUOI_CN+"',"                      
			+piDONVIQL_ID+","                         
			+"'"+psSO_NHA+"',"                        
			+piTHUTU_IN+","                           
			+"'"+psSOMAY_TN+"',"                      
			+"'"+psMA_TB+"',"                         
			+"'"+psMAY_CN+"',"                        
			+"'"+psMA_BC+"',"                         
			+piDONVITC_ID+","                         
			+piTRACUU_DB+","                          
			+"'"+psGIAPDANH+"',"                      
			+piCUOCNH_ID+","                          
			+piKHUYENMAI+","                          
			+piDANGKY_DB+","                          
			+"'"+psTEN_DB+"',"                        
			+piTRAMVT_ID+","                          
			+piVISA_ID+","                            
			+piTHUEBAO_ID+","                         
			+"'"+psMA_HD+"',"                         
			+"'"+psTEN_TB+"',"                        
			+"'"+psDIACHI+"',"                        
			+piLOAITB_ID+","                          
			+"'"+psDOITUONG_ID+"',"                   
			+piDANGKY_DV+","                            
			+piTRAGOP+","                               
			+piCUOC_LD+","                            
			+piCUOC_DV+","                            
			+piTRANGTHAI+","                          
			+piVAT_LD+","                             
			+piVAT_DV+","                             
			+piLOAIDB_ID+","                          
			+piINCHITIET+","                          
			+piCHUNGLOAI_ID+","                       
			+piTIENMAY+","                            
			+piVAT_MAY+","                            
			+psGHICHU+","                        
			+"'"+piQUAN_ID+"',"                            
			+"'"+piPHUONG_ID+"',"                          
			+"'"+piPHO_ID+"',"                             
			+piKIEULD_ID+","     
			+"'"+psDANHSACH_DV+"',"
			+"'"+psDANHSACH_DVNOIDUNG+"',"
			//+piDICHVUVT_ID+","                             
			+piLOAIKH_ID+"," 

			+piLOAIDAY_ID+","    
			+piSLDAY+","   
			+piDONVILD_ID+","
			+pdNGAY_PDV+"," 
			+piMANV_ID+ ","
			+piKHUYENMAI_ID + ","
			+piTIEN_KM
        +");"
        +"end;"
        ;
    return s;

  }  
    
}
