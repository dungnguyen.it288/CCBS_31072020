
package neo;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSetMetaData;

import javax.sql.RowSet;

import neo.smartui.common.CesarCode;
import neo.smartui.process.NEOProcessEx;
import neo.smartui.report.NEOCommonData;
import neo.smartui.report.NEOExecInfo;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;



public class ccbs_khuyenmai extends NEOProcessEx {
  public void run() {
    System.out.println("neo.khuyenmai was called");
  }

  
//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData laytt_kmid_congvans(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmcv_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_congvans('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmcv_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }



//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData laytt_kmid_dk_cktms(
 										 String func_schema,
 										 String schemaCommon,
 										 String km_dk_cktm_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_dk_cktms('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+km_dk_cktm_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }






//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData laytt_kmid_cktms(
 										 String func_schema,
 										 String schemaCommon,
 										 String km_cktm_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_cktms('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+km_cktm_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }
  
  
  


//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData laytt_kmid_cktts(
 										 String func_schema,
 										 String schemaCommon,
 										 String km_cktt_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_cktts('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+km_cktt_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }
  
     

//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData laytt_kmid_dk_cktts(
 										 String func_schema,
 										 String schemaCommon,
 										 String km_dk_cktt_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_dk_cktts('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+km_dk_cktt_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }




  
//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData layds_km_cachgiam_id(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmcg_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.layds_km_cachgiam_id('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmcg_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }



  
//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData layds_km_cachgiam_area_id(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmcg_area_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.layds_km_cachgiam_area_id('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmcg_area_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }




//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData laytt_kmid_dieukiens(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmdk_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_dieukiens('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmdk_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }



//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData laytt_kmid_hinhthucs(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmht_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_hinhthucs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmht_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }


public NEOCommonData layds_km_hinhthucs( String kmcv_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_congvans/km_hinhthucs_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmcv_id")+"="+kmcv_id+"&sid="+Math.random();
		
		System.out.println(url_); 	    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  
  
  

    
   public NEOCommonData layds_km_congvans( String radDangAD)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_congvans/km_congvans_ajax";	
		url_ = url_ + "&"+CesarCode.encode("ds_congvans")+"="+radDangAD+"&sid="+Math.random();
		
		System.out.println(url_); 	    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  
    public NEOCommonData layds_km_area_ids( String radDangAD)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_area_codes/km_area_ids_ajax";	
		url_ = url_ + "&"+CesarCode.encode("ds_area_id")+"="+radDangAD+"&sid="+Math.random();
		
		System.out.println(url_); 	    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  
  	
  	
   public NEOCommonData layds_kmid_congvans(
   			String pikmcv_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_congvans/km_hinhthucs_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmcv_id")+"="+pikmcv_id+"&sid="+Math.random();
			    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  	
  	
  	
   public NEOCommonData layds_kmid_hinhthucs(
   			String pikmht_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_cachgiams/km_cachgiams_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmht_id+"&sid="+Math.random();
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}  	
  	
   public NEOCommonData layds_id_hinhthuc_dk_cktts(
   			String pikmht_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_ckhau_tts/km_dk_ckhau_tts_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmht_id+"&sid="+Math.random();
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
   public NEOCommonData layds_id_hinhthuc_dk_cktms(
   			String pikmht_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_ckhau_tms/km_dk_ckhau_tms_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmht_id+"&sid="+Math.random();
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
   public NEOCommonData layds_id_hinhthuc_cktts(
   			String pikmht_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_ckhau_tts/km_ckhau_tts_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmht_id+"&sid="+Math.random();
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
   public NEOCommonData layds_id_hinhthuc_cktms(
   			String pikmht_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_ckhau_tms/km_ckhau_tms_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmht_id+"&sid="+Math.random();
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
 
   public NEOCommonData layds_kmid_hinhthuc_areas(
   			String pikmht_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_cachgiam_areas/km_cachgiam_areas_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmht_id+"&sid="+Math.random();
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
 
  	
   public NEOCommonData layds_kmid_dieukiens(
   			String pikmht_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_dieukiens/km_dieukiens_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmht_id+"&sid="+Math.random();
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  	 	
  	
  	
  public NEOCommonData ds_hinthucs(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/common/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}

  public NEOCommonData ds_dieukiens_ht(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_dieukiens/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
	
 	public NEOCommonData ds_hinthucs_area(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_cachgiam_areas/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
		
		public NEOCommonData ds_hinthucs_direction(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_cachgiam_directions/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
		
 public NEOCommonData ds_dieukiens_direction(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_cachgiam_directions/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
	
	public NEOCommonData ds_dieukiens_htt(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_uploads/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
		
		
		public NEOCommonData ds_htkm_dk_cktts(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_ckhau_tts/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
			public NEOCommonData ds_htkm_tbdbs(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_danhsach_tbs/km_danhsach_tbdbs/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
	
				public NEOCommonData ds_htkm_tbs(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_danhsach_tbs/km_danhsach_tbs/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
		
		public NEOCommonData ds_htkm_dk_cktms(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_ckhau_tms/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
	
  	
  		public NEOCommonData ds_htkm_cktts(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_ckhau_tts/ds_km_hinhthuc_cktts&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
	
		public NEOCommonData ds_htkm_cktms(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_ckhau_tms/ds_km_hinhthuc_cktms&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
		
	
	
  	
   public NEOCommonData layds_kmid_direct_codes(
   			String pikmcv_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_direction_codes/km_direct_details_ajax";	
		url_ = url_ + "&"+CesarCode.encode("km_kmtc_id")+"="+pikmcv_id+"&sid="+Math.random();
			    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  	
  	
   public NEOCommonData layds_kmid_area_codes(
   			String pikmcv_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_area_codes/km_area_codes_ajax";	
		url_ = url_ + "&"+CesarCode.encode("km_area_code_id")+"="+pikmcv_id+"&sid="+Math.random();
			    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  	
  	
   public NEOCommonData layds_kmid_areacode_details(
   			String pikmcv_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_area_codes/km_areacode_details_ajax";	
		url_ = url_ + "&"+CesarCode.encode("areacodes")+"="+pikmcv_id+"&sid="+Math.random();
			    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  	
 
  public NEOCommonData layds_cachgiam_id_tinhs(
 										 String func_schema,
 										 String schemaCommon,
 										 String cachgiam_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.layds_cachgiam_id_tinhs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"'," +cachgiam_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }

  
  public NEOCommonData themmoi_km_congvans(
  							String  schema,
  							String  schemaCommon  																	  						   			        
					        ,String psten_congvan
							,String psngay_batdau
							,String psngay_kt
							,String pscon_hieuluc
							,String psnoidung
							,String pskmuutien
							,String psnguoi_cn
  							,String psmay_cn						
                            ,String psupload 
							,String psngay_kp
							,String psfix_promotion
							,String pscommitted_liquidate
							,String psnums_subscriber							
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_congvans('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")
		+"','"+psten_congvan+"'"                      
        +","+psngay_batdau
        +","+psngay_kt
		+","+psngay_kp
        +",'"+pscon_hieuluc+"'"  
        +",'"+psnoidung+"'"
        +",'"+pskmuutien+"'"
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
		+",'"+psupload+"'"
		+",'"+psfix_promotion+"'"
		+",'"+pscommitted_liquidate+"'"
		+",'"+psnums_subscriber+"'"
        +");"
        +"end;"
        ;
    
   	 //System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
 
	
  
  public NEOCommonData capnhat_km_congvans(
  							String  schema,
  							String  schemaCommon,  
  							String  pikmcv_id																  						   			        
					       	,String psten_congvan
							,String psngay_batdau
							,String psngay_kt
							,String pscon_hieuluc
							,String psnoidung
							,String pskmuutien
							,String  psnguoi_cn
  							,String  psmay_cn				
                            ,String psupload
							,String psngay_kp
							,String psfix_promotion
							,String pscommitted_liquidate
							,String psnums_subscriber
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_congvans('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmcv_id+",'"+psten_congvan+"'"                         
        +","+psngay_batdau
        +","+psngay_kt
		+","+psngay_kp
        +",'"+pscon_hieuluc+"'"  
		+",'"+psnoidung+"'"
		+",'"+pskmuutien+"'"
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
		+",'"+psupload+"'"
		+",'"+psfix_promotion+"'"
		+",'"+pscommitted_liquidate+"'"
		+",'"+psnums_subscriber+"'"
        +");"
        +"end;"
        ;
		
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
    
  public NEOCommonData themmoi_km_hinhthucs(
  							String  schema,
  							String  schemaCommon,
  							String  pikmcv_id																	  						   			        
					       ,String  psten_hinhthuc
							,String psloaiht_id
							,String pscamket_sdlt
							,String pstinhtao_ds
							,String tienphaidong							
							,String kmchung
							,String chkHanmuc
							,String hanmuc
							,String psghichu
							,String  psnguoi_cn
  							,String  psmay_cn						
                            ,String psupload
							,String psFromdate
							,String psTodate
							,String PsEnable
  							,String psMoneyHM
							,String psMoneyCD
							,String psLoaiALO
									 ) {

    String s = "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_hinhthucs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmcv_id
		+",'"+psten_hinhthuc+"'"
        +","+psloaiht_id
        +","+pscamket_sdlt
        +","+pstinhtao_ds        
        +","+tienphaidong        
        +","+kmchung     
        +","+chkHanmuc
        +","+hanmuc
		+",'"+psghichu
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
		+",'"+psupload+"'"
		+",'"+psFromdate+"'"
		+",'"+psTodate+"'"
		+",'"+PsEnable+"'"
		+",'"+psMoneyHM+"'"
		+",'"+psMoneyCD+"'"
		+",'"+psLoaiALO+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_hinhthucs(
  							String  schema,
  							String  schemaCommon, 
  							String  pikmcv_id  
  							,String  pikmht_id																	  						   			        
					       ,String  psten_hinhthuc
							,String psloaiht_id
							,String pscamket_sdlt
							,String pstinhtao_ds
							,String pstienphaidong
							,String pskmchung
							,String chkHanmuc
							,String hanmuc
							,String psghichu
							,String psnguoi_cn
  							,String psmay_cn
							,String psupload				
							,String psFromdate
							,String psTodate
							,String PsEnable
  							,String psMoneyHM
							,String psMoneyCD
							,String psLoaiALO
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_hinhthucs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmcv_id+","+pikmht_id                    
        +",'"+psten_hinhthuc        
        +"',"+psloaiht_id
        +","+pscamket_sdlt
        +","+pstinhtao_ds
        +","+pstienphaidong
        +","+pskmchung
        +","+chkHanmuc
        +","+hanmuc
        +",'"+psghichu+"'"
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
		+",'"+psupload+"'"
		+",'"+psFromdate+"'"
		+",'"+psTodate+"'"
		+",'"+PsEnable+"'"
		+",'"+psMoneyHM+"'"
		+",'"+psMoneyCD+"'"
		+",'"+psLoaiALO+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
	public NEOCommonData xoa_km_hinhthucs(
  							String  schema,
  							String  schemaCommon,   
  							String  pikmcv_id,	  																					  						   			        
					       	String  pikmht_id						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_hinhthucs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmcv_id
		+","+pikmht_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
    }  
  
  
  //Thong tin ve cach giam cuoc. cachgiams
  public NEOCommonData themmoi_km_cachgiams(
  							String  schema,
  							String  schemaCommon 						
							,String sten_cachgiam							
							,String sso_thang
							,String sapdung_sauthanghm
							,String stien_kieuld1
							,String stien_kieuld2
							,String stien_kieuld3
							,String stien_kieuld4
							,String stien_phaitra
							,String sps_toithieu
							,String chk_novat_pstoithieu
							,String s_apdungkmtc_id
							,String skhoanmuctcs
							,String stienkm_duochuong
							,String stien_truocthue
							,String sghichu
							,String skmcv_id
							,String skmht_id
							,String shinhthuctt_id
							,String skieugiam_id	
							,String  psnguoi_cn
							,String  psmay_cn) 
   {
   	   String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_cachgiams('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"'"
		+",'"+sten_cachgiam
		+"','"+sso_thang
		+"','"+sapdung_sauthanghm
		+"','"+stien_kieuld1
		+"','"+stien_kieuld2
		+"','"+stien_kieuld3
		+"','"+stien_kieuld4
		+"','"+stien_phaitra
		+"','"+sps_toithieu
		+"','"+chk_novat_pstoithieu
		+"','"+s_apdungkmtc_id
		+"','"+skhoanmuctcs
		+"','"+stienkm_duochuong
		+"','"+stien_truocthue
		+"','"+sghichu
		+"','"+skmcv_id
		+"','"+skmht_id
		+"','"+shinhthuctt_id
		+"','"+skieugiam_id	
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_cachgiams(
  							String  schema,
  							String  schemaCommon 
  							,String pikmcg_id						
							,String sten_cachgiam							
							,String sso_thang
							,String sapdung_sauthanghm
							,String stien_kieuld1
							,String stien_kieuld2
							,String stien_kieuld3
							,String stien_kieuld4
							,String stien_phaitra
							,String sps_toithieu
							,String chk_novat_pstoithieu
							,String s_apdungkmtc_id
							,String skhoanmuctcs
							,String stienkm_duochuong
							,String stien_truocthue
							,String sghichu
							,String skmcv_id
							,String skmht_id
							,String shinhthuctt_id
							,String skieugiam_id	
							,String  psnguoi_cn
							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_cachgiams('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmcg_id                   
       	+",'"+sten_cachgiam
		+"','"+sso_thang
		+"','"+sapdung_sauthanghm
		+"','"+stien_kieuld1
		+"','"+stien_kieuld2
		+"','"+stien_kieuld3
		+"','"+stien_kieuld4
		+"','"+stien_phaitra
		+"','"+sps_toithieu
		+"','"+chk_novat_pstoithieu
		+"','"+s_apdungkmtc_id
		+"','"+skhoanmuctcs
		+"','"+stienkm_duochuong
		+"','"+stien_truocthue
		+"','"+sghichu
		+"','"+skmcv_id
		+"','"+skmht_id
		+"','"+shinhthuctt_id
		+"','"+skieugiam_id	
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_cachgiams(
  							String  schema,
  							String  schemaCommon,   
  							String  pikmht_id,	  																					  						   			        
					       	String  pikmcg_id						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_cachgiams('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmht_id
		+","+pikmcg_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  //--Ket thuc xu ly ve cach giam cuoc dien thoai.
  
  
  //Thong tin ve dieu kien lay danh sach khuyen mai
  public NEOCommonData themmoi_km_dieukiens(
  							String  schema,
  							String  schemaCommon 						
							,String psten_dieukien
							,String psdoituongs	
							,String psngay_kt
							,String psghichu
							,String pskmkt_id
							,String pskmht_id
							,String  psnguoi_cn
							,String  psmay_cn) 
   {
   	   String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_dieukiens('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"'"
		+",'"+psten_dieukien
		+"','"+psdoituongs
		+"',"+psngay_kt
		+",'"+psghichu
		+"','"+pskmkt_id
		+"','"+pskmht_id
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_dieukiens(
  							String  schema,
  							String  schemaCommon 
  							,String  pskmdk_id						
							,String psten_dieukien
							,String psdoituongs		
							,String psngay_kt
							,String psghichu
							,String pskmkt_id
							,String pskmht_id
							,String  psnguoi_cn
							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_dieukiens('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pskmdk_id                   
       	+",'"+psten_dieukien
        +"','"+psdoituongs
		+"',"+psngay_kt
		+",'"+psghichu
		+"','"+pskmkt_id
		+"','"+pskmht_id
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_dieukiens(
  							String  schema,
  							String  schemaCommon,   
  							String  pikmht_id,	  																					  						   			        
					       	String  pikmdk_id						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_dieukiens('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmht_id
		+","+pikmdk_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  //--Ket thuc xu ly ve dieukien lay danh sach.
  
	public NEOCommonData ds_nhanvien(int donviql_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=khuyenmai/baocao/danhsach_tien_kms/ds_nhanviens&" + CesarCode.encode("donviql_id") + "=" + donviql_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}
	
	public NEOCommonData ds_nhanvien_inphieu(int donviql_id)
	{
	    	String url_ = "/main?"; 
	    	url_ = url_+ CesarCode.encode("configFile") + "=khuyenmai/baocao/in_hoadon_kms/ds_nhanviens&" + CesarCode.encode("donviql_id") + "=" + donviql_id;
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}
  
  public NEOCommonData laytt_ngay_apdung(   String func_schema,
											String schemaCommon,
											String kmht_id,
											String kmcv_id)
	{
		String s = "begin ?:=" + schemaCommon + "promotions.laytt_ngay_apdung('" + getUserVar("userID")
			 + "','" + getUserVar("sys_agentcode")
			 + "','" + schemaCommon
			 + "','" + getUserVar("sys_dataschema")
			 + "'," + kmht_id
			 + "," + kmcv_id
			 + ");end;";
		System.out.println(s); 
		
		NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}
  
  
  /*Nhu thanh the:
  *chuc nang: lay thong tin thue bao khuyen mai.
  *phuc vu nut tim kiem
  *ngay_pt: 09/10/2008. 
  						
  */
    public NEOCommonData layds_tb_khuyenmais
								(String piPAGEID,
    							   String piREC_PER_PAGE,    							
    							   String ma_tb,
    							   String ngay_apdung, 
    							   String ngay_kt,   							  
    							   String khuyenmai_id,
    							   String trangthai_id) {
		
    String  s= "/main?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_danhsach_tbs/km_danhsach_tbs/km_danhsach_tbs_ajax"
		+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
		+ "&" +CesarCode.encode("ma_tb")+"="+ma_tb
		+ "&" +CesarCode.encode("ngay_apdung")+"="+ngay_apdung
		+ "&" +CesarCode.encode("ngay_kt")+"="+ngay_kt
		+ "&" +CesarCode.encode("khuyenmai_id")+"="+khuyenmai_id
		+ "&" +CesarCode.encode("trangthai_id")+"="+trangthai_id
        ;   
	System.out.println(s); 
		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_); 
     
  }
  
  
  public NEOCommonData themmoi_ds_khuyenmais(	 
										 String psschema,
  										 String psschemaCommon,   
  										 String psma_tb  
										,String pikmht_id
										,String pikmcv_id										
										,String psghichu
										,String psnguoi_cn
										,String psmay_cn
									
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"promotions.themmoi_ds_khuyenmais('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
        +",'"+psma_tb+"'"
        +","+pikmht_id 
        +","+pikmcv_id      
		+","+psghichu
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_); 
    
  			
  	}
	
public NEOCommonData capnhat_ds_khuyenmais(
     String psschema
	,String psschemaCommon  
	,String psdsSomay   									
	,String psdsKMID
	,String psdsApdung
	,String psghichu
	,String psnguoi_cn
	,String psmay_cn
									
 ){
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"promotions.capnhat_ds_khuyenmais('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)
        +"','"+psdsKMID
        +"','"+psdsApdung
        +"','"+psdsSomay       
		+"',"+psghichu
		+",'"+psnguoi_cn
		+"','"+psmay_cn
        +"');"
        +"end;";
        
     System.out.println(s); 		
  	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);    
  }  

public NEOCommonData laytt_tb_dacbiet ( String func_schema,
 										 String schemaCommon,
 										 String ma_tb,
 										 String khuyenmai_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_tb_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"','"+ma_tb+"'"
		+","+khuyenmai_id
		+");end;";
	
	 System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);  
  }
  
  
	
  public NEOCommonData laytt_khuyenmais(    String func_schema,
											String schemaCommon,
											String khuyenmai_id)
	{
		
	  String s = "";
	  /*
	  String s = "begin ?:=" + schemaCommon + "khuyenmai.laytt_khuyenmais('" + getUserVar("userID")
			 + "','" + getUserVar("sys_agentcode")
			 + "','" + schemaCommon
			 + "','" + getUserVar("sys_dataschema")
			 + "'," + khuyenmai_id
			 + ");end;";
	*/
		 System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}

	
	
	
 public NEOCommonData themmoi_tbkm_dacbiet(	 String psschema,
  										 String psschemaCommon,   
  										 String psma_tb
										,String pikmht_id
										,String pskieuld_id										
										,String psghichu
										,String pdngay_ld									
										,String pdngay_in_db
										,String psnguoi_cn
										,String psmay_cn
									
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"promotions.themmoi_tbkm_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
        +",'"+psma_tb+"'"
        +","+pikmht_id
        +","+pskieuld_id
        +","+psghichu	
		+","+pdngay_ld
	    +","+pdngay_in_db
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
      System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);  
  	}
  
  
	
	
	public NEOCommonData capnhat_tbkm_dacbiet(
  								 String psschema,
  										 String psschemaCommon,   
  										 String psma_tb
										,String pikmht_id
										,String pskieuld_id										
										,String psghichu
										,String pdngay_ld									
										,String pdngay_in_db
										,String psnguoi_cn
										,String psmay_cn
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"promotions.capnhat_tbkm_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
         +",'"+psma_tb+"'"
        +","+pikmht_id
        +","+pskieuld_id
        +","+psghichu	
		+","+pdngay_ld
	    +","+pdngay_in_db
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;";
		
    		System.out.println(s); 		
			NEOExecInfo nei_ = new NEOExecInfo(s);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
  }   
  
  
 public NEOCommonData xoa_tbkm_dacbiet( String psschema, 
 										String psschemaCommon, 	 
 										String psma_tb
										,String pikmht_id)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"promotions.xoa_tbkm_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_dataschema")+"'"
        +",'"+psma_tb+"'"
        +","+pikmht_id      
        +");"
        +"end;"
        ;        
    System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);  
  }
 
//khuyen mai theo ma vungs
	
    
  public NEOCommonData themmoi_km_cachgiam_areas(
  							String  schema,
  							String  schemaCommon
  								
							,String sten_cachgiam
							,String ssophut_tinnhan
							,String ssotien_phaitra
								
							,String sso_thang
							,String sapdung_sauthanghm
							,String s_cachtru
								
							,String sid_khuyenmais
							,String stu_ngay
							,String sden_ngay
							
							,String pstoithieu
							,String s_apdungkmtc_id
							,String skhoanmuctcs
							,String hm_sms
							,String hm_mms
							,String hm_tien
							,String hm_voice
							,String first_dur
							,String next_dur
							,String ngay_km
							,String anomaly
							,String anomaly_id
								
							,String sghichu
							//,String skmcv_id
							,String skmht_id
							,String shinhthuctt_id
							,String skieugiam_id
								
									,String psnguoi_cn
										,String psmay_cn					
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_cachgiam_areas('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"'"
		+",'"+sten_cachgiam+"'"
        +",'"+ssophut_tinnhan
        +"','"+ssotien_phaitra
        	
        +"','"+sso_thang        	
        +"','"+sapdung_sauthanghm
        +"','"+s_cachtru
        	
		+"','"+sid_khuyenmais
		+"','"+stu_ngay			
		+"','"+sden_ngay
		
		+"','"+ pstoithieu
		+"','"+ s_apdungkmtc_id
		+"','"+ skhoanmuctcs
		+"','"+ hm_sms
		+"','"+ hm_mms
		+"','"+ hm_tien
		+"','"+ hm_voice
		+"','"+ first_dur
		+"','"+ next_dur
		+"','"+ ngay_km
		+"','"+ anomaly
		+"','"+ anomaly_id
			
		+"','"+sghichu
		//+"','"+skmcv_id
		+"','"+skmht_id
		+"','"+shinhthuctt_id
			
		+"','"+skieugiam_id
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_cachgiam_areas(
  							String  schema,
  							String  schemaCommon, 
  							String  pikmcg_area_id  
  							,String sten_cachgiam
							,String ssophut_tinnhan
							,String ssotien_phaitra
								
							,String sso_thang
							,String sapdung_sauthanghm
							,String s_cachtru
								
							,String sid_khuyenmais
							,String stu_ngay
							,String sden_ngay
							
							,String pstoithieu
							,String s_apdungkmtc_id
							,String skhoanmuctcs
							,String hm_sms
							,String hm_mms
							,String hm_tien
							,String hm_voice
							,String first_dur
							,String next_dur
							,String ngay_km
							,String anomaly
							,String anomaly_id
								
							,String sghichu
							//,String skmcv_id
							,String skmht_id
							,String shinhthuctt_id
							,String skieugiam_id
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_cachgiam_areas('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmcg_area_id                    
       	+",'"+sten_cachgiam+"'"
        +",'"+ssophut_tinnhan
        +"','"+ssotien_phaitra
        	
        +"','"+sso_thang        	
        +"','"+sapdung_sauthanghm
        +"','"+s_cachtru
        	
		+"','"+sid_khuyenmais
		+"','"+stu_ngay			
		+"','"+sden_ngay
		
		+"','"+ pstoithieu
		+"','"+ s_apdungkmtc_id
		+"','"+ skhoanmuctcs
		+"','"+ hm_sms
		+"','"+ hm_mms
		+"','"+ hm_tien
		+"','"+ hm_voice
		+"','"+ first_dur
		+"','"+ next_dur
		+"','"+ ngay_km
		+"','"+ anomaly
		+"','"+ anomaly_id
			
		+"','"+sghichu
		//+"','"+skmcv_id
		+"','"+skmht_id
		+"','"+shinhthuctt_id
			
		+"','"+skieugiam_id
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_cachgiam_areas(
  							String  schema,
  							String  schemaCommon,   
  							String  pikmht_id,	  																					  						   			        
					       	String  pikmcg_area_id						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_cachgiam_areas('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmht_id
		+","+pikmcg_area_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  
//Ket thuc khuyen mai theo ma vungs

//khuyen mai theo khoan muc tinh cuoc
	
    
  public NEOCommonData themmoi_km_cachgiam_directions(
  							String  schema,
  							String  schemaCommon
  								
							,String sten_cachgiam
							,String pstien_phantram
							,String ssotien_phaitra
								
							,String sso_thang
							,String sapdung_sauthanghm
						//	,String s_cachtru
							
							,String pstoithieu
							,String s_apdungkmtc_id
							,String skhoanmuctcs
							
							,String kieutrutien
								
							,String sid_khuyenmais
						//	,String stu_ngay
						//	,String sden_ngay
								
							,String sghichu
							//,String skmcv_id
							,String skmht_id
							,String shinhthuctt_id
							,String skieugiam_id
								
									,String psnguoi_cn
										,String psmay_cn					
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_cachgiam_directions('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"'"
		+",'"+sten_cachgiam+"'"
        +",'"+pstien_phantram
        +"','"+ssotien_phaitra
       // +"','"+s_cachtru
        
        +"','"+sso_thang        	
        +"','"+sapdung_sauthanghm
        
        +"','"+pstoithieu  
        +"','"+s_apdungkmtc_id
		+"','"+skhoanmuctcs
        
        +"','"+kieutrutien
        
		+"','"+sid_khuyenmais
	//	+"','"+stu_ngay			
	//	+"','"+sden_ngay
			
		+"','"+sghichu
		//+"','"+skmcv_id
		+"','"+skmht_id
		+"','"+shinhthuctt_id
			
		+"','"+skieugiam_id
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_cachgiam_directions(
  							String  schema,
  							String  schemaCommon, 
  							String  pikmcg_direction_id  
  							,String sten_cachgiam
							,String pstien_phantram
							,String ssotien_phaitra
								
							,String sso_thang
							,String sapdung_sauthanghm
						//	,String s_cachtru
								
							,String pstoithieu
							,String s_apdungkmtc_id
							,String skhoanmuctcs
							
							,String kieutrutien
							
							,String sid_khuyenmais
							//,String stu_ngay
						//	,String sden_ngay
								
							,String sghichu
							//,String skmcv_id
							,String skmht_id
							,String shinhthuctt_id
							,String skieugiam_id
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_cachgiam_directions('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmcg_direction_id                    
       	+",'"+sten_cachgiam+"'"
        +",'"+pstien_phantram
        +"','"+ssotien_phaitra
        	
        +"','"+sso_thang        	
        +"','"+sapdung_sauthanghm
       // +"','"+s_cachtru
        	
        +"','"+pstoithieu       
        +"','"+s_apdungkmtc_id
		+"','"+skhoanmuctcs
        
        +"','"+kieutrutien
        
		+"','"+sid_khuyenmais
	//	+"','"+stu_ngay			
	//	+"','"+sden_ngay
			
		+"','"+sghichu
		//+"','"+skmcv_id
		+"','"+skmht_id
		+"','"+shinhthuctt_id
			
		+"','"+skieugiam_id
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_cachgiam_directions(
  							String  schema,
  							String  schemaCommon,   
  							String  pikmht_id,	  																					  						   			        
					       	String  pikmcg_direction_id						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_cachgiam_directions('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmht_id
		+","+pikmcg_direction_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  	
  	
  	 
 public NEOCommonData layds_km_cachgiam_direction_id(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmcg_direction_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.layds_km_cachgiam_direction_id('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmcg_direction_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }
  
  
   
   public NEOCommonData layds_kmid_hinhthuc_directions(
   			String pikmht_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_cachgiam_directions/km_cachgiam_directions_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmht_id+"&sid="+Math.random();
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  
//Ket thuc khuyen mai theo khoan muc tinh cuoc    

//Bat dau xu ly ID khuyen mai theo dieu huong dien thoai.
	
    
   public NEOCommonData layds_km_direct_codes( String radDangAD)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_direction_codes/km_direct_codes_ajax";	
		url_ = url_ + "&"+CesarCode.encode("ds_direct_codes")+"="+radDangAD+"&sid="+Math.random();
		
		System.out.println(url_); 	    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  
  
  
   public NEOCommonData layds_km_direct_details( String radDangAD)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_direction_codes/km_direct_details_ajax";	
		url_ = url_ + "&"+CesarCode.encode("ds_direct_details")+"="+radDangAD+"&sid="+Math.random();
		
		System.out.println(url_); 	    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  
   public NEOCommonData laytt_kmid_direct_codes(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmcv_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_direct_codes('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmcv_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }
  
     public NEOCommonData laytt_kmid_area_ids(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmcv_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_area_ids('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmcv_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }
  
  
  
  
  
  
   public NEOCommonData laytt_kmid_direct_details(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmcv_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_direct_details('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmcv_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }
  
  
  
  
  public NEOCommonData themmoi_km_direct_codes(
  							String  schema,
  							String  schemaCommon, 																	  						   			        
					        String name,
							String picon_hieuluc,		
							String psNoiDungKM
							,String  psnguoi_cn,
  							String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_direct_codes('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"','"+name+"'"                      
        +","+picon_hieuluc
        +",'"+psNoiDungKM
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
 

  
  public NEOCommonData capnhat_km_direct_codes(
  							String  schema,
  							String  schemaCommon,  
  							String  id,															  						   			        
					        String name,
							String picon_hieuluc,		
							String psNoiDungKM
							,String  psnguoi_cn
  							,String  psmay_cn				
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_direct_codes('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+id+",'"+name+"'"                         
        +","+picon_hieuluc
        +",'"+psNoiDungKM
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
     System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  	
  	  
    
  public NEOCommonData themmoi_km_direct_details(
  							String  schema,
  							String  schemaCommon,
  							String  pikm_kmtc_id,
  							String	txtname_1,
							String pskmtc_id,
							String pshuongdt_id,
							String psghichu
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_direct_details('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikm_kmtc_id		
        +",'"+txtname_1
        +"','"+pskmtc_id
        +"','"+pshuongdt_id
		+"','"+psghichu
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_direct_details(
  							String  schema,
  							String  schemaCommon, 
  							//String  id,
  							String  pikm_kmtc_id,
  							String	txtname_1,
							String pskmtc_id,
							String pshuongdt_id,
							String psghichu
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

	/*,"+id+"*/
    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_direct_details('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikm_kmtc_id                    
    	+",'"+txtname_1
        +"','"+pskmtc_id
        +"','"+pshuongdt_id
		+"','"+psghichu
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_direct_details(
  							String  schema,
  							String  schemaCommon,   
  							String  id,	  																					  						   			        
					       	String  pikm_kmtc_id						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_direct_details('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+id
		+","+pikm_kmtc_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  
  
  
//Ket thuc xu ly ID khuyen mai theo huong dien thoai.  


  
  
  public NEOCommonData themmoi_km_area_ids(
  							String  schema,
  							String  schemaCommon, 																	  						   			        
					        String name,
							String picon_hieuluc,		
							String psNoiDungKM
							,String  psnguoi_cn,
  							String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_area_ids('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"','"+name+"'"                      
        +","+picon_hieuluc
        +",'"+psNoiDungKM
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
 

  
  public NEOCommonData capnhat_km_area_ids(
  							String  schema,
  							String  schemaCommon,  
  							String  id,															  						   			        
					        String name,
							String picon_hieuluc,		
							String psNoiDungKM
							,String  psnguoi_cn
  							,String  psmay_cn				
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_area_ids('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+id+",'"+name+"'"                         
        +","+picon_hieuluc
        +",'"+psNoiDungKM
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
     System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  	

  public NEOCommonData xoa_km_areacode_details(
  							String  schema,
  							String  schemaCommon,   
  							String  km_area_code_id,	  																					  						   			        
					       	String  psareacode						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_areacode_details('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+km_area_code_id
		+",'"+psareacode       
        +"');"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  	  	  
 
   public NEOCommonData them_km_areacode_details(
  							String  schema,
  							String  schemaCommon,   
  							String  km_area_code_id,	  																					  						   			        
					       	String  psareacode						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.them_km_areacode_details('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+km_area_code_id
		+",'"+psareacode       
        +"');"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   	  	  


    
  public NEOCommonData themmoi_km_dk_cktms(
  							String  schema,
  							String  schemaCommon,
  							String  pskmht_id
  							,String pstxtten_dk_cktm  
							,String pstxtds_makh  
							,String pstxtdoituongs  
							,String psid_khuyenmais  
							,String pshinhthuctt_id  
							,String pskmtc_idkhuyenmai  
							,String pstxttienno_canduoi  
							,String pstxttienno_cantren  
							,String pschkgma_kh 
							,String pschkck_luytien 
							,String  psNoiDungKM 
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_dk_cktms('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pskmht_id
		+",'"+pstxtten_dk_cktm+"'"
        +",'"+pstxtds_makh
        +"','"+pstxtdoituongs
        +"','"+psid_khuyenmais
        +"',"+pshinhthuctt_id
        +","+pskmtc_idkhuyenmai
        +",'"+pstxttienno_canduoi
       	+"','"+pstxttienno_cantren
        +"',"+pschkgma_kh
        +","+pschkck_luytien
		+",'"+psNoiDungKM
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_dk_cktms(
  							String  schema,
  							String  schemaCommon, 
  							String  pikm_dk_cktm_id  
  							,String  pskmht_id
  							,String pstxtten_dk_cktm  
							,String pstxtds_makh  
							,String pstxtdoituongs  
							,String psid_khuyenmais  
							,String pshinhthuctt_id  
							,String pskmtc_idkhuyenmai  
								
							,String pstxttienno_canduoi  
							,String pstxttienno_cantren  
							,String pschkgma_kh 
							,String pschkck_luytien 
							,String  psNoiDungKM 
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_dk_cktms('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikm_dk_cktm_id+","+pskmht_id                    
       	+",'"+pstxtten_dk_cktm+"'"
        +",'"+pstxtds_makh
        +"','"+pstxtdoituongs
           +"','"+psid_khuyenmais
        +"',"+pshinhthuctt_id
        +","+pskmtc_idkhuyenmai
        +",'"+pstxttienno_canduoi
       	+"','"+pstxttienno_cantren
        +"',"+pschkgma_kh
        +","+pschkck_luytien
		+",'"+psNoiDungKM
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_cktms(
  							String  schema,
  							String  schemaCommon    						
  							,String pikm_dk_cktm_id 
  									,String  pskmht_id 					
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_dk_cktms('"+getUserVar("userID")
							+"','"+getUserVar("sys_agentcode")		
							+"','"+CesarCode.decode(schemaCommon)
							+"','"+getUserVar("sys_dataschema")+"',"+pikm_dk_cktm_id
							+","+pskmht_id       
							+");"
							+"end;"
							;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  
  
   public NEOCommonData themmoi_km_cktms(
  							String  schema,
  							String  schemaCommon,
  							String  pskmht_id
							,String psten_cktm
							,String pstilegiam_tiengiam
						//	,String psid_khuyenmais
							,String pstien_canduoi
							,String pstien_cantren
							,String pssonamhd_canduoi
							,String pssonamhd_cantren
							,String psslmay_canduoi
							,String psslmay_cantren
						//	,String pshinhthuctt_id
							,String pskieugiam_id
							,String  psNoiDungKM 
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_cktms('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pskmht_id
		+",'"+psten_cktm+"'"
        +",'"+pstilegiam_tiengiam
      //  +"','"+psid_khuyenmais
        +"','"+pstien_canduoi
       	+"','"+pstien_cantren
       	+"','"+pssonamhd_canduoi
       	+"','"+pssonamhd_cantren
       		
        +"','"+psslmay_canduoi
        +"','"+psslmay_cantren 
      //  +"',"+pshinhthuctt_id
       	+"',"+pskieugiam_id        	
		+",'"+psNoiDungKM
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_cktms(
  							String  schema,
  							String  schemaCommon, 
  							String  pikm_cktm_id  
  							,String  pskmht_id
  							,String psten_cktm
							,String pstilegiam_tiengiam
						//	,String psid_khuyenmais
							,String pstien_canduoi
							,String pstien_cantren
							,String pssonamhd_canduoi
							,String pssonamhd_cantren
							,String psslmay_canduoi
							,String psslmay_cantren
						//	,String pshinhthuctt_id
							,String pskieugiam_id
							,String  psNoiDungKM 
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_cktms('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikm_cktm_id+","+pskmht_id                    
      	+",'"+psten_cktm+"'"
        +",'"+pstilegiam_tiengiam
       // +"','"+psid_khuyenmais
        +"','"+pstien_canduoi
       	+"','"+pstien_cantren
       	+"','"+pssonamhd_canduoi
       	+"','"+pssonamhd_cantren
        +"','"+psslmay_canduoi
        +"','"+psslmay_cantren 
      //  +"',"+pshinhthuctt_id
       	+"',"+pskieugiam_id        	
		+",'"+psNoiDungKM
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_ext_cktms(
  							String  schema,
  							String  schemaCommon  
  						
  							,String pikm_dk_cktm_id 
  									,String  pskmht_id 					
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_cktms('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikm_dk_cktm_id
		+","+pskmht_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  
  //////////////////thanh toan
  public NEOCommonData themmoi_km_dk_cktts(
  							String  schema,
  							String  schemaCommon,
  							String  pskmht_id
  							,String psten_dieukien
							,String psds_makh
							,String psdoituongs
							,String pshinhthuctts
							,String pskieuno_id
							,String psnoidung	
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_dk_cktts('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pskmht_id
		+",'"+psten_dieukien+"'"
        +",'"+psds_makh
        +"','"+psdoituongs
        +"','"+pshinhthuctts
       	+"','"+pskieuno_id
		+"','"+psnoidung
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_dk_cktts(
  							String  schema,
  							String  schemaCommon, 
  							String  pikm_cktm_id  
  							,String  pskmht_id
  							,String psten_dieukien
							,String psds_makh
							,String psdoituongs
							,String pshinhthuctts
							,String pskieuno_id
							,String psnoidung
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_dk_cktts('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikm_cktm_id+","+pskmht_id                    
      	+",'"+psten_dieukien+"'"
        +",'"+psds_makh
        +"','"+psdoituongs
        +"','"+pshinhthuctts
       	+"','"+pskieuno_id
		+"','"+psnoidung
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_cktts(
  							String  schema,
  							String  schemaCommon    						
  							,String pikm_cktm_id 
  									,String  pskmht_id 					
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_dk_cktts('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikm_cktm_id
		+","+pskmht_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  
  
   public NEOCommonData themmoi_km_cktts(
  							String  schema,
  							String  schemaCommon,
  							String  pskmht_id					   	    
							,String psten_cktt
							,String pstilegiam_tiengiam
							,String pstien_canduoi
							,String pstien_cantren
							,String pskhoanmuctcs
							,String pscbohinhthuctt_id
							,String pscbokieugiam_id
							,String pscbokieutru_id
							,String psghichu
							,String  psnguoi_cn
  							,String  psmay_cn						                      
			 
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.themmoi_km_cktts('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pskmht_id
		+",'"+psten_cktt+"'"
        +",'"+pstilegiam_tiengiam
        +"','"+pstien_canduoi
        +"','"+pstien_cantren
       	+"','"+pskhoanmuctcs
       	+"',"+pscbohinhthuctt_id
       	+","+pscbokieugiam_id       		
        +","+pscbokieutru_id
		+",'"+psghichu
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_km_cktts(
  							String  schema,
  							String  schemaCommon,
  							String pskm_cktt_id	
  							,String  pskmht_id	 
							,String psten_cktt
							,String pstilegiam_tiengiam
							,String pstien_canduoi
							,String pstien_cantren
							,String pskhoanmuctcs
							,String pscbohinhthuctt_id
							,String pscbokieugiam_id
							,String pscbokieutru_id
							,String psghichu
							,String  psnguoi_cn
  							,String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.capnhat_km_cktts('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pskm_cktt_id+","+pskmht_id                    
     	+",'"+psten_cktt+"'"
        +",'"+pstilegiam_tiengiam
        +"','"+pstien_canduoi
        +"','"+pstien_cantren
       	+"','"+pskhoanmuctcs
       	+"',"+pscbohinhthuctt_id
       	+","+pscbokieugiam_id       		
        +","+pscbokieutru_id
		+",'"+psghichu
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_km_ext_cktts(
  							String  schema,
  							String  schemaCommon  
  						
  							,String pikm_cktm_id 
  									,String  pskmht_id 					
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.xoa_km_cktts('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikm_cktm_id
		+","+pskmht_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
	
  public String exportDSKM(String userId, String userIp, 
			  String maTB, String ngayApdung, String ngayKT, String kmhtId, String trangthaiId) {	  
		  	  	
	    try {
	    	String s = "begin ?:=ccs_common.promotions.layds_tb_khuyenmais_export(" 
				+ "'" + userId + "'"
				+ ",'" + userIp + "'"
				+ ",'" + getUserVar("sys_dataschema") + "'"
				+ ",'" + getSysConst("FuncSchema") + "'"
				+ ",'" + maTB + "'"
				+ ",'" + ngayApdung + "'"	
				+ ",'" + ngayKT + "'"
				+ "," + kmhtId
				+ "," + trangthaiId
				+ ");end;";
	    					
			RowSet rs = reqRSet("VNPBilling",s);
			
			String fileUploadDir = getSysConst("FileUploadDir");
			String ccs = getUserVar("sys_dataschema");
			ccs = ccs.replace(".", "");
			fileUploadDir = fileUploadDir + "Export_DSKM" + "\\" + ccs;
			String fileName = "dskm.xls";
			
			Runtime.getRuntime().exec("cmd /C mkdir " + fileUploadDir);
			
			String fileOutput = fileUploadDir + "\\" + fileName;

			exportToExcel(fileOutput, rs);
			
			String ret = "begin ?:='" + "{fileurl:\"" + fileOutput.replace("\\", "\\\\") + "\",filename:\"" + fileName +"\"}" + "'; end;";
			
			return ret;
		
	    } catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}	
 
  	private void exportToExcel(String outputFile, RowSet rowSet) throws Exception {

		File file = new File(outputFile);

		if (file.exists()) {
			file.delete();
		}

		FileOutputStream fos = new FileOutputStream(file, true);
		
		ResultSetMetaData rsMeta = rowSet.getMetaData();
		int nColumn = rsMeta.getColumnCount();		
		int nRow = 0;
		rowSet.last();
		nRow = rowSet.getRow();
		rowSet.beforeFirst();
		
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = null;
		HSSFRow titleRow = null;
		HSSFRow row = null;
		HSSFCell cell = null;
		
		int nSheet = 0;
		int nRowPerSheet = 60000;
		if(nRow % nRowPerSheet == 0) {
			nSheet = nRow % nRowPerSheet;			
		} else {
			nSheet = (nRow / nRowPerSheet) + 1;
		}
		
		int nRowTmp = 0;
		String rowType = null;
		for (int i = 1; i <= nSheet; i++) {
			sheet = workbook.createSheet("DSKM_" + i);
			
			nRowTmp = 0;
			titleRow = sheet.createRow(nRowTmp);
			for (int k = 0; k < nColumn; k++) {
				cell = titleRow.createCell(k);
				cell.setCellValue(rsMeta.getColumnName(k + 1));
			}
			
			while (rowSet.next()) {
				nRowTmp++;
				row = sheet.createRow(nRowTmp);
				
				for (int j = 0; j < nColumn; j++) {
					rowType = rsMeta.getColumnTypeName(j + 1);
					cell = row.createCell(j);
					if("NUMBER".equals(rowType)) {
						cell.setCellValue(rowSet.getDouble(j + 1));
					} else if("DATE".equals(rowType)) {
						cell.setCellValue(rowSet.getDate(j + 1));
					} else {
						cell.setCellValue(rowSet.getString(j + 1));
					}
				}
				
				if(nRowTmp == nRowPerSheet) {
					break;
				}
			}
			
			for (int k = 0; k < nColumn; k++) {
				sheet.autoSizeColumn((short) (k));
			}
		}
		workbook.write(fos);
		fos.flush();
		fos.close();
	}
	public NEOCommonData ds_hinhthuc_pttb(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_uploads/ds_km_hinhthucs_pttb&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}
	public NEOCommonData tinhlai_freekm(String Chuky, String thuebao, String hinhthuc)
	{

		String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/tinhlai_freekm/tinhlai_freekm_ajax"; 
		url_ = url_ + "&"+CesarCode.encode("chuky")+"="+Chuky+"&"+CesarCode.encode("thuebao")+"="+thuebao+"&"+CesarCode.encode("hinhthuc")+"="+hinhthuc;
    
		System.out.println(url_);
		NEOExecInfo nei_ = new NEOExecInfo(url_);
		nei_.setDataSrc("VNPBilling");
		return new NEOCommonData(nei_);

	}
	public NEOCommonData chuyen_cauhinh_pttb(
  							String  schema
  							,String schemaCommon 
  							,String pikmcv_id																  						   			        
					       	,String psten_congvan
							,String psngay_batdau
							,String psngayy_kt
							,String pscon_hieuluc
							,String psnoidung							
							,String psnguoi_cn
  							,String psmay_cn				                                   
									 ) {
									
    String s=    "begin ? := admin_v2.promo_config.cauhinh_congvan('"+schemaCommon
		+"','"+psnguoi_cn
		+"','"+getUserVar("sys_agentcode")
		+"','"+psmay_cn
		+"','"+pikmcv_id
		+"','"+psten_congvan
		+"','"+pscon_hieuluc+"'"
        +","+psngay_batdau
        +","+psngayy_kt         
		+",'"+psnoidung+"'"
        +");"
        +"end;"
        ;	   
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 return new NEOCommonData(nei_);
  }
	
	public NEOCommonData cauhinh_kmht_pttb(
  							String  schema
  							,String schemaCommon 
  							,String pikmcv_id
							,String pikmht_id							
					       	,String psten_hinhthuc													
							,String psnoidung							
							,String psnguoi_cn
  							,String psmay_cn				                                   
									 ) {
									
    String s=    "begin ? := admin_v2.promo_config.cauhinh_hinhthuc('"+schemaCommon
		+"','"+psnguoi_cn
		+"','"+psmay_cn
		+"','"+getUserVar("sys_agentcode")
		+"','"+pikmcv_id
		+"','"+pikmht_id
		+"','"+psten_hinhthuc     
		+"','"+psnoidung+"'"
        +");"
        +"end;"
        ;		
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 return new NEOCommonData(nei_);
  }	
  //upload tinh
	public NEOCommonData ds_hinthucs_upload(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_upload_tinh/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}

	public NEOCommonData cauhinh_dk_pttb( 								
 										 String schemaCommon,
 										 String psngaydk,
										 String psdieukien,
										 String pscongvan,
										 String psTypeMsisdn,
										 String psNgaycd,
										 String psNgaykh,										 
										 String psNgayDefault,
										 String psuserid,
										 String psuserip										 
										 ) 										
 										{
   String s= "begin ?:="+schemaCommon+"promotions.cauhinh_dk_pttb('"+getUserVar("sys_agentcode")	
		+"','"+schemaCommon
		+"','"+pscongvan
		+"','"+psdieukien
		+"','"+psngaydk		
		+"','"+psNgaykh
		+"','"+psNgaycd
		+"','"+psNgayDefault
		+"','"+psTypeMsisdn
		+"','"+psuserid
		+"','"+psuserip+"'"
		+");end;";
	
	 System.out.println("luattd 12345"+s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
   }
    
	public NEOCommonData layds_dieukiens(String pikmcv_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_congvans/km_dieukiens_ajax";	
		url_ = url_ + "&"+CesarCode.encode("kmht_id")+"="+pikmcv_id+"&sid="+Math.random();		
			    
	    System.out.println(url_);
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
		nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
	
	public NEOCommonData laytt_kmid_dieukien(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmcv_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.laytt_kmid_dk_pttb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmcv_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
	}
	
	public NEOCommonData gencode_ccbs(
  							String  schema,
  							String  schemaCommon,   
  							String  pikmcv_id,	  																					  						   			        
					       	String  psagentcode,						
                            String  psUserIp       
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.gencode_ccbs('"+getUserVar("userID")
		+"','"+psUserIp
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+pikmcv_id
		+"','"+psagentcode+"'"       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
    } 
	
	public NEOCommonData gencode_billing(
  							String  schema,
  							String  schemaCommon,   
  							String  pikmcv_id,	  																					  						   			        
					       	String  psagentcode,	
							String  psUserIp
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.gencode_billing('"+getUserVar("userID")
		+"','"+psUserIp
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+pikmcv_id
		+"','"+psagentcode+"'"       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
    } 
	
	public String get_log_data(String psAgent, String psType, String psCvid)
	{
       String s = "/main?" + CesarCode.encode("configFile")+"=ccbs_kmck/km_congvans/promo_log_list"
	   +"&"+CesarCode.encode("psAgent") + "=" + psAgent
	   +"&"+CesarCode.encode("psType") + "=" + psType
	   +"&"+CesarCode.encode("psCvid") + "=" + psCvid
	   ;
	   return s;
	}
	
	public NEOCommonData activePromotion(
  							String  schema,
  							String  schemaCommon,   
  							String  pikmcv_id,	  																					  						   			        
					       	String  psagentcode,	
							String  psUserIp
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.activePromotion('"+getUserVar("userID")
		+"','"+psUserIp
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+pikmcv_id
		+"','"+psagentcode+"'"       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
    } 
	
	public NEOCommonData disablePromotion(
  							String  schema,
  							String  schemaCommon,   
  							String  pikmcv_id,	  																					  						   			        
					       	String  psagentcode,	
							String  psUserIp
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"promotions.disablePromotion('"+getUserVar("userID")
		+"','"+psUserIp
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+pikmcv_id
		+"','"+psagentcode+"'"       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
    } 
	
	public NEOCommonData laytt_congvan_gencode(
 										 String func_schema,
 										 String schemaCommon,
 										 String kmcv_id) 
 										{
   String s= "begin ?:="+schemaCommon+"promotions.list_dispatch_gencode('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+kmcv_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }
}

