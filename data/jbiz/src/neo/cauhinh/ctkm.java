package neo.cauhinh;
import neo.smartui.common.*;
import neo.smartui.process.*;

import java.util.*;
import javax.sql.*;
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import neo.smartui.common.CesarCode;
import neo.smartui.report.*;
import com.svcon.jdbf.*;
import javax.sql.RowSet;
import sun.misc.*;
import java.net.HttpURLConnection;
import java.net.URL;
public class ctkm extends NEOProcessEx {
	
	public String add_new_ctkm(	 
				String psLEVEL_NAME      ,
				String psVALUE_MIN      ,
				String psVALUE_MAX         ,
				String psTYPE_MSISDN        ,
				String psGROUP_PKG           ,
				String psSTATUS              ,
				String psTYPE_PHONE   
				       )
		throws Exception {
			String result =null;
			String loai ="";
				
		String s = " begin ? := admin_v2.CONFIG_CTKM.addNew_config_ctkm("
                + "'" + psLEVEL_NAME + "'"
				+ ",'"+ psVALUE_MIN + "'"
                + ",'"+ psVALUE_MAX + "'" 
                + ",'"+ psTYPE_MSISDN +"'"
				+ ",'"+ psGROUP_PKG +"'"
				+ ",'"+ psSTATUS +"'"
				+ ",'"+ getUserVar("userID")   +"'" 
				+ ",'"+ psTYPE_PHONE  +"'"
                + ");"
                + "end;";
		System.out.println(s);
        result = this.reqValue("", s);
		
		
		return result;
    } 
	
	public String del_ctkm(	 
				String psid 
				
				       )
		throws Exception {
			String result =null;
				
		String s = " begin ? := admin_v2.CONFIG_CTKM.del_ctkm("
				+ "'"+ psid +"'"
				+ ",'"+ getUserVar("userID")   +"'" 
                + ");"
                + "end;";
		System.out.println(s);
        result = this.reqValue("", s);
		
		
		return result;
    }
	
	public String edit_ctkm(
				String psID	,
				String psLEVEL_NAME      ,
				String psVALUE_MIN      ,
				String psVALUE_MAX         ,
				String psTYPE_MSISDN        ,
				String psGROUP_PKG           ,
				String psSTATUS              ,
				String psTYPE_PHONE   
				       )
		throws Exception {
			String result =null;
			String loai ="";
				
		String s = " begin ? := admin_v2.CONFIG_CTKM.edit_ctkm("
                + "'" + psID + "'"
                + ",'" + psLEVEL_NAME + "'"
				+ ",'"+ psVALUE_MIN + "'"
                + ",'"+ psVALUE_MAX + "'" 
                + ",'"+ psTYPE_MSISDN +"'"
				+ ",'"+ psGROUP_PKG +"'"
				+ ",'"+ psSTATUS +"'"
				+ ",'"+ getUserVar("userID")   +"'" 
				+ ",'"+ psTYPE_PHONE  +"'"
                + ");"
                + "end;";
		System.out.println(s);
        result = this.reqValue("", s);
		
		
		return result;
    } 
	
	public String seach_dispacth(	 
				String pskey 
				
				       )
		throws Exception {
			String result =null;
				
		String s = " begin ? := admin_v2.CONFIG_CTKM.search_dispatch("
				+ "'"+ pskey +"'"
                + ");"
                + "end;";
		System.out.println(s);
        result = this.reqValue("", s);
		
		
		return result;
    }
	
	public String get_package_name(	 
				String psdispatch 
				
				       )
		throws Exception {
			String result =null;
				
		String s = " begin ? := admin_v2.CONFIG_CTKM.get_package_name("
				+ "'"+ psdispatch +"'"
                + ");"
                + "end;";
		System.out.println(s);
        result = this.reqValue("", s);
		
		
		return result;
    }
	public String add_new_ctkm_sort(	 
				String psLIMIT_DATA  ,
				String psID_LEVEL  ,
				String psPACKAGE_ID  ,
				String psDESCRIPTION  ,
				String psDISPATCH_ID  ,
				String psPACKAGE_FEE   
				       )
		throws Exception {
			String result =null;
			String loai ="";
				
		String s = " begin ? := admin_v2.CONFIG_CTKM.add_ctkm_sort("
                + "'" + psLIMIT_DATA + "'"
				+ ",'"+ psID_LEVEL + "'"
                + ",'"+ psPACKAGE_ID +"'"
				+ ",'"+ ConvertFont.UtoD(psDESCRIPTION) +"'"
				+ ",'"+ psDISPATCH_ID +"'"
				+ ",'"+ psPACKAGE_FEE  +"'"
				+ ",'"+ getUserVar("userID")   +"'" 
                + ");"
                + "end;";
		System.out.println(s);
        result = this.reqValue("", s);
		
		
		return result;
    }
	
	public String edit_ctkm_sort(	 
				String psSTT  ,
				String psLIMIT_DATA  ,
				String psID_LEVEL  ,
				String psPACKAGE_ID  ,
				String psDESCRIPTION  ,
				String psDISPATCH_ID  ,
				String psPACKAGE_FEE   
				       )
		throws Exception {
			String result =null;
			String loai ="";
				
		String s = " begin ? := admin_v2.CONFIG_CTKM.edit_ctkm_sort("
                + "'" + psSTT + "'"
				+ ",'"+ psLIMIT_DATA + "'"
				+ ",'"+ psID_LEVEL + "'"
                + ",'"+ psPACKAGE_ID +"'"
				+ ",'"+ psDESCRIPTION +"'"
				+ ",'"+ psDISPATCH_ID +"'"
				+ ",'"+ psPACKAGE_FEE  +"'"
				+ ",'"+ getUserVar("userID")   +"'" 
                + ");"
                + "end;";
		System.out.println(s);
        result = this.reqValue("", s);
		
		
		return result;
    }
	public String del_ctkm_sort(	
				String id_level,
				String psid 
				
				       )
		throws Exception {
			String result =null;
				
		String s = " begin ? := admin_v2.CONFIG_CTKM.del_ctkm_sort("
				+ "'"+ id_level +"'"
				+ ",'"+ psid +"'"
				+ ",'"+ getUserVar("userID")   +"'" 
                + ");"
                + "end;";
		System.out.println(s);
        result = this.reqValue("", s);
		
		
		return result;
    }
	
	public String add_ctkm_config_nc(	 
				String psPACKAGE_FEE_DST  ,
				String psDESCRIPTION_DST  ,
				String psPACKAGE_NAME_SRC  ,
				String psPACKAGE_NAME_DST  ,
				String psSTATUS   
				       )
		throws Exception {
			String result =null;
			String loai ="";
				
		String s = " begin ? := admin_v2.CONFIG_CTKM.add_ctkm_config_nc("
                + "'" + psPACKAGE_FEE_DST + "'"
				+ ",'"+ ConvertFont.UtoD(psDESCRIPTION_DST) + "'"
                + ",'"+ psPACKAGE_NAME_SRC + "'" 
                + ",'"+ psPACKAGE_NAME_DST +"'"
				+ ",'"+ psSTATUS +"'"
				+ ",'"+ getUserVar("userID")   +"'" 
                + ");"
                + "end;";
		System.out.println(s);
        result = this.reqValue("", s);
		
		
		return result;
    } 
	
	public String edit_ctkm_config_nc(	 
				String psSTT  ,
				String psPACKAGE_FEE_DST  ,
				String psDESCRIPTION_DST  ,
				String psPACKAGE_NAME_SRC  ,
				String psPACKAGE_NAME_DST  ,
				String psSTATUS   
				       )
		throws Exception {
			String result =null;
			String loai ="";
				
		String s = " begin ? := admin_v2.CONFIG_CTKM.edit_ctkm_config_nc("
                + "'" + psSTT + "'"
                + ",'" + psPACKAGE_FEE_DST + "'"
				+ ",'"+ ConvertFont.UtoD(psDESCRIPTION_DST )+ "'"
                + ",'"+ psPACKAGE_NAME_SRC + "'" 
                + ",'"+ psPACKAGE_NAME_DST +"'"
				+ ",'"+ psSTATUS +"'"
				+ ",'"+ getUserVar("userID")   +"'" 
                + ");"
                + "end;";
		System.out.println(s);
        result = this.reqValue("", s);
		
		
		return result;
    } 
	
	public String del_ctkm_config_nc(	 
				String psid 
				
				       )
		throws Exception {
			String result =null;
				
		String s = " begin ? := admin_v2.CONFIG_CTKM.del_ctkm_config_nc("
				+ "'"+ psid +"'"
				+ ",'"+ getUserVar("userID")   +"'" 
                + ");"
                + "end;";
		System.out.println(s);
        result = this.reqValue("", s);
		
		
		return result;
    }
	
	
	public String upsort(	 
				String psid_level,
				String pstt ,
				String psort_by 
				
				       )
		throws Exception {
			String result =null;
				
		String s = " begin ? := admin_v2.CONFIG_CTKM.up_sort("
				+ "'"+ psid_level +"'"
				+ ",'"+ pstt +"'"
				+ ",'"+ psort_by   +"'" 
                + ");"
                + "end;";
		System.out.println(s);
        result = this.reqValue("", s);
		
		
		return result;
    }

	public String sortdown(	 
				String psid_level,
				String pstt ,
				String psort_by 
				
				       )
		throws Exception {
			String result =null;
				
		String s = " begin ? := admin_v2.CONFIG_CTKM.down_sort("
				+ "'"+ psid_level +"'"
				+ ",'"+ pstt +"'"
				+ ",'"+ psort_by   +"'" 
                + ");"
                + "end;";
		System.out.println(s);
        result = this.reqValue("", s);
		
		
		return result;
    }
	
	
	  
	
}
