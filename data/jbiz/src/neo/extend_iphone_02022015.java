package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class extend_iphone extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("DataList");		
	}
	
	public String layls_giahan_iphone(String psSoTB
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/giahan_iphone/ajax_ls_giahan"
			+"&"+CesarCode.encode("so_tb")+"="+psSoTB;
	}	
		
	public  String laytt_tbgiahan(	String psFuncSchema
									  ,	String psDataSchema										
									  , String psUserID
									  , String psSoTB								  
									  )
	{
		
		String s = "begin ?:=admin_v2.pttb_chucnang.laytt_giahan_iphone('"+psDataSchema+"'"
																+",'"  +psUserID+"'" 
																+",'"  +psSoTB+"'" 																																
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }  
	  
	public  String giahan_iphone(String psFuncSchema
									  ,	String psSoTB
									  , String psMaHD
									  , String psTenKH
									  , String psDiachi
									  , String psLoaiGT
									  , String psSoGT
									  , String psTenLK
									  , String psSoIMEI
									  , String psNgayHM
									  , String psTienGH
									  , String psGhichu
									  , String psGoicuoc
									  , String psUserID
									  , String psUserIP
									  , String psDataSchema								  
									  )
	{
		
		String s = "begin ?:=  admin_v2.pttb_chucnang.giahan_iphone('"+psSoTB+"'"
																+",'"  +psMaHD+"'" 
																+",'"  +psTenKH+"'" 																
																+",'"  +psDiachi+"'" 
																+",'"  +psLoaiGT+"'"
																+",'"  +psSoGT+"'"
																+",'"  +psTenLK+"'"
																+",'"  +psSoIMEI+"'"
																+",'"  +psNgayHM+"'"
																+","   +psTienGH+""
																+",'"  +psGhichu+"'"
																+",'"  +psGoicuoc+"'"
																+",'"  +psUserID+"'"
																+",'"  +psUserIP+"'"
																+",'"  +psDataSchema+"'"
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }
	public  String huy_giahan_iphone(String psFuncSchema
									  ,	String psSoTB
									  , String psGhichu
									  , String psUserID
									  , String psUserIP									  
									  )
	{
		
		String s = "begin ?:="+psFuncSchema+"pttb_ccbs.huy_giahan_iphone('"+psSoTB+"'"
																+",'"  +psGhichu+"'"
																+",'"  +psUserID+"'"
																+",'"  +psUserIP+"'"
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }	
	//--------------------iTouch
	public String layls_can_itouch(String psSoTB
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/giahan_iphone/ajax_lscan_itouch"
			+"&"+CesarCode.encode("so_tb")+"="+psSoTB;
	}	
		
	public  String laytt_can_itouch(	String psFuncSchema
									  ,	String psDataSchema										
									  , String psUserID
									  , String psSoTB								  
									  )
	{
		
		String s = "begin ?:="+psFuncSchema+"pttb_ccbs.laytt_can_itouch('"+psDataSchema+"'"
																+",'"  +psUserID+"'" 
																+",'"  +psSoTB+"'" 																																
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }  
	  
	public  String huygoi_itouch(String psFuncSchema
									  ,	String psSoTB
									  , String psMaHD
									  , String psTenKH
									  , String psDiachi
									  , String psLoaiGT
									  , String psSoGT
									  , String psTenLK
									  , String psSoIMEI
									  , String psNgayHM									 
									  , String psGhichu
									  , String psGoicuoc
									  , String psUserID
									  , String psUserIP
									  , String psDataSchema								  
									  )
	{
		
		String s = "begin ?:= admin_v2.pttb_chucnang.huygoi_itouch('"+psSoTB+"'"
																+",'"  +psMaHD+"'" 
																+",'"  +psTenKH+"'" 																
																+",'"  +psDiachi+"'" 
																+",'"  +psLoaiGT+"'"
																+",'"  +psSoGT+"'"
																+",'"  +psTenLK+"'"
																+",'"  +psSoIMEI+"'"
																+",'"  +psNgayHM+"'"																
																+",'"  +psGhichu+"'"
																+",'"  +psGoicuoc+"'"
																+",'"  +psUserID+"'"
																+",'"  +psUserIP+"'"
																+",'"  +psDataSchema+"'"
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }	
	public  String xoayc_can_itouch(String psFuncSchema
									  ,	String psSoTB									  
									  , String psUserID
									  , String psUserIP									  
									  )
	{
		
		String s = "begin ?:="+psFuncSchema+"pttb_ccbs.xoayc_can_itouch('"+psSoTB+"'"															
																+",'"  +psUserID+"'"
																+",'"  +psUserIP+"'"
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }	
	public  String guicmdv_can_itouch(String psFuncSchema
									  ,	String psSoTB
									  , String psMahd			
									  , String psUserID
									  , String psUserIP									  
									  )
	{
		
		String s = "begin ?:=admin_v2.pttb_chucnang.delete_iTouch('"+psSoTB+"'"																
																+",'"  +psMahd+"'"
																+",'"  +psUserID+"'"
																+",'"  +psUserIP+"'"
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }
	public  String extend_isurf(String psFuncSchema
									  ,	String psSoTB
									  , String psMahd
									  , String psUserID
									  , String psUserIP									  
									  )
	{
		
		String s = "begin ?:="+psFuncSchema+"pttb_ccbs.extend_iSurf('"+psSoTB+"'"
																+",'"  +psMahd+"'"
																+",'"  +psUserID+"'"
																+",'"  +psUserIP+"'"
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }
	  
	public  String reg_isurf(String psFuncSchema
									  ,	String psSoTB
									  , String psMahd
									  , String psUserID
									  , String psUserIP									  
									  )
	{
		
		String s = "begin ?:= admin_v2.pttb_chucnang.extend_iSurf_reg('"+psSoTB+"'"
																+",'"  +psMahd+"'"
																+",'"  +psUserID+"'"
																+",'"  +psUserIP+"'"
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }
	public  String detroy_itouch(String psFuncSchema
									  ,	String psSoTB
									  , String psMahd
									  , String psUserID
									  , String psUserIP									  
									  )
	{
		
		String s = "begin ?:="+psFuncSchema+"pttb_ccbs.delete_iTouch('"+psSoTB+"'"
																+",'"  +psMahd+"'"
																+",'"  +psUserID+"'"
																+",'"  +psUserIP+"'"
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }
	 public  String getSubscriber_tt(String psFuncSchema
									  ,	String psSoTB								  
									  )
	{
		
		String s = "begin ?:="+psFuncSchema+"pttb_ccbs.laytt_tt_iSurft('"+psSoTB+"'"																
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }
	  public  String getSubscriber_tt_itouch(String psFuncSchema
									  ,	String psSoTB								  
									  )
	{
		
		String s = "begin ?:="+psFuncSchema+"pttb_ccbs.laytt_tt_itouch('"+psSoTB+"'"																
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }
}