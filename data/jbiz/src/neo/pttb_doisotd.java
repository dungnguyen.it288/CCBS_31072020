package neo;
import neo.smartui.common.CesarCode;

import neo.smartui.process.*;
import neo.smartui.report.*;


public class pttb_doisotd
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb_doisotd was called");
  }

   public String layds_doisotd(String dichvuvt_id
								,String chkSoMay
								,String DoiTu
								,String DoiThanh
								) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/doiso_td/ajax_dssomay"
        +"&"+CesarCode.encode("dichvuvt_id")+"="+dichvuvt_id
		+"&"+CesarCode.encode("chksomay")+"="+chkSoMay
		+"&"+CesarCode.encode("doitu")+"="+DoiTu
		+"&"+CesarCode.encode("doithanh")+"="+DoiThanh
        ;
  }

  public String thuchien_doisotd(String schema
								,String psNgayDS
								,String pidichvuvt_id
								,String pichkSoMay
								,String psDoiTu
								,String psDoiThanh
								,String psUSERID
								) 

{
		String s =    "begin ? := "+CesarCode.decode(schema)+"PTTB_DULIEU.thuchien_doisotd('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+psNgayDS+"'"
								+",'"+pidichvuvt_id+"'"
								+",'"+pichkSoMay+"'"
								+",'"+psDoiTu+"'"
								+",'"+psDoiThanh+"'"
								+",'"+psUSERID+"'"
								+");"
				+"end;"
				;
			System.out.println(s);
			return s;

  }  
//----------------------------------------
}
