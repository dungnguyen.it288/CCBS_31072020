package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class common
    extends NEOProcessEx {
  public void run() {
    System.out.println("neo.common was called");
  }
	public String getUrlQuan(String tinhid) {
    return "/main?"+CesarCode.encode("configFile")+"=common/frmdiachi_ajax_quan"		
		+"&"+CesarCode.encode("tinhid")+"="+tinhid 
      ;
  }
  
  public String getUrlPhuong(String quanID ) {
    return "/main?"+CesarCode.encode("configFile")+"=common/frmdiachi_ajax_phuong"
		+"&"+CesarCode.encode("quan_id")+"="+quanID		
        ;
  }  
  
  public String getUrlPho(String phuongID) {
    return "/main?"+CesarCode.encode("configFile")+"=common/frmdiachi_ajax_pho"
        +"&"+CesarCode.encode("phuong_id")+"="+phuongID
        ;
  }
public NEOCommonData check_quyen_admin(String psuserid)
	{
		String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := ccs_admin.pttb.lay_role_tu_user('"+psuserid+"'); ";
		xs_ = xs_ + " end; ";
		
		NEOExecInfo ei_ = new NEOExecInfo(xs_);  			
		return new NEOCommonData(ei_);	
	} 
  
}

