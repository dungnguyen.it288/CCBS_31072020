package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_nvdiday
extends NEOProcessEx {
	public void run()
	{
		System.out.println("neo.pttb_hopdongcdlh was called");
	}
	public String getUrlnvdiday(String donviql_id)
	{
		return "/main?" + CesarCode.encode("configFile") + "=pttb/danhmuc/ajax_toql"
			+ "&" + CesarCode.encode("donviql_id") + "=" + donviql_id;			
	}

	public String capnhatnvdiday(String schema,
			String piNHANVIEN_ID
			, String psTEN_NV
			, String piTOQL_ID
			, String piINSERTORUPDATE 
		)
	{
		String s = "begin ? := " + CesarCode.decode(schema) + "PTTB_TEST_KIEN.capnhat_nhanvien_dd('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
			+ piNHANVIEN_ID
			+ ",'" + psTEN_NV + "'"
			+ "," + piTOQL_ID
			+ "," + piINSERTORUPDATE
			+ ");"
			+ "end;";			
		System.out.println(s);
		return s;		
	}
}