package neo;
import neo.smartui.SessionProcess;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

import neo.qlsp.*;

public class khoso_vtt extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("KHOSO_VTT");		
	}
	
	public String doc_layth_thuebao_vtt(
		String psSoTB,
		String psKieuso_ID,
		String psSubRange,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/dk_chonsokhotinh/ajax_th_thuebao"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("kieuso_id") + "=" + psKieuso_ID
			+ "&" + CesarCode.encode("sub_range") + "=" + psSubRange
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	
	public String doc_layds_thuebao_vtt(
		String psSoTB,
		String psKieuso_ID,
		String psSubRange,
		String psUserid,
		String psPageNum,
		String psPageRec
	)
	{
		try {
		  boolean blnCheck = SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"doc_layds_thuebao_vtt", getUserVar("userID"),
					getUserVar("sys_agentcode"),"15|1|30");
		  if(!blnCheck)
				return  "/main?"+CesarCode.encode("configFile")+"=admin/not-access";
		  reqValue("begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
							+psSoTB+"','doc_layds_thuebao_vtt'," 
							+"'"+psSoTB+";"+psKieuso_ID+";"+psSubRange+";"+psPageNum+";"+psPageRec+"','"
							+psUserid+"','"
							+getUserVar("userID")+"','"
							+getEnvInfo().getParserParam("sys_userip")+"','"
							+getUserVar("sys_agentcode")+"'); "
							+ "   commit; "
							+ " end; ");		
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		
		return "/main?" + CesarCode.encode("configFile")+"=pttb/dk_chonsokhotinh/ajax_ds_thuebao"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("kieuso_id") + "=" + psKieuso_ID
			+ "&" + CesarCode.encode("sub_range") + "=" + psSubRange
			+ "&" + CesarCode.encode("userid") + "=" + psUserid
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	
	public String chonso_value(
		 String psSoTB 
  		)
	{
		String s = "begin ? :=admin_v2.pk_store.chonso_thuebao('"+psSoTB+"','"+getUserVar("userID")+"','"+getEnvInfo().getParserParam("sys_userip")+"','"+getUserVar("sys_agentcode")+"'); end;";			
		//System.out.println("View"+s);
		return s;		
	}
	
	public String doc_layds_phieudk(
		String psUserid
    	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/dk_chonsokhotinh/ajax_ds_phieudk"
  			+ "&" + CesarCode.encode("userid") + "=" + psUserid;
	}
	
	public String doc_layds_thuebao_giahan(
		String psSoTB			
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/giahan_khotinh/ajax_ds_thuebao"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("userid") + "=" + getUserVar("userID")
			+ "&" + CesarCode.encode("userip") + "=" + getEnvInfo().getParserParam("sys_userip");  		
	}
	
	public String giahan_value(
		 String psSoTB, 
		 String psNgayGH 
  	)
	{
		String s = "begin ? :=admin_v2.pk_store.giahan_thuebao('"+psSoTB+"','"+psNgayGH+"','"+getUserVar("userID")+"','"+getEnvInfo().getParserParam("sys_userip")+"','"+getUserVar("sys_agentcode")+"'); end;";			
		//System.out.println("View"+s);
		return s;		
	}
	
	public String doc_layds_config(String userip)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/chonso/config_value/ajax_ds";  		
	}
	
	public String update_config_value(
		 String psID, 
		 String psValue,
		 String psUserIp
  	)
	{
		String s = "begin ? :=admin_v2.pk_store.update_config('"+psID+"','"+psValue+"','"+getUserVar("userID")+"','"+psUserIp+"'); end;";			
		System.out.println("View:"+s);
		return s;		
	}
	
	//Kho so 18001166
	public String doc_layds_thuebao_18001166(
		String psSoTB,
		String psKieuso_ID,
		String psSubRange,
		String psUserid,
		String psPageNum,
		String psPageRec
	)
	{
		try {
		  boolean blnCheck = SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"doc_layds_thuebao_18001166", getUserVar("userID"),
					getUserVar("sys_agentcode"),"15|1|30");
		  if(!blnCheck)
				return  "/main?"+CesarCode.encode("configFile")+"=admin/not-access";
		  reqValue("begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
							+psSoTB+"','doc_layds_thuebao_18001166'," 
							+"'"+psSoTB+";"+psKieuso_ID+";"+psSubRange+";"+psPageNum+";"+psPageRec+"','"
							+psUserid+"','"
							+getUserVar("userID")+"','"
							+getEnvInfo().getParserParam("sys_userip")+"','"
							+getUserVar("sys_agentcode")+"'); "
							+ "   commit; "
							+ " end; ");		
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		
		String return_="";
		String uid_ = getUserVar("userID");
		String sSoTB="";
		String sKieuSo="";		
		try {		
			Connect cl = new Connect();
			String sessionId =cl.readSession();	
				
			if (psSoTB.equals("")) sSoTB = "*";
			else sSoTB = psSoTB;
								
			if (psKieuso_ID.equals("")) sKieuSo ="-1";
			else sKieuSo = psKieuso_ID;
				
			return_= cl.searchIsdnLite(sessionId.trim(),sSoTB,"30900",sKieuSo,psSubRange,psPageNum,psPageRec,"1");						
			return_=return_;			
		} catch (Exception ex) {}	
		
		String s1="";
		String rq_=null;
		try {
			s1 = "begin ?:= admin_v2.pkg_chonso_18001166.chonso_tmp('"+return_+"','"+uid_+"','3'); end;";
			rq_=this.reqValue("",s1); 
		}catch (Exception ex){} 
		
		return "/main?" + CesarCode.encode("configFile")+"=pttb/chonsokho_18001166/ajax_ds_thuebao"
			+ "&" + CesarCode.encode("userid") + "=" + uid_;	
	}
	
	public String doc_layth_thuebao_18001166(		
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/chonsokho_18001166/ajax_th_thuebao"
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	
	public String chonso_value_18001166(
		 String psSoTB 
  		)
	{
		String s = "begin ? :=admin_v2.pkg_chonso_18001166.chonso_thuebao('"+psSoTB+"','"+getUserVar("userID")+"','"+getEnvInfo().getParserParam("sys_userip")+"','"+getUserVar("sys_agentcode")+"'); end;";					
		return s;		
	}
	
	public String doc_layds_phieudk_18001166(
		String psUserid
    	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/chonsokho_18001166/ajax_ds_phieudk"
  			+ "&" + CesarCode.encode("userid") + "=" + psUserid;
	}
	
	public String chuyenkho_18001166(String psListIsdn, String psProvince)
	{
		String s = "begin ? :=admin_v2.pkg_chonso_18001166.chuyenkho_rieng('"+psListIsdn+"','"+psProvince+"','"+getUserVar("userID")+"','"+getEnvInfo().getParserParam("sys_userip")+"','"+getUserVar("sys_agentcode")+"'); end;";					
		return s;		
	}
	
	public String doc_layds_tb_chuyenkho_18001166(
		String psSoTB,
		String psKieuso_ID,
		String psDauSo,
		String psKhoSo,
		String psUserid,
		String psPageNum,
		String psPageRec
	)
	{
		try {
		  boolean blnCheck = SessionProcess.addSession(getEnvInfo()
					.getParserParam("sys_userip").toString(),
					"doc_layds_tb_chuyenkho_18001166", getUserVar("userID"),
					getUserVar("sys_agentcode"),"15|1|30");
		  if(!blnCheck)
				return  "/main?"+CesarCode.encode("configFile")+"=admin/not-access";
		  reqValue("begin "
					+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
							+psSoTB+"','doc_layds_tb_chuyenkho_18001166'," 
							+"'"+psSoTB+";"+psKieuso_ID+";"+psDauSo+";"+psPageNum+";"+psPageRec+"','"
							+psUserid+"','"
							+getUserVar("userID")+"','"
							+getEnvInfo().getParserParam("sys_userip")+"','"
							+getUserVar("sys_agentcode")+"'); "
							+ "   commit; "
							+ " end; ");		
			
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		
		String return_="";
		String uid_ = getUserVar("userID");
		String sSoTB="";
		String sKieuSo="";		
		try {		
			Connect cl = new Connect();
			String sessionId =cl.readSession();	
				
			if (psSoTB.equals("")) sSoTB = "*";
			else sSoTB = psSoTB;
								
			if (psKieuso_ID.equals("")) sKieuSo ="-1";
			else sKieuSo = psKieuso_ID;
				
			return_= cl.searchIsdnLite(sessionId.trim(),sSoTB,psKhoSo,sKieuSo,psDauSo,psPageNum,psPageRec,"1");						
			return_=return_;			
		} catch (Exception ex) {}	
		
		String s1="";
		String rq_=null;
		try {
			s1 = "begin ?:= admin_v2.pkg_chonso_18001166.chonso_tmp('"+return_+"','"+uid_+"','4'); end;";
			rq_=this.reqValue("",s1); 
		}catch (Exception ex){} 
		
		return "/main?" + CesarCode.encode("configFile")+"=pttb/chuyenkho_18001166/ajax_ds_thuebao"
			+ "&" + CesarCode.encode("userid") + "=" + uid_;	
	}
	
	public String doc_layth_tb_chuyenkho_18001166(		
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/chuyenkho_18001166/ajax_th_thuebao"
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	
	public String chuyenkho_chung(String psListIsdn, String psProvince, String psStock)
	{
		String s = "begin ? :=admin_v2.pkg_chonso_18001166.chuyenkho_chung('"+psListIsdn+"','"+psProvince+"','"+psStock+"','"+getUserVar("userID")+"','"+getEnvInfo().getParserParam("sys_userip")+"','"+getUserVar("sys_agentcode")+"'); end;";					
		return s;		
	}
	
	public String chonso_value_chung(String psSoTB, String psKho)
	{
		String s = "begin ? :=admin_v2.pkg_chonso_18001166.chonso_thuebao_chung('"+psSoTB+"','"+psKho+"','"+getUserVar("userID")+"','"+getEnvInfo().getParserParam("sys_userip")+"','"+getUserVar("sys_agentcode")+"'); end;";			
		return s;		
	}
	
	public String doc_layds_phieudk_chung(String psUserid, String psKho)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/chuyenkho_18001166/ajax_ds_phieudk"
  			+ "&" + CesarCode.encode("userid") + "=" + psUserid
			+ "&" + CesarCode.encode("kho") + "=" + psKho;
	}
	public String lay_tttb_chonso(String psSoTB){
		String return_="";
		String uid_ = getUserVar("userID");
		
		if (psSoTB.equals("")) return 	"0";			
		try {		
			Connect cl = new Connect();
			String rs_ ="begin select nvl(max(session_id),0) into ? from neo.keepalive where ip=UTL_INADDR.get_host_address; end;";	
			String sessionId = null;			
			sessionId = reqValue("CMDVOracleDS", rs_);
			return_= cl.checkIsdn(sessionId.trim(),psSoTB);
			
		} catch (Exception ex) {
			
		}
		return return_;		
		
	}
	//End kho 18001166	
}