package neo.pttb_new;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import sun.misc.BASE64Encoder;
import java.util.*;
import javax.sql.*;

import java.text.SimpleDateFormat;
import java.net.*;
import java.io.*;
import neo.qlsp.*;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import javax.xml.soap.*;
//import java.util.concurrent.TimeUnit;



public class upload extends NEOProcessEx 
{
	

	
	public String add_whitelist_dktt(String dstb, String psloai, String pskenh, String ghichu, String userid)
	{
		
		String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
					
		//xs_ = xs_ + " 	?:= dktt.pkg_whitelist_dktt.add_whitelist_dktt_func('"+dstb+"','"+psloai+"','"+pskenh+"','"+ghichu+"','"+userid+"'); "; 
		xs_ = xs_ + " 	?:= prepaid.pkg_whitelist_dktt.add_whitelist_dktt_func('"+dstb+"','"+psloai+"','"+pskenh+"','"+ghichu+"','"+userid+"'); "; 
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("CMDVOracleDS",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
	
	return result;
	}
	
	
	public String remove_whitelist_dktt(String dstb, String psloai, String pskenh, String ghichu, String userid)
	{
		
		String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
					
		xs_ = xs_ + " 	?:= prepaid.pkg_whitelist_dktt.remove_whitelist_dktt_func('"+dstb+"','"+psloai+"','"+pskenh+"','"+ghichu+"','"+userid+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("CMDVOracleDS",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
	return result;
	
	}
	
	public String  accept_whitelist_dktt(String uploadid, String userid)

	{
		
		String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
					
		xs_ = xs_ + " 	?:= admin_v2.pkg_upload.accept_upload_whitelist_dktt('"+uploadid+"','"+userid+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
	return result;
	}
	
	
}
