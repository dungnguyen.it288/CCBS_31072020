package neo.pttb_new;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import sun.misc.BASE64Encoder;
import java.util.*;
import javax.sql.*;

import java.text.SimpleDateFormat;
import java.net.*;
import java.io.*;
import neo.qlsp.*;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import javax.xml.soap.*;
//import java.util.concurrent.TimeUnit;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import neo.smartui.common.CesarCode;
import com.svcon.jdbf.*;
import javax.sql.RowSet;
import sun.misc.*;

public class multisim extends NEOProcessEx 
{
	
	public String apiJsonPost(String input,String method) {
		String urlHttp = "http://10.156.4.86:9090/msim/g/multisim/"+method;
		StringBuffer buf = new StringBuffer();
        try {        
            URL u = new URL(urlHttp);
            HttpURLConnection con = (HttpURLConnection)u.openConnection();
            con.setConnectTimeout(5000); 
            
            con.setRequestProperty("Content-Type","application/json;charset=utf-8");        
            con.setDoOutput(true);
            con.setDoInput(true);
            con.connect();
            
            OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
            writer.write(input);
            writer.flush();
            BufferedReader  reader ;
            if(con.getResponseCode() == 200){
              reader = new BufferedReader(new
              InputStreamReader(con.getInputStream()));
            }else{
              reader = new BufferedReader(new
              InputStreamReader(con.getErrorStream()));
            }            
            String line ;
            while( (line = reader.readLine()) != null )
            buf.append(line);
            
            reader.close();
            String resp= buf.toString();          
        
			return resp;

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}

	}
public String apiPost(String input) {
		String urlHttp = "http://10.211.26.247:9393/api/registerAccount";
		try {
			byte[] postData = input.getBytes("UTF-8");
			//byte[] postData = input.getBytes();
			int postDataLength = postData.length;
			// URL url = new URL(urlHttp + method);
			URL url = new URL(urlHttp );
			HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
			httpcon.setDoOutput(true);

			// add reuqest header
			httpcon.setRequestMethod("POST");
			httpcon.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			httpcon.setRequestProperty("charset", "utf-8");
			httpcon.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			// httpcon.setUseCaches( false );
			OutputStream os = httpcon.getOutputStream();
			os.write(postData);
			//os.write(input.getBytes());
			os.flush();
			// Send post request
			int responseCode = httpcon.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + urlHttp);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(httpcon.getInputStream()));
			String inputLine;
			String resp = "";
			while ((inputLine = in.readLine()) != null) {
				resp += (inputLine);
			}
			System.out.println("Response  : " + resp);
			in.close();
			// String kq =getValue(resp,"SYORI_STS");
			// String id_member =getValue(resp,"KAIIN_NO");
			// String out = kq+":"+id_member;
			return resp;

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}

	}
	
	
	public String msim_create_volte(String msisdn,String imsi, String subtype,String psimsi1,String psimsi2, String psimsi3,String msisdn1,String msisdn2,String msisdn3)
	
	{
	try{
		String body ="{\"msisdn\":\""+msisdn+"\",\"imsi\": \""+imsi+"\",\"sub_type\":\""+subtype+"\",\"msisdn1\":\""+msisdn1.trim()+"\",\"imsi1\":\""+psimsi1.trim()+"\",\"msisdn2\":\""+msisdn2.trim()+"\",\"imsi2\":\""+psimsi2.trim()+"\",\"msisdn3\":\""+msisdn3.trim()+"\",\"imsi3\":\""+psimsi3.trim()+"\",\"scheme\":\"ccbs\",\"profile\":\"testing\"}";
		  System.out.println("body:"+body);
		String api_reg = apiJsonPost(body,"msim_create");
				System.out.println(api_reg);
				JSONParser parser = new JSONParser();
				JSONObject obj = (JSONObject) parser.parse(api_reg);
				String errorcode =(String) obj.get("error_code");
				if(errorcode.equals("1200"))
				{
				JSONObject resObj = (JSONObject) obj.get("result");
				String resultCode = (String) resObj.get("error_id");
				String resultMsg = (String) resObj.get("error_msg");
				  if(resultCode.equals("0"))
				  {
					  
					  return "0";
				  }
				  else 
				  {
					  
					  return resultCode+":"+resultMsg; 
				  }
				}
				else 
					return errorcode;
				
	}catch (Exception ex){				
			System.out.println("Exception 2 : " + ex.getMessage());
			ex.printStackTrace();
			String result=ex.getMessage();
			return result ;
		}	
		
	}
	
	public String msim_modify_volte(String msisdn,String imsi, String subtype,String psimsi1,String psimsi2, String psimsi3,String msisdn1,String msisdn2,String msisdn3, String type,String thuthu)
	
	{
		try{
			String l_imsi1="";
		String	l_imsi2="";
		String	l_imsi3="";
		String	l_msisdn1="";
		String	l_msisdn2="";
		String	l_msisdn3="";
		
	/*	if(type.equals("1")) 
		{
			if(psimsi1.equals("0")){l_imsi1="";}else {l_imsi1="+"+psimsi1;}
			if(psimsi2.equals("0")){l_imsi2="";}else {l_imsi2="+"+psimsi2;}
			if(psimsi3.equals("0")){l_imsi3="";}else {l_imsi3="+"+psimsi3;}
			if(msisdn1.equals("0")){l_msisdn1="";}else {l_msisdn1="+"+msisdn1;}
			if(msisdn2.equals("0")){l_msisdn2="";}else {l_msisdn2="+"+msisdn2;}
			if(msisdn3.equals("0")){l_msisdn3="";}else {l_msisdn3="+"+msisdn3;}
		}
		else 
		{
			if(psimsi1.equals("0")){l_imsi1="";}else {l_imsi1="-"+psimsi1;}
			if(psimsi2.equals("0")){l_imsi2="";}else {l_imsi2="-"+psimsi2;}
			if(psimsi3.equals("0")){l_imsi3="";}else {l_imsi3="-"+psimsi3;}
			if(msisdn1.equals("0")){l_msisdn1="";}else {l_msisdn1="-"+msisdn1;}
			if(msisdn2.equals("0")){l_msisdn2="";}else {l_msisdn2="-"+msisdn2;}
			if(msisdn3.equals("0")){l_msisdn3="";}else {l_msisdn3="-"+msisdn3;}
		}
		*/
		
		if(type.equals("1")) 
		{
			if(thuthu.equals("1")){l_imsi1="+"+psimsi1;l_msisdn1="+"+msisdn1;} else {l_imsi1=psimsi1;l_msisdn1=msisdn1;}
			if(thuthu.equals("2")){l_imsi2="+"+psimsi2;l_msisdn2="+"+msisdn2;} else {l_imsi2=psimsi2;l_msisdn2=msisdn2;}
			if(thuthu.equals("3")){l_imsi3="+"+psimsi3;l_msisdn3="+"+msisdn3;}else{l_imsi3=psimsi3;l_msisdn3=msisdn3;}
			
		}
		else 
		{
			if(thuthu.equals("1")){l_imsi1="-"+psimsi1;l_msisdn1="-"+msisdn1;} else {l_imsi1=psimsi1;l_msisdn1=msisdn1;}
			if(thuthu.equals("2")){l_imsi2="-"+psimsi2;l_msisdn2="-"+msisdn2;} else {l_imsi2=psimsi2;l_msisdn2=msisdn2;}
			if(thuthu.equals("3")){l_imsi3="-"+psimsi3;l_msisdn3="-"+msisdn3;}else{l_imsi3=psimsi3;l_msisdn3=msisdn3;}
		}
		
		
		String body ="{\"msisdn\": \""+msisdn+"\", \"imsi\": \""+imsi+"\", \"sub_type\": \""+subtype+"\", \"msisdn1\": \""+l_msisdn1.trim()+"\", \"imsi1\": \""+l_imsi1.trim()+"\", \"msisdn2\":\""+l_msisdn2.trim()+"\", \"imsi2\": \""+l_imsi2.trim()+"\", \"msisdn3\": \""+l_msisdn3.trim()+"\", \"imsi3\": \""+l_imsi3.trim()+"\", \"scheme\": \"ccbs\", \"profile\": \"testing\"}";	
	    System.out.println("body:"+body);
		String api_reg = apiJsonPost(body,"msim_modify");
				System.out.println(api_reg);
				JSONParser parser = new JSONParser();
				JSONObject obj = (JSONObject) parser.parse(api_reg);
				String errorcode =(String) obj.get("error_code");
				if(errorcode.equals("1200"))
				{
				JSONObject resObj = (JSONObject) obj.get("result");
				String resultCode = (String) resObj.get("error_id");
				String resultMsg = (String) resObj.get("error_msg");
				  if(resultCode.equals("0"))
				  {
					  
					  return "0";
				  }
				  else 
				  {
					  
					  return resultCode+":"+resultMsg; 
				  }
				}
				else 
					return errorcode;
				
		}
		catch (Exception ex){				
			System.out.println("Exception 2 : " + ex.getMessage());
			ex.printStackTrace();
			String result=ex.getMessage();
			return result ;
		}	
		
	}
	public String msim_delete_all_volte(String msisdn,String imsi, String subtype,String psimsi1,String psimsi2, String psimsi3,String msisdn1,String msisdn2,String msisdn3)
	
	{
			String l_imsi1="";
		String	l_imsi2="";
		String	l_imsi3="";
		String	l_msisdn1="";
		String	l_msisdn2="";
		String	l_msisdn3="";
		
		if(psimsi1.equals("0")){l_imsi1="";}else {l_imsi1=psimsi1;}
			if(psimsi2.equals("0")){l_imsi2="";}else {l_imsi2=psimsi2;}
			if(psimsi3.equals("0")){l_imsi3="";}else {l_imsi3=psimsi3;}
			if(msisdn1.equals("0")){l_msisdn1="";}else {l_msisdn1=msisdn1;}
			if(msisdn2.equals("0")){l_msisdn2="";}else {l_msisdn2=msisdn2;}
			if(msisdn3.equals("0")){l_msisdn3="";}else {l_msisdn3=msisdn3;}
			
			
		try{
		String body ="{\"msisdn\": \""+msisdn+"\", \"imsi\": \""+imsi+"\", \"sub_type\": \""+subtype+"\", \"msisdn1\": \""+l_msisdn1+"\", \"imsi1\": \""+l_imsi1+"\", \"msisdn2\":\""+l_msisdn2+"\", \"imsi2\": \""+l_imsi2+"\", \"msisdn3\": \""+l_msisdn3+"\", \"imsi3\": \""+l_imsi3+"\", \"scheme\": \"ccbs\", \"profile\": \"testing\"}";
		System.out.println(body);
		String api_reg = apiJsonPost(body,"msim_delete_all");
				System.out.println(api_reg);
				JSONParser parser = new JSONParser();
				JSONObject obj = (JSONObject) parser.parse(api_reg);
				String errorcode =(String) obj.get("error_code");
				if(errorcode.equals("1200"))
				{
				JSONObject resObj = (JSONObject) obj.get("result");
				String resultCode = (String) resObj.get("error_id");
				String resultMsg = (String) resObj.get("error_msg");
				  if(resultCode.equals("0"))
				  {
					  
					  return "0";
				  }
				  else 
				  {
					  
					  return resultCode+":"+resultMsg; 
				  }
				}
				else 
					return errorcode;
				
		
		}
			catch (Exception ex){				
			System.out.println("Exception 2 : " + ex.getMessage());
			ex.printStackTrace();
			String result=ex.getMessage();
			return result ;
		}	
	}
	
	public String log_msim_volte(String msisdn, String imsi1,String imsi2,String imsi3,String msisdn1,String msisdn2,String msisdn3,String method, String error, String active,String psthutu)
	{
		
		String result="";
		String xs_ = "";
		String channel = "ccbs";
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		String uid_ = getUserVar("userID");
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_multisim.log_msim_volte('"+msisdn+"','"+imsi1+"','"+imsi2+"','"+imsi3+"','"+msisdn1+"','"+msisdn2+"','"+msisdn3+"','"+method+"','"+uid_+"','"+matinh_ngdung_+"','"+error+"','"+active+"','"+psthutu+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
		return result; 
	}
	
	public String khoitao(String psmsisdn, String psimsi1, String psimsi2, String psimsi3, String psuserid, String psagent,String kenh,String phuongthuc)
	{
		
		String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_multisim.khoitao_imsi_func('"+psmsisdn+"','"+psimsi1+"','"+psimsi2+"','"+psimsi3+"','"+psuserid+"','"+psagent+"','"+kenh+"','"+phuongthuc+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
		return result; 
	}
	public String getinfosim(String pssosim)
	{
		
		String result="";
		String xs_ = "";
		String channel = "ccbs";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_multisim.get_info_sim('"+pssosim+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
		return result; 
	}

	public String huyall(String psmsisdn, String psuserid)
	{
		
		String result="";
		String xs_ = "";
		String channel = "ccbs";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_multisim.huy_all_func('"+psmsisdn+"','"+psuserid+"','"+channel+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
		return result; 
	}
	public String in_xuat_tttgc_doisim(String sotb)
	{
		
		String result="";
		String xs_ = "";
		String channel = "ccbs";
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		String uid_ = getUserVar("userID");
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= ADMIN_V2.pkg_multisim.xuat_tttgc_doisim('CCS_"+matinh_ngdung_+".','"+matinh_ngdung_+"','"+uid_+"','','"+sotb+"','ADD'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
		return result; 
	}
	
	
	public String doi_sim(String sotb,String simmoi,String ghichu,String esim)
	{
		
	
		String result="";
		String subresult ="";
		String xs_ = "";
		String channel = "ccbs";
	
		String userid = getUserVar("userID");
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= neo.pkg_doisim_multisim.doi_sim_tb('"+sotb+"','"+simmoi+"','0','"+ghichu+"','"+userid+"','"+matinh_ngdung_+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("CMDVOracleDS",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
	
		if(result.equals("2"))
		{
				// cap nhat lai sim trong bang dangky_multisim
			 xs_ =  " begin ";		
			xs_ = xs_ + " 	?:= admin_v2.pkg_multisim.update_sim('"+sotb+"','"+simmoi+"','Doi sim','"+userid+"','"+matinh_ngdung_+"','"+esim+"'); "; // code cho QLSP				
			xs_ = xs_ + " end; ";
			
			try
			{		    
				 subresult=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       subresult = ex.getMessage();
			}
			
		}
		
			
		return result; 
	}
	
	public String chkSMOI(String sotb,String simmoi)
	{
		
		String result="";
		String xs_ = "";
		String channel = "ccbs";
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= neo.pkg_doisim_multisim.ktra_E_SIM_moi('"+sotb+"','"+simmoi+"','"+matinh_ngdung_+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("CMDVOracleDS",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
		return result; 
	}
	public String huy_imsi(String psmsisdn, String psimsi, String psuserid)
	{
		
		String result="";
		String xs_ = "";
		String channel = "ccbs";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_multisim.huy_imsi_func('"+psmsisdn+"','"+psimsi+"','"+psuserid+"','"+channel+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
		return result; 
	}
	
	public String get_thutu_imsi(String psmsisdn, String thutu)
	{
		
		String result="";
		String xs_ = "";
		String channel = "ccbs";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_multisim.get_thutu_msisdn('"+psmsisdn+"','"+thutu+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
		return result; 
	}
	
	public String xuathddt(String psmsisdn, String sotien, String thu_tu_1, String thu_tu_2, String thu_tu_3, String userid, String userip)
	{
		
		String result="";
		String xs_ = "";
		String matinh_ngdung_ = getUserVar("sys_agentcode");
		String dataschema="CCS_"+matinh_ngdung_+".";
		String action  = "ADD";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_multisim.xuat_tttgc('"+dataschema+"','"+matinh_ngdung_+"','"+userid+"','"+userip+"','"+psmsisdn+"','"+sotien+"','"+thu_tu_1+"','"+thu_tu_2+"','"+thu_tu_3+"','"+action+"'); ";// code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
		return result; 
	}
	
	public String check_valid_msisdn(String psmsisdn, String psagent)
	{
		
		String result="";
		String xs_ = "";
		String channel = "ccbs";
		xs_ = xs_ + " begin ";		
		xs_ = xs_ + " 	?:= admin_v2.pkg_multisim.check_valid_msisdn('"+psmsisdn+"','"+psagent+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
		return result; 
	}



}
