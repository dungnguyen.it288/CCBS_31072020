package neo.pttb_new;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import sun.misc.BASE64Encoder;
import java.util.*;
import javax.sql.*;

import java.text.SimpleDateFormat;
import java.net.*;
import java.io.*;
import neo.qlsp.*;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import javax.xml.soap.*;
//import java.util.concurrent.TimeUnit;



public class pttb extends NEOProcessEx 
{
	
	public String open_lock_user(String userid, String ghichu, String useropen, String typeid)
	{
		
		String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
					
		xs_ = xs_ + " 	?:= admin_v2.open_lock_user('"+userid+"','"+ghichu+"','"+typeid+"','"+useropen+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
		return result; 
	}
	
	public String update_info(String tranid, String id, String agent,String so_tb, String madv,String mabc,String diachi,String ngayhen,String userid,String status,String status_name,String ghichu,String channel)
	{
		
		String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
					
		xs_ = xs_ + " 	?:= admin_v2.pkg_api_pttb.update_request_func('"+id+"','"+so_tb+"','"+agent+"','"+madv+"','"+mabc+"','"+diachi+"','"+ngayhen+"','"+userid+"','"+status+"','"+ghichu+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
	
	if(result.equals("2"))
	{
	  // goi sang api
	  SimpleDateFormat fmtDate = new SimpleDateFormat("dd/MM/yyyy");
		  Date d = new Date();
		  String ngaycap  = fmtDate.format(d);
		  String Host="10.149.34.226";
			String Port="80";
			String Uri="Service/ServiceOutBound.asm";
			String method="UpdateTrangThaiOB";
		String Request="<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tem=\"http://tempuri.org/\">"+
					   "<soap:Header/>"+
					   "<soap:Body>"+
						 " <tem:UpdateTrangThaiOB>"+
							
							" <tem:prms>"+
							
							"	<tem:UserName>ccbsob</tem:UserName>"+
							
							"	<tem:PassWord>ccbsob@abc123</tem:PassWord>"+
							"	<tem:PhanViecId>"+tranid+"</tem:PhanViecId>"+
							"	<tem:TrangThaiId>"+status+"</tem:TrangThaiId>"+
							
							"	<tem:MoTaChiTiet>"+status_name+"</tem:MoTaChiTiet>"+
							
							"	<tem:NgayCapNhat>"+ngaycap+"</tem:NgayCapNhat>"+
							
							"	<tem:GhiChu>"+ghichu+"</tem:GhiChu>"+
							" </tem:prms>"+
						 " </tem:UpdateTrangThaiOB>"+
					  " </soap:Body>"+
					"</soap:Envelope>";
			
			StringBuffer buf = new StringBuffer();	    
		    try {
		    	URL url = new URL("http://10.149.34.226:8081/Service/ServiceOutBound.asmx");
					
		        URLConnection urlc = url.openConnection();
		        HttpURLConnection conn = (HttpURLConnection)urlc;
		       // urlc.setConnectTimeout(8000);			
		        conn.setRequestProperty("Content-Length", String.valueOf(Request.length()));
		        conn.setRequestProperty("Content-Type","application/soap+xml;charset=utf-8");
		        conn.setRequestProperty("SOAPAction", "http://10.149.34.226:8081/Service/ServiceOutBound.asmx/UpdateTrangThaiOB");	  
		        conn.setRequestMethod("POST");			
		        conn.setDoOutput(true);
		        conn.setDoInput(true);
		        OutputStreamWriter writer = new OutputStreamWriter( conn.getOutputStream() );
		        writer.write( Request );
		        writer.flush();
		        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		        String res_line;
		        while ((res_line = in.readLine()) != null)
		            buf.append(res_line);
		        in.close();
		        result= buf.toString();
		    }catch (Exception e) {
		        e.printStackTrace();
		        result= e.getMessage();
		    }
		
        if (result.matches("(.*)<Code>1</Code>(.*)")) {

				return "1";

				
			} else {
				return "-1";
			}
	}
	else {
		return result; 
		
	}
	}
	
	
	public String update_info_full(String tranid, String id, String agent,String so_tb,String quanid,String phuongid,String sonha, String madv,String mabc,String diachi,String ngayhen,String userid,String status,String status_name,String ghichu,String channel, String noicap, String ngaycap)
	{
		
		String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
					
		xs_ = xs_ + " 	?:= admin_v2.pkg_api_pttb.update_request_full_func('"+id+"','"+so_tb+"','"+agent+"','"+quanid+"','"+phuongid+"','"+sonha+"','"+madv+"','"+mabc+"','"+diachi+"','"+ngayhen+"','"+userid+"','"+status+"','"+ghichu+"','"+noicap+"','"+ngaycap+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
	
	
	
		return result; 
		
	
	}
	
	public String layds_file_image(String id, String sotb){
		
	return "/main?"+CesarCode.encode("configFile")+"=pttb_new/pttb_econtract/receive_request/ajax_ds_file_Personal"
			+"&"+CesarCode.encode("id")+"="+id + "&"+CesarCode.encode("so_tb")+"="+sotb;
    }
			
	public String  update_info_econtract(
	String id, String so_tb, String agent, String tenKH, 
	String sodaidien, String dtlienhe, String gioitinh, String chucdanh, 
	String ngaysinh, String loaigt, String sogt, String noicap, 
	String ngaycap_gt, String loaigt1, String sogt1, String noicap1, 
	String ngaycap_gt1,String doituong, String doituongsd, String quocgia, 
	String goicuoc, String diachikh, String diachict, String tentt,
	String diadiemtt, String diachiTT, String diachibc, String thuphi, 
	String mucphi_id, String nganhang_id, String mst, String buucuc, 
	String donviql_id, String billinglimit, String month_live, String email, 
	String tttuyen, String manv, String maT, 
	String khdn, String ckhoan, String maqhns, String mabaocuoc, 
	String inchitiet, String kieubc, String loaikh, String nganhnghe, 
	String khlon, String nguoigt, String nguoikhd, String ghichu,
	String quanctid, String phuongctid, String phoctid, String sonhact, 
	String quanttid, String phuongttid, String phottid, String sonhatt,
	String userid
	)
	{
		
		String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
					
		xs_ = xs_ + " 	?:= ccs_admin.pkg_api_e_contract.update_info_econtract("+
		"'"+id+"','"+so_tb+"','"+agent+"','"+tenKH+"',"+
		"'"+sodaidien+"','"+dtlienhe+"','"+gioitinh+"','"+chucdanh+"',"+
		"'"+ngaysinh+"','"+loaigt+"','"+sogt+"','"+noicap+"',"+
		"'"+ngaycap_gt+"','"+loaigt1+"','"+sogt1+"','"+noicap1+"',"+
		"'"+ngaycap_gt1+"','"+doituong+"','"+doituongsd+"','"+quocgia+"',"+
		"'"+goicuoc+"','"+diachikh+"','"+diachict+"','"+tentt+"',"+
		"'"+diadiemtt+"','"+diachiTT+"','"+diachibc+"','"+thuphi+"',"+
		"'"+mucphi_id+"','"+nganhang_id+"','"+mst+"','"+buucuc+"',"+
		"'"+donviql_id+"','"+billinglimit+"','"+month_live+"','"+email+"',"+
		"'"+tttuyen+"','"+manv+"','"+maT+"','"+khdn+"','"+
		ckhoan+"','"+maqhns+"','"+mabaocuoc+"',"+"'"+inchitiet+
		"','"+kieubc+"','"+loaikh+"','"+nganhnghe+"',"+"'"+khlon+
		"','"+nguoigt+"','"+nguoikhd+"','"+ghichu+"',"+"'"+quanctid+
		"','"+phuongctid+"','"+phoctid+"','"+sonhact+"',"+"'"+quanttid+
		"','"+phuongttid+"','"+phottid+"','"+sonhatt+"',"+"'"+userid+"'); "; 			
		xs_ = xs_ + " end; ";
		System.out.println(xs_);
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
		return result; 
	}
	public String chuyen_trangthai(String id, String so_tb, String agent,String userchuyen, String userid, String trangthai)
	{
		
		String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
					
		xs_ = xs_ + " 	?:= ccs_admin.pkg_api_e_contract.chuyen_trangthai_func('"+id+"','"+so_tb+"','"+agent+"','"+userchuyen+"','"+userid+"','"+trangthai+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
	
	
	
		return result; 
		
	
	}
	
}
