package neo.pttb_new;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import sun.misc.BASE64Encoder;
import java.util.*;
import javax.sql.*;

import java.text.SimpleDateFormat;
import java.net.*;
import java.io.*;
import neo.qlsp.*;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import javax.xml.soap.*;


public class pttb_econtract extends NEOProcessEx 
{
	
	
public String layds_file_image(String id, String sotb){
		
	return "/main?"+CesarCode.encode("configFile")+"=pttb_new/pttb_econtract/receive_request/ajax_ds_file_Personal"
			+"&"+CesarCode.encode("id")+"="+id + "&"+CesarCode.encode("so_tb")+"="+sotb;
    }
			
	public String  update_info_econtract(
	String id, String so_tb, String agent, String tenKH, 
	String sodaidien, String dtlienhe, String gioitinh, String chucdanh, 
	String ngaysinh, String loaigt, String sogt, String noicap, 
	String ngaycap_gt, String loaigt1, String sogt1, String noicap1, 
	String ngaycap_gt1,String doituong, String doituongsd, String quocgia, 
	String goicuoc, String diachikh, String diachict, String tentt,
	String diadiemtt, String diachiTT, String diachibc, String thuphi, 
	String mucphi_id, String nganhang_id, String mst, String buucuc, 
	String donviql_id, String billinglimit, String month_live, String email, 
	String tttuyen, String manv, String maT, 
	String khdn, String ckhoan, String maqhns, String mabaocuoc, 
	String inchitiet, String kieubc, String loaikh, String nganhnghe, 
	String khlon, String nguoigt, String nguoikhd, String ghichu,
	String quanctid, String phuongctid, String phoctid, String sonhact, 
	String quanttid, String phuongttid, String phottid, String sonhatt,
	String userid
	)
	{
		
		String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
					
		xs_ = xs_ + " 	?:= ccs_admin.pkg_api_e_contract.update_info_econtract("+
		"'"+id+"','"+so_tb+"','"+agent+"','"+tenKH+"',"+
		"'"+sodaidien+"','"+dtlienhe+"','"+gioitinh+"','"+chucdanh+"',"+
		"'"+ngaysinh+"','"+loaigt+"','"+sogt+"','"+noicap+"',"+
		"'"+ngaycap_gt+"','"+loaigt1+"','"+sogt1+"','"+noicap1+"',"+
		"'"+ngaycap_gt1+"','"+doituong+"','"+doituongsd+"','"+quocgia+"',"+
		"'"+goicuoc+"','"+diachikh+"','"+diachict+"','"+tentt+"',"+
		"'"+diadiemtt+"','"+diachiTT+"','"+diachibc+"','"+thuphi+"',"+
		"'"+mucphi_id+"','"+nganhang_id+"','"+mst+"','"+buucuc+"',"+
		"'"+donviql_id+"','"+billinglimit+"','"+month_live+"','"+email+"',"+
		"'"+tttuyen+"','"+manv+"','"+maT+"','"+khdn+"','"+
		ckhoan+"','"+maqhns+"','"+mabaocuoc+"',"+"'"+inchitiet+
		"','"+kieubc+"','"+loaikh+"','"+nganhnghe+"',"+"'"+khlon+
		"','"+nguoigt+"','"+nguoikhd+"','"+ghichu+"',"+"'"+quanctid+
		"','"+phuongctid+"','"+phoctid+"','"+sonhact+"',"+"'"+quanttid+
		"','"+phuongttid+"','"+phottid+"','"+sonhatt+"',"+"'"+userid+"'); "; 			
		xs_ = xs_ + " end; ";
		System.out.println(xs_);
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
		return result; 
	}
	public String chuyen_trangthai(String id, String so_tb, String agent,String userchuyen, String userid, String trangthai)
	{
		
		String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
					
		xs_ = xs_ + " 	?:= ccs_admin.pkg_api_e_contract.chuyen_trangthai_func('"+id+"','"+so_tb+"','"+agent+"','"+userchuyen+"','"+userid+"','"+trangthai+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
	
	
	
		return result; 
		
	
	}
	public String xoa_user_buucuc(String id, String matinh, String donvi, String buucuc, String userid)
	{
		String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
					
		xs_ = xs_ + " 	?:= ccs_admin.pkg_api_e_contract.xoadl_user_bc_func('"+id+"','"+donvi+"','"+buucuc+"','"+userid+"','"+matinh+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
	
	
	
		return result; 
		
	}
	
	public String capnhat_user_buucuc(String donviql, String mabc, String admin, String pheduyet, String userid, String matinh)
	{
		String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
					
		xs_ = xs_ + " 	?:= ccs_admin.pkg_api_e_contract.capnhat_user_bc_func('"+donviql+"','"+mabc+"','"+admin+"','"+pheduyet+"','"+userid+"','"+matinh+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
	
	
	
		return result; 
		
	}
	
	public String view_hd(String link)
	{
		String link_new=link;
		
		return link_new;
		
	}
	
	public String themmoi_user_buucuc(String donviql, String mabc, String admin, String pheduyet, String userid, String matinh)
	{
		String result="";
		String xs_ = "";
		xs_ = xs_ + " begin ";		
					
		xs_ = xs_ + " 	?:= ccs_admin.pkg_api_e_contract.themmoi_user_buucuc_func('"+donviql+"','"+mabc+"','"+admin+"','"+pheduyet+"','"+userid+"','"+matinh+"'); "; // code cho QLSP				
		xs_ = xs_ + " end; ";
		
		try
			{		    
				 result=	this.reqValue("",xs_);
			 
			} 
		catch (Exception ex) 
		    {            	
		       result = ex.getMessage();
			}
	
	
	
		return result; 
		
	}
	
}