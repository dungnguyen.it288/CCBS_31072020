package neo;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSetMetaData;

import javax.sql.RowSet;

import neo.smartui.common.CesarCode;
import neo.smartui.process.NEOProcessEx;
import neo.smartui.report.NEOCommonData;
import neo.smartui.report.NEOExecInfo;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import cosaldap.Base64.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;
import java.io.*;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.ResultSetMetaData;
import javax.sql.RowSet;

public class ccbs_promotion extends NEOProcessEx {
  public void run() {
    System.out.println("neo.khuyenmai was called");
  }
  public String themmoi_km_congvans(String  schema
  							,String schemaCommon  																	  						   			        
					        ,String psten_congvan
							,String psngay_batdau
							,String psngay_kt
							,String pscon_hieuluc
							,String psnoidung
							,String pskmuutien
							,String psnguoi_cn
  							,String psmay_cn						
                            ,String psupload 
							,String psngay_kp
							,String psfix_promotion
							,String pscommitted_liquidate
							,String psnums_subscriber  
							,String pskmchung
							,String psagents
							)throws Exception 
	{
	String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:= promotions_km.themmoi_km_congvans('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")
		+"','"+ConvertFont.UtoD(psten_congvan)+"'"                      
        +","+psngay_batdau
        +","+psngay_kt
		+","+psngay_kp
        +",'"+pscon_hieuluc+"'"  
        +",'"+ConvertFont.UtoD(psnoidung)+"'"
        +",'"+pskmuutien+"'"
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
		+",'"+psupload+"'"
		+",'"+psfix_promotion+"'"
		+",'"+pscommitted_liquidate+"'"
		+",'"+psnums_subscriber+"'"
		+",'"+pskmchung+"'"
		+",'"+psagents+"'"
        +");"
        +"end;";	
			
		System.out.println(xs_);
		return this.reqValue("VNPBilling", xs_);		
		//return this.reqValue("", xs_);
	}
	///Cap nhat cong van Moi 
	public String capnhat_km_congvans(String  schema,
  							String  schemaCommon  
  							,String  pikmcv_id																  						   			        
					       	,String psten_congvan
							,String psngay_batdau
							,String psngay_kt
							,String pscon_hieuluc
							,String psnoidung
							,String pskmuutien
							,String  psnguoi_cn
  							,String  psmay_cn				
                            ,String psupload
							,String psngay_kp
							,String psfix_promotion
							,String pscommitted_liquidate
							,String psnums_subscriber
							,String pskmchung
							,String psagents
							) throws Exception
	{
	String xs_ = "";
					xs_ = xs_ + " begin ";
					xs_ = xs_ + " ? := "+CesarCode.decode(schemaCommon)+"promotions_km.capnhat_km_congvans('"+getUserVar("userID")
					+"','"+getUserVar("sys_agentcode")		
					+"','"+CesarCode.decode(schemaCommon)
					+"','"+getUserVar("sys_dataschema")
					+"',"+pikmcv_id+",'"
					+ConvertFont.UtoD(psten_congvan)+"'"                         
					+","+psngay_batdau
					+","+psngay_kt
					+","+psngay_kp
					+",'"+pscon_hieuluc+"'"  
					+",'"+ConvertFont.UtoD(psnoidung)+"'"
					+",'"+pskmuutien+"'"
					+",'"+psnguoi_cn+"'"
					+",'"+psmay_cn+"'"
					+",'"+psupload+"'"
					+",'"+psfix_promotion+"'"
					+",'"+pscommitted_liquidate+"'"
					+",'"+psnums_subscriber+"'"
					+",'"+pskmchung+"'"
					+",'"+psagents+"'"
					+"); ";
					xs_ = xs_ + " end; ";	
			
		System.out.println(xs_);			
		return this.reqValue("VNPBilling", xs_);
	}
	///Them moi hinh thuc
	public String themmoi_km_hinhthucs(String  schema
  							,String  schemaCommon
  							,String  pikmcv_id																	  						   			        
					        ,String  psten_hinhthuc
							,String psloaiht_id
							,String pscamket_sdlt
							,String pstinhtao_ds
							,String tienphaidong							
							,String kmchung
							,String chkHanmuc
							,String hanmuc
							,String psghichu
							,String psnguoi_cn
  							,String psmay_cn						
                            ,String psupload
							,String psFromdate
							,String psTodate
							,String PsEnable
  							,String psMoneyHM
							,String psMoneyCD
							,String psLoaiALO )throws Exception 
	{
	String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " ? := "+CesarCode.decode(schemaCommon)+"promotions_km.themmoi_km_hinhthucs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+pikmcv_id
		+",'"+ConvertFont.UtoD(psten_hinhthuc)+"'"
        +","+psloaiht_id
        +","+pscamket_sdlt
        +","+pstinhtao_ds        
        +","+tienphaidong        
        +","+kmchung     
        +","+chkHanmuc
        +","+hanmuc
		+",'"+ConvertFont.UtoD(psghichu)
		+"','"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
		+",'"+psupload+"'"
		+",'"+psFromdate+"'"
		+",'"+psTodate+"'"
		+",'"+PsEnable+"'"
		+",'"+psMoneyHM+"'"
		+",'"+psMoneyCD+"'"
		+",'"+psLoaiALO+"'"
        +");"
        +"end;";	
			
		System.out.println(xs_);
		return this.reqValue("VNPBilling", xs_);		
	}
	//////Cap nhat Hinh thuc
	public String capnhat_km_hinhthucs(String  schema
  							,String  schemaCommon 
  							,String  pikmcv_id  
  							,String  pikmht_id																	  						   			        
					        ,String  psten_hinhthuc
							,String psloaiht_id
							,String pscamket_sdlt
							,String pstinhtao_ds
							,String pstienphaidong
							,String pskmchung
							,String chkHanmuc
							,String hanmuc
							,String psghichu
							,String psnguoi_cn
  							,String psmay_cn
							,String psupload				
							,String psFromdate
							,String psTodate
							,String PsEnable
  							,String psMoneyHM
							,String psMoneyCD
							,String psLoaiALO) throws Exception
	{
	String xs_ = "";
					xs_ = xs_ + " begin ";
					xs_ = xs_ + " ? := "+CesarCode.decode(schemaCommon)+"promotions_km.capnhat_km_hinhthucs('"+getUserVar("userID")
								+"','"+getUserVar("sys_agentcode")		
								+"','"+CesarCode.decode(schemaCommon)
								+"','"+getUserVar("sys_dataschema")+"',"+pikmcv_id+","+pikmht_id                    
								+",'"+ConvertFont.UtoD(psten_hinhthuc)        
								+"',"+psloaiht_id
								+","+pscamket_sdlt
								+","+pstinhtao_ds
								+","+pstienphaidong
								+","+pskmchung
								+","+chkHanmuc
								+","+hanmuc
								+",'"+ConvertFont.UtoD(psghichu)+"'"
								+",'"+psnguoi_cn+"'"
								+",'"+psmay_cn+"'"
								+",'"+psupload+"'"
								+",'"+psFromdate+"'"
								+",'"+psTodate+"'"
								+",'"+PsEnable+"'"
								+",'"+psMoneyHM+"'"
								+",'"+psMoneyCD+"'"
								+",'"+psLoaiALO+"'"
								+"); ";
								xs_ = xs_ + " end; ";		
			
		System.out.println(xs_);			
		return this.reqValue("VNPBilling", xs_);
	}
	/////Xoa hinh thuc
	public String xoa_km_hinhthucs(String  schema,
  							String  schemaCommon,   
  							String  pikmcv_id,	  																					  						   			        
					       	String  pikmht_id) throws Exception
	{
	String xs_ = "";
					xs_ = xs_ + " begin ";
					xs_ = xs_ + " ? := "+CesarCode.decode(schemaCommon)+"promotions_km.xoa_km_hinhthucs('"+getUserVar("userID")
									+"','"+getUserVar("sys_agentcode")		
									+"','"+CesarCode.decode(schemaCommon)
									+"','"+getUserVar("sys_dataschema")+"',"+pikmcv_id
									+","+pikmht_id       
									+");"
									+"end;";				
		System.out.println(xs_);			
		return this.reqValue("VNPBilling", xs_);
	}
	
	///Them moi cach giam 
	 public String themmoi_km_cachgiams(String  schema
  							,String  schemaCommon 						
							,String sten_cachgiam							
							,String sso_thang
							,String sapdung_sauthanghm
							,String stien_kieuld1
							,String stien_kieuld2
							,String stien_kieuld3
							,String stien_kieuld4
							,String stien_phaitra
							,String sps_toithieu
							,String chk_novat_pstoithieu
							,String s_apdungkmtc_id
							,String skhoanmuctcs
							,String stienkm_duochuong
							,String stien_truocthue
							,String sghichu
							,String skmcv_id
							,String skmht_id
							,String shinhthuctt_id
							,String skieugiam_id	
							,String  psnguoi_cn
							,String  psmay_cn)throws Exception 
	{
	String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	? := "+CesarCode.decode(schemaCommon)+"promotions_km.themmoi_km_cachgiams('"+getUserVar("userID")
						+"','"+getUserVar("sys_agentcode")		
						+"','"+CesarCode.decode(schemaCommon)
						+"','"+getUserVar("sys_dataschema")+"'"
						+",'"+ConvertFont.UtoD(sten_cachgiam)
						+"','"+sso_thang
						+"','"+sapdung_sauthanghm
						+"','"+stien_kieuld1
						+"','"+stien_kieuld2
						+"','"+stien_kieuld3
						+"','"+stien_kieuld4
						+"','"+stien_phaitra
						+"','"+sps_toithieu
						+"','"+chk_novat_pstoithieu
						+"','"+s_apdungkmtc_id
						+"','"+skhoanmuctcs
						+"','"+stienkm_duochuong
						+"','"+stien_truocthue
						+"','"+ConvertFont.UtoD(sghichu)
						+"','"+skmcv_id
						+"','"+skmht_id
						+"','"+shinhthuctt_id
						+"','"+skieugiam_id	
						+"','"+psnguoi_cn+"'"
						+",'"+psmay_cn+"'"
						+");"
						+"end;";	
			
		System.out.println(xs_);
		return this.reqValue("VNPBilling", xs_);		
		//return this.reqValue("", xs_);
	}
	
	//////Cap nhat Cach giam
	public String capnhat_km_cachgiams(String  schema
  							,String  schemaCommon 
  							,String pikmcg_id						
							,String sten_cachgiam							
							,String sso_thang
							,String sapdung_sauthanghm
							,String stien_kieuld1
							,String stien_kieuld2
							,String stien_kieuld3
							,String stien_kieuld4
							,String stien_phaitra
							,String sps_toithieu
							,String chk_novat_pstoithieu
							,String s_apdungkmtc_id
							,String skhoanmuctcs
							,String stienkm_duochuong
							,String stien_truocthue
							,String sghichu
							,String skmcv_id
							,String skmht_id
							,String shinhthuctt_id
							,String skieugiam_id	
							,String  psnguoi_cn
							,String  psmay_cn) throws Exception
	{
	String xs_ = "";
					xs_ = xs_ + " begin ";
					xs_ = xs_ + " ? := "+CesarCode.decode(schemaCommon)+"promotions_km.capnhat_km_cachgiams('"+getUserVar("userID")
										+"','"+getUserVar("sys_agentcode")		
										+"','"+CesarCode.decode(schemaCommon)
										+"','"+getUserVar("sys_dataschema")
										+"',"+pikmcg_id                   
										+",'"+ConvertFont.UtoD(sten_cachgiam)
										+"','"+sso_thang
										+"','"+sapdung_sauthanghm
										+"','"+stien_kieuld1
										+"','"+stien_kieuld2
										+"','"+stien_kieuld3
										+"','"+stien_kieuld4
										+"','"+stien_phaitra
										+"','"+sps_toithieu
										+"','"+chk_novat_pstoithieu
										+"','"+s_apdungkmtc_id
										+"','"+skhoanmuctcs
										+"','"+stienkm_duochuong
										+"','"+stien_truocthue
										+"','"+ConvertFont.UtoD(sghichu)
										+"','"+skmcv_id
										+"','"+skmht_id
										+"','"+shinhthuctt_id
										+"','"+skieugiam_id	
										+"','"+psnguoi_cn+"'"
										+",'"+psmay_cn+"'"
										+");"
										+"end;";		
			
		System.out.println(xs_);			
		return this.reqValue("VNPBilling", xs_);
	}
	/////Xoa Cach giam
	public String xoa_km_cachgiams(String  schema,
  							String  schemaCommon,   
  							String  pikmht_id,	  																					  						   			        
					       	String  pikmcg_id) throws Exception
	{
	String xs_ = "";
					xs_ = xs_ + " begin ";
					xs_ = xs_ + " ? := "+CesarCode.decode(schemaCommon)+"promotions_km.xoa_km_cachgiams('"+getUserVar("userID")
										+"','"+getUserVar("sys_agentcode")		
										+"','"+CesarCode.decode(schemaCommon)
										+"','"+getUserVar("sys_dataschema")+"',"+pikmht_id
										+","+pikmcg_id       
										+");"
										+"end;";				
		System.out.println(xs_);			
		return this.reqValue("VNPBilling", xs_);
	}
	///Them dieu kien
	public String capnhat_dieukien( 								
								 String schemaCommon,
								 String psagent,
								 String psngaydk,
								 String psdieukien,
								 String pscongvan,
								 String psTypeMsisdn,
								 String psNgaycd,
								 String psNgaykh,										 
								 String psNgayDefault,
								 String psuserid,
								 String psuserip										 
								 )
								 throws Exception 
	{
	String xs_ = "";
		xs_ = xs_ + " begin ";
		xs_ = xs_ + " 	?:="+CesarCode.decode(schemaCommon)+"promotions_km.cauhinh_dk_pttb('"+psagent	
							+"','"+schemaCommon
							+"','"+pscongvan
							+"','"+psdieukien
							+"','"+psngaydk		
							+"','"+psNgaykh
							+"','"+psNgaycd
							+"','"+psNgayDefault
							+"','"+psTypeMsisdn
							+"','"+psuserid
							+"','"+psuserip+"'"
							+");end;";	
			
		System.out.println(xs_+"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		return this.reqValue("VNPBilling", xs_);		
		//return this.reqValue("", xs_);
	}
	public NEOCommonData ds_htkm_tbs(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_baocaos/danhsach_kms/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
	//upload tinh
	public NEOCommonData ds_hinthucs_upload(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_upload_tinh/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}
	///lay dstb
	public NEOCommonData ds_dieukiens_htt(int pikmcv_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=ccbs_kmck/km_danhsach_tbs/km_danhsach_tbs/ds_km_hinhthucs&" + CesarCode.encode("kmcv_id") + "=" + pikmcv_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}	
}

