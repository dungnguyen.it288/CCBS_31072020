package neo.collection;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import neo.ws.WSDL;
import java.util.*;
import javax.sql.*;


public class capnhattrangthai extends NEOProcessEx {
	
	//check thue bao ton tai
	public String check_tb_tontai(String tb, String matinh) throws Exception {
        String result="";
        String s= "begin ? := admin_v2.pttb_dichvu.CHECK_TB_TONTAI("
                + "'"+tb+"'"
				+ ",'"+matinh+"'"
                + ");"
                + "end;";
		result=reqValue(s);		
       return result;
    }
	
	//lay thong tin thue bao	
		public String layTTTB(String so_tb, String ma_tinh) 
	{
		
		String jsonResult = "";
		String s = "begin ? := admin_v2.pttb_dichvu.layTTTB('"+so_tb+"','"+ma_tinh+"');" 
				+ "end;";	
		System.out.println(s);
		try{
			RowSet rowSet = this.reqRSet(s);
            while (rowSet.next()) {                
                jsonResult = "{\"ten_tb\":\""+rowSet.getString("ten_tb")+"\"";							
				jsonResult = jsonResult+",\"trangthai\":\""+rowSet.getString("trangthai_id")+"\"";	
				jsonResult = jsonResult+",\"matinh\":\""+rowSet.getString("ma_tinh")+"\"";
				jsonResult = jsonResult+",\"loai\":\""+rowSet.getString("loai")+"\"";						
				jsonResult = jsonResult+",\"diachi\":\""+rowSet.getString("diachi")+"\"}";
			}	
		}catch(Exception e){
		}			
		return "["+jsonResult+"]";
		//return s;
	}
	// cap nhat trang thai
	
	public String capnhat_tt(String sotb, String matinh, String trangthai)
	{	String result = "";
		String s = "begin ? := admin_v2.pttb_dichvu.capnhat_tt("
                + "'" + sotb + "'"
                + ",'"+ matinh + "'"                               
                + ",'" + trangthai + "'"               
                + ");"
                + "end;";
		try {
            result = reqValue(s);
        } catch (Exception e) {
        }	
        return result;
		}
	
	// add log
	
	public String add_log(String sotb, String matinh, String trangthai,String trangthai_update, String note)
	{	String result = "";
		String s = "begin ? := admin_v2.pttb_dichvu.add_log("
                + "'" + sotb + "'"
                + ",'"+ matinh + "'"                               
                + ",'" + trangthai + "'" 
				+ ",'" + trangthai_update + "'" 
				+ ",'" + getUserVar("userID") + "'" 
				+ ",'" + getUserVar("userIP") + "'" 
				+ ",'" + note + "'"               
                + ");"
                + "end;";
				
        try {
            result = reqValue(s);
        } catch (Exception e) {
        }	
        return result;
		}
		
}