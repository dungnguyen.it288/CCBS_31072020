/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package neo.collection;
import neo.smartui.process.*;
import neo.smartui.common.*;
import neo.smartui.report.*;
import java.util.*;
import javax.sql.*;
/**
 *
 * @author Administrator
 */
public class log_tracuu_sms extends  NEOProcessEx{

   public void run() {
			System.out.println("neo.collection.logtracuu");
	}
    public String insert_data_log(String func,String msisdn,String userid,String ip,String note,String agent)
    {
        String s="begin ? :=admin_v2.pkg_tracuu_log_call_sms.insert_log_search_call_sms("
                                                            +"'"+func+"'"
                                                            +",'"+msisdn+"'"
                                                            +",'"+userid+"'"
                                                            +",'"+ip+"'"
                                                            +",'"+note+"'"
                                                            +",'"+agent+"'"
                                                            +");"+"end;";
       return s;
    }
	public NEOCommonData c1rt_export_xls(	String psuserid ,
											String psuserip ,
											String psdatefrom ,
											String psdateto ,
											String psTypeOnline ,
											String psType ,
											String psTypeDetail ,
											String psAction ,
											String psResult ,
											String psMsisdn ,
											String psTypeView ,
											String psRecordPerPage ,
											String psPageIndex 
										)
	{
	String s="";
	
	s="begin ? := admin_v2.pkg_general_c1rt.get_info_monitor_c1rt('"
													+ psuserid +"','"
													+ psuserip +"','"
													+ psdatefrom +"','"
													+ psdateto +"','"
													+ psTypeOnline +"','"
													+ psType +"','"
													+ psTypeDetail +"','"
													+ psAction +"','"
													+ psResult +"','"
													+ psMsisdn +"','"
													+ psTypeView +"','"
													+ psRecordPerPage +"','"
													+ psPageIndex 
													+"'); end;";
	NEOExecInfo ei_ = new NEOExecInfo(s);

    return  new NEOCommonData(ei_);
	}
	public String resend_request_monitor(String psidlist,
										 String pstype,
										 String psuserid,
										 String psuserip) throws Exception 
	{
		String s="begin ? :=admin_v2.pkg_general_c1rt.resend_request_monitor("
                                                            +"'"+psidlist+"'"
                                                            +",'"+pstype+"'"
                                                            +",'"+psuserid+"'"
                                                            +",'"+psuserip+"'"                                                           
                                                            +");"+"end;";
		return this.reqValue("", s);		
	}
	public String check_role(String user_id,
							 String menu_id)
	{
		String s="begin ? :=admin_v2.check_access_menu("+"'"+user_id+"'"                                                            
                                                            +",'"+menu_id+"'"                                                           
                                                            +");"+"end;";
		System.out.println(s);
		return s;
	}
	
	public String check_matinh_vascloud(String psmsisdn)
	{
		String s="begin ? :=admin_v2.kiemtra_tinh_thuebao_vascloud("+"'"+psmsisdn+"'"                                                            
                                                            +",'"+getUserVar("userID")+"'" 
															+",'"+getUserVar("sys_agentcode")+"'" 
                                                            +");"+"end;";
		System.out.println(s);
		return s;
	}
	
}
