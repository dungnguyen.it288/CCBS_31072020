package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class giahan_muccuoc extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("DataList");		
	}
	
	public String layls_giahan(String psSoTB
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/giahan_muccuoc/ajax_ls_giahan"
			+"&"+CesarCode.encode("so_tb")+"="+psSoTB;
	}	
		
	public  String laytt_tbgiahan(	String psFuncSchema
									  ,	String psDataSchema										
									  , String psUserID
									  , String psSoTB								  
									  )
	{
		
		String s = "begin ?:="+psFuncSchema+"PKG_GIAHAN_MC.laytt_giahan_muccuoc('"+psDataSchema+"'"
																+",'"  +psUserID+"'" 
																+",'"  +psSoTB+"'" 																																
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }  
	  
	public  String giahan_dv(String psFuncSchema
									  ,	String psSoTB		
									  , String psTenKH
									  , String psDiachi									 
									  , String psSoGT									 
									  , String psGhichu
									  , String psGoicuoc
									  , String psUserID
									  , String psUserIP
									  , String psDataSchema								  
									  )
	{		

		String s = "begin ?:="+psFuncSchema+"PKG_GIAHAN_MC.giahan_muccuoc('"+psSoTB+"'"															
																+",'"  +psTenKH+"'" 																
																+",'"  +psDiachi+"'" 																
																+",'"  +psSoGT+"'"																
																+",'"  +psGhichu+"'"
																+",'"  +psGoicuoc+"'"
																+",'"  +psUserID+"'"
																+",'"  +psUserIP+"'"
																+",'"  +psDataSchema+"'"
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }
	public  String huy_giahan(String psFuncSchema
									  ,	String psSoTB
									  ,	String psMahd
									  , String psGhichu
									  , String psUserID
									  , String psUserIP
									  , String psSchema	
									  )
	{
		
		String s = "begin ?:="+psFuncSchema+"PKG_GIAHAN_MC.huy_giahan_gcdv('"+psSoTB+"'"
																+",'"  +psMahd+"'"
																+",'"  +psGhichu+"'"
																+",'"  +psUserID+"'"
																+",'"  +psUserIP+"'"
																+",'"  +psSchema+"'"
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }	
	
	public  String dangky_mcdv(String psFuncSchema
									  ,	String psSoTB
									  , String psMahd
									  , String psUserID
									  , String psUserIP
									  , String psSchema									  
									  )
	{
		
		String s = "begin ?:="+psFuncSchema+"PKG_GIAHAN_MC.dangky_mcdv('"+psSoTB+"'"
																+",'"  +psMahd+"'"
																+",'"  +psUserID+"'"
																+",'"  +psUserIP+"'"
																+",'"  +psSchema+"'"
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }
	public  String thaydoi_mcdv(String psFuncSchema
									  ,	String psSoTB
									  , String psMahd
									  , String psThaotac
									  , String psHanmuc_cu
									  , String psHanmuc_moi
									  , String psGhichu
									  , String psUserID
									  , String psUserIP
									  , String psSchema									  
									  )
	{
		
		String s = "begin ?:="+psFuncSchema+"PKG_GIAHAN_MC.thaydoi_mcdv('"+psSoTB+"'"
																+",'"  +psMahd+"'"
																+",'"  +psThaotac+"'"
																+",'"  +psHanmuc_cu+"'"
																+",'"  +psHanmuc_moi+"'"
																+",'"  +psGhichu+"'"
																+",'"  +psUserID+"'"
																+",'"  +psUserIP+"'"
																+",'"  +psSchema+"'"
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }
	 public  String getSubscriber_tt(String psFuncSchema
									    ,	String psSoTB
										,	String psUserID
										,	String psSchema									  
									  )
	{
		
		String s = "begin ?:="+psFuncSchema+"PKG_GIAHAN_MC.laytt_tt_dv('"+psSoTB+"'"
																+",'"  +psUserID+"'"
																+",'"  +psSchema+"'"		
																+	"); end;";	

		System.out.println(s);		
		return  s;
   	  }
	  
	public String layls_giahan_cd(String psSoTB
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/giahan_muccuoc/ajax_ls_giahan_cd"
			+"&"+CesarCode.encode("so_tb")+"="+psSoTB;
	}
}