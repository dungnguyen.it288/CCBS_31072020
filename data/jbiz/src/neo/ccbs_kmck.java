
package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;
import com.svcon.jdbf.*;
import javax.sql.RowSet;
import java.sql.*;
import java.util.zip.*;
import java.io.*;
import  java.util.*;
import java.net.*;
import org.apache.poi.*;


public class ccbs_kmck extends NEOProcessEx {
  public void run() {
    System.out.println("neo.khuyenmai was called");
  }

  
//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData layds_hinhthuc_kmid_tinhs(
 										 String func_schema,
 										 String schemaCommon,
 										 String khuyenmai_id) 
 										{
   String s= "begin ?:="+schemaCommon+"khuyenmai.layds_hinhthuc_kmid_tinhs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+khuyenmai_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }


    
   public NEOCommonData layds_hinhthuc_km_tinhs( String piDanhSach_KMCK)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/cauhinh/km_hinhthucs_ajax";	
		url_ = url_ + "&"+CesarCode.encode("danhsach_kmck")+"="+piDanhSach_KMCK+"&sid="+Math.random();
			    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  
  
   public NEOCommonData layds_cachgiam_kmid_tinhs(
   			String pihinhthuc_km_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/cauhinh/km_cachgiams_ajax";	
		url_ = url_ + "&"+CesarCode.encode("khuyenmai_id")+"="+pihinhthuc_km_id+"&sid="+Math.random();
			    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  
  public NEOCommonData layds_cachgiam_id_tinhs(
 										 String func_schema,
 										 String schemaCommon,
 										 String cachgiam_id) 
 										{
   String s= "begin ?:="+schemaCommon+"khuyenmai.layds_cachgiam_id_tinhs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"'," +cachgiam_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }


  public NEOCommonData themmoi_hinhthuc_km_tinhs(
  							String  schema,
  							String  schemaCommon,   																	  						   			        
					       	String  ten_khuyenmai,
							String  pdNgay_BatDau,
							String  pdNgay_KT,
							String  piLoaiKM_ID,						
							String  piSoThang,
							String  piapdung_sauthanghm,
							String  picon_hieuluc,
							String  chkcamket_sdlt,
							String  pdNgay_KH,
							String  pdNgay_Khoa2Chieu,
							String  psNoiDungKM,							
							String  chktinh_taods,
							String  chk_hienthi,
							String  psnguoi_cn,
  							String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"khuyenmai.themmoi_hinhthuc_km_tinhs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"','"+ten_khuyenmai+"'"                      
        +","+pdNgay_BatDau
        +","+pdNgay_KT
        +",'"+piLoaiKM_ID+"'"    
        +","+piSoThang
        +",'"+piapdung_sauthanghm+"'"
        +",'"+picon_hieuluc+"'"
		+","+chkcamket_sdlt		
		+","+pdNgay_KH
		+","+pdNgay_Khoa2Chieu
		+","+psNoiDungKM
		+","+chktinh_taods
		+","+chk_hienthi
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
 

  
  public NEOCommonData capnhat_hinhthuc_km_tinhs(
  							String  schema,
  							String  schemaCommon,  
  							String  piKhuyenMaiID, 																	  						   			        
					       	String  ten_khuyenmai,
							String  pdNgay_BatDau,
							String  pdNgay_KT,
							String  piLoaiKM_ID,						
							String  piSoThang,
							String  piapdung_sauthanghm,
							String  picon_hieuluc,
							String  chkcamket_sdlt,
							String  pdNgay_KH,
							String  pdNgay_Khoa2Chieu,
							String  psNoiDungKM,							
							String  chktinh_taods,
							String  chk_hienthi,
							String  psnguoi_cn,
  							String  psmay_cn				
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"khuyenmai.capnhat_hinhthuc_km_tinhs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+piKhuyenMaiID+",'"+ten_khuyenmai+"'"                         
        +","+pdNgay_BatDau
        +","+pdNgay_KT
        +",'"+piLoaiKM_ID+"'"     
        +","+piSoThang
        +",'"+piapdung_sauthanghm+"'"
        +",'"+picon_hieuluc+"'"
		+","+chkcamket_sdlt		
		+","+pdNgay_KH
		+","+pdNgay_Khoa2Chieu
		+","+psNoiDungKM
		+","+chktinh_taods
		+","+chk_hienthi
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
     System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
    
  public NEOCommonData themmoi_cachgiam_km_tinhs(
  							String  schema,
  							String  schemaCommon,   																	  						   			        
					       	String  piHinhThuc_kmid,
					       	String  psten_cachgiam,
							String  piKhoanmuctt_id,
							String  pikieu_apdung_id,
							String  pitien_ctp,
							String  pitien_kphuclai,
							String  pitien_hm,
							String  chk_xdoiuong_id,
							String  chk_xkhoanmuctc_id,
							String 	chktienkm_duochuong,
							String 	chktien_truocthue,
							String  doituongs,
							String  kmtcs,
							String  psGhiChu,
							String  psnguoi_cn,
  							String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"khuyenmai.themmoi_cachgiam_km_tinhs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+piHinhThuc_kmid
		+",'"+psten_cachgiam+"'"
        +","+piKhoanmuctt_id
        +","+pikieu_apdung_id
        +","+pitien_ctp
        +","+pitien_kphuclai
        +","+pitien_hm
        +","+chk_xdoiuong_id
		+","+chk_xkhoanmuctc_id		
		+","+chktienkm_duochuong
		+","+chktien_truocthue
		+",'"+doituongs+"'"
		+",'"+kmtcs+"'"
		+","+psGhiChu
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  public NEOCommonData capnhat_cachgiam_km_tinhs(
  							String  schema,
  							String  schemaCommon,   
  							String  piCachGiam_id,	
  						    String  psten_cachgiam,																  						   			        
					       	String  piHinhThuc_kmid,
							String  piKhoanmuctt_id,
							String  pikieu_apdung_id,
							String  pitien_ctp,
							String  pitien_kphuclai,
							String  pitien_hm,
							String  chk_xdoiuong_id,
							String  chk_xkhoanmuctc_id,
							String 	chktienkm_duochuong,
							String 	chktien_truocthue,							
							String  doituongs,
							String  kmtcs,
							String  psGhiChu,
							String  psnguoi_cn,
  							String  psmay_cn						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"khuyenmai.capnhat_cachgiam_km_tinhs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+piCachGiam_id+",'"+psten_cachgiam+"',"+piHinhThuc_kmid                    
        +","+piKhoanmuctt_id
        +","+pikieu_apdung_id
        +","+pitien_ctp
        +","+pitien_kphuclai
        +","+pitien_hm
        +","+chk_xdoiuong_id
		+","+chk_xkhoanmuctc_id		
		+","+chktienkm_duochuong
		+","+chktien_truocthue
		+",'"+doituongs+"'"
		+",'"+kmtcs+"'"
		+","+psGhiChu
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }   
  
  
   
  public NEOCommonData xoa_cachgiam_km_tinhs(
  							String  schema,
  							String  schemaCommon,   
  							String  piCachGiam_id,	  																					  						   			        
					       	String  piKhuyenmai_id						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"khuyenmai.xoa_cachgiam_km_tinhs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+piCachGiam_id
		+","+piKhuyenmai_id       
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  
  
  
  
   
  public NEOCommonData xoa_km_tinhs(
  							String  schema,
  							String  schemaCommon,     																					  						   			        
					       	String  piKhuyenmai_id						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"khuyenmai.xoa_km_tinhs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+piKhuyenmai_id 
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  

	public NEOCommonData ds_nhanvien(int donviql_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=khuyenmai/baocao/danhsach_tien_kms/ds_nhanviens&" + CesarCode.encode("donviql_id") + "=" + donviql_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}
	
	public NEOCommonData ds_nhanvien_inphieu(int donviql_id)
	{
	    	String url_ = "/main?"; 
	    	url_ = url_+ CesarCode.encode("configFile") + "=khuyenmai/baocao/in_hoadon_kms/ds_nhanviens&" + CesarCode.encode("donviql_id") + "=" + donviql_id;
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}
  
  public NEOCommonData laytt_ngay_apdung(   String func_schema,
											String schemaCommon,
											String khuyenmai_id)
	{
		String s = "begin ?:=" + schemaCommon + "khuyenmai.laytt_ngay_apdung('" + getUserVar("userID")
			 + "','" + getUserVar("sys_agentcode")
			 + "','" + schemaCommon
			 + "','" + getUserVar("sys_dataschema")
			 + "'," + khuyenmai_id
			 + ");end;";
		System.out.println(s); 
		
		NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}
  
  public NEOCommonData laytt_ngay_apdung_new(   String func_schema,
											String schemaCommon,
											String khuyenmai_id)
	{
		String s = "begin ?:=" + schemaCommon + "PROMOTIONS.laytt_ngay_apdung_new('" + getUserVar("userID")
			 + "','" + getUserVar("sys_agentcode")
			 + "','" + schemaCommon
			 + "','" + getUserVar("sys_dataschema")
			 + "'," + khuyenmai_id
			 + ");end;";
		System.out.println(s); 
		
		NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}
  
  /*Nhu thanh the:
  *chuc nang: lay thong tin thue bao khuyen mai.
  *phuc vu nut tim kiem
  *ngay_pt: 09/10/2008. 
  						
  */
    public NEOCommonData layds_tb_khuyenmais
								(String piPAGEID,
    							   String piREC_PER_PAGE,    							
    							   String ma_tb,
    							   String ngay_apdung, 
    							   String ngay_kt,   							  
    							   String khuyenmai_id,
    							   String trangthai_id) {
    String  s= "/main?"+CesarCode.encode("configFile")+"=ccbs_kmck/capnhat_ds/xuly_danhsach_km/ajax_layds_tb_khuyenmais"
		+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
		+ "&" +CesarCode.encode("ma_tb")+"="+ma_tb
		+ "&" +CesarCode.encode("ngay_apdung")+"="+ngay_apdung
		+ "&" +CesarCode.encode("ngay_kt")+"="+ngay_kt
		+ "&" +CesarCode.encode("khuyenmai_id")+"="+khuyenmai_id
		+ "&" +CesarCode.encode("trangthai_id")+"="+trangthai_id
        ;   
	System.out.println(s); 
		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_); 
     
  }
  
  
  public NEOCommonData themmoi_ds_khuyenmais(	 
										 String psschema,
  										 String psschemaCommon,   
  										 String psma_tb_1   
										,String pikhuyenmai_id_1										
										,String psghichu
										,String psnguoi_cn
										,String psmay_cn
									
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"khuyenmai.themmoi_ds_khuyenmais('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
        +",'"+psma_tb_1+"'"
        +",'"+pikhuyenmai_id_1+"'"      
		+","+psghichu
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_); 
    
  			
  	}
	
public NEOCommonData capnhat_ds_khuyenmais(
     String psschema
	,String psschemaCommon  
	,String psdsSomay   									
	,String psdsKMID
	,String psdsApdung
	,String psghichu
	,String psnguoi_cn
	,String psmay_cn
									
 ){
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"khuyenmai.capnhat_ds_khuyenmais('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)
        +"','"+psdsKMID
        +"','"+psdsApdung
        +"','"+psdsSomay       
		+"','"+psghichu
		+"','"+psnguoi_cn
		+"','"+psmay_cn
        +"');"
        +"end;";
        
     System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);    
  }  

public NEOCommonData laytt_tb_dacbiet ( String func_schema,
 										 String schemaCommon,
 										 String ma_tb,
 										 String khuyenmai_id) 
 										{
   String s= "begin ?:="+schemaCommon+"khuyenmai.laytt_tb_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"','"+ma_tb+"'"
		+","+khuyenmai_id
		+");end;";
	
	 System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);  
  }
  
  
  public NEOCommonData laytt_khuyenmais(    String func_schema,
											String schemaCommon,
											String khuyenmai_id)
	{
		String s = "begin ?:=" + schemaCommon + "khuyenmai.laytt_khuyenmais('" + getUserVar("userID")
			 + "','" + getUserVar("sys_agentcode")
			 + "','" + schemaCommon
			 + "','" + getUserVar("sys_dataschema")
			 + "'," + khuyenmai_id
			 + ");end;";

		 System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}

	
	
	
	public NEOCommonData capnhat_tbkm_dacbiet(
  								 String psschema,
  										 String psschemaCommon,   
  										 String psma_tb  										 
										,String pikhuyenmai_id
										,String pskieuld_id
										,String psghichu
										,String pdthang_bd
										,String pdthang_kt
										,String psnguoi_cn
										,String psmay_cn
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"khuyenmai.capnhat_tbkm_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
        +",'"+psma_tb+"'"
        +",'"+pikhuyenmai_id+"'"
        +","+pskieuld_id	
		+","+psghichu
	    +","+pdthang_bd
		+","+pdthang_kt
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
		
    System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
  }   
  
  
 public NEOCommonData xoa_tbkm_dacbiet( String psschema, String psschemaCommon, String psma_tb,String pikhuyenmai_id)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"khuyenmai.xoa_tbkm_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_dataschema")+"'"
        +",'"+psma_tb+"'"
        +","+pikhuyenmai_id      
        +");"
        +"end;"
        ;        
    System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);  
  }
 
 public NEOCommonData themmoi_tbkm_dacbiet(	 String psschema,
  										 String psschemaCommon,   
  										 String psma_tb_1
										,String pikhuyenmai_id_1
										,String pskieuld_id
										,String psghichu
										,String pdthang_bd
										,String pdthang_kt
										,String psnguoi_cn
										,String psmay_cn
									
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"khuyenmai.themmoi_tbkm_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
        +",'"+psma_tb_1+"'"
        +",'"+pikhuyenmai_id_1+"'"
        +","+pskieuld_id	
		+","+psghichu
	    +","+pdthang_bd
		+","+pdthang_kt
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
      System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);  
  	}
  
  public NEOCommonData themmoi_tbkm_dacbiet_new(	 
		String psschema,
		String psschemaCommon,
		String psma_tb_1,
		String pikhuyenmai_id_1,
		String piKMHT,
		String pskieuld_id,
		String psghichu,
		String pdthang_bd,
		String psnguoi_cn,
		String psmay_cn
	){  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"promotions.themmoi_tbkm_dacbiet('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
        +",'"+psma_tb_1+"'"
        +",'"+pikhuyenmai_id_1+"'"
		+","+piKMHT
        +","+pskieuld_id		
		+","+psghichu
	    +","+pdthang_bd
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
      System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);  
  	}
  
  public String layds_km_hinhthucs(String ps_agentcode, String ps_kmcv){
	return "/main?"+CesarCode.encode("configFile")+"=ccbs_kmck/km_danhsach_tbs/km_danhsach_tbs/hinhthuc_km"
	+"&"+CesarCode.encode("ps_agentcode") + "=" + ps_agentcode
	+"&"+CesarCode.encode("ps_kmcv") + "=" + ps_kmcv;
  }
  
  
  
}

