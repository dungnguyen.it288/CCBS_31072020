package neo;

import neo.smartui.common.CesarCode;
import neo.smartui.common.ConvertFont;

import neo.smartui.process.*;
import neo.smartui.report.*;
import neo.smartui.cache.*;

import java.util.*;
import javax.sql.*;

public class act
extends NEOProcessEx {
	public void run() {
		System.out.println("neo.act was called");
	}
	
  public String get_bugs(String psUser 
						 ,String psAgentCode							
						 ,String psFuncSchema
						 
						 ,String psModule 
						 ,String psTestDate 
						 ,String psTester 
						 ,String psSeverity 
						 
						 ,String psStatus 
						 ,String psUserBug 
						 ,String psBug 	) 
{
    
	
	String s = "/main?"+CesarCode.encode("configFile")+"=admin/act/act_bug_ajax"
        +"&"+CesarCode.encode("psuser")+"="+psUser
		+"&"+CesarCode.encode("psagentcode")+"="+psAgentCode
		+"&"+CesarCode.encode("psfuncschema")+"="+psFuncSchema
		
		+"&"+CesarCode.encode("psmodule")+"="+psModule
		+"&"+CesarCode.encode("pstestdate")+"="+psTestDate
		+"&"+CesarCode.encode("pstester")+"="+psTester
		+"&"+CesarCode.encode("psseverity")+"="+psSeverity
		
		+"&"+CesarCode.encode("psstatus")+"="+psStatus
		+"&"+CesarCode.encode("psuserbug")+"="+psUserBug		
		+"&"+CesarCode.encode("psbug")+"="+psBug;
		
	
	return s;
  }	
  
  
  public String addnew_bug ( String psUserid 
						 ,String psAgentCode							
						 ,String psFuncSchema
						 
						 ,String psModule 
						 ,String psTestDate 
						 ,String psTester 
						 ,String psSeverity 
						 
						 ,String psStatus 
						 ,String psUserBug 
						 ,String psBug 	) 
{
    
	String s=    "begin ? := "+psFuncSchema+"act.addnew_bug("
							+"  '"+psUserid
							+"','"+psAgentCode
							+"','"+psFuncSchema
							 
							+"','"+psModule
							+"','"+psTestDate
							+"','"+psTester 
							+"','"+psSeverity
							 
							+"','"+psStatus
							+"','"+psUserBug 
							+"',"+psBug 
							+"); end;";   
								System.out.println(s);
    return s;
  }	
  
    public String edit_bug ( String psUserid 
						 ,String psAgentCode							
						 ,String psFuncSchema
						 
						 ,String psBugID  
						 ,String psModule 
						 ,String psTestDate 
						 ,String psTester 
						 ,String psSeverity 
						 
						 ,String psStatus 
						 ,String psUserBug 
						 ,String psBug 	) 
{
    
	String s=    "begin ? := "+psFuncSchema+"act.edit_bug("
							+"  '"+psUserid
							+"','"+psAgentCode
							+"','"+psFuncSchema
							
							+"','"+psBugID 
							+"','"+psModule
							+"','"+psTestDate
							+"','"+psTester 
							+"','"+psSeverity
							 
							+"','"+psStatus
							+"','"+psUserBug 
							+"',"+psBug 
							+"); end;";   
								System.out.println(s);
    return s;
  }	

  
   public String delete_bug ( String psUserid 
						 ,String psAgentCode							
						 ,String psFuncSchema
						 
						 ,String psBugID  
						) 
{
    
	String s=    "begin ? := "+psFuncSchema+"act.delete_bug("
							+"  '"+psUserid
							+"','"+psAgentCode
							+"','"+psFuncSchema
							
							+"','"+psBugID 
							+"'); end;";   
								System.out.println(s);
    return s;
  }	
  
  
  
    public String search_bug ( String psUserid 
							 ,String psAgentCode							
							 ,String psFuncSchema
							 
							 ,String psBugID 	) 
{
    
	String s=    "begin ? := "+psFuncSchema+"act.search_bug("
							+"  '"+psUserid
							+"','"+psAgentCode
							+"','"+psFuncSchema							 
							+"','"+psBugID+"'); end;";   
								System.out.println(s);
    return s;
  }	
  
	public String  set_bug_status ( 
							  String psUserid 
							 ,String psAgentCode							
							 ,String psFuncSchema
							 
							 ,String psTypeCN
							 ,String psBugID 
							 ,String psStatus							 
							 ,String psResolution 
							 ) 
{
    
	String s=    "begin ? := "+psFuncSchema+"act.set_bug_status("
							+"  '"+psUserid
							+"','"+psAgentCode
							+"','"+psFuncSchema							 
							
							+"','"+psTypeCN	
							+"','"+psBugID	
							+"','"+psStatus	
							+"','"+psResolution+"'); end;";   
								System.out.println(s);
    return s;
  }	
	


}
