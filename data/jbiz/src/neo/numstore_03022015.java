package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;




public class numstore extends NEOProcessEx
{	
	public void run()
	{
		System.out.println("NumStore");		
	}
	
	public String doc_layds_thuebao(
		String psSoTB,
		String psKieuso_ID,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=dangky/ajax_ds_thuebao"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("kieuso_id") + "=" + psKieuso_ID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	
	public String doc_layth_thuebao(
		String psSoTB,
		String psKieuso_ID,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/laphd/chonso/ajax_th_thuebao"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("kieuso_id") + "=" + psKieuso_ID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String doc_layds_khoa(
		String psUserid
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/laphd/chonso/ajax_ds_khoa"
  			+ "&" + CesarCode.encode("userid") + "=" + psUserid;
	}
	
	public String doc_layds_thuebao_1(
		String psSoTB,
		String psKieuso_ID,
		String psSubRange,
		String psStatus,
		String psUserid,
		String psPageNum,
		String psPageRec
	)
	{
		String xs_ = "";
		if (getUserVar("userID") == null || getUserVar("userID").equals("")){
			xs_ = "begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
								+psSoTB+"','doc_layds_thuebao_1'," 
								+"'"+psSoTB+";"+psKieuso_ID+";"+psSubRange+";"+psStatus+";"+psPageNum+";"+psPageRec+"','"
								+psUserid+"','"
								+getUserVar("userID")+"','"
								+getEnvInfo().getParserParam("sys_userip")+"','"
								+getUserVar("sys_agentcode")+"'); "
								+ "   commit; "
								+ " end; ";		
			try{
				NEOExecInfo nei_ = new NEOExecInfo(xs_);
				return new NEOCommonData(nei_).toString();
			} catch (Exception ex) {
				ex.printStackTrace();	
			}
		}
		return "/main?" + CesarCode.encode("configFile")+"=pttb/laphd/chonso/ajax_ds_thuebao_1"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("kieuso_id") + "=" + psKieuso_ID
			+ "&" + CesarCode.encode("sub_range") + "=" + psSubRange
			+ "&" + CesarCode.encode("status") + "=" + psStatus
			+ "&" + CesarCode.encode("userid") + "=" + psUserid
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String doc_layds_thuebao_dk(
		String psSoTB,
		String psKieuso_ID,
		String psSubRange,
		String psStatus,
		String psUserid,
		String psPageNum,
		String psPageRec
	)
	{
	
		//Ghi log cat mo IC OC TRINHHV 19/01/2014
		String xs_ = "";
		if (getUserVar("userID") == null || getUserVar("userID").equals("")){
			xs_ = "begin "
						+ " 	? := ccs_batch.qtht.ins_log_call_ccbs('"
								+psSoTB+"','doc_layds_thuebao_dk'," 
								+"'"+psSoTB+";"+psKieuso_ID+";"+psSubRange+";"+psStatus+";"+psPageNum+";"+psPageRec+"','"
								+psUserid+"','"
								+getUserVar("userID")+"','"
								+getEnvInfo().getParserParam("sys_userip")+"','"
								+getUserVar("sys_agentcode")+"'); "
								+ "   commit; "
								+ " end; ";		
			try{
				NEOExecInfo nei_ = new NEOExecInfo(xs_);
				return new NEOCommonData(nei_).toString();
			} catch (Exception ex) {
				ex.printStackTrace();	
			}
		}
	
		return "/main?" + CesarCode.encode("configFile")+"=pttb/laphd/chonso/ajax_ds_thuebao_dk"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("kieuso_id") + "=" + psKieuso_ID
			+ "&" + CesarCode.encode("sub_range") + "=" + psSubRange
			+ "&" + CesarCode.encode("status") + "=" + psStatus
			+ "&" + CesarCode.encode("userid") + "=" + psUserid
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	
	public String doc_layds_kieuso_new(
		String psSubRange
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/laphd/chonso/ajax_kieuso_new"
  			+ "&" + CesarCode.encode("subrange") + "=" + psSubRange;
	}
	
	public String doc_validate(String psCode)
	{
		return "/jcaptchaValid?j_captcha_response="+psCode;
	}
/*
	Modify : luattd/20.06.2013
	Comment : check xac thuc captcha truoc khi gui lenhdang ky thue bao
*/	
	  
	public String doc_layds_daiso(
		String psSoTB,
		String psDaiso_ID,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/numstore/dangky_daiso/ajax_ds_daiso"
  			+ "&" + CesarCode.encode("soluong_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("daiso_id") + "=" + psDaiso_ID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	public String doc_layth_daiso(
		String psSoTB,
		String psDaiso_ID,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/numstore/dangky_daiso/ajax_th_daiso"
  			+ "&" + CesarCode.encode("so_tb") + "=" + psSoTB
  			+ "&" + CesarCode.encode("daiso_id") + "=" + psDaiso_ID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	
	public NEOCommonData dangky_daiso(
		String psIDDaiSo,
		String psDaiSoTB,
		String psSoluongTB,
		String psTen,
		String psDiachi,
		String psTenVietTat,
		String psTenDangKy,
		String psDienthoai,
		String psMaSoThue,
		String psWebsite,
		String psEmail,
		String psCode,
		String psSchema,
		String psUserID,
		String psUserIP
	){
		String s = "begin ?:= dms_public.store.dangky_daiso_admin('"
	 				+psDaiSoTB+"','"
					+psSoluongTB+"','"
					+psIDDaiSo+"','"
	 				+psTen+"','"
	 				+psDiachi+"','"
					+psTenVietTat+"','"
					+psTenDangKy+"','"
	 				+psDienthoai+"','"
	 				+psMaSoThue+"','"
	 				+psWebsite+"','"
	 				+psEmail+"','"
	 				+psCode+"','"
	 				+psSchema+"','"
					+psUserID+"','"
	 				+psUserIP+"'); end;";	
		NEOExecInfo nei_ = new NEOExecInfo(s);
		nei_.setDataSrc("NUMSTOREDBMS");
		return new NEOCommonData(nei_);
	}
	public String doc_layds_chitiet_daiso(
		String psKhodaisoID,
		String psPageNum,
		String psPageRec,
		String pssoluong
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/numstore/chitiet_daiso/ajax_ds_chitiet_daiso"
  			+ "&" + CesarCode.encode("khodaiso_id") + "=" + psKhodaisoID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec
			+ "&" + CesarCode.encode("soluong") + "=" + pssoluong;
	}
	public String doc_layth_chitiet_daiso(
		String psKhodaisoID,
		String psPageNum,
		String psPageRec
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=pttb/numstore/chitiet_daiso/ajax_th_chitiet_daiso"
  			+ "&" + CesarCode.encode("khodaiso_id") + "=" + psKhodaisoID
  			+ "&" + CesarCode.encode("page_num") + "=" + psPageNum
			+ "&" + CesarCode.encode("page_rec") + "=" + psPageRec;
	}
	
	public String doc_layds_province(
		String psArea
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=lapphieu/ajax_province"
  			+ "&" + CesarCode.encode("area") + "=" + psArea;
	}
	
	public String doc_layds_cuahang(
		String psArea,
		String psProvince,
		String PsProvinceName
	)
	{
		return "/main?" + CesarCode.encode("configFile")+"=lapphieu/ajax_ds_cuahang"
  			+ "&" + CesarCode.encode("area") + "=" + psArea
  			+ "&" + CesarCode.encode("province") + "=" + psProvince
			+ "&" + CesarCode.encode("name") + "=" + PsProvinceName
			;
	}
		//Kiem tra code file
}