package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_smtongdai
extends NEOProcessEx {
	public void run()
	{
		System.out.println("neo.pttb_smtongdai was called");
	}
	public String xoa_qlsomay(String schema,
		String psTUSO
		,String psDENSO
		,String piTONGDAI_ID
		,String piLOAITB_ID
		,String piTONGDAIORSOMAY
	)
	{
		String s = "begin ? := " + CesarCode.decode(schema) + "PTTB_TEST_KIEN.xoa_qlsomay("
					+ "'" + psTUSO + "'"
					+ ",'" + psDENSO + "'"
					+ "," + piTONGDAI_ID
					+ "," + piLOAITB_ID
					+ "," + piTONGDAIORSOMAY
			+ ");"
			+ "end;";
		System.out.println(s);
		return s;	
	}
	public String capnhat_qlsomay(String schema,
			String psTUSO
			, String psDENSO
			, String piLOAITB_ID
			, String piTONGDAI_ID
			, String piTRAMVT_ID
		)
	{
		String s = "begin ? := " + CesarCode.decode(schema) + "PTTB_TEST_KIEN.capnhat_qlsomay("
					+ "'" + psTUSO + "'"
					+ ",'" + psDENSO + "'"
					+ "," + piLOAITB_ID
					+ "," + piTONGDAI_ID
					+ "," + piTRAMVT_ID
			+ ");"
			+ "end;";			
		System.out.println(s);
		return s;		
	}
}