package neo;
import javax.sql.RowSet;

import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_chuyendbtinhcuoc
extends NEOProcessEx {
	public void run()
	{
		System.out.println("neo.pttb_chuyendbtinhcuoc was called");
	}


	public String capnhat_cuoc_tbdv_cuoithang(
      String psThangNam
      ,String picapnhat_trangthai
	  ,String pitinhtat

	) {
		String s = "begin ? := " + getSysConst("FuncSchema") + "PTTB_DULIEU.capnhat_cuoc_tbdv_cuoithang('"
				+getUserVar("userID")
				+"','"+getUserVar("sys_agentcode")
				+"','"+getUserVar("sys_dataschema")
				+ "','" + psThangNam + "'"
				+ "," + picapnhat_trangthai
				+ "," + pitinhtat
			+ ");"
			+ "end;";
						System.out.println(s);

		return s;

	}
	public String capnhat_trangthai_cuoc_tbdv() {
		String s = "begin ? := " + getSysConst("FuncSchema") + "PTTB_DULIEU.capnhat_trangthai_cuoc_tbdv('"
				+getUserVar("userID")
				+"','"+getUserVar("sys_agentcode")
				+"','"+getUserVar("sys_dataschema")+"'"
			+ ");"
			+ "end;";
		return s;

	}
	public String tao_danhba_tinhcuoc(
      String psThangNam
      ,String piXoaBangCu

	) {
		String s = "begin ? := " + getSysConst("FuncSchema") + "PTTB_DULIEU.tao_danhba_tinhcuoc('"
				+getUserVar("userID")
				+"','"+getUserVar("sys_agentcode")
				+"','"+getUserVar("sys_dataschema")
				+ "','" + psThangNam + "'"
				+ "," + piXoaBangCu
			+ ");"
			+ "end;";
		return s;

	}
public NEOCommonData chuyen_danhba_tinhcuoc(
      String psThangNam
      ,String piXoaBangCu

        ) {
                String s = "begin ? := ccs_common.DULIEU_AGENT.chuyen_danhba_tinhcuoc('"
                                +getUserVar("userID")
                                +"','"+getUserVar("sys_agentcode")
                                +"','"+getUserVar("sys_dataschema")
                                + "','" + psThangNam + "'"
                                + "," + piXoaBangCu
                        + ");"
                        + "end;";

                    NEOExecInfo nei_ = new NEOExecInfo(s);
                    nei_.setDataSrc("VNPBilling");
                    return new NEOCommonData(nei_);

	}
	
public NEOCommonData chot_danhba_theothang (
									String psThangNam
					     		   ,String piXoaBangCu
       						 ) 
 {
                String s = "begin ? := ccs_common.billing.chot_danhba_tc('"
                                +getUserVar("userID")
                                +"','"+getUserVar("sys_agentcode")
                                +"','"+getUserVar("sys_dataschema")
                                + "','" + psThangNam + "'"
                                + "," + piXoaBangCu
                        + ");"
                        + "end;";

                    NEOExecInfo nei_ = new NEOExecInfo(s);
                    nei_.setDataSrc("VNPBilling");
					System.out.print(s);
                    return new NEOCommonData(nei_);
					
	}

public NEOCommonData chot_ds_khuyenmai (
									String psThangNam
					     		  
       						 ) 
 {
                String s = "begin ? := ccs_common.billing.chot_ds_khuyenmais('"
                                +getUserVar("userID")
                                +"','"+getUserVar("sys_agentcode")
                                +"','"+getUserVar("sys_dataschema")
                                + "','" + psThangNam + "'"                             
                        + ");"
                        + "end;";

                    NEOExecInfo nei_ = new NEOExecInfo(s);
                    nei_.setDataSrc("VNPBilling");
					System.out.print(s);
                    return new NEOCommonData(nei_);
	}
	
 public NEOCommonData chuyen_no_dk(
      String psThangNam
      ,String piXoaBangCu

        ) {
                String s = "begin ? := ccs_common.DULIEU_AGENT.chuyen_no_dk('"
                                +getUserVar("userID")
                                +"','"+getUserVar("sys_agentcode")
                                +"','"+getUserVar("sys_dataschema")
                                + "','" + psThangNam + "'"
                                + "," + piXoaBangCu
                        + ");"
                        + "end;";

                    NEOExecInfo nei_ = new NEOExecInfo(s);
                    nei_.setDataSrc("VNPBilling");
                    return new NEOCommonData(nei_);

	}
        public NEOCommonData tonghop_no_ps(
      String psThangNam
      ,String piXoaBangCu

        ) {
                String s = "begin ? := ccs_common.DULIEU_AGENT.tonghop_no_ps('"
                                +getUserVar("userID")
                                +"','"+getUserVar("sys_agentcode")
                                +"','"+getUserVar("sys_dataschema")
                                + "','" + psThangNam + "'"
                                + "," + piXoaBangCu
                        + ");"
                        + "end;";

                    NEOExecInfo nei_ = new NEOExecInfo(s);
                    nei_.setDataSrc("VNPBilling");
                    return new NEOCommonData(nei_);

	}

	public String capnhat_biendongtb(
      String psThangNam

	) {
		String s = "begin ? := " + getSysConst("FuncSchema") + "PTTB_DULIEU.capnhat_biendongtb('"
				+getUserVar("userID")
				+"','"+getUserVar("sys_agentcode")
				+"','"+getUserVar("sys_dataschema")
				+ "','" + psThangNam + "'"
			+ ");"
			+ "end;";
		return s;

	}
	
	public String chuyen_dlieu_kmai_mb_cuoc(
      String psThangNam, String psmay_cn, String psnewdata, String psupdatedata
	) {
		String s = "begin ? := " + getSysConst("FuncSchema") + "pkg_dlieu_kmai.chuyen_dlieu_kmai_mb_cuoc("
				+ "'" + psThangNam + "'"
				+ ",'" + psmay_cn + "'"
				+ ",'" + getUserVar("userID") + "'"
				+ ",'" + psnewdata + "'"
				+ ",'" + psupdatedata + "'"
			+ ");"
			+ "end;";
		return s;

	}
public String layds_loi_danhba_tcs(
 										 String func_schema,
 										 String schemaCommon,
 										 String psChuKy) 
 										{
   String s= "begin ?:="+func_schema+"pttb_dulieu.layds_loi_danhba_tc('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")
		+"','"+schemaCommon
		+"',"+psChuKy
		+");end;";
	
	System.out.println(s); 	
	return s;
  }
//------------------------------------------------------------
 public String layds_km(String psChuky) {
	 
	 String s = "/main?" + CesarCode.encode("configFile") + "=pttb/chuyendb_tinhcuoc/km_careplus/ajax_khuyenmai"	 	
		+ "&" + CesarCode.encode("chuky") + "=" + psChuky;
	 return s;
 }

 public String layds_km_chitiet(String psChuky, String kmht_id, int mode) {
	 String s = "";
	 if(mode == 0) { //lay danh sach tb duoc huong km trong ds_khuyenmais
		 s = "/main?" + CesarCode.encode("configFile") + "=pttb/chuyendb_tinhcuoc/km_careplus/ds_chitiet/ajax_dskm"	 	
			+ "&" + CesarCode.encode("chuky") + "=" + psChuky
			+ "&" + CesarCode.encode("kmht_id") + "=" + kmht_id;
	 } else if (mode == 1) { //lay danh sach tb duoc tru tien trong km_mmyyyy
		 s = "/main?" + CesarCode.encode("configFile") + "=pttb/chuyendb_tinhcuoc/km_careplus/ds_chitiet/ajax_km_mmyyyy"	 	
			+ "&" + CesarCode.encode("chuky") + "=" + psChuky
			+ "&" + CesarCode.encode("kmht_id") + "=" + kmht_id;
	 } else { //lay danh sach tb duoc huong km trong ds_khuyenmais nhung ko duoc tru tien trong km_mmyyyy
		 s = "/main?" + CesarCode.encode("configFile") + "=pttb/chuyendb_tinhcuoc/km_careplus/ds_chitiet/ajax_ds_not_km_mmyyyy"	 	
			+ "&" + CesarCode.encode("chuky") + "=" + psChuky
			+ "&" + CesarCode.encode("kmht_id") + "=" + kmht_id;
	 }		 
	 
	 return s;
 }
 
 public String layds_careplus(String psChuky) {
	 String s = "/main?" + CesarCode.encode("configFile") + "=pttb/chuyendb_tinhcuoc/km_careplus/ajax_careplus"	 	
		+ "&" + CesarCode.encode("chuky") + "=" + psChuky;
	 return s;
 }
 
 public String layds_careplus_chitiet(String psChuky, int mode) {
	 String s = "/main?" + CesarCode.encode("configFile") + "=pttb/chuyendb_tinhcuoc/km_careplus/ds_chitiet/ajax_careplus"	 	
		+ "&" + CesarCode.encode("chuky") + "=" + psChuky
		+ "&" + CesarCode.encode("mode") + "=" + mode;
	 return s;
 }
 
 public NEOCommonData updGetBill(String psschema, String dataschema, int chkGetBill) {
	String s = "begin ?:="+CesarCode.decode(psschema)+"promotions.update_getBill('"+dataschema
			+ "'," + chkGetBill			
			+");end;";	
	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_); 
 }
 
 public String getAgentListByUser(String userId) {
	 String s = "/main?" + CesarCode.encode("configFile") + "=pttb/chuyendb_tinhcuoc/ds_nguoi_thuchien"	 	
		+ "&" + CesarCode.encode("userid") + "=" + userId;	
	 return s;	
 }
 
 public String export_km_chitiet(String psChuky, String kmht_id, int mode, String dataSchema, String userId) {
	 String s = "";
	 String fileName = "dskm_upload_error.xls";
	 
	 try {
		 if(mode == 0) { //lay danh sach tb duoc huong km trong ds_khuyenmais
			 fileName = "dskm.xls";
			 s = "begin ?:=ccs_common.PROMOTIONS.lay_dskm("
					+ "'" + psChuky + "'" 
					+ ",'" + kmht_id + "'" 
					+ ",'" + dataSchema + "'" 
					+ ",'" + userId + "'" 
					+ ");end;";
		 } else if (mode == 1) { //lay danh sach tb duoc tru tien trong km_mmyyyy
			 fileName = "dskm_duoc_trutien.xls";
			 s = "begin ?:=ccs_common.PROMOTIONS.lay_km_mmyyyy("
					+ "'" + psChuky + "'" 
					+ ",'" + kmht_id + "'" 
					+ ",'" + dataSchema + "'" 
					+ ",'" + userId + "'" 
					+ ");end;";
		 } else { //lay danh sach tb duoc huong km trong ds_khuyenmais nhung ko duoc tru tien trong km_mmyyyy
			 fileName = "dskm_koduoc_trutien.xls";
			 s = "begin ?:=ccs_common.PROMOTIONS.lay_ds_not_km_mmyyyy("
					+ "'" + psChuky + "'" 
					+ ",'" + kmht_id + "'" 
					+ ",'" + dataSchema + "'" 
					+ ",'" + userId + "'" 
					+ ");end;";
		 }
		 
		RowSet rowSet = reqRSet("VNPBilling", s);

		String fileUploadDir = getSysConst("FileUploadDir");
		String ccs = getUserVar("sys_dataschema");
		ccs = ccs.replace(".", "");
		fileUploadDir = fileUploadDir + "Export_DSKM" + "\\" + ccs;
		

		Runtime.getRuntime().exec("cmd /C mkdir " + fileUploadDir);

		String fileOutput = fileUploadDir + "\\" + fileName;

		ccbs_export.exportToExcel(fileOutput, fileName, rowSet);

		String ret = "begin ?:='" + "{fileurl:\""
				+ fileOutput.replace("\\", "\\\\") + "\",filename:\""
				+ fileName + "\"}" + "'; end;";

		return ret;

	} catch (Exception ex) {
		ex.printStackTrace();
		return null;
	}
 }
 
 
 public String export_careplus_chitiet(String psChuky, int mode, String dataSchema, String userId) {
	 String s = "";
	 String fileName = "";
	 
	 try {
		 if(mode == 0) { //lay danh sach careplus va so du tai khoan
			 fileName = "ds_careplus_sdtk.xls";
		 } else if (mode == 1) { //lay danh sach careplus
			 fileName = "ds_careplus.xls";
		 } else { //lay danh sach so du tai khoan
			 fileName = "ds_sdtk.xls";
		 }
		 
		 s = "begin ?:=ccs_common.PROMOTIONS.lay_dscareplus("
			 	+ "'" + psChuky + "'" 
				+ "," + mode
				+ ",'" + dataSchema + "'" 
				+ ",'" + userId + "'" 
				+ ");end;";
		 
		RowSet rowSet = reqRSet("VNPBilling", s);

		String fileUploadDir = getSysConst("FileUploadDir");
		String ccs = getUserVar("sys_dataschema");
		ccs = ccs.replace(".", "");
		fileUploadDir = fileUploadDir + "Export_DSKM" + "\\" + ccs;
		

		Runtime.getRuntime().exec("cmd /C mkdir " + fileUploadDir);

		String fileOutput = fileUploadDir + "\\" + fileName;

		ccbs_export.exportToExcel(fileOutput, fileName, rowSet);

		String ret = "begin ?:='" + "{fileurl:\""
				+ fileOutput.replace("\\", "\\\\") + "\",filename:\""
				+ fileName + "\"}" + "'; end;";
		
		System.out.println("++++++++++++++++++++++++  " + ret);
		
		return ret;
		
	} catch (Exception ex) {
		ex.printStackTrace();
		return null;
	}
 }
 
 
 public String export_ds_km(String psChuky, String dataSchema, String userId) {
	 String s = "";
	 String fileName = "";
	 
	 try {
		 fileName = "ds_km.xls";
		 
		 s = "begin ?:=ccs_common.PROMOTIONS.lay_ds_sl_km("
			 	+ "'" + psChuky + "'" 
				+ ",'" + dataSchema + "'" 
				+ ",'" + userId + "'" 
				+ ");end;";
		 
		RowSet rowSet = reqRSet("VNPBilling", s);

		String fileUploadDir = getSysConst("FileUploadDir");
		String ccs = getUserVar("sys_dataschema");
		ccs = ccs.replace(".", "");
		fileUploadDir = fileUploadDir + "Export_DSKM" + "\\" + ccs;
		

		Runtime.getRuntime().exec("cmd /C mkdir " + fileUploadDir);

		String fileOutput = fileUploadDir + "\\" + fileName;

		ccbs_export.exportToExcel(fileOutput, fileName, rowSet);

		String ret = "begin ?:='" + "{fileurl:\""
				+ fileOutput.replace("\\", "\\\\") + "\",filename:\""
				+ fileName + "\"}" + "'; end;";
		
		return ret;
		
	} catch (Exception ex) {
		ex.printStackTrace();
		return null;
	}
 }
	
	
	public String chuyen_dulieu_km_mb(
      String psThangNam, String psmay_cn, String psnewdata, String psupdatedata, String psbaocao
	) {
		String s = "";
		String sthang = psThangNam.substring(0,2) + psThangNam.substring(3);
		
		s = "begin ? := " + getSysConst("FuncSchema") + "pkg_dlieu_kmai.chuyen_dlieu_kmai_mb_cuoc("
					+ "'" + sthang + "'"
					+ ",'" + psmay_cn + "'"
					+ ",'" + getUserVar("userID") + "'"
					+ ",'" + psnewdata + "'"
					+ ",'" + psupdatedata + "'"
				+ ");"
				+ "end;";
			
		return s;

	}
	public String chuyen_dulieu_km_vttinh(
      String psThangNam, String psmay_cn, String psnewdata, String psbaocao
	) {
		String s = "";
		String sthang = psThangNam.substring(0,2) + psThangNam.substring(3);
		
		s = "begin ? := " + getSysConst("FuncSchema") + "pkg_dlieu_kmai.chuyen_dlieu_kmai_vttinh("
					+ "'" + sthang + "'"
					+ ",'" + psmay_cn + "'"
					+ ",'" + getUserVar("userID") + "'"
					+ ",'" + psnewdata + "'"					
				+ ");"
				+ "end;";
			
		return s;

	}
	
	public String chuyen_dulieu_km_bltk(
      String psThangNam, String psmay_cn, String psnewdata, String psfiletxt, String psbaocao
	) {
		String s = "";
		String sthang = psThangNam.substring(0,2) + psThangNam.substring(3);
		
		s = "begin ? := " + getSysConst("FuncSchema") + "pkg_dlieu_kmai.chuyen_dlieu_tk_baoluu("
					+ "'" + sthang + "'"
					+ ",'" + psmay_cn + "'"
					+ ",'" + getUserVar("userID") + "'"
					+ ",'" + psnewdata + "'"	
					+ ",'" + psfiletxt + "'"					
				+ ");"
				+ "end;";
			
		return s;

	}
	public String chuyen_dulieu_km_goi_vnpt(
      String psThangNam, String psmay_cn, String psnewdata, String psfiletxt, String psbaocao
	) {
		String s = "";
		String sthang = psThangNam.substring(0,2) + psThangNam.substring(3);
		
		s = "begin ? := " + getSysConst("FuncSchema") + "pkg_dlieu_kmai.chuyen_dlieu_goi_vnpt("
					+ "'" + sthang + "'"
					+ ",'" + psmay_cn + "'"
					+ ",'" + getUserVar("userID") + "'"
					+ ",'" + psnewdata + "'"	
					+ ",'" + psfiletxt + "'"					
				+ ");"
				+ "end;";			
		return s; 

	}
	public String chuyen_dulieu_km_nhom_gddn(
      String psThangNam, String psmay_cn, String psnewdata, String psfiletxt
	) {
		String s = "";
		String sthang = psThangNam.substring(0,2) + psThangNam.substring(3);
		
		s = "begin ? := " + getSysConst("FuncSchema") + "pkg_dlieu_kmai.chuyen_dlieu_nhom_gddn("
					+ "'" + sthang + "'"
					+ ",'" + psmay_cn + "'"
					+ ",'" + getUserVar("userID") + "'"
					+ ",'" + psnewdata + "'"	
					+ ",'" + psfiletxt + "'"					
				+ ");"
				+ "end;";			
		return s; 

	}
	public String chuyen_dulieu_iphone(
      String psThangNam, String psmay_cn, String psnewdata
	) {
		String s = "";
		String sthang = psThangNam.substring(0,2) + psThangNam.substring(3);
		
		s = "begin ? := " + getSysConst("FuncSchema") + "pkg_dlieu_kmai.chuyen_dlieu_iphone("
					+ "'" + sthang + "'"
					+ ",'" + psmay_cn + "'"
					+ ",'" + getUserVar("userID") + "'"
					+ ",'" + psnewdata + "'"					
				+ ");"
				+ "end;";			
		return s; 

	}
	public String chuyen_dulieu_numstore(
      String psThangNam, String psmay_cn, String psnewdata
	) {
		String s = "";
		String sthang = psThangNam.substring(0,2) + psThangNam.substring(3);
		
		s = "begin ? := " + getSysConst("FuncSchema") + "pkg_dlieu_kmai.chuyen_dlieu_numstore("
					+ "'" + sthang + "'"
					+ ",'" + psmay_cn + "'"
					+ ",'" + getUserVar("userID") + "'"
					+ ",'" + psnewdata + "'"					
				+ ");"
				+ "end;";			
		return s; 

	}
}
