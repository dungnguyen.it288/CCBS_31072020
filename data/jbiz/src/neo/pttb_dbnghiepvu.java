package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_dbnghiepvu
extends NEOProcessEx {
	public void run()
	{
		System.out.println("neo.pttb_hdphatsinh was called");
	}
	public String getUrlttKhachHang(String schema,
		String psMA_KH
		,String psMACQ
		,String psTENKH
		,String psSOMAY
		, String psSOMAY_TN		
	)
	{
		String s = "begin ? := " + CesarCode.decode(schema) + "pttb_test_kien.laytt_khachhang('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + psMA_KH + "'"
		+ ",'" + psMACQ + "'"
		+ ",'" + psTENKH + "'"
		+ ",'" + psSOMAY + "'"
		+ ",'" + psSOMAY_TN + "'"
		+");"
		+ " end;";
		return s;
	}
	public String xoadb_nghiepvu(String schema, String somay)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "pttb_test_kien.xoa_dbnghiepvu('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + somay + "'" + ");"
		+ " end;";
		System.out.println(s);
		return s;
	}
	public String capnhatdbNghiepVu(String schema,
       String psMA_KH
       ,String psDIACHI
       ,String psTEN_TB
       ,String psTEN_DB
       ,String psSO_NHA
       ,String piTHUTU_IN
       ,String piTYLE_VAT
       ,String piCUOC_TB
       ,String piCUOC_DV
       ,String psGHICHU
       ,String piLOAITB_ID
       ,String piDOITUONG_ID
       ,String piTRANGTHAI_ID
       ,String piDONVIQL_ID
       ,String piDONVITC_ID       
       ,String pdNGAY_LD
       ,String pdNGAY_TTHOAI       
	   ,String pdNGAY_TD
       ,String piINCHITIET
       ,String piDANGKY_DB
       ,String piGIAPDANH
       ,String piTRACUU_DB
       ,String piPHO_ID
       ,String piPHUONG_ID
       ,String piQUAN_ID
       ,String psSOMAY       
       ,String psCOQUAN
       ,String psDIACHI_CT
       ,String psTEN_TT
       ,String psDIACHI_TT
       ,String psSOTT_NHA
       ,String piPHOTT_ID
       ,String piPHUONGTT_ID
       ,String piQUANTT_ID
       ,String psSOCT_NHA
       ,String piPHOCT_ID
       ,String piPHUONGCT_ID
       ,String piQUANCT_ID
       ,String psMA_CQ	   
		)
	{

		String s = "begin ? := " + CesarCode.decode(schema) + "PTTB_TEST_KIEN.capnhat_dbnghiepvu('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"
				   + psMA_KH  + "'"
				   + ",'" + psDIACHI  + "'"
				   + ",'" + psTEN_TB  + "'"
				   + ",'" + psTEN_DB  + "'"
				   + ",'" + psSO_NHA  + "'"
				   + "," + piTHUTU_IN
				   + "," + piTYLE_VAT
				   + "," + piCUOC_TB
				   + "," + piCUOC_DV
				   + ",'" + psGHICHU + "'"
				   + "," + piLOAITB_ID
				   + "," + piDOITUONG_ID
				   + "," + piTRANGTHAI_ID
				   + "," + piDONVIQL_ID
				   + "," + piDONVITC_ID				   
				   + ",'" + pdNGAY_LD  + "'"
				   + ",'" + pdNGAY_TTHOAI  + "'"
				   + ",'" + pdNGAY_TD  + "'"
				   + "," + piINCHITIET
				   + "," + piDANGKY_DB
				   + "," + piGIAPDANH
				   + "," + piTRACUU_DB
				   + "," + piPHO_ID
				   + "," + piPHUONG_ID
				   + "," + piQUAN_ID
				   + ",'" + psSOMAY  + "'"
			//        danh ba khach hang
				   + ",'" + psCOQUAN  + "'"
				   + ",'" + psDIACHI_CT  + "'"
				   + ",'" + psTEN_TT  + "'"
				   + ",'" + psDIACHI_TT  + "'"
				   + ",'" + psSOTT_NHA  + "'"
				   + "," + piPHOTT_ID
				   + "," + piPHUONGTT_ID
				   + "," + piQUANTT_ID
				   + ",'" + psSOCT_NHA  + "'"
				   + "," + piPHOCT_ID
				   + "," + piPHUONGCT_ID
				   + "," + piQUANCT_ID
				   + ",'" + psMA_CQ  + "'"				   
				   + ");"
				   + "end;";	
		System.out.println(s);
		return s;		
	}

	public String themmoidbNghiepVu(String schema,
	   String psMA_KH
	   , String psDIACHI
	   , String psTEN_TB
	   , String psTEN_DB
	   , String psSO_NHA
	   , String piTHUTU_IN
	   , String piTYLE_VAT
	   , String piCUOC_TB
	   , String piCUOC_DV
	   , String psGHICHU
	   , String piLOAITB_ID
	   , String piDOITUONG_ID
	   , String piTRANGTHAI_ID
	   , String piDONVIQL_ID
	   , String piDONVITC_ID	   
	   , String pdNGAY_LD
	   , String pdNGAY_TTHOAI
	   , String pdNGAY_TD	   
	   , String piINCHITIET
	   , String piDANGKY_DB
	   , String piGIAPDANH
	   , String piTRACUU_DB
	   , String piPHO_ID
	   , String piPHUONG_ID
	   , String piQUAN_ID
	   , String psSOMAY
		)
	{

		String s = "begin ? := " + CesarCode.decode(schema) + "PTTB_TEST_KIEN.themmoi_dbnghiepvu('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"
				   + psMA_KH + "'"
				   + ",'" + psDIACHI + "'"
				   + ",'" + psTEN_TB + "'"
				   + ",'" + psTEN_DB + "'"
				   + ",'" + psSO_NHA + "'"
				   + "," + piTHUTU_IN
				   + "," + piTYLE_VAT
				   + "," + piCUOC_TB
				   + "," + piCUOC_DV
				   + ",'" + psGHICHU + "'"
				   + "," + piLOAITB_ID
				   + "," + piDOITUONG_ID
				   + "," + piTRANGTHAI_ID
				   + "," + piDONVIQL_ID
				   + "," + piDONVITC_ID				   		   
				   + ",'" + pdNGAY_LD + "'"
				   + ",'" + pdNGAY_TTHOAI + "'"
				   + ",'" + pdNGAY_TD + "'"
				   + "," + piINCHITIET
				   + "," + piDANGKY_DB
				   + "," + piGIAPDANH
				   + "," + piTRACUU_DB
				   + "," + piPHO_ID
				   + "," + piPHUONG_ID
				   + "," + piQUAN_ID
				   + ",'" + psSOMAY + "'"
				   + ");"
				   + "end;";
		System.out.println(s);
		return s;
	}
}