
package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;



public class ccbs_tbgt extends NEOProcessEx {
  public void run() {
    System.out.println("Starting Thread");
  }

  
//Lay thong tin cua khuyenmai_id cua tinh xxyyzz. 
 
 public NEOCommonData layds_hinhthuc_kmid_tinhs(
 										 String func_schema,
 										 String schemaCommon,
 										 String khuyenmai_id) 
 										{
   String s= "begin ?:="+schemaCommon+"khuyenmai.layds_hinhthuc_kmid_tinhs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+khuyenmai_id
		+");end;";
	
	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }


    
   public NEOCommonData layds_hinhthuc_km_tinhs( String piDanhSach_KMCK)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/cauhinh/km_hinhthucs_ajax";	
		url_ = url_ + "&"+CesarCode.encode("danhsach_kmck")+"="+piDanhSach_KMCK+"&sid="+Math.random();
			    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  
  
   public NEOCommonData layds_cachgiam_kmid_tinhs(
   			String pihinhthuc_km_id)
  	{
	 	String url_ ="/main";
		url_ = url_ + "?"+CesarCode.encode("configFile")+"=ccbs_kmck/cauhinh/km_cachgiams_ajax";	
		url_ = url_ + "&"+CesarCode.encode("khuyenmai_id")+"="+pihinhthuc_km_id+"&sid="+Math.random();
			    
	    NEOExecInfo nei_ = new NEOExecInfo(url_);
	 	nei_.setDataSrc("VNPBilling");
	    return new NEOCommonData(nei_);
  	}
  	
  
     
  public NEOCommonData xoa_hinhthuc_km_tbgt_tinhs(
  							String  schema,
  							String  schemaCommon,   	  																					  						   			        
					       	String  Khuyenmai_id						
                                   
									 ) {

    String s=    "begin ? := "+CesarCode.decode(schemaCommon)+"DISCOUNT_CDR.xoa_hinhthuc_km_tbgt_tinhs('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+CesarCode.decode(schemaCommon)
		+"','"+getUserVar("sys_dataschema")+"',"+Khuyenmai_id   
        +");"
        +"end;"
        ;
    
   	 System.out.println(s); 	
	 NEOExecInfo nei_ = new NEOExecInfo(s);
	 nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);
  }  
  

	public NEOCommonData ds_nhanvien(int donviql_id)
	{
		
			String url_ =  "/main?";
			url_ = url_ + CesarCode.encode("configFile") + "=khuyenmai/baocao/danhsach_tien_kms/ds_nhanviens&" + CesarCode.encode("donviql_id") + "=" + donviql_id;
			
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}
	
	public NEOCommonData ds_nhanvien_inphieu(int donviql_id)
	{
	    	String url_ = "/main?"; 
	    	url_ = url_+ CesarCode.encode("configFile") + "=khuyenmai/baocao/in_hoadon_kms/ds_nhanviens&" + CesarCode.encode("donviql_id") + "=" + donviql_id;
			NEOExecInfo nei_ = new NEOExecInfo(url_);
	 		nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}
  
  public NEOCommonData laytt_ngay_apdung(   String func_schema,
											String schemaCommon,
											String khuyenmai_id)
	{
		String s = "begin ?:=" + schemaCommon + "khuyenmai.laytt_ngay_apdung('" + getUserVar("userID")
			 + "','" + getUserVar("sys_agentcode")
			 + "','" + schemaCommon
			 + "','" + getUserVar("sys_dataschema")
			 + "'," + khuyenmai_id
			 + ");end;";
		System.out.println(s); 
		
		NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
	}
  
  
  /*Nhu thanh the:
  *chuc nang: lay thong tin thue bao khuyen mai.
  *phuc vu nut tim kiem
  *ngay_pt: 09/10/2008. 
  						
  */
    public NEOCommonData layds_tb_khuyenmais
								(String piPAGEID,
    							   String piREC_PER_PAGE,    							
    							   String ma_tb,
    							   String ngay_apdung, 
    							   String ngay_kt,   							  
    							   String khuyenmai_id,
    							   String trangthai_id) {
    String  s= "/main?"+CesarCode.encode("configFile")+"=ccbs_kmck/capnhat_ds/xuly_danhsach_km/ajax_layds_tb_khuyenmais"
		+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID	           
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE  
		+ "&" +CesarCode.encode("ma_tb")+"="+ma_tb
		+ "&" +CesarCode.encode("ngay_apdung")+"="+ngay_apdung
		+ "&" +CesarCode.encode("ngay_kt")+"="+ngay_kt
		+ "&" +CesarCode.encode("khuyenmai_id")+"="+khuyenmai_id
		+ "&" +CesarCode.encode("trangthai_id")+"="+trangthai_id
        ;   
	System.out.println(s); 
		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_); 
     
  }
  
  
  public NEOCommonData themmoi_ds_khuyenmais(	 
										 String psschema,
  										 String psschemaCommon,   
  										 String psma_tb_1   
										,String pikhuyenmai_id_1										
										,String psghichu
										,String psnguoi_cn
										,String psmay_cn
									
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"khuyenmai.themmoi_ds_khuyenmais('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
        +",'"+psma_tb_1+"'"
        +",'"+pikhuyenmai_id_1+"'"      
		+","+psghichu
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
    
   System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_); 
    
  			
  	}
	
public NEOCommonData capnhat_ds_khuyenmais(
     String psschema
	,String psschemaCommon  
	,String psdsSomay   									
	,String psdsKMID
	,String psdsApdung
	,String psghichu
	,String psnguoi_cn
	,String psmay_cn
									
 ){
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"khuyenmai.capnhat_ds_khuyenmais('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)
        +"','"+psdsKMID
        +"','"+psdsApdung
        +"','"+psdsSomay       
		+"','"+psghichu
		+"','"+psnguoi_cn
		+"','"+psmay_cn
        +"');"
        +"end;";
        
     System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);    
  }  

public NEOCommonData laytt_tb_gioithieu ( String func_schema,
 										 String schemaCommon,
 										 String ma_tb,
 										 String khuyenmai_id) 
 										{
   String s= "begin ?:="+schemaCommon+"DISCOUNT_CDR.laytt_tb_gioithieu('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"','"+ma_tb+"'"
		+","+khuyenmai_id
		+");end;";
	
	System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);  
  }
  
	public NEOCommonData capnhat_tbgt(
  								String psschema,
  										 String psschemaCommon   
  										,String psma_tb_gt
										,String psma_tb_hm
										,String pikhuyenmai_id_1
										,String psghichu
										,String pdthang_bd
										,String pdthang_kt
										,String psnguoi_cn
										,String psmay_cn
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"DISCOUNT_CDR.capnhat_tbgt('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
        +",'"+psma_tb_gt+"'"
		+",'"+psma_tb_hm+"'"
        +",'"+pikhuyenmai_id_1+"'"
		+","+psghichu
	    +","+pdthang_bd
		+","+pdthang_kt
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;		
    System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
  }   
  
 public NEOCommonData themmoi_tbgt(	 String psschema,
  										 String psschemaCommon   
  										,String psma_tb_gt
										,String psma_tb_hm
										,String pikhuyenmai_id_1
										,String psghichu
										,String pdthang_bd
										,String pdthang_kt
										,String psnguoi_cn
										,String psmay_cn
									
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"DISCOUNT_CDR.themmoi_tbgt('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	                   
        +",'"+psma_tb_gt+"'"
		+",'"+psma_tb_hm+"'"
        +",'"+pikhuyenmai_id_1+"'"
		+","+psghichu
	    +","+pdthang_bd
		+","+pdthang_kt
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;
      System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);  
  	}
	
	
 public NEOCommonData xoa_tbgt( String psschema, String psschemaCommon, String psma_tb_GT,String pikhuyenmai_id)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"DISCOUNT_CDR.xoa_tbgt('"+getUserVar("userID")
		+"','"+getUserVar("sys_dataschema")+"'"
        +",'"+psma_tb_GT+"'"
        +","+pikhuyenmai_id      
        +");"
        +"end;"
        ;        
    System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);  
  }

  public NEOCommonData capnhat_hinhthuc_tbgt(
										 String psschema
  										,String psschemaCommon   
										,String pikhuyenmai_id
										,String theo_phantram
										,String phantram_giam
										,String theo_khoanmuc
										,String khoanmuctc_giam
										,String tach_khoanmuctc
										,String theo_dinhmuc
										,String dinhmuc_giam
										,String cuoc_goi
										,String cuoc_ntin
										,String cuoc_khac
										,String psnguoi_cn
										,String psmay_cn
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"DISCOUNT_CDR.capnhat_hinhthuc_km_tbgt('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	
        +",'"+pikhuyenmai_id		
        +"','"+theo_phantram
		+"','"+phantram_giam
		+"','"+theo_khoanmuc
		+"','"+khoanmuctc_giam
		+"','"+tach_khoanmuctc
		+"','"+theo_dinhmuc
		+"','"+dinhmuc_giam
		+"','"+cuoc_goi
		+"','"+cuoc_ntin
	    +"','"+cuoc_khac+"'"
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;		
    System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
  }   
  
  
  
   public NEOCommonData themmoi_hinhthuc_tbgt(
										 String psschema
  										,String psschemaCommon   
										,String pikhuyenmai_id
										,String theo_phantram
										,String phantram_giam
										,String theo_khoanmuc
										,String khoanmuctc_giam
										,String tach_khoanmuctc
										,String theo_dinhmuc
										,String dinhmuc_giam
										,String cuoc_goi
										,String cuoc_ntin
										,String cuoc_khac
										,String psnguoi_cn
										,String psmay_cn
									)
  	{
  			
    String s=    "begin ? := "+CesarCode.decode(psschemaCommon)+"DISCOUNT_CDR.themmoi_hinhthuc_km_tbgt('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")	
		+"','"+getUserVar("sys_dataschema")  	
		+"','"+CesarCode.decode(psschemaCommon)	+"'"	
        +",'"+pikhuyenmai_id		
        +"','"+theo_phantram
		+"','"+phantram_giam
		+"','"+theo_khoanmuc
		+"','"+khoanmuctc_giam
		+"','"+tach_khoanmuctc
		+"','"+theo_dinhmuc
		+"','"+dinhmuc_giam
		+"','"+cuoc_goi
		+"','"+cuoc_ntin
	    +"','"+cuoc_khac+"'"
		+",'"+psnguoi_cn+"'"
		+",'"+psmay_cn+"'"
        +");"
        +"end;"
        ;		
    System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	 	nei_.setDataSrc("VNPBilling");
	    	return new NEOCommonData(nei_);
  }   
  
  
  public NEOCommonData laytt_hinhthuc_km_tbgt ( String func_schema,
 										 String schemaCommon,
 										 String khuyenmai_id) 
 										{
   String s= "begin ?:="+schemaCommon+"DISCOUNT_CDR.laytt_hinhthuc_km_tbgt('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")		
		+"','"+schemaCommon
		+"','"+getUserVar("sys_dataschema")
		+"',"+khuyenmai_id
		+");end;";
	
	System.out.println(s); 		
	NEOExecInfo nei_ = new NEOExecInfo(s);
	nei_.setDataSrc("VNPBilling");
	 return new NEOCommonData(nei_);  
  }
  
}

