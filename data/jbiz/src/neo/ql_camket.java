package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class ql_camket
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.ql_camket was called");
  }
  
  public String laytt_kieuso_thuebao(String pssotb) {
    return "begin ?:=admin_v2.pkg_camket_kieuso.laytt_kieuso_thuebao('"+pssotb+"'); end;";
  }
  
  public String layds_camket(String pskieuso, String pssotb) {
    return "/main?"+CesarCode.encode("configFile")+"=qlsimso/dk_camket/ajax_ds_camket"
        +"&"+CesarCode.encode("kieuso")+"="+pskieuso
		+"&"+CesarCode.encode("sotb")+"="+pssotb
        ;
  }
 
   public String layds_ls_camket(String pssotb) {
    return "/main?"+CesarCode.encode("configFile")+"=qlsimso/dk_camket/ajax_dsls_camket"
        +"&"+CesarCode.encode("sotb")+"="+pssotb
        ;
  }
 
   public String layds_log_camket(String pstungay, String psdenngay, String pssotb) {
    return "/main?"+CesarCode.encode("configFile")+"=qlsimso/dk_camket/ajax_ds_log_camket"
        +"&"+CesarCode.encode("tungay")+"="+pstungay
		+"&"+CesarCode.encode("denngay")+"="+psdenngay
		+"&"+CesarCode.encode("sotb")+"="+pssotb
		+"&"+CesarCode.encode("matinh")+"="+getUserVar("sys_agentcode")
        ;
  }
 
  public String dangky_camket(String psso_tb
						,String pskieuso_id                      
						,String pscuocthang_toithieu                    
						,String psthoigian_sudung                        
						,String psghichu
						,String psip)
{
    String s=  "begin ? := admin_v2.pkg_camket_kieuso.dangky_camket('"+psso_tb+"','"+pskieuso_id+"','"+pscuocthang_toithieu+"','"+psthoigian_sudung+"','"+psghichu+"','"+getUserVar("userID")+"','"+getUserVar("sys_agentcode")+"','"+psip+"'); end;";
	
    return s;
}

public String huy_camket(String psso_tb
						,String pskieuso_id                      
						,String pscuocthang_toithieu                    
						,String psthoigian_sudung                        
						,String psghichu
						,String psip)
{
    String s=  "begin ? := admin_v2.pkg_camket_kieuso.huy_camket('"+psso_tb+"','"+pskieuso_id+"','"+pscuocthang_toithieu+"','"+psthoigian_sudung+"','"+psghichu+"','"+getUserVar("userID")+"','"+getUserVar("sys_agentcode")+"','"+psip+"'); end;";
	
    return s;
}

 public String kiemtra_thuebao(String psso_tb)
{
    String s=  "begin ? := admin_v2.pkg_camket_kieuso.kiemtra_thuebao('"+psso_tb+"','"+getUserVar("userID")+"','"+getUserVar("sys_agentcode")+"'); end;";	
    return s;
}

public String dieuchinh_camket(String psSoTB, String psCuocTT, String psTienDC, String psThangCK, String psGhichu) 
 {
   String s= "begin ?:=admin_v2.pkg_dieuchinh_camket.update_camket('"+psSoTB
		+"','"+psCuocTT		
		+"','"+psTienDC
		+"','"+psThangCK
		+"','"+getUserVar("userID")
		+"','"+psGhichu
		+"');end;";
	return s;
  }

public String xoa_camket(String psSoTB, String psGhichu) 
 {
   String s= "begin ?:=admin_v2.pkg_dieuchinh_camket.delete_camket('"+psSoTB
		+"','"+getUserVar("userID")
		+"','"+psGhichu
		+"');end;";
	return s;
  }  
   
  public String upload_business(
		String upload_id,
		String userid
){
	String s= "begin ?:=admin_v2.pkg_dieuchinh_camket.update_ds_camket('"+upload_id	
		+"','"+userid
		+"');end;";
	return s;
	
}   
   
}
