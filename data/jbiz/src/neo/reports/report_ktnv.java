package neo.reports;
//package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class report_ktnv extends NEOProcessEx {
	public void run() {
		System.out.println("neo.reports was called");
	}
	//Lay danh sach bao cao
   public String tao_tylerocno(String kyhoadon) throws Exception 
	{
		 String s=    "begin ? := admin_v2.pkg_report_nocuoc.tao_tylerocno("		
			+"'"+kyhoadon+"'"
			+",'"+getUserVar("userID")+"'"	
			+");"
			+"end;";
			return this.reqValue("", s);
	}
	
   public String tao_doanhthunocuoc(String kyhoadon) throws Exception 
	{
		 String s=    "begin ? := admin_v2.pkg_report_nocuoc.tao_doanhthunocuoc("		
			+"'"+kyhoadon+"'"
			+",'"+getUserVar("userID")+"'"	
			+");"
			+"end;";
			return this.reqValue("", s);
	}	
	
	public NEOCommonData connectDB() {
			String s=    "begin ? := admin_v2.pkg_report_nocuoc.connectDB; end;" ;    
			System.out.println(s); 	
			NEOExecInfo nei_ = new NEOExecInfo(s);
			//nei_.setDataSrc("VNPBilling");
			return new NEOCommonData(nei_);
	}  	
		
	public NEOCommonData getBCPTTB(    String matinh,
									   String chukyno,
									   String loaithuebao, 											  
									   String kieutb) 	{

	String s="";
	String psschema = "a";//getUserVar("sys_dataschema");	
	String psuserid = "b";//getUserVar("sys_userid");
	String sFuncSchema = "neo.";//getSysConst("sys_funcschema");	
	
	s = "begin ? := NEO.PKG_BAOCAO.baocao_pttb('"
												+ psschema +"','"
												+ psuserid +"','"																																				
												+ matinh +"','"
												+ chukyno +"','"
												+ loaithuebao +"','"
												+ kieutb +"'); end;";	
	System.out.println(s);											
	NEOExecInfo ei_ = new NEOExecInfo(s);
    return  new NEOCommonData(ei_);
  }
}