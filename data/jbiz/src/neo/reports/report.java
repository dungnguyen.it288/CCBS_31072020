package neo.reports;
//package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class report extends NEOProcessEx {
	public void run() {
		System.out.println("neo.gadmin was called");
	}
	//Lay danh sach bao cao
   public String ds_baocao(String piloaibc,String pitenbc)
	{
		String  s= "/main?" + CesarCode.encode("configFile") + "=report/REPORT_FILE_ajax"
		+ "&" + CesarCode.encode("loaibc") +"="+piloaibc
		+ "&" + CesarCode.encode("tenbc")  +"="+pitenbc
		;
		System.out.println(s);
		return s;
	}
	
	public String doc_tinhTP(String pskhuvuc)
	{
		return "/main?" + CesarCode.encode("configFile")+"=report/report_pps/ajax_tinh"
  			+ "&" + CesarCode.encode("khu_vuc") + "=" + pskhuvuc;
	}

 	 public String doc_loaiTB(String psloaitb)
	{
		String  s= "/main?" + CesarCode.encode("configFile")+"=report/report_pttb/ajax_loaitb"
  			+ "&" + CesarCode.encode("loai") + "=" + psloaitb;
		System.out.println(s);
		return s;
	}
	public String doc_loaiMT(String pskieuTB, String psthang)
	{
		String  s= "/main?" + CesarCode.encode("configFile")+"=report/report_pttb/ajax_loaitb"
  			+ "&" + CesarCode.encode("pskieuTB") + "=" + pskieuTB
			+ "&" + CesarCode.encode("psthang") + "=" + psthang;
		System.out.println(s);
		return s;
	}
		
	public NEOCommonData getBCPTTB(    String matinh,
									   String chukyno,
									   String loaithuebao, 											  
									   String kieutb) 	{

	String s="";
	String psschema = "a";//getUserVar("sys_dataschema");	
	String psuserid = "b";//getUserVar("sys_userid");
	String sFuncSchema = "neo.";//getSysConst("sys_funcschema");	
	
	s = "begin ? := NEO.PKG_BAOCAO.baocao_pttb('"
												+ psschema +"','"
												+ psuserid +"','"																																				
												+ matinh +"','"
												+ chukyno +"','"
												+ loaithuebao +"','"
												+ kieutb +"'); end;";	
	System.out.println(s);											
	NEOExecInfo ei_ = new NEOExecInfo(s);
    return  new NEOCommonData(ei_);
  }
	public String tracuu_ls_dkdl(String piPAGEID,String piREC_PER_PAGE,String pseload,String pstungay,String psdenngay,String psso_tb, String psProvider)
	{
		String s= "/main?" + CesarCode.encode("configFile") + "=search/search_dkdl/ajax_search_ls_dkdl"
				+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
				+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
				+ "&" + CesarCode.encode("eload") +"="+pseload	
				+ "&" + CesarCode.encode("tungay") + "=" + pstungay
				+ "&" + CesarCode.encode("denngay") + "=" + psdenngay
				+ "&" + CesarCode.encode("so_tb") + "=" + psso_tb
				+ "&" + CesarCode.encode("provider") + "=" + psProvider
				; 
		System.out.println(s);
		return s;	
	}	
}