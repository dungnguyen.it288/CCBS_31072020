package neo.reports;
//package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class report_hddt extends NEOProcessEx {
	public void run() {
		System.out.println("REPORT HDDT called!");
	}
	public String get_donviql(String tinh) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/bc_hoadon_dt/prama_buucuc"
					+"&"+CesarCode.encode("pstinh") +"="+tinh;			
	}

	public String get_User(String tinh, String donviql) {
		return "/main?"+CesarCode.encode("configFile")+"=pttb/bc_hoadon_dt/prama_User"
					+"&"+CesarCode.encode("pstinh") +"="+tinh+"&"+CesarCode.encode("psdvql") +"="+donviql;						
	}	
}