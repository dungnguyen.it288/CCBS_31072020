package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class pttb_danhbatv
extends NEOProcessEx {
  public void run() {
    System.out.println("neo.pttb_danhbatv was called");
  }

  public String laytt_KhachHang(String schema,String userInput, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"PTTB_TEST_VIET.layds_frmDanhBaTV('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+userInput+"'); end;";
  }
  
  public String laytt_somay(String schema,String maCQ, String Somay) {
    return "begin ?:="+CesarCode.decode(schema)+"PTTB_TEST_VIET.laytt_frmDanhBaTV_somay('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+maCQ+"','"+Somay+"'); end;";
  }
  
  public String get_cbophongban(String value) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/DanhBaTV/ajax_cbophongban"
        +"&"+CesarCode.encode("ma_cq")+"="+value
        ;
  }
  public String getUrlDsKhachHang(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/DanhBaTV/ajax_layds_tabTB"
        +"&"+CesarCode.encode("userinput")+"="+userInput
        ;
  }
  

  public 			 String suadoiTB(String schema
									,String piTHUTU_IN
									,String psSO_NHA
									,String piDANGKY_DB
									,String psTEN_DB
									,String psGHICHU_108
									,String piMACQ_PHU
									,String psTEN_TB
									,String psDIACHI
									,String psMA_CQ
									,String picboLoaiTB
									,String psSOMAY
									,String picboPhongBan
								
									) {
	
    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.suadoiTB_frmdanhbaTV('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+piTHUTU_IN+"',"
								+"'"+psSO_NHA+"',"
								+"'"+piDANGKY_DB+"',"
								+"'"+psTEN_DB+"',"
								+"'"+psGHICHU_108+"',"
								+"'"+piMACQ_PHU+"',"
								+"'"+psTEN_TB+"',"
								+"'"+psDIACHI+"',"
								+"'"+psMA_CQ+"',"
								+"'"+picboLoaiTB+"',"
								+"'"+psSOMAY+"',"
								+"'"+picboPhongBan+"'"
																
        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }
  //function nay dung de sua doi tab KH
  public String suadoiKH(String schema
						,String piMA_CQ
						,String piNGANHNGHE_ID
						,String piDANGKY_DB
						,String piDANGKY_TV
						,String psTEN_CHINH
						,String psTEN_TM
						,String psTEN_TA
						,String psWEBSITE
						,String psEMAIL
						,String psGHICHU
						,String piTRANGTHAI_ID
						,String psDIACHI_CT
						,String psCOQUAN
						,String piLOAIKH_ID
						,String piPHUONG_ID
						,String piPHO_ID
						,String piQUAN_ID
						,String psSO_NHA


) 
{
    String s=    "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.suadoiKH_frmdanhbaTV('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+piMA_CQ+"',"
						+"'"+piNGANHNGHE_ID+"',"
						+"'"+piDANGKY_DB+"',"
						+"'"+piDANGKY_TV+"',"
						+"'"+psTEN_CHINH+"',"
						+"'"+psTEN_TM+"',"
						+"'"+psTEN_TA+"',"
						+"'"+psWEBSITE+"',"
						+"'"+psEMAIL+"',"
						+"'"+psGHICHU+"',"
						+"'"+piTRANGTHAI_ID+"',"
						+"'"+psDIACHI_CT+"',"
						+"'"+psCOQUAN+"',"
						+"'"+piLOAIKH_ID+"',"
						+"'"+piPHUONG_ID+"',"
						+"'"+piPHO_ID+"',"
						+"'"+piQUAN_ID+"',"
						+"'"+psSO_NHA+"'"
        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }

}
