package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;


public class pttb_cntuyenthu
extends NEOProcessEx {
	public void run()
	{
		System.out.println("neo.pttb_cntuyenthu was called");
	}
	public String getUrlttHopdongtl(String schema, String ma_hd, String ma_tb, String user_id)
	{
		String s = "begin ? := " + CesarCode.decode(schema) + "CCBS.laytt_tb('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_hd + "'"
		+ ",'" + ma_tb + "'"
		+ ",'" + user_id + "'"
		+");"
		+ " end;";
		return s;
	}
	public String hoanThanhHopdongTL(String schema, String ma_hd, String ngay_ht)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "CCBS.hoanthanh_hdtl('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_hd + "'"
		+ ",'" + ngay_ht + "'"
		+ ");"
		+ " end;";
		System.out.println(s);
		return s;	
	}
	//Lay danh sach tuyen thu
	public String layds_tuyenthu(String piPAGEID,String piREC_PER_PAGE,String piuserInput, String piMsisdn) 
	{
		String s = "/main?"+CesarCode.encode("configFile")+"=PTTB/CN_TUYENTHU/ajax_layds_tuyenthu"
		+ "&" + CesarCode.encode("sys_pageid") +"="+ piPAGEID	           
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE 
		+ "&" + CesarCode.encode("userInput") + "=" + piuserInput 	
		+ "&" + CesarCode.encode("MsisdnInput") + "=" + piMsisdn 			
		;
		System.out.println(s);
		return s;
	}  
  
	public String xoaTuyenthu(String schema, String manhom)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "PTTB_DULIEU.xoa_tuyenthu('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + manhom + "'" + ");"
		+ " end;";
		System.out.println(s);
		return s;
	}

/*	public String capnhatTuyenThu(String schema   
			,String piINSERTORUPDATE
			,String psMANHOM
			,String psDienThoai
			,String psTENNHOM
			,String piDONVIQL_ID
			,String psMA_BC
			,String psDIACHI
		)
	{
		String s = "begin ? := admin_v2.pttb_chucnang.capnhat_tuyenthu('"+getUserVar("userID")
					+"','"+getUserVar("sys_agentcode")
					+"','"+getUserVar("sys_dataschema")+"',"
					+ piINSERTORUPDATE					
					+ ",'" + psMANHOM + "'"
					+ ",'" + psDienThoai + "'"
					+ ",'" + psTENNHOM + "'"
					+ "," + piDONVIQL_ID
					+ ",'" + psMA_BC + "'"		
					+ ",'" + psDIACHI + "'"					
					+ ");"
					+ "end;";			
		System.out.println(s);
		return s;		
	}*/
	public String capnhatTuyenThu(String schema   
			,String piINSERTORUPDATE
			,String psMANHOM
			,String psDienThoai
			,String psTENNHOM
			,String piDONVIQL_ID
			,String psMA_BC
			,String psSMSDEBT
			,String psDIACHI
		)
	{
		String s = "begin ? := admin_v2.pttb_chucnang.capnhat_tuyenthu('"+getUserVar("userID")
					+"','"+getUserVar("sys_agentcode")
					+"','"+getUserVar("sys_dataschema")+"',"
					+ piINSERTORUPDATE					
					+ ",'" + psMANHOM + "'"
					+ ",'" + psDienThoai + "'"
					+ ",'" + psTENNHOM + "'"
					+ "," + piDONVIQL_ID
					+ ",'" + psMA_BC + "'"		
					+ ",'" + psSMSDEBT + "'"		
					+ ",'" + psDIACHI + "'"					
					+ ");"
					+ "end;";			
		System.out.println(s);
		return s;		
	}
	
	//Tuyen thu
	//Lay danh sach tuyen thu
	public String ds_tuyenthu(String piPAGEID,String piREC_PER_PAGE,String piuserInput) 
	{
		String s = "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/tuyen/ajax_ds_tuyenthu"
		+ "&" + CesarCode.encode("sys_pageid") +"="+ piPAGEID	           
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE 
		+ "&" + CesarCode.encode("userInput") + "=" + piuserInput 		
		;
		System.out.println(s);
		return s;
	}  
	
	public String UpdateTuyenThu(String schema   
			,String piINSERTORUPDATE
			,String psMATUYEN
			,String psTENTUYEN
			,String piMA_NV
			,String psMA_BC
			,String psGHICHU
		)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "PTTB_DULIEU.danhmuc_update_tuyenthu('"+getUserVar("userID")
					+"','"+getUserVar("sys_agentcode")
					+"','"+getUserVar("sys_dataschema")+"',"
					+ piINSERTORUPDATE					
					+ ",'" + psMATUYEN + "'"
					+ ",'" + psTENTUYEN + "'"
					+ ",'" + piMA_NV + "'"
					+ ",'" + psMA_BC + "'"		
					+ ",'" + psGHICHU + "'"					
					+ ");"
					+ "end;";			
		System.out.println(s);
		return s;		
	}
	
	public String DelTuyenthu(String schema, String matuyen)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "PTTB_DULIEU.danhmuc_delete_tuyenthu('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + matuyen + "'" + ");"
		+ " end;";
		System.out.println(s);
		return s;
	}
}