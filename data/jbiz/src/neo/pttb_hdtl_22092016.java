package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_hdtl
extends NEOProcessEx {
	public void run()
	{
		System.out.println("neo.pttb_hdtl was called");
	}
	
	public String getUrlDSHD(int trangthaihd_id)
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/HDTL/ajax_layds_hopdong"
		+"&"+CesarCode.encode("trangthaihd_id")+"="+trangthaihd_id
        ;
	}
	
	public String getUrlttHopdongtl(String schema, String ma_hd, String ma_tb, String user_id)
	{
		String s = "begin ? := " + schema + "pkg_canhuy_tb.laytt_tb('"
		+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")
		+"','" + ma_hd + "'"
		+ ",'" + ma_tb + "'"
		+");"
		+ " end;";
		return s;
	}
	public String hoanThanhHopdongTL(String schema, String ma_hd, String ngay_ht)
	{
		String s = "begin ? := admin_v2.pttb_tracuu.hoanthanh_hdtl('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_hd + "'"
		+ ",'" + ngay_ht + "'"
		+ ");"
		+ " end;";
		return s;	
	}
	/*ngocntn:31/01/2013
	public String hoanThanhHopdongTL(String schema, String ma_hd, String ngay_ht)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "PTTB_HTHD.hoanthanh_hdtl('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_hd + "'"
		+ ",'" + ngay_ht + "'"
		+ ");"
		+ " end;";
		return s;	
	}*/
	
	public String xoaHopdongtl(String schema, String ma_hd)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "pttb_laphd.xoa_hdtl('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_hd + "'" + ");"
		+ " end;";
		System.out.println(s);
		return s;
	}
	public String HDTL_GuidenCatMo(String schema, String ma_hd)
	{
		//String s = "begin ? :=" + CesarCode.decode(schema) + "pttb_dhanh.HDTL_GuidenCatMo('"+getUserVar("userID")
		String s = "begin ? :=admin_v2.pkg_api_interface.HDTL_GuidenCatMo('"+getUserVar("userID")
		
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_hd + "'" 
		+ ");"
		+ " end;";
		System.out.println(s);
		return s;
	}
	public String HDTL_GuidenCatMo_sps(String schema, String ma_hd)
	{
		String s = "begin ? :=admin_v2.pkg_api_interface.hdtl_guidencatmo_sps('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_hd + "'" 
		+ ");"
		+ " end;";
		System.out.println(s);
		return s;
	}
	
	
	public String HDTL_PhucHoi_GuidenCatMo(String schema, String ma_hd)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "pttb_dhanh.HDTL_Phuchoi_GuidenCatMo('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_hd + "'" 
		+ ");"
		+ " end;";
		System.out.println(s);
		return s;
	}
	public String capnhatHDThanhLy(String schema   
        ,String psMA_HD
        ,String psCOQUAN
        ,String pdNGAY_LAPHD
        ,String piLOAITB_ID
        ,String psMA_TB
        ,String psMA_KH
        ,String psGHICHU
        ,String psDIACHI_CT
        ,String psDIACHI_TT
        ,String psTEN_TT
        ,String psTEN_TB
        ,String psDIACHI
        ,String psUSER_ID
		,String psKIEUTL
		,String psLOAIHINHTT
		,String psHINHTHUC_TL
		)
	{
		String s = "begin ? :=ADMIN_V2.PTTB_HMM.capnhat_hopdong_tl_db_new('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"
					+ 		 psMA_HD + "'"
					+ ",'" + psCOQUAN + "'"
					+ ",'" + pdNGAY_LAPHD + "'"
					+ ",'" + piLOAITB_ID + "'"
					+ ",'" + psMA_TB + "'"
					+ ",'" + psMA_KH + "'"
					+ "," + psGHICHU
					+ "," + psDIACHI_CT + ""
					+ "," + psDIACHI_TT + ""
					+ ",'" + psTEN_TT + "'"
					+ ",'" + psTEN_TB + "'"
					+ "," + psDIACHI + ""
					+ ",'" + psKIEUTL + "'"
					+ ",'" + psLOAIHINHTT + "'"
					+ ",'" + psHINHTHUC_TL + "'"
					+ ");"
					+ "end;";			
		System.out.println("View"+s);
		return s;		
	}
	//Danh sach hop dong chua cat mo
	public String layds_hdtl_catmo()
	{
		return "/main?"+CesarCode.encode("configFile")+"=pttb/laphd/dhtl/ajax_layds_hopdong";
	}
	
	// Phuc hoi thue bao thanh ly tam thoi
	public String HDTL_PhucHoi_CTP(String schema, String ma_hd)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "pttb_dhanh.hdtl_phuchoi_CTP('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+ma_hd+"'" 
		+ ");"
		+ " end;";
		System.out.println(s);
		return s;
	}
	// Gui CMDV  thue bao chuyen mang tam thoi
	public String HDTL_GuidenCatMo_DB(String schema, String ma_tb)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) + "PTTB_DHANH.HDTL_GuidenCatMo_DB('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+ma_tb+"'" 
		+ ");"
		+ " end;";
		System.out.println(s);
		return s;
	}
	// Hoan thanh  thue bao chuyen mang tam thoi
	public String hoanThanhHopdongTL_DB(String schema, String ma_tb, String ngay_ht)
	{
		String s = "begin ? :=" + CesarCode.decode(schema) +"PTTB_HTHD.hoanthanh_hdtl_db('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_tb + "'"
		+ ",'" + ngay_ht + "'"
		+ ");"
		+ " end;";
		return s;	
	}
	
	/* Huy goi khi thanh ly thue bao
	   Natuan - 05/09/2012
	*/
	public String huygoi_tlhd(String ma_tb, String ip_)
	{
		String s = "begin ? := ADMIN_V2.PTTB_CHUCNANG.huygoi_thanhlyHD('"+ma_tb
		+"','"+ip_
		+"','"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")+"'"
		+ ");"
		+ " end;";
		return s;	
	}
	public String get_status_sps(String funcid)
	{
	
		String s = "begin ? := admin_v2.status_use_hlr_sps('"+funcid+ "');"
		+ " end;";
		return s;	
	}
}