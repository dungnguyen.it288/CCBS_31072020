package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;

public class pttb_tracuu
extends NEOProcessEx {
	public void run()
	{
		System.out.println("neo.pttb_tracuu was called");
	}

  public String get_cbophongban(String psMacq) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/danhmuc/ajax_phongban"
		+"&"+CesarCode.encode("macq") + "=" + psMacq
        ;
	}

  public String load_cbophuong(String pmahuyen) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/TRACUU/ajax_cbophuong"
        +"&"+CesarCode.encode("mahuyen")+"="+pmahuyen
        ;
   }
   public String load_DSpho(String piphuong_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/Danhmuc/ajax_DSpho"
        +"&"+CesarCode.encode("phuong_id")+"="+piphuong_id
        ;
   }
   public String nhanviennullhni(String piPAGEID,String piREC_PER_PAGE,String psSomay,int psDonvi,int psMabc)
  {
    return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/tracuu/nhanviennull/ajax_frmnhanviennull"
        + "&" + CesarCode.encode("page_num") +"="+ piPAGEID
    + "&" + CesarCode.encode("page_rec") + "=" + piREC_PER_PAGE
    + "&" + CesarCode.encode("somay") + "=" + psSomay
    + "&" + CesarCode.encode("donvi") + "=" + psDonvi
    + "&" + CesarCode.encode("mabc") + "=" + psMabc;
  }
	public String tracuuhd(
		String piPAGEID
		,String piREC_PER_PAGE
		,String psTEN_CQ
		, String psDIACHICQ
		, String psSO_GT
		, String psTENTB
		, String psDIACHITB
		, String psMATB
		, String psTENTT
		, String psSOSIM
		, String psKH_RR
		, String psTRANGTHAI
		, String psMAKH
		, String piHDORTB
		, String psMACQ
		)
	{
		return "/main?" + CesarCode.encode("configFile") + "=pttb/tracuu/tracuu_hd/ajax_DStracuu"
			+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
			+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
			+ "&" + CesarCode.encode("tencq") + "=" + psTEN_CQ
			+ "&" + CesarCode.encode("diachicq") + "=" + psDIACHICQ
			+ "&" + CesarCode.encode("so_gt") + "=" + psSO_GT
			+ "&" + CesarCode.encode("tentb") + "=" + psTENTB
			+ "&" + CesarCode.encode("diachitb") + "=" + psDIACHITB
			+ "&" + CesarCode.encode("matb") + "=" + psMATB
			+ "&" + CesarCode.encode("tentt") + "=" + psTENTT
			+ "&" + CesarCode.encode("sosim") + "=" + psSOSIM
			+ "&" + CesarCode.encode("kh_rr") + "=" + psKH_RR
			+ "&" + CesarCode.encode("trangthai") + "=" + psTRANGTHAI
			+ "&" + CesarCode.encode("makh") + "=" + psMAKH
			+ "&" + CesarCode.encode("hdortb") + "=" + piHDORTB
			+ "&" + CesarCode.encode("macq") + "=" + psMACQ
			;
	}

	// _____________Tra cuu thong tin khuyen mai_____________________
	public String tracuukhuyenmai(String piPAGEID,String piREC_PER_PAGE,String psCHU_KY,String psKhoanMuc,String psDonVi,String psSomay)
	{
		return "/main?" + CesarCode.encode("configFile") + "=pttb/tracuu/ajax_frmTraCuuKhuyenMai"
			+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
			+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
			+ "&" + CesarCode.encode("chuky") + "=" + psCHU_KY
			+ "&" + CesarCode.encode("khoanmuc") + "=" + psKhoanMuc
			+ "&" + CesarCode.encode("donvi") + "=" + psDonVi
			+ "&" + CesarCode.encode("somay") + "=" + psSomay
			;
	}
	// _____________Tra cuu thong tin khuyen mai cuoc lap dat_____________________
	public String tracuukhuyenmai_ld(String piPAGEID,String piREC_PER_PAGE,String psma_tb,String psMa_kh,String pscmt,String psTen_tt,String psDiachi)
	{
		return "/main?" + CesarCode.encode("configFile") + "=pttb/tracuu/tracuu_km/ajax_frmTraCuuKhuyenMai"
			+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
			+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
			+ "&" + CesarCode.encode("ma_tb") + "=" + psma_tb
			+ "&" + CesarCode.encode("Ma_kh") + "=" + psMa_kh
			+ "&" + CesarCode.encode("cmt")   + "=" + pscmt
			+ "&" + CesarCode.encode("ten_tt")+ "=" + psTen_tt
			+ "&" + CesarCode.encode("diachi")+ "=" + psDiachi
			;
	}
	// _____________Tra cuu thong tin so du tai khoan_____________________
	public String tracuusodutaikhoan(String psTHANG,String psMA_KH,String psSOMAY)
	{
		return "/main?" + CesarCode.encode("configFile") + "=pttb/tracuu/ajax_frmTraCuuSoDuTK"
			+ "&" + CesarCode.encode("thang") + "=" + psTHANG
			+ "&" + CesarCode.encode("makh") + "=" + psMA_KH
			+ "&" + CesarCode.encode("somay") + "=" + psSOMAY
			;
	}

	//_______________________________________________________

    public String update_frmphos(String schema
									,String piPHUONG_ID
									,String psDSpho_id

									) {

    String s= "begin ? := "+CesarCode.decode(schema)+"PTTB_TEST_VIET.danhmuc_update_frmPhos('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"',"
       							+piPHUONG_ID+","
								+"'"+psDSpho_id+"'"

        +");"
        +"end;"
        ;
    System.out.println(s);
    return s;

  }
  public String laytt_tbcd(String schema,String ma_tb, String userid) {
    return "begin ?:="+CesarCode.decode(schema)+"pttb_tracuu.laytt_tbcd('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','"+ma_tb+"'); end;";
  }
  public String laytt_tbdd(String schema, String ma_tb, String userid)
  {
	  return "begin ?:=" + CesarCode.decode(schema) + "pttb_tracuu.laytt_tbdd('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_tb  + "'); end;";
  }
  public String laytt_tbins(String schema, String ma_tb, String userid)
  {
	  return "begin ?:=" + CesarCode.decode(schema) + "pttb_tracuu.laytt_tbins('"+getUserVar("userID")
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")+"','" + ma_tb + "'); end;";
  }
  public String layds_hopdong_theo_tb(String psMA_TB) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tracuu/tttb/ajax_layds_hopdong"
        +"&"+CesarCode.encode("ma_tb")+"="+psMA_TB
        ;
  }
  public String layds_notonghop(String psMA_TB) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tracuu/tttb/ajax_lay_no_tonghop"
        +"&"+CesarCode.encode("ma_tb")+"="+psMA_TB
        ;
  }
  public String layds_nochitiet(String psMA_TB,String piLOAITIEN_ID) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tracuu/tttb/ajax_lay_no_chukyno"
        +"&"+CesarCode.encode("ma_tb")+"="+psMA_TB
		+"&"+CesarCode.encode("loaitien_id")+"="+piLOAITIEN_ID
        ;
  }
  public String getUrlDsnhanvienTC(String sdk) {
    return "/main?"+CesarCode.encode("configFile")+"=quangbinh/ajax_layDSnhanvienTC"
        +"&"+CesarCode.encode("sdk")+"="+sdk
        ;
  }

    /*
    NINHBD: Lay danh sach nhan vien null
    Ngay viet: 20/12/2008
    */

   public String nhanviennull(String piPAGEID,String piREC_PER_PAGE,String psSomay,int psDonvi,int psMabc)
	{
		return "/main?" + CesarCode.encode("configFile") + "=ccbs_qltn/tracuu/nhanviennull/ajax_frmnhanviennull"
        + "&" + CesarCode.encode("page_num") +"="+ piPAGEID
		+ "&" + CesarCode.encode("page_rec") + "=" + piREC_PER_PAGE
		+ "&" + CesarCode.encode("somay") + "=" + psSomay
		+ "&" + CesarCode.encode("donvi") + "=" + psDonvi
		+ "&" + CesarCode.encode("mabc") + "=" + psMabc;
	}
	
	public String layDSBCTheoDV(String userInput) {
    return "/main?"+CesarCode.encode("configFile")+"=ccbs_qltn/tracuu/nhanviennull/frmnhanviennull_ajax_buucuc"
        +"&"+CesarCode.encode("donvi")+"="+userInput
        ;
  } 

  //Lay danh sach cac so may la
  public String tracuusomayla(String piPAGEID,String piREC_PER_PAGE,String psTHANG,String psMA_TB)
	{
		return "/main?" + CesarCode.encode("configFile") + "=pttb/tracuu/ajax_frmTraCuuMayLa"
			+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
			+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
			+ "&" + CesarCode.encode("thang") + "=" + psTHANG
			+ "&" + CesarCode.encode("matb") + "=" + psMA_TB
			;
	}

 //Lay danh sach lich su khach hang
	public String layds_frmLSKH(String piPAGEID,String piREC_PER_PAGE,String txtMaKH,String txtTenKH)
	{
    return "/main?"+CesarCode.encode("configFile")+"=PTTB/TRACUU/ajax_layds_frmLSKH"
        + "&" + CesarCode.encode("sys_pageid") +"="+ piPAGEID
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
		+ "&" + CesarCode.encode("makh") +"="+ txtMaKH
		+ "&" + CesarCode.encode("tenkh") +"="+ txtTenKH
        ;
  }

  //Tim kiem theo ma khach
	public String timtheomakh(String schema,String psMaKH,String psTenKH)
		{ String s= "begin ? := "+schema+"PTTB_TRACUU.tracuu_khachhang("
							   +"'"+psMaKH+"',"
							   +"'"+psTenKH+"',"
							   +"'"+getUserVar("sys_dataschema")+"',"
							   +"'"+getUserVar("sys_agentcode")+"',"
							   +"'"+getUserVar("userID")+"'"
							   +");"
							   +"end;"
	                           ;
		System.out.println(s);
		return s;
	}

//Lay danh sach lich su danh ba
	public String layds_frmLSDB(String piPAGEID,String piREC_PER_PAGE,String txtSoMay,String txtMaKH,String piThaoTac)
	{
    return "/main?"+CesarCode.encode("configFile")+"=PTTB/TRACUU/ajax_layds_frmLSDB"
        + "&" + CesarCode.encode("sys_pageid") +"="+ piPAGEID
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
		+ "&" + CesarCode.encode("somay") +"="+ txtSoMay
		+ "&" + CesarCode.encode("makh") +"="+ txtMaKH
		+ "&" + CesarCode.encode("thaotac") +"="+ piThaoTac
        ;
  }

  //Tim kiem theo so may cua lich su danh ba
	public String timtheomatb_db(String schema,String psSoMay,String psMaKH,String psTenKH)
		{ String s= "begin ? := "+schema+"PTTB_TRACUU.tracuu_kh_danhba("
							   +"'"+psSoMay+"',"
							   +"'"+psMaKH+"',"
							   +"'"+psTenKH+"',"
							   +"'"+getUserVar("sys_dataschema")+"',"
							   +"'"+getUserVar("sys_agentcode")+"',"
							   +"'"+getUserVar("userID")+"'"
							   +");"
							   +"end;"
	                           ;
		System.out.println(s);
		return s;
	}

	//Lay danh sach lich su hop dong
	public String tracuu_frmLSHD(String piPAGEID,String piREC_PER_PAGE,String piloaihd,String pithaotac,String pitentruong,String pimahd,String pitungay,String pidenngay,String pinguoicn,String pimaycn)
	{
    String s= "/main?"+CesarCode.encode("configFile")+"=PTTB/TRACUU/tracuu_lichsu_hd/ajax_frmLichSuHopDong"
        + "&" + CesarCode.encode("sys_pageid") +"="+ piPAGEID
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
		+ "&" + CesarCode.encode("loaihd") +"="+ piloaihd
		+ "&" + CesarCode.encode("thaotac") +"="+ pithaotac
		+ "&" + CesarCode.encode("tentruong") +"="+ pitentruong
		+ "&" + CesarCode.encode("mahd") +"="+ pimahd
		+ "&" + CesarCode.encode("tungay") +"="+ pitungay
		+ "&" + CesarCode.encode("denngay") +"="+ pidenngay
		+ "&" + CesarCode.encode("nguoicn") +"="+ pinguoicn
		+ "&" + CesarCode.encode("maycn") +"="+ pimaycn
		;
	System.out.println(s);
	return s;
  }

	public String load_cbotentruong(String piloaihd_id) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tracuu/tracuu_lichsu_hd/ajax_cbotentruong"
        +"&"+CesarCode.encode("loaihd")+"="+piloaihd_id
        ;
   }

   /*Lay danh sach thue bao trong danh ba
	Nguoi cap nhat: Tuanna
	Ngay cap nhat: 2007.12.04 */
	public String tracuu_tb_danhba(String piPAGEID,String piREC_PER_PAGE,String pisomay,String pidonvi_ql,String piten_tb,String pidiachi_tb)
	{
    String s= "/main?"+CesarCode.encode("configFile")+"=PTTB/chucnang/xoa_danhba/ajax_layds_xoa_danhba"
        + "&" + CesarCode.encode("sys_pageid") +"="+ piPAGEID
		+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
		+ "&" + CesarCode.encode("somay") +"="+ pisomay
		+ "&" + CesarCode.encode("donvi_ql") +"="+ pidonvi_ql
		+ "&" + CesarCode.encode("ten_tb") +"="+ piten_tb
		+ "&" + CesarCode.encode("diachi_tb") +"="+ pidiachi_tb
		;
	System.out.println(s);
	return s;
  }

   //Lay danh sach cac so may la toan tinh
  public String tracuusomayla_vina(String piPAGEID,String piREC_PER_PAGE,String psTHANG,String psTINH,String psMA_TB,String psSAPXEP,String psKIEU)
	{
		return "/main?" + CesarCode.encode("configFile") + "=pttb/tracuu/mayla_vina/ajax_MayLavina"
			+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
			+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
			+ "&" + CesarCode.encode("thang") + "=" + psTHANG
			+ "&" + CesarCode.encode("tinh") + "=" + psTINH
			+ "&" + CesarCode.encode("matb") + "=" + psMA_TB
			+ "&" + CesarCode.encode("sapxep") + "=" + psSAPXEP
			+ "&" + CesarCode.encode("kieu") + "=" + psKIEU
			;
	}

  public String lay_tong_mayla(String schema,String psthang,String pstinh,String pssomay)
	{
		String  s=  "begin ?:= "+schema+"PTTB_TRACUU.lay_tong_mayla("
					+"'"+getUserVar("sys_dataschema")+"',"
					+"'"+getUserVar("sys_agentcode")+"',"
					+"'"+getUserVar("userID")+"',"
					+"'"+psthang+"',"
					+"'"+pstinh+"',"
					+"'"+pssomay+"'"
					+");"
					+"end;"
					;
		return s;
	}

	 //Lay danh sach bao cao
  public String ds_baocao(String piloaibc,String pitenbc)
	{
		String  s= "/main?" + CesarCode.encode("configFile") + "=gdkh/REPORT_FILE_ajax"
		+ "&" + CesarCode.encode("loaibc") +"="+piloaibc
		+ "&" + CesarCode.encode("tenbc")  +"="+pitenbc
		;
		System.out.println(s);
		return s;
	}

	/* Lay danh sach cac so may co cuoc phat sinh lon
	Ngay 2008/03/29*/
  public String tracuusomay_cuocps(String piPAGEID,String piREC_PER_PAGE,String psTHANG,String psDONVI,String psSOMAY,String psTONGTIEN)
	{
		return "/main?" + CesarCode.encode("configFile") + "=pttb/tracuu/tracuu_somay_cuocps/ajax_frmTraCuuMayCuocPS"
			+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
			+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
			+ "&" + CesarCode.encode("thang") + "=" + psTHANG
			+ "&" + CesarCode.encode("donvi") + "=" + psDONVI
			+ "&" + CesarCode.encode("somay") + "=" + psSOMAY
			+ "&" + CesarCode.encode("tongtien") + "=" + psTONGTIEN
			;
	}

/* Lay danh sach cac so may co cuoc thue bao>0 va cuoc phat sinh=0
	Ngay 2008/04/09*/
  public String tracuusomay_cuocps0(String piPAGEID,String piREC_PER_PAGE,String psTHANG,String psSOMAY)
	{
		return "/main?" + CesarCode.encode("configFile") + "=pttb/tracuu/tracuu_somay_cuocps0/ajax_frmTraCuuMayCuocPS0"
			+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
			+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
			+ "&" + CesarCode.encode("thang") + "=" + psTHANG
			+ "&" + CesarCode.encode("somay") + "=" + psSOMAY
			;
	}

  public String load_cbotrangthai(String psschema,String pskieu) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tracuu/tracuu_hd/ajax_cboTrangThai"
			+"&"+CesarCode.encode("pschema")+"="+psschema
			+"&"+CesarCode.encode("pkieu")+"="+pskieu
			;
   }
//Lay danh sach cac so may la
  public String tracuu_may_sai_ngayhd(String piPAGEID,String piREC_PER_PAGE,String psTHANG,String psMA_TB)
	{
		return "/main?" + CesarCode.encode("configFile") + "=pttb/tracuu/frmMaySaiNgayHD_ajax"
			+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
			+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
			+ "&" + CesarCode.encode("thang") + "=" + psTHANG
			+ "&" + CesarCode.encode("matb") + "=" + psMA_TB
			;
	}

	// Lay thong tin thue bao khoi phuc
	public String layds_tbkhoiphuc(String psSoMay)
	{
    return "/main?"+CesarCode.encode("configFile")+"=PTTB/chucnang/khoiphuc_tb/ajax_layds_frmKhoiPhucTB"
        + "&" + CesarCode.encode("somay") +"="+ psSoMay
        ;
  }
	//  Lay thong tin thoi gian hoat dong
    public String laytt_ngay_ld(String schema,String psma_tb)
	  {
		String  s= "begin ?:="+schema+"pttb_tracuu.lay_ngay_cat_mo("
						 +"'"+getUserVar("sys_dataschema")+"',"
						 +"'"+psma_tb+"'"
						 +");"
						 +"end;"
						 ;
		System.out.println(s);
		return s;
		}
	//  Lay thong tin nhap thue bao tra truoc
    public String laytt_nguoinhap(String schema,String psma_tb)
	  {
		String  s= "begin ?:="+schema+"try.laytt_nguoinhap("
						 +"'"+getUserVar("sys_dataschema")+"',"
						 +"'"+psma_tb+"'"
						 +");"
						 +"end;"
						 ;
		System.out.println(s);
		return s;
		}
	
	public String usernnull(String piPAGEID,String piREC_PER_PAGE,String piUser)
	  {
		return "/main?" + CesarCode.encode("configFile") + "=pttb/user_null/ajax_frmusernull"
        + "&" + CesarCode.encode("page_num") +"="+ piPAGEID
		+ "&" + CesarCode.encode("page_rec") + "=" + piREC_PER_PAGE
		+ "&" + CesarCode.encode("user_id") + "=" + piUser;
	  }		
	  
	 //Lay danh sach cac so may co cuoctb ma khong co trong danh ba
  public String somay_co_cuoctb_khong_trongdb(
		String piPAGEID,String piREC_PER_PAGE,String psTHANG,String psMA_TB)
	{
		return "/main?" + CesarCode.encode("configFile") + "=pttb/tracuu/have_cuoctb_notin_db/ajax_have_cuoctb_notin_db"
			+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
			+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
			+ "&" + CesarCode.encode("thang") + "=" + psTHANG
			+ "&" + CesarCode.encode("matb") + "=" + psMA_TB
			;
	}	
		/* 
	Tra cuu cac dai ly trung so cmnd
	Ngay 2009/05/23
	*/
    public String tracuu_trung_socmnd(String piPAGEID,String piREC_PER_PAGE,String psloai,String psdktk)
	{
		String s= "/main?" + CesarCode.encode("configFile") + "=PTTB/TRACUU/TRACUU_KH/ajax_frm_tracuu"
				+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
				+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
				+ "&" + CesarCode.encode("hinhthuc") + "=" + psloai
				+ "&" + CesarCode.encode("dktk") + "=" + psdktk
				; 
		return s;	
	}
	
	
		  public String prepaid_trung_sogt(String piPAGEID,String piREC_PER_PAGE,String prs_idnumber)
	{
		String s= "/main?" + CesarCode.encode("configFile") + "=PTTB/TRACUU/trungso_cmt/ajax_frm_tracuu"
				+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
				+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
				+ "&" + CesarCode.encode("idnumber") + "=" + prs_idnumber
			
				; 
		return s;	
	}
	
   // Date: 02/11/2009
    public String layDS_thuebao_TK(String pisomay) {
    
	return "/main?"+CesarCode.encode("configFile")+"=vinacore/moneypost_ajax"
		+ "&" + CesarCode.encode("somay") +"="+pisomay	           
		;		
    }   
	 public String timtheosotb_miendc( String func_schema,
 									String psMaTB,
    							   	String psMaKH,   
									String psTenKH,						
 	    							String pspage_num,
									String pspage_rec
									) 
 {
   String s= "begin ?:="+func_schema+"PTTB_TRACUU.tracuu_khachhang_miendc('"+getUserVar("userID")
		+"','"+getUserVar("sys_dataschema")+"'"
		+",'"+psMaTB+"'"
		+",'"+psMaKH+"'"
		+",'"+psTenKH+"'"
		+",'"+pspage_num+"'"
		+",'"+pspage_rec
		+"');end;";
	
	return s;
  } 
  
  public String layds_khachhang_miendc(String psMaTB,
  										String psMaKH,
										String psTenKH,
    							   		String pspage_num,
										String pspage_rec
										) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tracuu/iphone_kh_miendc/ajax_frmTraCuu_iPhone_miendc"
		+"&"+CesarCode.encode("somay") + "=" + psMaTB
		+"&"+CesarCode.encode("ma_kh") + "=" + psMaKH
		+"&"+CesarCode.encode("ten_tb") + "=" + psTenKH
		+"&"+CesarCode.encode("page_num") + "=" + pspage_num
		+"&"+CesarCode.encode("page_rec") + "=" + pspage_rec
        ;
	}
  public String layds_khachhang_datcoc(String psMaTB,
  										String psMaKH,
										String psTenKH,
										String psKIEU_DC,
    							   		String pspage_num,
										String pspage_rec
										) {
    return "/main?"+CesarCode.encode("configFile")+"=pttb/tracuu/tracuu_datcoc/ajax_frmTraCuu_datcoc"
		+"&"+CesarCode.encode("somay") + "=" + psMaTB
		+"&"+CesarCode.encode("ma_kh") + "=" + psMaKH
		+"&"+CesarCode.encode("ten_tb") + "=" + psTenKH
		+"&"+CesarCode.encode("kieu_dc") + "=" + psKIEU_DC
		+"&"+CesarCode.encode("page_num") + "=" + pspage_num
		+"&"+CesarCode.encode("page_rec") + "=" + pspage_rec
        ;
	}
	
  	//DATE 24-05-2011, ham` tra cuu du lieu upload cuoc cao (Thuyvtt2)
	 public String dulieu_upload_cuoc_cao(String piPAGEID,
										  String piREC_PER_PAGE,
										  String psTINH,
										  String psTUNGAY,
										  String psDENNGAY,
										  String psSOMAY,
										  String psLOAIBANG,
										  String psKIEU)
	{
		return "/main?" + CesarCode.encode("configFile") + "=PTTB/TRACUU/Tracuu_dulieu_upload_cuoc/ajax_frm_cuoc_cao"
			+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
			+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
			+ "&" + CesarCode.encode("tinh") + "=" + psTINH
			+ "&" + CesarCode.encode("tungay") + "=" + psTUNGAY
			+ "&" + CesarCode.encode("denngay") + "=" + psDENNGAY
			+ "&" + CesarCode.encode("somay") + "=" + psSOMAY
			+ "&" + CesarCode.encode("loaibang") + "=" + psLOAIBANG
			+ "&" + CesarCode.encode("kieu") + "=" + psKIEU
			;
	}
	//DATE 24-05-2011, ham` tra cuu du lieu upload cuoc ir ( Thuyvtt2)
	public String dulieu_upload_cuoc_ir(String piPAGEID,
										String piREC_PER_PAGE,
										String psTINH,
										String psTUNGAY,
										String psDENNGAY,
										String psSOMAY,
										String psLOAIBANG,
										String psKIEU)
	{
		return "/main?" + CesarCode.encode("configFile") + "=PTTB/TRACUU/Tracuu_dulieu_upload_cuoc/ajax_frm_cuoc_ir"
			+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
			+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
			+ "&" + CesarCode.encode("tinh") + "=" + psTINH
			+ "&" + CesarCode.encode("tungay") + "=" + psTUNGAY
			+ "&" + CesarCode.encode("denngay") + "=" + psDENNGAY
			+ "&" + CesarCode.encode("somay") + "=" + psSOMAY
			+ "&" + CesarCode.encode("loaibang") + "=" + psLOAIBANG
			+ "&" + CesarCode.encode("kieu") + "=" + psKIEU
			;
	}
	
	//DATE 24-05-2011, ham` update trang thai (0,1,2) cho thue bao upload cuoc cao, cuoc ir ( Thuyvtt2)
	public String doi_trangthai( String func_schema,
 								 String psDsSOMAY,
								 String psDsNgayps,
								 String psDsKIEU,
								 String psLOAIBANG
								) 
 {
   String s= "begin ?:="+func_schema+"PTTB_CCBS.xuly_trangthai('"+func_schema
		+"','"+getUserVar("sys_agentcode")
		+"','"+getUserVar("sys_dataschema")
		+"','"+getUserVar("userID")
		+"','"+psDsSOMAY
		+"','"+psDsKIEU
		+"','"+psLOAIBANG
		+"','"+psDsNgayps
		+"');"
		+"end;"
		;
		System.out.println(s);
		return s;
  } 

	 public String dulieu_upload_KM(String piPAGEID,
										  String piREC_PER_PAGE,
										  String psTINH,
										  String psTUNGAY,
										  String psDENNGAY,
										  String psSOMAY,
										  String psLOAI,
										  String psDUYET)
	{
		return "/main?" + CesarCode.encode("configFile") + "=PTTB/TRACUU/Tracuu_upload_KM/ajax_frm_KM"
			+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
			+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
			+ "&" + CesarCode.encode("tinh") + "=" + psTINH
			+ "&" + CesarCode.encode("tungay") + "=" + psTUNGAY
			+ "&" + CesarCode.encode("denngay") + "=" + psDENNGAY
			+ "&" + CesarCode.encode("somay") + "=" + psSOMAY
			+ "&" + CesarCode.encode("loai") + "=" + psLOAI
			+ "&" + CesarCode.encode("duyet") + "=" + psDUYET
			;
	}
	
	
	/* Export du lieu text */
	public String export_dulieu_text(String psTungay ,
											   String psDenngay,
											   String psMatinh)
	{
		String s= "begin ?:=admin_v2.pttb_chucnang.exp_alo_text('"+psTungay+"','"+psDenngay+"','"+psMatinh+"');end;";
		return s;
	}

	//ngay 07/06/2011	
	public NEOCommonData taofile_export_xls(String psTungay ,
											   String psDenngay,
											   String psMatinh
									       ) 
	{

		String s="";
		String sDataschema = getUserVar("sys_dataschema");
		String sUserid = getUserVar("sys_userid");
		String sFuncSchema = getSysConst("FuncSchema");	
		
		s="begin ? := admin_v2.promo_config.exp_alo_excel('"+psTungay+"','"+psDenngay+"','"+psMatinh+"'); end;";
		
		NEOExecInfo ei_ = new NEOExecInfo(s);

		return  new NEOCommonData(ei_);

	}

	public String lay_ds_km_tinh(String psTinh)
	{
		return "/main?" + CesarCode.encode("configFile") + "=PTTB/TRACUU/Tracuu_upload_KM/ajax_DSKM_tinh"
			+ "&" + CesarCode.encode("tinh") + "=" + psTinh
			;
	}
	

	public String duyet_ds_km_tinh( String func_schema,
 								    String psTINH,
									String psUser
								   ) 
	{		
		String s= "begin ?:="+func_schema+"PTTB_CCBS.duyet_ds_km('"+func_schema
			+"','"+psTINH
			+"','"+psUser
			+"');"
			+"end;"
			;
		System.out.println(s);
		return s;
	}   
	
	public String huy_duyet_ds_km_tinh( String func_schema,
 								    String psTINH,
									String psUser
								   ) 
	{		
		String s= "begin ?:="+func_schema+"PTTB_CCBS.huy_duyet_ds_km('"+func_schema
			+"','"+psTINH
			+"','"+psUser
			+"');"
			+"end;"
			;
		System.out.println(s);
		return s;
	} 
	/* DATE 09-06-2011 Ham` lay gia tri chon loai bao cao cho UPload KM (Thuyvtt2)*/
	public String lay_gtbc( String func_schema,
 						    String psTINH
						  ) 
   {
		String sAgentCode = getUserVar("sys_agentcode");
		String s= "begin ?:="+func_schema+"PTTB_CCBS.tra_gt_BC('"+func_schema
			+"','"+psTINH
			+"','"+sAgentCode
			+"');"
			+"end;"
			;
		System.out.println(s);
		return s;
   } 

   public String search_msin_2013(
		String puserid,
		String pagent,		
		String pmsisdn) throws Exception {
			String s ="begin ? :=admin_v2.pkg_upload_msisdn.search_msisdn_func("
				+"'"+puserid+"',"
				+"'"+pagent+"',"				
				+"'"+pmsisdn+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	
	public String tracuu_trasau_thuong(
		String psMATB
	)
	{
		return "/main?" + CesarCode.encode("configFile") + "=code/tracuu_trasau_thuong/ajax_dstracuu"
			+ "&" + CesarCode.encode("matb") + "=" + psMATB
			;
	}

	 public String tracuu_vplus_pps(String psMA_TB)
	{
		return "/main?" + CesarCode.encode("configFile") + "=pttb/ql_tratruoc/tracuu/ajax_tratruoc"
			+ "&" + CesarCode.encode("matb") + "=" + psMA_TB			
			;
	}
	public String tracuuhd_khdn(
		String piPAGEID
		,String piREC_PER_PAGE
		,String psTEN_CQ
		, String psDIACHICQ
		, String psSO_GT
		, String psTENTB
		, String psDIACHITB
		, String psMATB
		, String psTENTT
		, String psSOSIM
		, String psKH_RR
		, String psTRANGTHAI
		, String psMAKH
		, String piHDORTB
		, String psMACQ
		)
	{
		return "/main?" + CesarCode.encode("configFile") + "=mtv/khdn/tracuu_hd/ajax_DStracuu"
			+ "&" + CesarCode.encode("sys_pageid") +"="+piPAGEID
			+ "&" + CesarCode.encode("sys_rec_per_page") + "=" + piREC_PER_PAGE
			+ "&" + CesarCode.encode("tencq") + "=" + psTEN_CQ
			+ "&" + CesarCode.encode("diachicq") + "=" + psDIACHICQ
			+ "&" + CesarCode.encode("so_gt") + "=" + psSO_GT
			+ "&" + CesarCode.encode("tentb") + "=" + psTENTB
			+ "&" + CesarCode.encode("diachitb") + "=" + psDIACHITB
			+ "&" + CesarCode.encode("matb") + "=" + psMATB
			+ "&" + CesarCode.encode("tentt") + "=" + psTENTT
			+ "&" + CesarCode.encode("sosim") + "=" + psSOSIM
			+ "&" + CesarCode.encode("kh_rr") + "=" + psKH_RR
			+ "&" + CesarCode.encode("trangthai") + "=" + psTRANGTHAI
			+ "&" + CesarCode.encode("makh") + "=" + psMAKH
			+ "&" + CesarCode.encode("hdortb") + "=" + piHDORTB
			+ "&" + CesarCode.encode("macq") + "=" + psMACQ
			;
	}
	public String tracuu_trasau_thuong(
		String psMATB, String psChuky
	)
	{
		return "/main?" + CesarCode.encode("configFile") + "=code/tracuu_trasau_thuong/ajax_dstracuu"
			+ "&" + CesarCode.encode("matb") + "=" + psMATB
			+ "&" + CesarCode.encode("chuky") + "=" + psChuky
			;
	}
	
	public String tracuu_trasau(
		String psma_tb,
		String type
	){
		String schema = getUserVar("sys_dataschema");
		String userid = getUserVar("userID");
		String s="begin ? := admin_v2.pkg_ctkm.get_list_pkg_ccbs('"+psma_tb+"','"+type+"','"+schema+"','"+userid+"'); end;";
		System.out.println(s);
		return s;
	}
	public String tracuu_trasaumcc(
		String psma_tb,
		String type
	){
		String s="begin ? := admin_v2.pkg_ctkm.get_list_pkg_mcc('"+psma_tb+"','"+type+"'); end;";
		System.out.println(s);
		return s;
	}
	public String tracuu_trasau_more(
		String psma_tb,
		String type
	){
		String schema = getUserVar("sys_dataschema");
		String userid = getUserVar("userID");
		String s="begin ? := admin_v2.pkg_ctkm.get_list_pkg_more_ccbs('"+psma_tb+"','"+type+"','"+schema+"','"+userid+"'); end;";
		System.out.println(s);
		return s;
	}
	public String get_tracuu_trasau(
		String ds_goi
	){
		String s="begin ? := admin_v2.pkg_ctkm.get_info_package('"+ds_goi+"'); end;";
		System.out.println(s);
		return s;
	}
	
	public String check_tinh_tb(
		String ma_tb
	){
		String s="begin ? := admin_v2.pkg_ctkm.check_tinh_tb('"+ma_tb+"','"+getUserVar("sys_agentcode")+"'); end;";
		System.out.println(s);
		return s;
	}
	
	public String dangkygoi(
		String dispatch_id,
		String dssm,
		String date
	){
		String s ="begin ? :=admin_v2.pkg_api_data.them_moi_sps("
				+"'"+dispatch_id+"',"
				+"'"+dssm+"',"		
				+"'',"		
				+"'"+date+"',"	
				+"'Thuc hien dang ky tai chuc  nang Tra cuu goi tra sau',"			
				+"'"+getUserVar("userID")+"',"				
				+"'"+getUserVar("sys_agentcode")+"',"
				+"'"+getUserVar("sys_dataschema")+"',"		
				+"'"+getEnvInfo().getParserParam("sys_userip")+"',"
				+"0); end;";
		System.out.println(s);
		return s;
	}
	
	public String dangky_trasau(
		String ma_tb,
		String package_name
	){
		String s = "begin ? :=admin_v2.pkg_ctkm.dk_trasau('"+ma_tb+"','"+package_name+"'); end;";
		System.out.println(s);
		return s;
	}
	
		public String tkc_chuyendoi( String stb
								) 
	 {
	   String s= "begin ?:=ccs_admin.pttb_tracuu.layTk_chuyendoi('"+stb
			+"');"
			+"end;"
			;
			System.out.println(s);
			return s;
	  } 
	  
	public String cap_nhat_ad(
		    String psID ) 
	throws Exception {

		 String s =  "begin ?:= admin_v2.pk_update_excheck.cap_nhat_ad("	
			+"'"+getUserVar("userID")+"'"		
			+",'"+getUserVar("userIP")+"'"
			+",'"+psID+"'"		
			+");"
			+"end;"
			;
		return this.reqValue("", s);
	}
}