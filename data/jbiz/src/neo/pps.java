package neo;

import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.http.*;

public class pps extends NEOProcessEx {

    public void run() {
        System.out.println("neo.qltn.qltn_tienmat was called");
    }

	public NEOCommonData value_retry_dv(			
			String psDsTB
	){
		String s = "";
		String _userid = getUserVar("userID");
	try{
        s = s + " begin ";
		s = s + " ?:=prepaid.PKG_KHUYENMAI.reset_dv('"+psDsTB+"','"+_userid+"'); ";
		s = s + " end; ";
			}
		catch (Exception ex) {
			ex.printStackTrace();			
		}	
		NEOExecInfo ei_ = new NEOExecInfo(s);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
    }
	
	
	public NEOCommonData value_chuyentien(			
			String psTBA,
			String psTBB,
			String psMXT
	){
        String s = "begin ?:=prepaid.pps.chuyentien('"+        	
			psTBA+"','"+
			psTBB+"','"+
			psMXT+"','"+
			getUserVar("userID")+"','"+
			getUserVar("userIP")+"'); end;";
		NEOExecInfo ei_ = new NEOExecInfo(s);
        ei_.setDataSrc("PPS_DBMS");	
		return new NEOCommonData(ei_);
        
    }
	public String value_dknhantien(			
			String psDaily,
			String psEload
	){
        String s = "begin ?:=prepaid.pps.dk_nhantien('"+        	
			psDaily+"','"+
			"WEB"+"','"+
			"DKNT_"+psEload+"','"+
			getUserVar("userID")+"'); end;";
        return s;
    }
	public String value_retry_sog(			
			String psDs,
			String psType			
	) throws Exception{
		String s = "";
		String _userid = getUserVar("userID");
	    s = s + " begin ";
		s = s + " ?:=subadmin.PKG_SOG.reset_sog('"+psDs+"','"+psType+"','"+_userid+"'); ";
		s = s + " end; ";
		return this.reqValue("CMDVOracleDS",s);
    }
}