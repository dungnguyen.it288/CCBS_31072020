package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;
import neo.smartui.report.*;



public class qlda extends NEOProcessEx {
  public void run() {
    System.out.println("neo.inhoadon was called");
  }

  
  public NEOCommonData layTT_XNHTTK(String sMaTinh
									 ) {

    String s=    "begin ?:= hoangpm.pkg_qlda.laytt_xnhttk('"+sMaTinh+"'); end;";
    System.out.println(s); 
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);

  }
  
  public NEOCommonData capnhatTT_BBXN_HTTK(String ma_tinh
	, String ngay_hen_dtao_
	, String ngay_hen_tkct_
	, String muc2_
	, String muc3_
	, String muc4_ 
	, String muc5_
	, String ghi_chu_
	, String ngay_xn_
	) {

    String s = "begin ?:= hoangpm.pkg_qlda.capnhatTT_BBXN_HTTK('"+ ma_tinh 
		+ "','"+ ngay_hen_dtao_ 
		+ "','"+ ngay_hen_tkct_ 
		+ "','"+ muc2_ 
		+ "','"+ muc3_ 
		+ "','"+ muc4_ 
		+ "','"+ muc5_ 
		+ "',"+ ghi_chu_ 
		+ ",'"+ ngay_xn_ 
		+ "'); end; ";
    System.out.println(s); 
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);
  }    
  
  public NEOCommonData capnhatTT_XNHTTK(String ma_tinh
	, String ngay_hen_
	, String muc2_
	, String muc3_
	, String muc4_ 
	, String muc5_
	, String ghi_chu_
	, String ngay_xn_
	) {

    String s = "begin ?:= hoangpm.pkg_qlda.capnhatTT_XNHTTK('"+ ma_tinh 
		+ "','"+ ngay_hen_ 
		+ "','"+ muc2_ 
		+ "','"+ muc3_ 
		+ "','"+ muc4_ 
		+ "','"+ muc5_ 
		+ "',"+ ghi_chu_ 
		+ ",'"+ ngay_xn_ 
		+ "'); end; ";
    System.out.println(s); 
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);
  }  

  public NEOCommonData clsTT_XNHTTK(String ma_tinh)
{
    String s = "begin ?:= hoangpm.pkg_qlda.clsTT_XNHTTK('"+ ma_tinh + "'); end; ";
    System.out.println(s);
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);
  }    
  
  public String urlBaocao(String ma_baocao)
  {
    String url_ ="/main?"+CesarCode.encode("configFile")+"=admin/qlda/bc_"+ma_baocao
        +"&sid="+Math.random()
    ;
    return url_;	
  }  
  
  public String urlDsCongviec(
	String tu_ngay_yc
	, String den_ngay_yc
	, String ma_tinh
	, String ngay_can_hoan_thanh
	, String muc_quan_trong
	, String pham_vi
	, String tien_do_dap_ung
	, String nguoi_th
	)
  {
    String url_ ="/main?"+CesarCode.encode("configFile")+"=admin/qlda/bc_VNP_DSYEUCAU"
		+"&"+CesarCode.encode("tu_ngay_yc")+"="+tu_ngay_yc
		+"&"+CesarCode.encode("den_ngay_yc")+"="+den_ngay_yc
		+"&"+CesarCode.encode("ngay_can_hoan_thanh")+"="+ngay_can_hoan_thanh
		+"&"+CesarCode.encode("muc_quan_trong")+"="+muc_quan_trong
		+"&"+CesarCode.encode("ma_tinh")+"="+ma_tinh
		+"&"+CesarCode.encode("pham_vi")+"="+pham_vi
		+"&"+CesarCode.encode("tien_do_dap_ung")+"="+tien_do_dap_ung
		+"&"+CesarCode.encode("nguoi_th")+"="+nguoi_th
        +"&sid="+Math.random()
    ;
	System.out.println(url_);
    return url_;	
  }
    
    public String urlDsLsuGQ(
		String task_id
	)
  {
    String url_ ="/main?"+CesarCode.encode("configFile")+"=admin/qlda/task_hl"
		+"&"+CesarCode.encode("task_id")+"="+task_id
        +"&sid="+Math.random()
    ;
	System.out.println(url_);
    return url_;	
  }
  
  public NEOCommonData themmoiTT_Task(String nguoi_dapung
	, String ngay_yc
	, String ngay_can_hthanh
	, String tinh_yc
	, String pvi_yc 
	, String muc_ut_yc 
	, String noi_dung_yc
	, String ghi_chu_yc
	) {
		String s = "begin ?:= hoangpm.pkg_qlda.themmoiTT_Task('"+ nguoi_dapung 
		+ "','"+ ngay_yc 
		+ "','"+ ngay_can_hthanh 
		+ "','"+ tinh_yc 
		+ "','"+ pvi_yc 
		+ "','"+ muc_ut_yc 
		+ "','"+ noi_dung_yc 
		+ "','"+ ghi_chu_yc 
		+ "'); commit; end; ";
    System.out.println(s); 
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);
  }    
  
  public NEOCommonData capnhatTT_Task(String task_id
	, String ngay_yc
	, String ngay_can_hthanh
	, String tinh_yc
	, String pvi_yc 
	, String muc_ut_yc 
	, String noi_dung_yc
	, String ghi_chu_yc
	) {
		String s = "begin ?:= hoangpm.pkg_qlda.capnhatTT_Task('"+ task_id 
		+ "','"+ ngay_yc 
		+ "','"+ ngay_can_hthanh 
		+ "','"+ tinh_yc 
		+ "','"+ pvi_yc 
		+ "','"+ muc_ut_yc 
		+ "','"+ noi_dung_yc 
		+ "','"+ ghi_chu_yc 
		+ "'); commit; end; ";
    System.out.println(s); 
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);
  }     
  
  public NEOCommonData capnhatTT_GQ(String task_id
	, String nguoi_gq
	, String tien_do
	, String noi_dung_gq
	, String ghi_chu_gq
	) {
		String s = "begin ?:= hoangpm.pkg_qlda.capnhatTT_GQ('"+ task_id 
		+ "','"+ nguoi_gq 
		+ "','"+ tien_do 
		+ "',"+ noi_dung_gq 
		+ ","+ ghi_chu_gq 
		+ "); commit; end; ";
    System.out.println(s); 
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);
  }    
  
  public NEOCommonData layTT_CV(
	String nguoi_th
	, String ngay_bc
	) {
		String s = "begin ?:= hoangpm.pkg_qlda.laytt_cv_hangngay('"+ nguoi_th 
		+ "','"+ ngay_bc 
		+ "'); end; ";
    System.out.println(s); 
    NEOExecInfo nei_ = new NEOExecInfo(s);
    nei_.setDataSrc("VNPBilling");
    return new NEOCommonData(nei_);
  }    
}

