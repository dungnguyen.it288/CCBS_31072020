package neo;
import neo.smartui.common.CesarCode;
import neo.smartui.common.*;
import neo.smartui.process.*;
import neo.smartui.report.*;
import com.svcon.jdbf.*;
import javax.sql.RowSet;
import java.util.zip.*;
import java.io.*;
import java.net.*;
import org.apache.commons.net.*;
import neo.qltn.ftp;
import neo.qltn.FTPupload;
import sun.misc.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.*;
import java.text.*;
import java.nio.charset.StandardCharsets;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;

public class exp_rpt_comm extends NEOProcessEx{	
	
public String exp_chonso_detail(
		String pstungay,
		String psdenngay,
		String pstinh
	){
		
		String fileName ="";
		String file = "";
		try{
			String sql = "";
			String fileUploadDir = getSysConst("FileUploadDir");

			//Lay ten file
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//ddMMyyyyHH
			DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//ddMMyyyyHH
				
			java.util.Date now = cal.getTime();
			String time = dateFormat.format(now).toString().replace("/","");
			fileName = "pttb_chonso_"+ time+ ".csv";			
			// creat folder to save csv file
			String folderPath = fileUploadDir +"report_news/";
			File f = new File(folderPath);
			if(!f.exists())
				f.mkdir();				
			file = fileUploadDir +"report_news/" +fileName;				
			File file1 = new File(file);
			if (file1.exists()) {
				file1.delete();
			}
		//FileWriter writer = new FileWriter(file1);
		
		OutputStream os = new FileOutputStream(file1);
		os.write(239);
		os.write(187);
		os.write(191);
		PrintWriter out = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
		
		try{
			int counter = 0;
			out.print(ConvertFont.decodeW1252("STT,M&#227; t&#7881;nh,Ng&#224;y,s&#7889; TB 091,H&#242;a m&#7841;ng c&#7863;p &#273;&#244;i qua h&#7879; th&#7889;ng/c&#244;ng c&#7909;, s&#7889; TB c&#7863;p &#273;&#244;i 08x,lo&#7841;i thu&#234; bao,M&#7913;c CK,Th&#7901;i gian CK, &#272;K c&#7863;p &#273;&#244;i,Ng&#432;&#7901;i kh&#7903;i t&#7841;o\n"));
			sql = "begin ? := admin_v2.pkg_rpt_pttb.rpt_capdoi_func("
					+ "'"+pstungay+"',"
					+ "'"+psdenngay+"',"
					+ "'"+pstinh+"'"
					+ ");" 
					+ "end;";	
			System.out.println(sql);				
			RowSet rs = this.reqRSet(sql);	
			while (rs.next()) {				
				counter++;
				out.print(String.valueOf(counter)+"," 
						+ rs.getString("AGENT") + ","
						+ rs.getString("NGAYTH") + ","
						+ rs.getString("MSISDN") + ","
						+ rs.getString("CHANNEL") + ","						
						+ rs.getString("SUB") + ","
						+ rs.getString("POSTPAID") + ","
						+ rs.getString("TIEN_CK") + ","
						+ rs.getString("THANG_CK") + ","
						+ rs.getString("COUPLE") + ","
						+ rs.getString("USERID") + "\n"
				);
			}			
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}finally {
			out.flush();
			out.close();
		}
		return file;
	}catch(Exception e){
			e.printStackTrace();
			return "FALSE"+e.toString();
		}
		
	}
	
	

}