PACKAGE BODY PKG_BIRTHDAY
IS
function search_birthday_config(
	ps_schema varchar2,
	ps_config_name varchar2,
	ps_date_from varchar2,
	ps_date_to varchar2,
	ps_role_list varchar2,
	ps_status varchar2,
	ps_Type varchar2,
	ps_RecordPerPage varchar2,
	ps_PageIndex varchar2,
	ps_UserId varchar2,
	ps_UserIp varchar2)
return sys_refcursor
is
	s varchar2(1000);
	ref_ sys_refcursor;
begin
	--logger.access('ADMIN_V2.birthday_config|SEARCH',ps_config_name||'|'||ps_date_from||'|'||ps_date_to||'|'||ps_role_list||'|'||ps_status);
	s:='select ID,CONFIG_NAME,to_char(DATE_FROM,''dd/mm/yyyy'') DATE_FROM,to_char(DATE_TO,''dd/mm/yyyy'') DATE_TO,ROLE_LIST,STATUS from '||ps_schema||'birthday_config where 1=1';
 	if ps_config_name is not null then s:=s||' and config_name like ''%'||ps_config_name||'%'''; end if;
	if ps_date_from is not null then s:=s||' and date_from=to_date('''||ps_date_from||''',''dd/mm/yyyy'')'; end if;
	if ps_date_to is not null then s:=s||' and date_to=to_date('''||ps_date_to||''',''dd/mm/yyyy'')'; end if;
	if ps_role_list is not null then s:=s||' and role_list like ''%'||ps_role_list||'%'''; end if;
	if ps_status is not null then s:=s||' and status like ''%'||ps_status||'%'''; end if;
	s:=s||' order by ID asc';
	if ps_Type='1' then
		open ref_ for util.xuly_phantrang(s,ps_PageIndex,ps_RecordPerPage);
		return ref_;
	end if;
	if ps_Type='2' then
		return util.GetTotalRecord(s,ps_PageIndex,ps_RecordPerPage);
	end if;
	exception when others then
		declare	err varchar2(500);
 		begin	
			err:='Loi thuc hien, ma loi:'||to_char(sqlerrm);
			open ref_ for 'select :1 err from dual' using err;
		return ref_;
	end;
end;
function edit_birthday_config(
	ps_schema varchar2,
	ps_id varchar2,
	ps_config_name varchar2,
	ps_date_from varchar2,
	ps_date_to varchar2,
	ps_role_list varchar2,
	ps_status varchar2,
	ps_UserId varchar2,
	ps_UserIp varchar2)
return varchar2
is
	s varchar2(1000);
begin
	--logger.access('ADMIN_V2.birthday_config|SEARCH',ps_config_name||'|'||ps_date_from||'|'||ps_date_to||'|'||ps_role_list||'|'||ps_status);
	s:='update '||ps_schema||'birthday_config set config_name=:config_name,date_from=:date_from,date_to=:date_to,role_list=:role_list,status=:status where id=:id';
	execute immediate s using ps_config_name,to_date(ps_date_from,'dd/mm/yyyy'),to_date(ps_date_to,'dd/mm/yyyy'),ps_role_list,ps_status,ps_id;
	commit;
	return '1';
	exception when others then
		declare	err varchar2(500);
		begin	
				err:='Loi thuc hien, ma loi:'||to_char(sqlerrm); 
				return err;
		end;
end;
function new_birthday_config(
	ps_schema varchar2,
	ps_config_name varchar2,
	ps_date_from varchar2,
	ps_date_to varchar2,
	ps_role_list varchar2,
	ps_status varchar2,
	ps_UserId varchar2,
	ps_UserIp varchar2)
return varchar2
is
 	s varchar2(1000);
 	vn_id number;
begin
 	vn_id := admin_v2.GETSEQ('BIRTHDAY_CONFIG_SEQ');
	--logger.access('ADMIN_V2.birthday_config|SEARCH',ps_config_name||'|'||ps_date_from||'|'||ps_date_to||'|'||ps_role_list||'|'||ps_status);
	s:='insert into '||ps_schema||'birthday_config(id,config_name,date_from,date_to,role_list,status)
	 values (:id,:config_name,:date_from,:date_to,:role_list,:status)';
	execute immediate s using vn_id,ps_config_name,to_date(ps_date_from,'dd/mm/yyyy'),to_date(ps_date_to,'dd/mm/yyyy'),ps_role_list,ps_status;
	commit;
	return '1';
	exception when others then
		declare	err varchar2(500);
		begin
			err:='Loi thuc hien, ma loi:'||to_char(sqlerrm);
			return err;
		end;
end;
function del_birthday_config(
	ps_schema varchar2,
	ps_id varchar2,
	ps_UserId varchar2,
	ps_UserIp varchar2)
return varchar2
is
	s varchar2(1000);
begin
	--logger.access('ADMIN_V2.birthday_config|DEL',pId);
	s:='delete from '||ps_schema||'birthday_config where id=:id';
	execute immediate s using ps_id;
	commit;
	return '1';
	exception when others then
		declare	err varchar2(500);
	begin	
		err:='Loi thuc hien, ma loi:'||to_char(sqlerrm);
		return err;
	end;
end;
END;
