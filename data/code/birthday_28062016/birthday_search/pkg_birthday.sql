PACKAGE BODY PKG_BIRTHDAY
IS
function search_upload_data_cmsn(
	ps_schema varchar2,
	ps_msisdn varchar2,
	ps_agent varchar2,
	ps_group_id varchar2,
	ps_send_sms_ob varchar2,
	ps_gift varchar2,
	ps_userid varchar2,
	ps_userip varchar2,
	ps_modify_date varchar2,
	ps_cuoc_bq varchar2,
	ps_month_active varchar2,
	ps_birthday_date varchar2,
	ps_data_type varchar2,
	ps_care_plus_package varchar2,
	ps_send_sms varchar2,
	ps_note varchar2,
	ps_upload_id varchar2,
	ps_gift_result varchar2,
	ps_care_plus_gift varchar2,
	ps_Type varchar2,
	ps_RecordPerPage varchar2,
	ps_PageIndex varchar2,
	ps_UserId varchar2,
	ps_UserIp varchar2)
return sys_refcursor
is
	s varchar2(1000);
	ref_ sys_refcursor;
begin
	--logger.access('ADMIN_V2.upload_data_cmsn|SEARCH',ps_msisdn||'|'||ps_agent||'|'||ps_group_id||'|'||ps_send_sms_ob||'|'||ps_gift||'|'||ps_userid||'|'||ps_userip||'|'||ps_modify_date||'|'||ps_cuoc_bq||'|'||ps_month_active||'|'||ps_birthday_date||'|'||ps_data_type||'|'||ps_care_plus_package||'|'||ps_send_sms||'|'||ps_note||'|'||ps_upload_id||'|'||ps_gift_result||'|'||ps_care_plus_gift);
	s:='select ID,MSISDN,AGENT,GROUP_ID,SEND_SMS_OB,GIFT,USERID,USERIP,to_char(MODIFY_DATE,''dd/mm/yyyy'') MODIFY_DATE,CUOC_BQ,MONTH_ACTIVE,to_char(BIRTHDAY_DATE,''dd/mm/yyyy'') BIRTHDAY_DATE,DATA_TYPE,CARE_PLUS_PACKAGE,SEND_SMS,NOTE,UPLOAD_ID,GIFT_RESULT,CARE_PLUS_GIFT from '||ps_schema||'upload_data_cmsn where 1=1';
 	if ps_msisdn is not null then s:=s||' and msisdn like ''%'||ps_msisdn||'%'''; end if;
	if ps_agent is not null then s:=s||' and agent like ''%'||ps_agent||'%'''; end if;
	if ps_group_id is not null then s:=s||' and group_id like ''%'||ps_group_id||'%'''; end if;
	if ps_send_sms_ob is not null then s:=s||' and send_sms_ob like ''%'||ps_send_sms_ob||'%'''; end if;
	if ps_gift is not null then s:=s||' and gift like ''%'||ps_gift||'%'''; end if;
	if ps_userid is not null then s:=s||' and userid like ''%'||ps_userid||'%'''; end if;
	if ps_userip is not null then s:=s||' and userip like ''%'||ps_userip||'%'''; end if;
	if ps_modify_date is not null then s:=s||' and modify_date=to_date('''||ps_modify_date||''',''dd/mm/yyyy'')'; end if;
	if ps_cuoc_bq is not null then s:=s||' and cuoc_bq like ''%'||ps_cuoc_bq||'%'''; end if;
	if ps_month_active is not null then s:=s||' and month_active like ''%'||ps_month_active||'%'''; end if;
	if ps_birthday_date is not null then s:=s||' and birthday_date=to_date('''||ps_birthday_date||''',''dd/mm/yyyy'')'; end if;
	if ps_data_type is not null then s:=s||' and data_type like ''%'||ps_data_type||'%'''; end if;
	if ps_care_plus_package is not null then s:=s||' and care_plus_package like ''%'||ps_care_plus_package||'%'''; end if;
	if ps_send_sms is not null then s:=s||' and send_sms like ''%'||ps_send_sms||'%'''; end if;
	if ps_note is not null then s:=s||' and note like ''%'||ps_note||'%'''; end if;
	if ps_upload_id is not null then s:=s||' and upload_id like ''%'||ps_upload_id||'%'''; end if;
	if ps_gift_result is not null then s:=s||' and gift_result like ''%'||ps_gift_result||'%'''; end if;
	if ps_care_plus_gift is not null then s:=s||' and care_plus_gift like ''%'||ps_care_plus_gift||'%'''; end if;
	s:=s||' order by ID asc';
	if ps_Type='1' then
		open ref_ for util.xuly_phantrang(s,ps_PageIndex,ps_RecordPerPage);
		return ref_;
	end if;
	if ps_Type='2' then
		return util.GetTotalRecord(s,ps_PageIndex,ps_RecordPerPage);
	end if;
	exception when others then
		declare	err varchar2(500);
 		begin	
			err:='Loi thuc hien, ma loi:'||to_char(sqlerrm);
			open ref_ for 'select :1 err from dual' using err;
		return ref_;
	end;
end;
function edit_upload_data_cmsn(
	ps_schema varchar2,
	ps_id varchar2,
	ps_msisdn varchar2,
	ps_agent varchar2,
	ps_group_id varchar2,
	ps_send_sms_ob varchar2,
	ps_gift varchar2,
	ps_userid varchar2,
	ps_userip varchar2,
	ps_modify_date varchar2,
	ps_cuoc_bq varchar2,
	ps_month_active varchar2,
	ps_birthday_date varchar2,
	ps_data_type varchar2,
	ps_care_plus_package varchar2,
	ps_send_sms varchar2,
	ps_note varchar2,
	ps_upload_id varchar2,
	ps_gift_result varchar2,
	ps_care_plus_gift varchar2,
	ps_UserId varchar2,
	ps_UserIp varchar2)
return varchar2
is
	s varchar2(1000);
begin
	--logger.access('ADMIN_V2.upload_data_cmsn|SEARCH',ps_msisdn||'|'||ps_agent||'|'||ps_group_id||'|'||ps_send_sms_ob||'|'||ps_gift||'|'||ps_userid||'|'||ps_userip||'|'||ps_modify_date||'|'||ps_cuoc_bq||'|'||ps_month_active||'|'||ps_birthday_date||'|'||ps_data_type||'|'||ps_care_plus_package||'|'||ps_send_sms||'|'||ps_note||'|'||ps_upload_id||'|'||ps_gift_result||'|'||ps_care_plus_gift);
	s:='update '||ps_schema||'upload_data_cmsn set msisdn=:msisdn,agent=:agent,group_id=:group_id,send_sms_ob=:send_sms_ob,gift=:gift,userid=:userid,userip=:userip,modify_date=:modify_date,cuoc_bq=:cuoc_bq,month_active=:month_active,birthday_date=:birthday_date,data_type=:data_type,care_plus_package=:care_plus_package,send_sms=:send_sms,note=:note,upload_id=:upload_id,gift_result=:gift_result,care_plus_gift=:care_plus_gift where id=:id';
	execute immediate s using ps_msisdn,ps_agent,ps_group_id,ps_send_sms_ob,ps_gift,ps_userid,ps_userip,to_date(ps_modify_date,'dd/mm/yyyy'),ps_cuoc_bq,ps_month_active,to_date(ps_birthday_date,'dd/mm/yyyy'),ps_data_type,ps_care_plus_package,ps_send_sms,ps_note,ps_upload_id,ps_gift_result,ps_care_plus_gift,ps_id;
	commit;
	return '1';
	exception when others then
		declare	err varchar2(500);
		begin	
				err:='Loi thuc hien, ma loi:'||to_char(sqlerrm); 
				return err;
		end;
end;
function new_upload_data_cmsn(
	ps_schema varchar2,
	ps_msisdn varchar2,
	ps_agent varchar2,
	ps_group_id varchar2,
	ps_send_sms_ob varchar2,
	ps_gift varchar2,
	ps_userid varchar2,
	ps_userip varchar2,
	ps_modify_date varchar2,
	ps_cuoc_bq varchar2,
	ps_month_active varchar2,
	ps_birthday_date varchar2,
	ps_data_type varchar2,
	ps_care_plus_package varchar2,
	ps_send_sms varchar2,
	ps_note varchar2,
	ps_upload_id varchar2,
	ps_gift_result varchar2,
	ps_care_plus_gift varchar2,
	ps_UserId varchar2,
	ps_UserIp varchar2)
return varchar2
is
 	s varchar2(1000);
 	vn_id number;
begin
 	vn_id := admin_v2.GETSEQ('UPLOAD_DATA_CMSN_SEQ');
	--logger.access('ADMIN_V2.upload_data_cmsn|SEARCH',ps_msisdn||'|'||ps_agent||'|'||ps_group_id||'|'||ps_send_sms_ob||'|'||ps_gift||'|'||ps_userid||'|'||ps_userip||'|'||ps_modify_date||'|'||ps_cuoc_bq||'|'||ps_month_active||'|'||ps_birthday_date||'|'||ps_data_type||'|'||ps_care_plus_package||'|'||ps_send_sms||'|'||ps_note||'|'||ps_upload_id||'|'||ps_gift_result||'|'||ps_care_plus_gift);
	s:='insert into '||ps_schema||'upload_data_cmsn(id,msisdn,agent,group_id,send_sms_ob,gift,userid,userip,modify_date,cuoc_bq,month_active,birthday_date,data_type,care_plus_package,send_sms,note,upload_id,gift_result,care_plus_gift)
	 values (:id,:msisdn,:agent,:group_id,:send_sms_ob,:gift,:userid,:userip,:modify_date,:cuoc_bq,:month_active,:birthday_date,:data_type,:care_plus_package,:send_sms,:note,:upload_id,:gift_result,:care_plus_gift)';
	execute immediate s using vn_id,ps_msisdn,ps_agent,ps_group_id,ps_send_sms_ob,ps_gift,ps_userid,ps_userip,to_date(ps_modify_date,'dd/mm/yyyy'),ps_cuoc_bq,ps_month_active,to_date(ps_birthday_date,'dd/mm/yyyy'),ps_data_type,ps_care_plus_package,ps_send_sms,ps_note,ps_upload_id,ps_gift_result,ps_care_plus_gift;
	commit;
	return '1';
	exception when others then
		declare	err varchar2(500);
		begin
			err:='Loi thuc hien, ma loi:'||to_char(sqlerrm);
			return err;
		end;
end;
function del_upload_data_cmsn(
	ps_schema varchar2,
	ps_id varchar2,
	ps_UserId varchar2,
	ps_UserIp varchar2)
return varchar2
is
	s varchar2(1000);
begin
	--logger.access('ADMIN_V2.upload_data_cmsn|DEL',pId);
	s:='delete from '||ps_schema||'upload_data_cmsn where id=:id';
	execute immediate s using ps_id;
	commit;
	return '1';
	exception when others then
		declare	err varchar2(500);
	begin	
		err:='Loi thuc hien, ma loi:'||to_char(sqlerrm);
		return err;
	end;
end;
END;
