PACKAGE BODY PKG_HOTBILL
IS
function search_hotbill_limit_msisdn(
	ps_schema varchar2,
	ps_msisdn varchar2,
	ps_bl_id varchar2,
	ps_subject varchar2,
	ps_create_date varchar2,
	ps_create_by varchar2,
	ps_update_date varchar2,
	ps_update_by varchar2,
	ps_month_live varchar2,
	ps_status varchar2,
	ps_sms_war varchar2,
	ps_Type varchar2,
	ps_RecordPerPage varchar2,
	ps_PageIndex varchar2,
	ps_UserId varchar2,
	ps_UserIp varchar2)
return sys_refcursor
is
	s varchar2(1000);
	ref_ sys_refcursor;
begin
	--logger.access('ADMIN_V2.hotbill_limit_msisdn|SEARCH',ps_msisdn||'|'||ps_bl_id||'|'||ps_subject||'|'||ps_create_date||'|'||ps_create_by||'|'||ps_update_date||'|'||ps_update_by||'|'||ps_month_live||'|'||ps_status||'|'||ps_sms_war);
	s:='select ID,MSISDN,BL_ID,SUBJECT,to_char(CREATE_DATE,''dd/mm/yyyy'') CREATE_DATE,CREATE_BY,to_char(UPDATE_DATE,''dd/mm/yyyy'') UPDATE_DATE,UPDATE_BY,MONTH_LIVE,STATUS,SMS_WAR from '||ps_schema||'hotbill_limit_msisdn where 1=1';
 	if ps_msisdn is not null then s:=s||' and msisdn like ''%'||ps_msisdn||'%'''; end if;
	if ps_bl_id is not null then s:=s||' and bl_id like ''%'||ps_bl_id||'%'''; end if;
	if ps_subject is not null then s:=s||' and subject like ''%'||ps_subject||'%'''; end if;
	if ps_create_date is not null then s:=s||' and create_date=to_date('''||ps_create_date||''',''dd/mm/yyyy'')'; end if;
	if ps_create_by is not null then s:=s||' and create_by like ''%'||ps_create_by||'%'''; end if;
	if ps_update_date is not null then s:=s||' and update_date=to_date('''||ps_update_date||''',''dd/mm/yyyy'')'; end if;
	if ps_update_by is not null then s:=s||' and update_by like ''%'||ps_update_by||'%'''; end if;
	if ps_month_live is not null then s:=s||' and month_live like ''%'||ps_month_live||'%'''; end if;
	if ps_status is not null then s:=s||' and status like ''%'||ps_status||'%'''; end if;
	if ps_sms_war is not null then s:=s||' and sms_war like ''%'||ps_sms_war||'%'''; end if;
	s:=s||' order by ID asc';
	if ps_Type='1' then
		open ref_ for util.xuly_phantrang(s,ps_PageIndex,ps_RecordPerPage);
		return ref_;
	end if;
	if ps_Type='2' then
		return util.GetTotalRecord(s,ps_PageIndex,ps_RecordPerPage);
	end if;
	exception when others then
		declare	err varchar2(500);
 		begin	
			err:='Loi thuc hien, ma loi:'||to_char(sqlerrm);
			open ref_ for 'select :1 err from dual' using err;
		return ref_;
	end;
end;
function edit_hotbill_limit_msisdn(
	ps_schema varchar2,
	ps_id varchar2,
	ps_msisdn varchar2,
	ps_bl_id varchar2,
	ps_subject varchar2,
	ps_create_date varchar2,
	ps_create_by varchar2,
	ps_update_date varchar2,
	ps_update_by varchar2,
	ps_month_live varchar2,
	ps_status varchar2,
	ps_sms_war varchar2,
	ps_UserId varchar2,
	ps_UserIp varchar2)
return varchar2
is
	s varchar2(1000);
begin
	--logger.access('ADMIN_V2.hotbill_limit_msisdn|SEARCH',ps_msisdn||'|'||ps_bl_id||'|'||ps_subject||'|'||ps_create_date||'|'||ps_create_by||'|'||ps_update_date||'|'||ps_update_by||'|'||ps_month_live||'|'||ps_status||'|'||ps_sms_war);
	s:='update '||ps_schema||'hotbill_limit_msisdn set msisdn=:msisdn,bl_id=:bl_id,subject=:subject,create_date=:create_date,create_by=:create_by,update_date=:update_date,update_by=:update_by,month_live=:month_live,status=:status,sms_war=:sms_war where id=:id';
	execute immediate s using ps_msisdn,ps_bl_id,ps_subject,to_date(ps_create_date,'dd/mm/yyyy'),ps_create_by,to_date(ps_update_date,'dd/mm/yyyy'),ps_update_by,ps_month_live,ps_status,ps_sms_war,ps_id;
	commit;
	return '1';
	exception when others then
		declare	err varchar2(500);
		begin	
				err:='Loi thuc hien, ma loi:'||to_char(sqlerrm); 
				return err;
		end;
end;
function new_hotbill_limit_msisdn(
	ps_schema varchar2,
	ps_msisdn varchar2,
	ps_bl_id varchar2,
	ps_subject varchar2,
	ps_create_date varchar2,
	ps_create_by varchar2,
	ps_update_date varchar2,
	ps_update_by varchar2,
	ps_month_live varchar2,
	ps_status varchar2,
	ps_sms_war varchar2,
	ps_UserId varchar2,
	ps_UserIp varchar2)
return varchar2
is
 	s varchar2(1000);
 	vn_id number;
begin
 	vn_id := admin_v2.GETSEQ('HOTBILL_LIMIT_MSISDN_SEQ');
	--logger.access('ADMIN_V2.hotbill_limit_msisdn|SEARCH',ps_msisdn||'|'||ps_bl_id||'|'||ps_subject||'|'||ps_create_date||'|'||ps_create_by||'|'||ps_update_date||'|'||ps_update_by||'|'||ps_month_live||'|'||ps_status||'|'||ps_sms_war);
	s:='insert into '||ps_schema||'hotbill_limit_msisdn(id,msisdn,bl_id,subject,create_date,create_by,update_date,update_by,month_live,status,sms_war)
	 values (:id,:msisdn,:bl_id,:subject,:create_date,:create_by,:update_date,:update_by,:month_live,:status,:sms_war)';
	execute immediate s using vn_id,ps_msisdn,ps_bl_id,ps_subject,to_date(ps_create_date,'dd/mm/yyyy'),ps_create_by,to_date(ps_update_date,'dd/mm/yyyy'),ps_update_by,ps_month_live,ps_status,ps_sms_war;
	commit;
	return '1';
	exception when others then
		declare	err varchar2(500);
		begin
			err:='Loi thuc hien, ma loi:'||to_char(sqlerrm);
			return err;
		end;
end;
function del_hotbill_limit_msisdn(
	ps_schema varchar2,
	ps_id varchar2,
	ps_UserId varchar2,
	ps_UserIp varchar2)
return varchar2
is
	s varchar2(1000);
begin
	--logger.access('ADMIN_V2.hotbill_limit_msisdn|DEL',pId);
	s:='delete from '||ps_schema||'hotbill_limit_msisdn where id=:id';
	execute immediate s using ps_id;
	commit;
	return '1';
	exception when others then
		declare	err varchar2(500);
	begin	
		err:='Loi thuc hien, ma loi:'||to_char(sqlerrm);
		return err;
	end;
end;
END;
