package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class hotbill_limit_msisdn extends NEOProcessEx {
	public String new_hotbill_limit_msisdn(
		String pschema,
		String pmsisdn,
		String pbl_id,
		String psubject,
		String pcreate_date,
		String pcreate_by,
		String pupdate_date,
		String pupdate_by,
		String pmonth_live,
		String pstatus,
		String psms_war,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_hotbill.new_hotbill_limit_msisdn("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(pmsisdn)+"',"
				+"'"+pbl_id+"',"
				+"'"+psubject+"',"
				+"'"+pcreate_date+"',"
				+"'"+ConvertFont.UtoD(pcreate_by)+"',"
				+"'"+pupdate_date+"',"
				+"'"+ConvertFont.UtoD(pupdate_by)+"',"
				+"'"+pmonth_live+"',"
				+"'"+pstatus+"',"
				+"'"+psms_war+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_hotbill_limit_msisdn(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_hotbill.del_hotbill_limit_msisdn("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_hotbill_limit_msisdn(
	String pschema,
		String pid,
		String pmsisdn,
		String pbl_id,
		String psubject,
		String pcreate_date,
		String pcreate_by,
		String pupdate_date,
		String pupdate_by,
		String pmonth_live,
		String pstatus,
		String psms_war,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_hotbill.edit_hotbill_limit_msisdn("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pmsisdn)+"',"
				+"'"+pbl_id+"',"
				+"'"+psubject+"',"
				+"'"+pcreate_date+"',"
				+"'"+ConvertFont.UtoD(pcreate_by)+"',"
				+"'"+pupdate_date+"',"
				+"'"+ConvertFont.UtoD(pupdate_by)+"',"
				+"'"+pmonth_live+"',"
				+"'"+pstatus+"',"
				+"'"+psms_war+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
}