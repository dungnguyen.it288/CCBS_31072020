PACKAGE BODY PK_HOTBILL
IS
function search_hotbill_manage_log(
	ps_schema varchar2,
	ps_content varchar2,
	ps_create_by varchar2,
	ps_time_start varchar2,
	ps_time_end varchar2,
	ps_type_log varchar2,
	ps_Type varchar2,
	ps_RecordPerPage varchar2,
	ps_PageIndex varchar2,
	ps_UserId varchar2,
	ps_UserIp varchar2)
return sys_refcursor
is
	s varchar2(1000);
	ref_ sys_refcursor;
begin
	--logger.access('ADMIN_V2.hotbill_manage_log|SEARCH',ps_content||'|'||ps_create_by||'|'||ps_time_start||'|'||ps_time_end||'|'||ps_type_log);
	s:='select ID,CONTENT,CREATE_BY,to_char(TIME_START,''dd/mm/yyyy'') TIME_START,to_char(TIME_END,''dd/mm/yyyy'') TIME_END,TYPE_LOG from '||ps_schema||'hotbill_manage_log where 1=1';
 	if ps_content is not null then s:=s||' and content like ''%'||ps_content||'%'''; end if;
	if ps_create_by is not null then s:=s||' and create_by like ''%'||ps_create_by||'%'''; end if;
	if ps_time_start is not null then s:=s||' and time_start=to_date('''||ps_time_start||''',''dd/mm/yyyy'')'; end if;
	if ps_time_end is not null then s:=s||' and time_end=to_date('''||ps_time_end||''',''dd/mm/yyyy'')'; end if;
	if ps_type_log is not null then s:=s||' and type_log like ''%'||ps_type_log||'%'''; end if;
	s:=s||' order by ID asc';
	if ps_Type='1' then
		open ref_ for util.xuly_phantrang(s,ps_PageIndex,ps_RecordPerPage);
		return ref_;
	end if;
	if ps_Type='2' then
		return util.GetTotalRecord(s,ps_PageIndex,ps_RecordPerPage);
	end if;
	exception when others then
		declare	err varchar2(500);
 		begin	
			err:='Loi thuc hien, ma loi:'||to_char(sqlerrm);
			open ref_ for 'select :1 err from dual' using err;
		return ref_;
	end;
end;
function edit_hotbill_manage_log(
	ps_schema varchar2,
	ps_id varchar2,
	ps_content varchar2,
	ps_create_by varchar2,
	ps_time_start varchar2,
	ps_time_end varchar2,
	ps_type_log varchar2,
	ps_UserId varchar2,
	ps_UserIp varchar2)
return varchar2
is
	s varchar2(1000);
begin
	--logger.access('ADMIN_V2.hotbill_manage_log|SEARCH',ps_content||'|'||ps_create_by||'|'||ps_time_start||'|'||ps_time_end||'|'||ps_type_log);
	s:='update '||ps_schema||'hotbill_manage_log set content=:content,create_by=:create_by,time_start=:time_start,time_end=:time_end,type_log=:type_log where id=:id';
	execute immediate s using ps_content,ps_create_by,to_date(ps_time_start,'dd/mm/yyyy'),to_date(ps_time_end,'dd/mm/yyyy'),ps_type_log,ps_id;
	commit;
	return '1';
	exception when others then
		declare	err varchar2(500);
		begin	
				err:='Loi thuc hien, ma loi:'||to_char(sqlerrm); 
				return err;
		end;
end;
function new_hotbill_manage_log(
	ps_schema varchar2,
	ps_content varchar2,
	ps_create_by varchar2,
	ps_time_start varchar2,
	ps_time_end varchar2,
	ps_type_log varchar2,
	ps_UserId varchar2,
	ps_UserIp varchar2)
return varchar2
is
 	s varchar2(1000);
 	vn_id number;
begin
 	vn_id := admin_v2.GETSEQ('HOTBILL_MANAGE_LOG_SEQ');
	--logger.access('ADMIN_V2.hotbill_manage_log|SEARCH',ps_content||'|'||ps_create_by||'|'||ps_time_start||'|'||ps_time_end||'|'||ps_type_log);
	s:='insert into '||ps_schema||'hotbill_manage_log(id,content,create_by,time_start,time_end,type_log)
	 values (:id,:content,:create_by,:time_start,:time_end,:type_log)';
	execute immediate s using vn_id,ps_content,ps_create_by,to_date(ps_time_start,'dd/mm/yyyy'),to_date(ps_time_end,'dd/mm/yyyy'),ps_type_log;
	commit;
	return '1';
	exception when others then
		declare	err varchar2(500);
		begin
			err:='Loi thuc hien, ma loi:'||to_char(sqlerrm);
			return err;
		end;
end;
function del_hotbill_manage_log(
	ps_schema varchar2,
	ps_id varchar2,
	ps_UserId varchar2,
	ps_UserIp varchar2)
return varchar2
is
	s varchar2(1000);
begin
	--logger.access('ADMIN_V2.hotbill_manage_log|DEL',pId);
	s:='delete from '||ps_schema||'hotbill_manage_log where id=:id';
	execute immediate s using ps_id;
	commit;
	return '1';
	exception when others then
		declare	err varchar2(500);
	begin	
		err:='Loi thuc hien, ma loi:'||to_char(sqlerrm);
		return err;
	end;
end;
END;
