package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class hotbill_limit_msisdn extends NEOProcessEx {
	public String new_hotbill_manage_log(
		String pschema,
		String pcontent,
		String pcreate_by,
		String ptime_start,
		String ptime_end,
		String ptype_log,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pk_hotbill.new_hotbill_manage_log("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(pcontent)+"',"
				+"'"+ConvertFont.UtoD(pcreate_by)+"',"
				+"'"+ptime_start+"',"
				+"'"+ptime_end+"',"
				+"'"+ptype_log+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_hotbill_manage_log(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pk_hotbill.del_hotbill_manage_log("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_hotbill_manage_log(
	String pschema,
		String pid,
		String pcontent,
		String pcreate_by,
		String ptime_start,
		String ptime_end,
		String ptype_log,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pk_hotbill.edit_hotbill_manage_log("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pcontent)+"',"
				+"'"+ConvertFont.UtoD(pcreate_by)+"',"
				+"'"+ptime_start+"',"
				+"'"+ptime_end+"',"
				+"'"+ptype_log+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
}