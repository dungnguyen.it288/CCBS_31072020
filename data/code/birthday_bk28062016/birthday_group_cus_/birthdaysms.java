package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class birthdaysms extends NEOProcessEx {
	public String new_birthday_config(
		String pschema,
		String pconfig_name,
		String pdate_from,
		String pdate_to,
		String prole_list,
		String pstatus,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.new_birthday_config("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(pconfig_name)+"',"
				+"'"+pdate_from+"',"
				+"'"+pdate_to+"',"
				+"'"+ConvertFont.UtoD(prole_list)+"',"
				+"'"+pstatus+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_birthday_config(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.del_birthday_config("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_birthday_config(
	String pschema,
		String pid,
		String pconfig_name,
		String pdate_from,
		String pdate_to,
		String prole_list,
		String pstatus,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.edit_birthday_config("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pconfig_name)+"',"
				+"'"+pdate_from+"',"
				+"'"+pdate_to+"',"
				+"'"+ConvertFont.UtoD(prole_list)+"',"
				+"'"+pstatus+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
}