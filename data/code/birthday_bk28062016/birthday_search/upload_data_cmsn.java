package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class upload_data_cmsn extends NEOProcessEx {
	public String new_upload_data_cmsn(
		String pschema,
		String pmsisdn,
		String pagent,
		String pgroup_id,
		String psend_sms_ob,
		String pgift,
		String puserid,
		String puserip,
		String pmodify_date,
		String pcuoc_bq,
		String pmonth_active,
		String pbirthday_date,
		String pdata_type,
		String pcare_plus_package,
		String psend_sms,
		String pnote,
		String pupload_id,
		String pgift_result,
		String pcare_plus_gift,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.new_upload_data_cmsn("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(pmsisdn)+"',"
				+"'"+ConvertFont.UtoD(pagent)+"',"
				+"'"+pgroup_id+"',"
				+"'"+psend_sms_ob+"',"
				+"'"+ConvertFont.UtoD(pgift)+"',"
				+"'"+ConvertFont.UtoD(puserid)+"',"
				+"'"+ConvertFont.UtoD(puserip)+"',"
				+"'"+pmodify_date+"',"
				+"'"+pcuoc_bq+"',"
				+"'"+ConvertFont.UtoD(pmonth_active)+"',"
				+"'"+pbirthday_date+"',"
				+"'"+pdata_type+"',"
				+"'"+pcare_plus_package+"',"
				+"'"+psend_sms+"',"
				+"'"+ConvertFont.UtoD(pnote)+"',"
				+"'"+pupload_id+"',"
				+"'"+pgift_result+"',"
				+"'"+pcare_plus_gift+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_upload_data_cmsn(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.del_upload_data_cmsn("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_upload_data_cmsn(
	String pschema,
		String pid,
		String pmsisdn,
		String pagent,
		String pgroup_id,
		String psend_sms_ob,
		String pgift,
		String puserid,
		String puserip,
		String pmodify_date,
		String pcuoc_bq,
		String pmonth_active,
		String pbirthday_date,
		String pdata_type,
		String pcare_plus_package,
		String psend_sms,
		String pnote,
		String pupload_id,
		String pgift_result,
		String pcare_plus_gift,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.edit_upload_data_cmsn("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pmsisdn)+"',"
				+"'"+ConvertFont.UtoD(pagent)+"',"
				+"'"+pgroup_id+"',"
				+"'"+psend_sms_ob+"',"
				+"'"+ConvertFont.UtoD(pgift)+"',"
				+"'"+ConvertFont.UtoD(puserid)+"',"
				+"'"+ConvertFont.UtoD(puserip)+"',"
				+"'"+pmodify_date+"',"
				+"'"+pcuoc_bq+"',"
				+"'"+ConvertFont.UtoD(pmonth_active)+"',"
				+"'"+pbirthday_date+"',"
				+"'"+pdata_type+"',"
				+"'"+pcare_plus_package+"',"
				+"'"+psend_sms+"',"
				+"'"+ConvertFont.UtoD(pnote)+"',"
				+"'"+pupload_id+"',"
				+"'"+pgift_result+"',"
				+"'"+pcare_plus_gift+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
}