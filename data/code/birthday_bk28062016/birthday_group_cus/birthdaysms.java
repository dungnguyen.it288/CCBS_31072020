package neo.pttb;

import neo.smartui.process.*;
import neo.smartui.common.*;

public class birthdaysms extends NEOProcessEx {
	public String new_birthday_group_cus(
		String pschema,
		String pgroup_name,
		String pstatus,
		String pgift_val,
		String pnote,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.new_birthday_group_cus("
				+"'"+pschema+"',"
				+"'"+ConvertFont.UtoD(pgroup_name)+"',"
				+"'"+pstatus+"',"
				+"'"+pgift_val+"',"
				+"'"+ConvertFont.UtoD(pnote)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
       return this.reqValue("", s);
	}
	public String del_birthday_group_cus(
		String pschema,
		String pid,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.del_birthday_group_cus("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
	public String edit_birthday_group_cus(
	String pschema,
		String pid,
		String pgroup_name,
		String pstatus,
		String pgift_val,
		String pnote,
		String puserid,
		String puserip) throws Exception {
			String s ="begin ? :=ADMIN_V2.pkg_birthday.edit_birthday_group_cus("
				+"'"+pschema+"',"
				+"'"+pid+"',"
				+"'"+ConvertFont.UtoD(pgroup_name)+"',"
				+"'"+pstatus+"',"
				+"'"+pgift_val+"',"
				+"'"+ConvertFont.UtoD(pnote)+"',"
				+"'"+puserid+"',"
				+"'"+puserid+"'"
				+"); end;";
		System.out.println(s);
		return this.reqValue("", s);
	}
}