function accept_upload_datacont_func(
            psUpload_id varchar2,
            psAgentCode varchar2,
            psuserid varchar2,
            psuserip varchar2
            )
    return varchar2
IS
        s varchar2(2000);
        sql_ varchar2(2000);
        type_id NUMBER;
BEGIN
SAVEPOINT sp5;

--so thue bao,email
s:='UPDATE ccs_'||psAgentCode||'.danhba_dds_pttb a
    SET a.nguoi_cn = '''||psuserid||''' , a.may_cn = '''||psuserip||''',
    a.email = (SELECT d.detail FROM admin_v2.upload_data_contact d WHERE d.msisdn=a.somay
                AND d.upload_id = '||psUpload_id||' AND d.status IS NULL and d.type_id=1 )
    WHERE EXISTS (SELECT 1 FROM admin_v2.upload_data_contact c WHERE a.somay=c.msisdn AND c.upload_id = '||psUpload_id||'
     AND c.status IS NULL and c.type_id=1)';
    execute immediate s;
--ma kh,email
s:='UPDATE ccs_'||psAgentCode||'.khachhangs_pttb a
    SET a.nguoi_cn = '''||psuserid||''' , a.may_cn = '''||psuserip||''',
    a.email = (SELECT d.detail FROM admin_v2.upload_data_contact d WHERE d.msisdn=a.ma_kh
                AND d.upload_id = '||psUpload_id||' AND d.status IS NULL and d.type_id=4 )
    WHERE EXISTS (SELECT 1 FROM admin_v2.upload_data_contact c WHERE a.ma_kh=c.msisdn AND c.upload_id = '||psUpload_id||'
     AND c.status IS NULL and c.type_id=4)';
    execute immediate s;
--so tb, in chi tiet
s:='UPDATE ccs_'||psAgentCode||'.danhba_dds_pttb a
    SET a.nguoi_cn = '''||psuserid||''',a.may_cn = '''||psuserip||''',
    a.inchitiet = (SELECT to_number(d.detail) FROM admin_v2.upload_data_contact d
    WHERE d.msisdn=a.somay AND d.upload_id = '||psUpload_id||' AND d.status IS NULL and d.type_id=2)
    WHERE EXISTS (SELECT 1 FROM admin_v2.upload_data_contact b
    WHERE a.somay=b.msisdn AND b.upload_id = '||psUpload_id||' AND b.status IS NULL and b.type_id=2)';
   execute immediate s;

-- cap nhat nhan vien thu cuoc
s:='UPDATE ccs_'||psagentcode||'.nhanvien_tcs a
    set (a.dienthoai_lh,a.sms_debt) = (select d.detail,d.sms_debt FROM admin_v2.upload_data_contact d
    WHERE d.msisdn=a.ma_nv AND d.upload_id = '||psUpload_id||' AND d.status IS NULL and d.type_id=3)
    WHERE EXISTS (SELECT 1 FROM admin_v2.upload_data_contact b
    WHERE b.msisdn=a.ma_nv AND b.upload_id = '||psUpload_id||' AND b.status IS NULL and b.type_id=3)';
   execute immediate s;

--ma thue bao, ma khach hang
s:='INSERT INTO admin_v2.print_join_services( msisdn, cust_id, create_by, crate_date, create_ip,upload_id)
SELECT msisdn,detail,'''||psuserid||''',SYSDATE,'''||psuserip||''',upload_id
 FROM admin_v2.upload_data_contact
 WHERE upload_id = '||psUpload_id||' AND status IS NULL and type_id=5';
 execute immediate s;

--so tb, ma bao cuoc
s:='UPDATE ccs_'||psAgentCode||'.danhba_dds_pttb a
    SET a.nguoi_cn = '''||psuserid||''',a.may_cn = '''||psuserip||''',
    a.mabaocuoc = (SELECT to_number(d.detail) FROM admin_v2.upload_data_contact d
    WHERE d.msisdn=a.somay AND d.upload_id = '||psUpload_id||' AND d.status IS NULL and d.type_id=6)
    WHERE EXISTS (SELECT 1 FROM admin_v2.upload_data_contact b
    WHERE a.somay=b.msisdn AND b.upload_id = '||psUpload_id||' AND b.status IS NULL and b.type_id=6)';
   execute immediate s;
----------------------------
s:='update admin_v2.upload_data_contact
    set create_by = '''||psuserid||''',create_date=sysdate
    where upload_id = '||psUpload_id||' AND status IS NULL';
    execute immediate s;

COMMIT;
RETURN '1';
EXCEPTION
    WHEN others THEN
        BEGIN
            ROLLBACK to sp5;
            RETURN 'Loi upload - errcode: ' || SQLCODE || s;
        END;
END;

FUNCTION check_upload_data_contact
(
    psuserid         in  varchar2,
    psagentcode      in  varchar2,
    psusingLoader    in  varchar2,
    psupload_id      in  VARCHAR2
) RETURN varchar2
IS
    ref_ sys_Refcursor;
    s varchar2(1000);
    iCount NUMBER;
    kq VARCHAR2 (1000);
    type_id NUMBER;
BEGIN
    --kiem tra so may trung bi trung trong cung mot danh sach
    s:=' Update admin_v2.upload_data_contact a'
            ||' Set a.status=1'
            ||' where 1=1'
            ||' and a.upload_id = :1'
            ||' and a.type_id in (1,2,5,6)'--tuyetptm 13/10
            ||' and a.msisdn in (select b.msisdn from admin_v2.upload_data_contact b where b.upload_id = :2 group by b.msisdn,b.upload_id having count(1)>1)';
            execute immediate s using psupload_id,psupload_id;
    s:=' Update admin_v2.upload_data_contact a'
            ||' Set a.status=13'
            ||' where 1=1'
            ||' and a.upload_id = :1'
            ||' and a.type_id =4'
            ||' and a.msisdn in (select b.msisdn from admin_v2.upload_data_contact b where b.upload_id = :2
                and b.type_id =4 group by b.msisdn,b.upload_id having count(1)>1)';
            execute immediate s using psupload_id,psupload_id;
    s:=' Update admin_v2.upload_data_contact a'
            ||' Set a.status=8'
            ||' where 1=1'
            ||' and a.upload_id = :1'
            ||' and a.type_id =3'
            ||' and a.msisdn in (select b.msisdn from admin_v2.upload_data_contact b where
                b.upload_id = :2 and b.type_id =3 group by b.msisdn,b.upload_id having count(1)>1)';
            execute immediate s using psupload_id,psupload_id;
    --kiem tra ko la tra sau
     s:=' Update admin_v2.upload_data_contact a'
            ||' Set a.status=2'
            ||' where 1=1'
            ||' and a.upload_id = :1'
            ||' and a.type_id in (1,2,5,6)'--tuyetptm 13/10
            ||' and not exists(SELECT 1 FROM ccs_common.thue_bao b WHERE a.msisdn =b.so_tb and
                exists(SELECT 1 FROM ccs_common.loai_tb c
                                WHERE b.loai=c.loai_tb AND c.tra_sau=1 AND c.nhom=''V'')) ';
            execute immediate s using psupload_id;
    --khong thuoc tinh quan ly
     s:=' Update admin_v2.upload_data_contact a'
            ||' Set a.status=3'
            ||' where 1=1'
            ||' and a.upload_id = :1'
            ||' and a.type_id in (1,2,5,6)'--tuyetptm 13/10
            ||' and exists(SELECT 1 FROM ccs_common.thue_bao b WHERE a.msisdn =b.so_tb and b.ma_tinh <>'''||psagentcode||''') ';
            execute immediate s using psupload_id;
    --da huy
     s:=' Update admin_v2.upload_data_contact a'
            ||' Set a.status=4'
            ||' where 1=1'
            ||' and a.upload_id = :1'
            ||' and a.type_id in (1,2,5,6)'--tuyetptm 13/10
            ||' and exists(SELECT 1 FROM ccs_common.thue_bao b WHERE a.msisdn =b.so_tb and b.trang_thai =''SP'') ';
            execute immediate s using psupload_id;
      --thue bao khong nam trong danh ba
     s:=' Update admin_v2.upload_data_contact a'
            ||' Set a.status=5'
            ||' where 1=1'
            ||' and a.upload_id = :1'
            ||' and a.type_id in (1,2,5,6)'--tuyetptm 13/10
            ||' and not exists(SELECT 1 FROM ccs_'||psagentcode||'.danhba_dds_pttb b WHERE a.msisdn =b.somay) ';
            execute immediate s using psupload_id;
     --kiem tra kieu upload
       s:=' Update admin_v2.upload_data_contact a'
            ||' Set a.status=6'
            ||' where 1=1'
            ||' and a.upload_id = :1'
            ||' and a.type_id in (1,4)'
            ||' and instr(a.detail,''@'')=0 ';
            execute immediate s using psupload_id;

     --kiem tra han muc khong ton tai
      s:=' Update admin_v2.upload_data_contact a'
            ||' Set a.status=7'
            ||' where 1=1'
            ||' and a.upload_id = :1'
            ||' and a.type_id = 2'
            ||' and a.detail not in (''0'',''1'')';
            execute immediate s using psupload_id;
     --kiem tra nhan vien thu cuoc tuyen thu
     s:=' Update admin_v2.upload_data_contact a'
            ||' Set a.status=9'
            ||' where 1=1'
            ||' and a.upload_id = :1'
            ||' and a.type_id = 3'
            ||' and not exists (select 1 from ccs_'||psagentcode||'.nhanvien_tcs b where a.msisdn = b.ma_nv)';
            execute immediate s using psupload_id;
     --kiem tra so dien thoai lien he thon tai
/*     s:=' Update admin_v2.upload_data_contact a'
            ||' Set a.status=10'
            ||' where 1=1'
            ||' and a.upload_id = :1'
            ||' and a.type_id = 3'
            ||' and not exists (SELECT 1 FROM ccs_common.thue_bao b WHERE a.detail =b.so_tb and
                exists(SELECT 1 FROM ccs_common.loai_tb c
                                WHERE b.loai=c.loai_tb AND c.nhom=''V'') and b.trang_thai <> ''SP'')';
            execute immediate s using psupload_id;*/
        /*s:=' Update admin_v2.upload_data_contact a'
            ||' Set a.status=11'
            ||' where 1=1'
            ||' and a.upload_id = :1'
            ||' and a.type_id = 3'
            ||' and exists(select 1 from ccs_'||psagentcode||'.nhanvien_tcs where dienthoai_lh= a.detail and a.msisdn <> ma_nv)';
            execute immediate s using psupload_id;
         s:=' Update admin_v2.upload_data_contact a'
            ||' Set status=12'
            ||' where 1=1'
            ||' and upload_id = :1'
            ||' and a.type_id =3'
            ||' and a.detail in (select b.detail from admin_v2.upload_data_contact b where
                b.upload_id = :2 and b.type_id =3 group by b.detail,b.upload_id having count(1)>1)';
            execute immediate s using psupload_id,psupload_id;*/
        ---kiem tra su ton tai cua ma kh loai upload 4
        s:=' Update admin_v2.upload_data_contact a'
            ||' Set a.status=14'
            ||' where 1=1'
            ||' and a.upload_id = :1'
            ||' and a.type_id = 4'
            ||' and not exists(select 1 from ccs_'||psagentcode||'.khachhangs_pttb b where b.ma_kh= a.msisdn)';
            execute immediate s using psupload_id;
      --kiem tra thue bao khong thuoc ma kh
     s:=' Update admin_v2.upload_data_contact a'
            ||' Set a.status=15'
            ||' where 1=1'
            ||' and a.upload_id = :1'
            ||' and a.type_id =5'
            ||' and exists(SELECT 1 FROM ccs_'||psagentcode||'.danhba_dds_pttb b WHERE a.msisdn =b.somay and a.detail<>b.ma_kh ) ';
            execute immediate s using psupload_id;
     s:=' Update admin_v2.upload_data_contact a'
            ||' Set a.status=16'
            ||' where 1=1'
            ||' and a.upload_id = :1'
            ||' and a.type_id =5'
            ||' and exists(SELECT 1 FROM admin_v2.print_join_services b WHERE a.msisdn =b.msisdn and a.detail=b.cust_id ) ';
            execute immediate s using psupload_id;
     --kiem tra mabaocuoc is null
      s:=' Update admin_v2.upload_data_contact a'
            ||' Set a.status=18'
            ||' where 1=1'
            ||' and a.upload_id = :1'
            ||' and a.type_id = 6'
            ||' and a.detail is null';
            execute immediate s using psupload_id;

    RETURN '1';

    EXCEPTION WHEN OTHERS THEN
    ROLLBACK;
    RETURN 'Dinh dang file khong dung - errcode:'||TO_CHAR (SQLCODE) || SQLERRM || s;
END;

function data_contact_err(psUpload_id VARCHAR2, psUserSchema VARCHAR2,
    psComSchema VARCHAR2,psMatinh VARCHAR2)
    return sys_refcursor
    is
        s varchar2(2000);
        ref_ sys_refcursor;
    begin
        s:='SELECT
                km.msisdn||'',''||km.detail  so_tb ,
                CASE
                    WHEN km.status = 1 THEN ''Thue bao bi trung''
                    WHEN km.status = 2 THEN ''Thue bao khong la tra sau''
                    WHEN km.status = 3 THEN ''Thue bao khong thuoc tinh quan ly''
                    WHEN km.status = 4 THEN ''Thue bao o trang thai HUY''
                    WHEN km.status = 5 THEN ''Thue bao khong ton tai trong danh ba''
                    WHEN km.status = 6 THEN ''Dia chi email khong dung''
                    WHEN km.status = 7 THEN ''Thue bao co trang thai in chi tiet khong hop le''
                    WHEN km.status = 8 THEN ''Ma nhan vien thu cuoc bi trung''
                    WHEN km.status = 9 THEN ''Ma nhan vien thu cuoc khong ton tai''
                    WHEN km.status = 10 THEN ''Dien thoai lien he khong la tra sau cua VNP hoac da huy''
                    WHEN km.status = 11 THEN ''Dien thoai lien he da duoc nhan vien khac su dung''
                    WHEN km.status = 12 THEN ''Dien thoai lien he bi trung''
                    WHEN km.status = 13 THEN ''Ma khac hang bi trung''
                    WHEN km.status = 14 THEN ''Ma khach hang khong ton tai''
                    WHEN km.status = 15 THEN ''Thue bao khong thuoc ma khach hang upload''
                    WHEN km.status = 16 THEN ''Thue bao, ma khach hang da ton tai''
                    WHEN km.status = 17 THEN ''SMS_DBET phai co gia tri 0 hoac 1''
					WHEN km.status = 18 THEN ''Ma bao cuoc khong duoc de trong''
                    ELSE ''''
                END errMsg
            FROM admin_v2.upload_data_contact km
            WHERE km.upload_id = :1 AND km.status IS NOT null';
        open ref_ for s using psUpload_id;

        return ref_;
    exception
        when others then
            return ref_;
    end;
