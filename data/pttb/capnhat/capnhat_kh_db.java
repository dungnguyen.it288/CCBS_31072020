package neo.pttb;
import neo.smartui.common.CesarCode;
import neo.smartui.process.*;

public class capnhat_kh_db
    extends NEOProcessEx {
  public void run() {
    System.out.println("neo.capnhat_kh_db was called");
  }

public  String laytt_thuebao_theo_somay(String psFuncSchema,String psUserId , String psMaTinhNgDung, String  psSchemaData,String psInput)
   		{
   			String s="begin ?:= "+psFuncSchema+"pttb_capnhat.laytt_thuebao("
						+"'"+psUserId +"',"
						+"'"+psMaTinhNgDung+"',"
						+"'"+psSchemaData	+"',"
						+"'"+psInput+"'"
					+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
   			
public  String lay_so_thuebao_cua_khachhang(String psFuncSchema,String psUserId , String psMaTinhNgDung, String  psSchemaData, String psInput)
   		{
   			String s="begin ?:= "+psFuncSchema+"pttb_capnhat.lay_so_thuebao_cua_khachhang("
						+"'"+psUserId +"',"
						+"'"+psMaTinhNgDung+"',"
						+"'"+psSchemaData	+"',"
						+"'"+psInput+"'"
					+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
     
   
public  String laytt_kh(String psFuncSchema,String psUserId , String psMaTinhNgDung, String  psSchemaData, String  psColumn,String psInput)
   		{
   			String s="begin ?:= "+psFuncSchema+"pttb_capnhat.laytt_kh("
						+"'"+psUserId +"',"
						+"'"+psMaTinhNgDung+"',"
						+"'"+psSchemaData	+"',"
						+"'"+psColumn +"',"
						+"'"+psInput+"'"
					+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
  
  
  public  String laytt_so_khachhang_trong_cq(String psFuncSchema,String psUserId , String psMaTinhNgDung, String  psSchemaData, String psInput)
   		{
   			String s="begin ?:= "+psFuncSchema+"pttb_capnhat.laytt_so_khachhang_trong_cq("
						+"'"+psUserId +"',"
						+"'"+psMaTinhNgDung+"',"
						+"'"+psSchemaData	+"',"
						+"'"+psInput+"'"
					+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
  

  /*public  String capNhatKhachHang(	
								 String sUserid
								,String sMaTinh
								,String sDataSchema
								,String sFunSchema
								
								,String sMaKHCu
								,String sMaKH
								,String sMaCQ
								,String sCoQuan
								,String sTenTT
								,String sSoDaiDien
								,String sNguoiDaiDien
								,String sDienThoaiLH
								,String sPHAI
								,String sEmail
								,String sNgaySinh
								,String sSoGiayTo
								,String sNoiCapGT
								,String sNgayCapGT
								,String sSoGT1
								,String sNoiCapGT1
								,String sNgayCapGT1
								,String suanct_id
								,String shuongct_id
								,String shoct_id
								,String sonhact_id
								,String sDiaChiCT
								,String sMSThue
								,String sTaiKhoan
								,String suantt_id
								,String shuongtt_id
								,String shott_id
								,String sonhatt_id
								,String sDiaChiTT
								,String sDangKyTV
								,String sDangKyDB
								,String sKHRR
								,String sNguoiCN
								,String sMayCN
								,String sMaNV
								,String sTenNV
								,String sGhiChu
								,String sLoaiGTID
								,String sLoaiGTID1
								,String sLoaiKH
								,String sDiaDiemTT
								,String sNganHang
								,String sChuyenKhoan
								,String sDonViQL
								,String sMaBC
								,String sKHLon
								,String sUuTien
								,String sNganhNghe
								,String sDoiDVQLTB
)
   		{
   			String s="begin ?:= "+sFunSchema+"pttb_capnhat.capnhat_kh_pttb1("
						+"'" +getUserVar("userID") +"'"
						+",'"+getUserVar("sys_agentcode")+"'"
						+",'"+getUserVar("sys_dataschema")	+"'"
					
					    +",'"+sMaKHCu+"'"
						+",'"+sMaKH+"'"
						+",'"+sMaCQ+"'"
						+",'"+sCoQuan+"'"
						+",'"+sTenTT+"'"
						+",'"+sSoDaiDien+"'"
						+",'"+sNguoiDaiDien+"'"
						+",'"+sDienThoaiLH+"'"
						+",'"+sPHAI+"'"
						+",'"+sEmail+"'"
						+",'"+sNgaySinh+"'"
						+",'"+sSoGiayTo+"'"
						+",'"+sNoiCapGT+"'"
						+",'"+sNgayCapGT+"'"
						+",'"+sSoGT1+"'"
						+",'"+sNoiCapGT1+"'"
						+",'"+sNgayCapGT1+"'"
						+",'"+suanct_id+"'"
						+",'"+shuongct_id+"'"
						+",'"+shoct_id+"'"
						+",'"+sonhact_id+"'"
						+",'"+sDiaChiCT+"'"
						+",'"+sMSThue+"'"
						+",'"+sTaiKhoan+"'"
						+",'"+suantt_id+"'"
						+",'"+shuongtt_id+"'"
						+",'"+shott_id+"'"
						+",'"+sonhatt_id+"'"
						+",'"+sDiaChiTT+"'"
						+",'"+sDangKyTV+"'"
						+",'"+sDangKyDB+"'"
						+",'"+sKHRR+"'"
						+",'"+sNguoiCN+"'"
						+",'"+sMayCN+"'"
						+",'"+sMaNV+"'"
						+",'"+sTenNV+"'"
						+",'"+sGhiChu+"'"
						+",'"+sLoaiGTID+"'"
						+",'"+sLoaiGTID1+"'"
						+",'"+sLoaiKH+"'"
						+",'"+sDiaDiemTT+"'"
						+",'"+sNganHang+"'"
						+",'"+sChuyenKhoan+"'"
						+",'"+sDonViQL+"'"
						+",'"+sMaBC+"'"
						+",'"+sKHLon+"'"
						+",'"+sUuTien+"'"
						+",'"+sNganhNghe+"'"
						+",'"+sDoiDVQLTB+"'"
						

						+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
  */
   public  String capNhatKhachHang(	
								 String sUserid
								,String sMaTinh
								,String sDataSchema
								,String sFunSchema
								
								,String sMaKHCu
								,String sMaKH
								,String sMaCQ
								,String sCoQuan
								,String sTenTT
								,String sSoDaiDien
								,String sNguoiDaiDien
								,String sDienThoaiLH
								,String sPHAI
								,String sEmail
								,String sNgaySinh
								,String sSoGiayTo
								,String sNoiCapGT
								,String sNgayCapGT
								,String sSoGT1
								,String sNoiCapGT1
								,String sNgayCapGT1
								,String suanct_id
								,String shuongct_id
								,String shoct_id
								,String sonhact_id
								,String sDiaChiCT
								,String sMSThue
								,String sTaiKhoan
								,String suantt_id
								,String shuongtt_id
								,String shott_id
								,String sonhatt_id
								,String sDiaChiTT
								,String sDangKyTV
								,String sDangKyDB
								,String sKHRR
								,String sNguoiCN
								,String sMayCN
								,String sMaNV
								,String sTenNV
								,String sGhiChu
								,String sLoaiGTID
								,String sLoaiGTID1
								,String sLoaiKH
								,String sDiaDiemTT
								,String sNganHang
								,String sChuyenKhoan
								,String sDonViQL
								,String sMaBC
								,String sKHLon
								,String sUuTien
								,String sNganhNghe
								,String sDoiDVQLTB	
								,String sMaT
								,String sTenT
								,String sKyTen
								,String sNguoi_gt	
								,String sMa_ns								
)
   		{
   			String s="begin ?:= "+sFunSchema+"pttb_capnhat.capnhat_kh_pttb1("
						+"'" +getUserVar("userID") +"'"
						+",'"+getUserVar("sys_agentcode")+"'"
						+",'"+getUserVar("sys_dataschema")	+"'"	
						
					    +",'"+sMaKHCu+"'"
						+",'"+sMaKH+"'"
						+",'"+sMaCQ+"'"
						+",'"+sCoQuan+"'"
						+",'"+sTenTT+"'"
						+",'"+sSoDaiDien+"'"
						+",'"+sNguoiDaiDien+"'"
						+",'"+sDienThoaiLH+"'"
						+",'"+sPHAI+"'"
						+",'"+sEmail+"'"
						+",'"+sNgaySinh+"'"
						+",'"+sSoGiayTo+"'"
						+",'"+sNoiCapGT+"'"
						+",'"+sNgayCapGT+"'"
						+",'"+sSoGT1+"'"
						+",'"+sNoiCapGT1+"'"
						+",'"+sNgayCapGT1+"'"
						+",'"+suanct_id+"'"
						+",'"+shuongct_id+"'"
						+",'"+shoct_id+"'"
						+",'"+sonhact_id+"'"
						+",'"+sDiaChiCT+"'"
						+",'"+sMSThue+"'"
						+",'"+sTaiKhoan+"'"
						+",'"+suantt_id+"'"
						+",'"+shuongtt_id+"'"
						+",'"+shott_id+"'"
						+",'"+sonhatt_id+"'"
						+",'"+sDiaChiTT+"'"
						+",'"+sDangKyTV+"'"
						+",'"+sDangKyDB+"'"
						+",'"+sKHRR+"'"
						+",'"+sNguoiCN+"'"
						+",'"+sMayCN+"'"
						+",'"+sMaNV+"'"
						+",'"+sTenNV+"'"
						+",'"+sGhiChu+"'"
						+",'"+sLoaiGTID+"'"
						+",'"+sLoaiGTID1+"'"
						+",'"+sLoaiKH+"'"
						+",'"+sDiaDiemTT+"'"
						+",'"+sNganHang+"'"
						+",'"+sChuyenKhoan+"'"
						+",'"+sDonViQL+"'"
						+",'"+sMaBC+"'"
						+",'"+sKHLon+"'"
						+",'"+sUuTien+"'"
						+",'"+sNganhNghe+"'"
						+",'"+sDoiDVQLTB+"'"	
						+",'"+sMaT+"'"
						+",'"+sTenT+"'"
						+",'"+sKyTen+"'"
						+",'"+sNguoi_gt+"'"
						+",'"+sMa_ns+"'"						
						+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
  
    public  String capNhatDanhBa(	
								 String sUserid
								,String sMaTinh
								,String sDataSchema
								,String sFunSchema				
								,String sSOMAY
								,String sMA_KH
								,String sTEN_TB
								,String sTEN_DB
								,String sCUOC_TB
								,String sTYLE_VAT
								,String sINCHITIET
								,String sTRACUU_DB
								,String sDANGKY_DB
								,String sTHUTU_IN
								,String sNGAY_LD
								,String sNGAY_SN
								,String sNGAY_THUHOI
								,String sNGAY_CN
								,String sMAY_CN
								,String sNGUOI_CN
								,String sKHACHHANG_CU
								,String squan_id
								,String sphuong_id
								,String spho_id
								,String sso_nha
								,String sDIACHI
								,String sGHICHU
								,String sDONVIQL_ID
								,String sMA_BC
								,String sDOITUONG_ID
								,String sLOAITB_ID
								,String sTRANGTHAI_ID								
								,String piloaidcbc
								,String  pikieubc
								,String pinvpt
								,String pinvql
								,String pimabaocuoc
								,String sNGAY_SINH
								,String sEmail
								)
   		{
   			
			
			String s="begin ?:= "+sFunSchema+"pttb_capnhat.capnhat_db_pttb("
						+"'" +getUserVar("userID") +"'"
						+",'"+getUserVar("sys_agentcode")+"'"
						+",'"+getUserVar("sys_dataschema")	+"'"					
						+",'"+sSOMAY+"'"
						+",'"+sMA_KH+"'"
						+",'"+sTEN_TB+"'"
						+",'"+sTEN_DB+"'"
						+",'"+sCUOC_TB+"'"
						+",'"+sTYLE_VAT+"'"
						+",'"+sINCHITIET+"'"
						+",'"+sTRACUU_DB+"'"
						+",'"+sDANGKY_DB+"'"
						+",'"+sTHUTU_IN+"'"
						+",'"+sNGAY_LD+"'"
						+",'"+sNGAY_SN+"'"
						+",'"+sNGAY_THUHOI+"'"
						+",'"+sNGAY_CN+"'"
						+",'"+sMAY_CN+"'"
						+",'"+sNGUOI_CN+"'"
						+",'"+sKHACHHANG_CU+"'"
						+",'"+squan_id+"'"
						+",'"+sphuong_id+"'"
						+",'"+spho_id+"'"
						+",'"+sso_nha+"'"
						+",'"+sDIACHI+"'"
						+",'"+sGHICHU+"'"
						+",'"+sDONVIQL_ID+"'"
						+",'"+sMA_BC+"'"
						+",'"+sDOITUONG_ID+"'"
						+",'"+sLOAITB_ID+"'"
						+",'"+sTRANGTHAI_ID+"'"
						+",'"+piloaidcbc +"'"
						+",'"+pikieubc +"'"
						+",'"+pinvpt +"'"
						+",'"+pinvql +"'"
						+",'"+pimabaocuoc+"'"
						+",'"+sNGAY_SINH+"'"
						+",'"+sEmail+"'"

			+") ; end;" ;
			System.out.println("SQL: "+s);
			return s;
   			}
  
  
    public String layds_tb(String sInput) {
    return "/main?"+CesarCode.encode("configFile")+"=PTTB/capnhat/khachhang/ajax_layds_tb_trong_kh"
        +"&"+CesarCode.encode("sInput")+"="+sInput
        ;
  }
  
      public String layds_tb_cua_coquan(String sInput) {
    return "/main?"+CesarCode.encode("configFile")+"=PTTB/capnhat/danhba/ajax_layds_tbdd_trong_cq"
        +"&"+CesarCode.encode("somay")+"="+sInput
        ;
  }
  
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
