import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { UtilsService } from 'app/core/services/utils.service';

@Component({
    selector: 'app-access-denied',
    templateUrl: './access-denied.component.html'
})

export class AccessDeniedComponent implements OnInit, OnDestroy {

    constructor(private router: Router, private utilsService: UtilsService) { }

    ngOnInit() {
    }

    ngOnDestroy() {
    }
}
