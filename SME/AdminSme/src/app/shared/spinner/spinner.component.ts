import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { UtilsService } from 'app/core/services/utils.service';

@Component({
    selector: 'app-spinner',
    templateUrl: './spinner.component.html'
})

export class SpinnerComponent implements OnInit, OnDestroy {

    constructor(private router: Router, private utilsService: UtilsService) { }

    ngOnInit() {
    }

    ngOnDestroy() {
    }
}
