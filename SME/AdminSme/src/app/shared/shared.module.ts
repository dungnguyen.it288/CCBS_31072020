import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from "ngx-spinner";

import { SpinnerComponent } from './spinner/spinner.component';
import { AccessDeniedComponent } from './access-denied/access-denied.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgxSpinnerModule
    ],
    declarations: [
        SpinnerComponent,
        AccessDeniedComponent
    ],
    exports: [
        SpinnerComponent,
        AccessDeniedComponent
    ]
})

export class SharedModule {

    static forRoot() {
        return {
            ngModule: SharedModule,
            providers: []
        }
    }
}
