import { Component, OnInit } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';

declare const $: any;

//Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}

//Menu Items
export const ROUTES: RouteInfo[] = [
    {
        path: '/profile',
        title: 'Tổng quan',
        type: 'link',
        icontype: 'dashboard'
    },
    {
        path: '/system',
        title: 'Hệ thống',
        type: 'sub',
        icontype: 'apps',
        collapse: 'system',
        children: [
            { path: 'users', title: 'Người dùng', ab: '-' },
            { path: 'usergroups', title: 'Nhóm người dùng', ab: '-' },
            { path: 'userroles', title: 'Vai trò người dùng', ab: '-' }
        ]
    },
    {
        path: '/product',
        title: 'Quản lý sản phẩm',
        type: 'sub',
        icontype: 'apps',
        collapse: 'product',
        children: [
            { path: 'add-category', title: 'Thêm mới danh mục', ab: '-' },
            { path: 'add-new-product', title: 'Thêm mới sản phẩm', ab: '-' },
            { path: 'add-attr', title: 'Thêm mới thuộc tính', ab: '-' },
            { path: 'add-attr-set', title: 'Thêm mới tập thuộc tính', ab: '-' },
            { path: 'add-new-customer-service', title: 'Thêm mới dịch vụ', ab: '-' },
            { path: 'add-customer-object', title: 'Thêm mới đối tượng khách hàng', ab: '-' }
        ]
    },
    // {
    //     path: '/datacodes',
    //     title: 'Datacodes',
    //     type: 'sub',
    //     icontype: 'content_paste',
    //     collapse: 'datacodes',
    //     children: [
    //         { path: 'codes', title: 'Mã Data', ab: '-' },
    //         { path: 'requests', title: 'Phiếu yêu cầu', ab: '-' }
    //     ]
    // },
    {
        path: '/reports',
        title: 'Báo cáo',
        type: 'sub',
        icontype: 'timeline',
        collapse: 'reports',
        children: [
            { path: 'report-vnp-smeflex', title: 'Báo cáo SME Flex', ab: '-' },
            { path: 'report-vnp-detail', title: 'Báo cáo chi tiết', ab: '-' }
        ]
    }
];
@Component({
    selector: 'app-sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    username: String;
    public menuItems: any[];
    ps: any;
    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    ngOnInit() {
        this.username = localStorage.getItem('user_name');
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            this.ps = new PerfectScrollbar(elemSidebar);
        }
    }
    updatePS(): void {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            this.ps.update();
        }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }
}
