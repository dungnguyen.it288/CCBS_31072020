import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-layout',
    templateUrl: './auth-layout.component.html'
})
export class AuthLayoutComponent implements OnInit {
    private toggleButton: any;
    private sidebarVisible: boolean;
    mobile_menu_visible: any = 0;
    private _router: Subscription;

    constructor(private router: Router, private element: ElementRef) {
        this.sidebarVisible = false;
    }
    ngOnInit() {
        const navbar: HTMLElement = this.element.nativeElement;

        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
        this._router = this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: NavigationEnd) => {
            this.sidebarClose();
        });
    }
    sidebarOpen() {
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('nav-open');

        setTimeout(function () {
            this.toggleButton.classList.add('toggled');
        }.bind(this), 400);

        var $layer = document.createElement('div');
        $layer.setAttribute('class', 'close-layer');


        if (body.querySelectorAll('.wrapper-full-page')) {
            document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
        } else if (body.classList.contains('off-canvas-sidebar')) {
            document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
        }

        setTimeout(function () {
            $layer.classList.add('visible');
        }, 100);

        $layer.onclick = function () { //asign a function
            body.classList.remove('nav-open');
            $layer.classList.remove('visible');
            setTimeout(function () {
                $layer.remove();
                this.toggleButton.classList.remove('toggled');
            }.bind(this), 400);

            this.mobile_menu_visible = 0;
            this.sidebarVisible = false;
        }.bind(this);

        this.mobile_menu_visible = 1;
        this.sidebarVisible = true;
    };
    sidebarClose() {
        const $layer = document.getElementsByClassName('close-layer')[0];
        const body = document.getElementsByTagName('body')[0];

        body.classList.remove('nav-open');
        if ($layer) $layer.classList.remove('visible');
        setTimeout(function () {
            if ($layer) $layer.remove();
            this.toggleButton.classList.remove('toggled');
        }.bind(this), 10);

        this.mobile_menu_visible = 0;
        this.sidebarVisible = false;
    };
    sidebarToggle() {
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    }
}
