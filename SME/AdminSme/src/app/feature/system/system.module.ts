import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/app.module';
import { SharedModule } from 'app/shared/shared.module';

import { SystemRoutes } from './system.routing';

import { UsersComponent } from './users/users.component';
import { RootUsersComponent } from './users/roles/root-users.component';

import { UsersService } from './users/users.service';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(SystemRoutes),
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        SharedModule
    ],
    declarations: [
        UsersComponent,
        RootUsersComponent
    ],
    providers: [
        UsersService
    ]
})

export class SystemModule { }
