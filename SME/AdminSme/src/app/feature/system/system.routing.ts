import { Routes } from '@angular/router';

import { UsersComponent } from './users/users.component';

export const SystemRoutes: Routes = [
    {
        path: 'users',
        component: UsersComponent
    }
];
