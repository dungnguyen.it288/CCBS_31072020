import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { UtilsService } from 'app/core/services/utils.service';
import { UsersService } from '../users.service';

@Component({
    selector: 'app-root-users',
    templateUrl: './root-users.component.html'
})
export class RootUsersComponent implements OnInit, OnDestroy {
    showTable = false;
    users: Array<Object>;

    id: String;
    name: String;
    username: String;
    reseller_id: String;
    customer_id: String;

    page: Number;
    limit: Number;
    total: Number;
    end: Number;

    create_full_name: String; create_full_name_control = new FormControl('', [Validators.required]);
    create_user_name: String; create_user_name_control = new FormControl('', [Validators.required]);
    create_user_password: String; create_user_password_control = new FormControl('', [Validators.required]);
    create_created_user: String;
    create_email: String; create_email_control = new FormControl('', [Validators.required, Validators.email]);
    create_telephone: String; create_telephone_control = new FormControl('', [Validators.required]);
    create_is_enabled: Boolean;
    create_is_locked: Boolean;
    create_must_change_pass_next_logon: Boolean;
    create_cant_change_pass: Boolean;
    create_pass_never_expire: Boolean;
    create_start_date: Date;
    create_end_date: Date;

    update_id: String;
    update_full_name: String; update_full_name_control = new FormControl('', [Validators.required]);
    update_last_updated_user: String;
    update_email: String; update_email_control = new FormControl('', [Validators.required, Validators.email]);
    update_telephone: String; update_telephone_control = new FormControl('', [Validators.required]);
    update_is_enabled: Boolean;
    update_is_locked: Boolean;
    update_must_change_pass_next_logon: Boolean;
    update_cant_change_pass: Boolean;
    update_pass_never_expire: Boolean;
    update_start_date: Date;
    update_end_date: Date;

    constructor(private router: Router, private utilsService: UtilsService, private usersService: UsersService) { }

    ngOnInit() {
        this.page = 1;
        this.limit = 10;
        this.paginate(true);
    }

    ngOnDestroy() {
    }

    paginate(spinner: Boolean) {
        spinner ? this.utilsService.showSpinner() : 0;
        let query = {
            pagesize: this.page || 1,
            pageindex: this.limit || 10
        };
        this.usersService.getUserList(query, (data) => {
            this.users = data.result;

            // this.page = data.paginate.page;
            // this.limit = data.paginate.limit;
            this.total = this.users.length > 0 ? parseInt(this.users[0]['P_TOTAL']) : 0;
            this.end = Math.ceil(this.total.valueOf() / this.limit.valueOf());

            this.showTable = (this.users.length > 0);
            spinner ? this.utilsService.hideSpinner() : 0;
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            spinner ? this.utilsService.hideSpinner() : 0;
        });
    }

    search() {
        this.paginate(true);
    }

    limitChange() {
        this.page = 1;
        this.paginate(true);
    }

    pageChange(page: Number) {
        this.page = page;
        this.paginate(true);
    }

    onCreateMode() {
        this.create_full_name = '';
        this.create_user_name = '';
        this.create_user_password = '';
        this.create_created_user = localStorage.getItem('user_id');
        this.create_email = '';
        this.create_telephone = '';
        this.create_is_enabled = false;
        this.create_is_locked = false;
        this.create_must_change_pass_next_logon = false;
        this.create_cant_change_pass = false;
        this.create_pass_never_expire = false;
    }

    create() {
        if (
            !this.create_full_name ||
            !this.create_user_name ||
            !this.create_user_password ||
            !this.create_email ||
            !this.create_telephone
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        } else if (
            this.create_full_name.trim() === '' ||
            this.create_user_name.trim() === '' ||
            this.create_user_password.trim() === '' ||
            this.create_email.trim() === '' ||
            this.create_telephone.trim() === ''
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        }

        this.utilsService.showSpinner();
        let body = {
            full_name: this.create_full_name,
            user_name: this.create_user_name,
            user_password: this.create_user_password,
            created_user: parseInt(this.create_created_user.valueOf()),
            email: this.create_email,
            telephone: this.create_telephone,
            is_enabled: this.create_is_enabled ? 1 : 0,
            is_locked: this.create_is_locked ? 1 : 0,
            must_change_pass_next_logon: this.create_must_change_pass_next_logon ? 1 : 0,
            cant_change_pass: this.create_cant_change_pass ? 1 : 0,
            pass_never_expire: this.create_pass_never_expire ? 1 : 0,
            start_date: this.utilsService.formatDatetoString(this.create_start_date),
            end_date: this.utilsService.formatDatetoString(this.create_end_date)
        };
        this.usersService.createUser(body, (data) => {
            if (data.error_code === '0') {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Tạo người dùng thành công');
                document.getElementById('createUserClose').click();
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Tạo người dùng không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }

    onUpdateMode(user) {
        this.update_id = user.ID;
        this.update_full_name = user.FULL_NAME;
        this.update_last_updated_user = localStorage.getItem('user_id');
        this.update_email = user.EMAIL;
        this.update_telephone = user.TELEPHONE;
        this.update_is_enabled = user.IS_ENABLED === '0' ? false : true;
        this.update_is_locked = user.IS_LOCKED === '0' ? false : true;
        this.update_must_change_pass_next_logon = false;
        this.update_cant_change_pass = false;
        this.update_pass_never_expire = false;
        this.update_start_date = new Date(this.utilsService.formatStringtoDate(user.START_DATE));
        this.update_end_date = new Date(this.utilsService.formatStringtoDate(user.END_DATE));
    }

    update() {
        if (
            !this.update_id ||
            !this.update_full_name ||
            !this.update_email ||
            !this.update_telephone
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        } else if (
            this.update_id.trim() === '' ||
            this.update_full_name.trim() === '' ||
            this.update_email.trim() === '' ||
            this.update_telephone.trim() === ''
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        }

        this.utilsService.showSpinner();
        this.usersService.updateUser({
            id: parseInt(this.update_id.valueOf()),
            full_name: this.update_full_name,
            last_updated_user: parseInt(this.update_last_updated_user.valueOf()),
            email: this.update_email,
            telephone: this.update_telephone,
            is_enabled: this.update_is_enabled ? 1 : 0,
            is_locked: this.update_is_locked ? 1 : 0,
            must_change_pass_next_logon: this.update_must_change_pass_next_logon ? 1 : 0,
            cant_change_pass: this.update_cant_change_pass ? 1 : 0,
            pass_never_expire: this.update_pass_never_expire ? 1 : 0,
            start_date: this.utilsService.formatDatetoString(this.update_start_date),
            end_date: this.utilsService.formatDatetoString(this.update_end_date)
        }, (data) => {
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Cập nhật thông tin người dùng thành công');
                document.getElementById('updateUserClose').click();
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Cập nhật thông tin người dùng không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }

    delete(user) {
        this.utilsService.showSpinner();
        this.usersService.deleteUser({
            id: parseInt(user.ID)
        }, (data) => {
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Xóa người dùng thành công');
                document.getElementById('updateUserClose').click();
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Xóa người dùng không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }

}
