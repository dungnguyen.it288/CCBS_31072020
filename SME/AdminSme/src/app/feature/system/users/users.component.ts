import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { UtilsService } from 'app/core/services/utils.service';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html'
})

export class UsersComponent implements OnInit, OnDestroy {
    level: String;

    constructor(private router: Router, private utilsService: UtilsService) { }

    ngOnInit() {
        // this.utilsService.checkModulePermission('users');
        // this.level = localStorage.getItem('unit_level');
    }

    ngOnDestroy() {
    }
}
