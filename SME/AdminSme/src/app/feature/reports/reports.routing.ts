import { Routes } from '@angular/router';

import { RptVnpSmeflexComponent } from './report-vnp-smeflex/report-vnp-smeflex.component';
import { RptVnpDetailComponent } from './report-vnp-detail/report-vnp-detail.component';

export const ReportsRoutes: Routes = [
    {
        path: 'report-vnp-smeflex',
        component: RptVnpSmeflexComponent
    },
    {
        path: 'report-vnp-detail',
        component: RptVnpDetailComponent
    }
];
