import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from 'app/core/config';

@Injectable()
export class RptVnpSmeflexService {

    constructor(private http: HttpClient) { }


    getSmeFlexList(smeflexInfo, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            }
        };
        this.http.post(`${config.adminUrl}/api/Orders/vnpOrderReportFlex`, smeflexInfo, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }
}
