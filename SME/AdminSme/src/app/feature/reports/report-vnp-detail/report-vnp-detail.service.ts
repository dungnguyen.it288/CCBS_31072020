import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from 'app/core/config';

@Injectable()
export class RptVnpDetailService {

    constructor(private http: HttpClient) { }


    getDetailList(detailInfo, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            }
        };
        this.http.post(`${config.adminUrl}/api/Orders/vnpOrderReportDetail`, detailInfo, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }
}
