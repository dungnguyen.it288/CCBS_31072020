import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { UtilsService } from 'app/core/services/utils.service';
import { RptVnpDetailService } from '../report-vnp-detail/report-vnp-detail.service';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'report-vnp-detail',
    templateUrl: './report-vnp-detail.component.html',
    styleUrls: ['./report-vnp-detail.component.css']
})
export class RptVnpDetailComponent implements OnInit, OnDestroy {
    showTable = false;
    detailList: Array<Object>;
    attrSets: Array<Object>;

    id: String;
    name: String;
    username: String;
    reseller_id: String;
    customer_id: String;
    
    rpt_start_date: Date; rpt_start_date_control = new FormControl('', [Validators.required]);
    rpt_end_date: Date; rpt_end_date_control = new FormControl('', [Validators.required]);

    page: Number;
    limit: Number;
    total: Number;
    end: Number;

    create_code_category: String; create_code_category_control = new FormControl('', [Validators.required]);
    create_name_category: String; create_name_category_control = new FormControl('', [Validators.required]);
    create_parent_category: String; create_parent_category_control = new FormControl('', [Validators.required]);
    create_attr_category: String; create_attr_category_control = new FormControl('', [Validators.required]);

    update_id: String;
    update_code_category: String; update_code_category_control = new FormControl('', [Validators.required]);
    update_name_category: String; update_name_category_control = new FormControl('', [Validators.required]);
    update_parent_category: String; update_parent_category_control = new FormControl('', [Validators.required]);
    update_attr_category: String; update_attr_category_control = new FormControl('', [Validators.required]);

    constructor(private router: Router, private utilsService: UtilsService, private RptVnpDetailService: RptVnpDetailService, private datepipe: DatePipe) { }

    ngOnInit() {
        this.page = 1;
        this.limit = 10;
    }

    ngOnDestroy() {
    }

    paginate(spinner: Boolean) {
        spinner ? this.utilsService.showSpinner() : 0;
        let query = {
            pagesize: this.page || 1,
            pageindex: this.limit || 10,
            prov:"HNI",
            fromDate: this.datepipe.transform(this.rpt_start_date, 'yyyy-MM-dd'),
            toDate: this.datepipe.transform(this.rpt_end_date, 'yyyy-MM-dd'),
            status: -1,
            customer_service_id:-1
        };
        this.RptVnpDetailService.getDetailList(query, (data) => {
            this.detailList = data.result;
            console.log(query);

            this.total = this.detailList.length > 0 ? parseInt(this.detailList[0]['P_TOTAL']) : 0;
            this.end = Math.ceil(this.total.valueOf() / this.limit.valueOf());

            this.showTable = (this.detailList.length > 0);
            spinner ? this.utilsService.hideSpinner() : 0;
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error_message);
            spinner ? this.utilsService.hideSpinner() : 0;
        });
    }

    search() {
      if (
        !this.rpt_start_date ||
        !this.rpt_end_date
      ) {
          this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
          return;
      } else
      {    
        this.paginate(true);
      }
    }

    limitChange() {
        this.page = 1;
        this.paginate(true);
    }

    pageChange(page: Number) {
        this.page = page;
        this.paginate(true);
    }

}
