import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/app.module';
import { SharedModule } from 'app/shared/shared.module';

import { ReportsRoutes } from './reports.routing';

import { RptVnpSmeflexComponent } from './report-vnp-smeflex/report-vnp-smeflex.component';
import { RptVnpDetailComponent } from './report-vnp-detail/report-vnp-detail.component';

import { RptVnpSmeflexService } from './report-vnp-smeflex/report-vnp-smeflex.service';
import { RptVnpDetailService } from './report-vnp-detail/report-vnp-detail.Service';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ReportsRoutes),
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        SharedModule
    ],
    declarations: [RptVnpSmeflexComponent, RptVnpDetailComponent],
    providers: [
        RptVnpSmeflexService,
        RptVnpDetailService
    ]
})

export class ReportsModule { }
