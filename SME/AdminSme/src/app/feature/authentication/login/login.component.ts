import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { UtilsService } from 'app/core/services/utils.service';
import { AuthenticationService } from '../authentication.service';

@Component({
    selector: 'app-login-cmp',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit, OnDestroy {
    test: Date = new Date();
    private toggleButton: any;
    private sidebarVisible: boolean;
    private nativeElement: Node;

    username: String;
    password: String;
    useSSO: Boolean;

    constructor(private element: ElementRef, private utilsService: UtilsService, private authenticationService: AuthenticationService, private router: Router) {
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }

    ngOnInit() {
        var navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');
        body.classList.add('off-canvas-sidebar');
        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            const card = document.getElementsByClassName('card')[0];
            card.classList.remove('card-hidden');
        }, 700);
    }

    sidebarToggle() {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        var sidebar = document.getElementsByClassName('navbar-collapse')[0];
        if (this.sidebarVisible == false) {
            setTimeout(function () {
                toggleButton.classList.add('toggled');
            }, 500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        } else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    }

    ngOnDestroy() {
        const body = document.getElementsByTagName('body')[0];
        body.classList.remove('login-page');
        body.classList.remove('off-canvas-sidebar');
    }

    login() {
        this.useSSO ?
            this.authenticationService.loginSSO({
                username: this.username,
                password: this.password
            }, (data) => {
                if (!data.token) {
                    this.utilsService.showNotification('bottom', 'right', 'danger', 'Đăng nhập thất bại, vui lòng thử lại');
                } else {
                    this.utilsService.showNotification('bottom', 'right', 'info', `Chào mừng ${this.username} !`);
                    localStorage.setItem('accessToken', data.token);
                    localStorage.setItem('user_name', this.username.valueOf());
                    this.router.navigate(['/']);
                }
            }, (err) => {
                this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            }) :
            this.authenticationService.login({
                username: this.username,
                password: this.password
            }, (data) => {
                if (!data.token) {
                    this.utilsService.showNotification('bottom', 'right', 'danger', 'Đăng nhập thất bại, vui lòng thử lại');
                } else {
                    this.utilsService.showNotification('bottom', 'right', 'info', `Chào mừng ${this.username} !`);
                    localStorage.setItem('accessToken', data.token);
                    localStorage.setItem('user_name', this.username.valueOf());
                    this.router.navigate(['/']);
                }
            }, (err) => {
                this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            });
    }
}
