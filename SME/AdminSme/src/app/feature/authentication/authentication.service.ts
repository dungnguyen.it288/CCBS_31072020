import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from 'app/core/config';

@Injectable()
export class AuthenticationService {

    constructor(private http: HttpClient) { }

    login(userInfo, done, fail) {
        this.http.post(`${config.adminUrl}/login`, userInfo).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }

    loginSSO(userInfo, done, fail) {
        this.http.post(`${config.adminUrl}/ssologin`, userInfo).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }

    getAccessTokenInfo(done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            }
        };
        let body = {
            token: localStorage.getItem('accessToken')
        }
        this.http.post(`${config.adminUrl}/api/Account/getInfoAccountByToken`, body, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }

    getUserInfo(userInfo, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            }
        };
        this.http.post(`${config.adminUrl}/api/Account/getInfoAccount`, userInfo, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }
}
