import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { UtilsService } from 'app/core/services/utils.service';
import { AddAttrService } from './add-attribute.service';

@Component({
    selector: 'app-add-attribute',
    templateUrl: './add-attribute.component.html',
    styleUrls: ['./add-attribute.component.css']
})
export class AddAttrComponent implements OnInit, OnDestroy {
    showTable = false;
    attrs: Array<Object>;
    attrTypes: Array<Object>;

    id: String;
    name: String;
    username: String;
    reseller_id: String;
    customer_id: String;

    page: Number;
    limit: Number;
    total: Number;
    end: Number;

    create_code_attr: String; create_code_attr_control = new FormControl('', [Validators.required]);
    create_name_attr: String; create_name_attr_control = new FormControl('', [Validators.required]);
    create_type_attr: String; create_type_attr_control = new FormControl('', [Validators.required]);
    create_is_used: Boolean;

    update_id: String;
    update_code_attr: String; update_code_attr_control = new FormControl('', [Validators.required]);
    update_name_attr: String; update_name_attr_control = new FormControl('', [Validators.required]);
    update_type_attr: String; update_type_attr_control = new FormControl('', [Validators.required]);
    update_is_used: Boolean;

    constructor(private router: Router, private utilsService: UtilsService, private AddAttrService: AddAttrService) { }

    ngOnInit() {
        this.page = 1;
        this.limit = 10;
        this.paginate(true);
    }

    ngOnDestroy() {
    }

    paginate(spinner: Boolean) {
        spinner ? this.utilsService.showSpinner() : 0;
        let query = {
            pagesize: this.page || 1,
            pageindex: this.limit || 10
        };
        this.AddAttrService.getAttributeList(query, (data) => {
            this.attrs = data.result.productAttrList;
            // this.page = data.paginate.page;
            // this.limit = data.paginate.limit;
            this.total = this.attrs.length > 0 ? parseInt(this.attrs[0]['P_TOTAL']) : 0;
            this.end = Math.ceil(this.total.valueOf() / this.limit.valueOf());

            this.showTable = (this.attrs.length > 0);
            spinner ? this.utilsService.hideSpinner() : 0;
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error_message);
            spinner ? this.utilsService.hideSpinner() : 0;
        });
        
        this.AddAttrService.getAttributeList(query, (data) => {
            this.attrTypes = data.result.attrTypeList;
            // this.page = data.paginate.page;
            // this.limit = data.paginate.limit;
            this.total = this.attrTypes.length > 0 ? parseInt(this.attrTypes[0]['P_TOTAL']) : 0;
            this.end = Math.ceil(this.total.valueOf() / this.limit.valueOf());

            this.showTable = (this.attrTypes.length > 0);
            spinner ? this.utilsService.hideSpinner() : 0;
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error_message);
            spinner ? this.utilsService.hideSpinner() : 0;
        });
    }

    search() {
        this.paginate(true);
    }

    limitChange() {
        this.page = 1;
        this.paginate(true);
    }

    pageChange(page: Number) {
        this.page = page;
        this.paginate(true);
    }

    onCreateMode() {
        this.create_code_attr = '';
        this.create_name_attr = '';
        this.create_type_attr = '';
        this.create_is_used = false;
    }

    create() {
        if (
            !this.create_code_attr ||
            !this.create_name_attr ||
            !this.create_type_attr
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        } else if (
            this.create_code_attr.trim() === '' ||
            this.create_name_attr.trim() === ''
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        }

        this.utilsService.showSpinner();
        let body = {
            id: 0,
            attrKey: this.create_code_attr,
            attrName: this.create_name_attr,
            ctlProductAttrTypeId: this.create_type_attr,
            isUsed: this.create_is_used,
        };
        this.AddAttrService.createAttribute(body, (data) => {
            console.log(body);
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Tạo danh mục thành công');
                document.getElementById('createAttrsClose').click();
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Tạo danh mục không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }

    onUpdateMode(attrs) {
        this.update_id =  attrs.id;
        this.update_code_attr = attrs.attrKey;
        this.update_name_attr = attrs.attrName;
        this.update_type_attr = attrs.ctlProductAttrTypeId;
        this.update_is_used = attrs.isUsed;
    }

    update() {
        if (
            !this.update_name_attr ||
            !this.update_code_attr ||
            !this.update_type_attr
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        } else if (
            this.update_name_attr.trim() === '' ||
            this.update_code_attr.trim() === ''
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        }
        this.utilsService.showSpinner();
        let body = {
            id: this.update_id,
            attrKey: this.update_code_attr,
            attrName: this.update_name_attr,
            ctlProductAttrTypeId: this.update_type_attr,
            isUsed: this.update_is_used
        };
        console.log(body);
        this.AddAttrService.updateAttribute(body, (data) => {
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Cập nhật thông tin danh mục thành công');
                document.getElementById('updateAttrsClose').click();
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Cập nhật thông tin danh mục không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }

    delete(attrs) {
        this.utilsService.showSpinner();
        this.AddAttrService.deleteAttribute({
            id: parseInt(attrs.id)
        }, (data) => {
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Xóa danh mục thành công');
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Xóa danh mục không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }
}
