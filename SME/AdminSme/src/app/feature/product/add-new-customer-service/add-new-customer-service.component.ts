import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { UtilsService } from 'app/core/services/utils.service';
import { AddNewCusService } from '../add-new-customer-service/add-new-customer-service.service';
import { AddCustomerObjectService } from '../add-customer-object/add-customer-object.service';

@Component({
    selector: 'app-add-new-customer-service',
    templateUrl: './add-new-customer-service.component.html',
    styleUrls: ['./add-new-customer-service.component.css']
})
export class AddCusServiceComponent implements OnInit, OnDestroy {
    showTable = false;
    cusService: Array<Object>;
    cusObject: Array<Object>;
    productList: Array<Object>;
    childList: Array<Object>;

    id: String;
    name: String;
    username: String;
    reseller_id: String;
    customer_id: String;

    productcode: String;
    productname: String;

    page: Number;
    limit: Number;
    total: Number;
    end: Number;

    create_code_cus_service: String; create_code_cus_service_control = new FormControl('', [Validators.required]);
    create_name_cus_service: String; create_name_cus_service_control = new FormControl('', [Validators.required]);
    create_cus_object: String; create_cus_object_control = new FormControl('', [Validators.required]);
    create_is_active: Boolean;
    create_visibility: Boolean;
    create_child_service: String; create_child_service_control = new FormControl('', [Validators.required]);

    create_order_child: String; create_order_child_control = new FormControl('', [Validators.required]);
    create_group_child: String; create_group_child_control = new FormControl('', [Validators.required]);
    create_compulsory_child: Boolean;

    update_id: String;
    update_code_cus_service: String; update_code_cus_service_control = new FormControl('', [Validators.required]);
    update_name_cus_service: String; update_name_cus_service_control = new FormControl('', [Validators.required]);
    update_cus_object: String; update_cus_object_control = new FormControl('', [Validators.required]);
    update_is_active: Boolean;
    update_visibility: Boolean;
    update_child_service: String; update_child_service_control = new FormControl('', [Validators.required]);

    update_code_child: String; update_code_child_control = new FormControl('', [Validators.required]);
    update_name_child: String; update_name_child_control = new FormControl('', [Validators.required]);
    update_order_child: String; update_order_child_control = new FormControl('', [Validators.required]);
    update_group_child: String; update_group_child_control = new FormControl('', [Validators.required]);
    update_compulsory_child: Boolean;

    constructor(private router: Router, private utilsService: UtilsService, private AddNewCusService: AddNewCusService, private AddCustomerObjectService: AddCustomerObjectService) { }

    ngOnInit() {
        this.page = 1;
        this.limit = 10;
        this.paginate(true);
            this.paginateProduct(true);
    }

    ngOnDestroy() {
    }

    paginate(spinner: Boolean) {
        spinner ? this.utilsService.showSpinner() : 0;
        let query = {
            pagesize: this.page || 1,
            pageindex: this.limit || 10
        };
        this.AddNewCusService.getCusServiceList(query, (data) => {
            this.cusService = data.result.serviceList;

            this.total = this.cusService.length > 0 ? parseInt(this.cusService[0]['P_TOTAL']) : 0;
            this.end = Math.ceil(this.total.valueOf() / this.limit.valueOf());

            this.showTable = (this.cusService.length > 0);
            spinner ? this.utilsService.hideSpinner() : 0;
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error_message);
            spinner ? this.utilsService.hideSpinner() : 0;
        });
        
        this.AddCustomerObjectService.getCustomerObjectList(query, (data) => {
            this.cusObject = data.result.customerObjectList;

            console.log(this.cusObject);
            this.total = this.cusObject.length > 0 ? parseInt(this.cusObject[0]['P_TOTAL']) : 0;
            this.end = Math.ceil(this.total.valueOf() / this.limit.valueOf());

            this.showTable = (this.cusObject.length > 0);
            spinner ? this.utilsService.hideSpinner() : 0;
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error_message);
            spinner ? this.utilsService.hideSpinner() : 0;
        });
    }

    paginateProduct(spinner: Boolean) {
        spinner ? this.utilsService.showSpinner() : 0;
        let query = {
            pagesize: this.page || 1,
            pageindex: this.limit || 10
        };
        this.AddNewCusService.getCusServiceList(query, (data) => {
            this.productList = data.result.productList;

            this.total = this.productList.length > 0 ? parseInt(this.productList[0]['P_TOTAL']) : 0;
            this.end = Math.ceil(this.total.valueOf() / this.limit.valueOf());

            this.showTable = (this.productList.length > 0);
            spinner ? this.utilsService.hideSpinner() : 0;
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error_message);
            spinner ? this.utilsService.hideSpinner() : 0;
        });
        
    }

    addChild() {
        this.childList.push({
            prdCode: this.create_child_service,
            prdName: this.productList.find(x => x['prdCode'] ==  this.create_child_service)['prdName']
        });
    }

    addChildUp() {
        this.childList.push({
            prdCode: this.update_child_service,
            prdName: this.productList.find(x => x['prdCode'] ==  this.update_child_service)['prdName']
        });
    }

    search() {
        this.paginate(true);
    }

    searchProduct() {
        this.paginateProduct(true);
    }

    limitChange() {
        this.page = 1;
        this.paginate(true);
        this.paginateProduct(true);
    }

    pageChange(page: Number) {
        this.page = page;
        this.paginate(true);
        this.paginateProduct(true);
    }

    onCreateMode() {
        this.create_code_cus_service = '';
        this.create_name_cus_service = '';
        this.create_cus_object = '';
        this.create_is_active = false;
        this.create_visibility = false;
        this.childList = new Array<Object>();
    }

    create() {
        if (
            !this.create_code_cus_service ||
            !this.create_name_cus_service
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        } else if (
            this.create_code_cus_service.trim() === '' ||
            this.create_name_cus_service.trim() === ''
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        }

        this.utilsService.showSpinner();
        let body = {
            id: 0,
            svcCode: this.create_code_cus_service,
            svcName: this.create_name_cus_service,
            isUsed: null,
            ctlCustomerObjectId: this.create_cus_object,
            isActive: this.create_is_active,
            visibility: this.create_visibility,
        };
        this.AddNewCusService.createCusService(body, (data) => {
            console.log(body);
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Tạo danh mục thành công');
                document.getElementById('createCusServiceClose').click();
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Tạo danh mục không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
            this.paginateProduct(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }

    onUpdateMode(cusService) {
        this.update_id =  cusService.id;
        this.update_code_cus_service = cusService.svcCode;
        this.update_name_cus_service = cusService.svcName;
        this.update_cus_object = cusService.ctlCustomerObjectId;
        this.update_is_active = cusService.isActive;
        this.update_visibility = cusService.update_visibility;
        this.childList = cusService.ctlCustomerServiceProduct;
    }

    update() {
        if (
            !this.update_name_cus_service ||
            !this.update_code_cus_service
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        } else if (
            this.update_name_cus_service.trim() === '' ||
            this.update_code_cus_service.trim() === ''
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        }
        this.utilsService.showSpinner();
        let body = {
            id: this.update_id,
            svcCode: this.update_code_cus_service,
            svcName: this.update_name_cus_service,
            ctlCustomerObjectId: this.update_cus_object,
            isActive: this.update_is_active,
            visibility: this.update_visibility,
        };
        console.log(body);
        this.AddNewCusService.updateCusService(body, (data) => {
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Cập nhật thông tin danh mục thành công');
                document.getElementById('updateCusServiceClose').click();
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Cập nhật thông tin danh mục không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
            this.paginateProduct(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }

    delete(cusService) {
        this.utilsService.showSpinner();
        this.AddNewCusService.deleteCusService({
            id: parseInt(cusService.id)
        }, (data) => {
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Xóa danh mục thành công');
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Xóa danh mục không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
            this.paginateProduct(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }

    deleteChild(child) {
        let newChildList = new Array<Object>();
        this.childList.forEach((c, index) => {
            if (c['prdCode'] !== child['prdCode']) newChildList.push(c);
        });
        this.childList = newChildList;
    }
}
