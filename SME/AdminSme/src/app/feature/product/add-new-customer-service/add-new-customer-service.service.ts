import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from 'app/core/config';

@Injectable()
export class AddNewCusService {

    constructor(private http: HttpClient) { }

    getCusServiceList(filter, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            },
            param: filter
        };
        this.http.get(`${config.adminUrl}/api/Product/initCustomerService`, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }

    createCusService(cusserviceInfo, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            }
        };
        this.http.post(`${config.adminUrl}/api/Product/addNewCustomerService`, cusserviceInfo, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }

    updateCusService(cusserviceInfo, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            }
        };
        this.http.put(`${config.adminUrl}/api/Product/updateCustomerService`, cusserviceInfo, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }

    deleteCusService(cusserviceInfo, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            }
        };
        this.http.delete(`${config.adminUrl}/api/Product/deleteCustomerService/${cusserviceInfo.id}`, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }
    
}
