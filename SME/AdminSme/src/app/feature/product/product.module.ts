import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/app.module';
import { SharedModule } from 'app/shared/shared.module';

import { ProductRoutes } from './product.routing';

import { AddCategoryComponent } from './add-category/add-category.component';
import { AddAttrComponent } from './add-attribute/add-attribute.component';
import { AddAttrSetComponent } from './add-attribute-set/add-attribute-set.component';
import { AddNewProductComponent } from './add-new-product/add-new-product.component';
import { AddCusServiceComponent } from './add-new-customer-service/add-new-customer-service.component';
import { AddCustomerObjectComponent } from './add-customer-object/add-customer-object.component';

import { AddCategoryService } from './add-category/add-category.service';
import { AddAttrService } from './add-attribute/add-attribute.service';
import { AddAttrSetService } from './add-attribute-set/add-attribute-set.service';
import { AddNewCusService } from './add-new-customer-service/add-new-customer-service.service';
import { AddCustomerObjectService } from './add-customer-object/add-customer-object.service';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ProductRoutes),
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        SharedModule
    ],
    declarations: [AddCategoryComponent, AddAttrComponent, AddNewProductComponent, AddCustomerObjectComponent, AddAttrSetComponent, AddCusServiceComponent],
    providers: [
        AddCategoryService, AddAttrService, AddCustomerObjectService, AddAttrSetService, AddNewCusService
    ]
})

export class ProductModule { }
