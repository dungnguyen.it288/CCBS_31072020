import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { UtilsService } from 'app/core/services/utils.service';
import { AddCustomerObjectService } from '../add-customer-object/add-customer-object.service';

@Component({
    selector: 'app-add-customer-object',
    templateUrl: './add-customer-object.component.html',
    styleUrls: ['./add-customer-object.component.css']
})
export class AddCustomerObjectComponent implements OnInit, OnDestroy {
    showTable = false;
    cusObjects: Array<Object>;

    id: String;
    name: String;
    username: String;
    reseller_id: String;
    customer_id: String;

    page: Number;
    limit: Number;
    total: Number;
    end: Number;

    create_code_cusobject: String; create_code_cusobject_control = new FormControl('', [Validators.required]);
    create_name_cusobject: String; create_name_cusobject_control = new FormControl('', [Validators.required]);
    create_is_active: Boolean;

    update_id: String;
    update_code_cusobject: String; update_code_cusobject_control = new FormControl('', [Validators.required]);
    update_name_cusobject: String; update_name_cusobject_control = new FormControl('', [Validators.required]);
    update_is_active: Boolean;

    constructor(private router: Router, private utilsService: UtilsService, private AddCustomerObjectService: AddCustomerObjectService) { }

    ngOnInit() {
        this.page = 1;
        this.limit = 10;
        this.paginate(true);
    }

    ngOnDestroy() {
    }

    paginate(spinner: Boolean) {
        spinner ? this.utilsService.showSpinner() : 0;
        let query = {
            pagesize: this.page || 1,
            pageindex: this.limit || 10
        };
        this.AddCustomerObjectService.getCustomerObjectList(query, (data) => {
            this.cusObjects = data.result.customerObjectList;

            // this.page = data.paginate.page;
            // this.limit = data.paginate.limit;
            this.total = this.cusObjects.length > 0 ? parseInt(this.cusObjects[0]['P_TOTAL']) : 0;
            this.end = Math.ceil(this.total.valueOf() / this.limit.valueOf());

            this.showTable = (this.cusObjects.length > 0);
            spinner ? this.utilsService.hideSpinner() : 0;
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error_message);
            spinner ? this.utilsService.hideSpinner() : 0;
        });
    }

    search() {
        this.paginate(true);
    }

    limitChange() {
        this.page = 1;
        this.paginate(true);
    }

    pageChange(page: Number) {
        this.page = page;
        this.paginate(true);
    }

    onCreateMode() {
        this.create_code_cusobject = '';
        this.create_name_cusobject = '';
        this.create_is_active = false;
    }

    create() {
        if (
            !this.create_code_cusobject ||
            !this.create_name_cusobject
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        } else if (
            this.create_code_cusobject.trim() === '' ||
            this.create_name_cusobject.trim() === ''
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        }

        this.utilsService.showSpinner();
        let body = {
            id: 0,
            custCode: this.create_code_cusobject,
            custName: this.create_name_cusobject,
            isActive: this.create_is_active,
        };
        this.AddCustomerObjectService.createCustomerObject(body, (data) => {
            console.log(body);
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Tạo danh mục thành công');
                document.getElementById('createCusObjectsClose').click();
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Tạo danh mục không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }

    onUpdateMode(cusObjects) {
        this.update_id =  cusObjects.id;
        this.update_code_cusobject = cusObjects.custCode;
        this.update_name_cusobject = cusObjects.custName;
        this.update_is_active = cusObjects.isActive;
    }

    update() {
        if (
            !this.update_name_cusobject ||
            !this.update_code_cusobject
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        } else if (
            this.update_name_cusobject.trim() === '' ||
            this.update_code_cusobject.trim() === ''
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        }
        this.utilsService.showSpinner();
        let body = {
            id: this.update_id,
            custCode: this.update_code_cusobject,
            custName: this.update_name_cusobject,
            isActive: this.update_is_active
        };
        console.log(body);
        this.AddCustomerObjectService.updateCustomerObject(body, (data) => {
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Cập nhật thông tin danh mục thành công');
                document.getElementById('updateCusObjectsClose').click();
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Cập nhật thông tin danh mục không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }

    delete(cusObjects) {
        this.utilsService.showSpinner();
        this.AddCustomerObjectService.deleteCustomerObject({
            id: parseInt(cusObjects.id)
        }, (data) => {
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Xóa danh mục thành công');
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Xóa danh mục không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }
}
