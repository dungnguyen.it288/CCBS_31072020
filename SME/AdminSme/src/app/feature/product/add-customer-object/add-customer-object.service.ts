import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from 'app/core/config';

@Injectable()
export class AddCustomerObjectService {

    constructor(private http: HttpClient) { }

    getCustomerObjectList(filter, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            },
            param: filter
        };
        this.http.get(`${config.adminUrl}/api/CustomerService/initCustomerObject`, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }

    createCustomerObject(CustomerObjectInfo, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            }
        };
        this.http.post(`${config.adminUrl}/api/CustomerService/addNewCustomerObject`, CustomerObjectInfo, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }

    updateCustomerObject(CustomerObjectInfo, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            }
        };
        this.http.put(`${config.adminUrl}/api/CustomerService/updateCustomerObject`, CustomerObjectInfo, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }

    deleteCustomerObject(CustomerObjectInfo, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            }
        };
        this.http.delete(`${config.adminUrl}/api/CustomerService/deleteCustomerObject/${CustomerObjectInfo.id}`, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }
    
}
