import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { UtilsService } from 'app/core/services/utils.service';
import { AddCategoryService } from '../add-category/add-category.service';
import { AddAttrSetService } from '../add-attribute-set/add-attribute-set.service';

@Component({
    selector: 'app-add-category',
    templateUrl: './add-category.component.html',
    styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit, OnDestroy {
    showTable = false;
    categorys: Array<Object>;
    attrSets: Array<Object>;

    id: String;
    name: String;
    username: String;
    reseller_id: String;
    customer_id: String;

    page: Number;
    limit: Number;
    total: Number;
    end: Number;

    create_code_category: String; create_code_category_control = new FormControl('', [Validators.required]);
    create_name_category: String; create_name_category_control = new FormControl('', [Validators.required]);
    create_parent_category: String; create_parent_category_control = new FormControl('', [Validators.required]);
    create_attr_category: String; create_attr_category_control = new FormControl('', [Validators.required]);

    update_id: String;
    update_code_category: String; update_code_category_control = new FormControl('', [Validators.required]);
    update_name_category: String; update_name_category_control = new FormControl('', [Validators.required]);
    update_parent_category: String; update_parent_category_control = new FormControl('', [Validators.required]);
    update_attr_category: String; update_attr_category_control = new FormControl('', [Validators.required]);

    constructor(private router: Router, private utilsService: UtilsService, private AddCategoryService: AddCategoryService, private AddAttrSetService: AddAttrSetService) { }

    ngOnInit() {
        this.page = 1;
        this.limit = 10;
        this.paginate(true);
    }

    ngOnDestroy() {
    }

    paginate(spinner: Boolean) {
        spinner ? this.utilsService.showSpinner() : 0;
        let query = {
            pagesize: this.page || 1,
            pageindex: this.limit || 10
        };
        this.AddCategoryService.getCategoryList(query, (data) => {
            this.categorys = data.result.productCategoryList;

            // this.page = data.paginate.page;
            // this.limit = data.paginate.limit;
            this.total = this.categorys.length > 0 ? parseInt(this.categorys[0]['P_TOTAL']) : 0;
            this.end = Math.ceil(this.total.valueOf() / this.limit.valueOf());

            this.showTable = (this.categorys.length > 0);
            spinner ? this.utilsService.hideSpinner() : 0;
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error_message);
            spinner ? this.utilsService.hideSpinner() : 0;
        });
        this.AddAttrSetService.getAttributeSetList(query, (data) => {
            this.attrSets = data.result.productAttrSetList;

            // this.page = data.paginate.page;
            // this.limit = data.paginate.limit;
            this.total = this.attrSets.length > 0 ? parseInt(this.attrSets[0]['P_TOTAL']) : 0;
            this.end = Math.ceil(this.total.valueOf() / this.limit.valueOf());

            this.showTable = (this.attrSets.length > 0);
            spinner ? this.utilsService.hideSpinner() : 0;
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error_message);
            spinner ? this.utilsService.hideSpinner() : 0;
        });
    }

    search() {
        this.paginate(true);
    }

    limitChange() {
        this.page = 1;
        this.paginate(true);
    }

    pageChange(page: Number) {
        this.page = page;
        this.paginate(true);
    }

    onCreateMode() {
        this.create_code_category = '';
        this.create_name_category = '';
        this.create_parent_category = '';
        this.create_attr_category = '';
    }

    create() {
        if (
            !this.create_code_category ||
            !this.create_name_category
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        } else if (
            this.create_code_category.trim() === '' ||
            this.create_name_category.trim() === ''
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        }

        this.utilsService.showSpinner();
        let body = {
            id: 0,
            cgrCode: this.create_code_category,
            cgrName: this.create_name_category,
            cgrParentId: this.create_parent_category,
            ctlProductAttrSetId: this.create_attr_category
        };
        this.AddCategoryService.createCategory(body, (data) => {
            console.log(body);
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Tạo danh mục thành công');
                document.getElementById('createCategoryClose').click();
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Tạo danh mục không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }

    onUpdateMode(categorys) {
        this.update_id =  categorys.id;
        this.update_code_category = categorys.cgrCode;
        this.update_name_category = categorys.cgrName;
        this.update_parent_category = categorys.cgrParentId;
        this.update_attr_category = categorys.ctlProductAttrSetId;
    }

    update() {
        if (
            !this.update_name_category ||
            !this.update_code_category
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        } else if (
            this.update_name_category.trim() === '' ||
            this.update_code_category.trim() === ''
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        }
        this.utilsService.showSpinner();
        let body = {
            id: this.update_id,
            cgrCode: this.update_code_category,
            cgrName: this.update_name_category,
            cgrParentId: this.update_parent_category,
            ctlProductAttrSetId: this.update_attr_category
        };
        console.log(body);
        this.AddCategoryService.updateCategory(body, (data) => {
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Cập nhật thông tin danh mục thành công');
                document.getElementById('updateCategorysClose').click();
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Cập nhật thông tin danh mục không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }

    delete(categorys) {
        this.utilsService.showSpinner();
        this.AddCategoryService.deleteCategory({
            id: parseInt(categorys.id)
        }, (data) => {
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Xóa danh mục thành công');
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Xóa danh mục không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }
}
