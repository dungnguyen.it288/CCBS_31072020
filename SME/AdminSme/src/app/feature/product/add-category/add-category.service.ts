import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from 'app/core/config';

@Injectable()
export class AddCategoryService {

    constructor(private http: HttpClient) { }

    getCategoryList(filter, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            },
            param: filter
        };
        this.http.get(`${config.adminUrl}/api/Product/initProductCategory`, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }

    createCategory(categoryInfo, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            }
        };
        this.http.post(`${config.adminUrl}/api/Product/addNewProductCategory`, categoryInfo, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }

    updateCategory(categoryInfo, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            }
        };
        this.http.put(`${config.adminUrl}/api/Product/updateProductCategory`, categoryInfo, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }

    deleteCategory(categoryInfo, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            }
        };
        this.http.delete(`${config.adminUrl}/api/Product/deleteProductCategory/${categoryInfo.id}`, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }
    
}
