import { Routes } from '@angular/router';

import { AddCategoryComponent } from './add-category/add-category.component';
import { AddAttrComponent } from './add-attribute/add-attribute.component';
import { AddAttrSetComponent } from './add-attribute-set/add-attribute-set.component';
import { AddCustomerObjectComponent } from './add-customer-object/add-customer-object.component';
import { AddCusServiceComponent } from './add-new-customer-service/add-new-customer-service.component';

export const ProductRoutes: Routes = [
    {
        path: 'add-category',
        component: AddCategoryComponent
    },
    {
        path: 'add-attr',
        component: AddAttrComponent
    },
    {
        path: 'add-attr-set',
        component: AddAttrSetComponent
    },
    {
        path: 'add-customer-object',
        component: AddCustomerObjectComponent
    },
    {
        path: 'add-new-customer-service',
        component: AddCusServiceComponent
    }
];
