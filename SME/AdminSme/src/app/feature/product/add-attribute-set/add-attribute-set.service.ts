import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from 'app/core/config';

@Injectable()
export class AddAttrSetService {

    constructor(private http: HttpClient) { }

    getAttributeSetList(filter, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            },
            param: filter
        };
        this.http.get(`${config.adminUrl}/api/Product/initProductAttrSet`, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }

    createAttributeSet(attrSetInfo, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            }
        };
        this.http.post(`${config.adminUrl}/api/Product/addNewProductAttrSet`, attrSetInfo, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }

    updateAttributeSet(attrSetInfo, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            }
        };
        this.http.put(`${config.adminUrl}/api/Product/updateProductAttrSet`, attrSetInfo, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }

    deleteAttributeSet(attrSetInfo, done, fail) {
        let options = {
            headers: {
                'x-access-token': localStorage.getItem('accessToken')
            }
        };
        this.http.delete(`${config.adminUrl}/api/Product/deleteProductAttrSet/${attrSetInfo.id}`, options).toPromise().then(res => {
            done(res);
        }).catch(err => {
            fail(err);
        });
    }
}
