import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { UtilsService } from 'app/core/services/utils.service';
import { AddAttrSetService } from '../add-attribute-set/add-attribute-set.service';

@Component({
    selector: 'app-add-attribute-set',
    templateUrl: './add-attribute-set.component.html',
    styleUrls: ['./add-attribute-set.component.css']
})
export class AddAttrSetComponent implements OnInit, OnDestroy {
    showTable = false;
    attrsets: Array<Object>;
    cusObjects: Array<Object>;

    id: String;
    name: String;
    username: String;
    reseller_id: String;
    customer_id: String;

    page: Number;
    limit: Number;
    total: Number;
    end: Number;

    create_code_attribute_set: String; create_code_attribute_set_control = new FormControl('', [Validators.required]);
    create_name_attribute_set: String; create_name_attribute_set_control = new FormControl('', [Validators.required]);
    create_desc_attribute_set: String; create_desc_attribute_set_control = new FormControl('', [Validators.required]);

    update_id: String;
    update_code_attribute_set: String; update_code_attribute_set_control = new FormControl('', [Validators.required]);
    update_name_attribute_set: String; update_name_attribute_set_control = new FormControl('', [Validators.required]);
    update_desc_attribute_set: String; update_desc_attribute_set_control = new FormControl('', [Validators.required]);

    constructor(private router: Router, private utilsService: UtilsService, private AddAttrSetService: AddAttrSetService) { }

    ngOnInit() {
        this.page = 1;
        this.limit = 10;
        this.paginate(true);
    }

    ngOnDestroy() {
    }

    paginate(spinner: Boolean) {
        spinner ? this.utilsService.showSpinner() : 0;
        let query = {
            pagesize: this.page || 1,
            pageindex: this.limit || 10
        };
        this.AddAttrSetService.getAttributeSetList(query, (data) => {
            this.attrsets = data.result.productAttrSetList;

            console.log(this.attrsets);

            // this.page = data.paginate.page;
            // this.limit = data.paginate.limit;
            this.total = this.attrsets.length > 0 ? parseInt(this.attrsets[0]['P_TOTAL']) : 0;
            this.end = Math.ceil(this.total.valueOf() / this.limit.valueOf());

            this.showTable = (this.attrsets.length > 0);
            spinner ? this.utilsService.hideSpinner() : 0;
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error_message);
            spinner ? this.utilsService.hideSpinner() : 0;
        });
    }

    search() {
        this.paginate(true);
    }

    limitChange() {
        this.page = 1;
        this.paginate(true);
    }

    pageChange(page: Number) {
        this.page = page;
        this.paginate(true);
    }

    onCreateMode() {
        this.create_code_attribute_set = '';
        this.create_name_attribute_set = '';
        this.create_desc_attribute_set = '';
    }

    create() {
        if (
            !this.create_code_attribute_set ||
            !this.create_name_attribute_set
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        } else if (
            this.create_code_attribute_set.trim() === '' ||
            this.create_name_attribute_set.trim() === ''
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        }

        this.utilsService.showSpinner();
        let body = {
            id: 0,
            setCode: this.create_code_attribute_set,
            setName: this.create_name_attribute_set,
            setDesc: this.create_desc_attribute_set
        };
        this.AddAttrSetService.createAttributeSet(body, (data) => {
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Tạo danh mục thành công');
                document.getElementById('createAttrSetClose').click();
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Tạo danh mục không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }

    onUpdateMode(attrsets) {
        this.update_id =  attrsets.id;
        this.update_code_attribute_set = attrsets.setCode;
        this.update_name_attribute_set = attrsets.setName;
        this.update_desc_attribute_set = attrsets.setDesc;
    }

    update() {
        if (
            !this.update_name_attribute_set ||
            !this.update_code_attribute_set
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        } else if (
            this.update_name_attribute_set.trim() === '' ||
            this.update_code_attribute_set.trim() === ''
        ) {
            this.utilsService.showNotification('bottom', 'right', 'danger', 'Các trường nhập vào bị thiếu !');
            return;
        }
        this.utilsService.showSpinner();
        let body = {
            id: this.update_id,
            setCode: this.update_code_attribute_set,
            setName: this.update_name_attribute_set,
            setDesc: this.update_desc_attribute_set
        };
        console.log(body);
        this.AddAttrSetService.updateAttributeSet(body, (data) => {
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Cập nhật thông tin danh mục thành công');
                document.getElementById('updateAttrSetClose').click();
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Cập nhật thông tin danh mục không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }

    delete(attrsets) {
        this.utilsService.showSpinner();
        this.AddAttrSetService.deleteAttributeSet({
            id: parseInt(attrsets.id)
        }, (data) => {
            if (data.error_code === 0) {
                this.utilsService.showNotification('bottom', 'right', 'success', 'Xóa danh mục thành công');
            } else {
                this.utilsService.showNotification('bottom', 'right', 'danger', 'Xóa danh mục không thành công');
            }
            this.utilsService.hideSpinner();

            this.paginate(true);
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
            this.utilsService.hideSpinner();
        });
    }
}
