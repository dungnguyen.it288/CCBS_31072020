import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { UtilsService } from 'app/core/services/utils.service';
import { AuthenticationService } from 'app/feature/authentication/authentication.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit, OnDestroy {
    passwordChanged = false;

    user_id: String;
    username: String;
    company_name: String;
    company_address: String;
    email: String;

    constructor(private router: Router, private utilsService: UtilsService, private authenticationService: AuthenticationService) { }

    ngOnInit() {
        this.authenticationService.getUserInfo({
            user_name: localStorage.getItem('user_name')
        }, (data) => {
            this.user_id = data.result[0]['CUSTOMER_ID'];
            this.username = data.result[0]['USER_NAME'];
            this.company_name = data.result[0]['COMPANY_NAME'];
            this.company_address = data.result[0]['COMPANY_ADDRESS'];
            this.email = data.result[0]['EMAIL'];
        }, (err) => {
            this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
        });
    }

    ngOnDestroy() {
    }

    logout() {
        localStorage.removeItem('accessToken');
        localStorage.removeItem('user_id');
        localStorage.removeItem('user_username');
        localStorage.removeItem('user_name');
        localStorage.removeItem('user_module_permission');
        localStorage.removeItem('unit_id');
        localStorage.removeItem('unit_parent_id');
        localStorage.removeItem('unit_level');
        localStorage.removeItem('unit_name');
        this.router.navigate(['login']);
        this.utilsService.showNotification('bottom', 'right', 'warning', 'Đã đăng xuất');
    }

    // editUser() {
    //     if (this.user_id && this.username && this.password && this.name) {
    //         this.usersService.updateUser({
    //             user_id: this.user_id,
    //             username: this.username,
    //             password: this.password !== this.defaultPassword ? this.password : '-1',
    //             name: this.name,
    //             module_permission: this.module_permission
    //         }, (data) => {
    //             this.utilsService.showNotification('bottom', 'right', 'success', 'Sửa thông tin thành công');
    //         }, (err) => {
    //             this.utilsService.showNotification('bottom', 'right', 'danger', err.error.message);
    //         });
    //     }
    // }

}
