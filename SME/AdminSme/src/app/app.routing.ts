import { Routes } from '@angular/router';

import { MainLayoutComponent } from 'app/layouts/main/main-layout.component';
import { AuthLayoutComponent } from 'app/layouts/auth/auth-layout.component';

export const AppRoutes: Routes = [
    {
        path: '',
        component: MainLayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'profile',
                pathMatch: 'full'
            },
            {
                path: 'profile',
                loadChildren: 'app/feature/profile/profile.module#ProfileModule'
            },
            {
                path: 'system',
                loadChildren: 'app/feature/system/system.module#SystemModule'
            },
            {
                path: 'product',
                loadChildren: 'app/feature/product/product.module#ProductModule'
            },
            {
                path: 'reports',
                loadChildren: 'app/feature/reports/reports.module#ReportsModule'
            }
        ]
    }, {
        path: '',
        component: AuthLayoutComponent,
        loadChildren: 'app/feature/authentication/authentication.module#AuthenticationModule'
    }
];
