import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { DatePipe } from '@angular/common';

declare var $: any;

@Injectable()
export class UtilsService {

    constructor(private router: Router, private http: HttpClient, private spinnerService: NgxSpinnerService, private datepipe: DatePipe) { }

    showSpinner() {
        this.spinnerService.show();
    }

    hideSpinner() {
        this.spinnerService.hide();
    }

    showNotification(from, align, type, message) {
        const types = ['', 'info', 'success', 'warning', 'danger'];

        $.notify({
            icon: "notifications",
            message: message

        }, {
            type: type,
            timer: 4000,
            placement: {
                from: from,
                align: align
            },
            template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
                '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
                '<i class="material-icons" data-notify="icon">notifications</i> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
        });
    }

    checkModulePermission(permission) {
        if (localStorage.getItem('user_module_permission').search('all') === -1 && localStorage.getItem('user_module_permission').search(permission) === -1) {
            this.showNotification('bottom', 'right', 'danger', 'Bạn không có quyền truy cập chức năng này');
            this.router.navigate(['']);
        }
    }

    formatDatetoString(date) {
        return this.datepipe.transform(date, 'ddMMyyyy');
    }

    formatStringtoDate(text) {
        return `${text.substring(3, 5)}/${text.substring(0, 2)}/${text.substring(6, 10)}`;
    }
}
