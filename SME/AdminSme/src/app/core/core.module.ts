import { NgModule, Optional, SkipSelf } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from "ngx-spinner";

import { UtilsService } from 'app/core/services/utils.service';
import { AuthenticationService } from 'app/feature/authentication/authentication.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgxSpinnerModule
    ],
    providers: [
        UtilsService,
        AuthenticationService,
        DatePipe
    ]
})

export class CoreModule {

    constructor(@Optional() @SkipSelf() core: CoreModule) {
        if (core) {
            throw new Error('You shall not pass !');
        }
    }
}
