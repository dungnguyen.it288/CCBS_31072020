create or replace PACKAGE BODY laphd_hmm
IS
 FUNCTION LAYTT_NGUOIVANDONG(
       PSUSERID IN VARCHAR2
     , PSMATINHNGDUNG IN VARCHAR2
     , PSSCHEMAUSER IN VARCHAR2
     , PSDONVIQL   IN VARCHAR2
    ) RETURN SYS_REFCURSOR
    IS
        C SYS_REFCURSOR;
        S VARCHAR2(1000);
    BEGIN
     IF PSMATINHNGDUNG IN('HNI','HCM') THEN
        S:=' SELECT a.ten_nguoi,a.ma_nguoi FROM '||PSSCHEMAUSER||'nguoivandongs a where a.status = 1' ;
     ELSE
       IF PSDONVIQL IS NOT NULL AND LENGTH(PSDONVIQL)>0 THEN
         S:=' SELECT a.ten_nguoi,a.ma_nguoi FROM '||PSSCHEMAUSER||'nguoivandongs a where a.status = 1'

             ||'  ';
       END IF;

     END IF;
     S:= S||' ORDER BY a.ten_nguoi';
      DBMS_OUTPUT.PUT_LINE(S);
        OPEN C FOR S;
        RETURN C;
     EXCEPTION
          WHEN OTHERS THEN
             DECLARE
                STRTMP_  VARCHAR2(2000);
             BEGIN
                STRTMP_ :='laytt_nguoivandong|Error: '||TO_CHAR(SQLCODE)||': '||SQLERRM;
                STRTMP_ := STRTMP_||' | s = ' || S;
                PLOG.ERROR(STRTMP_);
             END;
            OPEN C FOR 'select '''' ten_nguoi, '''' ma_nguoi  from dual';
            RETURN C;
END;

FUNCTION LAYTT_NGUOIVANDONG_HRM(
       PSUSERID IN VARCHAR2
     , PSMATINHNGDUNG IN VARCHAR2
     , PSSCHEMAUSER IN VARCHAR2
     , PSDONVIQL   IN VARCHAR2
     , PSTYPE   IN VARCHAR2
    ) RETURN SYS_REFCURSOR
    IS
        C SYS_REFCURSOR;
        S VARCHAR2(1000);
    BEGIN
     IF PSMATINHNGDUNG IN('HNI','HCM') THEN
        S:=' SELECT a.ten_nguoi,a.ma_nguoi FROM '||PSSCHEMAUSER||'nguoivandongs a where a.status = 1' ;
     ELSE
       IF PSDONVIQL IS NOT NULL AND LENGTH(PSDONVIQL)>0 THEN
         S:=' SELECT a.ten_nguoi,a.ma_nguoi FROM '||PSSCHEMAUSER||'nguoivandongs a where a.status = 1'

             ||'  ';
       END IF;

     END IF;

     IF (PSTYPE != '2') then
         s:=s||' and HRM_STATE = 1) ';
     end IF;

     S:= S||' ORDER BY a.ten_nguoi';
     -- DBMS_OUTPUT.PUT_LINE(S);
        OPEN C FOR S;
        RETURN C;

     EXCEPTION
          WHEN OTHERS THEN
             DECLARE
                STRTMP_  VARCHAR2(2000);
             BEGIN
                STRTMP_ :='laytt_nguoivandong|Error: '||TO_CHAR(SQLCODE)||': '||SQLERRM;
                STRTMP_ := STRTMP_||' | s = ' || S;
                PLOG.ERROR(STRTMP_);
             END;
            OPEN C FOR 'select '''' ten_nguoi, '''' ma_nguoi  from dual';
            RETURN C;

END;

FUNCTION LAYTT_NGUOIVANDONG_LAPHD_HRM(
       PSUSERID IN VARCHAR2
     , PSMATINHNGDUNG IN VARCHAR2
     , PSSCHEMAUSER IN VARCHAR2
     , PSDONVIQL   IN VARCHAR2
     , PSTYPE   IN VARCHAR2
    ) RETURN SYS_REFCURSOR
    IS
        C SYS_REFCURSOR;
        S VARCHAR2(1000);
    BEGIN
    s:='select * From ( select ''Kh&#244;ng ch&#7885;n '','''||PSUSERID||''' ma_nguoi, ''1'' ords from dual union all ';
     IF PSMATINHNGDUNG IN('HNI','HCM') THEN
        S:=s||' SELECT a.ten_nguoi,a.ma_nguoi, a.ten_nguoi ords FROM '||PSSCHEMAUSER||'nguoivandongs a where a.status = 1' ;
     ELSE
       IF PSDONVIQL IS NOT NULL AND LENGTH(PSDONVIQL)>0 THEN
         S:=s||' SELECT a.ten_nguoi,a.ma_nguoi , a.ten_nguoi ords FROM '||PSSCHEMAUSER||'nguoivandongs a where a.status = 1'

             ||'  ';
       END IF;

     END IF;
     if (PSTYPE != '2') then
         s:=s||' and HRM_STATE = '||PSTYPE||' ) ';
     end if;
     S:= S||' ';
      DBMS_OUTPUT.PUT_LINE(S);
        OPEN C FOR S;
        RETURN C;
     EXCEPTION
          WHEN OTHERS THEN
             DECLARE
                STRTMP_  VARCHAR2(2000);
             BEGIN
                STRTMP_ :='laytt_nguoivandong|Error: '||TO_CHAR(SQLCODE)||': '||SQLERRM;
                STRTMP_ := STRTMP_||' | s = ' || S;
                PLOG.ERROR(STRTMP_);
             END;
            OPEN C FOR 'select '''' ten_nguoi, '''' ma_nguoi  from dual';
            RETURN C;
 END;

FUNCTION LAYDS_NGUOI_KHAC_CTV
      ( PSUSERID        IN VARCHAR2
      ,PSMATINHNGDUNG   IN VARCHAR2
      ,PSDATASCHEMA    IN VARCHAR2
      ,PIPAGEID        IN VARCHAR2
      ,PIREC_PER_PAGE  IN VARCHAR2
      ,PSMA_TTHI       IN VARCHAR2
      ,PSTEN_TTHI      IN VARCHAR2
    )
    RETURN  SYS_REFCURSOR
    IS
        C_CURSOR SYS_REFCURSOR;
        SSQL VARCHAR2(10000);
    BEGIN
        SSQL :=' SELECT a.ten_nguoi,a.ma_nguoi FROM '||PSDATASCHEMA||'nguoivandongs a WHERE 1=1 and HRM_TYPE NOT LIKE ''%CTV%''' ;

        DBMS_OUTPUT.PUT_LINE(SSQL);
        IF (PSMA_TTHI IS NOT NULL) AND (LENGTH(PSMA_TTHI)>0) THEN
            SSQL:=SSQL||' AND ccs_admin.uto_khongdau(lower(trim(a.ma_nguoi))) like ''%'||LOWER(TRIM(PSMA_TTHI))||'%''';
        END IF;

        IF (PSTEN_TTHI IS NOT NULL) AND (LENGTH(PSTEN_TTHI)>0) THEN
            SSQL:=SSQL||' AND
                lower(translate(translate(a.ten_nguoi,PTTB_ALERT.tcvn3, PTTB_ALERT.khongdau),PTTB_ALERT.unicode, PTTB_ALERT.khongdau)) like
                    replace(''%'||LOWER(TRANSLATE(TRANSLATE(REPLACE(PSTEN_TTHI,'#','%'),CCS_ADMIN.PTTB_ALERT.TCVN3, CCS_ADMIN.PTTB_ALERT.KHONGDAU),CCS_ADMIN.PTTB_ALERT.UNICODE, CCS_ADMIN.PTTB_ALERT.KHONGDAU))||'%'',''%%'','''')';
        END IF;

--        if (PSTYPE != '2') then
--         SSQL:=SSQL||' and HRM_STATE = '||PSTYPE||' ';
--        end if;

        SSQL := SSQL ||'order by a.ten_nguoi';
        OPEN C_CURSOR FOR CCS_ADMIN.PTTB.XULY_PHANTRANG(SSQL,PIPAGEID,PIREC_PER_PAGE);
        RETURN C_CURSOR;
    EXCEPTION
       WHEN OTHERS THEN 
         DECLARE
              STRTMP  VARCHAR2(2000);
         BEGIN
             STRTMP := 'layds_nguoi_tiepthi | Error: '||TO_CHAR(SQLCODE)||': '||SQLERRM;
--             PLOG.ERROR(STRTMP);
             OPEN C_CURSOR FOR 'SELECT '''' FROM DUAL';
             RETURN  C_CURSOR;
         END;
    END;


FUNCTION LAYDS_NGUOI_TIEPTHI_HRM
      ( PSUSERID        IN VARCHAR2
      ,PSMATINHNGDUNG   IN VARCHAR2
      ,PSDATASCHEMA    IN VARCHAR2
      ,PIPAGEID        IN VARCHAR2
      ,PIREC_PER_PAGE  IN VARCHAR2
      ,PSMA_TTHI       IN VARCHAR2
      ,PSTEN_TTHI      IN VARCHAR2
      ,PSHRM   IN VARCHAR2
      ,PSTTKD   IN VARCHAR2
    )
    RETURN  SYS_REFCURSOR
    IS
        C_CURSOR SYS_REFCURSOR;
        SSQL VARCHAR2(10000);
    BEGIN
        SSQL :=' SELECT a.ten_nguoi,a.ma_nguoi FROM '||PSDATASCHEMA||'nguoivandongs a WHERE 1=1 and (HRM_STATE = '|| PSHRM ||' OR HRM_STATE = '|| PSTTKD ||')' ;

        DBMS_OUTPUT.PUT_LINE(SSQL);
        IF (PSMA_TTHI IS NOT NULL) AND (LENGTH(PSMA_TTHI)>0) THEN
            SSQL:=SSQL||' AND ccs_admin.uto_khongdau(lower(trim(a.ma_nguoi))) like ''%'||LOWER(TRIM(PSMA_TTHI))||'%''';
        END IF;

        IF (PSTEN_TTHI IS NOT NULL) AND (LENGTH(PSTEN_TTHI)>0) THEN
            SSQL:=SSQL||' AND
                lower(translate(translate(a.ten_nguoi,PTTB_ALERT.tcvn3, PTTB_ALERT.khongdau),PTTB_ALERT.unicode, PTTB_ALERT.khongdau)) like
                    replace(''%'||LOWER(TRANSLATE(TRANSLATE(REPLACE(PSTEN_TTHI,'#','%'),CCS_ADMIN.PTTB_ALERT.TCVN3, CCS_ADMIN.PTTB_ALERT.KHONGDAU),CCS_ADMIN.PTTB_ALERT.UNICODE, CCS_ADMIN.PTTB_ALERT.KHONGDAU))||'%'',''%%'','''')';
        END IF;

--        if (PSTYPE != '2') then
--         SSQL:=SSQL||' and HRM_STATE = '||PSTYPE||' ';
--        end if;

        SSQL := SSQL ||'order by a.ten_nguoi';
        OPEN C_CURSOR FOR CCS_ADMIN.PTTB.XULY_PHANTRANG(SSQL,PIPAGEID,PIREC_PER_PAGE);
        RETURN C_CURSOR;
    EXCEPTION
       WHEN OTHERS THEN 
         DECLARE
              STRTMP  VARCHAR2(2000);
         BEGIN
             STRTMP := 'layds_nguoi_tiepthi | Error: '||TO_CHAR(SQLCODE)||': '||SQLERRM;
--             PLOG.ERROR(STRTMP);
             OPEN C_CURSOR FOR 'SELECT '''' FROM DUAL';
             RETURN  C_CURSOR;
         END;
    END;
END;