
FUNCTION province_chance_report_edt(psDataSchema VARCHAR2,
                             psUserId  VARCHAR2,
                             psUserIp  VARCHAR2,
                             psTungay  VARCHAR2,
                             psDenngay  VARCHAR2,
                             psNguoiTh VARCHAR2,
                             psMa_bc  VARCHAR2,
                             psDonViQL VARCHAR2,
                             psLoaiBC VARCHAR2,
                             psdn VARCHAR2,
                             psKieuBC NUMBER)
RETURN sys_refcursor
IS
   s             VARCHAR2 (30000);
   s_             VARCHAR2 (30000);
   ref_         sys_refcursor;
   ds_mabc       VARCHAR (1000);
   agent_ VARCHAR2(100):= substr(psDataSchema,5,3);
   psAgentCode VARCHAR2(100);
   psBuuCuc VARCHAR2(100);
   psDonVi VARCHAR2(100);
   dbl varchar2(50):='';
BEGIN
    ds_mabc:=REPLACE(''''||psMa_bc||'''',',',''',''');
    IF psLoaiBC = 1 THEN
        psAgentCode :=' a.ma_tinh_cu ';
        psBuuCuc:= 'a.ma_bc_cu ';
        psDonVi:= 'a.donviql_id_cu ';
    ELSE
        psAgentCode :=' a.ma_tinh ';
        psBuuCuc:= 'a.ma_bc ';
        psDonVi:= 'a.donviql_id ';
    END IF;
    s:='SELECT a.ma_kh,a.ma_kh_cu,a.ma_tb,a.ten_tt ten_tb,a.diachi_tt diachi,to_char(a.ngay_ht,''DD/MM/YYYY'') ngay_ht,a.ma_tinh_cu,
        a.nguoi_laphd nguoi_th,b.tenbuucuc,c.ten_dv,a.ma_tinh,a.tentb_cu,a.diachi_cu
         FROM ccs_common.thuebao_cts'||dbl||' a,
        '||psDataSchema||'buucucthus'||dbl||' b, '||psDataSchema||'donvi_qls'||dbl||' c
        WHERE a.trang_thai=5 and '||psDonVi||'=c.donviql_id AND '||psBuuCuc||'=b.ma_bc
        AND a.ngay_ht >= to_date('''||psTungay||''',''dd/mm/yyyy'')
        and  a.ngay_ht < to_date('''||psDenngay||''',''dd/mm/yyyy'') + 1
        and '||psAgentCode||' ='''||agent_||''' ';
    IF (psMa_bc <>'0') AND  (psMa_bc IS NOT null) THEN
    s:=s|| '  AND '||psBuuCuc||' in ('||ds_mabc||')';
    END IF;
    IF (psDonViQL <>'0') AND (psDonViQL IS NOT null ) THEN
     s:=s|| '  AND '||psDonVi||' in ('||psDonViQL||')';
     ELSE
         if(psDataSchema='CCS_HNI.') THEN
            if psdn ='1' then
                            s:=s|| '  AND '||psDonVi||' in (34)';
                          else
                            s:=s|| '  AND '||psDonVi||' not in (34)';
                          end if;
         END IF;
    END IF;
    IF (psNguoiTh <>'0') AND (psNguoiTh IS NOT null ) AND psLoaiBC=2 THEN
     s:=s|| '  AND a.nguoi_laphd = '''||psNguoiTh||'''';
    END IF;

  IF psKieuBC = 2 THEN
    s:= 'SELECT
decode(grouping(nguoi_th)+grouping(tenbuucuc)+grouping(ten_dv),0,max(ten_dv),1,max(ten_dv),2,max(ten_dv),''Tong:'') ten_dv,
decode(grouping(nguoi_th)+grouping(tenbuucuc)+grouping(ten_dv),0,max(tenbuucuc),1,max(tenbuucuc)) tenbuucuc,
decode(grouping(nguoi_th)+grouping(tenbuucuc)+grouping(ten_dv),0,max(nguoi_th)) nguoi_th,
decode(grouping(nguoi_th)+grouping(tenbuucuc)+grouping(ten_dv),0,count(ma_tb),COUNT(ma_tb)) sl_tb
FROM
( '||s||') GROUP BY ROLLUP (ten_dv,tenbuucuc,nguoi_th)' ;
  ELSE
    s:=s|| ' order by c.ten_dv,b.tenbuucuc';
  END IF;
dbms_output.put_line(s);
  OPEN ref_ FOR s;

   RETURN ref_;
EXCEPTION
   WHEN OTHERS THEN
     s:='SELECT '''' ma_kh,'''' ma_tb,'''' ten_tb,'''' diachi,'''' ngay_ld,'''' donviql_id,
                '''' ma_bc,'''' tenbuucuc,'''' ten_dv  from dual where 0=1';
     OPEN ref_ FOR s;
     RETURN ref_;

END;